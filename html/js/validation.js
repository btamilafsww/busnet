$(document).ready(function(){$.validator.setDefaults({ignore:":hidden:not(.admin_cancel_ride_chosen)"});$('.checkboxCon input:checked').parent().css('background-position','-114px -260px');$("#selectallseeker").click(function(){$('.caseSeeker').attr('checked',this.checked);if(this.checked){$(this).parent().addClass('checked');$('.checkboxCon').css('background-position','-114px -260px');}else{$(this).parent().removeClass('checked');$('.checkboxCon').css('background-position','-38px -260px');}});$(".caseSeeker").click(function(){if($(".caseSeeker").length==$(".caseSeeker:checked").length){$("#selectallseeker").attr("checked","checked");$("#selectallseeker").parent().addClass("checked");}else{$("#selectallseeker").removeAttr("checked");$("#selectallseeker").parent().removeClass("checked");}});$('.checkboxCon input').click(function(){if(this.checked){$(this).parent().css('background-position','-114px -260px');}else{$(this).parent().css('background-position','-38px -260px');}});});function checkBoxValidationUser(req,AdmEmail){var tot=0;var chkVal='on';var frm=$('#seekerActionForm input');for(var i=0;i<frm.length;i++){if(frm[i].type=='checkbox'){if(frm[i].checked){tot=1;if(frm[i].value!='on'){chkVal=frm[i].value;}}}}if(tot==0){alert(admin_checkBoxvalidationadmin);return false;}else if(chkVal=='on'){alert(admin_no_records_found);return false;}else{var didConfirm=confirm(admin_checkboxvalidationuser);if(didConfirm==true){$('#statusMode').val(req);$('#seekerActionForm').submit();}else{return false;}}}function checkBoxValidationAdmin(req,AdmEmail){var tot=0;var chkVal='on';var frm=$('#display_form input');for(var i=0;i<frm.length;i++){if(frm[i].type=='checkbox'){if(frm[i].checked){tot=1;if(frm[i].value!='on'){chkVal=frm[i].value;}}}}if(tot==0){alert(admin_checkBoxvalidationadmin);return false;}else if(chkVal=='on'){alert(admin_no_records_found);return false;}else{confirm_global_status(req,AdmEmail);}}function checkBoxWithSelectValidationAdmin(req,AdmEmail){var templat=$('#mail_contents').val();if(templat==''){alert(admin_select_mail_tempolate);return false;}var tot=0;var chkVal='on';var frm=$('#display_form input');for(var i=0;i<frm.length;i++){if(frm[i].type=='checkbox'){if(frm[i].checked){tot=1;if(frm[i].value!='on'){chkVal=frm[i].value;}}}}if(tot==0){alert(admin_checkBoxvalidationadmin);return false;}else if(chkVal=='on'){alert(admin_no_records_found);return false;}else{confirm_global_status(req,AdmEmail);}}function SelectValidationAdmin(req,AdmEmail){var templat=$('#mail_contents').val();if(templat==''){alert(admin_select_mail_tempolate);return false;}confirm_global_status(req,AdmEmail);}function confirm_global_status(req,AdmEmail){$.confirm({'title':Confirmation,'message':admin_checkboxvalidationuser,'buttons':{'Yes':{'class':'yes','action':function(){bulk_logs_action(req,AdmEmail);},'title':Yes},'No':{'class':'no','action':function(){return false;},'title':No}}});}function bulk_logs_action(req,AdmEmail){$("#collect_admin_email").modal();$("#admin_bulkAction").val(req);$("#admin_bulkSecurityEmail").val(AdmEmail);$('#simplemodal-container').css('height','150px');}function validate_admin_security_email(){var action=$("#admin_bulkAction").val();var AdmEmail=$("#admin_bulkSecurityEmail").val();var security_mail=$("#security_alertBox").val();if(security_mail==AdmEmail){$('#statusMode').val(action);$('#SubAdminEmail').val(AdmEmail);$('#display_form').submit();}else{$("#security_alertBox").css('border','1px solid red');}}function confirm_status(path){$.confirm({'title':Confirmation,'message':admin_common_change_status_record,'buttons':{Yes:{'class':'yes','action':function(){window.location=BaseURL+path;},'title':Yes},No:{'class':'no','action':function(){return false;},'title':No}}});}function confirm_mode(path){$.confirm({'title':Confirmation,'message':admin_change_mode_record,'buttons':{Yes:{'class':'yes','action':function(){window.location=BaseURL+path;},'title':Yes},No:{'class':'no','action':function(){return false;},'title':No}}});}


function confirm_delete(path,cdelete)
{
	var cdelete=cdelete||'permanent';
	if(cdelete=='permanent')
	{
		var msg=admin_delete_record_restore_later;
	}
	else
	{
		var msg=admin_delete_record_can_restore_later;
	}
	$.confirm({'title':security_delete,'message':msg,'buttons':{Yes:{'class':'yes','action':function(){window.location=BaseURL+path;},'title':Yes},No:{'class':'no','action':function(){return false;},'title':No}}});
}


	function checkBoxCategory(){var tot=0;var chkVal='on';var frm=$('#display_form input');for(var i=0;i<frm.length;i++){if(frm[i].type=='checkbox'){if(frm[i].checked){tot=1;chkVal=frm[i].value;}}}if(tot==0){alert(admin_checkBoxvalidationadmin);return false;}else if(tot>1){alert(admin_select_only_one_checkbox);return false;}else if(chkVal=='on'){alert(admin_no_records_found);return false;}else{confirm_category_checkbox(chkVal);}}function confirm_category_checkbox(chkVal){$.confirm({'title':'Confirmation','message':admin_checkboxvalidationuser,'buttons':{Yes:{'class':'yes','action':function(){$('#checkboxID').val(chkVal);$('#display_form').submit();},'title':Yes},No:{'class':'no','action':function(){return false;},'title':No}}});}function IsEmail(email){var regex=/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;return regex.test(email);}function validateAlphabet(value){var regexp=/^[a-zA-Z ]*$/;return regexp.test(value);}function hideErrDiv(arg){$("#"+arg).hide("slow");}function confirm_status_dashboard(path){$.confirm({'title':Confirmation,'message':admin_common_change_status_record,'buttons':{Yes:{'class':'yes','action':function(){window.location=BaseURL+'admin/dashboard/admin_dashboard';},'title':Yes},No:{'class':'no','action':function(){return false;},'title':No}}});}function changeCatPos(evt,catID){var pos=$(evt).prev().val();if((pos-pos)!=0){alert('Invalid position');return;}else{$(evt).hide();$(evt).next().show();$.ajax({type:'post',url:baseURL+'admin/category/changePosition',data:{'catID':catID,'pos':pos},complete:function(){$(evt).next().hide();$(evt).show().text('Done').delay(800).text('Update');}});}}var checked=false;function checkBoxController(frm,names,search_mode){if(checked==false){checked=true;}else{checked=false;}for(var i=0;i<frm.elements.length;i++){if(frm.elements[i].name==names){frm.elements[i].checked=checked;}}}function viewTrips(){var date_range=$('#date_range').val();date_range=btoa(date_range);window.location.href=baseURL+"admin/revenue/display_site_revenue?range="+encodeURIComponent(date_range);}function viewoverallTrips(){window.location.href=baseURL+"admin/revenue/display_site_revenue";}$(document).ready(function(){$(".Vehicle_Number_Chk").blur(function(){var vehicle_number=$(this).val();var driver_id=$("#driver_id").val();if(vehicle_number!=null&&vehicle_number!=''){$('#vehicle_number_exist').html(checking_number+'....');$.ajax({type:'post',url:'site/cms/check_number',data:{'vehicle_number':vehicle_number,'driver_id':driver_id},dataType:'json',success:function(res){$('#vehicle_number_exist').show();if(res.status=='1'){$('#vehicle_number_exist').html(res.message);$(".Vehicle_Number_Chk").val('');return false;}else{$('#vehicle_number_exist').html('');$('#vehicle_number_exist').hide();}}});}});});$("form").submit(function(){$("input[type=text],textarea").each(function(){$input=$(this).val();$new_input=preg_replace('#<script(.*?)>(.*?)</script>#is','',$input);$(this).val($new_input);});return false;});function viewTripsManal(){var mfrom=$('#billFromdate').val();var mto=$('#billTodate').val();var locationFilter=$('#locationFilter').val();if(mfrom==null||mfrom==""||mto==null||mto==""){alert('Select the month range.');}else{window.location.href=baseURL+"admin/revenue/display_site_revenue?from="+encodeURIComponent(mfrom)+'&to='+encodeURIComponent(mto)+'&location_id='+locationFilter;}}$(document).ready(function(){$('#locationFilter').click(function(){$('#locationFilter').change(function(){var mfrom=$('#billFromdate').val();var mto=$('#billTodate').val();var locationFilter=$('#locationFilter').val();window.location.href=baseURL+"admin/revenue/display_site_revenue?from="+encodeURIComponent(mfrom)+'&to='+encodeURIComponent(mto)+'&location_id='+locationFilter;});});});






function student_no_trip_trans(message,url)
{

	$.confirm({
 		'title'		: 'No trip or route found',
 		'message'	: message,
 		'buttons'	: {
 			'Yes': {
 				'class'	: 'yes',
 				'action': function(){
					window.location.href = url ;
 				},
                'title' : Yes
 			},
 			'No'	: {
 				'class'	: 'no',
 				'action': function(){
 					window.location.href = "/tranport/";
 				},
                'title' : No            // Nothing to do in this case. You can as well omit the action property.
 			}
 		}
 	});


}	





function student_no_trip(message,url,return_url)
{

	$.confirm({
 		'title'		: 'No route found',
 		'message'	: message,
 		'buttons'	: {
 			'Yes': {
 				'class'	: 'yes',
 				'action': function(){
					window.location.href = url ;
 				},
                'title' : Yes
 			},
 			'No'	: {
 				'class'	: 'no',
 				'action': function(){
 					window.location.href = "/"+return_url;
 				},
                'title' : No            // Nothing to do in this case. You can as well omit the action property.
 			}
 		}
 	});


}	




function all_no_data(title,message,url,return_url)
{

	$.confirm({
 		'title'		: title,
 		'message'	: message,
 		'buttons'	: {
 			'Yes': {
 				'class'	: 'yes',
 				'action': function(){
					window.location.href = url ;
 				},
                'title' : Yes
 			},
 			'No'	: {
 				'class'	: 'no',
 				'action': function(){
 					window.location.href = "/"+return_url;
 				},
                'title' : No            // Nothing to do in this case. You can as well omit the action property.
 			}
 		}
 	});


}	
