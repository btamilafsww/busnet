<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
*
*   Rides Management in operator panel
* 
*   @package    CI
*   @subpackage Controller
*   @author Katenterprise
*
**/

class Other_users extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('cookie', 'date', 'form','ride_helper','distcalc_helper'));
        $this->load->library(array('encrypt', 'form_validation'));
        $this->load->model(array('subadmin_model','app_model','brand_model','rides_model','vehicle_model','operators_model','bus_route_model','driver_model','bus_details_model','student_details_model','bus_trip_model'));
		if($this->checkLogin('O') != ''){
			$operator_id = $this->checkLogin('O');
			$chkOperator = $this->app_model->get_selected_fields(OPERATORS,array('_id' => new MongoDB\BSON\ObjectId($operator_id)),array('status'));
			$chkstatus = TRUE;
			$errMsg = '';
			if($chkOperator->num_rows() == 1){
				if($chkOperator->row()->status == 'Inactive'){
					$chkstatus = FALSE;
						if ($this->lang->line('operator_inactive_message') != '') 
								$errMsg= stripslashes($this->lang->line('operator_inactive_message')); 
						else  $errMsg = 'Your account is temporarily deactivated, Please contact admin';
						
				}
			} else {
				$chkstatus = FALSE;
				if ($this->lang->line('account_not_found') != '') 
						$errMsg= stripslashes($this->lang->line('account_not_found')); 
				else  $errMsg = 'Your account details not found';
				
			}
			if(!$chkstatus){
				 $newdata = array(
					'last_logout_date' => date("Y-m-d H:i:s")
				);
				$collection = OPERATORS;
				
				$condition = array('_id' => $this->checkLogin('O'));
				$this->app_model->update_details($collection, $newdata, $condition);
				$operatordata = array(
							APP_NAME.'_session_operator_id' => '',
							APP_NAME.'_session_operator_name' => '',
							APP_NAME.'_session_operator_email' => '',
							APP_NAME.'_session_vendor_location' =>''
						   
						);
				$this->session->unset_userdata($operatordata);
				$this->setErrorMessage('error', $errMsg);
				redirect(OPERATOR_NAME);
			}
		}
		
    }
    
	

   public function add_new_user()
   {	
	    if ($this->checkLogin('O') == '') 
	    {
            redirect(OPERATOR_NAME);
        } 
        else 
        {           

          	$this->data['heading'] = 'Add User Management';			


          	$this->data['re_namer']= array('driver' => 'Driver' ,'trip' => "Trip",'bus_route' => 'Bus Route','add_bus' => 'Vehicle','student_management_details' => "Student Management",'notification' => "Notification","logs" => "Logs","dashboard" => "Driver Dashboard","edit_driver_form" => "Edit driver","add_driver_form" => "Add Driver","view_driver"=>"View Driver","assign_route_view" => "Add Trip","view_trip" => "View Trip","edit_trip_bus" => "Edit Trip","add_bus_route" => "Add Route","view_bus_route" => "View Route" ,"add_bus_new" =>"Add Vehicle","view_bus_details" => "View Vehicle","add_new_class" => "Add Class","add_studentdetails"=>"Add Student","view_student_details" => "View Student Details","edit_student_detail" => "Edit Student","send_users_notification" => "Send Users Notification","send_drivers_notification" => "Send Drivers Notification","trip_details_list" => "Trip History" ,"display_parent_logs" => "Parent Notification Log","display_driver_logs" => "Driver Notification Logs") ;

			$this->load->view(OPERATOR_NAME.'/users/add_new_user',$this->data);
        }  
   }



   public function insert_user()
   {

   		 if ($this->checkLogin('O') == '') 
   		 {
            redirect(OPERATOR_NAME);
       	}
       	else 
       	{
					$operator_id =  $this->session->userdata(APP_NAME.'_session_operator_id');						
					$email = $this->input->post('email');					
					$mobile_number = $this->input->post('mobile_number');
					
					$returnUrl = OPERATOR_NAME.'/other_users/add_new_user';
					
					$privileges=json_decode($this->input->post('set_pages'));

					$operatordata=$this->app_model->get_all_details(OPERATORS,array('_id' => new MongoDB\BSON\ObjectId($operator_id) ))->result();					
	
					$operator_location = $operatordata[0]->operator_location;

					$dail_code =$operatordata[0]->dail_code;

					$condition=array('email' => $email);
					
				
				
					
					$duplicate_email = $this->app_model->get_all_details(OTHER_OPERATOR,$condition);
					if ($duplicate_email->num_rows() > 0){
							$this->setErrorMessage('error','Email address already exists');
							redirect($returnUrl);
					}

					$duplicate_email = $this->app_model->get_all_details(OPERATORS,$condition);
					if ($duplicate_email->num_rows() > 0){
							$this->setErrorMessage('error','Email address already exists');
							redirect($returnUrl);
					}			

					$condition = array('dail_code' => $dail_code,'mobile_number' => $mobile_number);
					
					$duplicate_phone = $this->app_model->get_all_details(OTHER_OPERATOR,$condition);
					if ($duplicate_phone->num_rows() > 0){
							$this->setErrorMessage('error','Mobile number already exists');
							redirect($returnUrl);
					}	


					$condition = array('dail_code' => $dail_code,'mobile_number' => $mobile_number);
					
					$duplicate_phone = $this->app_model->get_all_details(OPERATORS,$condition);
					if ($duplicate_phone->num_rows() > 0){
							$this->setErrorMessage('error','Mobile number already exists');
							redirect($returnUrl);
					}			




					$excludeArr = array("admin_password","set_pages","status", "operator_id", "mobile_number", "dail_code", "operator_location", "address", "country", "state", "city", "postal_code");
						
if(is_object($operatordata[0]->address))
    $operatordata[0]->address=(array)$operatordata[0]->address;
					$addressArr['address'] = array('address' => $operatordata[0]->address['address'] ,
																'country' => $operatordata[0]->address['country'],
																'state' =>$operatordata[0]->address['state'],
																'city' => $operatordata[0]->address['city'],
																'postal_code' => $operatordata[0]->address['postal_code']
																	);
			
					$password = $this->input->post('admin_password');
				
					if ($this->input->post('status') != '')
					{
						$status = 'Active';
					} 
					else 
					{
						$status = 'Inactive';
					}	
				
					$operator_data = array(
											'operator_id' => $operator_id,	
											'status' => $status,
											'created' => date('Y-m-d H:i:s'),
											'password' => md5($password),
											'dail_code' => $dail_code,
											'mobile_number' => (string) $this->input->post('mobile_number'),
											'privileges' => $privileges
										);
					
				    $operator_data['operator_location'] = $operator_location;		
										
				    $dataArr = array_merge($operator_data,$addressArr);				 
					
					$this->app_model->commonInsertUpdate(OTHER_OPERATOR,'insert',$excludeArr,$dataArr);
					$this->setErrorMessage('success','User added successfully');
					
					$last_insert_id = $this->cimongo->insert_id();
					$this->mail_model->send_operator_register_confirmation_mail($last_insert_id,$password);				
					
					redirect(OPERATOR_NAME.'/other_users/display_user_list');
			}
   }

	 public function display_user_list() 
	 {




        if ($this->checkLogin('O') == '') {
            redirect(OPERATOR_NAME);
        } else {
            if ($this->lang->line('admin_menu_users_list') != '') 
            $this->data['heading']= stripslashes($this->lang->line('admin_menu_users_list')); 
            else  $this->data['heading'] = 'Users List';
            
            $condition = array('status' => array('$ne' => 'Deleted'),"operator_id" => $this->session->userdata(APP_NAME.'_session_operator_id'));
            
            $user_type = $this->input->get('user_type');
            if($user_type == 'deleted'){
                $condition = array('status' => 'Deleted');
            }
            
            $sortArr  = array('created' => 'DESC');

            $sortby = '';
            
            if (isset($_GET['sortby'])) {
                $this->data['filter'] = 'filter';
                $sortby = $_GET['sortby'];
                if($sortby=="doj_asc"){
                    $sortArr  = array('created' => 'ASC');
                }
                if($sortby=="doj_desc"){
                    $sortArr  = array('created' => 'DESC');
                }
                if($sortby=="rides_asc"){
                    $sortArr  = array('no_of_rides' => 'ASC');
                }
                if($sortby=="rides_desc"){
                    $sortArr  = array('no_of_rides' => 'DESC');
                }
            }
            $this->data['sortby'] = $sortby;
            
            $filterArr = array();
            if ((isset($_GET['type']) && isset($_GET['value'])) && ($_GET['type'] != '' && $_GET['value'] != '')) {
                if (isset($_GET['type']) && $_GET['type'] != '') {
                    $this->data['type'] = $_GET['type'];
                }
                if (isset($_GET['value']) && $_GET['value'] != '') {
                    $this->data['value'] = $_GET['value'];
                    $filter_val = $this->data['value'];
                }
                $this->data['filter'] = 'filter';
                if($_GET['type'] == 'phone_number') {
                    #$filterArr = array("phone_number" => urldecode($filter_val),"country_code" => urldecode($_GET['country']));
                    $condition["phone_number"] = urldecode($filter_val);
                    $condition["country_code"] = "+".trim(urldecode($_GET['country']));
                } else {
                    $filterArr = array($this->data['type'] => urldecode($filter_val));
                }   
            }
            
            $usersCount = $this->app_model->get_all_counts(OTHER_OPERATOR, $condition, $filterArr);




            if ($usersCount > 1000) 
            {
                $searchPerPage = 500;
                $paginationNo = $this->uri->segment(4);
                if ($paginationNo == '') 
                {
                    $paginationNo = 0;
                } 
                else 
                {
                    $paginationNo = $paginationNo;
                }



                $user_data_Array =  $this->session->userdata(APP_NAME.'_session_operator_id');
               

                $this->data['usersList'] = $this->app_model->get_all_details(OTHER_OPERATOR,array('operator_id' =>  $user_data_Array));

                if($user_type == 'deleted')
                {
                
                    $searchbaseUrl = OPERATOR_NAME.'/users/display_user_list?user_type=deleted';
                    
                } 
                else 
                {
                    $searchbaseUrl = OPERATOR_NAME.'/users/display_user_list';
                }
                
                $config['num_links'] = 3;
                $config['display_pages'] = TRUE;
                $config['base_url'] = $searchbaseUrl;
                $config['total_rows'] = $usersCount;
                $config["per_page"] = $searchPerPage;
                $config["uri_segment"] = 4;
                $config['first_link'] = '';
                $config['last_link'] = '';
                $config['full_tag_open'] = '<ul class="tsc_pagination tsc_paginationA tsc_paginationA01">';
                $config['full_tag_close'] = '</ul>';
                if ($this->lang->line('pagination_prev_lbl') != '') $config['prev_link'] =stripslashes($this->lang->line('pagination_prev_lbl'));  else  $config['prev_link'] ='Prev';
                $config['prev_tag_open'] = '<li>';
                $config['prev_tag_close'] = '</li>';
                if ($this->lang->line('pagination_next_lbl') != '') $config['next_link'] =stripslashes($this->lang->line('pagination_next_lbl'));  else  $config['next_link'] ='Next';
                $config['next_tag_open'] = '<li>';
                $config['next_tag_close'] = '</li>';
                $config['cur_tag_open'] = '<li class="current"><a href="javascript:void(0);" style="cursor:default;">';
                $config['cur_tag_close'] = '</a></li>';
                $config['num_tag_open'] = '<li>';
                $config['num_tag_close'] = '</li>';
                $config['first_tag_open'] = '<li>';
                $config['first_tag_close'] = '</li>';
                $config['last_tag_open'] = '<li>';
                $config['last_tag_close'] = '</li>';
                if ($this->lang->line('pagination_first_lbl') != '') $config['first_link'] =stripslashes($this->lang->line('pagination_first_lbl'));  else  $config['first_link'] ='First';
                if ($this->lang->line('pagination_last_lbl') != '') $config['last_link'] = stripslashes($this->lang->line('pagination_last_lbl'));  else  $config['last_link'] ='Last';
                $this->pagination->initialize($config);
                $paginationLink = $this->pagination->create_links();
                $this->data['paginationLink'] = $paginationLink;

                $this->load->view(OPERATOR_NAME.'/users/display_userlist', $this->data);
            } 
            else 
            {
                              
                
               $user_data_Array =  $this->session->userdata(APP_NAME.'_session_operator_id');
               

                $this->data['usersList'] = $this->app_model->get_all_details(OTHER_OPERATOR,array('operator_id' =>  $user_data_Array));


                
             


                $this->data['paginationLink'] = '';
                $this->load->view(OPERATOR_NAME.'/users/display_userlist', $this->data);
            }
        }
    }




    public function edit_user_form()
    {

	   if ($this->checkLogin('O') == '') {
        redirect(OPERATOR_NAME);
        } else 
        {

    		$sub_user_id = $this->uri->segment(4, 0);

    		$this->data['user_id']=$sub_user_id ;

    		$this->data['heading']="Edit User";

    		 $user_data_Array =  $this->session->userdata(APP_NAME.'_session_operator_id');

    		$this->data['user_details']=$this->data['usersList'] = $this->app_model->get_all_details(OTHER_OPERATOR,array('operator_id' =>  $user_data_Array,'_id' => new MongoDB\BSON\ObjectId($sub_user_id)))->result();


    		if(empty($this->data['user_details']))
    		{
    			redirect(OPERATOR_NAME.'/other_users/display_user_list');
    		}


    		  $this->data['re_namer']= array('driver' => 'Driver' ,'trip' => "Trip",'bus_route' => 'Bus Route','add_bus' => 'Vehicle','student_management_details' => "Student Management",'notification' => "Notification","logs" => "Logs","dashboard" => "Driver Dashboard","edit_driver_form" => "Edit driver","add_driver_form" => "Add Driver","view_driver"=>"View Driver","assign_route_view" => "Add Trip","view_trip" => "View Trip","edit_trip_bus" => "Edit Trip","add_bus_route" => "Add Route","view_bus_route" => "View Route" ,"add_bus_new" =>"Add Vehicle","view_bus_details" => "View Vehicle","add_new_class" => "Add Class","add_studentdetails"=>"Add Student","view_student_details" => "View Student Details","edit_student_detail" => "Edit Student","send_users_notification" => "Send Users Notification","send_drivers_notification" => "Send Drivers Notification","trip_details_list" => "Trip History" ,"display_parent_logs" => "Parent Notification Log","display_driver_logs" => "Driver Notification Logs","notification_templates" => "Notification Template") ;

			$this->load->view(OPERATOR_NAME.'/users/edit_user',$this->data);

    	}
    }




   public function edit_user()
   {

   		 if ($this->checkLogin('O') == '') 
   		 {
            redirect(OPERATOR_NAME);
       	}
       	else 
       	{
					$operator_id =  $this->session->userdata(APP_NAME.'_session_operator_id');						
									
					$mobile_number = $this->input->post('mobile_number');
					
					$returnUrl = OPERATOR_NAME.'/other_users/display_user_list';
					
					$privileges=json_decode($this->input->post('set_pages'));

					$operatordata=$this->app_model->get_all_details(OPERATORS,array('_id' => new MongoDB\BSON\ObjectId($operator_id) ))->result();					
	
					$operator_location = $operatordata[0]->operator_location;

					$dail_code =$operatordata[0]->dail_code;

					$user_id=$this->input->post('user_id');



					
					$condition = array('dail_code' => $dail_code,'mobile_number' => $mobile_number,'_id' => array('$ne' => new MongoDB\BSON\ObjectId($user_id)));
					
					$duplicate_phone = $this->app_model->get_all_details(OTHER_OPERATOR,$condition);

					
							

					$condition = array('dail_code' => $dail_code,'mobile_number' => $mobile_number);
					
					$duplicate_phone = $this->app_model->get_all_details(OTHER_OPERATOR,$condition);
					if ($duplicate_phone->num_rows() > 0){
							$this->setErrorMessage('error','Mobile number already exists');
							redirect($returnUrl);
					}	


					$condition = array('dail_code' => $dail_code,'mobile_number' => $mobile_number);
					
					$duplicate_phone = $this->app_model->get_all_details(OPERATORS,$condition);
					if ($duplicate_phone->num_rows() > 0){
							$this->setErrorMessage('error','Mobile number already exists');
							redirect($returnUrl);
					}			

							


					$excludeArr = array("admin_password","set_pages","status", "operator_id", "mobile_number", "dail_code", 'email',"operator_location", "address", "country", "state", "city", "postal_code",'user_id');
						

					
					
				
					if ($this->input->post('status') != '')
					{
						$status = 'Active';
					} 
					else 
					{
						$status = 'Inactive';
					}	
				
					$operator_data = array(
											'operator_id' => $operator_id,	
											'status' => $status,
											'edited' => date('Y-m-d H:i:s'),
											
											'dail_code' => $dail_code,
											'mobile_number' => (string) $this->input->post('mobile_number'),
											'privileges' => $privileges
										);
					
				    $operator_data['operator_location'] = $operator_location;		
										
				    $dataArr = $operator_data;				 
					
					$this->app_model->commonInsertUpdate(OTHER_OPERATOR,'update',$excludeArr,$dataArr,array("_id" => new MongoDB\BSON\ObjectId($user_id)));
					$this->setErrorMessage('success','User edited successfully');
					
					$last_insert_id = $this->cimongo->insert_id();
					$this->mail_model->send_operator_register_confirmation_mail($last_insert_id,$password);				
					
					redirect(OPERATOR_NAME.'/other_users/display_user_list');
			}
   }





    public function view_user()
    {

	   if ($this->checkLogin('O') == '') {
        redirect(OPERATOR_NAME);
        } else 
        {

    		$sub_user_id = $this->uri->segment(4, 0);

    		$this->data['user_id']=$sub_user_id ;

    		$this->data['heading']="View User";

    		 $user_data_Array =  $this->session->userdata(APP_NAME.'_session_operator_id');

    		$this->data['user_details']=$this->data['usersList'] = $this->app_model->get_all_details(OTHER_OPERATOR,array('operator_id' =>  $user_data_Array,'_id' => new MongoDB\BSON\ObjectId($sub_user_id)))->result();


    		if(empty($this->data['user_details']))
    		{
    			redirect(OPERATOR_NAME.'/other_users/display_user_list');
    		}


    		$this->data['re_namer']= array('driver' => 'Driver' ,'trip' => "Trip",'bus_route' => 'Bus Route','add_bus' => 'Vehicle','school_management_details' => "School Management",'notification' => "Notification","dashboard" => "Driver Dashboard","edit_driver_form" => "Edit driver","add_driver_form" => "Add Driver","view_driver"=>"View Driver","assign_route_view" => "Add Trip","view_trip" => "View Trip","edit_trip_bus" => "Edit Trip","add_bus_route" => "Add Route","view_bus_route" => "View Route" ,"add_bus_new" =>"Add Vehicle","view_bus_details" => "View Vehicle","add_new_class" => "Add Class","add_studentdetails"=>"Add Student","view_student_details" => "View Student Details","edit_student_detail" => "Edit Student","send_users_notification" => "Send Users Notification","send_drivers_notification" => "Send Drivers Notification","notification_templates" => "Notification Template" ) ;

			$this->load->view(OPERATOR_NAME.'/users/view_user',$this->data);

    	}
    }

    ////////change password

    public function change_password_form() {
		 if ($this->checkLogin('O') == '') {
        redirect(OPERATOR_NAME);
        } 
		$subadmin_id = $this->uri->segment(4);

		 $this->data['heading'] = 'Change user Password';

		$condition = array('_id' => new MongoDB\BSON\ObjectId($subadmin_id));
		$this->data['user_details'] = $subadmin_details = $this->app_model->get_all_details(OTHER_OPERATOR, $condition);
		$this->load->view(OPERATOR_NAME.'/users/change_password', $this->data);
	}


	public function change_password() { 

		 if ($this->checkLogin('O') == '') {
        redirect(OPERATOR_NAME);
        } 
        else
        {	
		$password = $this->input->post('new_password');
		$subadmin_id = $this->input->post('subadmin_id');
		$dataArr = array('password' => md5($this->input->post('new_password')));
		$condition = array('_id' => new MongoDB\BSON\ObjectId($subadmin_id));
		$this->app_model->update_details(OTHER_OPERATOR, $dataArr, $condition);

		$subadmininfo = $this->app_model->get_all_details(OTHER_OPERATOR, $condition);
		$this->send_subadmin_pwd($password, $subadmininfo);

		$this->setErrorMessage('success', 'Subadmin password changed and sent to their email','admin_subadmin_password_changed');
		redirect(OPERATOR_NAME.'/other_users/display_user_list');

		}


	}


	public function send_subadmin_pwd($pwd = '', $subadmininfo) {
		$default_lang=$this->config->item('default_lang_code');
		$driver_name = $subadmininfo->row()->admin_name;
		$newsid = '22';
		$template_values = $this->user_model->get_email_template($newsid,$default_lang);
		$adminnewstemplateArr = array('email_title' => $this->config->item('email_title'), 
																'logo' => $this->config->item('logo_image'), 
																'footer_content' => $this->config->item('footer_content'), 
																'meta_title' => $this->config->item('meta_title'), 
																'site_contact_mail' => $this->config->item('site_contact_mail'));
			extract($adminnewstemplateArr);
			$message = '<!DOCTYPE HTML>
			<html>
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
			<meta name="viewport" content="width=device-width"/>
			<title>' . $template_values['subject'] . '</title>
			<body>';
			include($template_values['templateurl']);
			$message .= '</body>
			</html>';
		$sender_email = $this->config->item('site_contact_mail');
		$sender_name = $this->config->item('email_title');

		$email_values = array('mail_type' => 'html',
												'from_mail_id' => $sender_email,
												'mail_name' => $sender_name,
												'to_mail_id' => $subadmininfo->row()->email,
												'subject_message' => $template_values['subject'],
												'body_messages' => $message
											);
		$email_send_to_common = $this->subadmin_model->common_email_send($email_values);
	}


	public function delete_user(){
		 if ($this->checkLogin('O') == '') {
        redirect(OPERATOR_NAME);
        } else {
			$subadmin_id = $this->uri->segment(4,0);
			$condition = array('_id' => new MongoDB\BSON\ObjectId($subadmin_id));
			$this->subadmin_model->commonDelete(OTHER_OPERATOR,$condition);
			$this->setErrorMessage('success','User deleted successfully');
			redirect(OPERATOR_NAME.'/other_users/display_user_list');
		}
	}
	




}
