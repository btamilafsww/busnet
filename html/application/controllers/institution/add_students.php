<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *
 *   Rides Management in operator panel
 * 
 *   @package    CI
 *   @subpackage Controller
 *   @author Casperon
 *
 * */
class Add_students extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('cookie', 'date', 'form', 'ride_helper', 'distcalc_helper'));
        $this->load->library(array('encrypt', 'form_validation'));
        $this->load->model(array('map_model', 'app_model', 'brand_model', 'rides_model', 'driver_model', 'operators_model', 'bus_route_model', 'bus_details_model', 'student_details_model', 'class_details_model', 'bus_trip_model'));
        if ($this->checkLogin('O') != '') {
            $operator_id = $this->checkLogin('O');
            $chkOperator = $this->app_model->get_selected_fields(OPERATORS, array('_id' => new MongoDB\BSON\ObjectId($operator_id)), array('status'));
            $chkstatus = TRUE;
            $errMsg = '';
            if ($chkOperator->num_rows() == 1) {
                if ($chkOperator->row()->status == 'Inactive') {
                    $chkstatus = FALSE;
                    if ($this->lang->line('operator_inactive_message') != '')
                        $errMsg = stripslashes($this->lang->line('operator_inactive_message'));
                    else
                        $errMsg = 'Your account is temporarily deactivated, Please contact admin';
                }
            } else {
                $chkstatus = FALSE;
                if ($this->lang->line('account_not_found') != '')
                    $errMsg = stripslashes($this->lang->line('account_not_found'));
                else
                    $errMsg = 'Your account details not found';
            }
            if (!$chkstatus) {
                $newdata = array(
                    'last_logout_date' => date("Y-m-d H:i:s")
                );
                $collection = OPERATORS;

                $condition = array('_id' => $this->checkLogin('O'));
                $this->app_model->update_details($collection, $newdata, $condition);
                $operatordata = array(
                    APP_NAME . '_session_operator_id' => '',
                    APP_NAME . '_session_operator_name' => '',
                    APP_NAME . '_session_operator_email' => '',
                    APP_NAME . '_session_vendor_location' => ''
                );
                $this->session->unset_userdata($operatordata);
                $this->setErrorMessage('error', $errMsg);
                redirect(OPERATOR_NAME);
            }
        }
    }

    public function add_studentdetails() {
        if ($this->checkLogin('O') == '') {
            redirect(OPERATOR_NAME);
        } else {
            if ($this->lang->line('add_students_details_lang') != '')
                $heading = stripslashes($this->lang->line('add_students_details_lang'));
            else
                $heading = 'Add Students Details';

            $this->data['heading'] = $heading;

            $user_data_Array = new MongoDB\BSON\ObjectId($this->session->userdata(APP_NAME . '_session_operator_id'));


            $dial_codes = $this->bus_details_model->get_all_details(COUNTRY, array())->result();

            $html = "";

            foreach ($dial_codes as $key => $value) {
                if ($value->dial_code != "") {
                    if ($value->dial_code == $this->session->userdata(APP_NAME . '_session_dial_code')) {
                        $html = $html . "<option selected value='" . $value->dial_code . "'>" . $value->cca3 . " (" . $value->dial_code . ")</option>";
                    } else {
                        $html = $html . "<option  value='" . $value->dial_code . "'>" . $value->cca3 . " (" . $value->dial_code . ")</option>";
                    }
                }
            }


            $this->data['html'] = $html;
            $this->data['bus_no_list'] = $this->bus_details_model->get_all_details(BUS_DETAILS, array('operator_id' => $user_data_Array, 'status' => 'on'));
            $this->data['bus_route_list'] = $this->bus_route_model->get_all_details(BUS_ROUTE, array('operator_id' => $user_data_Array));
            $this->data['class_list'] = $this->class_details_model->get_all_details(CLASS_DETAILS, array('operator_id' => $user_data_Array));
            $this->data['section_list'] = $this->class_details_model->get_all_details(SECTION_DETAILS, array('operator_id' => $user_data_Array));
            $this->data['bus_trip_list'] = $this->bus_trip_model->get_all_details(BUS_TRIP, array('operator_id' => $user_data_Array));
            $this->load->view(OPERATOR_NAME . '/student_details/add_students_detail', $this->data);
        }
    }

    public function insertEdit_student_details() {
        if ($this->checkLogin('O') == '') {
            redirect(OPERATOR_NAME);
        } else {
            $excludeArr = array("absent_type", "absent_trip_id", "guardian_type", "address_state", "address_postalcode", "student_name", "student_last_name", "student_class_title", "student_section_title", "student_gender_title", "student_date_of_birth", "parent_name", "parent_last_name", "email", "mobile_number", "address", "student_trip_type_title", "student_trip_name_title", "bus_stop_pickup", "student_trip_name_title_drop", "bus_stop_drop", "row_id_hidden", "status", "country_code");

            $bus_trip_and_route_id_pickup = $this->input->post('student_trip_name_title');


            if ($bus_trip_and_route_id_pickup != "") {
                $trip_route_id_array_pickup = explode(",", $bus_trip_and_route_id_pickup);
                $bustrip_id_pickup = $trip_route_id_array_pickup[0];
                $busroute_id_pickup = $trip_route_id_array_pickup[1];

                $pickup_student_trip_id = new \MongoDB\BSON\ObjectId($bustrip_id_pickup);
                $pickup_student_busroute_id = new \MongoDB\BSON\ObjectId($busroute_id_pickup);
            } else {
                $pickup_student_trip_id = "";
                $pickup_student_busroute_id = "";
            }

            $bus_trip_and_route_id_drop = $this->input->post('student_trip_name_title_drop');
            if ($bus_trip_and_route_id_drop != "") {
                $trip_route_id_array_drop = explode(",", $bus_trip_and_route_id_drop);
                $bustrip_id_drop = $trip_route_id_array_drop[0];
                $busroute_id_drop = $trip_route_id_array_drop[1];

                $drop_student_trip_id = new \MongoDB\BSON\ObjectId($bustrip_id_drop);
                $drop_student_busroute_id = new \MongoDB\BSON\ObjectId($busroute_id_drop);
            } else {
                $drop_student_trip_id = "";
                $drop_student_busroute_id = "";
            }

            $bus_trip_and_stop_id_pickup = $this->input->post('bus_stop_pickup');
            if ($bus_trip_and_stop_id_pickup != "") {
                $trip_stop_id_array_pickup = explode(",", $bus_trip_and_stop_id_pickup);
                $bus_stop_id_pickup = $trip_stop_id_array_pickup[1];

                $pickup_student_stopid = new \MongoDB\BSON\ObjectId($bus_stop_id_pickup);
            } else {
                $pickup_student_stopid = "";
            }

            $bus_trip_and_stop_id_drop = $this->input->post('bus_stop_drop');
            if ($bus_trip_and_stop_id_drop != "") {
                $trip_stop_id_array_drop = explode(",", $bus_trip_and_stop_id_drop);
                $bus_stop_id_drop = $trip_stop_id_array_drop[1];

                $drop_student_stopid = new \MongoDB\BSON\ObjectId($bus_stop_id_drop);
            } else {
                $drop_student_stopid = "";
            }




            $guardian_type = array_values($this->input->post('guardian_type'));
            $parent_name = array_values($this->input->post('parent_name'));

            //$parent_last_name=array_values($this->input->post('parent_last_name'));

            $parent_email = array_values($this->input->post('email'));
            $parent_mobile_number = array_values($this->input->post('mobile_number'));
            //$parent_address= array_values($this->input->post('address'));
            //$address_state= array_values($this->input->post('address_state'));
            //$address_postal_code= array_values($this->input->post('address_postalcode'));



            $country_codes = array_values($this->input->post('country_code'));



            if ($this->input->post('status') != '') {
                $status = 'Active';
            } else {
                $status = 'Inactive';
            }




            $guadrians_details = array();


            for ($i = 0; $i < sizeof($parent_mobile_number); $i++) {

                array_push($guadrians_details, array('guardian_type' => $guardian_type[$i],
                    'parent_name' => $parent_name[$i],
                    "parent_email" => $parent_email[$i],
                    "phone_code" => $country_codes[$i],
                    "parent_mobile_number" => $parent_mobile_number[$i]
                ));
            }

            $class = "";
            if (null !== $this->input->post('student_class_title') && $this->input->post('student_class_title') != "") {
                $class = new \MongoDB\BSON\ObjectId($this->input->post('student_class_title'));
            }

            $section = "";
            if (null !== $this->input->post('student_section_title') && $this->input->post('student_section_title') != "") {
                $section = new \MongoDB\BSON\ObjectId($this->input->post('student_section_title'));
            }


            $condition = array();
            $dataArr = array(
                'operator_id' => new MongoDB\BSON\ObjectId($this->session->userdata(APP_NAME . '_session_operator_id')),
                'student_first_name' => $this->input->post('student_name'),
                'student_last_name' => $this->input->post('student_last_name'),
                'student_gender' => $this->input->post('student_gender_title'),
                'student_dob' => $this->input->post('student_date_of_birth'),
                'student_school_id' => $this->input->post('student_school_id'),
                'student_class' => $class,
                'student_section' => $section,
                'student_status' => $status,
                'parent_update_location' => (!empty($this->input->post('parent_update_location')) ? TRUE : FALSE),
                'student_trip_type' => $this->input->post('student_trip_type_title'),
                'student_pickup_trip_id' => $pickup_student_trip_id,
                'student_pickup_bus_route_id' => $pickup_student_busroute_id,
                'student_pickup_bus_stop_id' => $pickup_student_stopid,
                'student_drop_trip_id' => $drop_student_trip_id,
                'student_drop_bus_route_id' => $drop_student_busroute_id,
                'student_drop_bus_stop_id' => $drop_student_stopid,
                'guardian_details' => $guadrians_details,
                'student_att_status' => array('from_date' => new MongoDB\BSON\UTCDateTime(strtotime(date("Y-m-d"))), 'to_date' => new MongoDB\BSON\UTCDateTime(strtotime(date("Y-m-d"))), 'status' => "present", "absent_type" => "", "absent_trip_id" => "", "absent_half_availability" => ""),
                'created_by_id' => new MongoDB\BSON\ObjectId($this->session->userdata(APP_NAME . '_session_changes_done_by_id')),
                'created_by_name' => $this->session->userdata(APP_NAME . '_session_changes_done_by_name'),
                'created_at' => date("Y-m-d H:i:s")
            );

            if ($this->student_details_model->commonInsertUpdate(STUDENT_DETAILS, 'insert', $excludeArr, $dataArr, $condition)) {
                for ($i = 0; $i < sizeof($parent_mobile_number); $i++) {
                    $this->checkparentexist($parent_mobile_number[$i], $country_codes[$i], $parent_email[$i]);
                }
                $this->setErrorMessage('success', 'Student detail added successfully');
                $this->data['heading'] = 'Add New Student Detail';
                redirect(OPERATOR_NAME . '/add_students/student_lists', $this->data);
            } else {
                $this->setErrorMessage('failure', 'Student detail not added successfully');
                $this->data['heading'] = 'Add New Student Detail';
                redirect(OPERATOR_NAME . '/add_students/add_studentdetails', $this->data);
            }
        }
    }

    public function student_lists() {
        if ($this->checkLogin('O') == '') {
            redirect(OPERATOR_NAME);
        } else {
            /* if ($this->lang->line('student_list') != '') 
              $heading = stripslashes($this->lang->line('student_list'));
              else  $heading = 'Student List';
              $this->data['heading'] = $heading;
              $user_data_Array =  $this->session->userdata(APP_NAME.'_session_operator_id');

              $this->data['student_list_all'] = $this->student_details_model->get_all_details(STUDENT_DETAILS, array('operator_id' => $user_data_Array),array('created_at' => 'DESC'));
              $this->load->view(OPERATOR_NAME.'/student_details/student_details_list', $this->data); */



            $this->data['heading'] = "Student list";


            if ($this->checkLogin('O') == '') {
                $this->setErrorMessage('error', 'You must login first', 'admin_driver_login_first');
                redirect(OPERATOR_NAME);
            }
            if ($this->lang->line('student_list') != '')
                $heading = stripslashes($this->lang->line('student_list'));
            else
                $heading = 'Student List';
            $sortArr = array('created' => 'DESC');
            $sortby = '';
            if (isset($_GET['sortby'])) {
                $this->data['filter'] = 'filter';
                $sortby = $_GET['sortby'];
                if ($sortby == "doj_asc") {
                    $sortArr = array('created' => 'ASC');
                }
                if ($sortby == "doj_desc") {
                    $sortArr = array('created' => 'DESC');
                }
                if ($sortby == "rides_asc") {
                    $sortArr = array('no_of_rides' => 'ASC');
                }
                if ($sortby == "rides_desc") {
                    $sortArr = array('no_of_rides' => 'DESC');
                }
            }
            $sortArr = array('_id' => 'DESC');
            $this->data['sortby'] = $sortby;
            $condition = $filterArr = array();
            $filterCondition = array();
            if (isset($_GET['type']) && (isset($_GET['value']) || isset($_GET['vehicle_category'])) && $_GET['type'] != '' && ($_GET['value'] != '' || $_GET['vehicle_category'] != '')) {
                if (isset($_GET['type']) && $_GET['type'] != '') {
                    $this->data['type'] = $_GET['type'];
                }
                $filter_val = '';
                if (isset($_GET['value']) && $_GET['value'] != '') {
                    $this->data['value'] = $_GET['value'];
                    $filter_val = $this->data['value'];
                }
                $this->data['filter'] = 'filter';
                $filterCondition = array();
            }

            $this->cimongo->where(array('operator_id' => new MongoDB\BSON\ObjectId($this->checkLogin('O'))));

            $student_list_all = $this->user_model->get_all_counts(STUDENT_DETAILS, $filterCondition, $filterArr);
            if ($student_list_all > 1000) {
                $searchPerPage = 500;
                $paginationNo = $this->uri->segment(4);
                if ($paginationNo == '') {
                    $paginationNo = 0;
                } else {
                    $paginationNo = $paginationNo;
                }
                $this->cimongo->where(array('driver_location' => $this->session->userdata(APP_NAME . '_session_operator_location'), "operator_id" => new MongoDB\BSON\ObjectId($this->checkLogin('O'))));
                $this->data['student_list_all'] = $this->user_model->get_all_details(STUDENT_DETAILS, $filterCondition, $sortArr, $searchPerPage, $paginationNo, $filterArr);

                $searchbaseUrl = OPERATOR_NAME . '/add_students/student_lists/';
                $config['num_links'] = 3;
                $config['display_pages'] = TRUE;
                $config['base_url'] = $searchbaseUrl;
                $config['total_rows'] = $student_list_all;
                $config["per_page"] = $searchPerPage;
                $config["uri_segment"] = 4;
                $config['first_link'] = '';
                $config['last_link'] = '';
                $config['full_tag_open'] = '<ul class="tsc_pagination tsc_paginationA tsc_paginationA01">';
                $config['full_tag_close'] = '</ul>';
                if ($this->lang->line('pagination_prev_lbl') != '')
                    $config['prev_link'] = stripslashes($this->lang->line('pagination_prev_lbl'));
                else
                    $config['prev_link'] = 'Prev';
                $config['prev_tag_open'] = '<li>';
                $config['prev_tag_close'] = '</li>';
                if ($this->lang->line('pagination_next_lbl') != '')
                    $config['next_link'] = stripslashes($this->lang->line('pagination_next_lbl'));
                else
                    $config['next_link'] = 'Next';
                $config['next_tag_open'] = '<li>';
                $config['next_tag_close'] = '</li>';
                $config['cur_tag_open'] = '<li class="current"><a href="javascript:void(0);" style="cursor:default;">';
                $config['cur_tag_close'] = '</a></li>';
                $config['num_tag_open'] = '<li>';
                $config['num_tag_close'] = '</li>';
                $config['first_tag_open'] = '<li>';
                $config['first_tag_close'] = '</li>';
                $config['last_tag_open'] = '<li>';
                $config['last_tag_close'] = '</li>';

                if ($this->lang->line('pagination_first_lbl') != '')
                    $config['first_link'] = stripslashes($this->lang->line('pagination_first_lbl'));
                else
                    $config['first_link'] = 'First';
                if ($this->lang->line('pagination_last_lbl') != '')
                    $config['last_link'] = stripslashes($this->lang->line('pagination_last_lbl'));
                else
                    $config['last_link'] = 'Last';
                $this->pagination->initialize($config);
                $paginationLink = $this->pagination->create_links();
                $this->data['paginationLink'] = $paginationLink;
            } else {
                $this->data['paginationLink'] = '';
                $condition = array();
                $this->cimongo->where(array("operator_id" => new MongoDB\BSON\ObjectId($this->checkLogin('O'))));
                $this->data['student_list_all'] = $this->driver_model->get_all_details(STUDENT_DETAILS, $filterCondition, $sortArr, '', '', $filterArr);
            }



            $this->data['page_type'] = "student_lists";



            $this->load->view(OPERATOR_NAME . '/student_details/student_details_list', $this->data);
        }
    }

    public function edit_student_detail() {
        if ($this->checkLogin('O') == '') {
            //  $this->setErrorMessage('error', 'You must login first','admin_driver_login_first');
            redirect(OPERATOR_NAME);
        }

        $student_id = $this->uri->segment(4);

        if ($this->lang->line('dash_edit_student') != '')
            $this->data['heading'] = stripslashes($this->lang->line('dash_edit_student'));
        else
            $this->data['heading'] = 'Edit Student Setails';


        $user_data_Array = new MongoDB\BSON\ObjectId($this->session->userdata(APP_NAME . '_session_operator_id'));





        $dial_codes = $this->bus_details_model->get_all_details(COUNTRY, array())->result();


        $this->data['dial_codes'] = $dial_codes;

        $html = "";

        foreach ($dial_codes as $key => $value) {
            if ($value->dial_code != "") {
                if ($value->dial_code == $this->session->userdata(APP_NAME . '_session_dial_code')) {
                    $html = $html . "<option selected value='" . $value->dial_code . "'>" . $value->cca3 . " (" . $value->dial_code . ")</option>";
                } else {
                    $html = $html . "<option  value='" . $value->dial_code . "'>" . $value->cca3 . " (" . $value->dial_code . ")</option>";
                }
            }
        }





        $this->data['html'] = $html;










        $condition = array('_id' => new \MongoDB\BSON\ObjectId($student_id));
        $this->data['student_details'] = $student_details = $this->student_details_model->get_all_details(STUDENT_DETAILS, $condition);

        $this->data['bus_no_list'] = $this->bus_details_model->get_all_details(BUS_DETAILS, array('operator_id' => $user_data_Array, 'status' => 'on'));
        $this->data['bus_route_list'] = $this->bus_route_model->get_all_details(BUS_ROUTE, array('operator_id' => $user_data_Array));
        $this->data['class_list'] = $this->class_details_model->get_all_details(CLASS_DETAILS, array('operator_id' => $user_data_Array));
        $this->data['section_list'] = $this->class_details_model->get_all_details(SECTION_DETAILS, array('operator_id' => $user_data_Array));
        $this->data['bus_trip_list'] = $this->bus_trip_model->get_all_details(BUS_TRIP, array('operator_id' => $user_data_Array));




        if ($student_details->num_rows() == 0) {
            //     $this->setErrorMessage('error', 'No record found for this driver','admin_driver_no_record_found');
            redirect(OPERATOR_NAME . '/student_details/student_details_list');
        }

        $this->load->view(OPERATOR_NAME . '/student_details/edit_student_detail', $this->data);
    }

    public function update_student_details() {
        if ($this->checkLogin('O') == '') {
            redirect(OPERATOR_NAME);
        } else {
            $excludeArr = array("student_id", "absent_type", "absent_trip_id", "guardian_type", "address_state", "address_postalcode", "student_name", "student_last_name", "student_class_title", "student_section_title", "student_gender_title", "student_date_of_birth", "parent_name", "parent_last_name", "email", "mobile_number", "address", "student_trip_type_title", "student_trip_name_title", "bus_stop_pickup", "student_trip_name_title_drop", "bus_stop_drop", "row_id_hidden", 'country_code');

            $bus_trip_and_route_id_pickup = $this->input->post('student_trip_name_title');
            if ($bus_trip_and_route_id_pickup != "") {
                $trip_route_id_array_pickup = explode(",", $bus_trip_and_route_id_pickup);
                $bustrip_id_pickup = $trip_route_id_array_pickup[0];
                $busroute_id_pickup = $trip_route_id_array_pickup[1];

                $pickup_student_trip_id = new \MongoDB\BSON\ObjectId($bustrip_id_pickup);
                $pickup_student_busroute_id = new \MongoDB\BSON\ObjectId($busroute_id_pickup);
            } else {
                $pickup_student_trip_id = "";
                $pickup_student_busroute_id = "";
            }

            $bus_trip_and_route_id_drop = $this->input->post('student_trip_name_title_drop');
            if ($bus_trip_and_route_id_drop != "") {
                $trip_route_id_array_drop = explode(",", $bus_trip_and_route_id_drop);
                $bustrip_id_drop = $trip_route_id_array_drop[0];
                $busroute_id_drop = $trip_route_id_array_drop[1];

                $drop_student_trip_id = new \MongoDB\BSON\ObjectId($bustrip_id_drop);
                $drop_student_busroute_id = new \MongoDB\BSON\ObjectId($busroute_id_drop);
            } else {
                $drop_student_trip_id = "";
                $drop_student_busroute_id = "";
            }

            $bus_trip_and_stop_id_pickup = $this->input->post('bus_stop_pickup');
            if ($bus_trip_and_stop_id_pickup != "") {
                $trip_stop_id_array_pickup = explode(",", $bus_trip_and_stop_id_pickup);
                $bus_stop_id_pickup = $trip_stop_id_array_pickup[1];

                $pickup_student_stopid = new \MongoDB\BSON\ObjectId($bus_stop_id_pickup);
            } else {
                $pickup_student_stopid = "";
            }

            $bus_trip_and_stop_id_drop = $this->input->post('bus_stop_drop');
            if ($bus_trip_and_stop_id_drop != "") {
                $trip_stop_id_array_drop = explode(",", $bus_trip_and_stop_id_drop);
                $bus_stop_id_drop = $trip_stop_id_array_drop[1];

                $drop_student_stopid = new \MongoDB\BSON\ObjectId($bus_stop_id_drop);
            } else {
                $drop_student_stopid = "";
            }






            $guardian_type = array_values($this->input->post('guardian_type'));
            $parent_name = array_values($this->input->post('parent_name'));

            //$parent_last_name=array_values($this->input->post('parent_last_name'));

            $parent_email = array_values($this->input->post('email'));
            $parent_mobile_number = array_values($this->input->post('mobile_number'));
            //$parent_address= array_values($this->input->post('address'));
            //$address_state= array_values($this->input->post('address_state'));
            //$address_postal_code= array_values($this->input->post('address_postalcode'));
            $country_codes = array_values($this->input->post('country_code'));

            if ($this->input->post('status') != '') {
                $status = 'Active';
            } else {
                $status = 'Inactive';
            }




            $guadrians_details = array();


            for ($i = 0; $i < sizeof($parent_name); $i++) {

                array_push($guadrians_details, array('guardian_type' => $guardian_type[$i],
                    'parent_name' => $parent_name[$i],
                    "parent_email" => $parent_email[$i],
                    "phone_code" => $country_codes[$i],
                    "parent_mobile_number" => $parent_mobile_number[$i]
                ));
            }


            $class = "";
            if (null !== $this->input->post('student_class_title') && $this->input->post('student_class_title') != "") {
                $class = new \MongoDB\BSON\ObjectId($this->input->post('student_class_title'));
            }

            $section = "";
            if (null !== $this->input->post('student_section_title') && $this->input->post('student_section_title') != "") {
                $section = new \MongoDB\BSON\ObjectId($this->input->post('student_section_title'));
            }


            $condition = array('_id' => new \MongoDB\BSON\ObjectId($this->input->post('row_id_hidden')));
            $student_details = $this->student_details_model->get_all_details(STUDENT_DETAILS, $condition);
            $student_details = $student_details->row();
            $old_guardian_details = $student_details->guardian_details;
            for ($z = 0; $z < count($old_guardian_details); $z++) {
                if (is_object($old_guardian_details[$z]))
                    $old_guardian_details[$z] = (array) $old_guardian_details[$z];
                $condition = array('guardian_details.parent_mobile_number' => $old_guardian_details[$z]['parent_mobile_number']);
                $other_student_details = $this->student_details_model->get_all_details(STUDENT_DETAILS, $condition);
                if ($other_student_details->num_rows() == 1) {
                    $this->user_model->update_details(USERS, array('status' => ''), array('phone_number' => $old_guardian_details[$z]['parent_mobile_number']));
                }
            }
            $condition = array('_id' => new \MongoDB\BSON\ObjectId($this->input->post('row_id_hidden')));
            $dataArr = array(
                'operator_id' => new MongoDB\BSON\ObjectId($this->session->userdata(APP_NAME . '_session_operator_id')),
                'student_first_name' => $this->input->post('student_name'),
                'student_last_name' => $this->input->post('student_last_name'),
                'student_gender' => $this->input->post('student_gender_title'),
                'student_dob' => $this->input->post('student_date_of_birth'),
                'student_school_id' => $this->input->post('student_school_id'),
                'student_class' => $class,
                'student_section' => $section,
                'student_status' => $status,
                'parent_update_location' => (!empty($this->input->post('parent_update_location')) ? TRUE : FALSE),
                'student_trip_type' => $this->input->post('student_trip_type_title'),
                'student_pickup_trip_id' => $pickup_student_trip_id,
                'student_pickup_bus_route_id' => $pickup_student_busroute_id,
                'student_pickup_bus_stop_id' => $pickup_student_stopid,
                'student_drop_trip_id' => $drop_student_trip_id,
                'student_drop_bus_route_id' => $drop_student_busroute_id,
                'student_drop_bus_stop_id' => $drop_student_stopid,
                'guardian_details' => $guadrians_details,
                'updated_by_id' => $this->session->userdata(APP_NAME . '_session_changes_done_by_id'),
                'updated_by_name' => $this->session->userdata(APP_NAME . '_session_changes_done_by_name'),
                'updated_at' => date("Y-m-d H:i:s")
            );

            if ($this->student_details_model->commonInsertUpdate(STUDENT_DETAILS, 'update', $excludeArr, $dataArr, $condition)) {
                for ($i = 0; $i < sizeof($parent_mobile_number); $i++) {
                    $this->checkparentexist($parent_mobile_number[$i], $country_codes[$i], $parent_email[$i]);
                }
                $this->setErrorMessage('success', 'Student detail updated successfully');
                redirect(OPERATOR_NAME . '/add_students/student_lists', $this->data);
            } else {
                $this->setErrorMessage('failure', 'Student detail not updated successfully');
                $this->data['heading'] = 'Edit Student Detail';
                redirect($_SERVER['HTTP_REFERER'], $this->data);
            }
        }
    }

    public function view_student_details() {

        if ($this->checkLogin('O') == '') {
            //    $this->setErrorMessage('error', 'You must login first','admin_driver_login_first');
            redirect(OPERATOR_NAME);
        }

        $studentId = $this->uri->segment(4);

        if ($this->lang->line('student_view_details') != '')
            $this->data['heading'] = stripslashes($this->lang->line('student_view_details'));
        else
            $this->data['heading'] = 'View Student Details';

        $condition = array('_id' => new \MongoDB\BSON\ObjectId($studentId));
        $this->data['student_details'] = $students_details = $this->student_details_model->get_all_details(STUDENT_DETAILS, $condition);

        if ($students_details->num_rows() == 0) {
            // $this->setErrorMessage('error', 'No records found','admin_driver_no_records_found');
            redirect(OPERATOR_NAME . '/add_students/student_lists');
        }

        $this->load->view(OPERATOR_NAME . '/student_details/view_student_details', $this->data);
    }

    public function delete_student_details() {

        if ($this->checkLogin('O') == '') {
            redirect(OPERATOR_NAME);
        } else {
            $operator_id = $this->checkLogin('O');

            $student_id = $this->uri->segment(4, 0);
            $condition = array('_id' => new \MongoDB\BSON\ObjectId($student_id));                               //  id
            $operator_id_condition = array('operator_id' => new \MongoDB\BSON\ObjectId($operator_id), '_id' => new \MongoDB\BSON\ObjectId($student_id));            //  operator id


            $check_student_assign = $this->student_details_model->get_all_counts(STUDENT_DETAILS, $operator_id_condition);


            $this->app_model->commonDelete(STUDENT_DETAILS, $operator_id_condition);
            $this->setErrorMessage('success', 'Student deleted successfully', 'institution_student_deleted_sucess');



            redirect(OPERATOR_NAME . '/add_students/student_lists');
        }
    }

    public function travelling_student_lists() {
        if ($this->checkLogin('O') == '') {
            redirect(OPERATOR_NAME);
        } else {



            $this->data['heading'] = "Travelling student list";


            $operator_id = new MongoDB\BSON\ObjectId($this->session->userdata(APP_NAME . '_session_operator_id'));

            $current_trip_details = $this->app_model->get_selected_fields(CURRENT_TRIP_DETAILS, array('operator_id' => new MongoDB\BSON\ObjectId($operator_id), "trip_action" => "start", "start_current_date" => array('$eq' => date('Y-m-d'))), array('trip_id'))->result();



            $present_id = array();

            $absent_id = array();

            if (!empty($current_trip_details)) {
                $or_data = array();
                foreach ($current_trip_details as $key => $value) {

                    $a = array('student_pickup_trip_id' => $value->trip_id);
                    $b = array('student_drop_trip_id' => $value->trip_id);
                    array_push($or_data, $a, $b);
                }




                $counter = $this->app_model->get_selected_fields(STUDENT_DETAILS, array('$or' => $or_data), array('_id', 'student_att_status', 'student_drop_trip_id'))->result();


                foreach ($counter as $key => $value) {
                    if (is_object($value->student_att_status))
                        $value->{"student_att_status"} = (array) $value->student_att_status;
                    if ($value->student_att_status['from_date']->toDateTime()->getTimestamp() <= strtotime(date("Y-m-d")) && $value->student_att_status['to_date']->toDateTime()->getTimestamp() >= strtotime(date("Y-m-d")) && $value->student_att_status['status'] == 'absent') {

                        if ($value->student_att_status['absent_half_availability'] == "pickup" && $value->student_att_status['absent_type'] == "half" && (string) $value->student_drop_trip_id != "") {
                            $abs_cheker = 0;
                            foreach ($current_trip_details as $key => $value2) {

                                if ($value2->trip_id == $value->student_drop_trip_id) {
                                    array_push($absent_id, $value->_id);
                                    $abs_cheker = 1;
                                }
                            }

                            if ($abs_cheker == 0) {
                                array_push($present_id, $value->_id);
                            }
                        } else {
                            array_push($absent_id, $value->_id);
                        }
                    } else {
                        array_push($present_id, $value->_id);
                    }
                }
            }





            if ($this->checkLogin('O') == '') {
                $this->setErrorMessage('error', 'You must login first', 'admin_driver_login_first');
                redirect(OPERATOR_NAME);
            }
            if ($this->lang->line('student_list') != '')
                $heading = stripslashes($this->lang->line('student_list'));
            else
                $heading = 'Student List';
            $sortArr = array('created' => 'DESC');
            $sortby = '';
            if (isset($_GET['sortby'])) {
                $this->data['filter'] = 'filter';
                $sortby = $_GET['sortby'];
                if ($sortby == "doj_asc") {
                    $sortArr = array('created' => 'ASC');
                }
                if ($sortby == "doj_desc") {
                    $sortArr = array('created' => 'DESC');
                }
                if ($sortby == "rides_asc") {
                    $sortArr = array('no_of_rides' => 'ASC');
                }
                if ($sortby == "rides_desc") {
                    $sortArr = array('no_of_rides' => 'DESC');
                }
            }
            $sortArr = array('_id' => 'DESC');
            $this->data['sortby'] = $sortby;
            $condition = $filterArr = array();
            /* $filterCondition = array();
              if (isset($_GET['type']) && (isset($_GET['value']) || isset($_GET['vehicle_category'])) && $_GET['type'] != '' && ($_GET['value'] != '' || $_GET['vehicle_category'] != '')) {
              if (isset($_GET['type']) && $_GET['type'] != '') {
              $this->data['type'] = $_GET['type'];
              }
              $filter_val = '';
              if (isset($_GET['value']) && $_GET['value'] != '') {
              $this->data['value'] = $_GET['value'];
              $filter_val = $this->data['value'];
              }
              $this->data['filter'] = 'filter';
              $filterCondition = array();

              } */

            $filterCondition = array('_id' => array('$in' => $present_id));

            $this->cimongo->where(array('operator_id' => new MongoDB\BSON\ObjectId($this->checkLogin('O'))));

            $student_list_all = $this->user_model->get_all_counts(STUDENT_DETAILS, $filterCondition, $filterArr);
            if ($student_list_all > 1000) {
                $searchPerPage = 500;
                $paginationNo = $this->uri->segment(4);
                if ($paginationNo == '') {
                    $paginationNo = 0;
                } else {
                    $paginationNo = $paginationNo;
                }
                $this->cimongo->where(array('driver_location' => $this->session->userdata(APP_NAME . '_session_operator_location'), "operator_id" => new MongoDB\BSON\ObjectId($this->checkLogin('O'))));
                $this->data['student_list_all'] = $this->user_model->get_all_details(STUDENT_DETAILS, $filterCondition, $sortArr, $searchPerPage, $paginationNo, $filterArr);

                $searchbaseUrl = OPERATOR_NAME . '/add_students/student_lists/';
                $config['num_links'] = 3;
                $config['display_pages'] = TRUE;
                $config['base_url'] = $searchbaseUrl;
                $config['total_rows'] = $student_list_all;
                $config["per_page"] = $searchPerPage;
                $config["uri_segment"] = 4;
                $config['first_link'] = '';
                $config['last_link'] = '';
                $config['full_tag_open'] = '<ul class="tsc_pagination tsc_paginationA tsc_paginationA01">';
                $config['full_tag_close'] = '</ul>';
                if ($this->lang->line('pagination_prev_lbl') != '')
                    $config['prev_link'] = stripslashes($this->lang->line('pagination_prev_lbl'));
                else
                    $config['prev_link'] = 'Prev';
                $config['prev_tag_open'] = '<li>';
                $config['prev_tag_close'] = '</li>';
                if ($this->lang->line('pagination_next_lbl') != '')
                    $config['next_link'] = stripslashes($this->lang->line('pagination_next_lbl'));
                else
                    $config['next_link'] = 'Next';
                $config['next_tag_open'] = '<li>';
                $config['next_tag_close'] = '</li>';
                $config['cur_tag_open'] = '<li class="current"><a href="javascript:void(0);" style="cursor:default;">';
                $config['cur_tag_close'] = '</a></li>';
                $config['num_tag_open'] = '<li>';
                $config['num_tag_close'] = '</li>';
                $config['first_tag_open'] = '<li>';
                $config['first_tag_close'] = '</li>';
                $config['last_tag_open'] = '<li>';
                $config['last_tag_close'] = '</li>';

                if ($this->lang->line('pagination_first_lbl') != '')
                    $config['first_link'] = stripslashes($this->lang->line('pagination_first_lbl'));
                else
                    $config['first_link'] = 'First';
                if ($this->lang->line('pagination_last_lbl') != '')
                    $config['last_link'] = stripslashes($this->lang->line('pagination_last_lbl'));
                else
                    $config['last_link'] = 'Last';
                $this->pagination->initialize($config);
                $paginationLink = $this->pagination->create_links();
                $this->data['paginationLink'] = $paginationLink;
            } else {
                $this->data['paginationLink'] = '';
                $condition = array();
                $this->cimongo->where(array("operator_id" => new MongoDB\BSON\ObjectId($this->checkLogin('O'))));
                $this->data['student_list_all'] = $this->driver_model->get_all_details(STUDENT_DETAILS, $filterCondition, $sortArr, '', '', $filterArr);
            }




            $this->data['page_type'] = "travelling_student_lists";


            $this->load->view(OPERATOR_NAME . '/student_details/student_details_list', $this->data);
        }
    }

    public function absent_student_lists() {
        if ($this->checkLogin('O') == '') {
            redirect(OPERATOR_NAME);
        } else {

            $this->data['heading'] = "Absent student list";

            $operator_id = new MongoDB\BSON\ObjectId($this->session->userdata(APP_NAME . '_session_operator_id'));

            $current_trip_details = $this->app_model->get_selected_fields(CURRENT_TRIP_DETAILS, array('operator_id' => new MongoDB\BSON\ObjectId($operator_id), "trip_action" => "start", "start_current_date" => array('$eq' => date('Y-m-d'))), array('trip_id'))->result();



            $present_id = array();

            $absent_id = array();

            if (!empty($current_trip_details)) {
                $or_data = array();
                foreach ($current_trip_details as $key => $value) {

                    $a = array('student_pickup_trip_id' => $value->trip_id);
                    $b = array('student_drop_trip_id' => $value->trip_id);
                    array_push($or_data, $a, $b);
                }




                $counter = $this->app_model->get_selected_fields(STUDENT_DETAILS, array('$or' => $or_data), array('_id', 'student_att_status', 'student_drop_trip_id'))->result();

                foreach ($counter as $key => $value) {
                    if (is_object($value->student_att_status))
                        $value->{"student_att_status"} = (array) $value->student_att_status;
                    if ($value->student_att_status['from_date']->toDateTime()->getTimestamp() <= strtotime(date("Y-m-d")) && $value->student_att_status['to_date']->toDateTime()->getTimestamp() >= strtotime(date("Y-m-d")) && $value->student_att_status['status'] == 'absent') {


                        if ($value->student_att_status['absent_half_availability'] == "pickup" && $value->student_att_status['absent_type'] == "half" && (string) $value->student_drop_trip_id != "") {
                            $abs_cheker = 0;
                            foreach ($current_trip_details as $key => $value2) {

                                if ($value2->trip_id == $value->student_drop_trip_id) {
                                    array_push($absent_id, $value->_id);
                                    $abs_cheker = 1;
                                }
                            }

                            if ($abs_cheker == 0) {
                                array_push($present_id, $value->_id);
                            }
                        } else {
                            array_push($absent_id, $value->_id);
                        }
                    } else {
                        array_push($present_id, $value->_id);
                    }
                }
            }





            if ($this->checkLogin('O') == '') {
                $this->setErrorMessage('error', 'You must login first', 'admin_driver_login_first');
                redirect(OPERATOR_NAME);
            }
            if ($this->lang->line('student_list') != '')
                $heading = stripslashes($this->lang->line('student_list'));
            else
                $heading = 'Student List';
            $sortArr = array('_id' => 'DESC');
            $sortby = '';
            if (isset($_GET['sortby'])) {
                $this->data['filter'] = 'filter';
                $sortby = $_GET['sortby'];
                if ($sortby == "doj_asc") {
                    $sortArr = array('created' => 'ASC');
                }
                if ($sortby == "doj_desc") {
                    $sortArr = array('created' => 'DESC');
                }
                if ($sortby == "rides_asc") {
                    $sortArr = array('no_of_rides' => 'ASC');
                }
                if ($sortby == "rides_desc") {
                    $sortArr = array('no_of_rides' => 'DESC');
                }
            }
            $sortArr = array('_id' => 'DESC');
            $this->data['sortby'] = $sortby;
            $condition = $filterArr = array();
            /* $filterCondition = array();
              if (isset($_GET['type']) && (isset($_GET['value']) || isset($_GET['vehicle_category'])) && $_GET['type'] != '' && ($_GET['value'] != '' || $_GET['vehicle_category'] != '')) {
              if (isset($_GET['type']) && $_GET['type'] != '') {
              $this->data['type'] = $_GET['type'];
              }
              $filter_val = '';
              if (isset($_GET['value']) && $_GET['value'] != '') {
              $this->data['value'] = $_GET['value'];
              $filter_val = $this->data['value'];
              }
              $this->data['filter'] = 'filter';
              $filterCondition = array();

              } */

            $filterCondition = array('_id' => array('$in' => $absent_id));

            $this->cimongo->where(array('operator_id' => new MongoDB\BSON\ObjectId($this->checkLogin('O'))));

            $student_list_all = $this->user_model->get_all_counts(STUDENT_DETAILS, $filterCondition, $filterArr);
            if ($student_list_all > 1000) {
                $searchPerPage = 500;
                $paginationNo = $this->uri->segment(4);
                if ($paginationNo == '') {
                    $paginationNo = 0;
                } else {
                    $paginationNo = $paginationNo;
                }
                $this->cimongo->where(array('driver_location' => $this->session->userdata(APP_NAME . '_session_operator_location'), "operator_id" => new MongoDB\BSON\ObjectId($this->checkLogin('O'))));
                $this->data['student_list_all'] = $this->user_model->get_all_details(STUDENT_DETAILS, $filterCondition, $sortArr, $searchPerPage, $paginationNo, $filterArr);

                $searchbaseUrl = OPERATOR_NAME . '/add_students/student_lists/';
                $config['num_links'] = 3;
                $config['display_pages'] = TRUE;
                $config['base_url'] = $searchbaseUrl;
                $config['total_rows'] = $student_list_all;
                $config["per_page"] = $searchPerPage;
                $config["uri_segment"] = 4;
                $config['first_link'] = '';
                $config['last_link'] = '';
                $config['full_tag_open'] = '<ul class="tsc_pagination tsc_paginationA tsc_paginationA01">';
                $config['full_tag_close'] = '</ul>';
                if ($this->lang->line('pagination_prev_lbl') != '')
                    $config['prev_link'] = stripslashes($this->lang->line('pagination_prev_lbl'));
                else
                    $config['prev_link'] = 'Prev';
                $config['prev_tag_open'] = '<li>';
                $config['prev_tag_close'] = '</li>';
                if ($this->lang->line('pagination_next_lbl') != '')
                    $config['next_link'] = stripslashes($this->lang->line('pagination_next_lbl'));
                else
                    $config['next_link'] = 'Next';
                $config['next_tag_open'] = '<li>';
                $config['next_tag_close'] = '</li>';
                $config['cur_tag_open'] = '<li class="current"><a href="javascript:void(0);" style="cursor:default;">';
                $config['cur_tag_close'] = '</a></li>';
                $config['num_tag_open'] = '<li>';
                $config['num_tag_close'] = '</li>';
                $config['first_tag_open'] = '<li>';
                $config['first_tag_close'] = '</li>';
                $config['last_tag_open'] = '<li>';
                $config['last_tag_close'] = '</li>';

                if ($this->lang->line('pagination_first_lbl') != '')
                    $config['first_link'] = stripslashes($this->lang->line('pagination_first_lbl'));
                else
                    $config['first_link'] = 'First';
                if ($this->lang->line('pagination_last_lbl') != '')
                    $config['last_link'] = stripslashes($this->lang->line('pagination_last_lbl'));
                else
                    $config['last_link'] = 'Last';
                $this->pagination->initialize($config);
                $paginationLink = $this->pagination->create_links();
                $this->data['paginationLink'] = $paginationLink;
            } else {
                $this->data['paginationLink'] = '';
                $condition = array();
                $this->cimongo->where(array("operator_id" => new MongoDB\BSON\ObjectId($this->checkLogin('O'))));
                $this->data['student_list_all'] = $this->driver_model->get_all_details(STUDENT_DETAILS, $filterCondition, $sortArr, '', '', $filterArr);
            }




            $this->data['page_type'] = "absent_student_lists";


            $this->load->view(OPERATOR_NAME . '/student_details/student_details_list', $this->data);
        }
    }

    public function import_students() {
         if ($this->checkLogin('O') == '') {
            redirect(OPERATOR_NAME);
        } else {
            $this->data['heading'] = "Import Students";
            $this->load->view(OPERATOR_NAME . '/student_details/import_students_detail', $this->data);
        }
    }

}

/* End of file trip.php */
/* Location: ./application/controllers/operator/trip.php */