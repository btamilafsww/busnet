<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 *
 *   operator panel dashboard
 * 
 *   @package    CI
 *   @subpackage Controller
 *   @author Katenterprise
 *
 * */
class Dashboard extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('cookie', 'date', 'form'));
        $this->load->library(array('encrypt', 'form_validation'));
        $this->load->model('dashboard_model');
        $this->load->model('app_model', 'map_model');
        $this->load->model('student_details_model');
        $this->load->model('user_model');

        if ($this->checkLogin('O') != '') {



            $operator_id = $this->checkLogin('O');


            $chkOperator = $this->app_model->get_selected_fields(OPERATORS, array('_id' => new MongoDB\BSON\ObjectId($operator_id)), array('status'));
            $chkstatus = TRUE;
            $errMsg = '';



            if ($chkOperator->num_rows() == 1) {
                if ($chkOperator->row()->status == 'Inactive') {
                    $chkstatus = FALSE;
                    if ($this->lang->line('operator_inactive_message') != '')
                        $errMsg = stripslashes($this->lang->line('operator_inactive_message'));
                    else
                        $errMsg = 'Your account is temporarily deactivated, Please contact admin';
                }
            } else {
                $chkstatus = FALSE;
                if ($this->lang->line('account_not_found') != '')
                    $errMsg = stripslashes($this->lang->line('account_not_found'));
                else
                    $errMsg = 'Your account details not found';
            }
            if (!$chkstatus) {
                $newdata = array(
                    'last_logout_date' => date("Y-m-d H:i:s")
                );
                $collection = OPERATORS;

                $condition = array('_id' => $this->checkLogin('O'));
                $this->app_model->update_details($collection, $newdata, $condition);
                $operatordata = array(
                    APP_NAME . '_session_operator_id' => '',
                    APP_NAME . '_session_operator_name' => '',
                    APP_NAME . '_session_operator_email' => '',
                    APP_NAME . '_session_vendor_location' => ''
                );
                $this->session->unset_userdata($operatordata);
                $this->setErrorMessage('error', $errMsg);
                redirect(OPERATOR_NAME);
            }
        }
    }

    /**
     * 
     * Displays the dashboard
     *
     * @return HTTP REDIRECT, dashboard page
     *
     * */
    public function index() {
        if ($this->checkLogin('O') == '') {
            redirect(OPERATOR_NAME);
        } else {
            redirect(OPERATOR_NAME . '/dashboard/display_dashboard');
        }
    }

    /**
     * 
     * Displays the Rides statics for the Driver and Riders under the operator
     *
     * @return HTTP REDIRECT, dashboard page
     *
     * */
    public function display_dashboard() {
        if ($this->checkLogin('O') == '') {
            redirect(OPERATOR_NAME);
        } else {

            $this->data['plying_vehilces'] = $this->dashboard_model->current_trip_details();



            $this->data['delayed_vehicles'] = $this->dashboard_model->delayed_trip_details();

            $this->data['parent_students_pres_abs'] = $this->dashboard_model->students_present();

            $this->data['driver_notify_count'] = $this->dashboard_model->driver_notifications();
            $this->data['bus_breakdown'] = $this->dashboard_model->bus_breakdown();
            $this->data['delayed_bus'] = $this->dashboard_model->delayed_bus();
            $this->data['parent_notifications'] = $this->dashboard_model->parent_notifications();


            $operator_id = $this->checkLogin('O');
            $condition = array();

            $this->cimongo->where(array('booked_by' => $operator_id));
            $totalRides = $this->dashboard_model->get_all_counts(RIDES, $condition);
            $this->cimongo->where(array('operator_id' => $operator_id));


            $totalDrivers = $this->dashboard_model->get_all_counts(DRIVERS, $operator_id);

            $condition = array('status' => 'Active');
            $this->cimongo->where(array('operator_id' => $operator_id));
            $activeDrivers = $this->dashboard_model->get_all_counts(DRIVERS, array('operator_id' => $operator_id, 'status' => "Active"));


            $condition = array('ride_status' => 'Completed');
            $this->cimongo->where(array('booked_by' => $operator_id));
            $completedRides = $this->dashboard_model->get_all_counts(RIDES, $condition);

            $condition = array('ride_status' => 'Booked');
            $this->cimongo->where(array('booked_by' => $operator_id));
            $upcommingRides = $this->dashboard_model->get_all_counts(RIDES, $condition);

            $onRides = $this->dashboard_model->get_on_rides('', $operator_id);

            $condition = array('ride_status' => 'Cancelled', 'cancelled.primary.by' => 'User');
            $this->cimongo->where(array('booked_by' => $operator_id));
            $riderDeniedRides = $this->dashboard_model->get_all_counts(RIDES, $condition);

            $condition = array('ride_status' => 'Cancelled', 'cancelled.primary.by' => 'Driver');
            $this->cimongo->where(array('booked_by' => $operator_id));
            $driverDeniedRides = $this->dashboard_model->get_all_counts(RIDES, $condition);



            $totalStudents = $this->student_details_model->get_all_counts(STUDENT_DETAILS, array('operator_id' => $operator_id));
            $activeStudents = $this->dashboard_model->get_all_counts(STUDENT_DETAILS, array('operator_id' => $operator_id, 'student_status' => "Active"));

            $student_details_take = $this->student_details_model->get_all_details(STUDENT_DETAILS, array('operator_id' => $operator_id));
            $student_details_output = $student_details_take->result();

            $parent_numbers_array = array();

            foreach ($student_details_output as $key => $value) {
                foreach ($value->guardian_details as $key => $parentss) {
                    if(is_object($parentss))
                        $parentss=(array)$parentss;
                    if (in_array($parentss['parent_mobile_number'], $parent_numbers_array)) {
                        
                    } else {
                        array_push($parent_numbers_array, $parentss['parent_mobile_number']);
                    }
                }
            }

            $condition_s = array('phone_number' => array('$in' => $parent_numbers_array));
            $total_Users = $this->user_model->get_all_counts(USERS, $condition_s);

            $user_Active_condition = array('status' => 'Active', 'phone_number' => array('$in' => $parent_numbers_array));
            $active_Users = $this->user_model->get_all_counts(USERS, $user_Active_condition);



            $todayRides = 0;
            $todayDrivers = 0;
            $monthRides = 0;

            $todayRidesArr = $this->dashboard_model->get_today_rides('', $operator_id);
            $monthRidesArr = $this->dashboard_model->get_this_month_rides('', $operator_id);
            $yearRidesArr = $this->dashboard_model->get_this_year_rides('', $operator_id);

            if (!empty($todayRidesArr['result'])) {
                $todayRides = count($todayRidesArr['result']);
            }
            if (!empty($monthRidesArr['result'])) {
                $monthRides = count($monthRidesArr['result']);
            }
            if (!empty($yearRidesArr['result'])) {
                $yearRides = count($yearRidesArr['result']);
            }



            $monthDrivers = 0;
            $yearRides = 0;
            $yearDrivers = 0;

            $todayDriversArr = $this->dashboard_model->get_today_drivers($operator_id);
            $monthDriversArr = $this->dashboard_model->get_this_month_drivers('', $operator_id);
            $yearDriversArr = $this->dashboard_model->get_this_year_drivers('', $operator_id);


            if (!empty($todayDriversArr['result'])) {
                $todayDrivers = count($todayDriversArr['result']);
            }
            if (!empty($monthDriversArr['result'])) {
                $monthDrivers = count($monthDriversArr['result']);
            }
            if (!empty($yearDriversArr['result'])) {
                $yearDrivers = count($yearDriversArr['result']);
            }


            $todayStudents = 0;
            $monthStudents = 0;
            $yearStudents = 0;

            $todayStudentsArr = $this->dashboard_model->get_today_students($operator_id);
            $monthStudentsArr = $this->dashboard_model->get_this_month_Students('', $operator_id);
            $yearStudentsArr = $this->dashboard_model->get_this_year_Students($operator_id);


            if (!empty($todayStudentsArr['result'])) {
                $todayStudents = count($todayStudentsArr['result']);
            }
            if (!empty($monthStudentsArr['result'])) {
                $monthStudents = count($monthStudentsArr['result']);
            }
            if (!empty($yearStudentsArr['result'])) {
                $yearStudents = count($yearStudentsArr['result']);
            }


            $todayUsers = 0;
            $monthUsers = 0;
            $yearUsers = 0;

            $todayUsersArr = $this->dashboard_model->get_today_users($parent_numbers_array);
            $monthUsersArr = $this->dashboard_model->get_this_month_Users($parent_numbers_array);
            $yearUsersArr = $this->dashboard_model->get_this_year_Users($parent_numbers_array);


            if (!empty($todayUsersArr['result'])) {
                $todayUsers = count($todayUsersArr['result']);
            }
            if (!empty($monthUsersArr['result'])) {
                $monthUsers = count($monthUsersArr['result']);
            }
            if (!empty($yearUsersArr['result'])) {
                $yearUsers = count($yearUsersArr['result']);
            }




            $this->data['totalRides'] = $totalRides;
            $this->data['totalDrivers'] = $totalDrivers;
            $this->data['activeDrivers'] = $activeDrivers;
            $this->data['completedRides'] = $completedRides;
            $this->data['upcommingRides'] = $upcommingRides;
            $this->data['onRides'] = $onRides;
            $this->data['riderDeniedRides'] = $riderDeniedRides;
            $this->data['driverDeniedRides'] = $driverDeniedRides;
            $this->data['todayRides'] = $todayRides;
            $this->data['todayDrivers'] = $todayDrivers;
            $this->data['monthRides'] = $monthRides;
            $this->data['monthDrivers'] = $monthDrivers;
            $this->data['yearRides'] = $yearRides;
            $this->data['yearDrivers'] = $yearDrivers;

            $this->data['totalStudents'] = $totalStudents;
            $this->data['activeStudents'] = $activeStudents;
            $this->data['todayStudents'] = $todayStudents;
            $this->data['monthStudents'] = $monthStudents;
            $this->data['yearStudents'] = $yearStudents;

            $this->data['total_Users'] = $total_Users;
            $this->data['active_Users'] = $active_Users;
            $this->data['todayUsers'] = $todayUsers;
            $this->data['monthUsers'] = $monthUsers;
            $this->data['yearUsers'] = $yearUsers;

            $this->data['online_drivers_data'] = $this->map_avail_drivers();


            if ($this->lang->line('admin_menu_dashboard') != '')
                $this->data['heading'] = stripslashes($this->lang->line('admin_menu_dashboard'));
            else
                $this->data['heading'] = 'Dashboard';
            $this->load->view(OPERATOR_NAME . '/settings/dashboard', $this->data);
        }
    }

    public function map_avail_drivers() {
        if ($this->checkLogin('O') == '') {
            redirect(OPERATOR_NAME);
        } else {
            $operator_id = (string) $this->checkLogin('O');
            $operator_loc = $this->session->userdata(APP_NAME . '_session_operator_location');







            $driverList = $this->app_model->get_all_details(DRIVERS, array('operator_id' => new MongoDB\BSON\ObjectId($operator_id), 'loc' => array('$ne' => null)));




            $avail = 0;
            $unavail = 0;
            
            if (!empty($driverList->result())) {
                foreach ($driverList->result() as $driver) {



                    $current = time() - 300;



                    if (!is_null($driver->last_active_time->toDateTime()->getTimestamp()) && $driver->last_active_time->toDateTime()->getTimestamp() > $current) {
                        $avail++;
                    } else {
                        $unavail++;
                    }
                }
            }

            $this->data['online_drivers'] = $avail;
            $this->data['offline_drivers'] = $unavail;





            $return_data = array($avail, $unavail);



            return $return_data;
        }
    }

}

/* End of file dashboard.php */
/* Location: ./application/controllers/operator/dashboard.php */