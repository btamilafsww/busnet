<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *
 *   Rides Management in operator panel
 * 
 *   @package    CI
 *   @subpackage Controller
 *   @author Katenterprise
 *
 * */
class Add_bus extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('cookie', 'date', 'form', 'ride_helper', 'distcalc_helper'));
        $this->load->library(array('encrypt', 'form_validation'));
        $this->load->model(array('app_model', 'brand_model', 'rides_model', 'vehicle_model', 'operators_model', 'bus_route_model', 'driver_model', 'bus_details_model', 'student_details_model', 'bus_trip_model'));
        if ($this->checkLogin('O') != '') {
            $operator_id = $this->checkLogin('O');
            $chkOperator = $this->app_model->get_selected_fields(OPERATORS, array('_id' => new MongoDB\BSON\ObjectId($operator_id)), array('status'));
            $chkstatus = TRUE;
            $errMsg = '';
            if ($chkOperator->num_rows() == 1) {
                if ($chkOperator->row()->status == 'Inactive') {
                    $chkstatus = FALSE;
                    if ($this->lang->line('operator_inactive_message') != '')
                        $errMsg = stripslashes($this->lang->line('operator_inactive_message'));
                    else
                        $errMsg = 'Your account is temporarily deactivated, Please contact admin';
                }
            } else {
                $chkstatus = FALSE;
                if ($this->lang->line('account_not_found') != '')
                    $errMsg = stripslashes($this->lang->line('account_not_found'));
                else
                    $errMsg = 'Your account details not found';
            }
            if (!$chkstatus) {
                $newdata = array(
                    'last_logout_date' => date("Y-m-d H:i:s")
                );
                $collection = OPERATORS;

                $condition = array('_id' => $this->checkLogin('O'));
                $this->app_model->update_details($collection, $newdata, $condition);
                $operatordata = array(
                    APP_NAME . '_session_operator_id' => '',
                    APP_NAME . '_session_operator_name' => '',
                    APP_NAME . '_session_operator_email' => '',
                    APP_NAME . '_session_vendor_location' => ''
                );
                $this->session->unset_userdata($operatordata);
                $this->setErrorMessage('error', $errMsg);
                redirect(OPERATOR_NAME);
            }
        }
    }

    public function add_bus_new() {




        if ($this->checkLogin('O') == '') {
            redirect(OPERATOR_NAME);
        } else {
            $model_id = $this->uri->segment(4, 0);

            //  $this->data['brandList'] = $this->brand_model->get_all_details(BRAND, array(), array('brand_name' => 'ASC'));
            $this->data['typeList'] = $this->brand_model->get_all_details(CATEGORY, array(), array('name' => 'ASC'));

            $form_mode = FALSE;
            if ($this->lang->line('add_new_vehicle_page') != '')
                $heading = stripslashes($this->lang->line('add_new_vehicle_page'));
            else
                $heading = 'Add new vehicle';

            $this->data['route_list'] = $this->bus_route_model->get_all_details(BUS_ROUTE, array("operator_id" => new MongoDB\BSON\ObjectId($this->session->userdata(APP_NAME . '_session_operator_id'))));
            $this->data['form_mode'] = $form_mode;
            $this->data['heading'] = $heading;




            $this->load->view(OPERATOR_NAME . '/bus_route/add_new_buss', $this->data);
        }
    }

    public function vechile_type_list() {



        if ($this->checkLogin('O') == '') {
            redirect(OPERATOR_NAME);
        } else {


            $this->data['vehicleList'] = $this->vehicle_model->get_all_details(MODELS, array('brand' => $this->input->get("brand_id")));


            $html = "<option value='' >Select Model</option>";


            foreach ($this->data['vehicleList']->result() as $key => $value) {

                $html = $html . "<option value='" . $value->_id . "' >" . $value->name . "</option>";
            }




            echo $html;
        }
    }

    public function insert_bus_new() {


        if ($this->checkLogin('O') == '') {
            redirect(OPERATOR_NAME);
        } else {





            if ($this->input->post('type') != "" && $this->input->post('vehicle_number') != "") {




                $vehicle_number_checker = $this->bus_details_model->get_selected_fields(BUS_DETAILS, array('vehicle_number' => $this->input->post('vehicle_number'), "operator_id" => new MongoDB\BSON\ObjectId($this->session->userdata(APP_NAME . '_session_operator_id'))), array('_id', 'vehicle_number'));

                if ($vehicle_number_checker->num_rows() >= 1) {


                    $this->setErrorMessage('error', 'This bus number already exists');
                    redirect(OPERATOR_NAME . '/add_bus/add_bus_new');
                } else {




                    $excludeArr = array("type", "vehicle_number", "status");
                    $condition = array();



                    $dataArr = array(
                        'operator_id' => new MongoDB\BSON\ObjectId($this->session->userdata(APP_NAME . '_session_operator_id')),
                        'type' => $this->input->post('type'),
                        'vehicle_number' => $this->input->post('vehicle_number'),
                        'vehicle_code' => $this->input->post('vehicle_code'),
                        'status' => $this->input->post('status'),
                        'created_by_id' => $this->session->userdata(APP_NAME . '_session_changes_done_by_id'),
                        'created_by_name' => (string)$this->session->userdata(APP_NAME . '_session_changes_done_by_name'),
                        'created_at' => date("Y-m-d H:i:s")
                    );




                    $this->bus_details_model->commonInsertUpdate(BUS_DETAILS, 'insert', $excludeArr, $dataArr, $condition);
                    $location_id = $this->cimongo->insert_id();


                    $this->setErrorMessage('success', 'Vehicle added successfully', 'bus_added_success');
                }
            } else {
                $this->setErrorMessage('error', "Some data missing");
                redirect(OPERATOR_NAME . '/add_bus/add_bus_new');
            }




            $this->data['heading'] = 'Add New Route';
            redirect(OPERATOR_NAME . '/add_bus/bus_list');
        }
    }

    public function bus_list() {


        if ($this->checkLogin('O') == '') {
            redirect(OPERATOR_NAME);
        } else {


            if ($this->lang->line('display_vehicle_list') != '')
                $this->data['heading'] = stripslashes($this->lang->line('display_vehicle_list'));
            else
                $this->data['heading'] = 'Vehicle List';
            $sortArr = array('_id' => 'DESC');
            $sortby = '';
            if (isset($_GET['sortby'])) {
                $this->data['filter'] = 'filter';
                $sortby = $_GET['sortby'];
                if ($sortby == "doj_asc") {
                    $sortArr = array('created' => 'ASC');
                }
                if ($sortby == "doj_desc") {
                    $sortArr = array('created' => 'DESC');
                }
                if ($sortby == "rides_asc") {
                    $sortArr = array('no_of_rides' => 'ASC');
                }
                if ($sortby == "rides_desc") {
                    $sortArr = array('no_of_rides' => 'DESC');
                }
            }
            $this->data['sortby'] = $sortby;
            $condition = $filterArr = array();
            $filterCondition = array();
            if (isset($_GET['type']) && (isset($_GET['value']) || isset($_GET['vehicle_category'])) && $_GET['type'] != '' && ($_GET['value'] != '' || $_GET['vehicle_category'] != '')) {
                if (isset($_GET['type']) && $_GET['type'] != '') {
                    $this->data['type'] = $_GET['type'];
                }
                $filter_val = '';
                if (isset($_GET['value']) && $_GET['value'] != '') {
                    $this->data['value'] = $_GET['value'];
                    $filter_val = $this->data['value'];
                }
                $this->data['filter'] = 'filter';
                $filterCondition = array();
                if ($_GET['type'] == 'vehicle_type') {
                    $vehicle_category = trim($_GET['vehicle_category']);
                    $categoryVal = $this->user_model->get_all_details(CATEGORY, '', '', '', '', array('name' => $vehicle_category));
                    $filterCondition = array('category' => $categoryVal->row()->_id);
                } else if ($_GET['type'] == 'driver_location') {
                    $location = $this->user_model->get_all_details(LOCATIONS, '', '', '', '', array('city' => $_GET['value']));
                    $filterArr = array($this->data['type'] => $location->row()->_id);
                } else if ($_GET['type'] == 'mobile_number') {
                    $filterCondition["mobile_number"] = $filter_val;
                    $filterCondition["dail_code"] = $_GET['country'];
                } else if ($_GET['type'] == 'aadhar_card_number' || $_GET['type'] == 'vehicle_number') {
                    $filterCondition = array($this->data['type'] => $_GET['value']);
                } else {
                    $filterArr = array($this->data['type'] => $filter_val, "operator_id" => new MongoDB\BSON\ObjectId($this->session->userdata(APP_NAME . '_session_operator_id')));
                }
            }

            // $this->cimongo->where(array('driver_location' => (string)$this->session->userdata(APP_NAME.'_session_operator_location'),'operator_id' => new MongoDB\BSON\ObjectId($this->session->userdata(APP_NAME . '_session_operator_id'))));

            $route_count = $this->bus_details_model->get_all_counts(BUS_DETAILS, $filterCondition, $filterArr);
            if ($route_count > 1000) {
                $searchPerPage = 500;
                $paginationNo = $this->uri->segment(4);
                if ($paginationNo == '') {
                    $paginationNo = 0;
                } else {
                    $paginationNo = $paginationNo;
                }
                //$this->cimongo->where(array('driver_location'=>(string)$this->session->userdata(APP_NAME.'_session_operator_location'),"operator_id"=> new MongoDB\BSON\ObjectId($this->session->userdata(APP_NAME . '_session_operator_id'))));
                //$this->data['driversList'] = $this->user_model->get_all_details(DRIVERS, $filterCondition, $sortArr, $searchPerPage, $paginationNo,$filterArr);

                $this->data['bus_list'] = $this->bus_details_model->get_all_details(BUS_DETAILS, $filterCondition, $sortArr, $searchPerPage, $paginationNo, $filterArr);







                $searchbaseUrl = OPERATOR_NAME . '/bus_route/list_route/';
                $config['num_links'] = 3;
                $config['display_pages'] = TRUE;
                $config['base_url'] = $searchbaseUrl;
                $config['total_rows'] = $route_count;
                $config["per_page"] = $searchPerPage;
                $config["uri_segment"] = 4;
                $config['first_link'] = '';
                $config['last_link'] = '';
                $config['full_tag_open'] = '<ul class="tsc_pagination tsc_paginationA tsc_paginationA01">';
                $config['full_tag_close'] = '</ul>';
                if ($this->lang->line('pagination_prev_lbl') != '')
                    $config['prev_link'] = stripslashes($this->lang->line('pagination_prev_lbl'));
                else
                    $config['prev_link'] = 'Prev';
                $config['prev_tag_open'] = '<li>';
                $config['prev_tag_close'] = '</li>';
                if ($this->lang->line('pagination_next_lbl') != '')
                    $config['next_link'] = stripslashes($this->lang->line('pagination_next_lbl'));
                else
                    $config['next_link'] = 'Next';
                $config['next_tag_open'] = '<li>';
                $config['next_tag_close'] = '</li>';
                $config['cur_tag_open'] = '<li class="current"><a href="javascript:void(0);" style="cursor:default;">';
                $config['cur_tag_close'] = '</a></li>';
                $config['num_tag_open'] = '<li>';
                $config['num_tag_close'] = '</li>';
                $config['first_tag_open'] = '<li>';
                $config['first_tag_close'] = '</li>';
                $config['last_tag_open'] = '<li>';
                $config['last_tag_close'] = '</li>';

                if ($this->lang->line('pagination_first_lbl') != '')
                    $config['first_link'] = stripslashes($this->lang->line('pagination_first_lbl'));
                else
                    $config['first_link'] = 'First';
                if ($this->lang->line('pagination_last_lbl') != '')
                    $config['last_link'] = stripslashes($this->lang->line('pagination_last_lbl'));
                else
                    $config['last_link'] = 'Last';
                $this->pagination->initialize($config);
                $paginationLink = $this->pagination->create_links();
                $this->data['paginationLink'] = $paginationLink;
            } else {
                $this->data['paginationLink'] = '';
                $condition = array();
                //$this->cimongo->where(array('driver_location'=>(string)$this->session->userdata(APP_NAME.'_session_operator_location'),"operator_id"=> new MongoDB\BSON\ObjectId($this->session->userdata(APP_NAME . '_session_operator_id'))));
                //$this->data['driversList'] = $this->driver_model->get_all_details(DRIVERS,  $filterCondition, $sortArr, '', '', $filterArr);

                $this->data['bus_list'] = $this->bus_details_model->get_all_details(BUS_DETAILS, array("operator_id" => new MongoDB\BSON\ObjectId($this->session->userdata(APP_NAME . '_session_operator_id'))), array('_id' => 'DESC'));
                //var_dump($this->data['route_list']->result() );
            }

            $cabCats = $this->driver_model->get_selected_fields(CATEGORY, array(), array('_id', 'name', 'name_languages'))->result();

            $cabsTypeArr = array();
            $langCode = $this->data['langCode'];
            foreach ($cabCats as $cab) {

                $category_name = $cab->name;
                if (isset($cab->name_languages[$langCode]) && $cab->name_languages[$langCode] != '')
                    $category_name = $cab->name_languages[$langCode];

                $cabId = (string) $cab->_id;
                $cabsTypeArr[$cabId] = $cab;
                $cabsTypeArr[$cabId]->name = $category_name;
            }
            $this->data['cabCats'] = $cabsTypeArr;




            $this->load->view(OPERATOR_NAME . '/bus_route/list_bus', $this->data);
        }
    }

    public function view_bus_details() {




        if ($this->checkLogin('O') == '') {
            redirect(OPERATOR_NAME);
        } else {

            $bus_id = $this->uri->segment(4, 0);

            $this->data["heading"] = "View Bus Detail";



            $this->data["bus_data"] = $this->bus_details_model->get_all_details(BUS_DETAILS, array("operator_id" => new MongoDB\BSON\ObjectId($this->session->userdata(APP_NAME . '_session_operator_id')), "_id" => new MongoDB\BSON\ObjectId($bus_id)))->result();



            $this->data['typeList'] = $this->brand_model->get_all_details(CATEGORY, array("_id" => new MongoDB\BSON\ObjectId($this->data["bus_data"][0]->type)))->result();




            $this->load->view(OPERATOR_NAME . '/bus_route/bus_detail_view', $this->data);
        }
    }

    public function edit_vehicle() {


        if ($this->checkLogin('O') == '') {
            redirect(OPERATOR_NAME);
        } else {
            $bus_id = $this->uri->segment(4, 0);

            $this->data['typeList'] = $this->brand_model->get_all_details(CATEGORY, array(), array('name' => 'ASC'));

            $form_mode = FALSE;
            if ($this->lang->line('add_new_vehicle_page') != '')
                $heading = stripslashes($this->lang->line('add_new_vehicle_page'));
            else
                $heading = 'Edit vehicle details';

            $this->data['vehicle_details'] = $this->bus_details_model->get_all_details(BUS_DETAILS, array("operator_id" => new MongoDB\BSON\ObjectId($this->session->userdata(APP_NAME . '_session_operator_id')), "_id" => new MongoDB\BSON\ObjectId($bus_id)))->result();

            $this->data['vehicle_id'] = $bus_id;

            $this->data['route_list'] = $this->bus_route_model->get_all_details(BUS_ROUTE, array("operator_id" => new MongoDB\BSON\ObjectId($this->session->userdata(APP_NAME . '_session_operator_id'))));
            $this->data['form_mode'] = $form_mode;
            $this->data['heading'] = $heading;




            $this->load->view(OPERATOR_NAME . '/bus_route/edit_new_buss', $this->data);
        }
    }

    public function update_bus_details() {









        if ($this->checkLogin('O') == '') {
            redirect(OPERATOR_NAME);
        } else {





            if ($this->input->post('type') != "" && $this->input->post('vehicle_number') != "") {


                $vehicle_id = $this->input->post('vehicle_id');

                $vehicle_number_checker = $this->bus_details_model->get_selected_fields(BUS_DETAILS, array('vehicle_number' => $this->input->post('vehicle_number'), "operator_id" => new MongoDB\BSON\ObjectId($this->session->userdata(APP_NAME . '_session_operator_id')), "_id" => array('$ne' => new MongoDB\BSON\ObjectId($this->input->post('vehicle_id')))), array('_id', 'vehicle_number'));

                if ($vehicle_number_checker->num_rows() >= 1) {


                    $this->setErrorMessage('error', 'This bus number already exists');
                    redirect(OPERATOR_NAME . '/add_bus/add_bus_new');
                } else {




                    $excludeArr = array("type", "vehicle_number", "status", "vehicle_code", "vehicle_id");
                    $condition = array("_id" => new MongoDB\BSON\ObjectId($vehicle_id));



                    $dataArr = array(
                        'operator_id' => new MongoDB\BSON\ObjectId($this->session->userdata(APP_NAME . '_session_operator_id')),
                        'type' => $this->input->post('type'),
                        'vehicle_number' => $this->input->post('vehicle_number'),
                        'vehicle_code' => $this->input->post('vehicle_code'),
                        'status' => $this->input->post('status'),
                        'updated_by_id' => $this->session->userdata(APP_NAME . '_session_changes_done_by_id'),
                        'updated_by_name' => $this->session->userdata(APP_NAME . '_session_changes_done_by_name'),
                        'updated_at' => date("Y-m-d H:i:s")
                    );




                    $this->bus_details_model->commonInsertUpdate(BUS_DETAILS, 'update', $excludeArr, $dataArr, $condition);
                    $location_id = $this->cimongo->insert_id();


                    $this->setErrorMessage('success', 'Vehicle updated successfully');
                }
            } else {
                $this->setErrorMessage('error', "Some data missing");
                redirect(OPERATOR_NAME . '/add_bus/edit_new_buss');
            }





            redirect(OPERATOR_NAME . '/add_bus/bus_list');
        }
    }

    public function delete_vehicle() {
        if ($this->checkLogin('O') == '') {
            redirect(OPERATOR_NAME);
        } else {
            $operator_id = $this->checkLogin('O');

            $vehicle_id = $this->uri->segment(4, 0);
            $condition = array('_id' => new MongoDB\BSON\ObjectId($vehicle_id));        //	id

            $route_id_condition = array('bus_id' => $vehicle_id);     //	route id		

            $check_route_assign = $this->bus_route_model->get_all_details(BUS_TRIP, $route_id_condition);



            if ($check_route_assign->num_rows() == 0) {
                $this->app_model->commonDelete(BUS_DETAILS, $condition);
                $this->setErrorMessage('success', 'Vehicle detail deleted successfully');
            } else {








                $this->setErrorMessage('error', 'Vehicle is assigned to a trip');
            }



            //		$this->setErrorMessage('success', 'Bus route deleted successfully','institution_bus_route_deleted_sucess');
            redirect(OPERATOR_NAME . '/add_bus/bus_list');
        }
    }

}
