<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *
 * 	Drivers
 * 
 * 	@package		CI
 * 	@subpackage		Controller
 * 	@author			Katenterprise
 *
 * */
class Logs extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('cookie', 'date', 'form'));
        $this->load->library(array('encrypt', 'form_validation', 'excel'));
        $this->load->model(array('driver_model', 'bus_details_model', 'sms_model', 'bus_route_model'));
        $this->load->model(array('app_model', 'rides_model', 'bus_trip_model'));

        if ($this->checkLogin('O') != '') {
            $operator_id = $this->checkLogin('O');
            $chkOperator = $this->app_model->get_selected_fields(OPERATORS, array('_id' => new MongoDB\BSON\ObjectId($operator_id)), array('status'));
            $chkstatus = TRUE;
            $errMsg = '';
            if ($chkOperator->num_rows() == 1) {
                if ($chkOperator->row()->status == 'Inactive') {
                    $chkstatus = FALSE;
                    if ($this->lang->line('operator_inactive_message') != '')
                        $errMsg = stripslashes($this->lang->line('operator_inactive_message'));
                    else
                        $errMsg = 'Your account is temporarily deactivated, Please contact admin';
                }
            } else {
                $chkstatus = FALSE;
                if ($this->lang->line('account_not_found') != '')
                    $errMsg = stripslashes($this->lang->line('account_not_found'));
                else
                    $errMsg = 'Your account details not found';
            }
            if (!$chkstatus) {
                $newdata = array(
                    'last_logout_date' => date("Y-m-d H:i:s")
                );
                $collection = OPERATORS;

                $condition = array('_id' => $this->checkLogin('O'));
                $this->app_model->update_details($collection, $newdata, $condition);
                $operatordata = array(
                    APP_NAME . '_session_operator_id' => '',
                    APP_NAME . '_session_operator_name' => '',
                    APP_NAME . '_session_operator_email' => '',
                    APP_NAME . '_session_vendor_location' => ''
                );
                $this->session->unset_userdata($operatordata);
                $this->setErrorMessage('error', $errMsg);
                redirect(OPERATOR_NAME);
            }
        }
    }

    /**
     *
     * Initiate to display the driver dashboard page
     * 	
     * @return http request to the driver dashboard page
     * 	
     * */
    public function index() {
        if ($this->checkLogin('O') == '') {
            redirect(OPERATOR_NAME);
        } else {
            redirect(OPERATOR_NAME . '/dashboard/display_dashboard');
        }
    }

    /**
     *
     * Display the driver dashboard page
     * 	
     * @return HTML to show driver dashboard page
     * 	
     * */
    public function display_parent_logs() {
        if ($this->checkLogin('O') == '') {
            redirect(OPERATOR_NAME);
        } else {

            if ($this->checkLogin('O') == '') {
                $this->setErrorMessage('error', 'You must login first', 'admin_driver_login_first');
                redirect(OPERATOR_NAME);
            }
            $this->data['heading'] = 'Parent notification log';
            $sortArr = array('_id' => 'DESC');
            $sortby = '';
            if (isset($_GET['sortby'])) {
                $this->data['filter'] = 'filter';
                $sortby = $_GET['sortby'];
                if ($sortby == "doj_asc") {
                    $sortArr = array('created' => 'ASC');
                }
                if ($sortby == "doj_desc") {
                    $sortArr = array('created' => 'DESC');
                }
            }
            $this->data['sortby'] = $sortby;
            $condition = $filterArr = array();
            $filterCondition = array('operator_id' => new MongoDB\BSON\ObjectId($this->checkLogin('O')));
            if ((isset($_GET['value'])) && (isset($_GET['type']) )) {


                if (isset($_GET['type']) && $_GET['type'] != '') {
                    $this->data['type'] = $_GET['type'];
                }

                $filter_val = '';
                if (isset($_GET['value']) && $_GET['value'] != '') {
                    $this->data['value'] = $_GET['value'];
                    $filter_val = $this->data['value'];
                }
                $this->data['filter'] = 'filter';
                // $filterCondition = array();

                $filterArr = array($this->data['type'] => $filter_val);
            }

            //$this->cimongo->where(array('operator_id' => new MongoDB\BSON\ObjectId($this->checkLogin('O'))));

            $parent_logs_count = $this->app_model->get_all_counts(PARENT_NOTIFY, $filterCondition, $filterArr);
            if ($parent_logs_count > 1000) {






                $searchPerPage = 1000;
                $paginationNo = $this->uri->segment(4);
                if ($paginationNo == '') {
                    $paginationNo = 0;
                } else {
                    $paginationNo = $paginationNo;
                }
                /* $this->cimongo->where(array('driver_location'=>(string)$this->session->userdata(APP_NAME.'_session_operator_location'),"operator_id"=> new MongoDB\BSON\ObjectId($this->checkLogin('O')))); */
                $this->data['parent_logs'] = $this->app_model->get_all_details(PARENT_NOTIFY, $filterCondition, $sortArr, $searchPerPage, $paginationNo, $filterArr);

                $searchbaseUrl = OPERATOR_NAME . '/logs/display_parent_logs/';
                $config['num_links'] = 3;
                $config['display_pages'] = TRUE;
                $config['base_url'] = $searchbaseUrl;
                $config['total_rows'] = $parent_logs_count;
                $config["per_page"] = $searchPerPage;
                $config["uri_segment"] = 4;
                $config['first_link'] = '';
                $config['last_link'] = '';
                $config['full_tag_open'] = '<ul class="tsc_pagination tsc_paginationA tsc_paginationA01">';
                $config['full_tag_close'] = '</ul>';
                if ($this->lang->line('pagination_prev_lbl') != '')
                    $config['prev_link'] = stripslashes($this->lang->line('pagination_prev_lbl'));
                else
                    $config['prev_link'] = 'Prev';
                $config['prev_tag_open'] = '<li>';
                $config['prev_tag_close'] = '</li>';
                if ($this->lang->line('pagination_next_lbl') != '')
                    $config['next_link'] = stripslashes($this->lang->line('pagination_next_lbl'));
                else
                    $config['next_link'] = 'Next';
                $config['next_tag_open'] = '<li>';
                $config['next_tag_close'] = '</li>';
                $config['cur_tag_open'] = '<li class="current"><a href="javascript:void(0);" style="cursor:default;">';
                $config['cur_tag_close'] = '</a></li>';
                $config['num_tag_open'] = '<li>';
                $config['num_tag_close'] = '</li>';
                $config['first_tag_open'] = '<li>';
                $config['first_tag_close'] = '</li>';
                $config['last_tag_open'] = '<li>';
                $config['last_tag_close'] = '</li>';

                if ($this->lang->line('pagination_first_lbl') != '')
                    $config['first_link'] = stripslashes($this->lang->line('pagination_first_lbl'));
                else
                    $config['first_link'] = 'First';
                if ($this->lang->line('pagination_last_lbl') != '')
                    $config['last_link'] = stripslashes($this->lang->line('pagination_last_lbl'));
                else
                    $config['last_link'] = 'Last';
                $this->pagination->initialize($config);
                $paginationLink = $this->pagination->create_links();
                $this->data['paginationLink'] = $paginationLink;
            } else {
                $this->data['paginationLink'] = '';
                $condition = array();

                $this->data['parent_logs'] = $this->app_model->get_all_details(PARENT_NOTIFY, $filterCondition, $sortArr, '', '', $filterArr);
            }




            $langCode = $this->data['langCode'];

            $this->data["todays_log"] = "display_parent_logs";

            $this->load->view(OPERATOR_NAME . '/logs/parents_log_list', $this->data);
        }
    }

    public function today_display_parent_logs() {
        if ($this->checkLogin('O') == '') {
            redirect(OPERATOR_NAME);
        } else {

            if ($this->checkLogin('O') == '') {
                $this->setErrorMessage('error', 'You must login first', 'admin_driver_login_first');
                redirect(OPERATOR_NAME);
            }
            $this->data['heading'] = 'Today\'s parent notifications';
            $sortArr = array('_id' => 'DESC');
            $sortby = '';
            if (isset($_GET['sortby'])) {
                $this->data['filter'] = 'filter';
                $sortby = $_GET['sortby'];
                if ($sortby == "doj_asc") {
                    $sortArr = array('created' => 'ASC');
                }
                if ($sortby == "doj_desc") {
                    $sortArr = array('created' => 'DESC');
                }
            }
            $this->data['sortby'] = $sortby;
            $condition = $filterArr = array();
            $filterCondition = array('operator_id' => new MongoDB\BSON\ObjectId($this->checkLogin('O')) ,"created_date" => date("Y-m-d"));
            if ((isset($_GET['value'])) && (isset($_GET['type']) )) {


                if (isset($_GET['type']) && $_GET['type'] != '') {
                    $this->data['type'] = $_GET['type'];
                }

                $filter_val = '';
                if (isset($_GET['value']) && $_GET['value'] != '') {
                    $this->data['value'] = $_GET['value'];
                    $filter_val = $this->data['value'];
                }
                $this->data['filter'] = 'filter';
                //$filterCondition = array();

                $filterArr = array($this->data['type'] => $filter_val);
            }

            //$this->cimongo->where(array('operator_id' => new MongoDB\BSON\ObjectId($this->checkLogin('O'))));
           
            $parent_logs_count = $this->app_model->get_all_counts(PARENT_NOTIFY, $filterCondition, $filterArr);
            if ($parent_logs_count > 1000) {






                $searchPerPage = 1000;
                $paginationNo = $this->uri->segment(4);
                if ($paginationNo == '') {
                    $paginationNo = 0;
                } else {
                    $paginationNo = $paginationNo;
                }
                /* $this->cimongo->where(array('driver_location'=>(string)$this->session->userdata(APP_NAME.'_session_operator_location'),"operator_id"=> new MongoDB\BSON\ObjectId($this->checkLogin('O')))); */
                $this->data['parent_logs'] = $this->app_model->get_all_details(PARENT_NOTIFY, $filterCondition, $sortArr, $searchPerPage, $paginationNo, $filterArr);

                $searchbaseUrl = OPERATOR_NAME . '/logs/display_parent_logs/';
                $config['num_links'] = 3;
                $config['display_pages'] = TRUE;
                $config['base_url'] = $searchbaseUrl;
                $config['total_rows'] = $parent_logs_count;
                $config["per_page"] = $searchPerPage;
                $config["uri_segment"] = 4;
                $config['first_link'] = '';
                $config['last_link'] = '';
                $config['full_tag_open'] = '<ul class="tsc_pagination tsc_paginationA tsc_paginationA01">';
                $config['full_tag_close'] = '</ul>';
                if ($this->lang->line('pagination_prev_lbl') != '')
                    $config['prev_link'] = stripslashes($this->lang->line('pagination_prev_lbl'));
                else
                    $config['prev_link'] = 'Prev';
                $config['prev_tag_open'] = '<li>';
                $config['prev_tag_close'] = '</li>';
                if ($this->lang->line('pagination_next_lbl') != '')
                    $config['next_link'] = stripslashes($this->lang->line('pagination_next_lbl'));
                else
                    $config['next_link'] = 'Next';
                $config['next_tag_open'] = '<li>';
                $config['next_tag_close'] = '</li>';
                $config['cur_tag_open'] = '<li class="current"><a href="javascript:void(0);" style="cursor:default;">';
                $config['cur_tag_close'] = '</a></li>';
                $config['num_tag_open'] = '<li>';
                $config['num_tag_close'] = '</li>';
                $config['first_tag_open'] = '<li>';
                $config['first_tag_close'] = '</li>';
                $config['last_tag_open'] = '<li>';
                $config['last_tag_close'] = '</li>';

                if ($this->lang->line('pagination_first_lbl') != '')
                    $config['first_link'] = stripslashes($this->lang->line('pagination_first_lbl'));
                else
                    $config['first_link'] = 'First';
                if ($this->lang->line('pagination_last_lbl') != '')
                    $config['last_link'] = stripslashes($this->lang->line('pagination_last_lbl'));
                else
                    $config['last_link'] = 'Last';
                $this->pagination->initialize($config);
                $paginationLink = $this->pagination->create_links();
                $this->data['paginationLink'] = $paginationLink;
            } else {
                $this->data['paginationLink'] = '';
                $condition = array();

                $this->data['parent_logs'] = $this->app_model->get_all_details(PARENT_NOTIFY, $filterCondition, $sortArr, '', '', $filterArr);
            }




            $langCode = $this->data['langCode'];

            $this->data["todays_log"] = "today_display_parent_logs";

            $this->load->view(OPERATOR_NAME . '/logs/parents_log_list', $this->data);
        }
    }

    public function display_driver_logs() {
        if ($this->checkLogin('O') == '') {
            redirect(OPERATOR_NAME);
        } else {

            if ($this->checkLogin('O') == '') {
                $this->setErrorMessage('error', 'You must login first', 'admin_driver_login_first');
                redirect(OPERATOR_NAME);
            }
            $this->data['heading'] = 'Driver notification log';
            $sortArr = array('_id' => 'DESC');
            $sortby = '';
            if (isset($_GET['sortby'])) {
                $this->data['filter'] = 'filter';
                $sortby = $_GET['sortby'];
                if ($sortby == "doj_asc") {
                    $sortArr = array('created' => 'ASC');
                }
                if ($sortby == "doj_desc") {
                    $sortArr = array('created' => 'DESC');
                }
            }
            $this->data['sortby'] = $sortby;
            $condition = $filterArr = array();
            $filterCondition = array('operator_id' => new MongoDB\BSON\ObjectId($this->checkLogin('O')));
            if ((isset($_GET['value'])) && (isset($_GET['type']) )) {


                if (isset($_GET['type']) && $_GET['type'] != '') {
                    $this->data['type'] = $_GET['type'];
                }

                $filter_val = '';
                if (isset($_GET['value']) && $_GET['value'] != '') {
                    $this->data['value'] = $_GET['value'];
                    $filter_val = $this->data['value'];
                }
                $this->data['filter'] = 'filter';
                //  $filterCondition = array();

                $filterArr = array($this->data['type'] => $filter_val);
            }

            //$this->cimongo->where(array('operator_id' => new MongoDB\BSON\ObjectId($this->checkLogin('O'))));

            $driver_logs_count = $this->app_model->get_all_counts(DRIVER_NOTIFY, $filterCondition, $filterArr);
            if ($driver_logs_count > 1000) {






                $searchPerPage = 1000;
                $paginationNo = $this->uri->segment(4);
                if ($paginationNo == '') {
                    $paginationNo = 0;
                } else {
                    $paginationNo = $paginationNo;
                }
                /* $this->cimongo->where(array('driver_location'=>(string)$this->session->userdata(APP_NAME.'_session_operator_location'),"operator_id"=> new MongoDB\BSON\ObjectId($this->checkLogin('O')))); */
                $this->data['driver_logs'] = $this->app_model->get_all_details(DRIVER_NOTIFY, $filterCondition, $sortArr, $searchPerPage, $paginationNo, $filterArr);

                $searchbaseUrl = OPERATOR_NAME . '/logs/display_driver_logs/';
                $config['num_links'] = 3;
                $config['display_pages'] = TRUE;
                $config['base_url'] = $searchbaseUrl;
                $config['total_rows'] = $parent_logs_count;
                $config["per_page"] = $searchPerPage;
                $config["uri_segment"] = 4;
                $config['first_link'] = '';
                $config['last_link'] = '';
                $config['full_tag_open'] = '<ul class="tsc_pagination tsc_paginationA tsc_paginationA01">';
                $config['full_tag_close'] = '</ul>';
                if ($this->lang->line('pagination_prev_lbl') != '')
                    $config['prev_link'] = stripslashes($this->lang->line('pagination_prev_lbl'));
                else
                    $config['prev_link'] = 'Prev';
                $config['prev_tag_open'] = '<li>';
                $config['prev_tag_close'] = '</li>';
                if ($this->lang->line('pagination_next_lbl') != '')
                    $config['next_link'] = stripslashes($this->lang->line('pagination_next_lbl'));
                else
                    $config['next_link'] = 'Next';
                $config['next_tag_open'] = '<li>';
                $config['next_tag_close'] = '</li>';
                $config['cur_tag_open'] = '<li class="current"><a href="javascript:void(0);" style="cursor:default;">';
                $config['cur_tag_close'] = '</a></li>';
                $config['num_tag_open'] = '<li>';
                $config['num_tag_close'] = '</li>';
                $config['first_tag_open'] = '<li>';
                $config['first_tag_close'] = '</li>';
                $config['last_tag_open'] = '<li>';
                $config['last_tag_close'] = '</li>';

                if ($this->lang->line('pagination_first_lbl') != '')
                    $config['first_link'] = stripslashes($this->lang->line('pagination_first_lbl'));
                else
                    $config['first_link'] = 'First';
                if ($this->lang->line('pagination_last_lbl') != '')
                    $config['last_link'] = stripslashes($this->lang->line('pagination_last_lbl'));
                else
                    $config['last_link'] = 'Last';
                $this->pagination->initialize($config);
                $paginationLink = $this->pagination->create_links();
                $this->data['paginationLink'] = $paginationLink;
            } else {
                $this->data['paginationLink'] = '';
                $condition = array();

                $this->data['driver_logs'] = $this->app_model->get_all_details(DRIVER_NOTIFY, $filterCondition, $sortArr, '', '', $filterArr);
            }

            $this->data["todays_log"] = "display_driver_logs";


            $langCode = $this->data['langCode'];



            $this->load->view(OPERATOR_NAME . '/logs/driver_log_list', $this->data);
        }
    }

    public function today_driver_notification_log() {
        if ($this->checkLogin('O') == '') {
            redirect(OPERATOR_NAME);
        } else {

            if ($this->checkLogin('O') == '') {
                $this->setErrorMessage('error', 'You must login first', 'admin_driver_login_first');
                redirect(OPERATOR_NAME);
            }
            $this->data['heading'] = 'Today\'s  Driver notifications';
            $sortArr = array('_id' => 'DESC');
            $sortby = '';
            if (isset($_GET['sortby'])) {
                $this->data['filter'] = 'filter';
                $sortby = $_GET['sortby'];
                if ($sortby == "doj_asc") {
                    $sortArr = array('created' => 'ASC');
                }
                if ($sortby == "doj_desc") {
                    $sortArr = array('created' => 'DESC');
                }
            }
            $this->data['sortby'] = $sortby;
            $condition = $filterArr = array();
            $filterCondition = array('operator_id' => new MongoDB\BSON\ObjectId($this->checkLogin('O'))/*, 'created_at' => date("Y-m-d")*/);
            if ((isset($_GET['value'])) && (isset($_GET['type']) )) {


                if (isset($_GET['type']) && $_GET['type'] != '') {
                    $this->data['type'] = $_GET['type'];
                }

                $filter_val = '';
                if (isset($_GET['value']) && $_GET['value'] != '') {
                    $this->data['value'] = $_GET['value'];
                    $filter_val = $this->data['value'];
                }
                $this->data['filter'] = 'filter';
                //$filterCondition = array();

                $filterArr = array($this->data['type'] => $filter_val);
            }

            //$this->cimongo->where(array('operator_id' => new MongoDB\BSON\ObjectId($this->checkLogin('O'))));

            $driver_logs_count = $this->app_model->get_all_counts(DRIVER_NOTIFY, $filterCondition, $filterArr);
            if ($driver_logs_count > 1000) {






                $searchPerPage = 1000;
                $paginationNo = $this->uri->segment(4);
                if ($paginationNo == '') {
                    $paginationNo = 0;
                } else {
                    $paginationNo = $paginationNo;
                }
                /* $this->cimongo->where(array('driver_location'=>(string)$this->session->userdata(APP_NAME.'_session_operator_location'),"operator_id"=> new MongoDB\BSON\ObjectId($this->checkLogin('O')))); */
                $this->data['driver_logs'] = $this->app_model->get_all_details(DRIVER_NOTIFY, $filterCondition, $sortArr, $searchPerPage, $paginationNo, $filterArr);

                $searchbaseUrl = OPERATOR_NAME . '/logs/driver_notification_log/';
                $config['num_links'] = 3;
                $config['display_pages'] = TRUE;
                $config['base_url'] = $searchbaseUrl;
                $config['total_rows'] = $parent_logs_count;
                $config["per_page"] = $searchPerPage;
                $config["uri_segment"] = 4;
                $config['first_link'] = '';
                $config['last_link'] = '';
                $config['full_tag_open'] = '<ul class="tsc_pagination tsc_paginationA tsc_paginationA01">';
                $config['full_tag_close'] = '</ul>';
                if ($this->lang->line('pagination_prev_lbl') != '')
                    $config['prev_link'] = stripslashes($this->lang->line('pagination_prev_lbl'));
                else
                    $config['prev_link'] = 'Prev';
                $config['prev_tag_open'] = '<li>';
                $config['prev_tag_close'] = '</li>';
                if ($this->lang->line('pagination_next_lbl') != '')
                    $config['next_link'] = stripslashes($this->lang->line('pagination_next_lbl'));
                else
                    $config['next_link'] = 'Next';
                $config['next_tag_open'] = '<li>';
                $config['next_tag_close'] = '</li>';
                $config['cur_tag_open'] = '<li class="current"><a href="javascript:void(0);" style="cursor:default;">';
                $config['cur_tag_close'] = '</a></li>';
                $config['num_tag_open'] = '<li>';
                $config['num_tag_close'] = '</li>';
                $config['first_tag_open'] = '<li>';
                $config['first_tag_close'] = '</li>';
                $config['last_tag_open'] = '<li>';
                $config['last_tag_close'] = '</li>';

                if ($this->lang->line('pagination_first_lbl') != '')
                    $config['first_link'] = stripslashes($this->lang->line('pagination_first_lbl'));
                else
                    $config['first_link'] = 'First';
                if ($this->lang->line('pagination_last_lbl') != '')
                    $config['last_link'] = stripslashes($this->lang->line('pagination_last_lbl'));
                else
                    $config['last_link'] = 'Last';
                $this->pagination->initialize($config);
                $paginationLink = $this->pagination->create_links();
                $this->data['paginationLink'] = $paginationLink;
            } else {
                $this->data['paginationLink'] = '';
                $condition = array();

                $this->data['driver_logs'] = $this->app_model->get_all_details(DRIVER_NOTIFY, $filterCondition, $sortArr, '', '', $filterArr);
            }


            $this->data["todays_log"] = "today_driver_notification_log";

            $langCode = $this->data['langCode'];



            $this->load->view(OPERATOR_NAME . '/logs/driver_log_list', $this->data);
        }
    }

    public function live_track() {

        $driver_id = $this->uri->segment(4);
        $redirect_page = $this->uri->segment(5);
        $driver_data = $this->user_model->get_all_details(DRIVERS, array('_id' => new MongoDB\BSON\ObjectId($driver_id)))->result();
        if (!empty($driver_data)) {
            $this->data['heading'] = 'Live track';

            $this->data['driver_location'] = $driver_data[0]->loc;
            $this->data['driver_id'] = $driver_id;

            $this->data['driver_name'] = $driver_data[0]->driver_name;

            $this->load->view(OPERATOR_NAME . '/logs/live_vechicle_track', $this->data);
        } else {
            $this->setErrorMessage('error', "Driver details not found");
            redirect(OPERATOR_NAME . '/dashboard/' . $redirect_page);
        }
    }

}

/* End of file drivers.php */
/* Location: ./application/controllers/operator/drivers.php */