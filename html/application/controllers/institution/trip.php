<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *
 *   Rides Management in operator panel
 * 
 *   @package    CI
 *   @subpackage Controller
 *   @author Katenterprise
 *
 * */
class Trip extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('cookie', 'date', 'form', 'ride_helper', 'distcalc_helper'));
        $this->load->library(array('encrypt', 'form_validation'));
        $this->load->model(array('sms_model', 'map_model', 'student_details_model', 'current_trip_model', 'app_model', 'operators_model', 'bus_details_model', 'driver_model', 'bus_route_model', 'bus_trip_model'));
        if ($this->checkLogin('O') != '') {
            $operator_id = $this->checkLogin('O');
            $chkOperator = $this->app_model->get_selected_fields(OPERATORS, array('_id' => new MongoDB\BSON\ObjectId($operator_id)), array('status'));
            $chkstatus = TRUE;
            $errMsg = '';
            if ($chkOperator->num_rows() == 1) {
                if ($chkOperator->row()->status == 'Inactive') {
                    $chkstatus = FALSE;
                    if ($this->lang->line('operator_inactive_message') != '')
                        $errMsg = stripslashes($this->lang->line('operator_inactive_message'));
                    else
                        $errMsg = 'Your account is temporarily deactivated, Please contact admin';
                }
            } else {
                $chkstatus = FALSE;
                if ($this->lang->line('account_not_found') != '')
                    $errMsg = stripslashes($this->lang->line('account_not_found'));
                else
                    $errMsg = 'Your account details not found';
            }
            if (!$chkstatus) {
                $newdata = array(
                    'last_logout_date' => date("Y-m-d H:i:s")
                );
                $collection = OPERATORS;

                $condition = array('_id' => $this->checkLogin('O'));
                $this->app_model->update_details($collection, $newdata, $condition);
                $operatordata = array(
                    APP_NAME . '_session_operator_id' => '',
                    APP_NAME . '_session_operator_name' => '',
                    APP_NAME . '_session_operator_email' => '',
                    APP_NAME . '_session_vendor_location' => ''
                );
                $this->session->unset_userdata($operatordata);
                $this->setErrorMessage('error', $errMsg);
                redirect(OPERATOR_NAME);
            }
        }
    }

    ////add trip view

    public function assign_route_view() {

        if ($this->checkLogin('O') == '') {
            redirect(OPERATOR_NAME);
        } else {


            $this->data["heading"] = "Assign Trip";


            $this->data['route_list'] = $this->bus_route_model->get_all_details(BUS_ROUTE, array("operator_id" => new MongoDB\BSON\ObjectId($this->checkLogin('O'))));


            $this->data['driver_list'] = $this->driver_model->get_all_details(DRIVERS, array("operator_id" => new MongoDB\BSON\ObjectId($this->checkLogin('O'))));

            $this->data['Bus_list'] = $this->bus_details_model->get_all_details(BUS_DETAILS, array("operator_id" => new MongoDB\BSON\ObjectId($this->checkLogin('O'))));

            $this->data['center'] = $this->config->item('latitude') . ',' . $this->config->item('longitude');


            $this->load->view(OPERATOR_NAME . '/trip/assign_route', $this->data);
        }
    }

    //////////insert a trip

    public function insert_bus_trip() {

        if ($this->checkLogin('O') == '') {
            redirect(OPERATOR_NAME);
        } else {


            $trip_data_all = $this->driver_model->get_all_details(BUS_TRIP, array("operator_id" => new MongoDB\BSON\ObjectId($this->checkLogin('O')), 'driver_id' => $this->input->post('driver_id')));

            $trip_all_data = $trip_data_all->result();

            $post_data_driver_id = $this->input->post('driver_id');
            $post_data_captain_id = $this->input->post('captain_id');
            $post_data_start_time = strtotime($this->input->post('start_time'));
            $post_data_end_time = strtotime($this->input->post('end_time'));


            ////////changing format

            $get_date_start = explode('/', $this->input->post('start_date'));

            $get_date_start = $get_date_start[1] . "/" . $get_date_start[0] . "/" . $get_date_start[2];

            $get_date_stop = explode('/', $this->input->post('end_date'));

            $get_date_stop = $get_date_stop[1] . "/" . $get_date_stop[0] . "/" . $get_date_stop[2];




            $post_data_start_date = strtotime($get_date_start);



            $post_data_end_date = strtotime($get_date_stop);














            $start_date = $get_date_start;

            $dates = date_create($start_date);
            $start_date = date_format($dates, "Y-m-d H:i:s");



            $end_date = $get_date_stop;


            $dates = date_create($end_date);
            $end_date = date_format($dates, "Y-m-d H:i:s");


            $excludeArr = array('start_date', 'start_end', "no_need", "route_details", "route_id", "start_time", "driver_id", "trip_name", "end_time", "bus_id", "event-start-date", "event-end-date");
            $condition = array();

            $dataArr = array(
                'operator_id' => new MongoDB\BSON\ObjectId($this->checkLogin('O')),
                'route_details' => json_decode($this->input->post('route_details')),
                'start_time' => $this->input->post('start_time'),
                'end_time' => $this->input->post('end_time'),
                'driver_id' => $this->input->post('driver_id'),
                'captain_id' => $this->input->post('captain_id'),
                'whom_to_track' => $this->input->post('whom_to_track'),
                'bus_id' => $this->input->post('bus_id'),
                'trip_name' => $this->input->post('trip_name'),
                'trip_start_date' => new MongoDB\BSON\UTCDateTime(strtotime($start_date)* 1000),
                'trip_end_date' => new MongoDB\BSON\UTCDateTime(strtotime($end_date)* 1000),
                'created_by_id' => $this->session->userdata(APP_NAME . '_session_changes_done_by_id'),
                'created_by_name' => $this->session->userdata(APP_NAME . '_session_changes_done_by_name'),
                'created_at' => date("Y-m-d H:i:s")
            );

            $this->bus_trip_model->commonInsertUpdate(BUS_TRIP, 'insert', $excludeArr, $dataArr, $condition);
            $location_id = $this->cimongo->insert_id();

            $this->setErrorMessage('success', 'Trip added successfully');

            $this->data["heading"] = "Assign Trip";

            $this->data['route_list'] = $this->bus_route_model->get_all_details(BUS_ROUTE, array("operator_id" => new MongoDB\BSON\ObjectId($this->checkLogin('O'))));

            $this->data['driver_list'] = $this->driver_model->get_all_details(DRIVERS, array("operator_id" => new MongoDB\BSON\ObjectId($this->checkLogin('O'))));

            $this->data['Bus_list'] = $this->bus_details_model->get_all_details(BUS_DETAILS, array("operator_id" => new MongoDB\BSON\ObjectId($this->checkLogin('O'))));

            $this->data['center'] = $this->config->item('latitude') . ',' . $this->config->item('longitude');

            redirect(OPERATOR_NAME . '/trip/trip_list_view', $this->data);
        }
    }

    ////////////			trip list

    public function trip_list_view() {


        if ($this->checkLogin('O') == '') {
            $this->setErrorMessage('error', 'You must login first', 'admin_driver_login_first');
            redirect(OPERATOR_NAME);
        }

        if ($this->lang->line('Trip_List') != '')
            $this->data['heading'] = stripslashes($this->lang->line('Trip_List'));
        else
            $this->data['heading'] = 'Trip List';


        //$this->cimongo->where(array('driver_location' => $this->session->userdata(APP_NAME.'_session_operator_location'),'operator_id' =>new MongoDB\BSON\ObjectId($this->checkLogin('O'))));

        $tripCount = $this->bus_trip_model->get_all_counts(BUS_TRIP, array("operator_id" => new MongoDB\BSON\ObjectId($this->checkLogin('O'))), array('created_at' => 'DESC'));
        if ($tripCount > 1000) {
            $searchPerPage = 500;
            $paginationNo = $this->uri->segment(4);
            if ($paginationNo == '') {
                $paginationNo = 0;
            } else {
                $paginationNo = $paginationNo;
            }

            //$this->cimongo->where(array('driver_location'=>$this->session->userdata(APP_NAME.'_session_operator_location'),"operator_id"=> new MongoDB\BSON\ObjectId($this->checkLogin('O'))));
            $this->data['bus_trip_list'] = $this->bus_trip_model->get_all_details(BUS_TRIP, array("operator_id" => new MongoDB\BSON\ObjectId($this->checkLogin('O'))), array('created_at' => 'DESC'), $paginationNo);

            $searchbaseUrl = OPERATOR_NAME . '/trip/trip_list/';
            $config['num_links'] = 3;
            $config['display_pages'] = TRUE;
            $config['base_url'] = $searchbaseUrl;
            $config['total_rows'] = $tripCount;
            $config["per_page"] = $searchPerPage;
            $config["uri_segment"] = 4;
            $config['first_link'] = '';
            $config['last_link'] = '';
            $config['full_tag_open'] = '<ul class="tsc_pagination tsc_paginationA tsc_paginationA01">';
            $config['full_tag_close'] = '</ul>';
            if ($this->lang->line('pagination_prev_lbl') != '')
                $config['prev_link'] = stripslashes($this->lang->line('pagination_prev_lbl'));
            else
                $config['prev_link'] = 'Prev';
            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '</li>';
            if ($this->lang->line('pagination_next_lbl') != '')
                $config['next_link'] = stripslashes($this->lang->line('pagination_next_lbl'));
            else
                $config['next_link'] = 'Next';
            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="current"><a href="javascript:void(0);" style="cursor:default;">';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';
            $config['first_tag_open'] = '<li>';
            $config['first_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li>';
            $config['last_tag_close'] = '</li>';

            if ($this->lang->line('pagination_first_lbl') != '')
                $config['first_link'] = stripslashes($this->lang->line('pagination_first_lbl'));
            else
                $config['first_link'] = 'First';
            if ($this->lang->line('pagination_last_lbl') != '')
                $config['last_link'] = stripslashes($this->lang->line('pagination_last_lbl'));
            else
                $config['last_link'] = 'Last';
            $this->pagination->initialize($config);
            $paginationLink = $this->pagination->create_links();
            $this->data['paginationLink'] = $paginationLink;
        } else {
            $this->data['paginationLink'] = '';
            $condition = array();
            //$this->cimongo->where(array('driver_location'=>$this->session->userdata(APP_NAME.'_session_operator_location'),"operator_id"=> new MongoDB\BSON\ObjectId($this->checkLogin('O'))));
            $this->data['bus_trip_list'] = $this->bus_trip_model->get_all_details(BUS_TRIP, array("operator_id" => new MongoDB\BSON\ObjectId($this->checkLogin('O'))), array('created_at' => 'DESC'));
        }



        $this->load->view(OPERATOR_NAME . '/trip/trip_list', $this->data);
    }

    public function edit_trip_bus() {

        if ($this->checkLogin('O') == '') {
            redirect(OPERATOR_NAME);
        } else {







            $trip_id = $this->uri->segment(4);







            ////////////checking for live trips





            $current_trip_data = $this->app_model->get_all_details(CURRENT_TRIP_DETAILS, array("trip_id" => new MongoDB\BSON\ObjectId($trip_id)), array('trip_start_time' => -1), 1);



            $current_trip_data = $current_trip_data->result();





            if (isset($current_trip_data[0]) && !is_null($current_trip_data[0]->trip_start_time)) {

                if (strtotime(date('Y-m-d', $current_trip_data[0]->trip_start_time->toDateTime()->getTimestamp())) == strtotime(date("Y-m-d")) && $current_trip_data[0]->trip_action == "start") {

                    $this->setErrorMessage('error', 'You can not edit live trips');

                    redirect(OPERATOR_NAME . '/trip/trip_list');
                }
            }
























            $this->data["trip_data"] = $this->bus_trip_model->get_all_details(BUS_TRIP, array("_id" => new MongoDB\BSON\ObjectId($trip_id)));





            $this->data["trip_id"] = $trip_id;


            $this->data["heading"] = "Edit Trip";


            $this->data['route_list'] = $this->bus_route_model->get_all_details(BUS_ROUTE, array("operator_id" => new MongoDB\BSON\ObjectId($this->checkLogin('O'))));

            $this->data['driver_list'] = $this->driver_model->get_all_details(DRIVERS, array("operator_id" => new MongoDB\BSON\ObjectId($this->checkLogin('O'))));

            $this->data['Bus_list'] = $this->bus_details_model->get_all_details(BUS_DETAILS, array("operator_id" => new MongoDB\BSON\ObjectId($this->checkLogin('O'))));

            $this->data['center'] = $this->config->item('latitude') . ',' . $this->config->item('longitude');

            $this->load->view(OPERATOR_NAME . '/trip/edit_trip', $this->data);
        }
    }

    public function update_bus_trip() {
        if ($this->checkLogin('O') == '') {
            redirect(OPERATOR_NAME);
        } else {


            $trip_data_all = $this->driver_model->get_all_details(BUS_TRIP, array("operator_id" => new MongoDB\BSON\ObjectId($this->checkLogin('O')), 'driver_id' => $this->input->post('driver_id')));

            $trip_all_data = $trip_data_all->result();

            $post_data_driver_id = $this->input->post('driver_id');
            $post_data_start_time = $this->input->post('start_time');
            $post_data_end_time = $this->input->post('end_time');

            $post_data_trip_id = (string) $this->input->post('trip_id');




            $errMsg = "";



            ////////changing format

            $get_date_start = explode('/', $this->input->post('start_date'));

            $get_date_start = $get_date_start[1] . "/" . $get_date_start[0] . "/" . $get_date_start[2];

            $get_date_stop = explode('/', $this->input->post('end_date'));

            $get_date_stop = $get_date_stop[1] . "/" . $get_date_stop[0] . "/" . $get_date_stop[2];




            $start_date = $get_date_start;

            $dates = date_create($start_date);
            $start_date = date_format($dates, "Y-m-d H:i:s");



            $end_date = $get_date_stop;


            $dates = date_create($end_date);
            $end_date = date_format($dates, "Y-m-d H:i:s");



            $excludeArr = array("no_need", "route_details", "route_id", "start_time", "driver_id", "trip_name", "end_time", "bus_id", "trip_id", "event-start-date", "event-end-date");

            $condition = array('_id' => new MongoDB\BSON\ObjectId($this->input->post('trip_id')));

            $dataArr = array(
                'operator_id' => new MongoDB\BSON\ObjectId($this->checkLogin('O')),
                'route_details' => json_decode($this->input->post('route_details')),
                'start_time' => $this->input->post('start_time'),
                'end_time' => $this->input->post('end_time'),
                'driver_id' => $this->input->post('driver_id'),
                'captain_id' => $this->input->post('captain_id'),
                'whom_to_track' => $this->input->post('whom_to_track'),
                'bus_id' => $this->input->post('bus_id'),
                'trip_name' => $this->input->post('trip_name'),
                'trip_start_date' => new MongoDB\BSON\UTCDateTime(strtotime($start_date)* 1000),
                'trip_end_date' => new MongoDB\BSON\UTCDateTime(strtotime($end_date)* 1000),
                'updated_by_id' => $this->session->userdata(APP_NAME . '_session_changes_done_by_id'),
                'updated_by_name' => $this->session->userdata(APP_NAME . '_session_changes_done_by_name'),
                'updated_at' => date("Y-m-d H:i:s")
            );

            $this->bus_trip_model->commonInsertUpdate(BUS_TRIP, 'update', $excludeArr, $dataArr, $condition);





            $this->setErrorMessage('success', 'Trip Edited successfully');



            redirect(OPERATOR_NAME . '/trip/trip_list_view', $this->data);
        }
    }

    public function view_trip() {

        if ($this->checkLogin('O') == '') {
            redirect(OPERATOR_NAME);
        } else {


            $trip_id = $this->uri->segment(4);



            $this->data["trip_data"] = $this->bus_trip_model->get_all_details(BUS_TRIP, array("_id" => new MongoDB\BSON\ObjectId($trip_id)));

            $this->data["trip_id"] = $trip_id;


            $this->data["heading"] = "View Trip";


            $this->data['route_list'] = $this->bus_route_model->get_all_details(BUS_ROUTE, array("operator_id" => new MongoDB\BSON\ObjectId($this->checkLogin('O'))));

            $this->data['driver_list'] = $this->driver_model->get_all_details(DRIVERS, array("operator_id" => new MongoDB\BSON\ObjectId($this->checkLogin('O'))));

            $this->data['Bus_list'] = $this->bus_details_model->get_all_details(BUS_DETAILS, array("operator_id" => new MongoDB\BSON\ObjectId($this->checkLogin('O'))));

            $this->data['center'] = $this->config->item('latitude') . ',' . $this->config->item('longitude');

            $this->load->view(OPERATOR_NAME . '/trip/trip_deatils_view', $this->data);
        }
    }

    public function driver_assigned_check() {

        $trip_data_all = $this->driver_model->get_all_details(BUS_TRIP, array("operator_id" => new MongoDB\BSON\ObjectId($this->checkLogin('O')), 'driver_id' => $this->input->post('driver_id')));

        $trip_all_data = $trip_data_all->result();

        $post_data_driver_id = $this->input->post('driver_id');
        $post_data_start_time = strtotime($this->input->post('start_time'));
        $post_data_end_time = strtotime($this->input->post('end_time'));


        ////////changing format

        $get_date_start = explode('/', $this->input->post('start_date'));

        $get_date_start = $get_date_start[1] . "/" . $get_date_start[0] . "/" . $get_date_start[2];

        $get_date_stop = explode('/', $this->input->post('end_date'));

        $get_date_stop = $get_date_stop[1] . "/" . $get_date_stop[0] . "/" . $get_date_stop[2];




        $post_data_start_date = strtotime($get_date_start);



        $post_data_end_date = strtotime($get_date_stop);







        $errMsg = 0;



        for ($i = 0; $i < sizeof($trip_all_data); $i++) {
            $date_avail = 0;

            $driver_id_s = (string) $trip_all_data[$i]->driver_id;
            $trip_start_tm = strtotime($trip_all_data[$i]->start_time);
            $trip_end_tm = strtotime($trip_all_data[$i]->end_time);

            $trip_start_date = $trip_all_data[$i]->trip_start_date->toDateTime()->getTimestamp();

            $trip_end_date = $trip_all_data[$i]->trip_end_date->toDateTime()->getTimestamp();





            if ($post_data_driver_id == $driver_id_s) {

                /////////availability_date_checking

                if ($post_data_start_date >= $trip_start_date && $post_data_start_date <= $trip_end_date) {

                    $date_avail = 1;
                }

                if ($post_data_end_date <= $trip_end_date && $post_data_end_date >= $trip_start_date) {

                    $date_avail = 1;
                }

                if ($post_data_end_date >= $trip_start_date && $post_data_end_date <= $trip_end_date) {
                    $date_avail = 1;
                }

                if ($post_data_start_date >= $trip_start_date && $post_data_start_date <= $trip_end_date) {
                    $date_avail = 1;
                }

                if ($post_data_start_date <= $trip_start_date && $post_data_end_date >= $trip_end_date) {
                    $date_avail = 1;
                }



















                /////time checking

                if ($date_avail == 1) {
                    if ($post_data_start_time >= $trip_start_tm && $post_data_start_time <= $trip_end_tm) {

                        $errMsg = 1;

                        break;
                    }

                    if ($post_data_end_time <= $trip_end_tm && $post_data_end_time >= $trip_start_tm) {

                        $errMsg = 1;
                        break;
                    }

                    if ($post_data_end_time >= $trip_start_tm && $post_data_end_time <= $trip_end_tm) {
                        $errMsg = 1;
                        break;
                    }

                    if ($post_data_start_time >= $trip_start_tm && $post_data_start_time <= $trip_end_tm) {
                        $errMsg = 1;
                        break;
                    }

                    if ($post_data_start_time <= $trip_start_tm && $post_data_end_time >= $trip_end_tm) {
                        $errMsg = 1;
                        break;
                    }
                }
            }
        }
        $jsonarray=array();
        if(!empty($this->input->post('captain_id'))){
            $emsg=$this->check_captain_availability();
            if($errMsg == 1 && $emsg==1){
                $jsonarray['message']="The selected driver and captain is already assigned with a trip at the same time. Please change the time or select another driver and captain to assign";
                $jsonarray['result']=1;
            }
            elseif($errMsg == 0 && $emsg==1){
                $jsonarray['message']="The selected captain and captain is already assigned with a trip at the same time. Please change the time or select another captain to assign";
                $jsonarray['result']=1;
            }
        }else{
            if($errMsg == 1){
                $jsonarray['message']="The selected driver and captain is already assigned with a trip at the same time. Please change the time or select another captain to driver";
                $jsonarray['result']=1;
            }else{
                $jsonarray['result']=0;
            }
        }

        echo json_encode($jsonarray);
        die();
    }
    private function check_captain_availability(){
        $trip_data_all = $this->driver_model->get_all_details(BUS_TRIP, array("operator_id" => new MongoDB\BSON\ObjectId($this->checkLogin('O')), 'captain_id' => $this->input->post('captain_id')));

        $trip_all_data = $trip_data_all->result();

        $post_data_driver_id = $this->input->post('driver_id');
        $post_data_start_time = strtotime($this->input->post('start_time'));
        $post_data_end_time = strtotime($this->input->post('end_time'));


        ////////changing format

        $get_date_start = explode('/', $this->input->post('start_date'));

        $get_date_start = $get_date_start[1] . "/" . $get_date_start[0] . "/" . $get_date_start[2];

        $get_date_stop = explode('/', $this->input->post('end_date'));

        $get_date_stop = $get_date_stop[1] . "/" . $get_date_stop[0] . "/" . $get_date_stop[2];




        $post_data_start_date = strtotime($get_date_start);



        $post_data_end_date = strtotime($get_date_stop);







        $errMsg = 0;



        for ($i = 0; $i < sizeof($trip_all_data); $i++) {
            $date_avail = 0;

            $driver_id_s = (string) $trip_all_data[$i]->driver_id;
            $trip_start_tm = strtotime($trip_all_data[$i]->start_time);
            $trip_end_tm = strtotime($trip_all_data[$i]->end_time);

            $trip_start_date = $trip_all_data[$i]->trip_start_date->toDateTime()->getTimestamp();

            $trip_end_date = $trip_all_data[$i]->trip_end_date->toDateTime()->getTimestamp();





            if ($post_data_driver_id == $driver_id_s) {

                /////////availability_date_checking

                if ($post_data_start_date >= $trip_start_date && $post_data_start_date <= $trip_end_date) {

                    $date_avail = 1;
                }

                if ($post_data_end_date <= $trip_end_date && $post_data_end_date >= $trip_start_date) {

                    $date_avail = 1;
                }

                if ($post_data_end_date >= $trip_start_date && $post_data_end_date <= $trip_end_date) {
                    $date_avail = 1;
                }

                if ($post_data_start_date >= $trip_start_date && $post_data_start_date <= $trip_end_date) {
                    $date_avail = 1;
                }

                if ($post_data_start_date <= $trip_start_date && $post_data_end_date >= $trip_end_date) {
                    $date_avail = 1;
                }



















                /////time checking

                if ($date_avail == 1) {
                    if ($post_data_start_time >= $trip_start_tm && $post_data_start_time <= $trip_end_tm) {

                        $errMsg = 1;

                        break;
                    }

                    if ($post_data_end_time <= $trip_end_tm && $post_data_end_time >= $trip_start_tm) {

                        $errMsg = 1;
                        break;
                    }

                    if ($post_data_end_time >= $trip_start_tm && $post_data_end_time <= $trip_end_tm) {
                        $errMsg = 1;
                        break;
                    }

                    if ($post_data_start_time >= $trip_start_tm && $post_data_start_time <= $trip_end_tm) {
                        $errMsg = 1;
                        break;
                    }

                    if ($post_data_start_time <= $trip_start_tm && $post_data_end_time >= $trip_end_tm) {
                        $errMsg = 1;
                        break;
                    }
                }
            }
        }


        return $errMsg;
    }
    public function bus_assigned_check() {



        $trip_data_all = $this->driver_model->get_all_details(BUS_TRIP, array("operator_id" => new MongoDB\BSON\ObjectId($this->checkLogin('O')), 'bus_id' => $this->input->post('bus_id')));

        $trip_all_data = $trip_data_all->result();

        $post_data_bus_id = $this->input->post('bus_id');
        $post_data_start_time = strtotime($this->input->post('start_time'));
        $post_data_end_time = strtotime($this->input->post('end_time'));


        ////////changing format

        $get_date_start = explode('/', $this->input->post('start_date'));

        $get_date_start = $get_date_start[1] . "/" . $get_date_start[0] . "/" . $get_date_start[2];

        $get_date_stop = explode('/', $this->input->post('end_date'));

        $get_date_stop = $get_date_stop[1] . "/" . $get_date_stop[0] . "/" . $get_date_stop[2];




        $post_data_start_date = strtotime($get_date_start);



        $post_data_end_date = strtotime($get_date_stop);







        $errMsg = 0;



        for ($i = 0; $i < sizeof($trip_all_data); $i++) {
            $date_avail = 0;

            $post_data_bus_id_s = (string) $trip_all_data[$i]->bus_id;
            $trip_start_tm = strtotime($trip_all_data[$i]->start_time);
            $trip_end_tm = strtotime($trip_all_data[$i]->end_time);

            $trip_start_date = $trip_all_data[$i]->trip_start_date->toDateTime()->getTimestamp();

            $trip_end_date = $trip_all_data[$i]->trip_end_date->toDateTime()->getTimestamp();





            if ($post_data_bus_id == $post_data_bus_id_s) {

                /////////availability_date_checking

                if ($post_data_start_date >= $trip_start_date && $post_data_start_date <= $trip_end_date) {

                    $date_avail = 1;
                }

                if ($post_data_end_date <= $trip_end_date && $post_data_end_date >= $trip_start_date) {

                    $date_avail = 1;
                }

                if ($post_data_end_date >= $trip_start_date && $post_data_end_date <= $trip_end_date) {
                    $date_avail = 1;
                }

                if ($post_data_start_date >= $trip_start_date && $post_data_start_date <= $trip_end_date) {
                    $date_avail = 1;
                }

                if ($post_data_start_date <= $trip_start_date && $post_data_end_date >= $trip_end_date) {
                    $date_avail = 1;
                }



















                /////time checking

                if ($date_avail == 1) {
                    if ($post_data_start_time >= $trip_start_tm && $post_data_start_time <= $trip_end_tm) {

                        $errMsg = 1;

                        break;
                    }

                    if ($post_data_end_time <= $trip_end_tm && $post_data_end_time >= $trip_start_tm) {

                        $errMsg = 1;
                        break;
                    }

                    if ($post_data_end_time >= $trip_start_tm && $post_data_end_time <= $trip_end_tm) {
                        $errMsg = 1;
                        break;
                    }

                    if ($post_data_start_time >= $trip_start_tm && $post_data_start_time <= $trip_end_tm) {
                        $errMsg = 1;
                        break;
                    }

                    if ($post_data_start_time <= $trip_start_tm && $post_data_end_time >= $trip_end_tm) {
                        $errMsg = 1;
                        break;
                    }
                }
            }
        }


        echo $errMsg;
    }

    public function driver_assigned_check_update() {



        $trip_data_all = $this->driver_model->get_all_details(BUS_TRIP, array("operator_id" => new MongoDB\BSON\ObjectId($this->checkLogin('O')), 'driver_id' => $this->input->post('driver_id'), "_id" => array('$ne' => new MongoDB\BSON\ObjectId($this->input->post('trip_id')))));

        $trip_all_data = $trip_data_all->result();

        $post_data_driver_id = $this->input->post('driver_id');



        $post_data_start_time = strtotime($this->input->post('start_time'));
        $post_data_end_time = strtotime($this->input->post('end_time'));




        ////////changing format

        $get_date_start = explode('/', $this->input->post('start_date'));

        $get_date_start = $get_date_start[1] . "/" . $get_date_start[0] . "/" . $get_date_start[2];

        $get_date_stop = explode('/', $this->input->post('end_date'));

        $get_date_stop = $get_date_stop[1] . "/" . $get_date_stop[0] . "/" . $get_date_stop[2];




        $post_data_start_date = strtotime($get_date_start);



        $post_data_end_date = strtotime($get_date_stop);







        $errMsg = 0;



        for ($i = 0; $i < sizeof($trip_all_data); $i++) {
            $date_avail = 0;

            $driver_id_s = (string) $trip_all_data[$i]->driver_id;
            $trip_start_tm = strtotime($trip_all_data[$i]->start_time);
            $trip_end_tm = strtotime($trip_all_data[$i]->end_time);

            $trip_start_date = $trip_all_data[$i]->trip_start_date->toDateTime()->getTimestamp();

            $trip_end_date = $trip_all_data[$i]->trip_end_date->toDateTime()->getTimestamp();










            if ($post_data_driver_id == $driver_id_s) {

                /////////availability_date_checking

                if ($post_data_start_date >= $trip_start_date && $post_data_start_date <= $trip_end_date) {

                    $date_avail = 1;
                }

                if ($post_data_end_date <= $trip_end_date && $post_data_end_date >= $trip_start_date) {

                    $date_avail = 1;
                }

                if ($post_data_end_date >= $trip_start_date && $post_data_end_date <= $trip_end_date) {
                    $date_avail = 1;
                }

                if ($post_data_start_date >= $trip_start_date && $post_data_start_date <= $trip_end_date) {
                    $date_avail = 1;
                }

                if ($post_data_start_date <= $trip_start_date && $post_data_end_date >= $trip_end_date) {
                    $date_avail = 1;
                }














                /////time checking

                if ($date_avail == 1) {
                    if ($post_data_start_time >= $trip_start_tm && $post_data_start_time <= $trip_end_tm) {

                        $errMsg = 1;

                        break;
                    }

                    if ($post_data_end_time <= $trip_end_tm && $post_data_end_time >= $trip_start_tm) {

                        $errMsg = 1;
                        break;
                    }

                    if ($post_data_end_time >= $trip_start_tm && $post_data_end_time <= $trip_end_tm) {
                        $errMsg = 1;
                        break;
                    }

                    if ($post_data_start_time >= $trip_start_tm && $post_data_start_time <= $trip_end_tm) {
                        $errMsg = 1;
                        break;
                    }

                    if ($post_data_start_time <= $trip_start_tm && $post_data_end_time >= $trip_end_tm) {
                        $errMsg = 1;
                        break;
                    }
                }
            }
        }

        $jsonarray=array();
        if(!empty($this->input->post('captain_id'))){
            $emsg=$this->captain_assigned_check_update();
            if($errMsg == 1 && $emsg==1){
                $jsonarray['message']="The selected driver and captain is already assigned with a trip at the same time. Please change the time or select another driver and captain to assign";
                $jsonarray['result']=1;
            }
            elseif($errMsg == 0 && $emsg==1){
                $jsonarray['message']="The selected captain and captain is already assigned with a trip at the same time. Please change the time or select another captain to assign";
                $jsonarray['result']=1;
            }
        }else{
            if($errMsg == 1){
                $jsonarray['message']="The selected driver and captain is already assigned with a trip at the same time. Please change the time or select another captain to driver";
                $jsonarray['result']=1;
            }else{
                $jsonarray['result']=0;
            }
        }

        echo json_encode($jsonarray);
        die();
    }
private function captain_assigned_check_update() {



        $trip_data_all = $this->driver_model->get_all_details(BUS_TRIP, array("operator_id" => new MongoDB\BSON\ObjectId($this->checkLogin('O')), 'captain_id' => $this->input->post('driver_id'), "_id" => array('$ne' => new MongoDB\BSON\ObjectId($this->input->post('trip_id')))));

        $trip_all_data = $trip_data_all->result();

        $post_data_driver_id = $this->input->post('driver_id');



        $post_data_start_time = strtotime($this->input->post('start_time'));
        $post_data_end_time = strtotime($this->input->post('end_time'));




        ////////changing format

        $get_date_start = explode('/', $this->input->post('start_date'));

        $get_date_start = $get_date_start[1] . "/" . $get_date_start[0] . "/" . $get_date_start[2];

        $get_date_stop = explode('/', $this->input->post('end_date'));

        $get_date_stop = $get_date_stop[1] . "/" . $get_date_stop[0] . "/" . $get_date_stop[2];




        $post_data_start_date = strtotime($get_date_start);



        $post_data_end_date = strtotime($get_date_stop);







        $errMsg = 0;



        for ($i = 0; $i < sizeof($trip_all_data); $i++) {
            $date_avail = 0;

            $driver_id_s = (string) $trip_all_data[$i]->driver_id;
            $trip_start_tm = strtotime($trip_all_data[$i]->start_time);
            $trip_end_tm = strtotime($trip_all_data[$i]->end_time);

            $trip_start_date = $trip_all_data[$i]->trip_start_date->toDateTime()->getTimestamp();

            $trip_end_date = $trip_all_data[$i]->trip_end_date->toDateTime()->getTimestamp();










            if ($post_data_driver_id == $driver_id_s) {

                /////////availability_date_checking

                if ($post_data_start_date >= $trip_start_date && $post_data_start_date <= $trip_end_date) {

                    $date_avail = 1;
                }

                if ($post_data_end_date <= $trip_end_date && $post_data_end_date >= $trip_start_date) {

                    $date_avail = 1;
                }

                if ($post_data_end_date >= $trip_start_date && $post_data_end_date <= $trip_end_date) {
                    $date_avail = 1;
                }

                if ($post_data_start_date >= $trip_start_date && $post_data_start_date <= $trip_end_date) {
                    $date_avail = 1;
                }

                if ($post_data_start_date <= $trip_start_date && $post_data_end_date >= $trip_end_date) {
                    $date_avail = 1;
                }














                /////time checking

                if ($date_avail == 1) {
                    if ($post_data_start_time >= $trip_start_tm && $post_data_start_time <= $trip_end_tm) {

                        $errMsg = 1;

                        break;
                    }

                    if ($post_data_end_time <= $trip_end_tm && $post_data_end_time >= $trip_start_tm) {

                        $errMsg = 1;
                        break;
                    }

                    if ($post_data_end_time >= $trip_start_tm && $post_data_end_time <= $trip_end_tm) {
                        $errMsg = 1;
                        break;
                    }

                    if ($post_data_start_time >= $trip_start_tm && $post_data_start_time <= $trip_end_tm) {
                        $errMsg = 1;
                        break;
                    }

                    if ($post_data_start_time <= $trip_start_tm && $post_data_end_time >= $trip_end_tm) {
                        $errMsg = 1;
                        break;
                    }
                }
            }
        }

        return $errMsg;
    }
    public function bus_assigned_check_update() {



        $trip_data_all = $this->driver_model->get_all_details(BUS_TRIP, array("operator_id" => new MongoDB\BSON\ObjectId($this->checkLogin('O')), 'bus_id' => $this->input->post('bus_id'), "_id" => array('$ne' => new MongoDB\BSON\ObjectId($this->input->post('trip_id')))));

        $trip_all_data = $trip_data_all->result();

        $post_data_bus_id = $this->input->post('bus_id');
        $post_data_start_time = strtotime($this->input->post('start_time'));
        $post_data_end_time = strtotime($this->input->post('end_time'));


        ////////changing format

        $get_date_start = explode('/', $this->input->post('start_date'));

        $get_date_start = $get_date_start[1] . "/" . $get_date_start[0] . "/" . $get_date_start[2];

        $get_date_stop = explode('/', $this->input->post('end_date'));

        $get_date_stop = $get_date_stop[1] . "/" . $get_date_stop[0] . "/" . $get_date_stop[2];




        $post_data_start_date = strtotime($get_date_start);



        $post_data_end_date = strtotime($get_date_stop);







        $errMsg = 0;



        for ($i = 0; $i < sizeof($trip_all_data); $i++) {
            $date_avail = 0;

            $post_data_bus_id_s = (string) $trip_all_data[$i]->bus_id;
            $trip_start_tm = strtotime($trip_all_data[$i]->start_time);
            $trip_end_tm = strtotime($trip_all_data[$i]->end_time);

            $trip_start_date = $trip_all_data[$i]->trip_start_date->toDateTime()->getTimestamp();

            $trip_end_date = $trip_all_data[$i]->trip_end_date->toDateTime()->getTimestamp();





            if ($post_data_bus_id == $post_data_bus_id_s) {

                /////////availability_date_checking

                if ($post_data_start_date >= $trip_start_date && $post_data_start_date <= $trip_end_date) {

                    $date_avail = 1;
                }

                if ($post_data_end_date <= $trip_end_date && $post_data_end_date >= $trip_start_date) {

                    $date_avail = 1;
                }

                if ($post_data_end_date >= $trip_start_date && $post_data_end_date <= $trip_end_date) {
                    $date_avail = 1;
                }

                if ($post_data_start_date >= $trip_start_date && $post_data_start_date <= $trip_end_date) {
                    $date_avail = 1;
                }

                if ($post_data_start_date <= $trip_start_date && $post_data_end_date >= $trip_end_date) {
                    $date_avail = 1;
                }



















                /////time checking

                if ($date_avail == 1) {
                    if ($post_data_start_time >= $trip_start_tm && $post_data_start_time <= $trip_end_tm) {

                        $errMsg = 1;

                        break;
                    }

                    if ($post_data_end_time <= $trip_end_tm && $post_data_end_time >= $trip_start_tm) {

                        $errMsg = 1;
                        break;
                    }

                    if ($post_data_end_time >= $trip_start_tm && $post_data_end_time <= $trip_end_tm) {
                        $errMsg = 1;
                        break;
                    }

                    if ($post_data_start_time >= $trip_start_tm && $post_data_start_time <= $trip_end_tm) {
                        $errMsg = 1;
                        break;
                    }

                    if ($post_data_start_time <= $trip_start_tm && $post_data_end_time >= $trip_end_tm) {
                        $errMsg = 1;
                        break;
                    }
                }
            }
        }


        echo $errMsg;
    }

    public function trip_details_list() {
        if ($this->checkLogin('O') == '') {
            $this->setErrorMessage('error', 'You must login first', 'admin_driver_login_first');
            redirect(OPERATOR_NAME);
        } else
            $this->data['heading'] = 'Trips History';


        //$this->cimongo->where(array('driver_location' => $this->session->userdata(APP_NAME.'_session_operator_location'),'operator_id' => new MongoDB\BSON\ObjectId($this->checkLogin('O'))));




        $tripCount = $this->app_model->get_all_counts(CURRENT_TRIP_DETAILS, array("operator_id" => new MongoDB\BSON\ObjectId($this->checkLogin('O'))));

        if ($tripCount > 1000) {
            $searchPerPage = 500;
            $paginationNo = $this->uri->segment(4);
            if ($paginationNo == '') {
                $paginationNo = 0;
            } else {
                $paginationNo = $paginationNo;
            }
            //$this->cimongo->where(array('driver_location'=>$this->session->userdata(APP_NAME.'_session_operator_location'),"operator_id"=> new MongoDB\BSON\ObjectId($this->checkLogin('O'))));
            $this->data['trip_detail_list'] = $this->app_model->get_all_details(CURRENT_TRIP_DETAILS, array("operator_id" => new MongoDB\BSON\ObjectId($this->checkLogin('O'))), array('_id' => 'DESC'), $paginationNo);

            $searchbaseUrl = OPERATOR_NAME . '/trip/trip_details_list/';
            $config['num_links'] = 3;
            $config['display_pages'] = TRUE;
            $config['base_url'] = $searchbaseUrl;
            $config['total_rows'] = $tripCount;
            $config["per_page"] = $searchPerPage;
            $config["uri_segment"] = 4;
            $config['first_link'] = '';
            $config['last_link'] = '';
            $config['full_tag_open'] = '<ul class="tsc_pagination tsc_paginationA tsc_paginationA01">';
            $config['full_tag_close'] = '</ul>';
            if ($this->lang->line('pagination_prev_lbl') != '')
                $config['prev_link'] = stripslashes($this->lang->line('pagination_prev_lbl'));
            else
                $config['prev_link'] = 'Prev';
            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '</li>';
            if ($this->lang->line('pagination_next_lbl') != '')
                $config['next_link'] = stripslashes($this->lang->line('pagination_next_lbl'));
            else
                $config['next_link'] = 'Next';
            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="current"><a href="javascript:void(0);" style="cursor:default;">';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';
            $config['first_tag_open'] = '<li>';
            $config['first_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li>';
            $config['last_tag_close'] = '</li>';

            if ($this->lang->line('pagination_first_lbl') != '')
                $config['first_link'] = stripslashes($this->lang->line('pagination_first_lbl'));
            else
                $config['first_link'] = 'First';
            if ($this->lang->line('pagination_last_lbl') != '')
                $config['last_link'] = stripslashes($this->lang->line('pagination_last_lbl'));
            else
                $config['last_link'] = 'Last';
            $this->pagination->initialize($config);
            $paginationLink = $this->pagination->create_links();
            $this->data['paginationLink'] = $paginationLink;
        } else {
            $this->data['paginationLink'] = '';
            $condition = array();
            //$this->cimongo->where(array('driver_location'=>$this->session->userdata(APP_NAME.'_session_operator_location'),"operator_id"=> new MongoDB\BSON\ObjectId($this->checkLogin('O'))));
            $tripdetails = $this->app_model->get_all_details(CURRENT_TRIP_DETAILS, array("operator_id" => new MongoDB\BSON\ObjectId($this->checkLogin('O'))), array('_id' => 'DESC'));

            $this->data['trip_detail_list'] = $tripdetails;
        }



        $this->load->view(OPERATOR_NAME . '/trip/trip_details_list_view', $this->data);
    }

    public function trip_assigned_checker() {

        $trip_id = new MongoDB\BSON\ObjectId($this->input->post('trip_id'));

        $condition = array('$or' => array(array("student_pickup_trip_id" => $trip_id), array("student_drop_trip_id" => $trip_id)));
        $student_details = $this->student_details_model->get_all_details(STUDENT_DETAILS, $condition);



        $error_msg = 0;
        if ($student_details->num_rows() > 0) {
            $error_msg = $student_details->num_rows();
        }

        echo $error_msg;
    }

    public function vehicle_plying_list() {





        if ($this->checkLogin('O') == '') {
            $this->setErrorMessage('error', 'You must login first', 'admin_driver_login_first');
            redirect(OPERATOR_NAME);
        }


        $this->data['heading'] = 'Vehicle plying';





        $tripCount = $this->app_model->get_all_counts(CURRENT_TRIP_DETAILS, array("operator_id" => new MongoDB\BSON\ObjectId($this->checkLogin('O')), "trip_action" => "start", "start_current_date" => array('$eq' => date('Y-m-d'))));
        if ($tripCount > 1000) {
            $searchPerPage = 500;
            $paginationNo = $this->uri->segment(4);
            if ($paginationNo == '') {
                $paginationNo = 0;
            } else {
                $paginationNo = $paginationNo;
            }

            $this->data['trip_detail_list'] = $this->app_model->get_all_details(CURRENT_TRIP_DETAILS, array("operator_id" => new MongoDB\BSON\ObjectId($this->checkLogin('O')), "trip_action" => "start", "start_current_date" => array('$eq' => date('Y-m-d'))), array('_id' => 'DESC'), $paginationNo);

            $searchbaseUrl = OPERATOR_NAME . '/trip/trip_details_list/';
            $config['num_links'] = 3;
            $config['display_pages'] = TRUE;
            $config['base_url'] = $searchbaseUrl;
            $config['total_rows'] = $tripCount;
            $config["per_page"] = $searchPerPage;
            $config["uri_segment"] = 4;
            $config['first_link'] = '';
            $config['last_link'] = '';
            $config['full_tag_open'] = '<ul class="tsc_pagination tsc_paginationA tsc_paginationA01">';
            $config['full_tag_close'] = '</ul>';
            if ($this->lang->line('pagination_prev_lbl') != '')
                $config['prev_link'] = stripslashes($this->lang->line('pagination_prev_lbl'));
            else
                $config['prev_link'] = 'Prev';
            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '</li>';
            if ($this->lang->line('pagination_next_lbl') != '')
                $config['next_link'] = stripslashes($this->lang->line('pagination_next_lbl'));
            else
                $config['next_link'] = 'Next';
            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="current"><a href="javascript:void(0);" style="cursor:default;">';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';
            $config['first_tag_open'] = '<li>';
            $config['first_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li>';
            $config['last_tag_close'] = '</li>';

            if ($this->lang->line('pagination_first_lbl') != '')
                $config['first_link'] = stripslashes($this->lang->line('pagination_first_lbl'));
            else
                $config['first_link'] = 'First';
            if ($this->lang->line('pagination_last_lbl') != '')
                $config['last_link'] = stripslashes($this->lang->line('pagination_last_lbl'));
            else
                $config['last_link'] = 'Last';
            $this->pagination->initialize($config);
            $paginationLink = $this->pagination->create_links();
            $this->data['paginationLink'] = $paginationLink;
        } else {
            $this->data['paginationLink'] = '';
            $condition = array();

            $this->data['trip_detail_list'] = $this->app_model->get_all_details(CURRENT_TRIP_DETAILS, array("operator_id" => new MongoDB\BSON\ObjectId($this->checkLogin('O')), "trip_action" => "start", "start_current_date" => array('$eq' => date('Y-m-d'))), array('_id' => 'DESC'));
        }

        $this->data["page_type"] = "vehicle_plying_list";

        $this->load->view(OPERATOR_NAME . '/trip/current_trip_details_list_view', $this->data);
    }

    public function delayed_vehicles() {





        if ($this->checkLogin('O') == '') {
            $this->setErrorMessage('error', 'You must login first', 'admin_driver_login_first');
            redirect(OPERATOR_NAME);
        }


        $this->data['heading'] = 'Delayed vehicles';


        $option = array(
            array(
                '$lookup' => array(
                    "from" => CURRENT_TRIP_DETAILS,
                    "localField" => "trip_id",
                    "foreignField" => "trip_id",
                    "as" => "current_trip_details"
                )
            ),
            array('$match' => array(
                    "current_trip_details.operator_id" => new MongoDB\BSON\ObjectId($this->checkLogin('O')),
                    "delay_message" => array('$ne' => "BreakDown"),
                    "current_trip_details.trip_action" => "start",
                 "current_trip_details.start_current_date"  => array('$eq' => date('Y-m-d') )
                )
            )
        );
        $cursor = $this->cimongo->aggregate(DRIVER_NOTIFY, $option);
        $delayed_trip_details = $cursor->toArray();



        $delayed_trip_ids = array();
        $repeater_stopper = array();
        foreach (array_reverse($delayed_trip_details) as $key => $value) {

            if (!in_array($value['trip_id'], $repeater_stopper)) {

                array_push($delayed_trip_ids, $value['_id']);

                array_push($repeater_stopper, $value['trip_id']);
            }
        }







        $tripCount = $this->app_model->get_all_counts(DRIVER_NOTIFY, array("_id" => array('$in' => $delayed_trip_ids)));




        if ($tripCount > 1000) {
            $searchPerPage = 500;
            $paginationNo = $this->uri->segment(4);
            if ($paginationNo == '') {
                $paginationNo = 0;
            } else {
                $paginationNo = $paginationNo;
            }

            $this->data['trip_detail_list'] = $this->app_model->get_all_details(DRIVER_NOTIFY, array("_id" => array('$in' => $delayed_trip_ids)), array('_id' => 'DESC'), $paginationNo);

            $searchbaseUrl = OPERATOR_NAME . '/trip/trip_details_list/';
            $config['num_links'] = 3;
            $config['display_pages'] = TRUE;
            $config['base_url'] = $searchbaseUrl;
            $config['total_rows'] = $tripCount;
            $config["per_page"] = $searchPerPage;
            $config["uri_segment"] = 4;
            $config['first_link'] = '';
            $config['last_link'] = '';
            $config['full_tag_open'] = '<ul class="tsc_pagination tsc_paginationA tsc_paginationA01">';
            $config['full_tag_close'] = '</ul>';
            if ($this->lang->line('pagination_prev_lbl') != '')
                $config['prev_link'] = stripslashes($this->lang->line('pagination_prev_lbl'));
            else
                $config['prev_link'] = 'Prev';
            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '</li>';
            if ($this->lang->line('pagination_next_lbl') != '')
                $config['next_link'] = stripslashes($this->lang->line('pagination_next_lbl'));
            else
                $config['next_link'] = 'Next';
            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="current"><a href="javascript:void(0);" style="cursor:default;">';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';
            $config['first_tag_open'] = '<li>';
            $config['first_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li>';
            $config['last_tag_close'] = '</li>';

            if ($this->lang->line('pagination_first_lbl') != '')
                $config['first_link'] = stripslashes($this->lang->line('pagination_first_lbl'));
            else
                $config['first_link'] = 'First';
            if ($this->lang->line('pagination_last_lbl') != '')
                $config['last_link'] = stripslashes($this->lang->line('pagination_last_lbl'));
            else
                $config['last_link'] = 'Last';
            $this->pagination->initialize($config);
            $paginationLink = $this->pagination->create_links();
            $this->data['paginationLink'] = $paginationLink;
        } else {
            $this->data['paginationLink'] = '';
            $condition = array();

            $this->data['trip_detail_list'] = $this->app_model->get_all_details(DRIVER_NOTIFY, array("_id" => array('$in' => $delayed_trip_ids)), array('_id' => 'DESC'));
        }



        $this->data["page_type"] = "delayed_vehicles";

        $this->load->view(OPERATOR_NAME . '/trip/notification_trip_list_view', $this->data);
    }

    public function breakdown_vehicles() {





        if ($this->checkLogin('O') == '') {
            $this->setErrorMessage('error', 'You must login first', 'admin_driver_login_first');
            redirect(OPERATOR_NAME);
        }


        $this->data['heading'] = 'Breakdown vehicles';


        $option = array(
            array(
                '$lookup' => array(
                    "from" => CURRENT_TRIP_DETAILS,
                    "localField" => "trip_id",
                    "foreignField" => "trip_id",
                    "as" => "current_trip_details"
                )
            ),
            array('$match' => array(
                    "current_trip_details.operator_id" => new MongoDB\BSON\ObjectId($this->checkLogin('O')),
                    "delay_message" => "BreakDown",
                    "current_trip_details.trip_action" => "stop",
                    "current_trip_details.start_current_date" => array('$eq' => date('Y-m-d'))
                )
            )
        );
        $cursor = $this->cimongo->aggregate(DRIVER_NOTIFY, $option);
        $delayed_trip_details = $cursor->toArray();



        $delayed_trip_ids = array();
        $repeater_stopper = array();
        foreach (array_reverse($delayed_trip_details) as $key => $value) {

            if (!in_array($value['trip_id'], $repeater_stopper)) {

                array_push($delayed_trip_ids, $value['_id']);

                array_push($repeater_stopper, $value['trip_id']);
            }
        }




        $tripCount = $this->app_model->get_all_counts(DRIVER_NOTIFY, array("_id" => array('$in' => $delayed_trip_ids)));




        if ($tripCount > 1000) {
            $searchPerPage = 500;
            $paginationNo = $this->uri->segment(4);
            if ($paginationNo == '') {
                $paginationNo = 0;
            } else {
                $paginationNo = $paginationNo;
            }

            $this->data['trip_detail_list'] = $this->app_model->get_all_details(DRIVER_NOTIFY, array("_id" => array('$in' => $delayed_trip_ids)), array('_id' => 'DESC'), $paginationNo);

            $searchbaseUrl = OPERATOR_NAME . '/trip/trip_details_list/';
            $config['num_links'] = 3;
            $config['display_pages'] = TRUE;
            $config['base_url'] = $searchbaseUrl;
            $config['total_rows'] = $tripCount;
            $config["per_page"] = $searchPerPage;
            $config["uri_segment"] = 4;
            $config['first_link'] = '';
            $config['last_link'] = '';
            $config['full_tag_open'] = '<ul class="tsc_pagination tsc_paginationA tsc_paginationA01">';
            $config['full_tag_close'] = '</ul>';
            if ($this->lang->line('pagination_prev_lbl') != '')
                $config['prev_link'] = stripslashes($this->lang->line('pagination_prev_lbl'));
            else
                $config['prev_link'] = 'Prev';
            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '</li>';
            if ($this->lang->line('pagination_next_lbl') != '')
                $config['next_link'] = stripslashes($this->lang->line('pagination_next_lbl'));
            else
                $config['next_link'] = 'Next';
            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="current"><a href="javascript:void(0);" style="cursor:default;">';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';
            $config['first_tag_open'] = '<li>';
            $config['first_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li>';
            $config['last_tag_close'] = '</li>';

            if ($this->lang->line('pagination_first_lbl') != '')
                $config['first_link'] = stripslashes($this->lang->line('pagination_first_lbl'));
            else
                $config['first_link'] = 'First';
            if ($this->lang->line('pagination_last_lbl') != '')
                $config['last_link'] = stripslashes($this->lang->line('pagination_last_lbl'));
            else
                $config['last_link'] = 'Last';
            $this->pagination->initialize($config);
            $paginationLink = $this->pagination->create_links();
            $this->data['paginationLink'] = $paginationLink;
        } else {
            $this->data['paginationLink'] = '';
            $condition = array();

            $this->data['trip_detail_list'] = $this->app_model->get_all_details(DRIVER_NOTIFY, array("_id" => array('$in' => $delayed_trip_ids)), array('_id' => 'DESC'));
        }

        $this->data["page_type"] = "breakdown_vehicles";

        $this->load->view(OPERATOR_NAME . '/trip/notification_trip_list_view', $this->data);
    }

    public function vechicle_current_location() {




        if ($this->checkLogin('O') == '') {
            redirect(OPERATOR_NAME);
        } else {


            $current_trip_id = $this->uri->segment(4);

            $current_trip_data_cursor = $this->bus_trip_model->get_all_details(CURRENT_TRIP_DETAILS, array("_id" => new MongoDB\BSON\ObjectId($current_trip_id)));
            $current_trip_details = $current_trip_data_cursor->result();
            $this->data["current_trip_data"] = $current_trip_data_cursor;
            try {
                    $opt=array();
                if(count($current_trip_details)>0){
               $opt["_id"]=$current_trip_details[0]->trip_id;
                } $this->data["trip_data"] = $this->bus_trip_model->get_all_details(BUS_TRIP, $opt);
            } catch (Exception $ex) {
                
            }
            $this->data["heading"] = "Live Tracking of vehicle";
            if (!empty($current_trip_details)) {
                $or_data = array();
                foreach ($current_trip_details as $key => $value) {

                    $a = array('student_pickup_trip_id' => $value->trip_id);
                    $b = array('student_drop_trip_id' => $value->trip_id);
                    array_push($or_data, $a, $b);
                }




                $this->data["student_details"] = $this->app_model->get_selected_fields(STUDENT_DETAILS, array('$or' => $or_data))->result();
            }

            $this->data['route_list'] = $this->bus_route_model->get_all_details(BUS_ROUTE, array("operator_id" => new MongoDB\BSON\ObjectId($this->checkLogin('O'))));

            $this->data['driver_list'] = $this->driver_model->get_all_details(DRIVERS, array("operator_id" => new MongoDB\BSON\ObjectId($this->checkLogin('O'))));

            $this->data['Bus_list'] = $this->bus_details_model->get_all_details(BUS_DETAILS, array("operator_id" => new MongoDB\BSON\ObjectId($this->checkLogin('O'))));

            $this->data['center'] = $this->config->item('latitude') . ',' . $this->config->item('longitude');


            $this->load->view(OPERATOR_NAME . '/trip/live_vechicle_track', $this->data);
        }
    }

    public function live_driver_location() {

        if ($this->checkLogin('O') == '') {
            redirect(OPERATOR_NAME);
        } else {

            $returnArr = "";
            $driver_id = $this->input->post('driver_id');


            if ($driver_id != "") {


                $driverInfo = $this->app_model->get_selected_fields(DRIVERS, array('_id' => new MongoDB\BSON\ObjectId($driver_id)), array('loc'))->result();
                if (isset($driverInfo[0]->loc)) {


                    $returnArr = $driverInfo[0]->loc;
                } else {


                    $returnArr = "location_unknows";
                }
            }



            $json_encode = json_encode($returnArr, JSON_PRETTY_PRINT);
            echo $this->cleanString($json_encode);
        }
    }

    public function delete_trip() {
        if ($this->checkLogin('O') == '') {
            redirect(OPERATOR_NAME);
        } else {












            $current_trip_id = new MongoDB\BSON\ObjectId($this->uri->segment(4));





            ////////////checking for live trips





            $current_trip_data = $this->app_model->get_all_details(CURRENT_TRIP_DETAILS, array("trip_id" => new MongoDB\BSON\ObjectId($current_trip_id)), array('trip_start_time' => -1), 1);



            $current_trip_data = $current_trip_data->result();





            if (!is_null($current_trip_data[0]->trip_start_time)) {

                if (strtotime(date('Y-m-d', $current_trip_data[0]->trip_start_time->toDateTime()->getTimestamp())) == strtotime(date("Y-m-d")) && $current_trip_data[0]->trip_action == "start") {

                    $this->setErrorMessage('error', 'You can not delete live trips');
                    redirect(OPERATOR_NAME . '/trip/trip_list');
                } else {
                    $condition = array('_id' => $current_trip_id);

                    $this->app_model->commonDelete(BUS_TRIP, $condition);
                    $this->setErrorMessage('success', 'Trip deleted successfully');
                    redirect(OPERATOR_NAME . '/trip/trip_list_view', $this->data);
                }
            } else {


















                $student_details = $this->bus_trip_model->get_all_details(STUDENT_DETAILS, array('$or' => array(array('student_pickup_trip_id' => $current_trip_id), array('student_drop_trip_id' => $current_trip_id))));

                $condition = array('_id' => $current_trip_id);

                if ($student_details->num_rows() == 0) {
                    $this->app_model->commonDelete(BUS_TRIP, $condition);
                    $this->setErrorMessage('success', 'Trip deleted successfully');
                } else {



                    $errMsg = "The trip is assigned to " . $student_details->num_rows() . " students";






                    $this->setErrorMessage('error', $errMsg);
                }



                redirect(OPERATOR_NAME . '/trip/trip_list_view', $this->data);
            }
        }
    }

}

/* End of file trip.php */
/* Location: ./application/controllers/operator/trip.php */