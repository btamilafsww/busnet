<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Driver_welcome_mail extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $user_data = $this->input->post('user_data');
        
        if (isset($user_data) && $user_data != '') {
            $this->mail_model->send_driver_register_confirmation_mail($user_data);
        } else {
            echo 'Some Parameter Missing';
        }
    }

}

/* End of file generate_invoice.php */
/* Location: ./application/controllers/generate_invoice.php */