<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * Sms related functions
 * @author Katenterprise
 *
 */

class Sms_twilio extends MY_Controller { 
	function __construct(){
        parent::__construct();  error_reporting(-1);
		$this->load->helper(array('cookie','date','form','email'));
		$this->load->library(array('encrypt','form_validation','twilio'));		
		$this->data['loginCheck'] = $this->checkLogin('U');  
		$this->load->model(array('user_model','driver_model','app_model'));
    }
    
  
	/** 
	 * 
	 * Send Otp Ajax function 
	 */
	function send_otp(){
		$returnArr['status'] = '0';
		$returnArr['otp'] = '0';
		$otp_phone = $this->input->post('otp_phone');
		$phone_code = $this->input->post('phone_code');
		$riderId = $this->input->post('riderId');
		$checkMob = $this->user_model->get_selected_fields(DRIVERS,array('dail_code' => $phone_code,'mobile_number' => $otp_phone),array(),array('_id'));
		if($checkMob->num_rows() > 0){
			$returnArr['status'] = '2';
		} else {
		  if (substr($phone_code, 0, 1) == '+') {
            $phone_code = $phone_code;
			} else {
				$phone_code = '+' . $phone_code;
			}
			if ($otp_phone != '' && $phone_code != '') {
			 
				
				
				$otp_number = $this->user_model->get_random_string(6);
            
				if(isset($otp_phone)&&isset($phone_code)){
					$session_data=array(APP_NAME.'otp_phone_number'=>$otp_phone,APP_NAME.'otp_country_code'=>$phone_code,'isVerified'=>'false');
					$this->session->set_userdata($session_data);
				}
				$this->session->set_userdata(APP_NAME.'sms_otp', $otp_number);
				
		      $this->sms_model->opt_for_registration($phone_code, $otp_phone, $otp_number,$this->data['langCode']);
				if ($riderId != '') {
					try {
						$condition = array('_id' => new MongoDB\BSON\ObjectId(trim($riderId)));
						$this->user_model->update_details(DRIVERS, array('mobile_otp' => $otp_number), $condition);
					} catch (MongoException $ex) {
					}
				}
				$returnArr['otp_number'] = (string) $otp_number;
			
				$mode = $this->config->item('twilio_account_type');
				$returnArr['mode'] = $mode;
				if( $mode == 'sandbox') $returnArr['otp'] = $otp_number;
				$returnArr['status'] = '1';
			}
		}
		echo json_encode($returnArr);
		
	}
	
	
	function otp_verification(){
		$returnArr['status'] = '0';
		$otp = $this->input->post('otp');
		if ((string)$otp == (string)$this->session->userdata(APP_NAME.'sms_otp')) {
		$this->session->set_userdata('isVerified','true');
			$returnArr['status'] = '1';
		}
		echo json_encode($returnArr);
	}
	
	function check_is_valid_otp_fields(){
       	   
			$otp_phone = $this->input->post('otp_phone');
			$phone_code = $this->input->post('phone_code');
			$stored_mobile_number=$this->session->userdata(APP_NAME.'otp_phone_number');
			$stored_country_code=$this->session->userdata(APP_NAME.'otp_country_code');
			$isVerified=$this->session->userdata('isVerified');
			
			if($stored_mobile_number==$otp_phone && $stored_country_code==$phone_code && $isVerified=='true'){
			echo json_encode('success');
			}else{
			echo json_encode('error');
			}
	}



/////////////otp verification and login

	function otp_verification_driver_login(){

		$returnArr['status'] = '0';

		$otp = $this->input->post('otp');
		$phone_code=$this->input->post('phone_code');

		$phone_number=$this->input->post('phone_number');

	
		
        $returnArr['response'] = '';
       				 try {
            


					        $checkDriver = $this->driver_model->get_selected_fields(DRIVERS, array('mobile_number' => $phone_number,'dail_code' => $phone_code ), array('email', 'user_name', 'phone_number','push_notification','status'));
					      
					      	/*$email = $checkDriver[0]->email;*/

					      

					        $gcm_id = $this->input->post('gcm_id');
					        $deviceToken = (string) $this->input->post('deviceToken');

					        if (is_array($this->input->post())) {
					            $chkValues = count(array_filter($this->input->post()));
					        } else {
					            $chkValues = 0;
					        }
					        
								if ($checkDriver->row()->status == 'Active') {
												$push_data = array();
												$key = '';
												if ($gcm_id != "") {
													$key = $gcm_id;
													$push_data = array('push_notification.key' => $gcm_id, 'push_notification.type' => 'ANDROID');
												} else if ($deviceToken != "") {
													$key = $deviceToken;
													$push_data = array('push_notification.key' => $deviceToken, 'push_notification.type' => 'IOS');
												}
												
												$is_alive_other = "No";
												if (isset($checkDriver->row()->push_notification)) {
													if ($checkDriver->row()->push_notification['type'] != '') {
														if ($checkDriver->row()->push_notification['type'] == "ANDROID") {
															$existingKey = $checkDriver->row()->push_notification["key"];
														}
														if ($checkDriver->row()->push_notification['type'] == "IOS") {
															$existingKey = $checkDriver->row()->push_notification["key"];
														}
														if ($existingKey != $key) {
															$is_alive_other = "Yes";
														}
													}
												}
												$returnArr['is_alive_other'] = (string) $is_alive_other;
												
												if (!empty($push_data)) {
													$this->driver_model->update_details(DRIVERS, array('push_notification.key' => '', 'push_notification.type' => ''), $push_data);
													$this->driver_model->update_details(DRIVERS, $push_data, array('_id' => new MongoDB\BSON\ObjectId($checkDriver->row()->_id)));
												}
												
												$returnArr['status'] = '1';
												$returnArr['response'] = $this->format_string('You are Logged In successfully', 'you_logged_in');
												$driverVal = $this->driver_model->get_selected_fields(DRIVERS, array('_id' => new MongoDB\BSON\ObjectId($checkDriver->row()->_id)), array('operator_id','email', 'image', 'driver_name', 'push_notification', 'password'));
												if (isset($driverVal->row()->image)) {
													if ($driverVal->row()->image == '') {
														$driver_image = USER_PROFILE_IMAGE_DEFAULT;
													} else {
														$driver_image = USER_PROFILE_IMAGE . $driverVal->row()->image;
													}
												} else {
													$driver_image = USER_PROFILE_IMAGE_DEFAULT;
												}
											
												$returnArr['driver_image'] = (string) base_url() . $driver_image;
												$returnArr['driver_id'] = (string) $checkDriver->row()->_id;
												$returnArr['driver_name'] = (string) $driverVal->row()->driver_name;
												$returnArr['sec_key'] = md5((string) $driverVal->row()->_id);
												$returnArr['email'] = (string) $driverVal->row()->email;



					                       
					                            
					                            $logo=$this->app_model->get_all_details(OPERATORS, array('_id' => new MongoDB\BSON\ObjectId($driverVal->row()->operator_id) ))->result();

					                            if(isset($logo[0]->logo_image))
					                            {
					                                            if($logo[0]->logo_image == "")
					                                            {
					                                            $logo_name="8033804eb19cd93ba13fa1ab69a4cca5(1).png";   
					                                            }
					                                            else
					                                            {

					                                            $logo_name=$logo[0]->logo_image;   
					                                            }    

					                            }
					                            else
					                            {
					                            $logo_name="8033804eb19cd93ba13fa1ab69a4cca5(1).png";
					                            }    

					                            $returnArr['op_logo'] = base_url().'images/logo/'.$logo_name;

												
												$returnArr['key'] = (string) $key;
											} else {
												$returnArr['response'] = $this->format_string('Your account have not been activated yet', 'driver_account_not_activated');
											} 
										
					    } catch (MongoException $ex) {
					        $returnArr['response'] = $this->format_string('Error in connection', 'error_in_connection');
					    }
					    $json_encode = json_encode($returnArr, JSON_PRETTY_PRINT);
					    echo $this->cleanString($json_encode);









		
	}
	




	/** 
	* 
	* Send Otp Ajax function driver login
	*/
	function send_otp_driver_login(){
		$returnArr['status'] = '0';
		$returnArr['otp'] = '0';
		$otp_phone = $this->input->post('otp_phone');
		$phone_code = $this->input->post('phone_code');
		$riderId = $this->input->post('riderId');
		$checkMob = $this->user_model->get_selected_fields(DRIVERS,array('dail_code' => $phone_code,'mobile_number' => $otp_phone),array(),array('_id'));
		
		
		if($checkMob->num_rows() == 0){
			$returnArr['status'] = '2';
		} else {
		  if (substr($phone_code, 0, 1) == '+') {
            $phone_code = $phone_code;
			} else {
				$phone_code = '+' . $phone_code;
			}
			if ($otp_phone != '' && $phone_code != '') {
			 
				
				
				$otp_number = $this->user_model->get_random_string(6);
            
				if(isset($otp_phone)&&isset($phone_code)){
					$session_data=array(APP_NAME.'otp_phone_number'=>$otp_phone,APP_NAME.'otp_country_code'=>$phone_code,'isVerified'=>'false');
					$this->session->set_userdata($session_data);
				}
				$this->session->set_userdata(APP_NAME.'sms_otp', $otp_number);
				
		      $this->sms_model->driver_login_otp($phone_code, $otp_phone, $otp_number,$this->data['langCode']);
				if ($riderId != '') {
					try {
						$condition = array('_id' => new MongoDB\BSON\ObjectId(trim($riderId)));
						$this->user_model->update_details(DRIVERS, array('mobile_otp' => $otp_number), $condition);
					} catch (MongoException $ex) {
					}
				}
			
				$mode = $this->config->item('twilio_account_type');
				$returnArr['mode'] = $mode;
				if( $mode == 'sandbox') $returnArr['otp'] = $otp_number;
				$returnArr['status'] = '1';
			}
		}
		echo json_encode($returnArr);
		
	}






	/** 
	* 
	* Send Otp Ajax function driver login
	*/
	function send_otp_user_login(){
		$returnArr['status'] = '0';
		$returnArr['otp'] = '0';
		$otp_phone =$this->input->post('otp_phone');
		$phone_code = $this->input->post('phone_code');
		//$riderId = $this->input->post('riderId');
		$checkMob = $this->user_model->get_selected_fields(USERS,array('country_code' => $phone_code,'phone_number' => $otp_phone),array(),array('_id'));
		
		
		if($checkMob->num_rows() == 0){
			$returnArr['status'] = '2';
		} else {
		  if (substr($phone_code, 0, 1) == '+') {
            $phone_code = $phone_code;
			} else {
				$phone_code = '+' . $phone_code;
			}
			if ($otp_phone != '' && $phone_code != '') {
			 
				
				
				$otp_number = $this->user_model->get_random_string(6);
            
				if(isset($otp_phone)&&isset($phone_code)){
					$session_data=array(APP_NAME.'otp_phone_number'=>$otp_phone,APP_NAME.'otp_country_code'=>$phone_code,'isVerified'=>'false');
					$this->session->set_userdata($session_data);
				}
			$this->session->set_userdata(APP_NAME.'sms_otp', $otp_number);

			$returnArr['otp'] = (string) $otp_number;
				
			//$this->app_model->commonInsertUpdate(OTP_CHECKER,'insert',$excludeArr,$dataArr,$condition); 
				
		    $this->sms_model->driver_login_otp($phone_code, $otp_phone, $otp_number,$this->data['langCode']);
				/*if ($riderId != '') {
					try {
						$condition = array('_id' => new MongoDB\BSON\ObjectId(trim($riderId)));
						$this->user_model->update_details(USERS, array('mobile_otp' => $otp_number), $condition);
					} catch (MongoException $ex) {
					}
				}*/
			
				$mode = $this->config->item('twilio_account_type');
				$returnArr['mode'] = $mode;
				if( $mode == 'sandbox') $returnArr['otp'] = $otp_number;
				$returnArr['status'] = '1';
			}
		}
		echo json_encode($returnArr);
		
	}




	////////////otp verification and login

	function otp_verification_user_login(){

		$returnArr['status'] = '0';
		 $returnArr['response'] = '0';
		$otp = $this->input->post('otp');

		$phone_code= $this->input->post('phone_code');

		$phone_number=$this->input->post('phone_number');

		$gcm_id = $this->input->post('gcm_id');
		$deviceToken = $this->input->post('deviceToken');
		$latitude = $this->input->post('lat');
		$longitude = $this->input->post('lon');
			
			

		
		
       

        		try{



				        if (is_array($this->input->post())) {
				                $chkValues = count(array_filter($this->input->post()));
				            } else {
				                $chkValues = 0;
				            }

				            if ($chkValues >= 2) {
				               
									$checkAccount=$this->user_model->get_selected_fields(USERS, array('phone_number' =>$phone_number,'country_code' => $phone_code ),array('phone_number'));
									if($checkAccount->num_rows() == 1)  {
										$checkUser = $this->user_model->get_selected_fields(USERS, array('phone_number' =>$phone_number,'country_code' => $phone_code), array('email', 'user_name', 'phone_number', 'status','push_type','push_notification_key'));
										if ($checkUser->num_rows() == 1) {
											if ($checkUser->row()->status == "Active") {

				                                $all_push_data=$checkUser->result();

				                                if(isset($all_push_data[0]->push_notification_key))
				                                {    
				                                $all_push_data= $all_push_data[0]->push_notification_key;
				                                }
				                                else
				                                {
				                                   $all_push_data=[];   
				                                }    
				                                if(!is_array ($all_push_data))
				                                  {
				                                         $all_push_data=[]; 
				                                  }  


												$push_data = array();

												$key_id = '';
												if ($gcm_id != "") {
													$key_id = $gcm_id;
				                                 
				                                    array_push($all_push_data, array('gcm_id' => $gcm_id, 'push_type' => 'ANDROID'));

													$push_data =  array('push_notification_key' => $all_push_data); 
													$push_update_data = array('push_notification_key' => array());
												}
												if ($deviceToken != "") {
													$key_id = $deviceToken;

				                                     array_push($all_push_data, array('ios_token' => $deviceToken, 'push_type' => 'IOS'));

													$push_data = array('push_notification_key' => $all_push_data);
													$push_update_data = array('push_notification_key' => array());
												}
												/* if($key==""){
												  $this->user_model->update_details(USERS,array('push_type'=>''),array('_id'=>new MongoDB\BSON\ObjectId($checkUser->row()->_id)));
												  } 
												  */
												
												$is_alive_other = "No";

				                                $already_presesnt="No";

												if (isset($checkUser->row()->push_notification_key)) {



													if ($checkUser->row()->push_notification_key != '') {


				                                        foreach ($checkUser->row()->push_notification_key as $key => $value) 
				                                        {
				                                                  
				                                                  if(isset($value['push_type'])) 
				                                                  {  
				                                                    if($value['push_type'] == "ANDROID") {

				                                                        $existingKey = $value["gcm_id"];


				                                                    }
				                                                    if($value['push_type']== "IOS") {
				                                                         $existingKey = $value["ios_token"];
				                                                    
				                                                    }     


				                                                  } 
				                                                  else{
				                                                      $existingKey="";
				                                                   } 

				                                               /* if ($existingKey != $key_id) {
				                                                    
				                                                     $is_alive_other = "Yes";

				                                                    }*/

				                                                    if($existingKey == $key_id)
				                                                     {

				                                                        $already_presesnt ="Yes";
				                                                        break;
				                                                     }   


				                                        }

											
													}
												}
				                                  $is_alive_other = "No";
												$returnArr['is_alive_other'] = (string) $is_alive_other;
												


												if (!empty($push_data)) {

				                                  if($already_presesnt != "Yes")
				                                  {  
													$this->user_model->update_details(USERS, $push_update_data, $push_data);
													$this->user_model->update_details(USERS, $push_data, array('_id' => new MongoDB\BSON\ObjectId($checkUser->row()->_id)));
				                                   } 
												}


												$returnArr['status'] = '1';
												$returnArr['message'] = $this->format_string('You are Logged In successfully', 'you_logged_in');
												$userVal = $this->user_model->get_selected_fields(USERS, array('_id' => new MongoDB\BSON\ObjectId($checkUser->row()->_id)), array('email', 'image', 'user_name', 'country_code', 'phone_number', 'referral_code', 'push_notification_key.gcm_id', 'password'));
												if ($userVal->row()->image == '') {
													$user_image = USER_PROFILE_IMAGE_DEFAULT;
												} else {
													$user_image = USER_PROFILE_IMAGE . $userVal->row()->image;
												}
												$returnArr['user_image'] = base_url() . $user_image;
												$returnArr['user_id'] = (string) $checkUser->row()->_id;
												$returnArr['user_name'] = $userVal->row()->user_name;
												$returnArr['email'] = $userVal->row()->email;
												$returnArr['country_code'] = $userVal->row()->country_code;
												$returnArr['phone_number'] = $userVal->row()->phone_number;
												$returnArr['referal_code'] = $userVal->row()->referral_code;
												$returnArr['sec_key'] = md5((string) $checkUser->row()->_id);
												$returnArr['key'] = $key;
												
												if(isset($checkUser->row()->lang_code)){
														$returnArr['lang_code'] = $checkUser->row()->lang_code;
												}else{
														$returnArr['lang_code'] = $this->temp_lang;
												}	

												$walletDetail = $this->user_model->get_selected_fields(WALLET, array('user_id' => new MongoDB\BSON\ObjectId($checkUser->row()->_id)), array('total'));
												$avail_amount = 0;
												if (isset($walletDetail->row()->total)) {
													$avail_amount = $walletDetail->row()->total;
												}
												
				                                $wallet_amount = round($avail_amount,2);
				                                $returnArr['wallet_amount'] = (string) number_format($wallet_amount,2);
				                                
												$returnArr['currency'] = (string) $this->data['dcurrencyCode'];
												$location = $this->app_model->find_location(floatval($longitude), floatval($latitude));
									
												if(!empty($location['result'])) {
													$final_cat_list = $location['result'][0]['avail_category'];
												}

												$selected_Category='';
												if(!empty($final_cat_list)) {
													$selected_Category=$final_cat_list[0];
												}
												
												$returnArr['category'] = (string) $selected_Category;
											} else {
												if ($checkUser->row()->status == "Deleted") {
													$returnArr['message'] = $this->format_string("Your account is currently unavailable", "account_currently_unavailbale");
												} else {
													$returnArr['message'] = $this->format_string("Your account has been inactivated", "your_account_inactivated");
												}
											}
										} else {
											$returnArr['message'] = $this->format_string('Please check the email and password and try again', 'please_check_email_and_password');
										}
								 }else {
										$returnArr['message'] = $this->format_string('Your account does not exist', 'account_not_exists');
								 }
				              
				            } else {
				                $returnArr['message'] = $this->format_string("Some Parameters are missing", "some_parameters_missing");
				            }
				        } catch (MongoException $ex) {
				            $returnArr['response'] = $this->format_string("Error in connection", "error_in_connection");
				        }
				        $json_encode = json_encode($returnArr, JSON_PRETTY_PRINT);
				        echo $this->cleanString($json_encode);


	}
	






	 
}

?>