<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/** 
* 
* Remove Rides
* @author Katenterprise
*
**/
 
class Cls_rides extends MY_Controller {
	function __construct(){
        parent::__construct();
		$this->load->helper(array('cookie','date','form','email'));
		$this->load->library(array('encrypt','form_validation'));
		
    }
	
	public function index() {
		$start_time = time()-300;
		$this->cimongo->select(array('ride_id', 'type', 'ride_status', 'user'));
		$this->cimongo->where(array("ride_status" => 'Booked',"type" => 'Now',"booking_information.est_pickup_date"=>array('$lt'=>new \MongoDate($start_time))));
		$this->cimongo->order_by(array('_id' => 'ASC'));
		$res = $this->cimongo->get(RIDES);
		
		echo "<pre>"; print_r($res->result()); 
		
	}
	
	
	
}

/* End of file cls_rides.php */
/* Location: ./application/controllers/cron/cls_rides.php */