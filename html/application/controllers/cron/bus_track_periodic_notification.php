<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/** 
* 
* Billing related functions
* @author Katenterprise
*
**/
 
class bus_track_periodic_notification extends MY_Controller {
	function __construct(){
        parent::__construct();
		$this->load->helper(array('cookie','date','form','email'));
		$this->load->library(array('encrypt','form_validation'));
		$this->load->model('billing_model','app_model');
		$this->load->model('revenue_model');
    }
	




	public function trip_live_notifications()
	{

		$current_trip_details = $this->app_model->get_selected_fields(CURRENT_TRIP_DETAILS,array("trip_action"=> 'start'),array('_id','trip_id','start_lat_long'))->result();

		
		
		$current_time=strtotime(date('H:i'));
		if(!empty($current_trip_details))
		{

				
                foreach ($current_trip_details as $key => $value) {


                   	$trips_data=$this->app_model->get_selected_fields(BUS_TRIP,array( '_id' => $value->trip_id ),array('end_time'))->result();

                   	if(isset($trips_data[0]))
                   	{
                   		$actual_time=strtotime($trips_data[0]->end_time)+60*60;

                 		if($actual_time < $current_time)
                   		{
                   			   $dataArr=array("trip_action"=>"stop" ,"stop_current_date"=>date('Y-m-d'),"stop_lat_long" => $value->start_lat_long,"trip_stop_time" => new mongodate(strtotime(date("Y-m-d H:i:s"))));

                   			   $condition=array('_id'=> $value->_id );

                   				$this->app_model->commonInsertUpdate(CURRENT_TRIP_DETAILS, 'update', $excludeArr, $dataArr, $condition);

                   		}

                                          
                    }

                }

		}	



	}
	
}

/* End of file billing.php */
/* Location: ./application/controllers/cron/billing.php */