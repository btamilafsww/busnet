<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/** 
* 
* Billing related functions
* @author Katenterprise
*
**/
 
class Parent_notification_cron_job extends MY_Controller {
	function __construct(){
        parent::__construct();
		$this->load->helper(array('cookie','date','form','email'));
		$this->load->library(array('encrypt','form_validation'));
		$this->load->model(array('app_model','map_model','bus_trip_model','mail_model','sms_model'));
		
    }
	

    private $repeat_stopper=array();

	

	public function send_notifications()
	{

		$current_trip_details = $this->app_model->get_selected_fields(CURRENT_TRIP_DETAILS,array("trip_action"=> 'start',"start_current_date" => date("Y-m-d") ),array('_id','trip_id','start_lat_long',"driver_id"))->result();

			
		
		if(!empty($current_trip_details))
		{

				
               
		               
				  foreach ($current_trip_details as $key => $value) 
				  {
										

				  						/////////////////////////////////repeater checker table//
										$filterCondition=array('trip_id' => $value->trip_id,'created_date' => date("Y-m-d") );

										$repeat_stopper_table=$this->app_model->get_all_details(PARENT_NOTIFY_CRONER,  $filterCondition)->result();




										$near_pickup_location_repeater_stop=array();

										if(!empty($repeat_stopper_table))
										{
											if(null !== $repeat_stopper_table[0]->near_pickup_sent_student_ids)
											{
											$near_pickup_location_repeater_stop=$repeat_stopper_table[0]->near_pickup_sent_student_ids;
											}
										}

										////////////////driver details getter//

										//$driver_details=$this->app_model->get_all_details(DRIVERS,  array('_id' => $value->driver_id ));	

										////////////////////////////


										///////////trip_details////////

										 $trip_details= $this->app_model->get_all_details(BUS_TRIP, array("_id" => $value->trip_id ))->result();

										///////////////////////////////


										$or_data=array();
										$a=array('student_pickup_trip_id' => $value->trip_id );
										$b=array('student_drop_trip_id' => $value->trip_id );
										array_push($or_data,$a,$b);


										$this->repeat_stopper=array();


										$student_details=$this->app_model->get_all_details(STUDENT_DETAILS,array( '$or' => $or_data  ),array())->result();


										
										foreach ($student_details as $key => $value2) {


												  ////////stop lat_long

		                                        $stops_of_trip=$trip_details[0]->route_details['waypoints_details'];
		                                       
		                                        $bus_to_stop_distancep_id="";
		                                       if($trip_details[0]->trip_type == "pickup")
		                                       {
		                                        $index_id=array_search((string)$value2->student_pickup_bus_stop_id, array_column($stops_of_trip, 'id'));

		                                        $bus_to_stop_id=(string)$value2->student_pickup_bus_stop_id;

		                                       }
		                                       else
		                                       {
		                                       	$index_id=array_search((string)$value2->student_drop_bus_stop_id, array_column($stops_of_trip, 'id'));

		                                       	 $bus_to_stop_id=(string)$value2->student_drop_bus_stop_id;
		                                       }
		                                        
		                                        $student_stop_lat_long=explode(",",$trip_details[0]->route_details['waypoints_details'][$index_id]['waypoint']);	

		                                        $bus_to_stop_distance=$this->map_model->get_stop_distance($student_stop_lat_long,$value->driver_id );

												//////////nearing pickup notification
													echo $value2->student_first_name."-".$bus_to_stop_distance["result"][0]['distance'];
												  if($bus_to_stop_distance["result"][0]['distance'] < 300 && !in_array($value2->_id, $near_pickup_location_repeater_stop) )
												  {



												  	$this->send_nearing_notification($value2->guardian_details,$trip_details[0]->trip_type,$trip_details[0]->trip_name,$bus_to_stop_id);

												  	array_push($near_pickup_location_repeater_stop,$value2->_id);

												  		$excludeArr=array('none');
												  		
												  		
													  	if(!empty($repeat_stopper_table))
													  	{

													  		$dataArr=array('near_pickup_sent_student_ids' => $near_pickup_location_repeater_stop  );	
													  		$condition=array("_id" => $repeat_stopper_table[0]->_id);
													  		
													  		$this->app_model->commonInsertUpdate(PARENT_NOTIFY_CRONER,'update',$excludeArr,$dataArr,$condition);
													  	}
													  	else
													  	{
													  		$dataArr=array('near_pickup_sent_student_ids' => $near_pickup_location_repeater_stop,'trip_id' => $value->trip_id,'created_date' => date("Y-m-d")  );	
													  		$condition=array();
													  		$this->app_model->commonInsertUpdate(PARENT_NOTIFY_CRONER,'insert',$excludeArr,$dataArr,$condition);
													  	}	



												  }
												



										}







										


				}


               

                		

	    }



	}




	private function send_nearing_notification($guardian_details = array(),$trip_typ = '',$trip_name='',$bus_to_stop_id ="")
	{
			$or_data=array();

			foreach ($guardian_details as $key => $value) {
				$a=array('country_code' => $value['phone_code'],"phone_number" => $value['parent_mobile_number'] );

				array_push($or_data,$a);
			}
			


			


			$user_details=$this->app_model->get_all_details(USERS,array( '$or' => $or_data  ),array())->result(); 

			$message="";

			 if($trip_typ == "pickup")
            {
            	$message="The ".$trip_name." trip bus is nearing pickup location"; 
            }
            else
            {	
           		$message= "The ".$trip_name." trip bus is nearing drop location"; 
           	}
                                                                            
        
           	$phone_numbers_send=array();

           	


			foreach ($user_details as $key => $value) {


											if(!in_array( $value->country_code.$value->phone_number.'-'.$bus_to_stop_id , $this->repeat_stopper))
                                             {

                                                    ///////////push notification
                                             		 if(isset($user_details->notifiy))
                                                      {
                                                        if(isset($value->push_notification_key))
                                                        {
                                                            


                                                            foreach ($value->push_notification_key as $key => $user_puser) 
                                                            {
                                                                if(isset($user_puser['push_type']))
                                                                  {  

                                                                      if($user_puser['push_type'] == "ANDROID"  && $user_puser['gcm_id'] != "")
                                                                         {   
                                                                            

                                                                            $regIds=$user_puser['gcm_id'];

                                                                            if($trip_typ == "pickup")
                                                                            {
                                                                            	$msg=array('message' => "The ".$trip_name." trip bus is nearing pickup location"); 
                                                                            }
                                                                            else
                                                                            {	
                                                                           	    $msg=array('message' => "The ".$trip_name." trip bus is nearing drop location"); 
                                                                           	}
                                                                            
                                                                            $app="USER";    
                                                                              $type="ANDROID";    

                                                                            $this->simple_push_notification($regIds, $msg, $app,$type);

                                                                         }
                                                                         elseif ($user_puser['push_type'] == "IOS"  && $user_puser['ios_token'] != "") {
                                                                         
                                                                            

                                                                            $regIds=$user_puser['ios_token'];
                                                                            if($trip_typ == "pickup")
                                                                            {
                                                                            	$msg=array('message' => "The ".$trip_name." trip bus is nearing pickup location"); 
                                                                            }
                                                                            else
                                                                            {	
                                                                            $msg=array('message' => "The ".$trip_name." trip bus is nearing drop location"); 
                                                                           	}
                                                                             $app="USER";    
                                                                              $type="IOS";     

                                                                            $this->simple_push_notification($regIds, $msg, $app,$type);
                                                                           

                                                                           

                                                                         }


                                                                    }     


                                                            }


                                                                       
                                                            

                                                        } 









                                                        ////////phone notification

                                                        if( $value->notifiy['sms_status_notify'] == "yes"  )
                                                        {   

                                                              
                                                        		 $from = '';
                              
                              								 $this->sms_model->send_common_sms_bulk($from,$value->country_code.$value->phone_number, $message);

                                                        		

                                                                

                                                               // array_push($phone_numbers_send, $value->country_code.$value->phone_number); 


                                                        }





                                                        ///////////email notification

                                                        if($value->notifiy['email_status_notify'] == "yes" )
                                                        {
                                                               


                                                                $this->mail_model->send_user_near_mail($value->email,$message);

                                                                //$this->mail_model->send_driver_delay_mail($data_guardian['parent_email'],$trip_name,$delay_message);


                                                               



                                                        }



                                           }
                                           else 
                                           {            

                                                     
                                                     ///////////push notification

                                                        if(isset($value->push_notification_key))
                                                        {
                                                            


                                                            foreach ($value->push_notification_key as $key => $user_puser) 
                                                            {
                                                                if(isset($user_puser['push_type']))
                                                                  {  

                                                                      if($user_puser['push_type'] == "ANDROID"  && $user_puser['gcm_id'] != "")
                                                                         {   
                                                                            

                                                                            $regIds=$user_puser['gcm_id'];
                                                                            if($trip_typ == "pickup")
                                                                            {
                                                                            	$msg=array('message' => "The ".$trip_name." trip bus is nearing pickup location"); 
                                                                            }
                                                                            else
                                                                            {	
                                                                            $msg=array('message' => "The ".$trip_name." trip bus is nearing drop location"); 
                                                                           	}
                                                                            
                                                                            $app="USER";    
                                                                              $type="ANDROID";    

                                                                            $this->simple_push_notification($regIds, $msg, $app,$type);

                                                                         }
                                                                         elseif ($user_puser['push_type'] == "IOS"  && $user_puser['ios_token'] != "") {
                                                                         
                                                                          

                                                                            $regIds=$user_puser['ios_token'];
                                                                            
                                                                            if($trip_typ == "pickup")
                                                                            {
                                                                            	$msg=array('message' => "The ".$trip_name." trip bus is nearing pickup location"); 
                                                                            }
                                                                            else
                                                                            {	
                                                                            $msg=array('message' => "The ".$trip_name." trip bus is nearing drop location"); 
                                                                           	}
                                                                                

                                                                             $app="USER";    
                                                                              $type="IOS";    

                                                                            $this->simple_push_notification($regIds, $msg, $app,$type);
                                                                           

                                                                           

                                                                         }


                                                                    }     


                                                            }


                                                                    
                                                            

                                                        } 



                                                        	 $from = '';
                              
                              								 $this->sms_model->send_common_sms_bulk($from,$value->country_code.$value->phone_number, $message);

                                                        		
                                                              
                                                         	// array_push($phone_numbers_send,$value->country_code.$value->phone_number ); 
                                                       


                                                              $this->mail_model->send_user_near_mail($value->email,$message);  


                                                        

                                               
                                           } 
                                           array_push($this->repeat_stopper, $value->country_code.$value->phone_number.'-'.$bus_to_stop_id); 
                                      }     
                              }

                              
				
	}


	
}

/* End of file billing.php */
/* Location: ./application/controllers/cron/billing.php */