<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start();
/**
*
*	Operator Management for admin 
* 
*	@package	CI
*	@subpackage	Controller
*	@author	Katenterprise
*
**/
class Transporter extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->helper(array('cookie','date','form'));
		$this->load->library(array('encrypt','form_validation'));
		$this->load->model(array('app_model', 'mail_model', 'sms_model','user_model'));
		if ($this->checkPrivileges('transporter',$this->privStatus) == FALSE && $this->uri->segment(3) != "signup_institution"){
				redirect('admin');
		}
		$c_fun = $this->uri->segment(3);
		$restricted_function = array( 'delete_transporter', 'change_operator_status_global','change_password');
		if (in_array($c_fun, $restricted_function) && $this->data['isDemo'] === TRUE) {
			$this->setErrorMessage('error', 'You are in demo version. you can\'t do this action.','admin_template_demo_version');
			redirect($_SERVER['HTTP_REFERER']);
			die;
		}
	}

	/**
	* 
	* it display the transporter list
	*
	* @return HTTP REDIRECT, transporter list page
	*
	**/	
	public function index(){
			if ($this->checkLogin('A') == ''){
					redirect('admin');
			}else {
					redirect('admin/transporter/display_transporter_list');
			}
	}

	/**
	* 
	* it display the transporter list
	*
	* @return HTML, transporter list page
	*
	**/	
	public function display_transporter_list(){
			if ($this->checkLogin('A') == ''){
					redirect('admin');
			}else {


					$this->data['heading'] = 'Transporter list';
					$condition = array();
					//$this->data['operatorsList'] = $operatorsList = $this->app_model->get_all_details(TRANSPORT,$condition);



					$this->data['transporter_switch']=$this->session->userdata(APP_NAME.'_session_admin_transporter_switch');

					
					$option=array(
									    array(
									        '$lookup' => array(
									            "from" => STUDENT_DETAILS,
									            "localField" => "_id",
									            "foreignField" => "operator_id",
									            "as" => "student_details"
									        )
									    ),
									    
										  array( '$project' => array(
										   "_id" => 1,
										  "operator_name" => 1,
										  "email" => 1,
										  "status"  => 1,
										  "created"  => 1,
										  "password"  => 1,
										  "dail_code"  => 1,
										  "mobile_number"  => 1,
										  "session_switch"  => 1,
										    "student_details" => 1,
										    "student_details_count"=> array( '$size' => '$student_details' )
										  )),
										  array(
											    '$sort' => array(
											       'created' => -1
											    )
											  )
									);
                                        $cursor=$this->cimongo->aggregate(TRANSPORT,$option);
					$this->data['operatorsList']  = $cursor->toArray();

					$this->load->view('admin/transporter/display_transporter_list',$this->data);
			}
	}

	/**
	* 
	* it add or edit the transporter information page
	*
	* @param string $operator_id  transporter MongoID
	* @return HTML ,Add/edit transporter information page
	*
	**/	
	public function add_edit_transporter_form(){
			if ($this->checkLogin('A') == ''){
					redirect('admin');
			}else {
					$operator_id = $this->uri->segment(4,0);
					$form_mode=FALSE;
					if ($this->lang->line('admin_menu_add_transporter') != '')
							$heading = stripslashes($this->lang->line('admin_menu_add_transporter'));
					else $heading = 'Add Transport Manager';
					$this->data['operatorsList'] = $this->app_model->get_selected_fields(TRANSPORT,array('status' => 'Active'),array('_id','operator_name','email'));
					$this->data['locationList'] = $this->app_model->get_all_details(LOCATIONS, array('status' => 'Active'), array('city' => 'ASC'));
					if($operator_id != ''){
							$condition = array('_id' => new MongoDB\BSON\ObjectId($operator_id));
							$this->data['operator_details'] = $this->app_model->get_all_details(TRANSPORT,$condition);
							if ($this->data['operator_details']->num_rows() != 1){
									redirect('admin/transporter/display_operators_list');
							}
							$form_mode=TRUE;
							if ($this->lang->line('admin_menu_edit_transporter') != '')
									$heading = stripslashes($this->lang->line('admin_menu_edit_transporter'));
							else $heading = 'Edit Transport Manager';
					}
					$this->data['form_mode'] = $form_mode;
					$this->data['heading'] = $heading;
					$this->load->view('admin/transporter/add_edit_transporter',$this->data);
			}
	}
	/**
	* 
	* it insert or update the transporter information
	*
	* @param string $operator_id  transporter MongoID
	* @param string $email  transporter email
	* @param string $dail_code  transporter country code
	* @param string $mobile_number  transporter Mobile Number
	* @param string $operator_location  transporter Location
	* @return HTML ,operator list page
	*
	**/

	public function insertEditTransporter(){ 
		
			if ($this->checkLogin('A') == ''){
					redirect('admin');
			}else {

					$lat_long=array('lat' =>$this->input->post('input_lat'),'lng'=>$this->input->post('input_long') );


					$operator_id = $this->input->post('operator_id');						
					$email = $this->input->post('email');
					$dail_code = $this->input->post('dail_code');
					$mobile_number = $this->input->post('mobile_number');
					$operator_location = $lat_long;
			
					$returnUrl = 'admin/transporter/add_edit_transporter_form';
					if($operator_id != ''){
							$returnUrl =  'admin/transporter/add_edit_transporter_form/'.$operator_id;
					}
				
					if($operator_id==''){
							$condition=array('email' => $email);
					}else{
							$condition=array('email' => $email, '_id' => array('$ne' => new MongoDB\BSON\ObjectId($operator_id)));
					}
					$duplicate_email = $this->app_model->get_all_details(TRANSPORT,$condition);
					if ($duplicate_email->num_rows() > 0){
							$this->setErrorMessage('error','Email address already exists','driver_email_already');
							redirect($returnUrl);
					}		
				
					if($operator_id == ''){
						$condition = array('dail_code' => $dail_code,'mobile_number' => $mobile_number);
					}else{
						$condition = array('dail_code' => $dail_code, 'mobile_number' => $mobile_number, '_id' => array('$ne' => new MongoDB\BSON\ObjectId($operator_id)));
					}
					$duplicate_phone = $this->app_model->get_all_details(TRANSPORT,$condition);
					if ($duplicate_phone->num_rows() > 0){
							$this->setErrorMessage('error','Mobile number already exists','operators_mobile_already_exists');
							redirect($returnUrl);
					}			

					$excludeArr = array("status", "operator_id", "mobile_number", "dail_code", "operator_location", "address", "country", "state", "city", "postal_code",'session_switch');
				
					$addressArr['address'] = array('address' => $this->input->post('address'),
																			'country' => $this->input->post('country'),
																			'state' => $this->input->post('state'),
																			'city' => $this->input->post('city'),
																			'postal_code' => $this->input->post('postal_code')
																	);
			
					$password = $this->get_rand_str();
				
					if ($this->input->post('status') != ''){
							$status = 'Active';
					} else {
							$status = 'Inactive';
					}	
				
					$operator_data = array(
											'status' => $status,
											'created' => date('Y-m-d H:i:s'),
											'password' => md5($password),
											'dail_code' => (string) $this->input->post('dail_code'),
											'mobile_number' => (string) $this->input->post('mobile_number'),
											'session_switch' => (string) $this->input->post('session_switch')
										);
					
				  $operator_data['operator_location'] =$operator_location;		
										
				  $dataArr = array_merge($operator_data,$addressArr);
				  
					if($operator_id == ''){
							$this->app_model->commonInsertUpdate(TRANSPORT,'insert',$excludeArr,$dataArr);
							$this->setErrorMessage('success','Transport Operator added successfully','transporter_added_successfully');
							
							$last_insert_id = $this->cimongo->insert_id();
							$this->mail_model->send_operator_register_confirmation_mail($last_insert_id,$password);
				
					}	else	{
							$condition=array('_id'=>new MongoDB\BSON\ObjectId($operator_id));
							unset($dataArr['password']);
							unset($dataArr['created']);
							$this->app_model->commonInsertUpdate(TRANSPORT,'update',$excludeArr,$dataArr,$condition);
							$this->setErrorMessage('success','Transport Operator details updated successfully','transporter_updated_successfully');
					}
					redirect('admin/transporter/display_transporter_list');
			}
	}
	/**
	* 
	* it delete the operator information
	*
	* @param string $operator_id  Operator MongoID
	* @return HTTP REDIRECT ,operator list page
	*
	**/
	public function delete_Transporter(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		} else {
			$operator_id = $this->uri->segment(4,0);
			$condition = array('_id' => new MongoDB\BSON\ObjectId($operator_id));
			$operator_id_condition = array('operator_id' => new MongoDB\BSON\ObjectId($operator_id));
			
			$operator_info = $this->app_model->get_all_details(TRANSPORT,$condition);
			$this->app_model->commonDelete(TRANSPORT, $condition);

			$this->app_model->commonDelete(BUS_DETAILS, $operator_id_condition);
			$this->app_model->commonDelete(BUS_ROUTE, $operator_id_condition);
			$this->app_model->commonDelete(BUS_TRIP, $operator_id_condition);
			$this->app_model->commonDelete(CLASS_DETAILS, $operator_id_condition);
			$this->app_model->commonDelete(DRIVERS, $operator_id_condition);
			$this->app_model->commonDelete(OTHER_OPERATOR, $operator_id_condition);			
			$this->app_model->commonDelete(SECTION_DETAILS, $operator_id_condition);
			$this->app_model->commonDelete(STUDENT_DETAILS, $operator_id_condition);

			$this->setErrorMessage('success', 'Transport Operator deleted successfully','institution_status_change_sucess');
			redirect('admin/transporter/display_transporter_list');

		}
	}
	/**
	* 
	* it change the operator status 
	*
	* @param string $operator_id  Operator MongoID
	* @param string $mode  Active/Inactive
	* @return HTTP REDIRECT ,operator list page
	*
	**/
	public function change_Transporter_status(){
			if ($this->checkLogin('A') == ''){
					redirect('admin');
			}else {
					$mode = $this->uri->segment(4,0);
					$operator_id = $this->uri->segment(5,0);
					$status = ($mode == '0')?'Inactive':'Active';
					$newdata = array('status' => $status);
					$condition = array('_id' => new MongoDB\BSON\ObjectId($operator_id));
					$this->app_model->update_details(TRANSPORT,$newdata,$condition);
					$this->setErrorMessage('success', 'Transport Operator Status Changed Successfully','admin_transport_status_change');
					redirect('admin/transporter/display_transporter_list');
			}
	}
	/**
	* 
	* it change the operator status bulk
	*
	* @param string $checkbox_id  Operator MongoID ARRAY[]
	* @param string $statusMode  Active/Inactive  ARRAY[]
	* @return HTTP REDIRECT ,operator list page
	*
	**/
	public function change_Transporter_status_global(){
			if (count($_POST['checkbox_id']) > 0 && $_POST['statusMode'] != '') {
		$this->app_model->activeInactiveCommon(TRANSPORT, '_id');
		if (strtolower($_POST['statusMode']) == 'delete') {
			$this->setErrorMessage('success', 'Transport Operator records deleted successfully','admin_transport_record_delete');
		} else {
			$this->setErrorMessage('success', 'Transport Operator status changed successfully','admin_transport_status_change');
		}
		redirect('admin/transporter/display_transporter_list');
	}
	}
	
	/**
	* 
	* it display the operator information details
	*
	* @param string $operatorId  Operator MongoID
	* @return HTML ,operator information page
	*
	**/
	public function view_Transporter() {
		if ($this->checkLogin('A') == '') {
			$this->setErrorMessage('error', 'You must login first','admin_driver_login_first');
			redirect('admin');
		}
		$operatorId = $this->uri->segment(4);
		if ($this->lang->line('transport_view_details') != '') 
				$this->data['heading']= stripslashes($this->lang->line('transport_view_details')); 
		else  $this->data['heading'] = 'View Transport Operator Details';
		$condition = array('_id' => new MongoDB\BSON\ObjectId($operatorId));
		$this->data['operator_details'] = $operator_details = $this->app_model->get_all_details(TRANSPORT, $condition);
		$this->data["transporter_id"]=$operatorId; 
		if ($operator_details->num_rows() == 0) {
			$this->setErrorMessage('error', 'No records found','admin_driver_no_records_found');
			redirect('admin/transporter/display_transporter_list');
		}

		$this->load->view('admin/transporter/view_transporter', $this->data);
	}
	/**
	* 
	* it display the operator password change page
	*
	* @param string $operatorId  Operator MongoID
	* @return HTML ,operator password change page
	*
	**/
	public function change_password_form() {
		if ($this->checkLogin('A') == '') {
			redirect('admin');
		}
		$operatorId = $this->uri->segment(4);
		if ($this->lang->line('transport_change_password') != '') 
				$this->data['heading']= stripslashes($this->lang->line('transport_change_password')); 
		else  $this->data['heading'] = 'Change Transport Operator Password';
		$condition = array('_id' => new MongoDB\BSON\ObjectId($operatorId));
		$this->data['operator_details'] = $operator_details = $this->app_model->get_all_details(TRANSPORT, $condition);
		$this->load->view('admin/transporter/change_password', $this->data);
	}
	/**
	* 
	* it change the operator password information
	*
	* @param string $operatorId  Operator MongoID
	* @param string $new_password  Operator Password
	* @return HTTP REDIRECT ,operator list page
	*
	**/
	public function change_password() {
		if ($this->checkLogin('A') == '' || $this->input->post('new_password') == '') {
			redirect('admin');
		}
		$password = $this->input->post('new_password');
		$operatorId = $this->input->post('operator_id');
		$dataArr = array('password' => md5($this->input->post('new_password')));
		$condition = array('_id' => new MongoDB\BSON\ObjectId($operatorId));
		$operator_details = $this->app_model->update_details(TRANSPORT, $dataArr, $condition);
		$operatorinfo = $this->app_model->get_all_details(TRANSPORT, $condition);
		$this->send_transporter_pwd($password, $operatorinfo);
		$this->setErrorMessage('success', 'Transport Operator password changed and sent to operator successfully');
		redirect('admin/transporter/display_transporter_list');
	}
	/**
	* 
	* it send mail about operator password change from admin
	*
	* @param string $pwd  Operator password
	* @param object $operatorinfo  Operator query result object
	* @return CALLBACK ,change the operator password information function
	*
	**/

	public function send_Transporter_pwd($pwd = '', $operatorinfo) {
		$default_lang=$this->config->item('default_lang_code');
		$driver_name = $operatorinfo->row()->operator_name;
		$newsid = '2';
		$template_values = $this->app_model->get_email_template($newsid,$default_lang);
		$adminnewstemplateArr = array('email_title' => $this->config->item('email_title'), 'logo' => $this->config->item('logo_image'), 'footer_content' => $this->config->item('footer_content'), 'meta_title' => $this->config->item('meta_title'), 'site_contact_mail' => $this->config->item('site_contact_mail'));
		extract($adminnewstemplateArr);
				$message = '<!DOCTYPE HTML>
				<html>
				<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
				<meta name="viewport" content="width=device-width"/>
				<title>' . $template_values['subject'] . '</title>
				<body>';
		include($template_values['templateurl']);
		$message .= '</body>
				</html>';
		$sender_email = $this->config->item('site_contact_mail');
		$sender_name = $this->config->item('email_title');
		$email_values = array('mail_type' => 'html',
			'from_mail_id' => $sender_email,
			'mail_name' => $sender_name,
			'to_mail_id' => $operatorinfo->row()->email,
			'subject_message' => $template_values['subject'],
			'body_messages' => $message
		);
		$email_send_to_common = $this->app_model->common_email_send($email_values);
	}










	public function switch_to_transporter_session()
	{
		if ($this->checkLogin('A') == '') {
			redirect('admin');
		}
		else
		{	




						if($this->session->userdata(APP_NAME.'_session_admin_transporter_switch') == "yes")
						{


									$operatorId = $this->uri->segment(4);		
									$operator_details= $this->app_model->get_all_details(TRANSPORT,array('_id' => new MongoDB\BSON\ObjectId($operatorId)))->result();


									$operator_email=$operator_details[0]->email;

									$operator_password=$operator_details[0]->password;


							            $email = $operator_email;
							            $pwd =$operator_password;
							            $collection = TRANSPORT;
							            

							            $condition = array('email' => $email, 'password' => $pwd,'status' => 'Active');



							            $query = $this->app_model->get_all_details($collection, $condition);


							            
							              ////////////other users_array



							            $privileges=$this->config->item('schooladminPrev');					            

							              

							            if ($query->num_rows() == 1) {	
							                                      


							                                        $operatordata = array(
							                                        APP_NAME.'_session_operator_id' => $query->row()->_id,
							                                        APP_NAME.'_session_operator_name' => $query->row()->operator_name,
							                                        APP_NAME.'_session_operator_email' => $query->row()->email,
							                                        APP_NAME.'_session_operator_location' => $query->row()->operator_location,
							                                        APP_NAME.'_session_operator_privileges' =>$privileges,
							                                        APP_NAME.'_session_operator_user_type' => "main",

							                                        APP_NAME.'_session_user_model' => "transporter",
							                                        APP_NAME.'_session_dial_code' =>$query->row()->dail_code,						                                   

																	APP_NAME.'_session_changes_done_by_id' => $this->session->userdata(APP_NAME.'_session_admin_id'),
																	APP_NAME.'_session_changes_done_by_name' => $this->session->userdata(APP_NAME.'_session_admin_name')

							                                       
							                                        );


							                                        $this->session->set_userdata($operatordata);
							                                        $newdata = array(
							                                        'last_login_date' => date("Y-m-d H:i:s"),
							                                        'last_login_ip' => $this->input->ip_address()
							                                        );
							                                        $condition = array('_id' => $query->row()->_id);
							                                        $this->app_model->update_details($collection, $newdata, $condition);
							                                        $this->setErrorMessage('success', 'Logged in Successfully','admin_adminlogin_logged');
							                                        redirect(TRANSPORT_NAME.'/dashboard/display_dashboard');




							            }
							          
							             else {



							                $this->setErrorMessage('error', 'Invalid Login Details','admin_adminlogin_invalid_login');
							            }
							           redirect('admin');         	

						}
						else {
						 redirect('admin');         	
					   }		            
							       


			}				            



	}





	public function change_operator_status_global(){
			if (count($_POST['checkbox_id']) > 0 && $_POST['statusMode'] != '') {
		$this->app_model->activeInactiveCommon(TRANSPORT, '_id');
		if (strtolower($_POST['statusMode']) == 'delete') {
			$this->setErrorMessage('success', 'Transport Operator records deleted successfully');
		} else {
			$this->setErrorMessage('success', 'Transport Operator status changed successfully');
		}
		redirect('admin/transporter/display_transporter_list');
	}
	}
	






	public function signup_institution(){ 
		
			    	


					$country = $this->input->post('country_location');
					$state= $this->input->post('state');
					$city = $this->input->post('city');
					$postal_code = $this->input->post('postal_code');
                    $address = $this->input->post('address');	

                    $address_string=$address.','.$city.','.$state.','.$postal_code.','.$country;	

                    $location=array("lat"=> "1.290270","lng" => "103.851959");

					if($address_string!=''){

							$address_string = str_replace(" ", "+", $address_string);
							$json = file_get_contents("https://maps.google.com/maps/api/geocode/json?address=".$address_string."&sensor=false".$this->data['google_maps_api_key']);
							$jsonArr = json_decode($json);

							if(isset($jsonArr->{'results'}[0]->{'geometry'}->{'location'}->{'lat'}))
							{	

							$lat = $jsonArr->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
							$lang = $jsonArr->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
							$location=array("lat"=> $lat,"lng" => $lang);

							}
							

					}	







			    	$lat_long=$location;
				






					
					$email = $this->input->post('email');
					$dail_code = $this->input->post('phone_code');
					$mobile_number = $this->input->post('init_box_mobile_number');

					$password =  $this->input->post('password');

					$operator_location =$lat_long;
					



					$excludeArr = array("operator_name","status", "operator_id", "init_box_mobile_number", "phone_code", "operator_location", "address", "country", "state", "city", "postal_code",'session_switch','input_lat','input_long');
				






					$returnUrl="institution/signup";



					$condition=array('email' => $email);
					
					$duplicate_email = $this->app_model->get_all_details(TRANSPORT,$condition);
					if ($duplicate_email->num_rows() > 0){
							$this->setErrorMessage('error','Email address already exists','driver_email_already');
							redirect($returnUrl);
					}		
				
					
					$condition = array('dail_code' => $dail_code,'mobile_number' => $mobile_number);
					
					$duplicate_phone = $this->app_model->get_all_details(TRANSPORT,$condition);
					if ($duplicate_phone->num_rows() > 0){
							$this->setErrorMessage('error','Mobile number already exists','operators_mobile_already_exists');
							redirect($returnUrl);
					}			

					
					$addressArr['address'] = array('address' => $address,
																			'country' => $country,
																			'state' => $state,
																			'city' => $city,
																			'postal_code' => $postal_code
																	);
			
					
				
					
					$status = 'Inactive';

				
					$operator_data = array(
											'status' => $status,
											'created' => date('Y-m-d H:i:s'),
											'password' => md5($password),
											'dail_code' => (string) $this->input->post('dail_code'),
											'mobile_number' => (string) $this->input->post('mobile_number'),
											'session_switch' => "on",
											'operator_name' => (string) $this->input->post('institution_name') 
										);
					
				  $operator_data['operator_location'] =$operator_location;		
										
				  $dataArr = array_merge($operator_data,$addressArr);





				  
					
							$this->app_model->commonInsertUpdate(TRANSPORT,'insert',$excludeArr,$dataArr);
							$this->setErrorMessage('success','Operator added successfully','operators_added_successfully');
							
							$last_insert_id = $this->cimongo->insert_id();
							$this->mail_model->send_operator_register_confirmation_mail($last_insert_id,$password);

								$this->mail_model->send_admin_register_confirmation_mail((string)$last_insert_id,"Transport Operator");
				
							$_SESSION["transport_registered_successfuly"] = "Successfully";		

							redirect('transport/signup');
		
	}


































}

/* End of file operators.php */
/* Location: ./application/controllers/admin/operators.php */