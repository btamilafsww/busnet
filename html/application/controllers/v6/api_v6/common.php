<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/** 
* 
* Common functions
* @author Katenterprise
*
**/
 
class Common extends MY_Controller {

	function __construct(){
    parent::__construct();
		$this->load->helper(array('cookie','date','form','email'));
		$this->load->library(array('encrypt','form_validation'));
		$this->load->model('user_action_model'); 
		$this->load->model('app_model'); 
		$responseArr=array();
		
		/* Authentication Begin */
        $headers = $this->input->request_headers();
		header('Content-type:application/json;charset=utf-8');
		if (array_key_exists("Authkey", $headers)) $auth_key = $headers['Authkey']; else $auth_key = "";
		if(stripos($auth_key,APP_NAME) === false) {
			$cf_fun= $this->router->fetch_method();
			$apply_function = array();
			if(!in_array($cf_fun,$apply_function)){
				show_404();
			}
		}
		
		if(array_key_exists("Apptype",$headers)) $this->Apptype =$headers['Apptype'];
		if(array_key_exists("Userid",$headers)) $this->Userid =$headers['Userid'];
		if(array_key_exists("Driverid",$headers)) $this->Driverid =$headers['Driverid'];
		if(array_key_exists("Apptoken",$headers)) $this->Token =$headers['Apptoken'];
		try{
			if(($this->Userid!="" || $this->Driverid!="") && $this->Token!="" && $this->Apptype!=""){
				if($this->Driverid!=''){
					$deadChk = $this->app_model->get_selected_fields(DRIVERS, array('_id' => new MongoDB\BSON\ObjectId($this->Driverid)), array('push_notification'));
					if($deadChk->num_rows()>0){
						$storedToken ='';
						if(strtolower($deadChk->row()->push_notification['type']) == "ios"){
							$storedToken = $deadChk->row()->push_notification["key"];
						}
						if(strtolower($deadChk->row()->push_notification['type']) == "android"){
							$storedToken = $deadChk->row()->push_notification["key"];
						}
						$c_fun= $this->router->fetch_method();
						$apply_function = array('update_receive_mode','get_app_info');
						if(!in_array($c_fun,$apply_function)){
							if($storedToken!=''){
								if ($storedToken != $this->Token) {
									echo json_encode(array("is_dead" => "Yes"));
									die;
								}
							}
						}
					}
				}
				if($this->Userid!=''){
					$deadChk = $this->app_model->get_selected_fields(USERS, array('_id' => new MongoDB\BSON\ObjectId($this->Userid)), array('push_type', 'push_notification_key'));
					if($deadChk->num_rows()>0){
						$storedToken ='';
						if(strtolower($deadChk->row()->push_type) == "ios"){
							$storedToken = $deadChk->row()->push_notification_key["ios_token"];
						}
						if(strtolower($deadChk->row()->push_type) == "android"){
							$storedToken = $deadChk->row()->push_notification_key["gcm_id"];
						}
						if($storedToken!=''){
							if($storedToken != $this->Token){
								echo json_encode(array("is_dead"=>"Yes")); die;
							}
						}
					}
				}
			 }
		} catch (MongoException $ex) {
			
		}
		/*	Authentication End	*/
			
    }
	
	
	/**
	*
	*	This function will send the request to nearby drivers while send retry request
	*
	**/	
	public function retry_ride_request() {
        $returnArr['status'] = '0';
        $returnArr['response'] = '';
        $returnArr['acceptance'] = 'No';
		
		try {
			$user_id = (string)$this->input->post('user_id');
			$ride_id = (string)$this->input->post('ride_id');
			if($user_id!='' && $ride_id!=''){
				$checkUser = $this->app_model->get_selected_fields(USERS, array('_id' => new MongoDB\BSON\ObjectId($user_id)), array('email'));
                if ($checkUser->num_rows() == 1) {
                    $checkRide = $this->app_model->get_all_details(RIDES, array('user.id' => $user_id,'ride_id' => $ride_id));
                    if ($checkRide->num_rows() == 1) {
						if ($checkRide->row()->ride_status == 'Confirmed' || $checkRide->row()->ride_status == 'Arrived') {
							$returnArr['acceptance'] = 'Yes';
							
							$driver_id = $checkRide->row()->driver['id'];
							$mindurationtext = '';
							if (isset($checkRide->row()->driver['est_eta'])) {
								$mindurationtext = $checkRide->row()->driver['est_eta'] . '';
							}
							$lat_lon = @explode(',', $checkRide->row()->driver['lat_lon']);
							$driver_lat = $lat_lon[0];
							$driver_lon = $lat_lon[1];

							$checkDriver = $this->app_model->get_selected_fields(DRIVERS, array('_id' => new MongoDB\BSON\ObjectId($driver_id)), array('_id', 'driver_name', 'image', 'avg_review', 'email', 'dail_code', 'mobile_number', 'vehicle_number', 'vehicle_model'));
							/* Preparing driver information to share with user -- Start */
							$driver_image = USER_PROFILE_IMAGE_DEFAULT;
							if (isset($checkDriver->row()->image)) {
								if ($checkDriver->row()->image != '') {
									$driver_image = USER_PROFILE_IMAGE . $checkDriver->row()->image;
								}
							}
							$driver_review = 0;
							if (isset($checkDriver->row()->avg_review)) {
								$driver_review = $checkDriver->row()->avg_review;
							}
							$vehicleInfo = $this->app_model->get_selected_fields(MODELS, array('_id' => new MongoDB\BSON\ObjectId($checkDriver->row()->vehicle_model)), array('_id', 'name', 'brand_name'));
							$vehicle_model = '';
							if ($vehicleInfo->num_rows() > 0) {
								$vehicle_model = $vehicleInfo->row()->name;
							}

							$driver_profile = array('driver_id' => (string) $checkDriver->row()->_id,
								'driver_name' => (string) $checkDriver->row()->driver_name,
								'driver_email' => (string) $checkDriver->row()->email,
								'driver_image' => (string) base_url() . $driver_image,
								'driver_review' => (string) floatval($driver_review),
								'driver_lat' => floatval($driver_lat),
								'driver_lon' => floatval($driver_lon),
								'min_pickup_duration' => $mindurationtext,
								'ride_id' => (string) $ride_id,
								'phone_number' => (string) $checkDriver->row()->dail_code . $checkDriver->row()->mobile_number,
								'vehicle_number' => (string) $checkDriver->row()->vehicle_number,
								'vehicle_model' => (string) $vehicle_model
							);
							/* Preparing driver information to share with user -- End */
							if (empty($driver_profile)) {
								$driver_profile = json_decode("{}");
							}
							if (empty($riderlocArr)) {
								$riderlocArr = json_decode("{}");
							}
							$returnArr['response'] = array('type' => (string)0, 
																			'ride_id' => (string) $ride_id, 
																			'message' => $this->format_string('ride confirmed', 'ride_confirmed'), 
																			'driver_profile' => $driver_profile, 
																			'rider_location' => $riderlocArr
																		);
						}else{
							$limit = 10;
							$pickup_location = $checkRide->row()->booking_information['pickup']['location'];
							$pickup_lat = $checkRide->row()->booking_information['pickup']['latlong']['lat'];
							$pickup_lon = $checkRide->row()->booking_information['pickup']['latlong']['lon'];
							
							$drop_location = $checkRide->row()->booking_information['drop']['location'];
							
							$category = $checkRide->row()->booking_information['service_id'];
									
                            $coordinates = array(floatval($pickup_lon), floatval($pickup_lat));
							$requested_drivers = $checkRide->row()->requested_drivers;
							if(empty($requested_drivers)){
								$requested_drivers = array();
							}
							$category_drivers = $this->app_model->get_nearest_driver($coordinates, (string) $category, $limit,'',$requested_drivers);
							if (empty($category_drivers['result'])) {
								$category_drivers = $this->app_model->get_nearest_driver($coordinates, (string) $category, $limit * 2,'',$requested_drivers);
							}
							$android_driver = array();
							$apple_driver = array();
							$push_and_driver = array();
							$push_ios_driver = array();
							foreach ($category_drivers['result'] as $driver) {
								if (isset($driver['push_notification'])) {
									$d_id=new MongoDB\BSON\ObjectId((string)$category_drivers['result'][0]['_id']);
									array_push($requested_drivers,$d_id);
									if ($driver['push_notification']['type'] == 'ANDROID') {
										if (isset($driver['push_notification']['key'])) {
											if ($driver['push_notification']['key'] != '') {
												$android_driver[] = $driver['push_notification']['key'];
												$k = $driver['push_notification']['key'];
												$push_and_driver[$k] = $driver['_id'];
											}
										}
									}
									if ($driver['push_notification']['type'] == 'IOS') {
										if (isset($driver['push_notification']['key'])) {
											if ($driver['push_notification']['key'] != '') {
												$apple_driver[] = $driver['push_notification']['key'];
												$k = $driver['push_notification']['key'];
												$push_ios_driver[$k] = $driver['_id'];
											}
										}
									}
								}
							}
							
							$response_time = $this->config->item('respond_timeout');
							$options = array($ride_id, $response_time, $pickup_location, $drop_location);
							if (!empty($android_driver)) {
								foreach ($push_and_driver as $keys => $value) {
									$driver_id = $value;
									$driver_Msg = $this->format_string("Request for pickup user","request_pickup_user", '', 'driver', (string)$driver_id);
									$condition = array('_id' => new MongoDB\BSON\ObjectId($driver_id));
									$this->cimongo->where($condition)->inc('req_received', 1)->update(DRIVERS);$this->sendPushNotification($keys, $driver_Msg, 'ride_request', 'ANDROID', $options, 'DRIVER');
								}								
							}
							if (!empty($apple_driver)) {
								foreach ($push_ios_driver as $keys => $value) {
									$driver_id = $value;
									$driver_Msg = $this->format_string("Request for pickup user","request_pickup_user", '', 'driver', (string)$driver_id);
									$condition = array('_id' => new MongoDB\BSON\ObjectId($driver_id));
									$this->cimongo->where($condition)->inc('req_received', 1)->update(DRIVERS);$this->sendPushNotification($keys, $driver_Msg, 'ride_request', 'IOS', $options, 'DRIVER');
								}								
							}
							$this->app_model->update_details(RIDES, array("requested_drivers"=>array_unique($requested_drivers)), array('ride_id' => $ride_id));
							$returnArr['response'] = $this->format_string("Searching for a driver", "searching_for_a_driver");
						}
						
						$returnArr['status'] = '1';
					}else{
						$returnArr['response'] = $this->format_string("Invalid Ride", "invalid_ride");
					}
				}else{
					$returnArr['response'] = $this->format_string("Driver not found", "driver_not_found");
				}
			}else{
				$returnArr['response'] = $this->format_string("Some Parameters Missing", "some_parameters_missing");
			}
		}catch (MongoException $ex) {
            $returnArr['response'] = $this->format_string("Error in connection", "error_in_connection");
        }
        $json_encode = json_encode($returnArr, JSON_PRETTY_PRINT);
        echo $this->cleanString($json_encode);
    }
	
	/**
	*
	*	This function will update the information for rides request acknowledgement
	*
	**/	
	public function ack_ride_request() {
        $returnArr['status'] = '0';
        $returnArr['response'] = '';
		
		try {
			$driver_id = (string)$this->input->post('driver_id');
			$ride_id = (string)$this->input->post('ride_id');
			
			if($driver_id!='' && $ride_id!=''){
				$checkDriver = $this->app_model->get_selected_fields(DRIVERS, array('_id' => new MongoDB\BSON\ObjectId($driver_id)), array('email'));
                if ($checkDriver->num_rows() == 1) {
                    $checkRide = $this->app_model->get_selected_fields(RIDES, array('ride_id' => $ride_id), array('ride_id', 'ride_status'));
                    if ($checkRide->num_rows() == 1) {
						
						$returnArr['status'] = '1';
						$returnArr['response'] = $this->format_string("Request Acknowledged", "request_acknowledged");
					}else{
						$returnArr['response'] = $this->format_string("Invalid Ride", "invalid_ride");
					}
				}else{
					$returnArr['response'] = $this->format_string("Driver not found", "driver_not_found");
				}
			}else{
				$returnArr['response'] = $this->format_string("Some Parameters Missing", "some_parameters_missing");
			}
		}catch (MongoException $ex) {
            $returnArr['response'] = $this->format_string("Error in connection", "error_in_connection");
        }
        $json_encode = json_encode($returnArr, JSON_PRETTY_PRINT);
        echo $this->cleanString($json_encode);
    }
	
	
	/**
	*
	*	This function will update the information for denied rides
	*
	**/	
	public function deny_ride_request() {
        $returnArr['status'] = '0';
        $returnArr['response'] = '';
		
		try {
			$driver_id = (string)$this->input->post('driver_id');
			$ride_id = (string)$this->input->post('ride_id');
			
			if($driver_id!='' && $ride_id!=''){
				$checkDriver = $this->app_model->get_selected_fields(DRIVERS, array('_id' => new MongoDB\BSON\ObjectId($driver_id)), array('email'));
                if ($checkDriver->num_rows() == 1) {
                    $checkRide = $this->app_model->get_selected_fields(RIDES, array('ride_id' => $ride_id), array('ride_id', 'ride_status'));
                    if ($checkRide->num_rows() == 1) {						
						$returnArr['status'] = '1';
						$returnArr['response'] = $this->format_string("Request Denied Successfully", "request_denied_successfully");
					}else{
						$returnArr['response'] = $this->format_string("Invalid Ride", "invalid_ride");
					}
				}else{
					$returnArr['response'] = $this->format_string("Driver not found", "driver_not_found");
				}
			}else{
				$returnArr['response'] = $this->format_string("Some Parameters Missing", "some_parameters_missing");
			}
		}catch (MongoException $ex) {
            $returnArr['response'] = $this->format_string("Error in connection", "error_in_connection");
        }
        $json_encode = json_encode($returnArr, JSON_PRETTY_PRINT);
        echo $this->cleanString($json_encode);
    }
	
	
}

/* End of file common.php */
/* Location: ./application/controllers/api_v4/common.php */