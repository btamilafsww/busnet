<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/** 
* 
* Reports functions
* @author Katenterprise
*
**/
 
class Reports extends MY_Controller {

	function __construct(){
    parent::__construct();
		$this->load->helper(array('cookie','date','form','email'));
		$this->load->library(array('encrypt','form_validation'));
		$this->load->model('user_action_model'); 
		$this->load->model('app_model'); 
		$responseArr=array();
		
		/* Authentication Begin */
        $headers = $this->input->request_headers();
		header('Content-type:application/json;charset=utf-8');
		if (array_key_exists("Authkey", $headers)) $auth_key = $headers['Authkey']; else $auth_key = "";
		if(stripos($auth_key,APP_NAME) === false) {
			$cf_fun= $this->router->fetch_method();
			$apply_function = array();
			if(!in_array($cf_fun,$apply_function)){
				show_404();
			}
		}
		
		if(array_key_exists("Apptype",$headers)) $this->Apptype =$headers['Apptype'];
		if(array_key_exists("Userid",$headers)) $this->Userid =$headers['Userid'];
		if(array_key_exists("Driverid",$headers)) $this->Driverid =$headers['Driverid'];
		if(array_key_exists("Apptoken",$headers)) $this->Token =$headers['Apptoken'];
		try{
			if(($this->Userid!="" || $this->Driverid!="") && $this->Token!="" && $this->Apptype!=""){
				if($this->Driverid!=''){
					$deadChk = $this->app_model->get_selected_fields(DRIVERS, array('_id' => new MongoDB\BSON\ObjectId($this->Driverid)), array('push_notification'));
					if($deadChk->num_rows()>0){
						$storedToken ='';
						if(strtolower($deadChk->row()->push_notification['type']) == "ios"){
							$storedToken = $deadChk->row()->push_notification["key"];
						}
						if(strtolower($deadChk->row()->push_notification['type']) == "android"){
							$storedToken = $deadChk->row()->push_notification["key"];
						}
						$c_fun= $this->router->fetch_method();
						$apply_function = array('update_receive_mode','get_app_info');
						if(!in_array($c_fun,$apply_function)){
							if($storedToken!=''){
								if ($storedToken != $this->Token) {
									echo json_encode(array("is_dead" => "Yes"));
									die;
								}
							}
						}
					}
				}
				if($this->Userid!=''){
					$deadChk = $this->app_model->get_selected_fields(USERS, array('_id' => new MongoDB\BSON\ObjectId($this->Userid)), array('push_type', 'push_notification_key'));
					if($deadChk->num_rows()>0){
						$storedToken ='';
						if(strtolower($deadChk->row()->push_type) == "ios"){
							$storedToken = $deadChk->row()->push_notification_key["ios_token"];
						}
						if(strtolower($deadChk->row()->push_type) == "android"){
							$storedToken = $deadChk->row()->push_notification_key["gcm_id"];
						}
						if($storedToken!=''){
							if($storedToken != $this->Token){
								echo json_encode(array("is_dead"=>"Yes")); die;
							}
						}
					}
				}
			 }
		} catch (MongoException $ex) {
			
		}
		/*	Authentication End	*/
			
    }
	
	
	
	/**
	*
	*	This function will update the information for denied rides
	*
	**/	
	public function send_reports() {
        $returnArr['status'] = '0';
        $returnArr['response'] = '';
		
		try {
			$id = (string)$this->input->post('id');
			$user_type = strtolower($this->input->post('user_type'));
			$subject = (string)$this->input->post('subject');
			$message = (string)$this->input->post('message');
			if($user_type != '' && $id != '' && $message != '' &&  $subject != '' && ($user_type == 'user' || $user_type == 'driver')){
				$collection = USERS;
				if($user_type == 'driver'){
					$collection = DRIVERS;
				}
				$checkReporter = $this->app_model->get_selected_fields($collection , array('_id' => new MongoDB\BSON\ObjectId($id)), array('email','user_name','driver_name','country_code','phone_number','dail_code','mobile_number'));
				if($user_type == 'driver'){
					$reporter_name = $checkReporter->row()->driver_name;
					$phone_number = $checkReporter->row()->dail_code.$checkReporter->row()->mobile_number;
				} else {
					$reporter_name =  $checkReporter->row()->user_name;
					$phone_number = $checkReporter->row()->country_code.$checkReporter->row()->phone_number;
				}
				$email = $checkReporter->row()->email;
				
				
                if ($checkReporter->num_rows() == 1) {
					$report_id = (string) time();
					$dataArr =  array('report_id' => $report_id,
														'reporter_id' => new MongoDB\BSON\ObjectId($id),
														'reporter_type' => (string)$user_type,
														'reporter_details' => array('name' => $reporter_name,'email' => $email,'phone_number' => $phone_number),
														'subject' => $subject,
														'message' => $message,
														'status' => 'open',
														'created_date' => new MongoDate(time())
					);
					$this->app_model->simple_insert(REPORTS,$dataArr);
					$mailArr = array('reporter_name' => $reporter_name,
													 'reporter_type' => ucfirst($user_type),
													 'report_subject' => $subject,
													 'report_message' => $message,
													 'reporter_email' => $email,
													 'report_id' => $report_id
										);
					$this->load->model('mail_model');
					$this->mail_model->send_report_to_admin($mailArr);
					$returnArr['status'] = '1';
					$report_details = array('report_id' => $report_id,'subject' => $subject,'message' => $message);
					$returnArr['response']  = array('message' => $this->format_string("Report has been submitted successfully", "report_sent_success"), 'report_details' => $report_details);
				}else{
					$returnArr['response'] = $this->format_string("Reporter details not found", "reporter_not_found");
				}
			}else{
				$returnArr['response'] = $this->format_string("Some Parameters Missing", "some_parameters_missing");
			}
		}catch (MongoException $ex) {
            $returnArr['response'] = $this->format_string("Error in connection", "error_in_connection");
        }
        $json_encode = json_encode($returnArr, JSON_PRETTY_PRINT);
        echo $this->cleanString($json_encode);
    }

	/**
	*
	*	This function will update the information for denied rides
	*
	**/	
	public function reports_list() {
        $returnArr['status'] = '0';
        $returnArr['response'] = '';
		
		try {
			$id = (string)$this->input->post('id');
			$user_type = strtolower($this->input->post('user_type'));
			
			
			if($user_type != '' && $id != ''  && ($user_type == 'user' || $user_type == 'driver')){
				$collection = USERS;
				if($user_type == 'driver'){
					$collection = DRIVERS;
				}
				$checkReporter = $this->app_model->get_selected_fields($collection , array('_id' => new MongoDB\BSON\ObjectId($id)), array('email','user_name','driver_name'));
				if($user_type == 'driver'){
					$reporter_name = $checkReporter->row()->driver_name;
				} else {
					$reporter_name =  $checkReporter->row()->user_name;
				}
				$email = $checkReporter->row()->email;
				
                if ($checkReporter->num_rows() == 1) {
					$reports_list = $this->app_model->get_all_details(REPORTS,array('reporter_id' => new MongoDB\BSON\ObjectId($id)));
					$reportsList = array();
					foreach($reports_list->result() as $reports){
						$reportsList[] = array('report_id' => (string)$reports->report_id,
																'report_status' => ucfirst($reports->status),
																'subject' => $reports->subject,
																'message' => $reports->message,
																'reported_date' => date('Y-m-d h:i A',$reports->created_date->sec)
													);
					}
					$reports_count = (string)count($reportsList);
					if($reports_count > 0){
						$returnArr['status'] = '1';
						$returnArr['response']  = array('reports_count' => $reports_count,'reports_list' => $reportsList);
					} else {
						$returnArr['response'] = $this->format_string("You have not sent any reports yet", "have_not_sent_report");
					}
				}else{
					$returnArr['response'] = $this->format_string("Reporter details not found", "reporter_not_found");
				}
			}else{
				$returnArr['response'] = $this->format_string("Some Parameters Missing", "some_parameters_missing");
			}
		}catch (MongoException $ex) {
            $returnArr['response'] = $this->format_string("Error in connection", "error_in_connection");
        }
        $json_encode = json_encode($returnArr, JSON_PRETTY_PRINT);
        echo $this->cleanString($json_encode);
    }
	
	
}

/* End of file common.php */
/* Location: ./application/controllers/api_v4/common.php */