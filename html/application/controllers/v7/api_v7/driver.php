<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * 
 * This controller contains the functions related to Drivers at the app end
 * @author Katenterprise
 *
 * */
class Driver extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('cookie', 'date', 'form', 'email'));
        $this->load->library(array('encrypt', 'form_validation'));
        $this->load->model(array('sms_model', 'app_model', 'driver_notification_model', 'driver_model', 'bus_route_model', 'student_details_model', 'bus_trip_model', 'current_trip_model', 'mail_model'));


        /* Authentication Begin */
        $headers = $this->input->request_headers();
        header('Content-type:application/json;charset=utf-8');
        if (array_key_exists("Authkey", $headers))
            $auth_key = $headers['Authkey'];
        else
            $auth_key = "";
        if (stripos($auth_key, APP_NAME) === false) {
            $cf_fun = $this->router->fetch_method();
            $apply_function = array('login', 'logout', 'update_driver_location', 'driver_student_tree', 'trip_list');
            /* if(!in_array($cf_fun,$apply_function)){
              show_404();
              } */
        }

        if (array_key_exists("Apptype", $headers))
            $this->Apptype = $headers['Apptype'];
        if (array_key_exists("Driverid", $headers))
            $this->Driverid = $headers['Driverid'];
        if (array_key_exists("Apptoken", $headers))
            $this->Token = $headers['Apptoken'];
        try {
            if ($this->Driverid != "" && $this->Token != "" && $this->Apptype != "") {
                $deadChk = $this->app_model->get_selected_fields(DRIVERS, array('_id' => new MongoDB\BSON\ObjectId($this->Driverid)), array('push_notification', 'status'));
                if ($deadChk->num_rows() > 0) {
                    $storedToken = '';
                    if (is_object($deadChk->row()->push_notification))
                        $deadChk->row()->{"push_notification"} = (array) $deadChk->row()->push_notification;
                    if (strtolower($deadChk->row()->push_notification['type']) == "ios") {
                        $storedToken = $deadChk->row()->push_notification["key"];
                    }
                    if (strtolower($deadChk->row()->push_notification['type']) == "android") {
                        $storedToken = $deadChk->row()->push_notification["key"];
                    }

                    $c_fun = (string) $this->router->fetch_method();
                    $apply_function = array('login', 'logout', 'update_driver_location');
                    if (!in_array($c_fun, $apply_function)) {
                        if (strtolower($deadChk->row()->status) != "active") {
                            $is_out_message = $this->format_string('Your account has been modified, please login to again.', 'is_out_message');
                            echo json_encode(array("is_out" => "Yes", "message" => $is_out_message));
                            die;
                        }
                        if ($storedToken != '') {
                            if ($storedToken != $this->Token) {
                                echo json_encode(array("is_dead" => "Yes"));
                                die;
                            }
                        }
                    }
                } else {
                    $is_out_message = $this->format_string('Your account has been modified, please login to again.', 'is_out_message');
                    echo json_encode(array("is_out" => "Yes", "message" => $is_out_message));
                    die;
                }
            }
        } catch (MongoException $ex) {
            
        }
        /* Authentication End */
    }

    /**
     *
     * Login Driver 
     *
     * */
    public function login() {
        $returnArr['status'] = '0';
        $returnArr['response'] = '';
        try {
            $email = $this->input->post('email');
            $password = $this->input->post('password');
            $gcm_id = $this->input->post('gcm_id');
            $deviceToken = (string) $this->input->post('deviceToken');

            if (is_array($this->input->post())) {
                $chkValues = count(array_filter($this->input->post()));
            } else {
                $chkValues = 0;
            }

            if ($email != '' && $password != '') {
                if (valid_email($email)) {
                    $checkAccount = $this->driver_model->get_selected_fields(DRIVERS, array('email' => strtolower($email)), array('email'));
                    if ($checkAccount->num_rows() == 1) {
                        $checkDriver = $this->driver_model->get_selected_fields(DRIVERS, array('email' => strtolower($email), 'password' => md5($password)), array('email', 'user_name', 'phone_number', 'push_notification', 'status'));
                        if ($checkDriver->num_rows() == 1) {
                            if ($checkDriver->row()->status == 'Active') {
                                $push_data = array();
                                $key = '';
                                if ($gcm_id != "") {
                                    $key = $gcm_id;
                                    $push_data = array('push_notification.key' => $gcm_id, 'push_notification.type' => 'ANDROID');
                                } else if ($deviceToken != "") {
                                    $key = $deviceToken;
                                    $push_data = array('push_notification.key' => $deviceToken, 'push_notification.type' => 'IOS');
                                }

                                $is_alive_other = "No";
                                if (isset($checkDriver->row()->push_notification)) {
                                    $checkDriver->row()->{"push_notification"} = (property_exists($checkDriver->row(), "push_notification")) ? (array) $checkDriver->row()->push_notification : array();
                                    if ($checkDriver->row()->push_notification['type'] != '') {
                                        if ($checkDriver->row()->push_notification['type'] == "ANDROID") {
                                            $existingKey = $checkDriver->row()->push_notification["key"];
                                        }
                                        if ($checkDriver->row()->push_notification['type'] == "IOS") {
                                            $existingKey = $checkDriver->row()->push_notification["key"];
                                        }
                                        if ($existingKey != $key) {
                                            $is_alive_other = "Yes";
                                        }
                                    }
                                }
                                $returnArr['is_alive_other'] = (string) $is_alive_other;

                                if (!empty($push_data)) {
                                    $this->driver_model->update_details(DRIVERS, array('push_notification.key' => '', 'push_notification.type' => ''), $push_data);
                                    $this->driver_model->update_details(DRIVERS, $push_data, array('_id' => new MongoDB\BSON\ObjectId($checkDriver->row()->_id)));
                                }

                                $returnArr['status'] = '1';
                                $returnArr['response'] = $this->format_string('You are Logged In successfully', 'you_logged_in');
                                $driverVal = $this->driver_model->get_selected_fields(DRIVERS, array('_id' => new MongoDB\BSON\ObjectId($checkDriver->row()->_id)), array('operator_id', 'email', 'image', 'driver_name', 'push_notification', 'password'));
                                if (isset($driverVal->row()->image)) {
                                    if ($driverVal->row()->image == '') {
                                        $driver_image = USER_PROFILE_IMAGE_DEFAULT;
                                    } else {
                                        $driver_image = USER_PROFILE_IMAGE . $driverVal->row()->image;
                                    }
                                } else {
                                    $driver_image = USER_PROFILE_IMAGE_DEFAULT;
                                }

                                $returnArr['driver_image'] = (string) base_url() . $driver_image;
                                $returnArr['driver_id'] = (string) $checkDriver->row()->_id;
                                $returnArr['driver_name'] = (string) $driverVal->row()->driver_name;
                                $returnArr['sec_key'] = md5((string) $driverVal->row()->_id);
                                $returnArr['email'] = (string) $driverVal->row()->email;



                                $operatoe_name = "Busnet";

                                $logo = $this->app_model->get_all_details(OPERATORS, array('_id' => new MongoDB\BSON\ObjectId($driverVal->row()->operator_id)))->result();

                                if (empty($logo)) {
                                    $logo = $this->app_model->get_all_details(TRANSPORT, array('_id' => new MongoDB\BSON\ObjectId($driverVal->row()->operator_id)))->result();
                                }


                                if (isset($logo[0]->logo_image)) {
                                    if ($logo[0]->logo_image == "") {
                                        $logo_name = "8033804eb19cd93ba13fa1ab69a4cca5(1).png";
                                    } else {

                                        $logo_name = $logo[0]->logo_image;
                                    }

                                    $operatoe_name = $logo[0]->operator_name;
                                } else {
                                    $logo_name = "8033804eb19cd93ba13fa1ab69a4cca5(1).png";
                                }

                                $returnArr['op_logo'] = base_url() . 'images/logo/' . $logo_name;

                                $path = 'images/logo/' . $logo_name;
                                $type = pathinfo($path, PATHINFO_EXTENSION);
                                $data = file_get_contents($path);
                                $base64 = base64_encode($data);
                                $returnArr['base64_op_logo'] = $base64;
                                $returnArr['op_name'] = $operatoe_name;

                                $returnArr['key'] = (string) $key;
                            } else {
                                $returnArr['response'] = $this->format_string('Your account have not been activated yet', 'driver_account_not_activated');
                            }
                        } else {
                            $returnArr['response'] = $this->format_string('Please check the email and password and try again', 'please_check_email_and_password');
                        }
                    } else {
                        $returnArr['response'] = $this->format_string('Your account does not exist', 'account_not_exists');
                    }
                } else {
                    $returnArr['response'] = $this->format_string("Invalid Email address", "invalid_email_address");
                }
            } else {
                if ($gcm_id == "" && ($email != '' && $password != '')) {
                    $returnArr['response'] = $this->format_string("Cannot recognize your device", "cannot_recognise_device");
                } else {
                    $returnArr['response'] = $this->format_string("Some Parameters are missing", "some_parameters_missing");
                }
            }
        } catch (MongoException $ex) {
            $returnArr['response'] = $this->format_string('Error in connection', 'error_in_connection');
        }
        $json_encode = json_encode($returnArr, JSON_PRETTY_PRINT);
        echo $this->cleanString($json_encode);
    }

    /**
     *
     * Logout Driver 
     *
     * */
    public function logout() {
        $returnArr['status'] = '0';
        $returnArr['response'] = '';
        try {
            $driver_id = $this->input->post('driver_id');
            $device = $this->input->post('device');

            if ($driver_id != '' && $device != '') {
                $checkDriver = $this->driver_model->get_selected_fields(DRIVERS, array('_id' => new MongoDB\BSON\ObjectId($driver_id)), array('push_notification'));
                if ($checkDriver->num_rows() == 1) {
                    if ($device == 'IOS' || $device == 'ANDROID') {
                        $condition = array('_id' => new MongoDB\BSON\ObjectId($driver_id));
                        $this->driver_model->update_details(DRIVERS, array('availability' => 'No', 'push_notification.key' => '', 'push_notification.type' => ''), $condition);
                        $returnArr['status'] = '1';
                        $returnArr['response'] = $this->format_string('You are logged out', 'you_are_logged_out');
                    } else {
                        $returnArr['response'] = $this->format_string('Invalid inputs', 'invalid_input');
                    }
                } else {
                    $returnArr['response'] = $this->format_string("Driver not found", "driver_not_found");
                }
            } else {
                $returnArr['response'] = $this->format_string("Some Parameters are missing", "some_parameters_missing");
            }
        } catch (MongoException $ex) {
            $returnArr['response'] = $this->format_string("Error in connection", "error_in_connection");
        }
        $json_encode = json_encode($returnArr, JSON_PRETTY_PRINT);
        echo $this->cleanString($json_encode);
    }

    /**
     *
     * Update driver availablity
     *
     * */
    public function update_driver_availablity() {
        $returnArr['status'] = '0';
        $returnArr['response'] = '';
        try {
            $driver_id = $this->input->post('driver_id');
            $availability = $this->input->post('availability');
            $distance = floatval($this->input->post('distance'));

            if (is_array($this->input->post())) {
                $chkValues = count(array_filter($this->input->post()));
            } else {
                $chkValues = 0;
            }
            if ($chkValues >= 2) {
                $checkDriver = $this->driver_model->get_selected_fields(DRIVERS, array('_id' => new MongoDB\BSON\ObjectId($driver_id)), array('_id', 'last_online_time', 'driver_name', 'vehicle_number'));
                if ($checkDriver->num_rows() == 1) {
                    if ($availability == 'Yes') {
                        $dataArr = array('last_online_time' => new MongoDB\BSON\UTCDateTime(time()));
                        $this->driver_model->update_details(DRIVERS, $dataArr, array('_id' => new MongoDB\BSON\ObjectId($driver_id)));
                    } else {
                        if (isset($checkDriver->row()->last_online_time)) {
                            update_mileage_system($driver_id, $checkDriver->row()->last_online_time->toDateTime()->getTimestamp(), 'free-roaming', $distance, $this->data['d_distance_unit']);
                        }
                    }
                    $avail_data = array('availability' => $availability, 'last_active_time' => new MongoDB\BSON\UTCDateTime(time()));
                    $this->driver_model->update_details(DRIVERS, $avail_data, array('_id' => new MongoDB\BSON\ObjectId($driver_id)));
                    $driver_name = "";
                    $vehicle_number = "";
                    if (isset($checkDriver->row()->driver_name))
                        $driver_name = $checkDriver->row()->driver_name;
                    if (isset($checkDriver->row()->vehicle_number))
                        $vehicle_number = $checkDriver->row()->vehicle_number;
                    $returnArr['status'] = '1';
                    $returnArr['driver_name'] = $driver_name;
                    $returnArr['vehicle_number'] = $vehicle_number;
                    $returnArr['response'] = $this->format_string('Availability Updated', 'availability_updated');
                } else {
                    $returnArr['response'] = $this->format_string("Driver not found", "driver_not_found");
                }
            } else {
                $returnArr['response'] = $this->format_string("Some Parameters Missing", "some_parameters_missing");
            }
        } catch (MongoException $ex) {
            $returnArr['response'] = $this->format_string('Error in connection', 'error_in_connection');
        }
        $json_encode = json_encode($returnArr, JSON_PRETTY_PRINT);
        echo $this->cleanString($json_encode);
    }

    /**
     *
     * Update driver Mode
     *
     * */
    public function update_driver_mode() {
        $returnArr['status'] = '0';
        $returnArr['response'] = '';
        try {
            $driver_id = $this->input->post('driver_id');
            $type = $this->input->post('type');
            if ($type == '') {
                $type = 'Available';
            }

            if (is_array($this->input->post())) {
                $chkValues = count(array_filter($this->input->post()));
            } else {
                $chkValues = 0;
            }
            if (isset($_GET['dev'])) {
                if ($_GET['dev'] == 'jj') {
                    $avail_data = array('mode' => $type);
                    $this->driver_model->update_details(DRIVERS, $avail_data, array());
                }
            }
            if ($chkValues >= 2) {
                $checkDriver = $this->driver_model->get_selected_fields(DRIVERS, array('_id' => new MongoDB\BSON\ObjectId($driver_id)), array('_id'));
                if ($checkDriver->num_rows() == 1) {
                    $avail_data = array('mode' => $type);
                    $this->driver_model->update_details(DRIVERS, $avail_data, array('_id' => new MongoDB\BSON\ObjectId($driver_id)));
                    $this->driver_model->update_details(DRIVERS, $avail_data, array());
                    $returnArr['status'] = '1';
                    $returnArr['response'] = $this->format_string('Mode Updated', 'mode_updated');
                } else {
                    $returnArr['response'] = $this->format_string("Driver not found", "driver_not_found");
                }
            } else {
                $returnArr['response'] = $this->format_string("Some Parameters Missing", "some_parameters_missing");
            }
        } catch (MongoException $ex) {
            $returnArr['response'] = $this->format_string("Error in connection", "error_in_connection");
        }
        $json_encode = json_encode($returnArr, JSON_PRETTY_PRINT);
        echo $this->cleanString($json_encode);
    }

    /**
     *
     * This Function return the rider informations
     *
     * */
    public function get_rider_information() {
        $returnArr['status'] = '0';
        $returnArr['response'] = '';
        try {
            $driver_id = $this->input->post('driver_id');
            $ride_id = $this->input->post('ride_id');

            if (is_array($this->input->post())) {
                $chkValues = count(array_filter($this->input->post()));
            } else {
                $chkValues = 0;
            }

            if ($chkValues >= 2) {
                $checkDriver = $this->app_model->get_selected_fields(DRIVERS, array('_id' => new MongoDB\BSON\ObjectId($driver_id)), array('email'));
                if ($checkDriver->num_rows() == 1) {
                    $checkRide = $this->app_model->get_selected_fields(RIDES, array('ride_id' => $ride_id), array('ride_id', 'ride_status', 'booking_information', 'user.id', 'driver.id'));
                    if ($checkRide->num_rows() == 1) {
                        $user_id = $checkRide->row()->user['id'];
                        $checkUser = $this->app_model->get_selected_fields(USERS, array('_id' => new MongoDB\BSON\ObjectId($user_id)), array('email', 'user_name', 'country_code', 'phone_number', 'image', 'avg_review'));
                        $infoArr = array();
                        if ($checkUser->num_rows() == 1) {
                            if ($checkUser->row()->image == '') {
                                $user_image = USER_PROFILE_IMAGE_DEFAULT;
                            } else {
                                $user_image = USER_PROFILE_IMAGE . $checkUser->row()->image;
                            }
                            $user_review = 0;
                            if (isset($checkUser->row()->avg_review)) {
                                $user_review = $checkUser->row()->avg_review;
                            }
                            $infoArr = array('user_name' => $checkUser->row()->user_name,
                                'user_id' => (string) $checkUser->row()->_id,
                                'user_email' => $checkUser->row()->email,
                                'user_phone' => $checkUser->row()->country_code . '' . $checkUser->row()->phone_number,
                                'user_image' => base_url() . $user_image,
                                'user_review' => floatval($user_review),
                                'ride_id' => $ride_id
                            );
                        }

                        if (empty($infoArr)) {
                            $infoArr = json_decode("{}");
                        }
                        $returnArr['status'] = '1';
                        $returnArr['response'] = array('information' => $infoArr);
                    } else {
                        $returnArr['response'] = $this->format_string("Invalid Ride", "invalid_ride");
                    }
                } else {
                    $returnArr['response'] = $this->format_string("Driver not found", "driver_not_found");
                }
            } else {
                $returnArr['response'] = $this->format_string("Some Parameters Missing", "some_parameters_missing");
            }
        } catch (MongoException $ex) {
            $returnArr['response'] = $this->format_string("Error in connection", "error_in_connection");
        }
        $json_encode = json_encode($returnArr, JSON_PRETTY_PRINT);
        echo $this->cleanString($json_encode);
    }

    /**
     *
     * This Function returns the driver rides list
     *
     * */
    public function driver_all_ride_list() {
        $returnArr['status'] = '0';
        $returnArr['response'] = '';
        try {
            $driver_id = (string) $this->input->post('driver_id');
            $type = (string) $this->input->post('trip_type');
            if ($type == '')
                $type = 'all';

            if ($driver_id != '') {
                $driverVal = $this->app_model->get_selected_fields(DRIVERS, array('_id' => new MongoDB\BSON\ObjectId($driver_id)), array('city', 'avail_category'));
                if ($driverVal->num_rows() > 0) {
                    $checkRide = $this->app_model->get_ride_list_for_driver($driver_id, $type, array('booking_information', 'ride_id', 'ride_status'));
                    $rideArr = array();
                    if ($checkRide->num_rows() > 0) {
                        foreach ($checkRide->result() as $ride) {
                            $group = 'all';
                            if ($ride->ride_status == 'Onride' || $ride->ride_status == 'Confirmed' || $ride->ride_status == 'Arrived') {
                                $group = 'onride';
                            } else if ($ride->ride_status == 'Completed' || $ride->ride_status == 'Finished') {
                                $group = 'completed';
                            }
                            $rideArr[] = array('ride_id' => $ride->ride_id,
                                'ride_time' => get_time_to_string("h:i A", $ride->booking_information['booking_date']->toDateTime()->getTimestamp()),
                                'ride_date' => get_time_to_string("jS M, Y", $ride->booking_information['booking_date']->toDateTime()->getTimestamp()),
                                'pickup' => $ride->booking_information['pickup']['location'],
                                'group' => $group,
                                'datetime' => get_time_to_string("d-m-Y", $ride->booking_information['booking_date']->toDateTime()->getTimestamp()),
                            );
                        }
                    }

                    if (empty($rideArr)) {
                        $rideArr = json_decode("{}");
                    }
                    $total_rides = intval($checkRide->num_rows());
                    $returnArr['status'] = '1';
                    $returnArr['response'] = array('total_rides' => (string) $total_rides, 'rides' => $rideArr);
                } else {
                    $returnArr['response'] = $this->format_string("Driver not found", "driver_not_found");
                }
            } else {
                $returnArr['response'] = $this->format_string("Some Parameters are missing", "some_parameters_missing");
            }
        } catch (MongoException $ex) {
            $returnArr['response'] = $this->format_string("Error in connection", "error_in_connection");
        }
        $json_encode = json_encode($returnArr, JSON_PRETTY_PRINT);
        echo $this->cleanString($json_encode);
    }

    /**
     *
     * This Function return the drivers particular ride details
     *
     * */
    public function view_driver_ride_information() {
        $returnArr['status'] = '0';
        $returnArr['response'] = '';
        try {
            $driver_id = (string) $this->input->post('driver_id');
            $ride_id = (string) $this->input->post('ride_id');

            if ($driver_id != '') {
                $driverVal = $this->app_model->get_selected_fields(DRIVERS, array('_id' => new MongoDB\BSON\ObjectId($driver_id)), array('_id'));
                if ($driverVal->num_rows() > 0) {
                    $checkRide = $this->app_model->get_all_details(RIDES, array('ride_id' => $ride_id, 'driver.id' => $driver_id));
                    if ($checkRide->num_rows() == 1) {
                        $fareArr = array();
                        $summaryArr = array();
                        $min_short = $this->format_string('min', 'min_short');
                        $mins_short = $this->format_string('mins', 'mins_short');
                        if (isset($checkRide->row()->summary)) {
                            if (is_array($checkRide->row()->summary)) {
                                foreach ($checkRide->row()->summary as $key => $values) {
                                    if ($key == "ride_duration") {
                                        if ($values <= 1) {
                                            $unit = $min_short;
                                        } else {
                                            $unit = $mins_short;
                                        }
                                        $summaryArr[$key] = (string) $values . ' ' . $unit;
                                    } else if ($key == "waiting_duration") {
                                        if ($values <= 1) {
                                            $unit = $min_short;
                                        } else {
                                            $unit = $mins_short;
                                        }
                                        $summaryArr[$key] = (string) $values . ' ' . $unit;
                                    } else {
                                        $summaryArr[$key] = (string) $values;
                                    }
                                }
                            }
                        }
                        /* if (isset($checkRide->row()->summary)) {
                          if (is_array($checkRide->row()->summary)) {
                          $summaryArr = $checkRide->row()->summary;
                          }
                          } */
                        if (isset($checkRide->row()->total)) {
                            if (is_array($checkRide->row()->total)) {
                                $total_bill = 0.00;
                                $tips_amount = 0.00;
                                $coupon_discount = 0.00;
                                $grand_bill = 0.00;
                                $total_paid = 0.00;
                                $wallet_usage = 0.00;
                                if (isset($checkRide->row()->total['total_fare'])) {
                                    $total_bill = $checkRide->row()->total['total_fare'];
                                }

                                if (isset($checkRide->row()->total['tips_amount'])) {
                                    $tips_amount = $checkRide->row()->total['tips_amount'];
                                }

                                $tips_status = '0';
                                if ($tips_amount > 0) {
                                    $tips_status = '1';
                                }


                                if (isset($checkRide->row()->total['coupon_discount'])) {
                                    $coupon_discount = $checkRide->row()->total['coupon_discount'];
                                }
                                if (isset($checkRide->row()->total['grand_fare'])) {
                                    $grand_bill = $checkRide->row()->total['grand_fare'];
                                }
                                if (isset($checkRide->row()->total['paid_amount'])) {
                                    $total_paid = $checkRide->row()->total['paid_amount'];
                                }
                                if (isset($checkRide->row()->total['wallet_usage'])) {
                                    $wallet_usage = $checkRide->row()->total['wallet_usage'];
                                }
                                $fareArr = array('total_bill' => (string) floatval(round($total_bill, 2)),
                                    'coupon_discount' => (string) floatval(round($coupon_discount, 2)),
                                    'grand_bill' => (string) floatval(round($grand_bill, 2)),
                                    'total_paid' => (string) floatval(round($total_paid, 2)),
                                    'wallet_usage' => (string) floatval(round($wallet_usage, 2))
                                );

                                $tipsArr = array('tips_status' => $tips_status,
                                    'tips_amount' => (string) floatval($tips_amount)
                                );
                            }
                        }

                        $pay_status = '';
                        $disp_pay_status = '';
                        if (isset($checkRide->row()->pay_status)) {
                            $pay_status = $checkRide->row()->pay_status;
                            if ($pay_status == 'Paid') {
                                $disp_pay_status = $this->format_string("Paid", "paid");
                            } else {
                                $pay_status == 'Pending';
                                $disp_pay_status = $this->format_string("Pending", "pending");
                            }
                        }


                        $doAction = 0;
                        if ($checkRide->row()->ride_status == 'Booked' || $checkRide->row()->ride_status == 'Confirmed' || $checkRide->row()->ride_status == 'Cancelled' || $checkRide->row()->ride_status == 'Arrived') {
                            $doAction = 1;
                            if ($checkRide->row()->ride_status == 'Cancelled') {
                                $doAction = 0;
                            }
                        }
                        $iscontinue = 'NO';
                        if ($checkRide->row()->ride_status == 'Confirmed' || $checkRide->row()->ride_status == 'Arrived' || $checkRide->row()->ride_status == 'Onride') {
                            if ($checkRide->row()->ride_status == 'Confirmed') {
                                $iscontinue = 'arrived';
                            }
                            if ($checkRide->row()->ride_status == 'Arrived') {
                                $iscontinue = 'begin';
                            }
                            if ($checkRide->row()->ride_status == 'Onride') {
                                $iscontinue = 'end';
                            }
                        }
                        $user_profile = array();
                        if ($iscontinue != 'NO' || $iscontinue == 'NO') {
                            $userVal = $this->driver_model->get_selected_fields(USERS, array('_id' => new MongoDB\BSON\ObjectId($checkRide->row()->user['id'])), array('_id', 'user_name', 'email', 'image', 'avg_review', 'phone_number', 'country_code', 'push_type', 'push_notification_key'));
                            if ($userVal->num_rows() > 0) {
                                if ($userVal->row()->image == '') {
                                    $user_image = USER_PROFILE_IMAGE_DEFAULT;
                                } else {
                                    $user_image = USER_PROFILE_IMAGE . $userVal->row()->image;
                                }
                                $user_review = 0;
                                if (isset($userVal->row()->avg_review)) {
                                    $user_review = $userVal->row()->avg_review;
                                }

                                $drop_location = 0;
                                $drop_loc = '';
                                $drop_lat = '';
                                $drop_lon = '';
                                if ($checkRide->row()->booking_information['drop']['location'] != '') {
                                    $drop_location = 1;
                                    $drop_loc = $checkRide->row()->booking_information['drop']['location'];
                                    $drop_lat = $checkRide->row()->booking_information['drop']['latlong']['lat'];
                                    $drop_lon = $checkRide->row()->booking_information['drop']['latlong']['lon'];
                                }

                                $ride_date = get_time_to_string("M d, Y", $checkRide->row()->booking_information['est_pickup_date']->toDateTime()->getTimestamp());
                                $pickup_date = '';
                                $drop_date = '';
                                if ($checkRide->row()->ride_status == 'Booked' || $checkRide->row()->ride_status == 'Confirmed' || $checkRide->row()->ride_status == 'Cancelled' || $checkRide->row()->ride_status == 'Arrived' || $checkRide->row()->ride_status == 'Onride') {
                                    $pickup_date = get_time_to_string("h:i A", $checkRide->row()->booking_information['est_pickup_date']->toDateTime()->getTimestamp());
                                } else {
                                    $pickup_date = get_time_to_string("h:i A", $checkRide->row()->history['begin_ride']->toDateTime()->getTimestamp());
                                    $drop_date = get_time_to_string("h:i A", $checkRide->row()->history['end_ride']->toDateTime()->getTimestamp());
                                }

                                $user_profile = array('user_name' => $userVal->row()->user_name,
                                    'user_id' => (string) $userVal->row()->_id,
                                    'user_email' => $userVal->row()->email,
                                    'phone_number' => (string) $userVal->row()->country_code . $userVal->row()->phone_number,
                                    'user_image' => base_url() . $user_image,
                                    'user_review' => floatval($user_review),
                                    'ride_id' => $ride_id,
                                    'pickup_location' => $checkRide->row()->booking_information['pickup']['location'],
                                    'pickup_lat' => $checkRide->row()->booking_information['pickup']['latlong']['lat'],
                                    'pickup_lon' => $checkRide->row()->booking_information['pickup']['latlong']['lon'],
                                    'pickup_time' => $pickup_date,
                                    'drop_location' => (string) $drop_location,
                                    'drop_loc' => (string) $drop_loc,
                                    'drop_lat' => (string) $drop_lat,
                                    'drop_lon' => (string) $drop_lon,
                                    'drop_time' => (string) $drop_date
                                );
                            }
                        }

                        $dropArr = array();
                        if ($checkRide->row()->booking_information['drop']['location'] != '') {
                            $dropArr = $checkRide->row()->booking_information['drop'];
                        }
                        if (empty($dropArr)) {
                            $dropArr = json_decode("{}");
                        }
                        $distance_unit = $this->data['d_distance_unit'];
                        if (isset($checkRide->row()->fare_breakup['distance_unit'])) {
                            $distance_unit = $checkRide->row()->fare_breakup['distance_unit'];
                        }
                        $disp_distance_unit = $distance_unit;
                        if ($distance_unit == 'km')
                            $disp_distance_unit = $this->format_string('km', 'km');
                        if ($distance_unit == 'mi')
                            $disp_distance_unit = $this->format_string('mi', 'mi');

                        if (!empty($summaryArr)) {
                            $summaryArr['currency'] = (string) $checkRide->row()->currency;
                            $summaryArr['ride_fare'] = (string) floatval(round($grand_bill, 2));
                        }

                        $invoice_path = 'trip_invoice/' . $ride_id . '_large.jpg';
                        if (file_exists($invoice_path)) {
                            $invoice_src = base_url() . $invoice_path;
                        } else {
                            $invoice_src = '';
                        }

                        $drop_date_time = '';
                        if (is_null($checkRide->row()->booking_information['drop_date']->toDateTime()->getTimestamp())) {
                            $drop_date_time = get_time_to_string("h:i A", $checkRide->row()->booking_information['drop_date']->toDateTime()->getTimestamp()) . ' ' . $this->format_string('on', 'on') . ' ' . get_time_to_string("jS M, Y", $checkRide->row()->booking_information['drop_date']->toDateTime()->getTimestamp());
                        }
                        $disp_status = '';
                        if ($checkRide->row()->ride_status == 'Booked') {
                            $disp_status = $this->format_string("Booked", "booked");
                        } else if ($checkRide->row()->ride_status == 'Confirmed') {
                            $disp_status = $this->format_string("Accepted", "accepted");
                        } else if ($checkRide->row()->ride_status == 'Cancelled') {
                            $disp_status = $this->format_string("Cancelled", "cancelled");
                        } else if ($checkRide->row()->ride_status == 'Completed') {
                            $disp_status = $this->format_string("Completed", "completed");
                        } else if ($checkRide->row()->ride_status == 'Finished') {
                            $disp_status = $this->format_string("Awaiting Payment", "await_payment");
                        } else if ($checkRide->row()->ride_status == 'Arrived' || $checkRide->row()->ride_status == 'Onride') {
                            $disp_status = $this->format_string("On Ride", "on_ride");
                        }

                        $trip_type = "Normal";
                        if (isset($checkRide->row()->pool_ride)) {
                            if ($checkRide->row()->pool_ride == "Yes") {
                                $trip_type = "Share";
                            }
                        }

                        $responseArr = array('currency' => $checkRide->row()->currency,
                            'cab_type' => $checkRide->row()->booking_information['service_type'],
                            'trip_type' => $trip_type,
                            'ride_id' => $checkRide->row()->ride_id,
                            'ride_status' => $checkRide->row()->ride_status,
                            'disp_status' => $disp_status,
                            'do_cancel_action' => (string) $doAction,
                            'pay_status' => $pay_status,
                            'disp_pay_status' => $disp_pay_status,
                            'pickup' => $checkRide->row()->booking_information['pickup'],
                            'drop' => $dropArr,
                            'ride_date' => (string) $ride_date,
                            'pickup_date' => $pickup_date,
                            'drop_date' => $drop_date,
                            'summary' => $summaryArr,
                            'fare' => $fareArr,
                            'tips' => $tipsArr,
                            'continue_ride' => $iscontinue,
                            'distance_unit' => $disp_distance_unit,
                            'invoice_src' => $invoice_src,
                            'user_id' => (string) $checkRide->row()->user['id']
                        );

                        $receive_cash = 'Disable';
                        if ($this->config->item('pay_by_cash') != '' && $this->config->item('pay_by_cash') != 'Disable') {
                            $receive_cash = 'Enable';
                        }

                        $req_payment = 'Enable';
                        $payArr = $this->driver_model->get_all_details(PAYMENT_GATEWAY, array("status" => "Enable"));
                        if ($payArr->num_rows() == 0) {
                            $req_payment = 'Disable';
                        }

                        if (empty($responseArr)) {
                            $responseArr = json_decode("{}");
                        }
                        $returnArr['status'] = '1';
                        $returnArr['response'] = array('receive_cash' => $receive_cash,
                            'req_payment' => $req_payment,
                            'details' => $responseArr,
                            'user_profile' => $user_profile
                        );
                    } else {
                        $returnArr['response'] = $this->format_string("Ride not found", "ride_not_found");
                    }
                } else {
                    $returnArr['response'] = $this->format_string("Invalid User", "invalid_user");
                }
            } else {
                $returnArr['response'] = $this->format_string("Some Parameters Missing", "some_parameters_missing");
            }
        } catch (MongoException $ex) {
            $returnArr['response'] = $this->format_string("Error in connection", "error_in_connection");
        }
        $json_encode = json_encode($returnArr, JSON_PRETTY_PRINT);
        echo $this->cleanString($json_encode);
    }

    /**
     *
     * This Function sends the request to riders about payment
     *
     * */
    public function requesting_payment() {
        $returnArr['status'] = '0';
        $returnArr['response'] = '';
        try {
            $driver_id = (string) $this->input->post('driver_id');
            $ride_id = (string) $this->input->post('ride_id');


            if ($driver_id != '') {
                $driverChek = $this->app_model->get_all_details(DRIVERS, array('_id' => new MongoDB\BSON\ObjectId($driver_id)), array());
                if ($driverChek->num_rows() > 0) {
                    $checkRide = $this->app_model->get_all_details(RIDES, array('ride_id' => $ride_id, 'driver.id' => $driver_id));
                    if ($checkRide->num_rows() == 1) {
                        $user_id = $checkRide->row()->user['id'];
                        $userVal = $this->driver_model->get_selected_fields(USERS, array('_id' => new MongoDB\BSON\ObjectId($user_id)), array('_id', 'user_name', 'email', 'image', 'avg_review', 'phone_number', 'country_code', 'push_type', 'push_notification_key'));
                        if (isset($userVal->row()->push_type)) {
                            if ($userVal->row()->push_type != '') {

                                $tip_status = '0';
                                $tips_amount = '0.00';
                                if (isset($checkRide->row()->total['tips_amount'])) {
                                    if ($checkRide->row()->total['tips_amount'] > 0) {
                                        $tip_status = '0';
                                        $tips_amount = (string) $checkRide->row()->total['tips_amount'];
                                    }
                                }

                                /* Preparing driver information to share with user -- Start */
                                $driver_image = USER_PROFILE_IMAGE_DEFAULT;
                                if (isset($driverChek->row()->image)) {
                                    if ($driverChek->row()->image != '') {
                                        $driver_image = USER_PROFILE_IMAGE . $driverChek->row()->image;
                                    }
                                }
                                $driver_review = 0;
                                if (isset($driverChek->row()->avg_review)) {
                                    $driver_review = $driverChek->row()->avg_review;
                                }
                                $driver_name = '';
                                if (isset($driverChek->row()->driver_name)) {
                                    $driver_name = $driverChek->row()->driver_name;
                                }
                                $driver_lat = '';
                                $driver_long = '';
                                if (isset($driverChek->row()->loc)) {
                                    $driver_lat = $driverChek->row()->loc['lat'];
                                    $driver_long = $driverChek->row()->loc['lon'];
                                }
                                $user_name = $userVal->row()->user_name;
                                $user_lat = '';
                                $user_long = '';
                                $userLocation = $this->app_model->get_all_details(USER_LOCATION, array('user_id' => new MongoDB\BSON\ObjectId($user_id)));
                                if ($userLocation->num_rows() > 0) {
                                    if (isset($userLocation->row()->geo)) {
                                        $latlong = $userLocation->row()->geo;
                                        $user_lat = $latlong[1];
                                        $user_long = $latlong[0];
                                    }
                                }
                                $subtotal = 0;
                                $coupon = 0;
                                $service_tax = 0;
                                $total = 0;
                                if (isset($checkRide->row()->total['total_fare'])) {
                                    if ($checkRide->row()->total['total_fare'] > 0) {
                                        $subtotal = $checkRide->row()->total['total_fare'];
                                    }
                                }
                                if (isset($checkRide->row()->total['coupon_discount'])) {
                                    if ($checkRide->row()->total['coupon_discount'] > 0) {
                                        $coupon = $checkRide->row()->total['coupon_discount'];
                                    }
                                }
                                if (isset($checkRide->row()->total['service_tax'])) {
                                    if ($checkRide->row()->total['service_tax'] > 0) {
                                        $service_tax = $checkRide->row()->total['service_tax'];
                                    }
                                }
                                if (isset($checkRide->row()->total['grand_fare'])) {
                                    if ($checkRide->row()->total['grand_fare'] > 0) {
                                        $total = $checkRide->row()->total['grand_fare'];
                                    }
                                }


                                $message = $this->format_string("your payment is pending", "your_payment_is_pending", '', 'user', (string) $userVal->row()->_id);
                                $currency = $checkRide->row()->currency;
                                $mins = $this->format_string('mins', 'mins');

                                $distance_unit = $this->data['d_distance_unit'];
                                if (isset($checkRide->row()->fare_breakup['distance_unit'])) {
                                    $distance_unit = $checkRide->row()->fare_breakup['distance_unit'];
                                }
                                if ($distance_unit == 'km') {
                                    $distance_km = $this->format_string('km', 'km');
                                }
                                $options = array('currency' => (string) $currency,
                                    'ride_fare' => (string) $checkRide->row()->total['grand_fare'],
                                    'ride_distance' => (string) $checkRide->row()->summary['ride_distance'] . ' ' . $distance_km,
                                    'ride_duration' => (string) $checkRide->row()->summary['ride_duration'] . ' ' . $mins,
                                    'waiting_duration' => (string) $checkRide->row()->summary['waiting_duration'] . ' ' . $mins,
                                    'ride_id' => (string) $ride_id,
                                    'user_id' => (string) $user_id,
                                    'tip_status' => (string) $tip_status,
                                    'tips_amount' => (string) $tips_amount,
                                    'driver_name' => (string) $driver_name,
                                    'driver_image' => (string) base_url() . $driver_image,
                                    'driver_review' => (string) $driver_review,
                                    'driver_lat' => (string) $driver_lat,
                                    'driver_long' => (string) $driver_long,
                                    'user_name' => (string) $user_name,
                                    'user_lat' => (string) $user_lat,
                                    'user_long' => (string) $user_long,
                                    'subtotal' => (string) $subtotal,
                                    'coupon' => (string) $coupon,
                                    'service_tax' => (string) $service_tax,
                                    'total' => (string) $total,
                                    'base_fare' => (string) $checkRide->row()->total['base_fare']
                                );


                                if ($userVal->row()->push_type == 'ANDROID') {
                                    if (isset($userVal->row()->push_notification_key['gcm_id'])) {
                                        if ($userVal->row()->push_notification_key['gcm_id'] != '') {
                                            $this->sendPushNotification(array($userVal->row()->push_notification_key['gcm_id']), $message, 'requesting_payment', 'ANDROID', $options, 'USER');
                                        }
                                    }
                                }
                                if ($userVal->row()->push_type == 'IOS') {
                                    if (isset($userVal->row()->push_notification_key['ios_token'])) {
                                        if ($userVal->row()->push_notification_key['ios_token'] != '') {
                                            $this->sendPushNotification(array($userVal->row()->push_notification_key['ios_token']), $message, 'requesting_payment', 'IOS', $options, 'USER');
                                        }
                                    }
                                }
                            }
                        }

                        $returnArr['status'] = '1';
                        $returnArr['response'] = $this->format_string('request sent', 'request_sent');
                    } else {
                        $returnArr['response'] = $this->format_string("Invalid Ride", "invalid_ride");
                    }
                } else {
                    $returnArr['response'] = $this->format_string("Driver not found", "driver_not_found");
                }
            } else {
                $returnArr['response'] = $this->format_string("Some Parameters Missing", "some_parameters_missing");
            }
        } catch (MongoException $ex) {
            $returnArr['response'] = $this->format_string("Error in connection", "error_in_connection");
        }
        $json_encode = json_encode($returnArr, JSON_PRETTY_PRINT);
        echo $this->cleanString($json_encode);
    }

    /**
     *
     * This Function accepting the cash and update the ride payment status
     *
     * */
    public function cash_payment_received() {
        $returnArr['status'] = '0';
        $returnArr['response'] = '';
        try {
            $driver_id = (string) $this->input->post('driver_id');
            $ride_id = (string) $this->input->post('ride_id');
            $amount = $this->input->post('amount');


            if ($driver_id != '' && $ride_id != '' && $amount != '') {
                $driverChek = $this->app_model->get_selected_fields(DRIVERS, array('_id' => new MongoDB\BSON\ObjectId($driver_id)), array());
                if ($driverChek->num_rows() > 0) {
                    $checkRide = $this->app_model->get_all_details(RIDES, array('ride_id' => $ride_id, 'driver.id' => $driver_id));
                    if ($checkRide->num_rows() == 1) {
                        $paid_amount = 0.00;
                        $tips_amount = 0.00;


                        if (isset($checkRide->row()->total['tips_amount'])) {
                            $tips_amount = $checkRide->row()->total['tips_amount'];
                        }

                        if (isset($checkRide->row()->total)) {
                            if (isset($checkRide->row()->total['grand_fare']) && isset($checkRide->row()->total['wallet_usage'])) {
                                $paid_amount = ($checkRide->row()->total['grand_fare'] + $tips_amount) - $checkRide->row()->total['wallet_usage'];
                                $paid_amount = round($paid_amount, 2);
                            }
                        }
                        $pay_summary = 'Cash';
                        if (isset($checkRide->row()->pay_summary)) {
                            if ($checkRide->row()->pay_summary != '') {
                                if ($checkRide->row()->pay_summary != 'Cash') {
                                    if ($checkRide->row()->pay_summary['type'] != "Cash") {
                                        $pay_summary = $checkRide->row()->pay_summary['type'] . '_Cash';
                                    }
                                }
                            } else {
                                $pay_summary = 'Cash';
                            }
                        }
                        $pay_summary = array('type' => $pay_summary);
                        $paymentInfo = array('ride_status' => 'Completed',
                            'pay_status' => 'Paid',
                            'history.pay_by_cash_time' => new MongoDB\BSON\UTCDateTime(time()),
                            'total.paid_amount' => round(floatval($paid_amount), 2),
                            'pay_summary' => $pay_summary
                        );

                        if ($checkRide->row()->pay_status != "Paid") {
                            $this->app_model->update_details(RIDES, $paymentInfo, array('ride_id' => $ride_id));
                            /* Update Stats Starts */
                            $current_date = new MongoDB\BSON\UTCDateTime(strtotime(date("Y-m-d 00:00:00")));
                            $field = array('ride_completed.hour_' . date('H') => 1, 'ride_completed.count' => 1);
                            $this->app_model->update_stats(array('day_hour' => $current_date), $field, 1);
                            /* Update Stats End */
                            $avail_data = array('mode' => 'Available', 'availability' => 'Yes');
                            $this->app_model->update_details(DRIVERS, $avail_data, array('_id' => new MongoDB\BSON\ObjectId($driver_id)));
                            $trans_id = time() . rand(0, 2578);
                            $transactionArr = array('type' => 'cash',
                                'amount' => floatval($paid_amount),
                                'trans_id' => $trans_id,
                                'trans_date' => new MongoDB\BSON\UTCDateTime(time())
                            );
                            $this->app_model->simple_push(PAYMENTS, array('ride_id' => $ride_id), array('transactions' => $transactionArr));

                            $user_id = $checkRide->row()->user['id'];
                            $userVal = $this->driver_model->get_selected_fields(USERS, array('_id' => new MongoDB\BSON\ObjectId($user_id)), array('_id', 'user_name', 'email', 'image', 'avg_review', 'phone_number', 'country_code', 'push_type', 'push_notification_key'));
                            if (isset($userVal->row()->push_type)) {
                                if ($userVal->row()->push_type != '') {
                                    $message = $this->format_string("your billing amount paid successfully", "your_billing_amount_paid", '', 'user', (string) $userVal->row()->_id);
                                    $options = array('ride_id' => (string) $ride_id, 'user_id' => (string) $user_id);
                                    if ($userVal->row()->push_type == 'ANDROID') {
                                        if (isset($userVal->row()->push_notification_key['gcm_id'])) {
                                            if ($userVal->row()->push_notification_key['gcm_id'] != '') {
                                                $this->sendPushNotification(array($userVal->row()->push_notification_key['gcm_id']), $message, 'payment_paid', 'ANDROID', $options, 'USER');
                                            }
                                        }
                                    }
                                    if ($userVal->row()->push_type == 'IOS') {
                                        if (isset($userVal->row()->push_notification_key['ios_token'])) {
                                            if ($userVal->row()->push_notification_key['ios_token'] != '') {
                                                $this->sendPushNotification(array($userVal->row()->push_notification_key['ios_token']), $message, 'payment_paid', 'IOS', $options, 'USER');
                                            }
                                        }
                                    }
                                }
                            }

                            #	make and sending invoice to the rider 	#
                            $this->app_model->update_ride_amounts($ride_id);
                            $fields = array(
                                'ride_id' => (string) $ride_id
                            );
                            $url = base_url() . 'prepare-invoice';
                            $this->load->library('curl');
                            $output = $this->curl->simple_post($url, $fields);
                        }

                        $returnArr['status'] = '1';
                        $returnArr['response'] = $this->format_string('amount received', 'amount_received');
                    } else {
                        $returnArr['response'] = $this->format_string("Invalid Ride", "invalid_ride");
                    }
                } else {
                    $returnArr['response'] = $this->format_string("Driver not found", "driver_not_found");
                }
            } else {
                $returnArr['response'] = $this->format_string("Some Parameters Missing", "some_parameters_missing");
            }
        } catch (MongoException $ex) {
            $returnArr['response'] = $this->format_string("Error in connection", "error_in_connection");
        }
        $json_encode = json_encode($returnArr, JSON_PRETTY_PRINT);
        echo $this->cleanString($json_encode);
    }

    /**
     *
     * This function returns the banking detail of the driver
     *
     * */
    public function get_banking_details() {
        $returnArr['status'] = '0';
        $returnArr['response'] = '';
        try {
            $driver_id = $this->input->post('driver_id');

            if ($driver_id != '') {
                $checkDriver = $this->driver_model->get_selected_fields(DRIVERS, array('_id' => new MongoDB\BSON\ObjectId($driver_id)), array('_id', 'banking'));
                if ($checkDriver->num_rows() == 1) {
                    $bankingArr = array("acc_holder_name" => (string) '',
                        "acc_holder_address" => (string) '',
                        "acc_number" => (string) '',
                        "bank_name" => (string) '',
                        "branch_name" => (string) '',
                        "branch_address" => (string) '',
                        "swift_code" => (string) '',
                        "routing_number" => (string) ''
                    );
                    if (isset($checkDriver->row()->banking)) {
                        if (is_array($checkDriver->row()->banking)) {
                            if (!empty($checkDriver->row()->banking)) {
                                $bankingArr = $checkDriver->row()->banking;
                            }
                        }
                    }
                    if (empty($bankingArr)) {
                        $bankingArr = json_decode("{}");
                    }
                    $returnArr['status'] = '1';
                    $returnArr['response'] = array('banking' => $bankingArr);
                } else {
                    $returnArr['response'] = $this->format_string("Driver not found", "driver_not_found");
                }
            } else {
                $returnArr['response'] = $this->format_string("Some Parameters Missing", "some_parameters_missing");
            }
        } catch (MongoException $ex) {
            $returnArr['response'] = $this->format_string("Error in connection", "error_in_connection");
        }
        $json_encode = json_encode($returnArr, JSON_PRETTY_PRINT);
        echo $this->cleanString($json_encode);
    }

    /**
     *
     * This function save and return the banking detail of the driver
     *
     * */
    public function save_banking_details() {
        $returnArr['status'] = '0';
        $returnArr['response'] = '';
        try {
            $driver_id = $this->input->post('driver_id');

            if (is_array($this->input->post())) {
                $chkValues = count(array_filter($this->input->post()));
            } else {
                $chkValues = 0;
            }

            if ($driver_id != '' && $chkValues >= 6) {
                $checkDriver = $this->driver_model->get_selected_fields(DRIVERS, array('_id' => new MongoDB\BSON\ObjectId($driver_id)), array('_id', 'banking'));
                if ($checkDriver->num_rows() == 1) {

                    $banking = array("acc_holder_name" => trim($this->input->post('acc_holder_name')),
                        "acc_holder_address" => trim($this->input->post('acc_holder_address')),
                        "acc_number" => trim($this->input->post('acc_number')),
                        "bank_name" => trim($this->input->post('bank_name')),
                        "branch_name" => trim($this->input->post('branch_name')),
                        "branch_address" => trim($this->input->post('branch_address')),
                        "swift_code" => trim($this->input->post('swift_code')),
                        "routing_number" => trim($this->input->post('routing_number'))
                    );
                    $dataArr = array('banking' => $banking);
                    $this->driver_model->update_details(DRIVERS, $dataArr, array('_id' => new MongoDB\BSON\ObjectId($driver_id)));

                    $checkDriver = $this->driver_model->get_selected_fields(DRIVERS, array('_id' => new MongoDB\BSON\ObjectId($driver_id)), array('_id', 'banking'));
                    $bankingArr = array();
                    if (isset($checkDriver->row()->banking)) {
                        if (is_array($checkDriver->row()->banking)) {
                            $bankingArr = $checkDriver->row()->banking;
                        }
                    }
                    if (empty($bankingArr)) {
                        $bankingArr = json_decode("{}");
                    }
                    $returnArr['status'] = '1';
                    $returnArr['response'] = array('banking' => $bankingArr);
                } else {
                    $returnArr['response'] = $this->format_string("Driver not found", "driver_not_found");
                }
            } else {
                $returnArr['response'] = $this->format_string("Some Parameters Missing", "some_parameters_missing");
            }
        } catch (MongoException $ex) {
            $returnArr['response'] = $this->format_string("Error in connection", "error_in_connection");
        }
        $json_encode = json_encode($returnArr, JSON_PRETTY_PRINT);
        echo $this->cleanString($json_encode);
    }

    /**
     *
     * This Function returns the driver payment list
     *
     * */
    public function driver_all_payment_list() {
        $returnArr['status'] = '0';
        $returnArr['response'] = '';
        try {
            $driver_id = (string) $this->input->post('driver_id');

            if ($driver_id != '') {
                $driverVal = $this->app_model->get_selected_fields(DRIVERS, array('_id' => new MongoDB\BSON\ObjectId($driver_id)), array('city', 'avail_category'));
                if ($driverVal->num_rows() > 0) {
                    $total_payments = 5;
                    $paymentArr = array();
                    $billingDetails = $this->app_model->get_all_details(BILLINGS, array('driver_id' => $driver_id), array('bill_date' => 'DESC'));
                    if ($billingDetails->num_rows() > 0) {
                        foreach ($billingDetails->result() as $bill) {
                            $paymentArr[] = array('pay_id' => (string) $bill->invoice_id,
                                'pay_duration_from' => (string) get_time_to_string("d-m-Y", $bill->bill_from->toDateTime()->getTimestamp()),
                                'pay_duration_to' => (string) get_time_to_string("d-m-Y", $bill->bill_to->toDateTime()->getTimestamp()),
                                'amount' => (string) $bill->driver_earnings,
                                'pay_date' => (string) get_time_to_string("d-m-Y", $bill->bill_date->toDateTime()->getTimestamp())
                            );
                        }
                    }
                    if (empty($paymentArr)) {
                        $paymentArr = json_decode("{}");
                    }

                    $returnArr['status'] = '1';
                    $returnArr['response'] = array('total_payments' => (string) $total_payments, 'payments' => $paymentArr, 'currency' => (string) $this->data['dcurrencyCode']);
                } else {
                    $returnArr['response'] = $this->format_string("Driver not found", "driver_not_found");
                }
            } else {
                $returnArr['response'] = $this->format_string("Some Parameters are missing", "some_parameters_missing");
            }
        } catch (MongoException $ex) {
            $returnArr['response'] = $this->format_string("Error in connection", "error_in_connection");
        }
        $json_encode = json_encode($returnArr, JSON_PRETTY_PRINT);
        echo $this->cleanString($json_encode);
    }

    /**
     *
     * This Function returns the driver payment summary
     *
     * */
    public function view_driver_payment_information() {
        $returnArr['status'] = '0';
        $returnArr['response'] = '';
        try {
            $driver_id = (string) $this->input->post('driver_id');
            $invoice_id = (string) $this->input->post('pay_id');

            if ($driver_id != '' && $invoice_id != '') {
                $driverVal = $this->app_model->get_selected_fields(DRIVERS, array('_id' => new MongoDB\BSON\ObjectId($driver_id)), array('city', 'avail_category'));
                if ($driverVal->num_rows() > 0) {
                    $total_payments = 0;
                    $paymentArr = array();
                    $listsArr = array();

                    $billingDetails = $this->app_model->get_all_details(BILLINGS, array('invoice_id' => floatval($invoice_id)));
                    if ($billingDetails->num_rows() > 0) {
                        $paymentArr[] = array('pay_id' => (string) $billingDetails->row()->invoice_id,
                            'pay_duration_from' => (string) get_time_to_string("d-m-Y", $billingDetails->row()->bill_from->toDateTime()->getTimestamp()),
                            'pay_duration_to' => (string) get_time_to_string("d-m-Y", $billingDetails->row()->bill_to->toDateTime()->getTimestamp()),
                            'amount' => (string) $billingDetails->row()->driver_earnings,
                            'pay_date' => (string) get_time_to_string("d-m-Y", $billingDetails->row()->bill_date->toDateTime()->getTimestamp())
                        );

                        $ridesVal = $this->app_model->get_billing_rides($billingDetails->row()->bill_from->toDateTime()->getTimestamp(), $billingDetails->row()->bill_to->toDateTime()->getTimestamp(), $billingDetails->row()->driver_id);
                        if ($ridesVal->num_rows() > 0) {
                            $total_payments = $ridesVal->num_rows();
                            foreach ($ridesVal->result() as $rides) {
                                $listsArr[] = array('ride_id' => (string) $rides->ride_id,
                                    'amount' => (string) $rides->driver_revenue,
                                    'ride_date' => (string) date("d-m-Y", $rides->booking_information['pickup_date']->toDateTime()->getTimestamp())
                                );
                            }
                        }
                    }

                    if (empty($paymentArr)) {
                        $paymentArr = json_decode("{}");
                    }if (empty($listsArr)) {
                        $listsArr = json_decode("{}");
                    }
                    $returnArr['status'] = '1';
                    $returnArr['response'] = array('total_payments' => (string) $total_payments,
                        'payments' => $paymentArr,
                        'listsArr' => $listsArr,
                        'currency' => (string) $this->data['dcurrencyCode']
                    );
                } else {
                    $returnArr['response'] = $this->format_string("Driver not found", "driver_not_found");
                }
            } else {
                $returnArr['response'] = $this->format_string("Some Parameters are missing", "some_parameters_missing");
            }
        } catch (MongoException $ex) {
            $returnArr['response'] = $this->format_string("Error in connection", "error_in_connection");
        }
        $json_encode = json_encode($returnArr, JSON_PRETTY_PRINT);
        echo $this->cleanString($json_encode);
    }

    /**
     *
     * This Function complete the free trip 
     *
     * */
    public function trip_completed() {
        $returnArr['status'] = '0';
        $returnArr['response'] = '';
        try {
            $driver_id = (string) $this->input->post('driver_id');
            $ride_id = (string) $this->input->post('ride_id');


            if ($driver_id != '' && $ride_id != '') {
                $driverChek = $this->app_model->get_selected_fields(DRIVERS, array('_id' => new MongoDB\BSON\ObjectId($driver_id)), array());
                if ($driverChek->num_rows() > 0) {
                    $checkRide = $this->app_model->get_all_details(RIDES, array('ride_id' => $ride_id, 'driver.id' => $driver_id));
                    if ($checkRide->num_rows() == 1) {
                        $paid_amount = 0.00;
                        $pay_summary = array('type' => 'FREE');
                        $paymentInfo = array('ride_status' => 'Completed',
                            'pay_status' => 'Paid',
                            'history.pay_by_coupon_time' => new MongoDB\BSON\UTCDateTime(time()),
                            'total.paid_amount' => round(floatval($paid_amount), 2),
                            'pay_summary' => $pay_summary
                        );
                        $this->app_model->update_details(RIDES, $paymentInfo, array('ride_id' => $ride_id));

                        /* Update Stats Starts */
                        $current_date = new MongoDB\BSON\UTCDateTime(strtotime(date("Y-m-d 00:00:00")));
                        $field = array('ride_completed.hour_' . date('H') => 1, 'ride_completed.count' => 1);
                        $this->app_model->update_stats(array('day_hour' => $current_date), $field, 1);
                        /* Update Stats End */

                        $avail_data = array('mode' => 'Available', 'availability' => 'Yes');
                        $this->app_model->update_details(DRIVERS, $avail_data, array('_id' => new MongoDB\BSON\ObjectId($driver_id)));
                        $trans_id = time() . rand(0, 2578);
                        $transactionArr = array('type' => 'coupon',
                            'amount' => floatval($paid_amount),
                            'trans_id' => $trans_id,
                            'trans_date' => new MongoDB\BSON\UTCDateTime(time())
                        );
                        $this->app_model->simple_push(PAYMENTS, array('ride_id' => $ride_id), array('transactions' => $transactionArr));

                        $user_id = $checkRide->row()->user['id'];
                        $userVal = $this->driver_model->get_selected_fields(USERS, array('_id' => new MongoDB\BSON\ObjectId($user_id)), array('_id', 'user_name', 'email', 'image', 'avg_review', 'phone_number', 'country_code', 'push_type', 'push_notification_key'));
                        if (isset($userVal->row()->push_type)) {
                            if ($userVal->row()->push_type != '') {
                                $message = $this->format_string("your billing amount paid successfully", "your_billing_amount_paid");
                                $options = array('ride_id' => (string) $ride_id, 'user_id' => (string) $user_id);
                                if ($userVal->row()->push_type == 'ANDROID') {
                                    if (isset($userVal->row()->push_notification_key['gcm_id'])) {
                                        if ($userVal->row()->push_notification_key['gcm_id'] != '') {
                                            $this->sendPushNotification(array($userVal->row()->push_notification_key['gcm_id']), $message, 'payment_paid', 'ANDROID', $options, 'USER');
                                        }
                                    }
                                }
                                if ($userVal->row()->push_type == 'IOS') {
                                    if (isset($userVal->row()->push_notification_key['ios_token'])) {
                                        if ($userVal->row()->push_notification_key['ios_token'] != '') {
                                            $this->sendPushNotification(array($userVal->row()->push_notification_key['ios_token']), $message, 'payment_paid', 'IOS', $options, 'USER');
                                        }
                                    }
                                }
                            }
                        }
                        #	make and sending invoice to the rider 	#
                        $this->app_model->update_ride_amounts($ride_id);
                        $fields = array(
                            'ride_id' => (string) $ride_id
                        );
                        $url = base_url() . 'prepare-invoice';
                        $this->load->library('curl');
                        $output = $this->curl->simple_post($url, $fields);


                        $returnArr['status'] = '1';
                        $returnArr['response'] = $this->format_string('Ride Completed', 'ride_completed');
                    } else {
                        $returnArr['response'] = $this->format_string("Invalid Ride", "invalid_ride");
                    }
                } else {
                    $returnArr['response'] = $this->format_string("Driver not found", "driver_not_found");
                }
            } else {
                $returnArr['response'] = $this->format_string("Some Parameters Missing", "some_parameters_missing");
            }
        } catch (MongoException $ex) {
            $returnArr['response'] = $this->format_string("Error in connection", "error_in_connection");
        }
        $json_encode = json_encode($returnArr, JSON_PRETTY_PRINT);
        echo $this->cleanString($json_encode);
    }

    /**
     *
     * This Function returns the trip payment process
     *
     * */
    public function check_trip_payment_status() {
        $responseArr['status'] = '0';
        $responseArr['response'] = '';
        try {
            $driver_id = $this->input->post('driver_id');
            $ride_id = $this->input->post('ride_id');
            if ($driver_id != '' && $ride_id != '') {
                $checkDriver = $this->app_model->get_selected_fields(DRIVERS, array('_id' => new MongoDB\BSON\ObjectId($driver_id)), array());
                if ($checkDriver->num_rows() == 1) {
                    $checkRide = $this->app_model->get_selected_fields(RIDES, array('ride_id' => $ride_id, 'driver.id' => $driver_id), array('ride_id', 'ride_status', 'pay_status', 'booking_information', 'driver_review_status'));
                    if ($checkRide->num_rows() == 1) {
                        $trip_waiting = 'Yes';
                        $ratting_submited = 'No';
                        if ($checkRide->row()->ride_status == 'Completed') {
                            $trip_waiting = 'No';
                        }
                        if ($checkRide->row()->ride_status == 'Finished') {
                            $trip_waiting = 'Yes';
                        }

                        if ($trip_waiting == 'Yes') {
                            if (isset($checkRide->row()->driver_review_status)) {
                                if ($checkRide->row()->driver_review_status == 'Yes') {
                                    $ratting_submited = 'Yes';
                                }
                            }
                        }
                        if ($ratting_submited == 'Yes') {
                            $ratting_pending = 'No';
                        } else {
                            $ratting_pending = 'Yes';
                        }

                        $responseArr['status'] = '1';
                        $responseArr['response'] = array('trip_waiting' => (string) $trip_waiting,
                            'ratting_pending' => (string) $ratting_pending,
                        );
                    } else {
                        $responseArr['response'] = $this->format_string('Invalid Ride', 'invalid_ride');
                    }
                } else {
                    $responseArr['response'] = $this->format_string('Authentication Failed', 'authentication_failed');
                }
            } else {
                $responseArr['response'] = $this->format_string("Some Parameters are missing", "some_parameters_missing");
            }
        } catch (MongoException $ex) {
            $responseArr['response'] = $this->format_string('Error in connection', 'error_in_connection');
        }
        $json_encode = json_encode($responseArr, JSON_PRETTY_PRINT);
        echo $this->cleanString($json_encode);
    }

    /**
     *
     * Deduct the automatic payment for a trip while end the trip
     *
     * */
    public function auto_payment_deduct($ride_id = '') {
        $rideinfoUpdated = $this->app_model->get_all_details(RIDES, array('ride_id' => $ride_id));
        $bayMethod = '';
        if ($rideinfoUpdated->num_rows() == 1) {
            $user_id = $rideinfoUpdated->row()->user['id'];
            $wallet_amount = $this->app_model->get_all_details(WALLET, array('user_id' => new MongoDB\BSON\ObjectId($user_id)));
            $total_grand_fare = $rideinfoUpdated->row()->total['grand_fare'];
            if ($wallet_amount->num_rows() > 0) {
                if ($total_grand_fare <= $wallet_amount->row()->total) {
                    $bayMethod = 'wallet';
                } else {
                    $bayMethod = 'stripe';
                }
            } else {
                $bayMethod = 'stripe';
            }
            $is_completed = 'No';
            if ($bayMethod == 'wallet') {
                $bal_walletamount = ($wallet_amount->row()->total - $total_grand_fare);
                $walletamount = array('total' => floatval($bal_walletamount));
                $this->app_model->update_details(WALLET, $walletamount, array('user_id' => new MongoDB\BSON\ObjectId($user_id)));
                $txn_time = time() . rand(0, 2578);
                $initialAmt = array('type' => 'DEBIT',
                    'debit_type' => 'payment',
                    'ref_id' => $ride_id,
                    'trans_amount' => floatval($total_grand_fare),
                    'avail_amount' => floatval($bal_walletamount),
                    'trans_date' => new MongoDB\BSON\UTCDateTime(time()),
                    'trans_id' => $txn_time
                );
                $this->app_model->simple_push(WALLET, array('user_id' => new MongoDB\BSON\ObjectId($user_id)), array('transactions' => $initialAmt));
                $is_completed = 'Yes';
            } else if ($bayMethod == 'stripe') {
                $stripe_settings = $this->data['stripe_settings'];
                if ($stripe_settings['status'] == 'Enable') {
                    $getUsrCond = array('_id' => new MongoDB\BSON\ObjectId($user_id));
                    $get_user_info = $this->app_model->get_selected_fields(USERS, $getUsrCond, array('email', 'stripe_customer_id'));
                    $email = $get_user_info->row()->email;
                    $stripe_customer_id = '';
                    $auto_pay_status = 'No';
                    if (isset($get_user_info->row()->stripe_customer_id)) {
                        $stripe_customer_id = $get_user_info->row()->stripe_customer_id;
                        if ($stripe_customer_id != '') {
                            $auto_pay_status = 'Yes';
                        }
                    }

                    if ($auto_pay_status == 'Yes') {
                        require_once('./stripe/lib/Stripe.php');

                        $stripe_settings = $this->data['stripe_settings'];
                        $secret_key = $stripe_settings['settings']['secret_key'];
                        $publishable_key = $stripe_settings['settings']['publishable_key'];

                        $stripe = array(
                            "secret_key" => $secret_key,
                            "publishable_key" => $publishable_key
                        );
                        $description = ucfirst($this->config->item('email_title')) . ' - trip payment';


                        $currency = $this->data['dcurrencyCode'];
                        if (isset($rideinfoUpdated->row()->currency))
                            $currency = $rideinfoUpdated->row()->currency;
                        $amounts = $this->get_stripe_currency_smallest_unit($total_grand_fare, $currency);

                        Stripe::setApiKey($secret_key);


                        try {
                            if ($stripe_customer_id != '') {
                                // Charge the Customer instead of the card
                                $charge = Stripe_Charge::create(array(
                                            "amount" => $amounts, # amount in cents, again
                                            "currency" => $currency,
                                            "customer" => $stripe_customer_id,
                                            "description" => $description)
                                );

                                $paymentData = array('user_id' => $user_id,
                                    'ride_id' => $ride_id,
                                    'payType' => 'stripe',
                                    'stripeTxnId' => $charge['id']
                                );
                                $is_completed = 'Yes';
                                $strip_txnid = $charge['id'];
                            }
                        } catch (Exception $e) {
                            $error = $e->getMessage();
                        }
                    }
                }
            }


            if ($is_completed == 'Yes') {
                ###	Update into the ride and driver collection ###
                if ($rideinfoUpdated->row()->pay_status == 'Pending' || $rideinfoUpdated->row()->pay_status == 'Processing') {
                    if (isset($rideinfoUpdated->row()->total)) {
                        if (isset($rideinfoUpdated->row()->total['grand_fare'])) {
                            $paid_amount = round($rideinfoUpdated->row()->total['grand_fare'], 2);
                        }
                    }
                    if ($bayMethod == 'stripe') {
                        $pay_summary = 'Gateway';
                        $trans_id = $strip_txnid;
                        $type = 'Card';
                    } else if ($bayMethod == 'wallet') {
                        $pay_summary = 'Wallet';
                        $trans_id = $txn_time;
                        $type = 'wallet';
                    }
                    $pay_summary = array('type' => $pay_summary);
                    $paymentInfo = array('ride_status' => 'Completed',
                        'pay_status' => 'Paid',
                        'total.paid_amount' => round(floatval($paid_amount), 2),
                        'pay_summary' => $pay_summary
                    );
                    if ($bayMethod == 'stripe') {
                        $paymentInfo['history.pay_by_gateway_time'] = new MongoDB\BSON\UTCDateTime(time());
                    } else if ($bayMethod == 'wallet') {
                        $paymentInfo['history.wallet_usage_time'] = new MongoDB\BSON\UTCDateTime(time());
                    }
                    $this->app_model->update_details(RIDES, $paymentInfo, array('ride_id' => $ride_id));
                    /* Update Stats Starts */
                    $current_date = new MongoDB\BSON\UTCDateTime(strtotime(date("Y-m-d 00:00:00")));
                    $field = array('ride_completed.hour_' . date('H') => 1, 'ride_completed.count' => 1);
                    $this->app_model->update_stats(array('day_hour' => $current_date), $field, 1);
                    /* Update Stats End */
                    $avail_data = array('mode' => 'Available', 'availability' => 'Yes');
                    $driver_id = $rideinfoUpdated->row()->driver['id'];
                    $this->app_model->update_details(DRIVERS, $avail_data, array('_id' => new MongoDB\BSON\ObjectId($driver_id)));
                    $transactionArr = array('type' => $type,
                        'amount' => floatval($paid_amount),
                        'trans_id' => $trans_id,
                        'trans_date' => new MongoDB\BSON\UTCDateTime(time())
                    );
                    $this->app_model->simple_push(PAYMENTS, array('ride_id' => $ride_id), array('transactions' => $transactionArr));
                }
            }
        }
    }

    /**
     *
     * This Function returns the trip information to drivers
     *
     * */
    public function get_trip_information() {
        $responseArr['status'] = '0';
        $responseArr['response'] = '';
        try {
            $driver_id = $this->input->post('driver_id');
            $ride_id = $this->input->post('ride_id');
            if ($driver_id != '') {
                $this->load->helper('ride_helper');
                get_trip_information($driver_id);
                exit;
            } else {
                $responseArr['response'] = $this->format_string("Some Parameters are missing", "some_parameters_missing");
            }
        } catch (MongoException $ex) {
            $responseArr['response'] = $this->format_string('Error in connection', 'error_in_connection');
        }
        $json_encode = json_encode($responseArr, JSON_PRETTY_PRINT);
        echo $this->cleanString($json_encode);
    }

    /**
     *
     * 	This function will update the information for rides request acknowledgement
     *
     * */
    public function ack_ride_request() {
        $returnArr['status'] = '0';
        $returnArr['response'] = '';

        try {
            $driver_id = (string) $this->input->post('driver_id');
            $ride_id = (string) $this->input->post('ride_id');

            if ($driver_id != '' && $ride_id != '') {
                $checkDriver = $this->app_model->get_selected_fields(DRIVERS, array('_id' => new MongoDB\BSON\ObjectId($driver_id)), array('email'));
                if ($checkDriver->num_rows() == 1) {
                    $checkRide = $this->app_model->get_selected_fields(RIDES, array('ride_id' => $ride_id), array('ride_id', 'ride_status'));
                    if ($checkRide->num_rows() == 1) {

                        $returnArr['status'] = '1';
                        $returnArr['response'] = $this->format_string("Request Acknowledged", "request_acknowledged");
                    } else {
                        $returnArr['response'] = $this->format_string("Invalid Ride", "invalid_ride");
                    }
                } else {
                    $returnArr['response'] = $this->format_string("Driver not found", "driver_not_found");
                }
            } else {
                $returnArr['response'] = $this->format_string("Some Parameters Missing", "some_parameters_missing");
            }
        } catch (MongoException $ex) {
            $returnArr['response'] = $this->format_string("Error in connection", "error_in_connection");
        }
        $json_encode = json_encode($returnArr, JSON_PRETTY_PRINT);
        echo $this->cleanString($json_encode);
    }

    /**
     *
     * 	This function will update the information for denied rides
     *
     * */
    public function deny_ride_request() {
        $returnArr['status'] = '0';
        $returnArr['response'] = '';

        try {
            $driver_id = (string) $this->input->post('driver_id');
            $ride_id = (string) $this->input->post('ride_id');

            if ($driver_id != '' && $ride_id != '') {
                $checkDriver = $this->app_model->get_selected_fields(DRIVERS, array('_id' => new MongoDB\BSON\ObjectId($driver_id)), array('email'));
                if ($checkDriver->num_rows() == 1) {
                    $checkRide = $this->app_model->get_selected_fields(RIDES, array('ride_id' => $ride_id), array('ride_id', 'ride_status'));
                    if ($checkRide->num_rows() == 1) {
                        $returnArr['status'] = '1';
                        $returnArr['response'] = $this->format_string("Request Denied Successfully", "request_denied_successfully");
                    } else {
                        $returnArr['response'] = $this->format_string("Invalid Ride", "invalid_ride");
                    }
                } else {
                    $returnArr['response'] = $this->format_string("Driver not found", "driver_not_found");
                }
            } else {
                $returnArr['response'] = $this->format_string("Some Parameters Missing", "some_parameters_missing");
            }
        } catch (MongoException $ex) {
            $returnArr['response'] = $this->format_string("Error in connection", "error_in_connection");
        }
        $json_encode = json_encode($returnArr, JSON_PRETTY_PRINT);
        echo $this->cleanString($json_encode);
    }

    /**
     *
     * 	This function will update the driver location and send the location to user by notifications
     *
     * */
    public function driver_update_ride_location() {
        $returnArr['status'] = '0';
        $returnArr['response'] = '';

        try {
            $driver_id = (string) $this->input->post('driver_id');
            $ride_id = (string) $this->input->post('ride_id');
            $lat = (string) $this->input->post('lat');
            $lon = (string) $this->input->post('lon');
            $bearing = (string) $this->input->post('bearing');

            if ($driver_id != '' && $ride_id != '' && $lat != '' && $lon != '') {
                $checkDriver = $this->app_model->get_selected_fields(DRIVERS, array('_id' => new MongoDB\BSON\ObjectId($driver_id)), array('email'));
                if ($checkDriver->num_rows() == 1) {
                    $checkRide = $this->app_model->get_selected_fields(RIDES, array('ride_id' => $ride_id), array('ride_id', 'ride_status', 'user'));
                    if ($checkRide->num_rows() == 1) {
                        $ride_location[] = array('lat' => $lat,
                            'lon' => $lon,
                            'update_time' => new MongoDate(time())
                        );
                        $checkRideHistory = $this->app_model->get_selected_fields(RIDE_HISTORY, array('ride_id' => $ride_id), array('values'));
                        if ($checkRideHistory->num_rows() > 0) {
                            if (!empty($travel_historyArr)) {
                                $this->app_model->simple_push(RIDE_HISTORY, array('ride_id' => $ride_id), array('values' => $rowVal));
                            }
                        } else {
                            if (!empty($travel_historyArr)) {
                                $this->app_model->simple_insert(RIDE_HISTORY, array('ride_id' => $ride_id, 'values' => $travel_historyArr));
                            }
                        }
                        /* Notification to user about driver current ride location */
                        $user_id = $checkRide->row()->user['id'];
                        $userVal = $this->app_model->get_selected_fields(USERS, array('_id' => new MongoDB\BSON\ObjectId($user_id)), array('_id', 'user_name', 'email', 'image', 'avg_review', 'phone_number', 'country_code', 'push_type', 'push_notification_key'));
                        if (isset($userVal->row()->push_type)) {
                            if ($userVal->row()->push_type != '') {
                                $message = $this->format_string('Driver current ride location', 'driver_curr_ride_loc', '', 'user', (string) $userVal->row()->_id);
                                $options = array("action" => "driver_loc", 'ride_id' => (string) $ride_id, 'latitude' => (string) $lat, 'longitude' => (string) $lon, 'bearing' => (string) $bearing);
                                if ($userVal->row()->push_type == 'ANDROID') {
                                    if (isset($userVal->row()->push_notification_key['gcm_id'])) {
                                        if ($userVal->row()->push_notification_key['gcm_id'] != '') {
                                            $this->sendPushNotification(array($userVal->row()->push_notification_key['gcm_id']), $message, 'driver_loc', 'ANDROID', $options, 'USER');
                                        }
                                    }
                                }
                                if ($userVal->row()->push_type == 'IOS') {
                                    if (isset($userVal->row()->push_notification_key['ios_token'])) {
                                        if ($userVal->row()->push_notification_key['ios_token'] != '') {
                                            $this->sendPushNotification(array($userVal->row()->push_notification_key['ios_token']), $message, 'driver_loc', 'IOS', $options, 'USER');
                                        }
                                    }
                                }
                            }
                        }
                        $returnArr['status'] = '1';
                        $returnArr['response'] = $this->format_string("Updated Successfully", "updated_successfully");
                    } else {
                        $returnArr['response'] = $this->format_string("Invalid Ride", "invalid_ride");
                    }
                } else {
                    $returnArr['response'] = $this->format_string("Driver not found", "driver_not_found");
                }
            } else {
                $returnArr['response'] = $this->format_string("Some Parameters Missing", "some_parameters_missing");
            }
        } catch (MongoException $ex) {
            $returnArr['response'] = $this->format_string('Error in connection', 'error_in_connection');
        }
        $json_encode = json_encode($returnArr, JSON_PRETTY_PRINT);
        echo $this->cleanString($json_encode);
    }

    /**
     *
     * This Function returns the driver dashboard
     *
     * */
    public function driver_dashboard() {
        $responseArr['status'] = '0';
        $responseArr['response'] = '';
        try {
            $driver_id = $this->input->post('driver_id');
            $driver_lat = $this->input->post('driver_lat');
            $driver_lon = $this->input->post('driver_lon');
            if ($driver_id != '' && $driver_lat != '' && $driver_lon != '') {
                $checkDriver = $this->app_model->get_selected_fields(DRIVERS, array('_id' => new MongoDB\BSON\ObjectId($driver_id)), array('driver_name', 'image', 'avg_review', 'email', 'dail_code', 'mobile_number', 'driver_commission', 'loc', 'category', 'availability', 'mode', 'driver_location'));
                if ($checkDriver->num_rows() == 1) {
                    $driver_image = USER_PROFILE_IMAGE_DEFAULT;
                    if (isset($checkDriver->row()->image)) {
                        if ($checkDriver->row()->image != '') {
                            $driver_image = USER_PROFILE_IMAGE . $checkDriver->row()->image;
                        }
                    }
                    $driver_review = 0;
                    if (isset($checkDriver->row()->avg_review)) {
                        $driver_review = $checkDriver->row()->avg_review;
                    }
                    $availability = 'No';
                    if (isset($checkDriver->row()->availability)) {
                        $availability = $checkDriver->row()->availability;
                    }
                    $availability_string = 'Yes';
                    $ride_status_string = 'No';
                    if ($checkDriver->row()->mode == 'Available') {
                        $availability_string = 'Yes';
                    } else if ($checkDriver->row()->mode == 'Booked') {
                        $checkPending = $this->app_model->get_uncompleted_trips($driver_id, array('ride_id', 'ride_status', 'pay_status'));
                        if ($checkPending->num_rows() > 0) {
                            if ($checkPending->row()->ride_status == 'Onride') {
                                $ride_status_string = 'Yes';
                            }
                            $availability_string = 'No';
                        } else {
                            $availability_string = 'Yes';
                        }
                    }
                    /* $driver_lat = $checkDriver->row()->loc['lat'];
                      $driver_lon = $checkDriver->row()->loc['lon']; */
                    /* $vehicleInfo = $this->driver_model->get_selected_fields(MODELS, array('_id' => new MongoDB\BSON\ObjectId($checkDriver->row()->vehicle_model)), array('_id', 'name', 'brand_name'));
                      $vehicle_model = '';
                      if ($vehicleInfo->num_rows() > 0) {
                      $vehicle_model = $vehicleInfo->row()->name;
                      }
                      $categoryInfo = $this->driver_model->get_selected_fields(CATEGORY, array('_id' => new MongoDB\BSON\ObjectId($checkDriver->row()->category)), array('_id', 'name', 'brand_name', 'icon_car_image','name_languages'));
                      $driver_category = '';
                      $category_icon = base_url().ICON_MAP_CAR_IMAGE;
                      if ($categoryInfo->num_rows() > 0) {
                      $driver_category = $categoryInfo->row()->name;
                      if(isset($categoryInfo->row()->name_languages)){
                      $langKey = $this->data['sms_lang_code'];
                      $arrVal = $categoryInfo->row()->name_languages;
                      if(array_key_exists($langKey,$arrVal)){
                      if($categoryInfo->row()->name_languages[$langKey]!=""){
                      $driver_category = $categoryInfo->row()->name_languages[$langKey];
                      }
                      }
                      }
                      if(isset($categoryInfo->row()->icon_car_image)){
                      $category_icon = base_url() . ICON_IMAGE . $categoryInfo->row()->icon_car_image;
                      }

                      } */

                    $last_trip = array();
                    $checkTrip = $this->app_model->get_all_details(RIDES, array('driver.id' => $driver_id, 'ride_status' => "Completed", "pay_status" => "Paid"), array("_id" => "DESC"));
                    if ($checkTrip->num_rows() > 0) {
                        $last_trip = array("ride_time" => get_time_to_string("h:i A", $checkTrip->row()->booking_information['drop_date']->toDateTime()->getTimestamp()),
                            "ride_date" => get_time_to_string("jS M, Y", $checkTrip->row()->booking_information['drop_date']->toDateTime()->getTimestamp()),
                            "earnings" => (string) number_format($checkTrip->row()->driver_revenue, 2),
                            "currency" => (string) $this->data['dcurrencyCode']
                        );
                    }

                    $today_earnings = array();
                    $checkRide = $this->app_model->get_today_rides($driver_id);
                    if (!empty($checkRide['result'])) {
                        $online_hours = $checkRide['result'][0]['freeTime'] + $checkRide['result'][0]['tripTime'] + $checkRide['result'][0]['waitTime'];
                        $online_hours_txt = '0 hours';
                        if ($online_hours > 0) {
                            if ($online_hours >= 60) {
                                $online_hours_in_hrs = ($online_hours / 60);
                                $online_hours_txt = round($online_hours_in_hrs, 2) . ' hours';
                            } else {
                                $online_hours_txt = $online_hours . ' minutes';
                            }
                        }
                        $mins = $this->format_string('min', 'min_short');
                        $mins_short = $this->format_string('mins', 'mins_short');
                        if ($checkRide['result'][0]['ridetime'] > 1) {
                            $min_unit = $mins_short;
                        } else {
                            $min_unit = $mins;
                        }
                        $trip = $this->format_string('trip', 'trip_singular');
                        $trips = $this->format_string('trips', 'trip_plural');
                        if ($checkRide['result'][0]['totalTrips'] > 1) {
                            $trip_unit = $trips;
                        } else {
                            $trip_unit = $trip;
                        }

                        $today_earnings = array("online_hours" => (string) $checkRide['result'][0]['ridetime'] . ' ' . $min_unit,
                            "trips" => (string) $checkRide['result'][0]['totalTrips'],
                            "earnings" => (string) number_format($checkRide['result'][0]['driverAmount'], 2),
                            "currency" => (string) $this->data['dcurrencyCode'],
                            "trip_unit" => (string) $trip_unit
                        );
                    }
                    $today_tips = array();
                    $todayTips = $this->app_model->get_today_tips($driver_id);
                    if (!empty($todayTips['result'])) {
                        $today_tips = array("trips" => (string) $todayTips['result'][0]['totalTrips'],
                            "tips" => (string) number_format($todayTips['result'][0]['tipsAmount'], 2),
                            "currency" => (string) $this->data['dcurrencyCode']
                        );
                    }

                    if (empty($last_trip)) {
                        $last_trip = json_decode("{}");
                    }
                    if (empty($today_earnings)) {
                        $today_earnings = json_decode("{}");
                    }
                    if (empty($today_tips)) {
                        $today_tips = json_decode("{}");
                    }

                    $go_online_status = "0";
                    $go_online_string = $this->format_string('Currently you can\'t able to service in this location', 'driver_cannot_service_in_this_location');
                    $driver_location = $checkDriver->row()->driver_location;
                    $service_location = $this->app_model->find_location(floatval($driver_lon), floatval($driver_lat));
                    if (!empty($service_location['result'])) {
                        $service_location_arr = array_column($service_location['result'], '_id');
                        $sArr = array();
                        foreach ($service_location_arr as $locs) {
                            $sArr[] = (string) $locs;
                        }
                        if (in_array($driver_location, $sArr)) {
                            $go_online_status = "1";
                            $go_online_string = "";
                            /* $service_location_id = (string)$service_location['result'][0]['_id'];
                              if($service_location_id==$driver_location){
                              $go_online_status = "1";
                              $go_online_string = "";
                              } */
                        }
                    }



                    $driver_dashboard = array("currency" => (string) $this->data['dcurrencyCode'],
                        'driver_id' => (string) $checkDriver->row()->_id,
                        'availability' => (string) $availability,
                        'driver_status' => (string) $checkDriver->row()->availability,
                        'driver_name' => (string) $checkDriver->row()->driver_name,
                        'driver_email' => (string) $checkDriver->row()->email,
                        'driver_image' => (string) base_url() . $driver_image,
                        'driver_review' => (string) floatval($driver_review),
                        'driver_lat' => (string) floatval($driver_lat),
                        'driver_lon' => (string) floatval($driver_lon),
                        'phone_number' => (string) $checkDriver->row()->dail_code . $checkDriver->row()->mobile_number,
                        'last_trip' => $last_trip,
                        'today_earnings' => $today_earnings,
                        'today_tips' => $today_tips,
                        'availability_string' => (string) $availability_string,
                        'ride_status_string' => (string) $ride_status_string,
                        'category_icon' => (string) $category_icon,
                        'go_online_status' => (string) $go_online_status,
                        'go_online_string' => (string) $go_online_string
                    );

                    $responseArr['status'] = '1';
                    $responseArr['response'] = $driver_dashboard;
                } else {
                    $responseArr['response'] = $this->format_string('Authentication Failed', 'authentication_failed');
                }
            } else {
                $responseArr['response'] = $this->format_string("Some Parameters are missing", "some_parameters_missing");
            }
        } catch (MongoException $ex) {
            $responseArr['response'] = $this->format_string('Error in connection', 'error_in_connection');
        }
        $json_encode = json_encode($responseArr, JSON_PRETTY_PRINT);
        echo $this->cleanString($json_encode);
    }

    /**
     *
     * This function changes the driver password
     *
     * */
    public function change_password() {
        $responseArr['status'] = '0';
        $responseArr['response'] = '';
        try {
            $driver_id = $this->input->post('driver_id');
            $password = $this->input->post('password');
            $new_password = (string) $this->input->post('new_password');

            if ($driver_id != '' && $password != '' && $new_password != '') {
                $checkDriver = $this->app_model->get_selected_fields(DRIVERS, array('_id' => new MongoDB\BSON\ObjectId($driver_id)), array('password'));
                if ($checkDriver->num_rows() == 1) {
                    if (strlen($new_password) >= 6) {
                        if ($checkDriver->row()->password == md5($password)) {
                            $condition = array('_id' => new MongoDB\BSON\ObjectId($driver_id));
                            $dataArr = array('password' => md5($new_password));
                            $this->app_model->update_details(DRIVERS, $dataArr, $condition);
                            $responseArr['status'] = '1';
                            $responseArr['response'] = $this->format_string('Password changed successfully.', 'password_changed');
                        } else {
                            $responseArr['response'] = $this->format_string('Your current password is not matching.', 'password_not_matching');
                        }
                    } else {
                        $responseArr['response'] = $this->format_string('Password should be at least 6 characters.', 'password_should_be_6_characters');
                    }
                } else {
                    $responseArr['response'] = $this->format_string('Authentication Failed', 'authentication_failed');
                }
            } else {
                $responseArr['response'] = $this->format_string("Some Parameters are missing.", "some_parameters_missing");
            }
        } catch (MongoException $ex) {
            $responseArr['response'] = $this->format_string('Error in connection', 'error_in_connection');
        }
        $json_encode = json_encode($responseArr, JSON_PRETTY_PRINT);
        echo $this->cleanString($json_encode);
    }

    /**
     *
     * This function forgot driver password request
     *
     * */
    public function forgot_password() {
        $responseArr['status'] = '0';
        $responseArr['response'] = '';
        try {
            $email = $this->input->post('email');
            if ($email != '') {
                $checkDriver = $this->app_model->get_selected_fields(DRIVERS, array('email' => $email), array('password', 'driver_name', 'email'));
                if ($checkDriver->num_rows() == 1) {
                    $new_pwd = $this->get_rand_str('6') . time();
                    $newdata = array('reset_id' => $new_pwd);
                    $condition = array('email' => $email);
                    $this->app_model->update_details(DRIVERS, $newdata, $condition);
                    $this->send_driver_pwd($new_pwd, $checkDriver);
                    $responseArr['status'] = '1';
                    $responseArr['response'] = $this->format_string('Password reset link has been sent to your email address.', 'password_reset_link_sent');
                } else {
                    $responseArr['response'] = $this->format_string('Email id does not match our records', 'record_not_have');
                }
            } else {
                $responseArr['response'] = $this->format_string("Some Parameters are missing.", "some_parameters_missing");
            }
        } catch (MongoException $ex) {
            $responseArr['response'] = $this->format_string('Error in connection', 'error_in_connection');
        }
        $json_encode = json_encode($responseArr, JSON_PRETTY_PRINT);
        echo $this->cleanString($json_encode);
    }

    /**
     *
     * This function send the new password to driver email
     *
     * */
    public function send_driver_pwd($pwd = '', $query) {
        $newsid = '10';
        $reset_url = base_url() . 'driver/reset-password-form/' . $pwd;
        $user_name = $query->row()->driver_name;
        $template_values = $this->app_model->get_email_template($newsid, $this->data['langCode']);
        $subject = 'From: ' . $this->config->item('email_title') . ' - ' . $template_values['subject'];
        $drivernewstemplateArr = array('email_title' => $this->config->item('email_title'), 'mail_emailTitle' => $this->config->item('email_title'), 'mail_logo' => $this->config->item('logo_image'), 'mail_footerContent' => $this->config->item('footer_content'), 'mail_metaTitle' => $this->config->item('meta_title'), 'mail_contactMail' => $this->config->item('site_contact_mail'));
        extract($drivernewstemplateArr);
        $message = '<!DOCTYPE HTML>
			<html>
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
			<meta name="viewport" content="width=device-width"/>
			<title>' . $subject . '</title>
			<body>';
        include($template_values['templateurl']);
        $message .= '</body>
			</html>';
        $sender_email = $this->config->item('site_contact_mail');
        $sender_name = $this->config->item('email_title');

        $email_values = array('mail_type' => 'html',
            'from_mail_id' => $sender_email,
            'mail_name' => $sender_name,
            'to_mail_id' => $query->row()->email,
            'subject_message' => 'Password Reset',
            'body_messages' => $message
        );
        $email_send_to_common = $this->app_model->common_email_send($email_values);
    }

    ////////////////////tree api


    public function driver_student_tree() {
        $responseArr['status'] = '0';
        $responseArr['response'] = '';
        try {

            $driver_id = $this->input->post('driver_id');
            $trip_id = $this->input->post('trip_id');




            $bus_route_details = $this->bus_trip_model->get_all_details(BUS_TRIP, array('_id' => new MongoDB\BSON\ObjectId($trip_id)));




            $bus_route_details = $bus_route_details->result();


            if (count($bus_route_details) > 0 && is_object($bus_route_details[0]->route_details))
                $bus_route_details[0]->{"route_details"} = (array) $bus_route_details[0]->route_details;

            $true_route_details = $this->app_model->get_all_details(BUS_ROUTE, array('_id' => new MongoDB\BSON\ObjectId($bus_route_details[0]->route_details['route_id'])))->result();



            //var_dump($bus_route_id);




            $route_stops = $bus_route_details[0]->route_details;




            $student_final = array();

            $counter = 0;
            //adding student details
            foreach ($route_stops['waypoints_details'] as $key => $value) {

                if (is_object($value))
                    $value = (array) $value;
                if ($bus_route_details[0]->trip_type == "drop") {

                    $student_details = $this->student_details_model->get_all_details(STUDENT_DETAILS, array('student_drop_bus_stop_id' => new MongoDB\BSON\ObjectId($value['id'])));
                    $student_details = $student_details->result();
                } else if ($bus_route_details[0]->trip_type == "pickup") {
                    $student_details = $this->student_details_model->get_all_details(STUDENT_DETAILS, array('student_pickup_bus_stop_id' => new MongoDB\BSON\ObjectId($value['id'])));
                    $student_details = $student_details->result();
                }


                $available_students_count = 0;


                $student_final = array();

                $counter2 = 0;
                $current_date = date("Y-m-d");

                foreach ($student_details as $key => $data) {

                    $available_students_count++;

                    ///////student attendance status
                    $stud_status = "present";
                    if (is_object($student_details[$counter2]->student_att_status))
                        $student_details[$counter2]->{"student_att_status"} = (array) $student_details[$counter2]->student_att_status;
                    if (strtotime(date('Y-m-d', $student_details[$counter2]->student_att_status["from_date"]->toDateTime()->getTimestamp())) <= strtotime($current_date) && strtotime(date('Y-m-d', $student_details[$counter2]->student_att_status["to_date"]->toDateTime()->getTimestamp())) >= strtotime($current_date)) {
                        $stud_status = $student_details[$counter2]->student_att_status["status"];

                        if ($stud_status == "absent") {
                            $available_students_count--;
                        }


                        if (null !== $student_details[$counter2]->student_att_status['absent_half_availability'] && $student_details[$counter2]->student_att_status['absent_type'] == "half") {


                            if ($student_details[$counter2]->student_att_status['absent_half_availability'] == "pickup") {
                                if ($bus_route_details[0]->trip_type == "drop" && $stud_status == "absent") {
                                    $stud_status = "present";
                                    $available_students_count++;
                                }
                            }
                        }
                    }




                    $student_final[$counter2] = array('student_attr'=>$student_details[$counter2]->student_att_status,'student_id' => $student_details[$counter2]->_id, 'student_name' => $data->student_first_name, 'status' => $stud_status);
                    $counter2++;
                }

                $stop_time = date("g:i a", strtotime($value['stop_time']));

                if ($value['stop_time'] == "-") {
                    $stop_time = '-';
                }

                $stop_with_stud[$counter] = array("stop_time" => strtoupper($stop_time), "stop_id" => $value['id'], "stop_name" => $value["busstop_name"], "stop_lat_long" => $value["waypoint"], "available_students_count" => $available_students_count, "student_details" => $student_final);

                $counter++;
            }




            $answer_data = array("trip_id" => $trip_id, "trip_type" => $bus_route_details[0]->trip_type, "trip_name" => $bus_route_details[0]->trip_name, 'driver_id' => $bus_route_details[0]->driver_id, "start_point" => $route_stops['start_point'], "end_point" => $route_stops['endpoint'], "poly_encode" => $true_route_details[0]->polyencode, "route_id" => $route_stops['route_id'], "stop_details_with_stud_list" => $stop_with_stud);


            $responseArr['response'] = $answer_data;

            $responseArr['status'] = '1';
        } catch (MongoException $ex) {
            $responseArr['response'] = $this->format_string('Error in connection', 'error_in_connection');
        }
        $json_encode = json_encode($responseArr, JSON_PRETTY_PRINT);
        echo $this->cleanString($json_encode);
    }

////////////trip_list



    public function trip_list() {
        $responseArr['status'] = '0';
        $responseArr['response'] = '';
        try {

            $driver_id = $this->input->post('driver_id');



            $current_date = date("Y-m-d");







            $this->data['trip_list'] = $this->bus_trip_model->get_all_details(BUS_TRIP, array("driver_id" => $driver_id), array('start_time' => 1));

            $array_data = $this->data['trip_list']->result();




            $operator_id = $this->data['trip_list']->result();

            $operator_id = $operator_id[0]->operator_id;


            $operators = $this->app_model->get_all_details(OPERATORS, array('_id' => new MongoDB\BSON\ObjectId($operator_id)))->result();
            $operator_name = "";
            if (!empty($operators)) {
                $operator_name = $operators[0]->operator_name;
            }
            $holidays = $this->app_model->get_all_details(YEAR_TIMELINE, array('operator_id' => $operator_id, 'year' => date("Y")))->result();

            if (!empty($holidays)) {
                $holidays = $holidays[0]->holidays;
            } else {
                $holidays = array("no_data");
            }

            $finaldata = array();


            foreach ($array_data as $key => $value) {

                ////holidays checking
                if (!in_array($current_date, $holidays)) {


                    if (strtotime(date('Y-m-d', $value->trip_start_date->toDateTime()->getTimestamp())) <= strtotime($current_date) && strtotime(date('Y-m-d', $value->trip_end_date->toDateTime()->getTimestamp())) >= strtotime($current_date)) {
                        $routes = $this->bus_trip_model->get_all_details(BUS_ROUTE, array("_id" => new MongoDB\BSON\ObjectId($value->route_details->route_id)));
                        $startpoints = array();
                        $endpoints = array();
                        $stops = array();
                        if (is_object($routes)) {
                            $routes_list = $routes->result();
                            foreach ($routes_list as $route) {
                                $startpoints = $route->startpoints;
                                $endpoints = $route->endpoints;
                                $stops = $route->waypoints;
                                break;
                            }
                        }
                        $student_count = 0;
                        if ($value->trip_type == "pickup") {

                            $student_list = $this->student_details_model->get_all_details(STUDENT_DETAILS, array('student_pickup_trip_id' => new MongoDB\BSON\ObjectId((string) $value->_id)))->result();
                        } elseif ($value->trip_type == "drop") {
                            $student_list = $this->student_details_model->get_all_details(STUDENT_DETAILS, array('student_drop_trip_id' => new MongoDB\BSON\ObjectId((string) $value->_id)))->result();
                        }
                        if (isset($student_list) && is_array($student_list))
                            $student_count = count($student_list);
                        $current_trip_data=$this->student_details_model->get_all_details(CURRENT_TRIP_DETAILS, array('trip_id' => new MongoDB\BSON\ObjectId((string) $value->_id),"start_current_date" => date('Y-m-d')))->result();
                        $array_data = array("current_trip"=>$current_trip_data,"student_count" => $student_count, "waypoints" => count($value->route_details->waypoints_details), "startpoints" => $startpoints, "endpoints" => $endpoints, "trip_name_time" => $value->trip_name . " - " . strtoupper(date("g:i a", strtotime($value->start_time))), "trip_name" => $value->trip_name, "trip_id" => (string) $value->{"_id"}, "trip_type" => $value->trip_type);

                        array_push($finaldata, $array_data);
                    }
                }
            }

            $responseArr['response'] = $finaldata;
            $responseArr['school_name'] = $operator_name;

            $responseArr['status'] = '1';
        } catch (MongoException $ex) {
            $responseArr['response'] = $this->format_string('Error in connection', 'error_in_connection');
        }
        $json_encode = json_encode($responseArr, JSON_PRETTY_PRINT);
        echo $this->cleanString($json_encode);
    }

////////////delay notification


    public function insert_delay_notifiction() {

        $responseArr['status'] = '0';
        $responseArr['response'] = '';
        try {


            $driver_id = $this->input->post('driver_id');

            $trip_id = $this->input->post('trip_id');

            $route_id = $this->input->post('route_id');

            $stop_id = $this->input->post('stop_id');

            $delay_message = $this->input->post('delay_message');

            $driver_lat_long = $this->input->post('lat_long');

            $stop_time = date("G:i", strtotime($this->input->post('stop_time')));
            ;

            $trip_type = $this->input->post('trip_type');





            $condition = array();

            if ($trip_type != "" && $driver_lat_long != "" && $driver_id != "" && $trip_id != "" && $route_id != "" && $stop_id != "" && $delay_message != "") {


                if ($trip_type == "pickup") {

                    $student_list = $this->student_details_model->get_all_details(STUDENT_DETAILS, array('student_pickup_trip_id' => new MongoDB\BSON\ObjectId($trip_id)))->result();
                } elseif ($trip_type == "drop") {

                    $student_list = $this->student_details_model->get_all_details(STUDENT_DETAILS, array('student_drop_trip_id' => new MongoDB\BSON\ObjectId($trip_id)))->result();
                }


                $trip_list_data = $this->app_model->get_all_details(BUS_TRIP, array('_id' => new MongoDB\BSON\ObjectId($trip_id)))->result();



                if (!empty($student_list)) {



                    //$trip_list_data=$this->app_model->get_all_details(BUS_TRIP, array('_id' => new  MongoDB\BSON\ObjectId($trip_id)))->result();

                    $trip_name = $trip_list_data[0]->trip_name;


                    ////above timed stops

                    $avilavle_stop_After_time = array();
                    if (is_object($trip_list_data[0]->route_details)) {
                        $trip_list_data[0]->{"route_details"} = (array) $trip_list_data[0]->route_details;
                    }
                    foreach ($trip_list_data[0]->route_details['waypoints_details'] as $key => $value) {
                        if (is_object($value))
                            $value = (array) $value;


                        if (strtotime($value['stop_time']) > strtotime($stop_time) || $stop_time == "-") {

                            array_push($avilavle_stop_After_time, $value['id']);
                        }
                    }








                    $repeat_stopper = array();

                    $emailer = array();

                    $user_token_id_repeater = array();




                    foreach ($student_list as $key => $value) {

                        $trips__id = "";

                        if ($trip_type == "pickup") {
                            $trips__id = (string) $value->student_pickup_bus_stop_id;
                        } elseif ($trip_type == "drop") {

                            $trips__id = (string) $value->student_drop_bus_stop_id;
                        }







                        if (in_array($trips__id, $avilavle_stop_After_time)) {


                            foreach ($value->guardian_details as $key => $data_guardian) {
                                if (is_object($data_guardian))
                                    $data_guardian = (array) $data_guardian;
                                $user_details = $this->app_model->get_all_details(USERS, array('country_code' => $data_guardian['phone_code'], 'phone_number' => $data_guardian['parent_mobile_number']))->result();










                                if (!empty($user_details)) {
                                    if (isset($user_details[0]->notifiy)) {



                                        ///////////push notification

                                        if (isset($user_details[0]->push_notification_key)) {



                                            foreach ($user_details[0]->push_notification_key as $key => $user_puser) {
                                                if (is_object($user_puser))
                                                    $user_puser = (array) $user_puser;
                                                if (isset($user_puser['push_type'])) {

                                                    if ($user_puser['push_type'] == "ANDROID" && !in_array($user_puser['gcm_id'], $user_token_id_repeater) && $user_puser['gcm_id'] != "") {
                                                        array_push($user_token_id_repeater, $user_puser['gcm_id']);

                                                        $regIds = $user_puser['gcm_id'];
                                                        $msg = array('message' => "The trip " . $trip_name . " will be delayed by " . $delay_message);


                                                        $app = "USER";
                                                        $type = "ANDROID";

                                                        $this->simple_push_notification($regIds, $msg, $app, $type);
                                                    } elseif ($user_puser['push_type'] == "IOS" && !in_array($user_puser['ios_token'], $user_token_id_repeater) && $user_puser['ios_token'] != "") {

                                                        array_push($user_token_id_repeater, $user_puser['ios_token']);

                                                        $regIds = $user_puser['ios_token'];
                                                        $msg = array('message' => "The trip " . $trip_name . " will be delayed by " . $delay_message);
                                                        $app = "USER";
                                                        $type = "IOS";

                                                        $this->simple_push_notification($regIds, $msg, $app, $type);
                                                    }
                                                }
                                            }
                                        }









                                        ////////phone notification
                                        if (is_object($user_details[0]->notifiy))
                                            $user_details[0]->{"notifiy"} = (array) $user_details[0]->notifiy;
                                        if ($user_details[0]->notifiy['sms_status_notify'] == "yes" && !in_array($data_guardian['phone_code'] . $data_guardian['parent_mobile_number'], $repeat_stopper)) {

                                            array_push($repeat_stopper, $data_guardian['phone_code'] . $data_guardian['parent_mobile_number']);
                                        }





                                        ///////////email notification

                                        if ($user_details[0]->notifiy['email_status_notify'] == "yes" && !in_array($data_guardian['parent_email'], $emailer)) {
                                            array_push($emailer, $data_guardian['parent_email']);


                                            $this->mail_model->send_driver_delay_mail($user_details[0]->email, $trip_name, $delay_message);

                                            //$this->mail_model->send_driver_delay_mail($data_guardian['parent_email'],$trip_name,$delay_message);
                                        }
                                    } else {


                                        ///////////push notification

                                        if (isset($user_details[0]->push_notification_key)) {



                                            foreach ($user_details[0]->push_notification_key as $key => $user_puser) {
                                                if (isset($user_puser['push_type'])) {

                                                    if ($user_puser['push_type'] == "ANDROID" && !in_array($user_puser['gcm_id'], $user_token_id_repeater) && $user_puser['gcm_id'] != "") {
                                                        array_push($user_token_id_repeater, $user_puser['gcm_id']);

                                                        $regIds = $user_puser['gcm_id'];
                                                        $msg = array('message' => "The trip " . $trip_name . " will be delayed by " . $delay_message);


                                                        $app = "USER";
                                                        $type = "ANDROID";

                                                        $this->simple_push_notification($regIds, $msg, $app, $type);
                                                    } elseif ($user_puser['push_type'] == "IOS" && !in_array($user_puser['ios_token'], $user_token_id_repeater) && $user_puser['ios_token'] != "") {

                                                        array_push($user_token_id_repeater, $user_puser['ios_token']);

                                                        $regIds = $user_puser['ios_token'];
                                                        $msg = array('message' => "The trip " . $trip_name . " will be delayed by " . $delay_message);


                                                        $app = "USER";
                                                        $type = "IOS";

                                                        $this->simple_push_notification($regIds, $msg, $app, $type);
                                                    }
                                                }
                                            }
                                        }






                                        ////////phone notification

                                        if (!in_array($data_guardian['phone_code'] . $data_guardian['parent_mobile_number'], $repeat_stopper)) {

                                            array_push($repeat_stopper, $data_guardian['phone_code'] . $data_guardian['parent_mobile_number']);
                                        }

                                        ///////////email notification
                                        if (!in_array($data_guardian['parent_email'], $emailer)) {
                                            array_push($emailer, $data_guardian['parent_email']);


                                            $this->mail_model->send_driver_delay_mail($user_details[0]->email, $trip_name, $delay_message);
                                            //$this->mail_model->send_driver_delay_mail($data_guardian['parent_email'],$trip_name,$delay_message);
                                        }
                                    }
                                }
                            }
                        }
                    }


                    //var_dump($repeat_stopper);

                    $this->sms_model->driver_delay_msg($repeat_stopper, $trip_name, $delay_message);
                }






                $driver_details = $this->app_model->get_all_details(DRIVERS, array('_id' => new MongoDB\BSON\ObjectId($driver_id)))->result();

                $actual_address = "unknown";

                if ($driver_lat_long != '') {


                    $json = file_get_contents("https://maps.google.com/maps/api/geocode/json?latlng=" . $driver_lat_long . "&sensor=false" . $this->data['google_maps_api_key']);
                    $jsonArr = json_decode($json);
                    $status = $jsonArr->status;

                    if ($status == "OK") {
                        $actual_address = $jsonArr->results[0]->formatted_address;
                    }
                }




                $excludeArr = array("driver_id", "trip_id", "route_id", "stop_id", "delay_message", "driver_lat_long", "notified_time");

                $dataArr = array(
                    "operator_id" => $trip_list_data[0]->operator_id,
                    "driver_id" => new MongoDB\BSON\ObjectId($driver_id),
                    "trip_id" => new MongoDB\BSON\ObjectId($trip_id),
                    "route_id" => new MongoDB\BSON\ObjectId($route_id),
                    "stop_id" => new MongoDB\BSON\ObjectId($stop_id),
                    "delay_message" => $delay_message,
                    "driver_lat_long" => $driver_lat_long,
                    "trip_name" => $trip_list_data[0]->trip_name,
                    "driver_name" => $driver_details[0]->driver_name,
                    'send_from_address' => $actual_address,
                    "notified_time" => new MongoDB\BSON\UTCDateTime(time()),
                    "created_at" => date("Y-m-d")
                );




                $this->driver_notification_model->commonInsertUpdate(DRIVER_NOTIFY, 'insert', $excludeArr, $dataArr, $condition);






















                $responseArr['status'] = '1';
                $responseArr['response'] = "Successfully submited";
            } else {
                $responseArr['response'] = "some parameters missing";
            }
        } catch (MongoException $ex) {
            $responseArr['response'] = $this->format_string('Error in connection', 'error_in_connection');
        }
        $json_encode = json_encode($responseArr, JSON_PRETTY_PRINT);
        echo $this->cleanString($json_encode);
    }

    public function trip_action() {

        $responseArr['status'] = '0';
        $responseArr['response'] = '';
        try {


            $driver_id = new MongoDB\BSON\ObjectId($this->input->post('driver_id'));

            $trip_id = new MongoDB\BSON\ObjectId($this->input->post('trip_id'));

            $lat_long = $this->input->post('lat_long');

            $trip_action = $this->input->post('trip_action');

            $actual_address = "unknown";

            if ($lat_long != '') {


                $json = file_get_contents("https://maps.google.com/maps/api/geocode/json?latlng=" . $lat_long . "&sensor=false" . $this->data['google_maps_api_key']);
                $jsonArr = json_decode($json);
                $status = $jsonArr->status;

                if ($status == "OK") {
                    $actual_address = $jsonArr->results[0]->formatted_address;
                }
            }


            $excludeArr = array("driver_id_par", "trip_id_par", "lat_long_par", "trip_action_par", '0');



            //update breakdown   
            if ($trip_action == "start") {
                $notify_update_condition = array("trip_id" => $trip_id, 'created_at' => date("Y-m-d"), "delay_message" => "BreakDown");
                $dataArr1 = array("delay_message" => "Break down");
                $this->app_model->commonInsertUpdate(DRIVER_NOTIFY, 'update', $excludeArr, $dataArr1, $notify_update_condition);
            }





            $current_trip_data = $this->current_trip_model->get_all_details(CURRENT_TRIP_DETAILS, array("trip_id" => $trip_id), array('trip_start_time' => -1), 1);

            $trip_data = $this->current_trip_model->get_all_details(BUS_TRIP, array("_id" => $trip_id))->result();

            if (empty($trip_data)) {
                $trip_data['trip_name'] = "-";
            }

            $driver_details = $this->current_trip_model->get_all_details(DRIVERS, array("_id" => $driver_id))->result();

            $driver_name = $driver_details[0]->driver_name;

            $current_trip_data = $current_trip_data->result();





            if (!is_null($current_trip_data[0]->trip_start_time->toDateTime()->getTimestamp())) {

                if (strtotime(date('Y-m-d', $current_trip_data[0]->trip_start_time->toDateTime()->getTimestamp())) == strtotime(date("Y-m-d"))) {


                    $condition = array('_id' => $current_trip_data[0]->_id);




                    if ($trip_action == "start") {

                        $dataArr = array("start_address" => $actual_address, "trip_action" => $trip_action, "start_current_date" => date('Y-m-d'), "start_lat_long" => $lat_long, "trip_start_time" => new MongoDB\BSON\UTCDateTime(strtotime(date("Y-m-d H:i:s"))));

                        $this->current_trip_model->commonInsertUpdate(CURRENT_TRIP_DETAILS, 'update', $excludeArr, $dataArr, $condition);
                    } elseif ($trip_action == "stop") {

                        $dataArr = array("stop_address" => $actual_address, "trip_action" => $trip_action, "stop_current_date" => date('Y-m-d'), "stop_lat_long" => $lat_long, "trip_stop_time" => new MongoDB\BSON\UTCDateTime(strtotime(date("Y-m-d H:i:s"))));
                        $this->current_trip_model->commonInsertUpdate(CURRENT_TRIP_DETAILS, 'update', $excludeArr, $dataArr, $condition);
                    }
                } else {





                    $condition = array();

                    if ($trip_action == "start") {


                        $dataArr = array("start_address" => $actual_address, "start_current_date" => date('Y-m-d'), 'operator_id' => new MongoDB\BSON\ObjectId($trip_data[0]->operator_id), 'driver_name' => $driver_name, 'trip_name' => $trip_data[0]->trip_name, 'driver_id' => $driver_id, "trip_id" => $trip_id, "start_lat_long" => $lat_long, "trip_action" => $trip_action, "trip_start_time" => new MongoDB\BSON\UTCDateTime(strtotime(date("Y-m-d H:i:s"))));


                        $this->current_trip_model->commonInsertUpdate(CURRENT_TRIP_DETAILS, 'insert', $excludeArr, $dataArr, $condition);
                    } elseif ($trip_action == "stop") {

                        $dataArr = array("stop_address" => $actual_address, "stop_current_date" => date('Y-m-d'), 'operator_id' => new MongoDB\BSON\ObjectId($trip_data[0]->operator_id), 'driver_name' => $driver_name, 'trip_name' => $trip_data[0]->trip_name, 'driver_id' => $driver_id, "trip_id" => $trip_id, "stop_lat_long" => $lat_long, "trip_action" => $trip_action, "trip_stop_time" => new MongoDB\BSON\UTCDateTime(strtotime(date("Y-m-d H:i:s"))));


                        $this->current_trip_model->commonInsertUpdate(CURRENT_TRIP_DETAILS, 'insert', $excludeArr, $dataArr, $condition);
                    }
                }
            } else {


                $condition = array();

                if ($trip_action == "start") {


                    $dataArr = array("start_address" => $actual_address, "start_current_date" => date('Y-m-d'), 'operator_id' => new MongoDB\BSON\ObjectId($trip_data[0]->operator_id), 'driver_name' => $driver_name, 'trip_name' => $trip_data[0]->trip_name, 'driver_id' => $driver_id, "trip_id" => $trip_id, "start_lat_long" => $lat_long, "trip_action" => $trip_action, "trip_start_time" => new MongoDB\BSON\UTCDateTime(strtotime(date("Y-m-d H:i:s"))));


                    $this->current_trip_model->commonInsertUpdate(CURRENT_TRIP_DETAILS, 'insert', $excludeArr, $dataArr, $condition);
                } elseif ($trip_action == "stop") {

                    $dataArr = array("stop_address" => $actual_address, "stop_current_date" => date('Y-m-d'), 'operator_id' => new MongoDB\BSON\ObjectId($trip_data[0]->operator_id), 'driver_name' => $driver_name, 'trip_name' => $trip_data[0]->trip_name, 'driver_id' => $driver_id, "trip_id" => $trip_id, "stop_lat_long" => $lat_long, "trip_action" => $trip_action, "trip_stop_time" => new MongoDB\BSON\UTCDateTime(strtotime(date("Y-m-d H:i:s"))));


                    $this->current_trip_model->commonInsertUpdate(CURRENT_TRIP_DETAILS, 'insert', $excludeArr, $dataArr, $condition);
                }
            }


            $responseArr['status'] = '1';
            $responseArr['response'] = 'successfully updated';
        } catch (MongoException $ex) {
            $responseArr['response'] = $this->format_string('Error in connection', 'error_in_connection');
        }
        $json_encode = json_encode($responseArr, JSON_PRETTY_PRINT);
        echo $this->cleanString($json_encode);
    }

}

/* End of file driver.php */
/* Location: ./application/controllers/v7/api_v7/driver.php */