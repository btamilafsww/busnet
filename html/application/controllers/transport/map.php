<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
*
*	Map
* 
*	@package		CI
*	@subpackage		Controller
*	@author			Katenterprise
*
**/
class Map extends MY_Controller {
	function __construct(){
        parent::__construct();
		$this->load->helper(array('cookie','date','form'));
		$this->load->library(array('encrypt','form_validation'));		
		$this->load->model(array('map_model'));
		$this->load->model(array('app_model'));
		
		if($this->checkLogin('O') != ''){
			$operator_id = $this->checkLogin('O');
			$chkOperator = $this->app_model->get_selected_fields(TRANSPORT,array('_id' => new MongoDB\BSON\ObjectId($operator_id)),array('status'));
			$chkstatus = TRUE;
			$errMsg = '';
			if($chkOperator->num_rows() == 1){
				if($chkOperator->row()->status == 'Inactive'){
					$chkstatus = FALSE;
						if ($this->lang->line('operator_inactive_message') != '') 
								$errMsg= stripslashes($this->lang->line('operator_inactive_message')); 
						else  $errMsg = 'Your account is temporarily deactivated, Please contact admin';
						
				}
			} else {
				$chkstatus = FALSE;
				if ($this->lang->line('account_not_found') != '') 
						$errMsg= stripslashes($this->lang->line('account_not_found')); 
				else  $errMsg = 'Your account details not found';
				
			}
			if(!$chkstatus){
				 $newdata = array(
					'last_logout_date' => date("Y-m-d H:i:s")
				);
				$collection = TRANSPORT;
				
				$condition = array('_id' => $this->checkLogin('O'));
				$this->app_model->update_details($collection, $newdata, $condition);
				$operatordata = array(
							APP_NAME.'_session_operator_id' => '',
							APP_NAME.'_session_operator_name' => '',
							APP_NAME.'_session_operator_email' => '',
							APP_NAME.'_session_vendor_location' =>''
						   
						);
				$this->session->unset_userdata($operatordata);
				$this->setErrorMessage('error', $errMsg);
				redirect(TRANSPORT_NAME);
			}
		}
	
    }
    
   /**
    *
    * To redirect to available drivers on map page
    * 	
    * @return HTTP to redirect available drivers on map page
    *	
    **/	
   	public function index(){
				if ($this->checkLogin('O') == ''){
						redirect(TRANSPORT_NAME);
				}else {
						redirect(TRANSPORT_NAME.'/map/map_avail_drivers');
				}
	}
		
   /**
    *
    * To load the available drivers on map in particular location
    *
    * @param string $address is location address
    * @return HTML to show available drivers on map page
    *	
    **/	
   	public function map_avail_drivers(){
			if ($this->checkLogin('O') == ''){
					redirect(TRANSPORT_NAME);
			}else{
					$operator_id = (string)$this->checkLogin('O');
					$operator_loc = $this->session->userdata(APP_NAME.'_session_operator_location');

				

					//$condition = array('_id' => $this->session->userdata(APP_NAME.'_session_operator_location'));
				
					//$location = $this->map_model->get_selected_fields(LOCATIONS,$condition,array('_id','location'));
				



					/*$coordinates=array(floatval($location->row()->location['lng']),floatval($location->row()->location['lat']));
				
					$location=array($location->row()->location['lat'],$location->row()->location['lng']);
*/

					$coordinates=array_reverse(array(floatval($operator_loc->lat),floatval($operator_loc->lng)));
				
					$location=array(floatval($operator_loc->lat),floatval($operator_loc->lng));	
					

					$center=@implode($location,',');
				
					$condition=array('status'=>'Active');
					$category = $this->map_model->get_selected_fields(CATEGORY,$condition,array('name','image','name_languages'));
					$availCategory=array();
					$langCode = $this->data['langCode'];
					if($category->num_rows()>0){
							foreach($category->result() as $cat){
									$category_name = $cat->name;
									if(isset($cat->name_languages[$langCode]) && $cat->name_languages[$langCode] != '') $category_name = $cat->name_languages[$langCode];
									$availCategory[(string)$cat->_id]=$category_name;
							}
					}
					
					$address=$this->input->get('location');
					
					if($address!=''){
							$address = str_replace(" ", "+", $address);
							$json = file_get_contents("https://maps.google.com/maps/api/geocode/json?address=$address&sensor=false".$this->data['google_maps_api_key']);
							$jsonArr = json_decode($json);
							$lat = $jsonArr->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
							$lang = $jsonArr->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
							
							$location=array($lat,$lang);
							$coordinates=array_reverse($location);
							
					}

					


					if(!empty($coordinates) & $coordinates[0]!='') {

							$driverList = $this->map_model->get_nearest_driver($coordinates, $operator_id); 

					} else {
						 $this->setErrorMessage('error', 'No location Found','admin_no_location');
						 redirect(TRANSPORT_NAME.'/dashboard/display_dashboard');
					}

					
							
					$this->load->library('googlemaps');

					$config['center'] =$center;
					$config['zoom'] = '14';
					$config['minzoom'] = '3';
					$config['maxzoom'] = '24';
					$config['places'] = TRUE;
					$config['cluster'] = FALSE;
					$config['language'] = $this->data['langCode'];
					$config['placesAutocompleteInputID'] = 'location';
					$config['placesAutocompleteBoundsMap'] = TRUE;
					$this->googlemaps->initialize($config);
					$avail = 0;
					$unavail = 0;
					$onride = 0;
					if(is_object($driverList)){
							foreach($driverList->toArray() as $driver){
								if(is_object($driver))
                                                                    $driver=(array)$driver;
                                                                if(is_object($driver['loc']))
                                                                    $driver['loc']=(array)$driver['loc'];
									$loc=array_reverse($driver['loc']);
									$latlong=@implode($loc,',');
									$marker = array();
									$marker['position'] = $latlong;
									$current=time()-300;
								 
								 
									if($driver['availability']=='Yes' && $driver['mode'] == 'Available' & isset($driver['last_active_time']->sec) && $driver['last_active_time']->sec > $current){
										$avail++;
										$marker['icon'] = base_url().'images/pin-available.png';
									} else if($driver['availability']=='Yes' && $driver['mode'] == 'Booked'){
										$onride++;
										$marker['icon'] = base_url().'images/pin-yellow.png';
									} else {
										$unavail++;
										$marker['icon'] = base_url().'images/pin-unavailable.png';
									}
									$marker['icon_scaledSize'] = '25,25';
									/*$catDis = "";
									if(array_key_exists((string)$driver['category'],$availCategory)){
											$catDis = $availCategory[(string)$driver['category']];
									}*/
									$own_driv = '';
									if((string)$driver['driver_name'] == $operator_id){
											if ($this->lang->line('dash_operator_own_driver') != '')
											$own_driv = '(' . stripslashes($this->lang->line('dash_operator_own_driver')) . ')'; else $own_driv = '(Own Driver)';
									}
									$marker['infowindow_content'] ="<div style='width:150px !important;height:50Px!important;'>".$driver['driver_name']." "  . $own_driv ."<br/></div>";
									$this->googlemaps->add_marker($marker);
							}
					}
					$this->data['map'] = $this->googlemaps->create_map();
					$this->data['online_drivers'] = $avail;
					$this->data['offline_drivers'] = $unavail;
					$this->data['onride_drivers'] = $onride;
					$this->data['address'] = urldecode($address);
							
					if ($this->lang->line('admin_menu_map_view') != '') 
							$title= stripslashes($this->lang->line('admin_menu_map_view')); 
					else  $title = 'Map View';
					$this->data['heading'] = $title;
					$this->load->view(TRANSPORT_NAME.'/map/availbale_drivers',$this->data);
			}
	}
    
   /**
	*
	* To load the available users on map in particular location
    *
	* @param string $address is location address
	* @return HTML to shows available users on map page
    *	
    **/
   	public function map_avail_users(){
			if ($this->checkLogin('O') == ''){
					redirect(TRANSPORT_NAME);
			}else{
					
					$operator_id = (string)$this->checkLogin('O');
					$operator_loc = $this->session->userdata(APP_NAME.'_session_operator_location');
				

					$coordinates=array_reverse(array(floatval($operator_loc->lat),floatval($operator_loc->lng)));
				
					$location=array(floatval($operator_loc->lat),floatval($operator_loc->lng));	
					
					$center=@implode($location,',');
				
					$condition=array('status'=>'Active');
					$category = $this->map_model->get_selected_fields(CATEGORY,$condition,array('name','image'));
					$availCategory=array();
					if($category->num_rows()>0){
							foreach($category->result() as $cat){
									$availCategory[(string)$cat->_id]=$cat->name;
							}
					}
				
					$address=$this->input->get('location');
					
					if($address!=''){
							$address = str_replace(" ", "+", $address);
							$json = file_get_contents("https://maps.google.com/maps/api/geocode/json?address=$address&sensor=false".$this->data['google_maps_api_key']);
							$jsonArr = json_decode($json);
							$lat = $jsonArr->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
							$lang = $jsonArr->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
							
							$location=array(floatval($lat),floatval($lang));
							$coordinates=array_reverse($location);
							
					}
					if(!empty($coordinates) & $coordinates[0]!='') {
							$userList = $this->map_model->get_nearest_user($coordinates); 
					} else {
							$this->setErrorMessage('error', 'No location Found','admin_no_location');
							redirect(TRANSPORT_NAME . '/dashboard/display_dashboard');
					}
		
					$this->load->library('googlemaps');

					$config['center'] =$center;
					$config['zoom'] = '6';
					$config['minzoom'] = '3';
					$config['maxzoom'] = '24';
					$config['places'] = TRUE;
					$config['cluster'] = FALSE;
					$config['language'] = $this->data['langCode'];
					$config['placesAutocompleteInputID'] = 'location';
					$config['placesAutocompleteBoundsMap'] = TRUE;
					$this->googlemaps->initialize($config);
					
					if(is_object($userList)){
							foreach($userList->toArray() as $user){
                                                            if(is_object($user))
                                                                $user=(array)$user;
                                                            if(is_object($user['loc']))
                                                                $user['loc']=(array)$user['loc'];
									$loc=array_reverse($user['loc']);
									$latlong=@implode($loc,',');
									$marker = array();
									$marker['position'] = $latlong;
									$marker['icon'] = base_url().'images/user.png';
									$marker['icon_scaledSize'] = '25,25';
									$marker['infowindow_content'] ="<div style='width:200px !important;height:50Px!important;'>".$user['user_name'].'<br/>'.$user['email']."</div>";
									$this->googlemaps->add_marker($marker);
							}
					}
					$this->data['map'] = $this->googlemaps->create_map();
	
					$this->data['address'] = urldecode($address);
					if ($this->lang->line('admin_menu_map_view') != '') 
							$title= stripslashes($this->lang->line('admin_menu_map_view')); 
					else  $title = 'Map View';
					$this->data['heading'] = $title;
					$this->load->view(TRANSPORT_NAME . '/map/availbale_users',$this->data);
			}
	}
	
}


/* End of file map.php */
/* Location: ./application/controllers/operator/map.php */