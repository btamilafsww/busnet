<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
*
*   Rides Management in operator panel
* 
*   @package    CI
*   @subpackage Controller
*   @author Katenterprise
*
**/

class class_details extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('cookie', 'date', 'form','ride_helper','distcalc_helper'));
        $this->load->library(array('encrypt', 'form_validation'));
        $this->load->model(array('app_model','class_details_model'));
		if($this->checkLogin('O') != ''){
			$operator_id = $this->checkLogin('O');
			$chkOperator = $this->app_model->get_selected_fields(TRANSPORT,array('_id' => new MongoDB\BSON\ObjectId($operator_id)),array('status'));
			$chkstatus = TRUE;
			$errMsg = '';
			if($chkOperator->num_rows() == 1){
				if($chkOperator->row()->status == 'Inactive'){
					$chkstatus = FALSE;
						if ($this->lang->line('operator_inactive_message') != '') 
								$errMsg= stripslashes($this->lang->line('operator_inactive_message')); 
						else  $errMsg = 'Your account is temporarily deactivated, Please contact admin';
						
				}
			} else {
				$chkstatus = FALSE;
				if ($this->lang->line('account_not_found') != '') 
						$errMsg= stripslashes($this->lang->line('account_not_found')); 
				else  $errMsg = 'Your account details not found';
				
			}
			if(!$chkstatus){
				 $newdata = array(
					'last_logout_date' => date("Y-m-d H:i:s")
				);
				$collection = TRANSPORT;
				
				$condition = array('_id' => $this->checkLogin('O'));
				$this->app_model->update_details($collection, $newdata, $condition);
				$operatordata = array(
							APP_NAME.'_session_operator_id' => '',
							APP_NAME.'_session_operator_name' => '',
							APP_NAME.'_session_operator_email' => '',
							APP_NAME.'_session_vendor_location' =>''
						   
						);
				$this->session->unset_userdata($operatordata);
				$this->setErrorMessage('error', $errMsg);
				redirect(TRANSPORT_NAME);
			}
		}
		
    }
    
	///class view
	public function add_new_class()
	{
	
		

	
	    if ($this->checkLogin('O') == '') {
            redirect(TRANSPORT_NAME);
        } else {
          
            $this->data['heading'] = "Add New Class";

             $this->data['class_list'] = $this->class_details_model->get_all_details(CLASS_DETAILS, array("operator_id"=> $this->session->userdata(APP_NAME.'_session_operator_id')),array('created_at' => 'DESC'));

             $this->data['section_list'] = $this->class_details_model->get_all_details(SECTION_DETAILS, array("operator_id"=> $this->session->userdata(APP_NAME.'_session_operator_id')),array('created_at' => 'DESC'));	
             


            $this->load->view(TRANSPORT_NAME.'/class/add_new_class', $this->data);
        }
	
	
	
	
	
	
	
	}

//insert class
	public function insert_class_new()
	{



		 if ($this->checkLogin('O') == '') {
            redirect(TRANSPORT_NAME);
        } 
        else 
        {
                   


	        	if($this->input->post('class_name') != "" )
				{
					


								$class_checker = $this->class_details_model->get_selected_fields(CLASS_DETAILS,array('class_name'=>  $this->input->post('class_name'),"operator_id"=> $this->session->userdata(APP_NAME.'_session_operator_id') ),array('_id'));

								

								

								

								if ($class_checker->num_rows() >= 1) 
								{
									$class_loc_id =$class_checker->result()[0]->_id;
									$section_already_exsist = $this->class_details_model->get_selected_fields(SECTION_DETAILS,array('section_name'=>  $this->input->post('section_name'),"operator_id"=> $this->session->userdata(APP_NAME.'_session_operator_id') ,"class_name"=>  $this->input->post('class_name')),array('_id'));



									if ($section_already_exsist->num_rows() >= 1) 
									{
										$this->setErrorMessage('error', 'This class and Section already exists');
										redirect(TRANSPORT_NAME . '/class_details/add_new_class');
									}
									else
									{

										$excludeArr_s= array("class_name","section_name");
										$condition_s = array();	

										$dataArr_s = array(

														'class_id' => $class_loc_id,	
														'class_name' => $this->input->post('class_name'),	
														'operator_id' => $this->session->userdata(APP_NAME.'_session_operator_id'),	
														'section_name' =>$this->input->post('section_name'),														
														'created_by_id' => $this->session->userdata(APP_NAME.'_session_changes_done_by_id'),
														'created_by_name' => $this->session->userdata(APP_NAME.'_session_changes_done_by_name'),
														'created_at' => date("Y-m-d H:i:s")
														);
										
										$this->class_details_model->commonInsertUpdate(SECTION_DETAILS,'insert',$excludeArr_s,$dataArr_s,$condition_s);
										$location_id = $this->cimongo->insert_id();											
									 	
							//			$this->setErrorMessage('success','Section added successfully');

										$this->setErrorMessage('success','Class and Section added successfully');

									}








									/*$this->setErrorMessage('error', 'This class already exists');
									redirect(TRANSPORT_NAME . '/class_details/add_new_class');*/
								}
								else
								{	

										$excludeArr= array("class_name","section_name");
										$condition = array();
										


										$dataArr = array(
														'operator_id' => $this->session->userdata(APP_NAME.'_session_operator_id'),		
														'class_name' =>$this->input->post('class_name'),
														'created_by_id' => $this->session->userdata(APP_NAME.'_session_changes_done_by_id'),
														'created_by_name' => $this->session->userdata(APP_NAME.'_session_changes_done_by_name'),
														'created_at' => date("Y-m-d H:i:s")
														);
										
										$class_id = $this->class_details_model->commonInsertUpdate(CLASS_DETAILS,'insert',$excludeArr,$dataArr,$condition);
										$location_id = $this->cimongo->insert_id();


									if($this->input->post('section_name') != "")
									{


										$section_checker = $this->class_details_model->get_selected_fields(SECTION_DETAILS,array('section_name'=>  $this->input->post('class_name'),"operator_id"=> $this->session->userdata(APP_NAME.'_session_operator_id') ),array('_id'));

										if ($section_checker->num_rows() >= 1) 
										{
											$this->setErrorMessage('error', 'This section already exists');
											redirect(TRANSPORT_NAME . '/class_details/add_new_class');	
										}
										else
										{	
											$excludeArr_s= array("class_name","section_name");
											$condition_s = array();										


											$dataArr_s = array(

															'class_id' => $location_id,	
															'class_name' => $this->input->post('class_name'),	
															'operator_id' => $this->session->userdata(APP_NAME.'_session_operator_id'),	
															'section_name' =>$this->input->post('section_name'),
															
															'created_by_id' => $this->session->userdata(APP_NAME.'_session_changes_done_by_id'),
															'created_by_name' => $this->session->userdata(APP_NAME.'_session_changes_done_by_name'),
															'created_at' => date("Y-m-d H:i:s")
															);
											
											$this->class_details_model->commonInsertUpdate(SECTION_DETAILS,'insert',$excludeArr_s,$dataArr_s,$condition_s);
											$location_id = $this->cimongo->insert_id();											
										 	
								//			$this->setErrorMessage('success','Section added successfully');

											$this->setErrorMessage('success','Class and Section added successfully');

									    }	

									 }
									 else
									 {
									 		$this->setErrorMessage('success','Class added successfully');
									 }	

							   }			


       			}
       			else
				{
					$this->setErrorMessage('error', "Some data missing");
				}	
			
			
				$this->data['class_list'] = $this->class_details_model->get_all_details(CLASS_DETAILS, array("operator_id"=> $this->session->userdata(APP_NAME.'_session_operator_id')));
			

		
				$this->data['heading'] = 'Add New Class';	
				redirect(TRANSPORT_NAME .'/class_details/add_new_class');





	}
 }

	///add view section


	public function add_new_section()
	{
	
		

	
	    if ($this->checkLogin('O') == '') {
            redirect(TRANSPORT_NAME);
        } else {
          
            $this->data['heading'] = "Add New Section";


            $this->data['section_list'] = $this->class_details_model->get_all_details(SECTION_DETAILS, array("operator_id"=> $this->session->userdata(APP_NAME.'_session_operator_id')));
			

            $this->load->view(TRANSPORT_NAME.'/class/add_new_section', $this->data);
        }
	
	
	
	
	
	
	
	}

	///add section

	///add section

	public function insert_class_section()
	{



		 if ($this->checkLogin('O') == '') {
            redirect(TRANSPORT_NAME);
        } else {
          
         


        	if($this->input->post('section_name') != ""  )
				{
					


							
								$section_checker = $this->class_details_model->get_selected_fields(SECTION_DETAILS,array('section_name'=>  $this->input->post('section_name'),"operator_id"=> $this->session->userdata(APP_NAME.'_session_operator_id') ),array('_id'));

								if ($section_checker->num_rows() >= 1) 
								{


								$this->setErrorMessage('error', 'This section already exists');
								redirect(TRANSPORT_NAME . '/class_details/add_new_class');
								

								}
								else
								{	
							



										$excludeArr= array("section_name");
										$condition = array();
										


										$dataArr = array(
														'operator_id' => $this->session->userdata(APP_NAME.'_session_operator_id'),	
													
														'section_name' =>$this->input->post('section_name'),
														
														'created_by_id' => $this->session->userdata(APP_NAME.'_session_changes_done_by_id'),
														'created_by_name' => $this->session->userdata(APP_NAME.'_session_changes_done_by_name'),
														'created_at' => date("Y-m-d H:i:s")
														);
										
										$this->class_details_model->commonInsertUpdate(SECTION_DETAILS,'insert',$excludeArr,$dataArr,$condition);
										$location_id = $this->cimongo->insert_id();
										
									 	
										$this->setErrorMessage('success','Section added successfully');


							   }			











           
       			 }
       			 else
				{
					$this->setErrorMessage('error', "Some data missing");
				}	
			
			
				 $this->data['section_list'] = $this->class_details_model->get_all_details(SECTION_DETAILS, array("operator_id"=> $this->session->userdata(APP_NAME.'_session_operator_id')));
			
		
				$this->data['heading'] = 'Add New Section';	
				redirect(TRANSPORT_NAME .'/class_details/add_new_class');





	}



	}	







	public function insert_class_along_section()
	{


					 if ($this->checkLogin('O') == '') {
			            redirect(TRANSPORT_NAME);
			        } 
			        else 
			        {
			                   


				        	if($this->input->post('class_name') != "" )
							{
								


											$class_checker = $this->class_details_model->get_selected_fields(CLASS_DETAILS,array('class_name'=>  $this->input->post('class_name'),"operator_id"=> $this->session->userdata(APP_NAME.'_session_operator_id') ),array('_id'));

											

										

											
											if ($class_checker->num_rows() >= 1) 
											{
												redirect(TRANSPORT_NAME .'/class_details/add_new_class#class_stud');
													$this->setErrorMessage('error', "Class already exists");
											}
											else
											{


													$excludeArr= array("class_name","section_name_mul");
													$condition = array();
													


													$dataArr = array(
																	'operator_id' => $this->session->userdata(APP_NAME.'_session_operator_id'),	
																
																	'section_ids' =>$this->input->post('section_name_mul'),

																	'class_name' =>$this->input->post('class_name'),
																	
																	'created_by_id' => $this->session->userdata(APP_NAME.'_session_changes_done_by_id'),
																	'created_by_name' => $this->session->userdata(APP_NAME.'_session_changes_done_by_name'),
																	'created_at' => date("Y-m-d H:i:s")
																	);
													
												
													$this->class_details_model->commonInsertUpdate(CLASS_DETAILS,'insert',$excludeArr,$dataArr,$condition);
												

													redirect(TRANSPORT_NAME .'/class_details/add_new_class#class_stud');

												
											}	
							}
							else
							{
									$this->setErrorMessage('error', "Some data missing");

									redirect(TRANSPORT_NAME .'/class_details/add_new_class#class_stud');
							}	
							
					}						



	}




	public function edit_class()
	{
			 if ($this->checkLogin('O') == '') {
			            redirect(TRANSPORT_NAME);
			        } 
			        else 
			        {

			        	try
			        	{

					        	 $class_id = $this->uri->segment(4);

					        	$this->data['class_details']=$this->class_details_model->get_all_details(CLASS_DETAILS, array("_id"=> new MongoDB\BSON\ObjectId( $class_id )))->result();

					        	if(empty($this->data['class_details']))
					        	{
					        		redirect(TRANSPORT_NAME .'/class_details/add_new_class#class_stud');
					        	}

					        	 $this->data['section_list'] = $this->class_details_model->get_all_details(SECTION_DETAILS, array("operator_id"=> $this->session->userdata(APP_NAME.'_session_operator_id')));
					
					        	 $this->data['class_id']=$class_id;
					        	$this->data['heading']="Edit Class";

			        			$this->load->view(TRANSPORT_NAME.'/class/edit_class', $this->data);	


			        	}
			        	catch(Exception $e)
			        	{

				        		redirect(TRANSPORT_NAME .'/class_details/add_new_class#class_stud');
				        	
			        	}


			        }	


	}


	public function update_class_along_section()
	{
			

		 if ($this->checkLogin('O') == '') {
            redirect(TRANSPORT_NAME);
        } else {
          
         


        	if($this->input->post('class_name') != ""  )
				{
					


							
								$section_checker = $this->class_details_model->get_selected_fields(CLASS_DETAILS,array('class_name'=>  $this->input->post('class_name'),"operator_id"=> $this->session->userdata(APP_NAME.'_session_operator_id') ),array('_id'));


								$repeat_checker=$section_checker->result();

								if(!empty($repeat_checker))
								{
									if((string)$repeat_checker[0]->_id != $this->input->post('class_id') )
									{
										$repeat_checker=1;
									}
								}





								if ($repeat_checker == 1 ) 
								{
									$this->setErrorMessage('error', "Class Name already exists");
								}
								else{
									

													$excludeArr= array("class_name","section_name_mul","class_id");
													$condition = array("_id" => new  MongoId($this->input->post('class_id')) );
													


													$dataArr = array(
																	'operator_id' => $this->session->userdata(APP_NAME.'_session_operator_id'),	
																
																	'section_ids' =>$this->input->post('section_name_mul'),

																	'class_name' =>$this->input->post('class_name'),
																	
																	'edited_by_id' => $this->session->userdata(APP_NAME.'_session_changes_done_by_id'),
																	'edited_by_name' => $this->session->userdata(APP_NAME.'_session_changes_done_by_name'),
																	'edited_at' => date("Y-m-d H:i:s")
																	);

													$this->class_details_model->commonInsertUpdate(CLASS_DETAILS,'update',$excludeArr,$dataArr,$condition);	

													$this->setErrorMessage('success', "Class Updated successfully");

									}
					}
					
			}						

									redirect(TRANSPORT_NAME .'/class_details/add_new_class#class_stud');					
	}





	public function delete_class()
	{
			

		 if ($this->checkLogin('O') == '') {
            redirect(TRANSPORT_NAME);
        } else {
          
         	
        	$class_id = $this->uri->segment(4);

        	if($class_id != ""  )
				{
					
					 $section_id = $this->uri->segment(4);
					 $condition = array('_id' => new MongoDB\BSON\ObjectId($section_id));       
					 $this->app_model->commonDelete(CLASS_DETAILS, $condition);
					 $this->setErrorMessage('success', "Class Deleted successfully");		
					
				}
					
			}						

									redirect(TRANSPORT_NAME .'/class_details/add_new_class#class_stud');					
	}






	public function edit_section()
	{
			 if ($this->checkLogin('O') == '') {
			            redirect(TRANSPORT_NAME);
			        } 
			        else 
			        {

			        	try
			        	{

					        	 $section_id = $this->uri->segment(4);

					        	$this->data['section_details']=$this->class_details_model->get_all_details(SECTION_DETAILS, array("_id"=> new MongoDB\BSON\ObjectId( $section_id )))->result();

					        	if(empty($this->data['section_details']))
					        	{
					        		redirect(TRANSPORT_NAME .'/class_details/add_new_class#class_stud');
					        	}

					        	
					
					        	$this->data['section_id']=$section_id;
					        	$this->data['heading']="Edit Section";

			        			$this->load->view(TRANSPORT_NAME.'/class/edit_section', $this->data);	


			        	}
			        	catch(Exception $e)
			        	{

				        		redirect(TRANSPORT_NAME .'/class_details/add_new_class');
				        	
			        	}


			        }	


	}


	public function update_section()
	{
			

		 if ($this->checkLogin('O') == '') {
            redirect(TRANSPORT_NAME);
        } else {
          
         


        	if($this->input->post('section_name') != ""  )
				{
					


							
								$section_checker = $this->class_details_model->get_selected_fields(SECTION_DETAILS,array('section_name'=>  $this->input->post('section_name'),"operator_id"=> $this->session->userdata(APP_NAME.'_session_operator_id') ),array('_id'));


								$repeat_checker=$section_checker->result();

								if(!empty($repeat_checker))
								{
									if((string)$repeat_checker[0]->_id != $this->input->post('section_id') )
									{
										$repeat_checker=1;
									}
								}





								if ($repeat_checker == 1 ) 
								{
									$this->setErrorMessage('error', "Section Name already exists");
								}
								else{
									

													$excludeArr= array("section_name","section_name_mul","section_id");
													$condition = array("_id" => new  MongoId($this->input->post('section_id')) );
													


													$dataArr = array(
																	'operator_id' => $this->session->userdata(APP_NAME.'_session_operator_id'),	
																
																	

																	'section_name' =>$this->input->post('section_name'),
																	
																	'edited_by_id' => $this->session->userdata(APP_NAME.'_session_changes_done_by_id'),
																	'edited_by_name' => $this->session->userdata(APP_NAME.'_session_changes_done_by_name'),
																	'edited_at' => date("Y-m-d H:i:s")
																	);

													$this->class_details_model->commonInsertUpdate(SECTION_DETAILS,'update',$excludeArr,$dataArr,$condition);	

													$this->setErrorMessage('success', "Section Updated successfully");

									}
					}
					
			}						

									redirect(TRANSPORT_NAME .'/class_details/add_new_class');					
	}

	
	public function delete_section()
	{
			

		 if ($this->checkLogin('O') == '') {
            redirect(TRANSPORT_NAME);
        } else {
          
         	
        	$section_id = $this->uri->segment(4);

        	if($section_id != ""  )
				{
						

					$already_present_contion = array('section_ids' => $section_id);					//	route id		

					$check_s =$this->class_details_model->get_all_details(CLASS_DETAILS,  $already_present_contion)->result();



					if(empty($check_s))
					{	
						 $section_id = $this->uri->segment(4);
						 $condition = array('_id' => new MongoDB\BSON\ObjectId($section_id));       
						 $this->app_model->commonDelete(SECTION_DETAILS, $condition);
						 $this->setErrorMessage('success', "Section Deleted successfully");		
					}
					else
					{
						 $this->setErrorMessage('error', "Cannot delete Since it is assigned to a class");		
					}	
				}
					
			}						

									redirect(TRANSPORT_NAME .'/class_details/add_new_class');					
	}

	
}
