<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
*
*   Rides Management in operator panel
* 
*   @package    CI
*   @subpackage Controller
*   @author Katenterprise
*
**/

class Bus_route extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('cookie', 'date', 'form','ride_helper','distcalc_helper'));
        $this->load->library(array('encrypt', 'form_validation'));
        $this->load->model(array('driver_notification_model','app_model','rides_model','driver_model','vehicle_model','student_details_model','operators_model','bus_trip_model','bus_route_model','bus_details_model'));
		if($this->checkLogin('O') != ''){
			$operator_id = $this->checkLogin('O');

			$operator_type=$this->session->userdata(APP_NAME.'_session_operator_user_type');
			
			if($operator_type == "main")
			{	

			$chkOperator = $this->app_model->get_selected_fields(TRANSPORT,array('_id' => new MongoDB\BSON\ObjectId($operator_id)),array('status'));
			
			}
			else
			{
			
			$chkOperator = $this->app_model->get_selected_fields(TRANSPORT,array('_id' => new MongoDB\BSON\ObjectId($this->session->userdata(APP_NAME.'_session_subadmin_id'))),array('status'));
			
			}	

			$schooladminpevilages=$this->session->userdata(APP_NAME.'_session_operator_privileges');


			$current_page = $this->uri->segment(3);

                        if(is_object($schooladminpevilages))
                            $schooladminpevilages=(array)$schooladminpevilages;
			if(isset($schooladminpevilages["bus_route"]) )
			{




					///page access



				if($current_page == "add_bus_route" || $current_page == "insert_bus_route" )
				{

					 if(!in_array("add_bus_route", $schooladminpevilages["bus_route"]))
					 {

					 	redirect(TRANSPORT_NAME);
					 }

				}
				elseif($current_page == "view_bus_route" || $current_page == "list_bus_route")
				{
					 if(!in_array("view_bus_route", $schooladminpevilages["bus_route"]))
					 {
					 	redirect(TRANSPORT_NAME);
					 }

				}	



			}	
			else
			{
				redirect(TRANSPORT_NAME);
		
			}	



			$chkstatus = TRUE;
			$errMsg = '';
			if($chkOperator->num_rows() == 1){
				if($chkOperator->row()->status == 'Inactive'){
					$chkstatus = FALSE;
						if ($this->lang->line('operator_inactive_message') != '') 
								$errMsg= stripslashes($this->lang->line('operator_inactive_message')); 
						else  $errMsg = 'Your account is temporarily deactivated, Please contact admin';
						
				}
			} else {
				$chkstatus = FALSE;
				if ($this->lang->line('account_not_found') != '') 
						$errMsg= stripslashes($this->lang->line('account_not_found')); 
				else  $errMsg = 'Your account details not found';
				
			}
			if(!$chkstatus){
				 $newdata = array(
					'last_logout_date' => date("Y-m-d H:i:s")
				);
				$collection = TRANSPORT;
				
				$condition = array('_id' => $this->checkLogin('O'));
				$this->app_model->update_details($collection, $newdata, $condition);
				$operatordata = array(
							APP_NAME.'_session_operator_id' => '',
							APP_NAME.'_session_TRANSPORT_NAME' => '',
							APP_NAME.'_session_operator_email' => '',
							APP_NAME.'_session_vendor_location' =>''
						   
						);
				$this->session->unset_userdata($operatordata);
				$this->setErrorMessage('error', $errMsg);
				redirect(TRANSPORT_NAME);
			}
		}
		
    }
    
	
	
	public function add_bus_route(){

  		if ($this->checkLogin('O') == '') 
  		{
		redirect(TRANSPORT_NAME);
        } else { 
		
		$this->data['center']=$this->config->item('latitude').','.$this->config->item('longitude');
		
		
		 $this->data['heading'] = 'Add New Route';	
		$this->load->view(TRANSPORT_NAME.'/bus_route/add_bus_route',$this->data);
		
		
		}
	
	}

    public function insert_bus_route()
	{
	
	
	if ($this->checkLogin('O') == '') 
  		{
		redirect(TRANSPORT_NAME);
        } else { 
	

				$way_data=json_decode($this->input->post('stopping_point'));
				$way_array=array();

				for($i=0;$i < sizeof($way_data); $i++)
				{

					$id= new MongoDB\BSON\ObjectId();
					
					$set_arr=$way_data[$i];
					
					$set_arr=array('_id' => $id , "way_stop" => $set_arr );;

					array_push($way_array , $set_arr);
				}

				
			
				
	
				$excludeArr= array("starting_point","stopping_point","ending_point","route_name","route_name_hidden");
				$condition = array();
				
				$dataArr = array(
								'operator_id' => $this->session->userdata(APP_NAME.'_session_operator_id'),	
								'route_name'  =>$this->input->post('route_name_hidden'),
								'startpoints' =>json_decode($this->input->post('starting_point')), 
								'endpoints' =>json_decode($this->input->post('ending_point')),	
				
								'waypoints' => $way_array,

								'polyencode' => $this->input->post('polyencode'),
								'created_by_id' => $this->session->userdata(APP_NAME.'_session_changes_done_by_id'),
								'created_by_name' => $this->session->userdata(APP_NAME.'_session_changes_done_by_name'),
								'created_at'=>array('created_at' => date("Y-m-d H:i:s"))  
								
								);
				
				$this->bus_route_model->commonInsertUpdate(BUS_ROUTE,'insert',$excludeArr,$dataArr,$condition);
				$location_id = $this->cimongo->insert_id();
				
			 	
				$this->setErrorMessage('success','Route added successfully','route_added_success');
				
			
				$this->data['center']=$this->config->item('latitude').','.$this->config->item('longitude');
		
		
				$this->data['heading'] = 'Add New Route';	
				redirect(TRANSPORT_NAME .'/bus_route/list_bus_route',$this->data);
				//$this->list_bus_route();			
				
		}		
	}
	













	
	public function list_bus_route()
	{
		
  		if ($this->checkLogin('O') == '') 
  		{
		redirect(TRANSPORT_NAME);
        } else { 
			
		
        if ($this->lang->line('route_list_header') != '') 
                $this->data['heading']= stripslashes($this->lang->line('route_list_header')); 
        else  $this->data['heading'] = 'Display Route List';	
        $sortArr  = array('created' => 'DESC');
        $sortby = '';
        if (isset($_GET['sortby'])) {
                $this->data['filter'] = 'filter';
                $sortby = $_GET['sortby'];
                if($sortby=="doj_asc"){
                        $sortArr  = array('created' => 'ASC');
                }
                if($sortby=="doj_desc"){
                        $sortArr  = array('created' => 'DESC');
                }
                if($sortby=="rides_asc"){
                        $sortArr  = array('no_of_rides' => 'ASC');
                }
                if($sortby=="rides_desc"){
                        $sortArr  = array('no_of_rides' => 'DESC');
                }
        }
        $this->data['sortby'] = $sortby;
        $condition = $filterArr = array();
        $filterCondition = array();
        if (isset($_GET['type']) && (isset($_GET['value']) || isset($_GET['vehicle_category'])) && $_GET['type'] != '' && ($_GET['value'] != '' || $_GET['vehicle_category'] != '')) {
            if (isset($_GET['type']) && $_GET['type'] != '') {
                    $this->data['type'] = $_GET['type'];
            }
            $filter_val = '';
            if (isset($_GET['value']) && $_GET['value'] != '') {
                    $this->data['value'] = $_GET['value'];
                    $filter_val = $this->data['value'];
            }
            $this->data['filter'] = 'filter';
            $filterCondition = array();
            if($_GET['type'] == 'vehicle_type'){
                    $vehicle_category = trim($_GET['vehicle_category']);
                    $categoryVal=$this->user_model->get_all_details(CATEGORY,'','','','',array('name'=>$vehicle_category));
                    $filterCondition = array('category' => $categoryVal->row()->_id);
            } else if($_GET['type'] == 'driver_location') {
                    $location=$this->user_model->get_all_details(LOCATIONS,'','','','',array('city'=>$_GET['value']));
                    $filterArr = array($this->data['type'] => $location->row()->_id);
            } else if($_GET['type'] == 'mobile_number') {
                    $filterCondition["mobile_number"] = $filter_val;
                    $filterCondition["dail_code"] = $_GET['country'];                
            } else if($_GET['type'] == 'aadhar_card_number' || $_GET['type'] == 'vehicle_number') {
            $filterCondition = array($this->data['type'] => $_GET['value']);
    
            }else{ 
                    $filterArr = array($this->data['type'] => $filter_val,"operator_id"=> $this->session->userdata(APP_NAME.'_session_operator_id'));
            }    
        }
        
       // $this->cimongo->where(array('driver_location' => (string)$this->session->userdata(APP_NAME.'_session_operator_location'),'operator_id' => $this->session->userdata(APP_NAME.'_session_operator_id')));
        
        $route_count = $this->bus_route_model->get_all_counts(BUS_ROUTE, $filterCondition,$filterArr);
        if ($route_count > 1000) {
            $searchPerPage = 500;
            $paginationNo = $this->uri->segment(4);
            if ($paginationNo == '') {
                $paginationNo = 0;
            } else {
                $paginationNo = $paginationNo;
            }
			//$this->cimongo->where(array('driver_location'=>(string)$this->session->userdata(APP_NAME.'_session_operator_location'),"operator_id"=> $this->session->userdata(APP_NAME.'_session_operator_id')));
            //$this->data['driversList'] = $this->user_model->get_all_details(DRIVERS, $filterCondition, $sortArr, $searchPerPage, $paginationNo,$filterArr);

            $this->data['route_list'] = $this->bus_route_model->get_all_details(BUS_ROUTE, $filterCondition, $sortArr, $searchPerPage, $paginationNo,$filterArr);
		

            $searchbaseUrl = TRANSPORT_NAME.'/bus_route/list_route/'; 
            $config['num_links'] = 3;
            $config['display_pages'] = TRUE;
            $config['base_url'] = $searchbaseUrl;
            $config['total_rows'] = $route_count;
            $config["per_page"] = $searchPerPage;
            $config["uri_segment"] = 4;
            $config['first_link'] = '';
            $config['last_link'] = '';
            $config['full_tag_open'] = '<ul class="tsc_pagination tsc_paginationA tsc_paginationA01">';
            $config['full_tag_close'] = '</ul>';
            if ($this->lang->line('pagination_prev_lbl') != '') $config['prev_link'] =stripslashes($this->lang->line('pagination_prev_lbl'));  else  $config['prev_link'] ='Prev';
            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '</li>';
            if ($this->lang->line('pagination_next_lbl') != '') $config['next_link'] =stripslashes($this->lang->line('pagination_next_lbl'));  else  $config['next_link'] ='Next';
            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="current"><a href="javascript:void(0);" style="cursor:default;">';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';
            $config['first_tag_open'] = '<li>';
            $config['first_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li>';
            $config['last_tag_close'] = '</li>';
						
            if ($this->lang->line('pagination_first_lbl') != '') $config['first_link'] =stripslashes($this->lang->line('pagination_first_lbl'));  else  $config['first_link'] ='First';
            if ($this->lang->line('pagination_last_lbl') != '') $config['last_link'] = stripslashes($this->lang->line('pagination_last_lbl'));  else  $config['last_link'] ='Last';
            $this->pagination->initialize($config);
            $paginationLink = $this->pagination->create_links();
            $this->data['paginationLink'] = $paginationLink;
        } else {
            $this->data['paginationLink'] = '';
            $condition = array();
			//$this->cimongo->where(array('driver_location'=>(string)$this->session->userdata(APP_NAME.'_session_operator_location'),"operator_id"=> $this->session->userdata(APP_NAME.'_session_operator_id')));
            //$this->data['driversList'] = $this->driver_model->get_all_details(DRIVERS,  $filterCondition, $sortArr, '', '', $filterArr);
			
			$this->data['route_list'] = $this->bus_route_model->get_all_details(BUS_ROUTE, array("operator_id"=> $this->session->userdata(APP_NAME.'_session_operator_id')),array('_id' => 'DESC'));
			//var_dump($this->data['route_list']->result() );
        }
		
		$cabCats = $this->driver_model->get_selected_fields(CATEGORY, array(), array('_id', 'name','name_languages'))->result();
		
        $cabsTypeArr = array();
		$langCode = $this->data['langCode'];
        foreach ($cabCats as $cab) {
			
			$category_name = $cab->name;
			if(isset($cab->name_languages[$langCode ]) && $cab->name_languages[$langCode ] != '') $category_name = $cab->name_languages[$langCode ];
			
            $cabId = (string) $cab->_id;
            $cabsTypeArr[$cabId] = $cab;
			$cabsTypeArr[$cabId]->name = $category_name;
        }
        $this->data['cabCats'] = $cabsTypeArr;
		
        $this->load->view(TRANSPORT_NAME .'/bus_route/list_route', $this->data);
		
		
		}

	}



	 public function view_bus_route()


	{

		if ($this->checkLogin('O') == '') 
  		{
		redirect(TRANSPORT_NAME);
        } else { 
			$route_id = $this->uri->segment(4);



			$this->data['center']=$this->config->item('latitude').','.$this->config->item('longitude');
		
			$this->data['route_detail'] = $this->bus_route_model->get_all_details(BUS_ROUTE, array("_id" =>  new MongoDB\BSON\ObjectId($route_id)) );

			
			$this->load->view(TRANSPORT_NAME .'/bus_route/route_view', $this->data);
		}
		
	}





	public function delete_bus_routes(){
		
		if ($this->checkLogin('O') == '')
		{
			redirect(TRANSPORT_NAME);
		} 
		else 
		{
			$operator_id = $this->checkLogin('O');

			$bus_route_id = $this->uri->segment(4,0);			
			$condition = array('_id' => new MongoDB\BSON\ObjectId($bus_route_id));								//	id
			$operator_id_condition = array('operator_id' => new MongoDB\BSON\ObjectId($operator_id));			//	operator id
			$route_id_condition = array('route_details.route_id' => $bus_route_id);					//	route id		

			$check_route_assign =$this->bus_route_model->get_all_details(BUS_TRIP,  $route_id_condition);

		

			if( $check_route_assign->num_rows() == 0)
			{
				$this->app_model->commonDelete(BUS_ROUTE, $condition);
				$this->setErrorMessage('success', 'Bus route deleted successfully','institution_bus_route_deleted_sucess');
			}
			else 
			{

			

				if ($this->lang->line('route_is_assigned') != '') 
				{
								$errMsg=str_replace("name_here",$check_route_assign->result()[0]->trip_name,$this->lang->line('route_is_assigned'));

				}
				else
				{
				  $errMsg =str_replace("name_here",$check_route_assign->result()[0]->trip_name,"Route cannot be deleted since it is assigned to the trip - name_here");
				}  



				

				$this->setErrorMessage('error', $errMsg);

			}

	//		$this->setErrorMessage('success', 'Bus route deleted successfully','institution_bus_route_deleted_sucess');
			redirect(TRANSPORT_NAME.'/bus_route/list_bus_route');

		}
	}








 public function edit_bus_route()
 {




 		if ($this->checkLogin('O') == '') 
  		{
		redirect(TRANSPORT_NAME);
        } else { 
		


        $bus_route_id = $this->uri->segment(4,0);		
        
        $this->date['route_details']=$this->bus_route_model->get_all_details(BUS_ROUTE, array("_id"=> new MongoDB\BSON\ObjectId($bus_route_id) ))->result();

     	  $this->date['bus_route_id']=$bus_route_id;

     	   $stop_ids=array(); 

     	  foreach ($this->date['route_details'][0]->waypoints as $key => $value) 
     	  {
     	  	
     	  	$student_assigned_stops=$this->app_model->get_all_details(STUDENT_DETAILS, array('$or' => array(array("student_drop_bus_stop_id"=> $value['_id'] ),array("student_pickup_bus_stop_id"=> $value['_id'] ) )))->result();

	     	  	if(!empty($student_assigned_stops))
	     	  	{
	     	  		array_push($stop_ids,$value['_id']);
	     	  	}

     	  }  

     	  
     	$this->data['do_not_delete_stops']=$stop_ids;


		$this->data['center']=$this->session->userdata(APP_NAME.'_session_operator_location')['lat'].','.$this->session->userdata(APP_NAME.'_session_operator_location')['lng'];
		
		
		 $this->data['heading'] = 'Edit Route';	
		$this->load->view(TRANSPORT_NAME.'/bus_route/edit_bus_route',$this->data);
		
		
		}
















 }	


 public function update_bus_route()
 {
 	if ($this->checkLogin('O') == '') 
  		{
		redirect(TRANSPORT_NAME);
        } else { 
		



				
				$way_data=json_decode($this->input->post('stopping_point'));

				//$for_checking=array_reverse(json_decode($this->input->post('stopping_point')));



				$way_array=array();

				$start_point_full_details=json_decode($this->input->post('starting_point'));
				$end_point_full_details=json_decode($this->input->post('ending_point'));
				
				$excludeArr= array("starting_point","stopping_point","ending_point","route_name","route_name_hidden",'polyencode','route_id','mongo_id');

				for($i=0;$i < sizeof($way_data); $i++)
				{
					if(isset($way_data[$i]->mongo_id) && $way_data[$i]->mongo_id != "" )
					{	
					$id= new MongoDB\BSON\ObjectId($way_data[$i]->mongo_id);

					 unset($way_data[$i]->mongo_id);
					}
					else
					{
						$id= new MongoDB\BSON\ObjectId();	

						 unset($way_data[$i]->mongo_id);
					}	

					

					 $set_arr=$way_data[$i];
					
					$set_arr=array('_id' => $id , "way_stop" => $set_arr );;

					array_push($way_array , $set_arr);
				}

				
				
			
				$trip_data_all = $this->app_model->get_all_details(BUS_TRIP, array("route_details.route_id"=> (string)$this->input->post('route_id')))->result();


				

				if(!empty($trip_data_all))
				{

					$route_details=array();

					foreach ($trip_data_all as $key => $value) {


								$route_data_result=$value->route_details;


								

								$waypoints_array=array();


								
							
								$stop_resetting=array();

									for($i=0;$i < sizeof($way_array); $i++)
										{
										
											if(isset($way_array[$i]["_id"]) &&  	$way_array[$i]["_id"] != "" )
											{	
												$data_index=array_search((string)$way_array[$i]["_id"], array_column($route_data_result['waypoints_details'], 'id'));
												
												

												if((string)$data_index != "")
												{
													
													echo "test".$data_index."test2<br>";
													$pusher_array=array("id" => (string)$way_array[$i]["_id"],"busstop_name" => $way_array[$i]['way_stop']->busstop_name,"location_name" => $way_array[$i]['way_stop']->location_name,"waypoint" => $way_array[$i]['way_stop']->waypoint,"stop_time" =>  $route_data_result['waypoints_details'][$data_index]['stop_time'] );
													array_push($stop_resetting,$pusher_array);
												}
												else
												{
													$pusher_array=array("id" => (string)$way_array[$i]["_id"],"busstop_name" => $way_array[$i]['way_stop']->busstop_name,"location_name" => $way_array[$i]['way_stop']->location_name,"waypoint" => $way_array[$i]['way_stop']->waypoint,"stop_time" => "-" );
													array_push($stop_resetting,$pusher_array);
												}	

											}
																				

										}
															
									$route_details=array('route_id' => (string)$this->input->post('route_id'),'route_name' => (string)$this->input->post('route_name_hidden'),"start_point" => $start_point_full_details[0]->start_point,"endpoint" => $start_point_full_details[0]->start_point,"waypoints_details" => $stop_resetting);
										
									$condition = array("_id" =>  new MongoDB\BSON\ObjectId($value->_id));

									$dataArr = array("route_details" => $route_details);
									$this->app_model->commonInsertUpdate(BUS_TRIP,'update',$excludeArr,$dataArr,$condition); 

						
					}

					




				}

			

	
				
				$condition = array("_id" =>  new MongoDB\BSON\ObjectId($this->input->post('route_id')));
				
				$dataArr = array(
								'operator_id' => $this->session->userdata(APP_NAME.'_session_operator_id'),	
								'route_name'  =>$this->input->post('route_name_hidden'),
								'startpoints' =>json_decode($this->input->post('starting_point')), 
								'endpoints' =>json_decode($this->input->post('ending_point')),	
				
								'waypoints' => $way_array,

								'polyencode' => $this->input->post('polyencode'),
								
								'updated_by_id' => $this->session->userdata(APP_NAME.'_session_changes_done_by_id'),
								'updated_by_name' => $this->session->userdata(APP_NAME.'_session_changes_done_by_name'),
								'updated_at' => date("Y-m-d H:i:s") 
								
								);







				
				$this->bus_route_model->commonInsertUpdate(BUS_ROUTE,'update',$excludeArr,$dataArr,$condition);
				$location_id = $this->cimongo->insert_id();
				
			 	
				$this->setErrorMessage('success','Route updated successfully now can create a trip for this route');
				
			
				$this->data['center']=$this->session->userdata(APP_NAME.'_session_operator_location')['lat'].','.$this->session->userdata(APP_NAME.'_session_operator_location')['lng'];
		
		
		
				$this->data['heading'] = 'Add New Route';	
				redirect(TRANSPORT_NAME .'/bus_route/list_bus_route',$this->data);

		}


 }











}
/* End of file trip.php */
/* Location: ./application/controllers/operator/trip.php */