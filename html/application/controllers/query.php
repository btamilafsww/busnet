<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Query extends MY_Controller {
	function __construct(){
        parent::__construct();
		$this->load->helper(array('cookie','date','form'));
		$this->load->library(array('encrypt','form_validation'));		
		$this->load->model(array('user_model'));
		$this->load->model(array('app_model'));
    }
	public function geoindex(){
		$this->cimongo->ensure_index(RIDE_STATISTICS,array('location'=>'2dsphere'));
		$this->cimongo->ensure_index(USERS,array('loc'=>'2dsphere'));
	}
	public function v_ride(){
		$rideId = $this->uri->segment(3);
		$getRide = $this->user_model->get_all_details(RIDES,array('ride_id' => $rideId));
		echo '<pre>'; print_r($getRide->result()); die;
	}
	public function h_ride(){
		$rideId = $this->uri->segment(3);
		$getRideHIstory = $this->user_model->get_all_details(TRAVEL_HISTORY,array('ride_id' => $rideId));
		echo '<pre>'; print_r($getRideHIstory->result()); die;
	}   
	public function j_ride() {
		$res_arr = array();
		$res2_arr = array();
		$res3_arr = array();
		$res4_arr = array();
		$val1 = array();
		$val2 = array();
		$rideId = $this->uri->segment(3);
		$getRideHIstory = $this->user_model->get_all_details(TRAVEL_HISTORY,array('ride_id' => $rideId));
		foreach ($getRideHIstory->result() as $key => $data) {
			foreach($data->history_end as $value) {
				$val1[]=array('lat'=>$value['lat'],'lon'=>$value['lon']); 
			}
		}
		$json_encode = json_encode($val1);
		echo $this->cleanString($json_encode);
	}
	
	public function makers_model() {
		/* $data=file_get_contents(base_url().'vehicle_maker_models_years.json');
		$new_data=json_decode($data);
		
		foreach($new_data as $key=>$record) {
			foreach($record as $maker) {
					
					$maker_name=$maker->name;
					$insertArr['brand'] = '';
					$insertArr['brand_name'] = '';
					$sel_fields = array('_id','brand_name');
					$seourl = str_replace(' ','-',trim($maker_name));
					$condition = array('seourl' => $seourl);
					$checkMaker = $this->user_model->get_selected_fields(BRAND, $condition,$sel_fields);
					if($checkMaker->num_rows() == 1){
						$insertArr['brand'] = (string)$checkMaker->row()->_id;
						$insertArr['brand_name'] = $checkMaker->row()->brand_name;
					} else {
						$brand_data= array('brand_name' => ucfirst($maker_name), 'seourl' => $seourl,'status' => 'Active','created' => date('Y-m-d H:i:s'),'brand_logo' => '');
						$this->user_model->simple_insert(BRAND,$brand_data);
						$insertArr['brand'] = (string)$this->cimongo->insert_id();
					    $insertArr['brand_name'] = ucfirst($maker_name);
					}
					$insertArr['type'] = '';
					$insertArr['type_name'] = '';
			
					
					$checkType = $this->user_model->get_selected_fields(VEHICLES,array(),array('_id','vehicle_type'));
					$vehicle_type=array();
					foreach($checkType->result() as $data) {
						if($data->vehicle_type!='') {
							$vehicle_type[]=array('type'=>(string)$data->_id,'type_name'=>$data->vehicle_type);
						}
					}
					
					$random_index=rand(0,count($vehicle_type));
					$insertArr['type']=$vehicle_type[$random_index]['type'];
					$insertArr['type_name']=$vehicle_type[$random_index]['type_name'];
					if($insertArr['type']=='' && $insertArr['type_name']=='') {
						$insertArr['type']=(string)$checkType->row()->_id;
						$insertArr['type_name']=$checkType->row()->vehicle_type;
					}
					
					//print_r($maker->models);
					foreach($maker->models as $models) {
						//print_r($models);
						$model_condition = array();
						$insertArr['name'] = '';
						$insertArr['name'] = $models->name;
						$sel_fields = array('_id','name');
						$condition = array('$or'=>array(array('name' => $models->name),array('name' => ucfirst($models->name)),array('name' => strtolower($models->name)),array('name' => strtoupper($models->name)),array('name' => ucwords($models->name))));
						$checkModels = $this->user_model->get_selected_fields(MODELS, $condition,$sel_fields);
						if($checkModels->num_rows() == 1){
							$model_condition = array('name' => $checkModels->row()->name);
							
						} else {
							$years_array=array();
							foreach($models->years as $year) {
								
								$years_array[]=$year->year;
							}
							//print_r($years_array);
							
							$insertArr['year_of_model']=$years_array;
							$insertArr['status']='Active';
							
							unset($insertArr['_id']);
							
							$this->user_model->simple_insert(MODELS,$insertArr);
							
						}
						
				  }
			}
		} */
		
	}
	public function remove_make_model() {
		/* $this->user_model->commonDelete(BRAND,array());
        $this->user_model->commonDelete(MODELS,array()); */
	}
	public function v_driver(){
		$driver_id=$this->uri->segment(3);
		$getInfo = $this->user_model->get_all_details(DRIVERS, array('_id' => new MongoDB\BSON\ObjectId(($driver_id))));
		if($getInfo->num_rows()>0){
			echo "<pre>"; print_r($getInfo->result());
		}else{
			echo "Invalid";
		}
	}
	public function v_user(){
		$driver_id=$this->uri->segment(3);
		$getInfo = $this->user_model->get_all_details(USERS, array('_id' => new MongoDB\BSON\ObjectId(($driver_id))));
		if($getInfo->num_rows()>0){
			echo "<pre>"; print_r($getInfo->result());
		}else{
			echo "Invalid";
		}
	}
	
	public function gMap(){
		$ride_id=$this->uri->segment(3);
		$this->load->helper(array('ride_helper'));
		create_and_save_travel_path_in_map($ride_id);
	}
	
	public function u_wallet(){
		$userInfo = $this->user_model->get_all_details(USERS,array());
		if($userInfo->num_rows()>0){
			foreach($userInfo->result() as $row){
				$user_id = (string)$row->_id;
				$walletInfo = $this->app_model->get_all_details(WALLET, array('user_id' => new MongoDB\BSON\ObjectId($user_id)));
				if($walletInfo->num_rows() == 1){
					$amount = floatval($walletInfo->row()->total);
					if($amount<=0){
						$amount = 0;
					}
					$this->app_model->update_details(USERS,array('wallet_amount' =>floatval($amount)),array('_id' => new MongoDB\BSON\ObjectId($user_id)));
					
					echo $user_id." : ".$amount.'<br/>';
					
				}
			}
		}
	}
	public function invoice_check() {
		$this->mail_model->send_invoice('883483','arockiasamy@Katenterprise.in');
		echo "done";
	}
	public function update_amount_ride() {
		$ride_detail = $this->app_model->get_all_details(RIDES, array('ride_status' =>'Completed'));
		foreach($ride_detail->result() as $data) {
			$original_grand_fare=$data->total['grand_fare'];
			$grand_fare=round($data->total['grand_fare']);
			$this->app_model->update_details(RIDES,array('total.original_grand_fare' =>floatval($original_grand_fare),'total.grand_fare'=>floatval($grand_fare)),array('ride_id' =>$data->ride_id));
		}
	}
	public function invoice_mail() {
		$message=file_get_contents('http://192.168.1.251:8081/arockia/invoice/invoice.html');
		
		$invoicename = time(). '.pdf';
		$file_to_save = 'trip_invoice/' . $invoicename;
		include("./mpdf/mpdf.php");
		$mpdf=new mPDF($langcode); 
		//$stylesheet = file_get_contents('pdf.css');
		//$mpdf->WriteHTML($stylesheet,1);
		$mpdf->WriteHTML(stripcslashes($message));
		$mpdf->Output($file_to_save,'D');
		$attachments = $file_to_save;
	
	}
	
	public function refer_history() {
	
		$ride_detail = $this->app_model->get_all_details(REFER_HISTORY,array('history'=>array('$all'=>array(array('$elemMatch'=>array('reference_id'=>'581876efcae2aa4c1700002a','used'=>'true'))))));
		echo "<pre>";
		print_r($ride_detail->result());
	}
	
}

/* End of file query.php */
/* Location: ./application/controllers/query.php */