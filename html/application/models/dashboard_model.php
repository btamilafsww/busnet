<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * 
 * This model contains all db functions related to admin management
 * @author Katenterprise
 *
 * */
class Dashboard_model extends My_Model {

    public function __construct() {
         $this->load->model('app_model');
        parent::__construct();
    }

    /**
     *
     * This function return the total number of on rides
     *
     * */
    public function get_on_rides($driver_id = '', $operator_id = '') {
        if ($driver_id != '') {
            $where_clause = array(
                '$or' => array(
                    array("ride_status" => 'Confirmed'),
                    array("ride_status" => 'Arrived'),
                    array("ride_status" => 'Onride'),
                    array("ride_status" => 'Finished'),
                ),
                'driver.id' => $driver_id
            );
        } else {
            $where_clause = array(
                '$or' => array(
                    array("ride_status" => 'Confirmed'),
                    array("ride_status" => 'Arrived'),
                    array("ride_status" => 'Onride'),
                    array("ride_status" => 'Finished'),
                )
            );
        }
        $this->cimongo->where($where_clause, TRUE);
				if($operator_id != ''){
						$this->cimongo->where(array('booked_by' => $operator_id));
				}						   
        $res = $this->cimongo->count_all_results(RIDES);
        return $res;
    }
	
	public function get_on_rides_company($company_id = '') {
			$where_clause = array(
			'$or' => array(
				array("ride_status" => 'Confirmed'),
				array("ride_status" => 'Arrived'),
				array("ride_status" => 'Onride'),
				array("ride_status" => 'Finished'),
			)
		);
        $this->cimongo->where($where_clause, TRUE);
		if($company_id != ''){
				$this->cimongo->where(array('company_id' => $company_id));
		}						   
        $res = $this->cimongo->count_all_results(RIDES);
        return $res;
    }

    /**
     *
     * This function return the total earnings
     *
     * */
    public function get_total_earnings($driver_id = '') {
        if ($driver_id != '') {
            $option = array(
                array(
                    '$project' => array(
                        'ride_status' => 1,
                        'total' => 1,
                        'driver.id' => 1
                    )
                ),
                array(
                    '$match' => array(
                        'ride_status' => array('$eq' => 'Completed'),
                        'driver.id' => $driver_id
                    )
                ),
                array(
                    '$group' => array(
                        '_id' => '$ride_status',
                        'ride_status' => ['$last' => '$ride_status'],
                        'totalAmount' => array('$sum' => '$total.grand_fare')
                    )
                )
            );
        } else {
            $option = array(
                array(
                    '$project' => array(
                        'ride_status' => 1,
                        'total' => 1
                    )
                ),
                array(
                    '$match' => array(
                        'ride_status' => array('$eq' => 'Completed')
                    )
                ),
                array(
                    '$group' => array(
                        '_id' => '$ride_status',
                        'ride_status' => ['$last' => '$ride_status'],
                        'totalAmount' => array('$sum' => '$total.grand_fare')
                    )
                )
            );
        }
        $cursor = $this->cimongo->aggregate(RIDES, $option);
        $res = $cursor->toArray();
        $totalAmount = 0;
        if (!empty($res)) {
            if (isset($res['result'][0]['totalAmount'])) {
                $totalAmount = $res['result'][0]['totalAmount'];
            }
        }
        return $totalAmount;
    }
    
    

    /**
     *
     * This function return the current month details
     *
     * */
   
	
	public function get_this_month_drivers_company($company_id = '') {
      if($company_id != ''){
            $option = array(
                array(
                    '$project' => array(
                        'driver_name' => 1,
                        'created' => 1,
						'company_id' => 1
                    )
                ),
                array(
                    '$match' => array(
                        'created' => array('$gte' => date('Y-m-01 00:00:00'), '$lte' => date('Y-m-d H:i:s')),
												'company_id' => $company_id
                    )
                )
            );
        } else {
            $option = array(
                array(
                    '$project' => array(
                        'driver_name' => 1,
                        'created' => 1
                    )
                ),
                array(
                    '$match' => array(
                        'created' => array('$gte' => date('Y-m-01 00:00:00'), '$lte' => date('Y-m-d H:i:s'))
                    )
                )
            );
        }
        $res = $this->cimongo->aggregate(DRIVERS, $option);
        return $res;
    }
	public function get_this_today_drivers_company($company_id = '') {
      if($company_id != ''){
            $option = array(
                array(
                    '$project' => array(
                        'driver_name' => 1,
                        'created' => 1,
						'company_id' => 1
                    )
                ),
                array(
                    '$match' => array(
                        'created' => array('$gte' => date('Y-m-d 00:00:00'), '$lte' => date('Y-m-d H:i:s')),
												'company_id' => $company_id
                    )
                )
            );
        } else {
            $option = array(
                array(
                    '$project' => array(
                        'driver_name' => 1,
                        'created' => 1
                    )
                ),
                array(
                    '$match' => array(
                        'created' => array('$gte' => date('Y-m-01 00:00:00'), '$lte' => date('Y-m-d H:i:s'))
                    )
                )
            );
        }
        $res = $this->cimongo->aggregate(DRIVERS, $option);
        return $res;
    }



	
	public function get_this_year_drivers_company($company_id = '') {

      if($company_id != ''){
            $option = array(
                array(
                    '$project' => array(
                        'driver_name' => 1,
                        'created' => 1,
						'operator_id' => 1
                    )
                ),
                array(
                    '$match' => array(
                        'created' => array('$gte' => date('Y-01-01 00:00:00'), '$lte' => date('Y-m-d H:i:s')),
						'operator_id' => $company_id
                    )
                )
            );
        } else {
            $option = array(
                array(
                    '$project' => array(
                        'driver_name' => 1,
                        'created' => 1
                    )
                ),
                array(
                    '$match' => array(
                        'created' => array('$gte' => date('Y-m-01 00:00:00'), '$lte' => date('Y-m-d H:i:s'))
                    )
                )
            );
        }
        $res = $this->cimongo->aggregate(DRIVERS, $option);
        return $res;
    }
	
    /**
     *
     * This function return the current month details
     *
     * */
    public function get_this_month_rides($driver_id = '', $operator_id = '') {
	
		$groupArr = array(
						'$group' => array(
							'_id' =>'$driver.id',
							'ride_count'=>array('$sum'=>1)
						)
					);
		
        if ($driver_id != '') {
            $option = array(
                array(
                    '$project' => array(
                        'ride_id' => 1,
                        'history.end_ride' => 1,
                        'driver.id' => 1
                    )
                ),
                array(
                    '$match' => array(
                        'history.end_ride' => array('$gte' => new MongoDB\BSON\UTCDateTime(strtotime(date('Y-m-01 00:00:00'))), '$lte' => new MongoDB\BSON\UTCDateTime(strtotime(date('Y-m-d H:i:s')))),
                        'driver.id' => $driver_id
                    )
                ),
				$groupArr
            );
        } else if($operator_id != ''){
            $option = array(
                array(
                    '$project' => array(
                        'ride_id' => 1,
                        'history.end_ride' => 1,
												'booked_by' => 1
                    )
                ),
                array(
                    '$match' => array(
                        'history.end_ride' => array('$gte' => new MongoDB\BSON\UTCDateTime(strtotime(date('Y-m-01 00:00:00'))), '$lte' => new MongoDB\BSON\UTCDateTime(strtotime(date('Y-m-d H:i:s')))),
												'booked_by' => $operator_id
                    )
                )
            );
        } else {
            $option = array(
                array(
                    '$project' => array(
                        'ride_id' => 1,
                        'history.end_ride' => 1
                    )
                ),
                array(
                    '$match' => array(
                        'history.end_ride' => array('$gte' => new MongoDB\BSON\UTCDateTime(strtotime(date('Y-m-01 00:00:00'))), '$lte' => new MongoDB\BSON\UTCDateTime(strtotime(date('Y-m-d H:i:s'))))
                    )
                ),
				$groupArr
            );
        }
        $cursor = $this->cimongo->aggregate(RIDES, $option);
        $res = $cursor->toArray();
        return $res;
    }
    public function get_this_month_rides_company($company_id = '') {
       
		$option = array(
			array(
				'$project' => array(
					'ride_id' => 1,
					'company_id' => 1,
					'history.end_ride' => 1,
					'booked_by' => 1
				)
			),
			array(
				'$match' => array(
					'history.end_ride' => array('$gte' => new MongoDB\BSON\UTCDateTime(strtotime(date('Y-m-01 00:00:00'))), '$lte' => new MongoDB\BSON\UTCDateTime(strtotime(date('Y-m-d H:i:s')))),
					'company_id' => $company_id
				)
			)
		);
        
        $res = $this->cimongo->aggregate(RIDES, $option);
        return $res;
    }
    
	
	public function get_today_rides($driver_id = '',$operator_id = '',$ride_status='') {
	
		$groupArr=array(
								'$group' => array(
									'_id' =>'$driver.id',
									'ride_count'=>array('$sum'=>1)
								)
							);
	
        if ($driver_id != '') {
            $option = array(
                array(
                    '$project' => array(
                        'ride_id' => 1,
                        'history.end_ride' => 1,
                        'driver.id' => 1,
						'ride_status' => 1
                    )
                ),
                array(
                    '$match' => array(
                        'history.end_ride' => array('$gte' => new MongoDB\BSON\UTCDateTime(strtotime(date('Y-m-d 00:00:00'))), '$lte' => new MongoDB\BSON\UTCDateTime(strtotime(date('Y-m-d H:i:s')))),
                        'driver.id' => $driver_id
                    )
                ),
				$groupArr
            );
			if($ride_status != ''){
				$option[1]['$match']['ride_status'] = $ride_status;
				#echo '<pre>'; print_r($option); die;
			}
        } else if($operator_id != ''){
            $option = array(
                array(
                    '$project' => array(
                        'ride_id' => 1,
                        'history.end_ride' => 1,
												'booked_by' => 1
                    )
                ),
                array(
                    '$match' => array(
                        'history.end_ride' => array('$gte' => new MongoDB\BSON\UTCDateTime(strtotime(date('Y-m-d 00:00:00'))), '$lte' => new MongoDB\BSON\UTCDateTime(strtotime(date('Y-m-d H:i:s')))),
												'booked_by' => $operator_id
                    )
                )
            );
        } else {
            $option = array(
                array(
                    '$project' => array(
                        'ride_id' => 1,
                        'history.end_ride' => 1
                    )
                ),
                array(
                    '$match' => array(
                        'history.end_ride' => array('$gte' => new MongoDB\BSON\UTCDateTime(strtotime(date('Y-m-d 00:00:00'))), '$lte' => new MongoDB\BSON\UTCDateTime(strtotime(date('Y-m-d H:i:s'))))
                    )
                ),
				$groupArr
            );
        }
        $cursor = $this->cimongo->aggregate(RIDES, $option);
        $res = $cursor->toArray();
        return $res;
    }
	
	









	
   
   
	/**
     *
     * This function return the current year details
     *
     * */
    public function get_this_year_rides($driver_id = '', $operator_id = '') {
        
		$groupArr=array(
						'$group' => array(
							'_id' =>'$driver.id',
							'ride_count'=>array('$sum'=>1)
						)
					);
								
		if ($driver_id != '') {
            $option = array(
                array(
                    '$project' => array(
                        'ride_id' => 1,
                        'history.end_ride' => 1,
                        'driver.id' => 1
                    )
                ),
                array(
                    '$match' => array(
                        'history.end_ride' => array('$gte' => new MongoDB\BSON\UTCDateTime(strtotime(date('Y-01-01 00:00:00'))), '$lte' => new MongoDB\BSON\UTCDateTime(strtotime(date('Y-m-d H:i:s')))),
                        'driver.id' => $driver_id
                    )
                ),
				$groupArr
            );
        } else if($operator_id != ''){
            $option = array(
                array(
                    '$project' => array(
                        'ride_id' => 1,
                        'history.end_ride' => 1,
												'booked_by' => 1
                    )
                ),
                array(
                    '$match' => array(
                        'history.end_ride' => array('$gte' => new MongoDB\BSON\UTCDateTime(strtotime(date('Y-01-01 00:00:00'))), '$lte' => new MongoDB\BSON\UTCDateTime(strtotime(date('Y-m-d H:i:s')))),
												'booked_by' => $operator_id
                    )
                )
            );
        } else {
            $option = array(
                array(
                    '$project' => array(
                        'ride_id' => 1,
                        'history.end_ride' => 1
                    )
                ),
                array(
                    '$match' => array(
                        'history.end_ride' => array('$gte' => new MongoDB\BSON\UTCDateTime(strtotime(date('Y-01-01 00:00:00'))), '$lte' => new MongoDB\BSON\UTCDateTime(strtotime(date('Y-m-d H:i:s'))))
                    )
                ),
				$groupArr
            );
        }
        $cursor = $this->cimongo->aggregate(RIDES, $option);
        $res = $cursor->toArray();
        return $res;
    }
	
	public function get_this_year_rides_company($company_id = '') {
		$option = array(
			array(
				'$project' => array(
					'ride_id' => 1,
					'company_id' => 1,
					'history.end_ride' => 1
				)
			),
			array(
				'$match' => array(
					'history.end_ride' => array('$gte' => new MongoDB\BSON\UTCDateTime(strtotime(date('Y-01-01 00:00:00'))), '$lte' => new MongoDB\BSON\UTCDateTime(strtotime(date('Y-m-d H:i:s')))),
					'company_id'=>$company_id
				)
			)
		);
        
        $res = $this->cimongo->aggregate(RIDES, $option);
        return $res;
    }
	public function get_this_today_rides_company($company_id = '') {
		$option = array(
			array(
				'$project' => array(
					'ride_id' => 1,
					'company_id' => 1,
					'history.end_ride' => 1
				)
			),
			array(
				'$match' => array(
					'history.end_ride' => array('$gte' => new MongoDB\BSON\UTCDateTime(strtotime(date('Y-m-d 00:00:00'))), '$lte' => new MongoDB\BSON\UTCDateTime(strtotime(date('Y-m-d H:i:s')))),
					'company_id'=>$company_id
				)
			)
		);
        
        $res = $this->cimongo->aggregate(RIDES, $option);
        return $res;
    }

    /**
     *
     * This function return the monthly earnings
     *
     * */
    public function get_monthly_earnings($fromdate = '', $todate = '', $driver_id = '') {
        if ($driver_id != '') {
            $option = array(
                array(
                    '$project' => array(
                        'ride_status' => 1,
                        'total' => 1,
                        'amount_commission' => 1,
                        'commission_percent' => 1,
						'driver_revenue' => 1,
                        'driver.id' => 1,
                        'pay_status' => 1,
                        'history' => 1
                    )
                ),
                array(
                    '$match' => array(
                        'ride_status' => array('$eq' => 'Completed'),
                        'pay_status' =>array('$eq'=>'Paid'),
                        'history.end_ride' => array('$gte' => new MongoDB\BSON\UTCDateTime($fromdate), '$lte' => new MongoDB\BSON\UTCDateTime($todate)),
                        'driver.id' => $driver_id
                    )
                ),
                array(
                    '$group' => array(
                        '_id' => '$ride_status',
                        #'ride_status' => ['$last' => '$ride_status'],
                        'totalAmount' => array('$sum' => '$driver_revenue'),
                        'driver_Earnings' => array('$sum' => '$driver_revenue'),
                        'site_Earnings' => array('$sum' => '$amount_commission')
                    )
                )
            );
        } else {
            $option = array(
                array(
                    '$project' => array(
                        'ride_status' => 1,
                        'total' => 1,
                        'driver_revenue' => 1,
                        'amount_commission' => 1,
                        'pay_status' => 1,
                        'history' => 1
                    )
                ),
                array(
                    '$match' => array(
                        'ride_status' => array('$eq' => 'Completed'),
                        'pay_status' =>array('$eq'=>'Paid'),
                        'history.end_ride' => array('$gte' => new MongoDB\BSON\UTCDateTime($fromdate), '$lte' => new MongoDB\BSON\UTCDateTime($todate))
                    )
                ),
                array(
                    '$group' => array(
                        '_id' => '$ride_status',
                       # 'ride_status' => ['$last' => '$ride_status'],
                        'totalAmount' => array('$sum' => '$total.grand_fare'),
                        'driver_Earnings' => array('$sum' => '$driver_revenue'),
                        'site_Earnings' => array('$sum' => '$amount_commission')
                    )
                )
            );
        }
        #echo '<pre>'; print_r($option); die;
        $cursor = $this->cimongo->aggregate(RIDES, $option);
        $res = $cursor->toArray();
        $resultArr = array('totalAmount' => 0, 'driver_Earnings' => 0, 'site_Earnings' => 0);
        if (!empty($res)) {
            if (isset($res['result'][0]['totalAmount'])) {
				if ($driver_id != '') {
					$resultArr = array('totalAmount' => $res['result'][0]['totalAmount'],
												'driver_Earnings' => $res['result'][0]['driver_Earnings'], 
												'site_Earnings' => $res['result'][0]['site_Earnings']
											);
				}
                else
                {
					$resultArr = array('totalAmount' => $res['result'][0]['driver_Earnings'] + $res['result'][0]['site_Earnings'],
												'driver_Earnings' => $res['result'][0]['driver_Earnings'], 
												'site_Earnings' => $res['result'][0]['site_Earnings']
											);
				}
            }
        }
        return $resultArr;
    }
	
	
	/**
	*
	* This function return the total amount in site
	*
	* */
	public function get_current_wallet_balance() {
		$option = array(array('$project' => array('wallet_amount' => 1,
																	'status' => 1
																)
								),
								array('$group' => array('_id' => '$email',
																	'count'=> array('$sum'=>1),
																	'totalAmount' => array('$sum'=>'$wallet_amount')
															)
								)
						);
		$cursor = $this->cimongo->aggregate(USERS, $option);
                $res = $cursor->toArray();
		$totalWallet = 0;
		if (!empty($res)) {
			if (isset($res['result'][0]['totalAmount'])) {
				$totalWallet = round($res['result'][0]['totalAmount'],2);
			}
		}
		return $totalWallet;
	}






    public function get_today_students($operator_id = '') 
    {

        if ($operator_id != '') 
        {
            $option = array(
                array(
                    '$project' => array(
                        'student_first_name' => 1,
                        'created_at' => 1,
                        'operator_id' => 1
                    )
                ),
                array(
                    '$match' => array(
                        'created_at' => array('$gte' => date('Y-m-d 00:00:00'), '$lte' => date('Y-m-d H:i:s')),
                        'operator_id' => $operator_id
                    )
                )
            );
        } 
        else 
        {
            $option = array(
                array(
                    '$project' => array(
                        'student_first_name' => 1,
                        'created_at' => 1
                    )
                ),
                array(
                    '$match' => array(
                        'created_at' => array('$gte' => date('Y-m-d 00:00:00'), '$lte' => date('Y-m-d H:i:s'))
                    )
                )
            );
        }

        $cursor = $this->cimongo->aggregate(STUDENT_DETAILS, $option);
        $res = $cursor->toArray();
        return $res;

    }





    public function get_this_month_Students($student_id = '', $operator_id = '') 
    {

        if ($student_id != '') 
        {
                    $option = array(
                array(
                    '$project' => array(
                        'student_first_name' => 1,
                        'created_at' => 1
                    )
                ),
                array(
                    '$match' => array(
                        'created_at' => array('$gte' => date('Y-m-01 00:00:00'), '$lte' => date('Y-m-d H:i:s')),
                        '_id' => new MongoDB\BSON\ObjectId($student_id)
                    )
                )
            );
            $this->cimongo->where();
        } 
        else if($operator_id != '')
        {
            $option = array(
                array(
                    '$project' => array(
                        'student_first_name' => 1,
                        'created_at' => 1,
                        'operator_id' => 1
                    )
                ),
                array(
                    '$match' => array(
                        'created_at' => array('$gte' => date('Y-m-01 00:00:00'), '$lte' => date('Y-m-d H:i:s')),
                        'operator_id' => $operator_id
                    )
                )
            );
        } 
        else 
        {
            $option = array(
                array(
                    '$project' => array(
                        'student_first_name' => 1,
                        'created_at' => 1
                    )
                ),
                array(
                    '$match' => array(
                        'created_at' => array('$gte' => date('Y-m-01 00:00:00'), '$lte' => date('Y-m-d H:i:s'))
                    )
                )
            );
        }

        $cursor = $this->cimongo->aggregate(STUDENT_DETAILS, $option);
        $res = $cursor->toArray();
        return $res;

    }



    public function get_this_year_Students($company_id = '') 
    {


      if($company_id != '')
      {
            $option = array(
                array(
                    '$project' => array(
                        'student_first_name' => 1,
                        'created_at' => 1,
                        'operator_id' => 1
                    )
                ),
                array(
                    '$match' => array(
                        'created_at' => array('$gte' => date('Y-01-01 00:00:00'), '$lte' => date('Y-m-d H:i:s')),
                        'operator_id' => $company_id
                    )
                )
            );
        } 
        else 
        {
            
            $option = array(
                array(
                    '$project' => array(
                        'student_first_name' => 1,
                        'created_at' => 1
                    )
                ),
                array(
                    '$match' => array(
                        'created_at' => array('$gte' => date('Y-m-01 00:00:00'), '$lte' => date('Y-m-d H:i:s'))
                    )
                )
            );
        }

       

        $cursor = $this->cimongo->aggregate(STUDENT_DETAILS, $option);
        $res = $cursor->toArray();
        return $res;

    }




    public function get_today_drivers($operator_id = '') {

        if ($operator_id != '') {
            $option = array(
                array(
                    '$project' => array(
                        'driver_name' => 1,
                        'created' => 1,
                        'operator_id' => 1
                    )
                ),
                array(
                    '$match' => array(
                        'created' => array('$gte' => date('Y-m-d 00:00:00'), '$lte' => date('Y-m-d H:i:s')),
                        'operator_id' => $operator_id
                    )
                )
            );
        } else {
            $option = array(
                array(
                    '$project' => array(
                        'driver_name' => 1,
                        'created' => 1
                    )
                ),
                array(
                    '$match' => array(
                        'created' => array('$gte' => date('Y-m-d 00:00:00'), '$lte' => date('Y-m-d H:i:s'))
                    )
                )
            );
        }
        $cursor = $this->cimongo->aggregate(DRIVERS, $option);
        $res=$cursor->toArray();
        return $res;
    }

    /**
     *
     * This function return the current year details
     *
     * */
    public function get_this_year_drivers($driver_id = '', $operator_id = '') {

        if ($driver_id != '') {
            $option = array(
                array(
                    '$project' => array(
                        'driver_name' => 1,
                        'created' => 1
                    )
                ),
                array(
                    '$match' => array(
                        'created' => array('$gte' => date('Y-01-01 00:00:00'), '$lte' => date('Y-m-d H:i:s')),
                        '_id' => new MongoDB\BSON\ObjectId($driver_id)
                    )
                )
            );
        } else if($operator_id != ''){
            $option = array(
                array(
                    '$project' => array(
                        'driver_name' => 1,
                        'created' => 1,
                        'operator_id' => 1
                    )
                ),
                array(
                    '$match' => array(
                        'created' => array('$gte' => date('Y-01-01 00:00:00'), '$lte' => date('Y-m-d H:i:s')),
                        'operator_id' => $operator_id
                    )
                )
            );
        } else {
            $option = array(
                array(
                    '$project' => array(
                        'driver_name' => 1,
                        'created' => 1
                    )
                ),
                array(
                    '$match' => array(
                        'created' => array('$gte' => date('Y-01-01 00:00:00'), '$lte' => date('Y-m-d H:i:s'))
                    )
                )
            );
        }
        $cursor = $this->cimongo->aggregate(DRIVERS, $option);
        $res = $cursor->toArray();
        return $res;
    }



     public function get_this_month_drivers($driver_id = '', $operator_id = '') {
        if ($driver_id != '') {
            $option = array(
                array(
                    '$project' => array(
                        'driver_name' => 1,
                        'created' => 1
                    )
                ),
                array(
                    '$match' => array(
                        'created' => array('$gte' => date('Y-m-01 00:00:00'), '$lte' => date('Y-m-d H:i:s')),
                        '_id' => new MongoDB\BSON\ObjectId($driver_id)
                    )
                )
            );
            $this->cimongo->where();
        } else if($operator_id != ''){
            $option = array(
                array(
                    '$project' => array(
                        'driver_name' => 1,
                        'created' => 1,
                        'operator_id' => 1
                    )
                ),
                array(
                    '$match' => array(
                        'created' => array('$gte' => date('Y-m-01 00:00:00'), '$lte' => date('Y-m-d H:i:s')),
                        'operator_id' => $operator_id
                    )
                )
            );
        } else {
            $option = array(
                array(
                    '$project' => array(
                        'driver_name' => 1,
                        'created' => 1
                    )
                ),
                array(
                    '$match' => array(
                        'created' => array('$gte' => date('Y-m-01 00:00:00'), '$lte' => date('Y-m-d H:i:s'))
                    )
                )
            );
        }
        $cursor = $this->cimongo->aggregate(DRIVERS, $option);
        $res = $cursor->toArray();
        return $res;
    }



    public function get_today_users($phone_number = '') 
    {

        if ($phone_number != '') 
        {
            $option = array(
                array(
                    '$project' => array(
                        'user_name' => 1,
                        'created' => 1,
                        'phone_number' => 1
                    )
                ),
                array(
                    '$match' => array(
                        'created' => array('$gte' => date('Y-m-d 00:00:00'), '$lte' => date('Y-m-d H:i:s')),
                        'phone_number' => array('$in' => $phone_number)  
                    )
                )
            );
        } 
        else 
        {
            $option = array(
                array(
                    '$project' => array(
                        'user_name' => 1,
                        'created' => 1
                    )
                ),
                array(
                    '$match' => array(
                        'created' => array('$gte' => date('Y-m-d 00:00:00'), '$lte' => date('Y-m-d H:i:s'))
                    )
                )
            );
        }
      

        $cursor = $this->cimongo->aggregate(USERS, $option);
        $res = $cursor->toArray();
        return $res;
    }




    public function get_this_month_Users($phone_number = '') 
    {

        if($phone_number != '')
        {
            $option = array(
                array(
                    '$project' => array(
                        'user_name' => 1,
                        'created' => 1,
                        'phone_number' => 1
                    )
                ),
                array(
                    '$match' => array(
                        'created' => array('$gte' => date('Y-m-01 00:00:00'), '$lte' => date('Y-m-d H:i:s')),
                        'phone_number' => array('$in' => $phone_number)  
                    )
                )
            );
        } 
        else 
        {
            $option = array(
                array(
                    '$project' => array(
                        'user_name' => 1,
                        'created' => 1
                    )
                ),
                array(
                    '$match' => array(
                        'created' => array('$gte' => date('Y-m-01 00:00:00'), '$lte' => date('Y-m-d H:i:s'))
                    )
                )
            );
        }

        $cursor = $this->cimongo->aggregate(USERS, $option);
        $res = $cursor->toArray();
        return $res;

    }


    public function get_this_year_Users($phone_number = '') 
    {

        if($phone_number != '')
        {
            $option = array(
                array(
                    '$project' => array(
                        'user_name' => 1,
                        'created' => 1,
                        'phone_number' => 1
                    )
                ),
                array(
                    '$match' => array(
                        'created' => array('$gte' => date('Y-01-01 00:00:00'), '$lte' => date('Y-m-d H:i:s')),
                        'phone_number' => array('$in' => $phone_number)  
                    )
                )
            );
        } 
        else 
        {
            $option = array(
                array(
                    '$project' => array(
                        'user_name' => 1,
                        'created' => 1
                    )
                ),
                array(
                    '$match' => array(
                        'created' => array('$gte' => date('Y-01-01 00:00:00'), '$lte' => date('Y-m-d H:i:s'))
                    )
                )
            );
        }

        $cursor = $this->cimongo->aggregate(USERS, $option);
        $res = $cursor->toArray();
        return $res;

    }




//////////////////////////online vechicles



public function current_trip_details()
{
 



                    $option=array(

                                        array(
                                            '$lookup' => array(
                                                "from" => BUS_TRIP,
                                                "localField" => "trip_id",
                                                "foreignField" => "_id",
                                                "as" => "trip_details"
                                            )
                                        ),
                                        
                                          array( '$match' => array(
                                          "operator_id" => $this->session->userdata(APP_NAME.'_session_operator_id'),
                                          "trip_action"=> "start",
                                          "start_current_date"  => array('$eq' => date('Y-m-d') )

                                          
                                          
                                          )                                   
                                         
                                          )
                                         
                                    );
                    $res=$this->cimongo->aggregate(CURRENT_TRIP_DETAILS,$option);
                    $current_trip_details=$res->toArray();

                     
                   return sizeof($current_trip_details);



}








//////////////////////////delayed vechicles



public function delayed_trip_details()
{
 



                    $option=array(

                                        array(
                                            '$lookup' => array(
                                                "from" => BUS_TRIP,
                                                "localField" => "trip_id",
                                                "foreignField" => "_id",
                                                "as" => "trip_details"
                                            ),
                                             '$lookup' => array(
                                                "from" => DRIVER_NOTIFY,
                                                "localField" => "trip_id",
                                                "foreignField" => "trip_id",
                                                "as" => "notify_details"
                                            )

                                        ),
                                        
                                          array( '$match' => array(
                                          "operator_id" => $this->session->userdata(APP_NAME.'_session_operator_id'),
                                          "trip_action"=> "start",
                                          "start_current_date"  => array('$eq' => date('Y-m-d') )

                                          )                                   
                                         
                                          )
                                         
                                    );
$res=$this->cimongo->aggregate(CURRENT_TRIP_DETAILS,$option);
                    $current_trip_details=$res;

                   
                   return $current_trip_details;



}



/////////////////present_childrem





public function students_present()
{
 
            $operator_id = $this->session->userdata(APP_NAME.'_session_operator_id');

            $current_trip_details = $this->app_model->get_selected_fields(CURRENT_TRIP_DETAILS,array('operator_id' => new MongoDB\BSON\ObjectId($operator_id), "trip_action"=> "start","start_current_date"  => array('$eq' => date('Y-m-d') )),array('trip_id'))->result();

            

            $present_count=0;

            $absent_count=0;

            if(!empty($current_trip_details))
            {
                $or_data=array();
                foreach ($current_trip_details as $key => $value) {

                      $a=array('student_pickup_trip_id' => $value->trip_id );
                      $b=array('student_drop_trip_id' => $value->trip_id );
                         array_push($or_data,$a,$b);
                    
                }


               

                $counter=$this->app_model->get_selected_fields(STUDENT_DETAILS,array( '$or' => $or_data  ),array('_id','student_att_status','student_drop_trip_id'))->result();

               
                foreach ($counter as $key => $value) {

                    if(is_object($value->student_att_status))
                $value->{"student_att_status"}=(array)$value->student_att_status;
                        if($value->student_att_status['from_date']->toDateTime()->getTimestamp() <= strtotime(date("Y-m-d")) && $value->student_att_status['to_date']->toDateTime()->getTimestamp() >= strtotime(date("Y-m-d")) && $value->student_att_status['status'] == 'absent')
                        {

                                    

                                     if($value->student_att_status['absent_half_availability'] == "pickup" && $value->student_att_status['absent_type'] == "half" && (string)$value->student_drop_trip_id !="" )
                                        {
                                            $abs_cheker=0;


                                                    foreach ($current_trip_details as $key => $value2) 
                                                    {
                                                       

                                                            if((string)$value2->trip_id == (string)$value->student_drop_trip_id  )
                                                            {
                                                                $absent_count++;
                                                                 $abs_cheker=1;
                                                            }
                                                        

                                                    } 

                                                    if( $abs_cheker==0)
                                                     {   
                                                     $present_count++;
                                                     }   
                                        }
                                        else{
                                            $absent_count++;
                                        }    
                                      
                        }
                        else
                        {
                             $present_count++;
                        }    


                }    





            }



            $resturn_data=array( $present_count,$absent_count);

            


            return $resturn_data;



}


/////////////////driver notifications

public function driver_notifications()
{
             $option=array(

                                        array(
                                            '$lookup' => array(
                                                "from" => BUS_TRIP,
                                                "localField" => "trip_id",
                                                "foreignField" => "_id",
                                                "as" => "trip_details"
                                            )

                                        ),
                                        
                                          array( '$match' => array(
                                         
                                          "trip_details.operator_id" => $this->session->userdata(APP_NAME.'_session_operator_id'),

                                             "created_at"  => array('$eq' => date('Y-m-d') )

                                          )                                   
                                         
                                          )
                                         
                                    );
$res=$this->cimongo->aggregate(DRIVER_NOTIFY,$option);
                    $current_trip_details=$res->toArray();

              
    

                   return sizeof($current_trip_details);
}    



/////////////////bus breakdown

public function bus_breakdown()
{
   
             $option=array(

                                        array(
                                            '$lookup' => array(
                                                "from" => CURRENT_TRIP_DETAILS,
                                                "localField" => "trip_id",
                                                "foreignField" => "trip_id",
                                                "as" => "current_trip_details"
                                            )

                                        ),
                                        
                                          array( '$match' => array(
                                         
                                          "current_trip_details.operator_id" => $this->session->userdata(APP_NAME.'_session_operator_id'),
                                              "delay_message" =>   "BreakDown",
                                               "current_trip_details.trip_action" => "stop",
                                            "current_trip_details.start_current_date"   => array('$eq' => date('Y-m-d') )

                                          )                                   
                                         
                                          )
                                         
                                    );
$dres=$this->cimongo->aggregate(DRIVER_NOTIFY,$option);
                    $delayed_trip_details=$dres->toArray();

                   

                    $delayed_trip_ids=array();
                    $repeater_stopper=array();
                    foreach ( array_reverse($delayed_trip_details) as $key => $value) {

                        if(!in_array(  $value['trip_id'] , $repeater_stopper ))
                        {   

                        array_push($delayed_trip_ids, $value['_id']);

                        array_push($repeater_stopper, $value['trip_id']);

                        }

                    }


                     
                
                $tripCount = $this->app_model->get_all_counts(DRIVER_NOTIFY,array("_id" =>  array('$in' =>  $delayed_trip_ids )));

              

                   return $tripCount;
}   


/////////////////dealyed bus

public function delayed_bus()
{
   
             $option=array(

                                        array(
                                            '$lookup' => array(
                                                "from" => BUS_TRIP,
                                                "localField" => "trip_id",
                                                "foreignField" => "_id",
                                                "as" => "trip_details"
                                            )

                                        ),
                                        
                                          array( '$match' => array(
                                         
                                          "trip_details.operator_id" => $this->session->userdata(APP_NAME.'_session_operator_id'),
                                              "delay_message" =>  array( '$ne' => "BreakDown"),
                                              
                                             "created_at"  => array('$eq' => date('Y-m-d') )

                                          )                                   
                                            
                                          )
                                         
                                         
                                    );

                    //$current_trip_details=$this->cimongo->aggregate(DRIVER_NOTIFY,$option)["result"];
$dres=$this->cimongo->aggregate(DRIVER_NOTIFY,$option);
                    $delayed_trip_details=$dres->toArray();


                    $delayed_trip_ids=array();
                    foreach ( $delayed_trip_details as $key => $value) {

                       
                        array_push($delayed_trip_ids, $value['trip_id']);


                    }


                     
                
                $tripCount = $this->app_model->get_all_counts(CURRENT_TRIP_DETAILS,array("operator_id"=> $this->session->userdata(APP_NAME.'_session_operator_id'),"trip_action"=> "start","start_current_date"  => array('$eq' => date('Y-m-d')  ),"trip_id" =>  array('$in' =>  $delayed_trip_ids )));

                   return $tripCount;
}   

    
    

/////////////////parent_notifications bus

public function parent_notifications()
{
   
            
                    
            $operator_id = $this->session->userdata(APP_NAME.'_session_operator_id');

            $current_trip_details = $this->app_model->get_selected_fields(PARENT_NOTIFY,array('operator_id' => new MongoDB\BSON\ObjectId($operator_id), "created_date"=> date('Y-m-d')),array('trip_id'))->result();

   

                   return sizeof($current_trip_details);
}   



}