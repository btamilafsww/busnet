<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 /**
* 
* This model contains all db functions related to operators management
* @author Katenterprise
*
**/

class Operators_model extends My_Model{

		public function __construct(){
				parent::__construct();
		}
		
		/**
		* 
		* This functions selects rides list 
		**/
		public function get_rides_total($operators_id = ''){
				$this->cimongo->select('*');
				if($operators_id != ''){
					$where_clause['booked_by'] = $operators_id;
				}
				$this->cimongo->where($where_clause, TRUE);
				$res = $this->cimongo->get(RIDES);
				return $res;
		}
		
		/**
		* 
		* This functions selects rides list 
		**/
		public function get_operators_rides_total($operators_id = '', $action = ''){
				$this->cimongo->select('*');
							
				if($action != ''){
					if($action == 'Onride'){ 
							$where_clause = array(
									'$or' => array(
											array("ride_status" => 'Onride'),
											array("ride_status" => 'Confirmed'),
											array("ride_status" => 'Arrived'),
											array("ride_status" => 'Finished'),
									)
							);
					} else if ($action == 'riderCancelled') {
							$where_clause = array('ride_status' => 'Cancelled', 'cancelled.primary.by' => 'User');
					} else if ($action == 'driverCancelled') {
							$where_clause = array('ride_status' => 'Cancelled', 'cancelled.primary.by' => 'Driver');
					} else {
						$where_clause['ride_status'] = $action;
					}
				}
				
				if($operators_id != ''){
					$where_clause['booked_by'] = $operators_id;
				}
				
				$this->cimongo->where($where_clause, TRUE);
				$res = $this->cimongo->get(RIDES);
				return $res;
		}
		
		/**
		* 
		* This functions selects rides list 
		**/
		public function get_rides_list($operators_id='', $limit=FALSE,$offset=FALSE){
				$this->cimongo->select('*');		
				if($operators_id != ''){
						$where_clause['booked_by'] = $operators_id;
				}		
				$this->cimongo->where($where_clause, TRUE);
				$this->cimongo->order_by(array('ride_id' => -1));
				if ($limit !== FALSE && is_numeric($limit) && $offset !== FALSE && is_numeric($offset)) {
						$res = $this->cimongo->get(RIDES,$limit,$offset);
				}else{
						$res = $this->cimongo->get(RIDES);  
				}
				
				return $res;
		}
		
		/**
		* 
		* This functions selects rides list 
		**/
		public function get_operators_rides($operators_id='', $limit=FALSE,$offset=FALSE, $action = ''){
				$this->cimongo->select('*');		
					
				if($action != ''){
						if($action == 'Onride'){ 
								$where_clause = array(
										'$or' => array(
												array("ride_status" => 'Onride'),
												array("ride_status" => 'Confirmed'),
												array("ride_status" => 'Arrived'),
												array("ride_status" => 'Finished'),
										)
								);
						} else if ($action == 'riderCancelled') {
								$where_clause = array('ride_status' => 'Cancelled', 'cancelled.primary.by' => 'User');
						} else if ($action == 'driverCancelled') {
								$where_clause = array('ride_status' => 'Cancelled', 'cancelled.primary.by' => 'Driver');
						} else {
							$where_clause['ride_status'] = $action;
						}
				}
				if($operators_id != ''){
						$where_clause['booked_by'] = $operators_id;
				}
				$this->cimongo->where($where_clause, TRUE);
				$this->cimongo->order_by(array('ride_id' => -1));
			  if ($limit !== FALSE && is_numeric($limit) && $offset !== FALSE && is_numeric($offset)) {
						$res = $this->cimongo->get(RIDES,$limit,$offset);
				}else{
						$res = $this->cimongo->get(RIDES);  
				}
				return $res;
		}
	
}

?>