<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * 
 * This model contains all db functions related to map management
 * @author Katenterprise
 *
 * */
class Map_model extends My_Model {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Check driver exist or not
     * */
    public function get_nearest_driver($coordinates = array(), $operator_id = '') {






        if ($operator_id != '') {
            $matchCond = array(
                //'availability' =>array('$eq'=>'Yes'),
                'status' => array('$eq' => 'Active'),
                'operator_id' => new MongoDB\BSON\ObjectId($operator_id)
            );
        } else {
            $matchCond = array(
                //'availability' =>array('$eq'=>'Yes'),
                'status' => array('$eq' => 'Active')
            );
        }

        $option = array(
            array(
                '$geoNear' => array("near" => array("type" => "Point",
                        "coordinates" => $coordinates
                    ),
                    "spherical" => true,
                    //"maxDistance"=>50000,
                    "includeLocs" => 'loc',
                    "distanceField" => "distance",
                    "distanceMultiplier" => 0.001
                ),
            ),
            array(
                '$project' => array(
                    'category' => 1,
                    'driver_name' => 1,
                    'loc' => 1,
                    'availability' => 1,
                    'status' => 1,
                    'distance' => 1,
                    'mode' => 1,
                    'last_active_time' => 1,
                    'operator_id' => 1,
                    'driver_location' => 1
                )
            ),
            array(
                '$match' => $matchCond
            )
        );


        $res = $this->cimongo->aggregate(DRIVERS, $option);


        return $res;
    }

    public function get_nearest_driver_company($coordinates = array(), $company_id = '') {
        /* $option=array('$geoNear'=>array("near"=>array("type"=>"Point",
          "coordinates"=>$coordinates
          ),
          "spherical"=> true,
          "maxDistance"=>5000000,
          "includeLocs"=>'loc',
          "distanceField"=>"distance",
          "distanceMultiplier"=>0.001,
          'num' => 10
          )); */



        $matchCond = array(
            'company_id' => array('$eq' => $company_id),
            'status' => array('$eq' => 'Active')
        );


        $option = array(
            array(
                '$geoNear' => array("near" => array("type" => "Point",
                        "coordinates" => $coordinates
                    ),
                    "spherical" => true,
                    //"maxDistance"=>50000,
                    "includeLocs" => 'loc',
                    "distanceField" => "distance",
                    "distanceMultiplier" => 0.001,
                    "num" => 100000
                ),
            ),
            array(
                '$project' => array(
                    'category' => 1,
                    'driver_name' => 1,
                    'loc' => 1,
                    'availability' => 1,
                    'status' => 1,
                    'distance' => 1,
                    'mode' => 1,
                    'last_active_time' => 1,
                    'operator_id' => 1,
                    'driver_location' => 1,
                    'company_id' => 1
                )
            ),
            array(
                '$match' => $matchCond
            )
        );



        $res = $this->cimongo->aggregate(DRIVERS, $option);
        return $res;
    }

    public function get_nearest_user($coordinates = array(), $user_list_id = array()) {



        $option = array(
            array(
                '$geoNear' => array("near" => array("type" => "Point",
                        "coordinates" => $coordinates
                    ),
                    "spherical" => true,
                    //"maxDistance"=>50000,
                    "includeLocs" => 'loc',
                    "distanceField" => "distance",
                    "distanceMultiplier" => 0.001,
                    "num" => 100000
                ),
            ),
            array(
                '$project' => array(
                    'user_name' => 1,
                    'loc' => 1,
                    'status' => 1,
                    'email' => 1,
                    'phone_number' => 1,
                    'distance' => 1
                )
            ),
            array(
                '$match' => array(
                    '_id' => array('$in' => $user_list_id)
                )
            )
        );
        $res = $this->cimongo->aggregate(USERS, $option);


        return $res;
    }

    public function get_stop_distance($coordinates_get = array(), $driver_id) {






        $matchCond = array(
            '_id' => new MongoDB\BSON\ObjectId($driver_id)
        );



        $option = array(
            array(
                '$geoNear' => array("near" => array("type" => "Point",
                        "coordinates" => [(float) $coordinates_get[1], (float) $coordinates_get[0]]
                    ),
                    "spherical" => true,
                    //"maxDistance"=>50000,
                    "includeLocs" => 'loc',
                    "distanceField" => "distance",
                    "num" => 100000000
                ),
            ),
            array(
                '$project' => array(
                    'category' => 1,
                    'driver_name' => 1,
                    'loc' => 1,
                    'availability' => 1,
                    'status' => 1,
                    'distance' => 1,
                    'mode' => 1,
                    'last_active_time' => 1,
                    'operator_id' => 1,
                    'driver_location' => 1,
                    'company_id' => 1
                )
            ),
            array(
                '$match' => $matchCond
            )
        );



        $res = $this->cimongo->aggregate(DRIVERS, $option);
        return $res;
    }

}

?>