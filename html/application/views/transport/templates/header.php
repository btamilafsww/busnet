<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta name="viewport" content="width=device-width"/>
<base href="<?php echo base_url(); ?>">
<title><?php echo $heading.' - '.$title;?></title>
<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url();?>images/logo/<?php echo $favicon;?>">
<link href="css/reset.css" rel="stylesheet" type="text/css" media="screen">
<link href="css/font-awesome.css" rel="stylesheet" type="text/css">
<link href="css/layout.css" rel="stylesheet" type="text/css" media="screen">
<link href="css/themes.css" rel="stylesheet" type="text/css" media="screen">
<link href="css/typography.css" rel="stylesheet" type="text/css" media="screen">
<link href="css/styles.css" rel="stylesheet" type="text/css" media="screen">
<link href="css/rating.css" rel="stylesheet" type="text/css" media="screen">
<link href="css/themes-changes.css" rel="stylesheet" type="text/css" media="screen">

<!-- <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="screen"> -->
<link href="css/jquery.jqplot.css" rel="stylesheet" type="text/css" media="screen">
<link href="css/jquery-ui-1.8.18.custom.css" rel="stylesheet" type="text/css" media="screen">
<link href="css/data-table.css" rel="stylesheet" type="text/css" media="screen">
<link href="css/form.css" rel="stylesheet" type="text/css" media="screen">

<link href="css/ui-elements.css" rel="stylesheet" type="text/css" media="screen">
<link href="css/wizard.css" rel="stylesheet" type="text/css">
<link href="css/sprite.css" rel="stylesheet" type="text/css" media="screen">
<link href="css/gradient.css" rel="stylesheet" type="text/css" media="screen">
<link href="css/developer.css" rel="stylesheet" type="text/css">
<link href="css/developer_colors.css" rel="stylesheet" type="text/css">
<link href="css/custom-dev-css.css" rel="stylesheet" type="text/css">

<link href="css/route_map.css" rel="stylesheet" type="text/css">

<link rel="stylesheet" type="text/css" media="all" href="plugins/daterangepicker/css/glyphicons.css" />



<?php
$currentUrl = $this->uri->segment(2, 0);

if($currentUrl != "notification")
{
?>	

<!-- boostrap -->
 <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">

 <link rel="stylesheet" href="bootstrap/css/bootstrap-datepicker.min.css">


 <link rel="stylesheet" href="css/bootstrap-datetimepicker.css">

<link rel="stylesheet" href="bootstrap/calender/bootstrap-year-calendar.min.css" >

<?php } ?>





<!--<link rel="stylesheet" type="text/css" href="css/ie/ie7.css" />
<link rel="stylesheet" type="text/css" href="css/ie/ie8.css" />
<link rel="stylesheet" type="text/css" href="css/ie/ie9.css" />-->
<script type="text/javascript">
		var BaseURL = '<?php echo base_url();?>';
		var baseURL = '<?php echo base_url();?>';

</script>
<?php
		
	$this->load->view('site/templates/validation_script');
		
?>
<script src="js/jquery-1.7.1.min.js"></script>
<script src="js/jquery-1.12.4.js"></script>
<script src="js/jquery-migrate-1.4.1.js"></script>



<script src="js/jquery-ui-1.8.18.custom.min.js"></script>
<script src="js/jquery.ui.touch-punch.js"></script>
<script src="js/chosen.jquery.js"></script>
<script src="js/uniform.jquery.js"></script>
<script src="js/bootstrap-dropdown.js"></script>
<script src="js/bootstrap-colorpicker.js"></script>
<script src="js/sticky.full.js"></script>
<script src="js/jquery.noty.js"></script>
<script src="js/selectToUISlider.jQuery.js"></script>
<script src="js/fg.menu.js"></script>
<script src="js/jquery.tagsinput.js"></script>
<script src="js/jquery.cleditor.js"></script>
<script src="js/jquery.tipsy.js"></script>
<script src="js/jquery.peity.js"></script>
<script src="js/jquery.simplemodal.js"></script>
<script src="js/jquery.jBreadCrumb.1.1.js"></script>
<script src="js/jquery.colorbox-min.js"></script>
<script src="js/jquery.idTabs.min.js"></script>
<script src="js/jquery.multiFieldExtender.min.js"></script>
<script src="js/jquery.confirm.js"></script>
<script src="js/elfinder.min.js"></script>
<script src="js/accordion.jquery.js"></script>
<script src="js/autogrow.jquery.js"></script>
<script src="js/check-all.jquery.js"></script>
<script src="js/data-table.jquery.js"></script>
<script src="js/ZeroClipboard.js"></script>
<script src="js/TableTools.min.js"></script>
<script src="js/jeditable.jquery.js"></script>
<script src="js/ColVis.min.js"></script>
<script src="js/duallist.jquery.js"></script>
<script src="js/easing.jquery.js"></script>
<script src="js/full-calendar.jquery.js"></script>
<script src="js/input-limiter.jquery.js"></script>
<script src="js/inputmask.jquery.js"></script>
<script src="js/iphone-style-checkbox.jquery.js"></script>
<script src="js/meta-data.jquery.js"></script>
<script src="js/quicksand.jquery.js"></script>
<script src="js/raty.jquery.js"></script>
<script src="js/smart-wizard.jquery.js"></script>
<script src="js/stepy.jquery.js"></script>
<script src="js/treeview.jquery.js"></script>
<script src="js/ui-accordion.jquery.js"></script> 
<script src="js/vaidation.jquery.js"></script>
<script src="js/mosaic.1.0.1.min.js"></script>
<script src="js/jquery.collapse.js"></script>
<script src="js/jquery.cookie.js"></script>
<script src="js/jquery.autocomplete.min.js"></script>
<script src="js/localdata.js"></script>
<script src="js/excanvas.min.js"></script>
<script src="js/jquery.jqplot.min.js"></script>
<script src="js/chart-plugins/jqplot.dateAxisRenderer.min.js"></script>
<script src="js/chart-plugins/jqplot.cursor.min.js"></script>
<script src="js/chart-plugins/jqplot.logAxisRenderer.min.js"></script>
<script src="js/chart-plugins/jqplot.canvasTextRenderer.min.js"></script>
<script src="js/chart-plugins/jqplot.canvasAxisTickRenderer.min.js"></script>
<script src="js/chart-plugins/jqplot.highlighter.min.js"></script>
<script src="js/chart-plugins/jqplot.pieRenderer.min.js"></script>
<script src="js/chart-plugins/jqplot.barRenderer.min.js"></script>
<script src="js/chart-plugins/jqplot.categoryAxisRenderer.min.js"></script>
<script src="js/chart-plugins/jqplot.pointLabels.min.js"></script>
<script src="js/chart-plugins/jqplot.meterGaugeRenderer.min.js"></script>
<script src="js/jquery.MultiFile.js"></script>
<script src="js/validation.js"></script>







<!-- bootstrap -->
<?php


if($currentUrl != "notification")
{
?>	

  <script src="bootstrap/js/bootstrap.min.js"></script>
  <script src="bootstrap/js/bootstrap-datepicker.min.js"></script>
   <script src="bootstrap/js/popper.min.js"></script>
 <script src="js/moment.min.js"></script>
  <script src="js/bootstrap-datetimepicker.min.js"></script>



<?php } ?>







<script src="js/custom-scripts.js"></script>
<script src="js/jquery-input-file-text.js"></script>
<script type="text/javascript" src="js/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
tinyMCE.init({
// General options
mode : "specific_textareas",
editor_selector : "mceEditor",
theme : "advanced",
plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
 
// Theme options
theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
theme_advanced_toolbar_location : "top",
theme_advanced_toolbar_align : "left",
theme_advanced_statusbar_location : "bottom",
theme_advanced_resizing : true,
file_browser_callback : "ajaxfilemanager",
relative_urls : false,
convert_urls: false,
// Example content CSS (should be your site CSS)
content_css : "css/example.css",
 
// Drop lists for link/image/media/template dialogs
//template_external_list_url : "js/template_list.js",
external_link_list_url : "js/link_list.js",
external_image_list_url : "js/image_list.js",
media_external_list_url : "js/media_list.js",
 
// Replace values for the template plugin
template_replace_values : {
username : "Some User",
staffid : "991234"
}
});

function ajaxfilemanager(field_name, url, type, win) {
		var ajaxfilemanagerurl = '<?php echo base_url();?>js/tinymce/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php';
		switch (type) {
				case "image":
						break;
				case "media":
						break;
				case "flash": 
						break;
				case "file":
						break;
				default:
						return false;
		}
		tinyMCE.activeEditor.windowManager.open({
				url: '<?php echo base_url();?>js/tinymce/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php',
				width: 782,
				height: 440,
				inline : "yes",
				close_previous : "no"
		},{
				window : win,
				input : field_name
		});
					
		return false;			
		var fileBrowserWindow = new Array();
		fileBrowserWindow["file"] = ajaxfilemanagerurl;
		fileBrowserWindow["title"] = "Ajax File Manager";
		fileBrowserWindow["width"] = "782";
		fileBrowserWindow["height"] = "440";
		fileBrowserWindow["close_previous"] = "no";
		tinyMCE.openWindow(fileBrowserWindow, {
				window : win,
				input : field_name,
				resizable : "yes",
				inline : "yes",
				editor_id : tinyMCE.getWindowArg("editor_id")
		});
		
		return false;
}
</script>
<script type="text/javascript">
function hideErrDiv(arg) {
		document.getElementById(arg).style.display = 'none';
}
</script>
<?php $checkbox_lan=get_language_array_for_keyword($this->data['langCode']);?>
<script>

$(function () {
$('.on_off :checkbox').iphoneStyle();
$('.yes_no :checkbox').iphoneStyle({checkedLabel:'<?php echo $checkbox_lan['verify_status_yes_ucfirst']; ?>', uncheckedLabel: '<?php echo $checkbox_lan['verify_status_no_ucfirst']; ?>'});
$('.flat_percentage :checkbox').iphoneStyle({checkedLabel: '<?php echo $checkbox_lan['coupon_code_flat']; ?>', uncheckedLabel: '<?php echo $checkbox_lan['coupon_code_percent']; ?>'});
$('.active_inactive :checkbox').iphoneStyle({checkedLabel: '<?php echo $checkbox_lan['status_active_ucfirst']; ?>', uncheckedLabel: '<?php echo $checkbox_lan['status_inactive_ucfirst']; ?>'});
$('.publish_unpublish :checkbox').iphoneStyle({checkedLabel: '<?php echo $checkbox_lan['status_publish_ucfirst']; ?>', uncheckedLabel: '<?php echo $checkbox_lan['status_unpublish_ucfirst']; ?>'});
$('.live_sandbox :checkbox').iphoneStyle({checkedLabel: '<?php echo $checkbox_lan['checkbox_live']; ?>', uncheckedLabel: '<?php echo $checkbox_lan['checkbox_sandbox']; ?>'});
$('.disabled :checkbox').iphoneStyle();
$('.ac_nonac :checkbox').iphoneStyle({checkedLabel:'<?php echo $checkbox_lan['checkbox_ac']; ?>', uncheckedLabel: '<?php echo $checkbox_lan['checkbox_non_ac']; ?>'});
$('.cod_on_off :checkbox').iphoneStyle({checkedLabel: '<?php echo $checkbox_lan['status_enable_ucfirst']; ?>', uncheckedLabel: '<?php echo $checkbox_lan['status_disable_ucfirst']; ?>'});
$('.prod_dev :checkbox').iphoneStyle({ checkedLabel: '<?php echo $checkbox_lan['checkbox_production']; ?>', uncheckedLabel: '<?php echo $checkbox_lan['checkbox_development']; ?>' });
});
</script>
</head>
<body id="theme-default" class="full_block">
<?php  $this->load->view(TRANSPORT_NAME.'/templates/sidebar.php'); ?>
<?php
$currentUrl = $this->uri->segment(2,0); 
$currentPage = $this->uri->segment(3,0);
if($currentUrl==''){
		$currentUrl = 'dashboard';
} 
if($currentPage==''){
		$currentPage = 'dashboard';
}
$current_url = $_SERVER['REQUEST_URI'];
?>

 <?php  
$this->load->view('site/templates/datetime_lang_script');
?>

<div id="container">
<div id="header">
<div class="header_left">

<!-- mobile header-->

<div id="responsive_mnu">
<a href="#responsive_menu" class="fg-button" id="hierarchybreadcrumb"><span class="responsive_icon"></span><?php if ($this->lang->line('admin_menu_menu') != '') echo stripslashes($this->lang->line('admin_menu_menu')); else echo 'Menu'; ?></a>
<div id="responsive_menu" class="hidden">
<ul>
<li>
						<a href="<?php echo base_url().TRANSPORT_NAME; ?>/dashboard/display_dashboard" <?php if ($currentUrl == 'dashboard') { echo 'class="active"'; } ?>>
							<span class="nav_icon computer_imac"></span> 
							<?php if ($this->lang->line('admin_menu_dashboard') != '') echo stripslashes($this->lang->line('admin_menu_dashboard')); else echo 'Dashboard'; ?>
						</a>
					</li>
					












			<?php 
					if($operator_type == "main")
					{
			?>
						<li>
							<a href="<?php echo $current_url; ?>" <?php if ($currentUrl == 'settings') { echo 'class="active"'; } ?>>
								<span class="nav_icon settings_2"></span><?php if ($this->lang->line('admin_menu_settings') != '') echo stripslashes($this->lang->line('admin_menu_settings')); else echo 'Settings'; ?><span class="up_down_arrow">&nbsp;</span>
							</a>
							<ul class="acitem" <?php if ($currentUrl == 'settings') { echo 'style="display: block;"'; } else { echo 'style="display: none;"'; } ?>>
								<li>
									<a href="<?php echo TRANSPORT_NAME; ?>/settings/edit_profile_form" <?php if ($currentPage == 'edit_profile_form') { echo 'class="active"';}?>>
										<span class="list-icon">&nbsp;</span><?php if ($this->lang->line('dash_operator_profile_settings') != '') echo stripslashes($this->lang->line('dash_operator_profile_settings')); else echo 'Profile Settings'; ?>
									</a>
								</li>
								<li>
									<a href="<?php echo TRANSPORT_NAME; ?>/settings/change_password" <?php if ($currentPage == 'change_password') { echo 'class="active"'; } ?>>
										<span class="list-icon">&nbsp;</span><?php if ($this->lang->line('admin_menu_change_password') != '') echo stripslashes($this->lang->line('admin_menu_change_password')); else echo 'Change Password'; ?>
									</a>
								</li>

								<li>
									<a href="<?php echo TRANSPORT_NAME; ?>/settings/yearly_time_setting" <?php if ($currentPage == 'yearly_time_setting') { echo 'class="active"'; } ?>>
										<span class="list-icon">&nbsp;</span><?php if ($this->lang->line('yearly_time_setting') != '') echo stripslashes($this->lang->line('yearly_time_setting')); else echo 'Time Line'; ?>
									</a>
								</li>

							</ul>
						</li>				

			<?php
					}
			?>


			

			 <?php 
                         if(is_object($schooladminPrevs))
                             $schooladminPrevs=(array)$schooladminPrevs;
                 	if(isset($schooladminPrevs["school_management_details"]))
					{ 
			?>	                    
	                    <li>
							<a href="<?php echo $current_url; ?>" <?php if ($currentUrl == 'add_new_class' || $currentUrl == 'add_students' || $currentUrl == 'class_details' ) { echo 'class="active"'; }?>>
								<span class="nav_icon" style="background: unset;"><img style="width:100%;    vertical-align: unset;" src="images/sprite-icons/Chalkboard.png"></span> <?php if ($this->lang->line('school_management_menu') != '') echo stripslashes($this->lang->line('school_management_menu')); else echo 'School Management'; ?><span class="up_down_arrow">&nbsp;</span>
							</a>

							<ul class="acitem" <?php if ($currentUrl == 'school_management_details' || $currentUrl == 'add_new_class' || $currentUrl == 'add_students' || $currentUrl == 'class_details') { echo 'style="display: block;"'; } else { echo 'style="display: none;"'; } ?>>
							
							<?php  
								if(in_array("add_new_class", $schooladminPrevs["school_management_details"]))
								{ 
							?>	
									<li>
										<a href="<?php echo TRANSPORT_NAME; ?>/class_details/add_new_class" <?php if ($currentPage == 'add_new_class') { echo 'class="active"'; } ?>>
											<span class="list-icon">&nbsp;</span><?php if ($this->lang->line('add_class') != '') echo stripslashes($this->lang->line('add_class')); else echo 'Add Class';?>
										</a>
									</li>
							<?php 
								} 
							?>


							<?php  
								 if(in_array("add_studentdetails", $schooladminPrevs["school_management_details"]))
								{ 
							?>
									<li>
										<a href="<?php echo TRANSPORT_NAME; ?>/add_students/add_studentdetails" <?php if ($currentPage == 'add_studentdetails') { echo 'class="active"'; } ?>>
											<span class="list-icon">&nbsp;</span><?php if ($this->lang->line('student_add') != '') echo stripslashes($this->lang->line('student_add')); else echo 'Add Student';?>
										</a>
									</li>
	                       <?php 
	                       		} 
	                       	?>	

	                       	<?php  
								 if(in_array("view_student_details", $schooladminPrevs["school_management_details"]) || in_array("edit_student_detail", $schooladminPrevs["add_students"]))
								{ 
							?>
									<li>
										<a href="<?php echo TRANSPORT_NAME; ?>/add_students/student_lists" <?php if ($currentPage == 'student_lists' || $currentPage =='view_student_details') { echo 'class="active"'; } ?>>
											<span class="list-icon">&nbsp;</span><?php if ($this->lang->line('student_list') != '') echo stripslashes($this->lang->line('student_list')); else echo 'Student List';?>
										</a>
									</li>
	                       <?php
	                       		} 
	                       	?>	   



							</ul>
						</li>
                    
            <?php 
               		} 
            ?>



			<?php 
					if($operator_type == "main")
					{
			?>
						 <li>
							<a href="<?php echo $current_url; ?>" <?php if ($currentUrl == 'other_users') { echo 'class="active"'; } ?>>
								<span class="nav_icon admin_user"></span><?php if ($this->lang->line('user_management') != '') echo stripslashes($this->lang->line('user_management')); else echo 'User Management'; ?><span class="up_down_arrow">&nbsp;</span>
							</a>
							<ul class="acitem" <?php if ($currentUrl == 'other_users') { echo 'style="display: block;"'; } else { echo 'style="display: none;"'; } ?>>
							
								<li>
									<a href="<?php echo TRANSPORT_NAME; ?>/other_users/add_new_user" <?php if ($currentPage == 'add_new_user') { echo 'class="active"'; } ?>>
										<span class="list-icon">&nbsp;</span><?php if ($this->lang->line('add_user_management') != '') echo stripslashes($this->lang->line('add_user_management')); else echo 'Add User'; ?>
									</a>
								</li>


								<li>
								<a href="<?php echo TRANSPORT_NAME; ?>/other_users/display_user_list" <?php if ($currentPage == 'display_user_list') { echo 'class="active"'; } ?>>
									<span class="list-icon">&nbsp;</span>User List
								</a>
								</li>
							</ul>
						</li>
			<?php
					}
			?>




			<?php if(isset($schooladminPrevs["driver"]))
					{ ?>

					<li>
						<a href="<?php echo $current_url; ?>" <?php if (($currentUrl == 'drivers' || $currentPage == 'view_driver_reviews') && ($currentPage != 'add_edit_category_types' && $currentPage != 'add_edit_category' && $currentPage != 'display_drivers_category' && $currentPage != 'edit_language_category')) { echo 'class="active"'; } ?>>
							<span class="nav_icon user_2"></span> <?php if ($this->lang->line('admin_menu_driver_management') != '') echo stripslashes($this->lang->line('admin_menu_driver_management')); else echo 'Driver Management'; ?><span class="up_down_arrow">&nbsp;</span>
						</a>

						<ul class="acitem" <?php if (($currentUrl == 'drivers' || $currentPage == 'view_driver_reviews') && ($currentPage != 'add_edit_category_types' && $currentPage != 'add_edit_category' && $currentPage != 'display_drivers_category' && $currentPage != 'edit_language_category')) { echo 'style="display: block;"'; } else { echo 'style="display: none;"'; }?>>
							
						<?php  
							 if(in_array("dashboard", $schooladminPrevs["driver"]))

							{ ?>
							<li>
								<a href="<?php echo TRANSPORT_NAME; ?>/drivers/display_driver_dashboard" <?php if ($currentPage == 'display_driver_dashboard') { echo 'class="active"';}?>>
									<span class="list-icon">&nbsp;</span><?php if ($this->lang->line('admin_menu_drivers_dashboard') != '') echo stripslashes($this->lang->line('admin_menu_drivers_dashboard')); else echo 'Drivers Dashboard'; ?>
								</a>
							</li>

						<?php }

						 ?>
						

					<?php  
							 if(in_array("edit_driver_form", $schooladminPrevs["driver"]) || in_array("view_driver", $schooladminPrevs["driver"]) )

							{ ?>
							<li>
								<a href="<?php echo TRANSPORT_NAME; ?>/drivers/display_drivers_list" <?php if ($currentPage == 'display_drivers_list' || $currentPage == 'edit_driver_form' || $currentPage == 'change_password_form' || $currentPage == 'view_driver' || $currentPage == 'banking' || $currentPage == 'view_driver_reviews' || $currentPage == 'display_rides') { echo 'class="active"'; } ?>>
									<span class="list-icon">&nbsp;</span><?php if ($this->lang->line('admin_menu_drivers_list') != '') echo stripslashes($this->lang->line('admin_menu_drivers_list')); else echo 'Drivers List'; ?>
								</a>
							</li>
						<?php } 

							
						?>
						
							<?php  
							 if(in_array("add_driver_form", $schooladminPrevs["driver"]))

							{ ?>
							<li>
								<a href="<?php echo TRANSPORT_NAME; ?>/drivers/add_driver_form" <?php if ($currentPage == 'add_driver_form') { echo 'class="active"'; }?>>
									<span class="list-icon">&nbsp;</span><?php if ($this->lang->line('admin_menu_add_driver') != '') echo stripslashes($this->lang->line('admin_menu_add_driver')); else echo 'Add Driver'; ?>
								</a>
							</li>
						<?php 
								}
								

						?>




						</ul>
					</li>

					<?php } ?>




					<?php if(isset($schooladminPrevs["trip"]))
					{ ?>	



					<li>
						<a href="<?php echo $currentUrl; ?>" <?php if ($currentUrl == 'trip' || $currentPage == 'assign_route_view') { echo 'class="active"'; } ?>>
							<span class="nav_icon car"></span> <?php if ($this->lang->line('heading_trip_management') != '') echo stripslashes($this->lang->line('heading_trip_management')); else echo 'Trip Management'; ?><span class="up_down_arrow">&nbsp;</span>
						</a>
						<ul class="acitem" <?php if ($currentUrl == 'trip') { echo 'style="display: block;"'; } else { echo 'style="display: none;"'; } ?>>
								
						<?php  
							 if(in_array("assign_route_view", $schooladminPrevs["trip"]))

							{ ?>	
							<li>
								<a href="<?php echo TRANSPORT_NAME; ?>/trip/assign_route_view" <?php if($currentPage=='assign_route_view'){ echo 'class="active"';} ?>>
									<span class="list-icon">&nbsp;</span><?php if ($this->lang->line('add_route_trip') != '') echo stripslashes($this->lang->line('add_route_trip')); else echo 'Add Trip'; ?>
								</a>
							</li>

						<?php  
							 
							 }
						  ?>	



						<?php  
							 if(in_array("view_trip", $schooladminPrevs["trip"]) ||  in_array("edit_trip_bus", $schooladminPrevs["trip"]))

							{ ?>		  

							<li>
								<a href="<?php echo TRANSPORT_NAME; ?>/trip/trip_list_view" <?php if($currentPage=='trip_list_view'){ echo 'class="active"';} ?>>
									<span class="list-icon">&nbsp;</span><?php if ($this->lang->line('trip_list_view') != '') echo stripslashes($this->lang->line('trip_list_view')); else echo 'Trip List'; ?>
								</a>
							</li>
									
								<?php  
							 
							 }
						?>	

							<?php  
										if(in_array("trip_details_list", $schooladminPrevs["trip"]))
										{ 
									?>	
											<li>
												<a href="<?php echo TRANSPORT_NAME; ?>/trip/trip_details_list" <?php if($currentPage=='trip_details_list'){ echo 'class="active"';} ?>>
													<span class="list-icon">&nbsp;</span><?php if ($this->lang->line('trip_details_list') != '') echo stripslashes($this->lang->line('trip_details_list')); else echo 'Trip History'; ?>
												</a>
											</li>
												
									<?php  							 
										}
									?>

								
						</ul>
					</li>	               
               
		

					<?php } ?>



					<?php if(isset($schooladminPrevs["bus_route"]))
					{ ?>	
 	
                    
                    <li>
						<a href="<?php echo $current_url; ?>" <?php if ($currentUrl == 'bus_route') { echo 'class="active"'; }?>>
							<span class="nav_icon globe"></span> <?php if ($this->lang->line('bus_route_management_side_nav') != '') echo stripslashes($this->lang->line('bus_route_management_side_nav')); else echo 'Route Management'; ?><span class="up_down_arrow">&nbsp;</span>
						</a>
						<ul class="acitem" <?php if ($currentUrl == 'bus_route') { echo 'style="display: block;"'; } else { echo 'style="display: none;"'; } ?>>
							


						<?php  
							 if(in_array("add_bus_route", $schooladminPrevs["bus_route"]))

							{ ?>	


							<li>
								<a href="<?php echo TRANSPORT_NAME; ?>/bus_route/add_bus_route" <?php if ($currentPage == 'add_bus_route') { echo 'class="active"'; } ?>>
									<span class="list-icon">&nbsp;</span><?php if ($this->lang->line('add_bus_route_side_nav') != '') echo stripslashes($this->lang->line('add_bus_route_side_nav')); else echo 'Add Route';?>
								</a>
							</li>
							
							<?php  
							 
							 }
						?>	

						<?php  
							 if(in_array("view_bus_route", $schooladminPrevs["bus_route"]))

							{ ?>	

							<li>
								<a href="<?php echo TRANSPORT_NAME; ?>/bus_route/list_bus_route" <?php if ($currentPage == 'list_bus_route') { echo 'class="active"'; } ?>>
									<span class="list-icon">&nbsp;</span><?php if ($this->lang->line('route_list_bus_route_side_nav') != '') echo stripslashes($this->lang->line('route_list_bus_route_side_nav')); else echo 'Route List'; ?>
								</a>
							</li>
						

								<?php  
							 
							 }
						?>	




						</ul>
					</li>
                    
                    <?php } ?>





                    	<?php if(isset($schooladminPrevs["add_bus"]))
					{ ?>	
 	
                    
                    
                    <li>
						<a href="<?php echo $current_url; ?>" <?php if ($currentUrl == 'add_bus') { echo 'class="active"'; }?>>
							<span class="nav_icon" style="background: unset;"><img style="width:100%;    vertical-align: unset;" src="images/Bus.png"></span> <?php if ($this->lang->line('vehicle_management_side_nav') != '') echo stripslashes($this->lang->line('vehicle_management_side_nav')); else echo 'Vehicle Management'; ?><span class="up_down_arrow">&nbsp;</span>
						</a>
						<ul class="acitem" <?php if ($currentUrl == 'add_bus') { echo 'style="display: block;"'; } else { echo 'style="display: none;"'; } ?>>
							




						<?php  
							 if(in_array("add_bus_new", $schooladminPrevs["add_bus"]))

							{ ?>	




							<li>
								<a href="<?php echo TRANSPORT_NAME; ?>/add_bus/add_bus_new" <?php if ($currentPage == 'add_bus_new') { echo 'class="active"'; } ?>>
									<span class="list-icon">&nbsp;</span><?php if ($this->lang->line('add_new_vehicle_nav') != '') echo stripslashes($this->lang->line('add_new_vehicle_nav')); else echo 'Add Vehicle';?>
								</a>
							</li>
						<?php } ?>






						<?php  
							 if(in_array("view_bus_details", $schooladminPrevs["add_bus"]))

							{ ?>	




							<li>
								<a href="<?php echo TRANSPORT_NAME; ?>/add_bus/bus_list" <?php if ($currentPage == 'bus_list') { echo 'class="active"'; } ?>>
									<span class="list-icon">&nbsp;</span><?php if ($this->lang->line('vehicle_list_nav') != '') echo stripslashes($this->lang->line('vehicle_list_nav')); else echo 'Vehicle List';?>
								</a>
							</li>
						<?php } ?>
						</ul>
					</li>
                    
                    
                       <?php } ?>



















































					<li>
						<a href="<?php echo $current_url; ?>" <?php if ($currentUrl == 'map') { echo 'class="active"'; }?>>
							<span class="nav_icon marker map-new"></span> <?php if ($this->lang->line('admin_menu_map_view') != '') echo stripslashes($this->lang->line('admin_menu_map_view')); else echo 'Map View'; ?><span class="up_down_arrow">&nbsp;</span>
						</a>
						<ul class="acitem" <?php if ($currentUrl == 'map') { echo 'style="display: block;"'; } else { echo 'style="display: none;"'; } ?>>
							<li>
								<a href="<?php echo TRANSPORT_NAME; ?>/map/map_avail_drivers" <?php if ($currentPage == 'map_avail_drivers') { echo 'class="active"'; } ?>>
									<span class="list-icon">&nbsp;</span><?php if ($this->lang->line('admin_menu_view_available_drivers') != '') echo stripslashes($this->lang->line('admin_menu_view_available_drivers')); else echo 'View available drivers';?>
								</a>
							</li>
							<li>
								<a href="<?php echo TRANSPORT_NAME; ?>/map/map_avail_users" <?php if ($currentPage == 'map_avail_users') { echo 'class="active"'; } ?>>
									<span class="list-icon">&nbsp;</span><?php if ($this->lang->line('admin_menu_view_available_users') != '') echo stripslashes($this->lang->line('admin_menu_view_available_users')); else echo 'View available users'; ?>
								</a>
							</li>
						</ul>
					</li>
					


					<!-- notification -->


                    <?php if(isset($schooladminPrevs["notification"]))
					{ ?>	
 	
                    
					<li>
						<a href="<?php echo $current_url; ?>" <?php if ($currentUrl == 'notification' || $currentPage == 'display_notification_user_list') { echo 'class="active"'; } ?>>
						<span class="nav_icon users"></span> <?php if ($this->lang->line('admin_menu_notification') != '') echo stripslashes($this->lang->line('admin_menu_notification')); else echo 'Notification'; ?><span class="up_down_arrow">&nbsp;</span>
						</a>
						<ul class="acitem" <?php if ($currentUrl == 'notification' || $currentPage == 'display_notification_user_list' || $currentPage == 'display_notification_driver_list') { echo 'style="display: block;"'; } else { echo 'style="display: none;"'; } ?>>
							

							<?php  
							 if(in_array("send_users_notification", $schooladminPrevs["notification"]))

							{ ?>		
							<li>
								<a href="<?php echo TRANSPORT_NAME; ?>/notification/display_notification_driver_list" <?php if ($currentPage == 'display_notification_driver_list') { echo 'class="active"'; } ?>>
								<span class="list-icon"></span> <?php if ($this->lang->line('admin_menu_drivers') != '') echo stripslashes($this->lang->line('admin_menu_drivers')); else echo 'Drivers'; ?>
								</a>
							</li>

							<?php } ?>

							<?php  
							if(in_array("send_drivers_notification", $schooladminPrevs["notification"]))

							{ ?>	


							<li>
								<a href="<?php echo TRANSPORT_NAME; ?>/notification/display_notification_user_list" <?php if ($currentPage == 'display_notification_user_list') { echo 'class="active"'; } ?>>
								<span class="list-icon"></span> <?php if ($this->lang->line('admin_menu_users') != '') echo stripslashes($this->lang->line('admin_menu_users')); else echo 'Users'; ?>
								</a>
							</li>
						<?php } ?>
						</ul>
					</li>
													
                 	<?php } ?>

</ul>
</div>
</div>


<!-- mobile header end -->







</div>
<div class="header_right">
<div id="user_nav" style="width: 300px;">
<ul>
	<li class="user_thumb"><span class="icon"><img src="images/profile.png" class="tipBot" width="30" height="30" alt="<?php echo $this->session->userdata(APP_NAME.'_session_operator_name'); ?>" title="<?php echo $this->session->userdata(APP_NAME.'_session_operator_name'); ?>"></span></li>
	<li class="user_info">
			<span class="user_name">
				
			
			<?php if ($allPrev == '1'){?>
			
				<a href="<?php echo base_url();?>" target="_blank" class="tipBot" title="<?php if ($this->lang->line('driver_view_site') != '') echo stripslashes($this->lang->line('driver_view_site')); else echo 'View Site'; ?>"><?php if ($this->lang->line('admin_header_visit_site') != '') echo stripslashes($this->lang->line('admin_header_visit_site')); else echo 'Visit Site'; ?></a> &#124;               
				<a href="<?php echo TRANSPORT_NAME; ?>/settings/edit_profile_form" class="tipBot" title="<?php if ($this->lang->line('driver_edit_account_details') != '') echo stripslashes($this->lang->line('driver_edit_account_details')); else echo 'Edit account details'; ?>"><?php if ($this->lang->line('admin_menu_settings') != '') echo stripslashes($this->lang->line('admin_menu_settings')); else echo 'Settings'; ?></a>
			
			<?php }else {?>
		
				<a href="<?php echo base_url();?>" target="_blank" class="tipBot" title="<?php if ($this->lang->line('driver_view_site') != '') echo stripslashes($this->lang->line('driver_view_site')); else echo 'View Site'; ?>"><?php if ($this->lang->line('admin_header_visit_site') != '') echo stripslashes($this->lang->line('admin_header_visit_site')); else echo 'Visit Site'; ?></a> &#124; 
				<a href="<?php echo TRANSPORT_NAME; ?>/settings/change_password" class="tipBot" title="<?php if ($this->lang->line('driver_click_to_change') != '') echo stripslashes($this->lang->line('driver_click_to_change')); else echo 'Click to change your password'; ?>"><?php if ($this->lang->line('admin_menu_change_password') != '') echo stripslashes($this->lang->line('admin_menu_change_password')); else echo 'Change Password'; ?></a> 
			</span>
			<?php }?>
	</li>
	
	<li class="logout"><a href="<?php echo TRANSPORT_NAME; ?>/settings/logout" class="tipBot" title="<?php if ($this->lang->line('rider_profile_logout') != '') echo stripslashes($this->lang->line('rider_profile_logout')); else echo 'Logout'; ?>"><span class="icon"></span><?php if ($this->lang->line('rider_profile_logout') != '') echo stripslashes($this->lang->line('rider_profile_logout')); else echo 'Logout'; ?></a></li>
</ul>
</div>
	</div>
</div>
<?php if (validation_errors() != ''){?>
<div id="validationErr">
		<script>setTimeout("hideErrDiv('validationErr')", 3000);</script>
		<p><?php echo validation_errors();?></p>
</div>
<?php }?>


<script src="js/jquery.growl.js" type="text/javascript"></script>
<link href="css/jquery.growl.css" rel="stylesheet" type="text/css" />

<?php if($this->session->flashdata('sErrMSG') != '') { ?>
<script type="text/javascript">
		<?php 
		$sErrMSGdecoded = base64_decode($this->session->flashdata('sErrMSG'));
		$sErrMSGKeydecoded = base64_decode($this->session->flashdata('sErrMSGKey'));
		if($this->session->flashdata('sErrMSGType')=='message-red'){
		?>
				$.growl.error({ location: "tc",title:"<?php echo $sErrMSGKeydecoded; ?>",message: "<?php echo  $sErrMSGdecoded;  ?>" });
		<?php } ?>
		<?php
		if($this->session->flashdata('sErrMSGType')=='message-green'){ 
		?>
				$.growl.notice({ location: "tc",title:"<?php echo $sErrMSGKeydecoded; ?>",message: "<?php echo  $sErrMSGdecoded;  ?>"});
		<?php } ?>
		<?php 
		if($this->session->flashdata('sErrMSGType')=='warning'){ 
		?>
				$.growl.warning({ location: "tc",message: "<?php echo  $sErrMSGdecoded;  ?>" });
		<?php } ?>
</script>
<?php } ?>

<input type="hidden" id="tabValidator" value="Yes"/>
