<?php
$this->load->view(TRANSPORT_NAME.'/templates/header.php');
?>
<?php
$type = '';
if(isset($_GET['type']) && $_GET['type']!='') {
        $type=$_GET['type'];
}
$dialcode=array();

foreach ($countryList as $country) {    
     if ($country->dial_code != '') {           
            $dialcode[]=str_replace(' ', '', $country->dial_code);
     }
}
 
asort($dialcode);
$dialcode=array_unique($dialcode);                                    
?>

<script>
$(document).ready(function(){
        $vehicle_category='';
        $country='';
        <?php  if($type == 'vehicle_type' && isset($_GET['vehicle_category'])) { ?>
        $vehicle_category = "<?php echo $_GET['vehicle_category']; ?>";
        <?php }?>
        <?php  if($type == 'mobile_number' &&  isset($_GET['country'])) {?>
        $country = "<?php echo $_GET['country']; ?>";
        <?php }?>
        if($vehicle_category != ''){
                $('.vehicle_category').css("display","inline");
                $('#filtervalue').css("display","none");
                $('#filtervalue').css("display","block");
                $("#country").attr("disabled", true);
        }
        if($country != ''){
                $('#country').css("display","inline");
                $('.vehicle_category').attr("disabled", true);      
        }
        $("#filtertype").change(function(){
                $filter_val = $(this).val();
                $('#filtervalue').val('');
                $('.vehicle_category').css("display","none");
                $('#filtervalue').css("display","inline");
                $('#country').css("display","none");
                $("#country").attr("disabled", true);
                $(".vehicle_category").attr("disabled", true);
            
                if($filter_val == 'vehicle_type'){
                        $('.vehicle_category').css("display","inline");
                        $('#filtervalue').css("display","none");
                        $('#country').css("display","none");
                        $('.vehicle_category').prop("disabled", false);
                        $("#country").attr("disabled", true);
                }
                if($filter_val == 'mobile_number'){
                        $('#country').css("display","inline");
                        $('#country').prop("disabled", false);
                        $(".vehicle_category").attr("disabled", true);
                        $('.vehicle_category').css("display","none");
                }           
        }); 
});
</script>
<div id="content">
    <div class="grid_container">
    
        <div class="grid_12">
                <div class="">
                        <div class="widget_content">
                                <span class="clear"></span>                     
                                <div class="">
                                        <div class=" filter_wrap">
                                                
                                        </div>
                                </div>
                        </div>
                </div>
        </div>
    
        <?php
        $attributes = array('id' => 'display_form');
        echo form_open(TRANSPORT_NAME.'/drivers/change_driver_status_global', $attributes)
        ?>
        <div class="grid_12">
            <div class="widget_wrap">
                <div class="widget_top">
                    <span class="h_icon blocks_images"></span>
                    <h6><?php echo $heading ?></h6>

                </div>
                <div class="widget_content">
                   <?php
                    if ($paginationLink != '') {
                        echo $paginationLink;
                        $tble = 'allstudent_list';
                    } else {
                        $tble = 'student_list';
                    }
                    ?> 

                    <table class="display" id="<?php echo $tble; ?>" width='100%'>
                        <thead>
                            <tr>
                                <th style="width:65px" class="center">
                                    <?php if ($this->lang->line('operator_s_no') != '') echo stripslashes($this->lang->line('operator_s_no')); else echo 'S.No'; ?>
                                </th>

                                <th style="width:170px" class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                    <?php if ($this->lang->line('student_list_student_name') != '') echo stripslashes($this->lang->line('student_list_student_name')); else echo 'Student Name'; ?>
                                </th>

                                <th class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                     <?php if ($this->lang->line('operator_add_student_class_title_category') != '') echo stripslashes($this->lang->line('operator_add_student_class_title_category')); else echo 'Class'; ?>
                                </th>

                                <th class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                     <?php if ($this->lang->line('operator_add_student_section_title_category') != '') echo stripslashes($this->lang->line('operator_add_student_section_title_category')); else echo 'Section'; ?>
                                </th>

                                <th class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                     <?php if ($this->lang->line('operator_add_student_trip_type_category') != '') echo stripslashes($this->lang->line('operator_add_student_trip_type_category')); else echo 'Trip Type'; ?>
                                </th>

                                <th class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                     <?php if ($this->lang->line('operator_add_student_trip_name_category') != '') echo stripslashes($this->lang->line('operator_add_student_trip_name_category')); else echo 'Trip Name'; ?>
                                </th>                                

                               <!--  <th class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                    <?php if ($this->lang->line('operator_student_bus_route_title') != '') echo stripslashes($this->lang->line('operator_student_bus_route_title')); else echo 'BUS ROUTE'; ?>
                                </th>

                                <th class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                     <?php if ($this->lang->line('operator_student_bus_stop_title') != '') echo stripslashes($this->lang->line('operator_student_bus_stop_title')); else echo 'BUS STOP'; ?>
                                </th> -->

                                
                                <th>
                                    <?php if ($this->lang->line('admin_subadmin_action') != '') echo stripslashes($this->lang->line('admin_subadmin_action')); else echo 'Action'; ?>
                                </th>

                            </tr>
                        </thead>


                        <tbody>

                        <?php 
                            $i = 0;
                            if ($student_list_all->num_rows() > 0) 
                            {                           
                                foreach ($student_list_all->result() as $row) 
                                { 
                                    $i++;
                        ?>
                                    <tr style="border-bottom: 1px solid #dddddd !important;">

                                        <td class="center tr_select ">
                                            <?php echo $i; ?>
                                        </td>

                                        <td class="center">
                                            <?php echo $row->student_first_name; ?>                                            
                                        </td>                   
                                                                            
                                        <td class="center" style="width:70px;">
                                            <?php 

                                            if($row->student_class != "")
                                              {  
                                                $class_id= array('_id' => new MongoDB\BSON\ObjectId($row->student_class));

                                                $class_detail = $this->class_details_model->get_all_details(CLASS_DETAILS, $class_id);

                                                $class_detail = $class_detail->result();                                            
                                                if(!empty($class_detail))
                                                {    
                                                echo $class_detail[0]->class_name;
                                                }
                                                else
                                                 {
                                                    echo "-";
                                                 } 

                                               }
                                               else
                                                {
                                                     echo "-";
                                                }    
                                            ?>                                              
                                        </td>       

                                        <td class="center" style="width:50px;">
                                            <?php  

                                            if($row->student_section != "")
                                            {                                                
                                                $section_id= array('_id' => new MongoDB\BSON\ObjectId($row->student_section));

                                                $section_detail = $this->class_details_model->get_all_details(SECTION_DETAILS, $section_id);

                                                $section_detail = $section_detail->result();                                            

                                              
                                               

                                                 if(!empty($section_detail))
                                                {    
                                                    echo $section_detail[0]->section_name;
                                                }
                                                else
                                                 {
                                                    echo "-";
                                                 }  
                                              }
                                              else
                                               {
                                                     echo "-";
                                               }   


                                            ?>
                                        </td>


                                        <td>
                                            <?php                                                
                                                $student_trip_type = $row->student_trip_type;
                                            ?>

                                            <?php
                                                if($student_trip_type == "pickup")
                                                {
                                            ?>
                                                    <div>
                                            <?php
                                                    echo "One way(Home to School)";
                                            ?>
                                                    </div>
                                            <?php
                                                }

                                                if($student_trip_type == "drop")
                                                {
                                            ?>
                                                    <div>
                                            <?php
                                                    echo "One way(School to Home)";
                                            ?>
                                                    </div>
                                            <?php
                                                }

                                                if($student_trip_type == "both")
                                                {
                                            ?>
                                                    <div>
                                            <?php
                                                    echo "One way(Home to School)";
                                            ?>
                                                    </div>
                                                    <div>
                                            <?php
                                                    echo "One way(School to Home)";
                                            ?>
                                                    </div>
                                            <?php
                                                } 
                                            ?>
                                        </td>


                                        <td>
                                            <?php
                                                $student_trip_type = $row->student_trip_type;
                                            ?>

                                            <?php
                                                if($student_trip_type == "pickup")
                                                {                                                    
                                                    $pickup_trip_id= array('_id' => new MongoDB\BSON\ObjectId($row->student_pickup_trip_id));

                                                    $pikup_trip_detail = $this->bus_trip_model->get_all_details(BUS_TRIP, $pickup_trip_id);

                                                    $pikup_trip_detail = $pikup_trip_detail->result(); 
                                            ?>
                                                    <div>
                                            <?php
                                                        echo "One way(Home to School) - ".$pikup_trip_detail[0]->trip_name;
                                            ?>
                                                    </div>
                                            <?php
                                                }

                                                if($student_trip_type == "drop")
                                                {
                                                    $drop_trip_id= array('_id' => new MongoDB\BSON\ObjectId($row->student_drop_trip_id));

                                                    $drop_trip_detail = $this->bus_trip_model->get_all_details(BUS_TRIP, $drop_trip_id);

                                                    $drop_trip_detail = $drop_trip_detail->result();     
                                            ?>
                                                    <div>
                                            <?php 
                                                        echo "One way(School to Home) - ".$drop_trip_detail[0]->trip_name;
                                            ?>
                                                    </div>
                                            <?php                                                   
                                                }

                                                if($student_trip_type == "both")
                                                {

                                                    $pickup_trip_id= array('_id' => new MongoDB\BSON\ObjectId($row->student_pickup_trip_id));

                                                    $pikup_trip_detail = $this->bus_trip_model->get_all_details(BUS_TRIP, $pickup_trip_id);

                                                    $pikup_trip_detail = $pikup_trip_detail->result(); 
                                            ?>
                                                    <div>
                                            <?php
                                                        echo "One way(Home to School) - ".$pikup_trip_detail[0]->trip_name;
                                            ?>
                                                    </div>
                                                    
                                            <?php
                                                    $drop_trip_id= array('_id' => new MongoDB\BSON\ObjectId($row->student_drop_trip_id));

                                                    $drop_trip_detail = $this->bus_trip_model->get_all_details(BUS_TRIP, $drop_trip_id);

                                                    $drop_trip_detail = $drop_trip_detail->result();     
                                            ?>
                                                    <div>
                                            <?php 
                                                        echo "One way(School to Home) - ".$drop_trip_detail[0]->trip_name;
                                            ?>
                                                    </div>
                                            <?php                                                    
                                                } 
                                            ?>
                                        </td>

                                 







                                        <td class="center" style="width:30px;">

                                            <?php if($page_type == "student_lists") { ?>

                                            <span>
                                                    <a class="action-icons c-delete" href="<?php echo TRANSPORT_NAME; ?>/add_students/delete_student_details/<?php echo $row->_id;?>" title="<?php if ($this->lang->line('admin_common_delete') != '') echo stripslashes($this->lang->line('admin_common_delete')); else echo 'Delete'; ?>">
                                                            <?php if ($this->lang->line('admin_common_delete') != '') echo stripslashes($this->lang->line('admin_common_delete')); else echo 'Delete'; ?>
                                                    </a>
                                            </span>




                                            <span><a class="action-icons c-edit" href="<?php echo TRANSPORT_NAME; ?>/add_students/edit_student_detail/<?php echo $row->_id; ?>" title="<?php if ($this->lang->line('admin_common_edit') != '') echo stripslashes($this->lang->line('admin_common_edit')); else echo 'Edit'; ?>"><?php if ($this->lang->line('admin_common_edit') != '') echo stripslashes($this->lang->line('admin_common_edit')); else echo 'Edit'; ?></a></span>

                                            <?php } ?>


                                            <span><a class="action-icons c-suspend" href="<?php echo TRANSPORT_NAME; ?>/add_students/view_student_details/<?php echo $row->_id; ?>/<?php echo $page_type; ?>" title="<?php if ($this->lang->line('admin_common_view') != '') echo stripslashes($this->lang->line('admin_common_view')); else echo 'View'; ?>"></a></span>                                           
                                        </td>                                       
                                       
                                    </tr>
                    <?php
                                }
                            }
                    ?>
                        </tbody>

                        <tfoot>
                            <tr>
                                <th style="width:65px" class="center">
                                    <?php if ($this->lang->line('operator_s_no') != '') echo stripslashes($this->lang->line('operator_s_no')); else echo 'S.No'; ?>
                                </th>

                                <th style="width:170px" class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                    <?php if ($this->lang->line('student_list_student_name') != '') echo stripslashes($this->lang->line('student_list_student_name')); else echo 'Student Name'; ?>
                                </th>

                                <th class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                     <?php if ($this->lang->line('operator_add_student_class_title_category') != '') echo stripslashes($this->lang->line('operator_add_student_class_title_category')); else echo 'Class'; ?>
                                </th>

                                <th class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                     <?php if ($this->lang->line('operator_add_student_section_title_category') != '') echo stripslashes($this->lang->line('operator_add_student_section_title_category')); else echo 'Section'; ?>
                                </th>

                                <th class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                     <?php if ($this->lang->line('operator_add_student_trip_type_category') != '') echo stripslashes($this->lang->line('operator_add_student_trip_type_category')); else echo 'Trip Type'; ?>
                                </th>

                                <th class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                     <?php if ($this->lang->line('operator_add_student_trip_name_category') != '') echo stripslashes($this->lang->line('operator_add_student_trip_name_category')); else echo 'Trip Name'; ?>
                                </th>                                

                               <!--  <th class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                    <?php if ($this->lang->line('operator_student_bus_route_title') != '') echo stripslashes($this->lang->line('operator_student_bus_route_title')); else echo 'BUS ROUTE'; ?>
                                </th>

                                <th class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                     <?php if ($this->lang->line('operator_student_bus_stop_title') != '') echo stripslashes($this->lang->line('operator_student_bus_stop_title')); else echo 'BUS STOP'; ?>
                                </th> -->

                                
                                <th>
                                    <?php if ($this->lang->line('admin_subadmin_action') != '') echo stripslashes($this->lang->line('admin_subadmin_action')); else echo 'Action'; ?>
                                </th>
                            </tr>
                        </tfoot>





                        
                    </table>

                   <!--  <?php
                    if ($paginationLink != '') {
                        echo $paginationLink;
                    }
                    ?> -->

                </div>
            </div>
        </div>
        <input type="hidden" name="statusMode" id="statusMode"/>
        <input type="hidden" name="SubAdminEmail" id="SubAdminEmail"/>
        </form> 
    </div>
    <span class="clear"></span>
</div>
</div>
<style>                                     
        .b_warn {
            background: orangered none repeat scroll 0 0;
            border: medium none red;
        }
        
        .filter_widget .btn_30_light {
            margin: -11px;
            width: 83%;
        }
</style>
<?php
$this->load->view(TRANSPORT_NAME.'/templates/footer.php');
?>