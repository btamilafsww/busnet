<?php 
$this->load->view(TRANSPORT_NAME.'/templates/header.php');

$v_docx = 0; $d_docx = 0;
foreach($docx_list->result() as $docx) if($docx->category == 'Driver') $d_docx++; else $v_docx++;
?>
<div id="content" class="admin-settings edit-global-set add_drive_catagory">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">
                <div class="widget_top">
                    <span class="h_icon list"></span>
                    <h6><?php if ($this->lang->line('admin_drivers_edit_driver') != '') echo stripslashes($this->lang->line('admin_drivers_edit_driver')); else echo 'Edit Driver'; ?></h6>
                </div>
                <div class="widget_content">
                    <?php
                    $attributes = array('class' => 'form_container left_label', 'id' => 'driver_form', 'enctype' => 'multipart/form-data');
                    echo form_open_multipart(TRANSPORT_NAME.'/drivers/insertEdit_driver', $attributes);

                    $driver_details = $driver_details->row();
                    ?>
                                        <input name="driver_id" id="driver_id" type="hidden" value="<?php echo $driver_details->_id; ?>" />
                    <ul class="left-contsec operator_drive_sec">
                        
                      

                        <li>
                            <div class="form_grid_12">
                                <h3><?php if ($this->lang->line('admin_drivers_login_details') != '') echo stripslashes($this->lang->line('admin_drivers_login_details')); else echo 'Login Details'; ?></h3>
                            </div>
                        </li>

                        <li>
                            <div class="form_grid_12">
                                <label class="field_title"><?php if ($this->lang->line('admin_drivers_driver_name') != '') echo stripslashes($this->lang->line('admin_drivers_driver_name')); else echo 'Driver Name'; ?> <span class="req">*</span></label>
                                <div class="form_input">
                                    <input name="driver_name" id="driver_name" type="text"  class="required large tipTop alphanumeric" title="<?php if ($this->lang->line('driver_upload_enter_driver_fullname') != '') echo stripslashes($this->lang->line('driver_upload_enter_driver_fullname')); else echo 'Please enter the driver fullname'; ?>" value="<?php if (isset($driver_details->driver_name)) echo $driver_details->driver_name; ?>" />
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="form_grid_12">
                                <label class="field_title"><?php if ($this->lang->line('admin_drivers_email_address') != '') echo stripslashes($this->lang->line('admin_drivers_email_address')); else echo 'Email Address'; ?> <span class="req">*</span></label>
                                <div class="form_input">
                                    <input name="email" id="email" type="text"  class="required large tipTop email" title="<?php if ($this->lang->line('driver_enter_driver_email_address') != '') echo stripslashes($this->lang->line('driver_enter_driver_email_address')); else echo 'Please enter the driver email address'; ?>" value="<?php if (isset($driver_details->email)) echo $driver_details->email; ?>" readonly onkeypress="javascript: alert('You can not change email');" />
                                </div>
                            </div>
                        </li>


                        <li>
                            <div class="form_grid_12">
                                <h3><?php if ($this->lang->line('admin_drivers_address_details') != '') echo stripslashes($this->lang->line('admin_drivers_address_details')); else echo 'Address Details'; ?></h3>
                            </div>
                        </li>

                        <li>
                            <div class="form_grid_12">
                                <label class="field_title"><?php if ($this->lang->line('admin_drivers_address') != '') echo stripslashes($this->lang->line('admin_drivers_address')); else echo 'Address'; ?><span class="req">*</span></label>
                                <div class="form_input">
                                    <textarea name="address" id="address"  class="required large tipTop" title="<?php if ($this->lang->line('driver_enter_driver_address') != '') echo stripslashes($this->lang->line('driver_enter_driver_address')); else echo 'Please enter the driver address'; ?>" style="width: 372px;"><?php if (isset($driver_details->address['address'])) echo $driver_details->address['address']; ?></textarea>
                                </div>
                            </div>
                        </li>

                        <li>
                            <div class="form_grid_12">
                                <label class="field_title"><?php if ($this->lang->line('admin_drivers_country') != '') echo stripslashes($this->lang->line('admin_drivers_country')); else echo 'Country'; ?><span class="req">*</span></label>
                                <div class="form_input">
                                    <select name="county" id="county"  class="required chzn-select" style="height: 31px; width: 51%;">
                                        <option value=""><?php if ($this->lang->line('select_country') != '') echo stripslashes($this->lang->line('select_country')); else echo 'Select country'; ?></option>
                                        <?php foreach ($countryList as $country) { ?>
                                            <option value="<?php echo $country->name; ?>" data-dialCode="<?php echo $country->dial_code; ?>" <?php if ($driver_details->address['county'] == $country->name) echo 'selected="selected"' ?>><?php echo $country->name; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </li>

                        <li>
                            <div class="form_grid_12">
                                <label class="field_title"><?php if ($this->lang->line('admin_drivers_state_province_region') != '') echo stripslashes($this->lang->line('admin_drivers_state_province_region')); else echo 'State / Province / Region'; ?><span class="req">*</span></label>
                                <div class="form_input">
                                    <input name="state" id="state" type="text"  class="required large tipTop" title="<?php if ($this->lang->line('driver_enter_driver_state') != '') echo stripslashes($this->lang->line('driver_enter_driver_state')); else echo 'Please enter the state'; ?>" value="<?php if (isset($driver_details->address['state'])) echo $driver_details->address['state']; ?>"/>
                                </div>
                            </div>
                        </li>

                        <li>
                            <div class="form_grid_12">
                                <label class="field_title"><?php if ($this->lang->line('admin_drivers_city') != '') echo stripslashes($this->lang->line('admin_drivers_city')); else echo 'City'; ?><span class="req">*</span></label>
                                <div class="form_input">
                                    <input name="city" id="city" type="text"  class="required large tipTop" title="<?php if ($this->lang->line('location_enter_the_city') != '') echo stripslashes($this->lang->line('location_enter_the_city')); else echo 'Please enter the city'; ?>" value="<?php if (isset($driver_details->address['city'])) echo $driver_details->address['city']; ?>"/>
                                </div>
                            </div>
                        </li>




                        <li>
                            <div class="form_grid_12">
                                <label class="field_title"><?php if ($this->lang->line('admin_drivers_postal_code') != '') echo stripslashes($this->lang->line('admin_drivers_postal_code')); else echo 'Postal Code'; ?><span class="req">*</span></label>
                                <div class="form_input">
                                    <input name="postal_code" id="postal_code" type="text"   maxlength="10"class="required large tipTop" title="<?php if ($this->lang->line('driver_enter_postal_code') != '') echo stripslashes($this->lang->line('driver_enter_postal_code')); else echo 'Please enter the postal code'; ?>" value="<?php if (isset($driver_details->address['postal_code'])) echo $driver_details->address['postal_code']; ?>"/>
                                </div>
                            </div>
                        </li>


                        <li>
                            <div class="form_grid_12">
                                <label class="field_title"><?php if ($this->lang->line('admin_drivers_mobile_number') != '') echo stripslashes($this->lang->line('admin_drivers_mobile_number')); else echo 'Mobile Number'; ?><span class="req">*</span></label>
                                <div class="form_input">
                                    




                                      <!-- phone code -->



                                       <select style="width: 15% !important;" id="country_code" name="dail_code" class="required small tipTop chzn-select"  title="<?php if ($this->lang->line('driver_enter_mobile_country_code') != '') echo stripslashes($this->lang->line('driver_enter_mobile_country_code')); else echo 'Please enter mobile country code'; ?>" >


                                        <?php 




                                                    foreach ($dial_codes as $key => $value) 
                                                    {
                                                        if($value->dial_code != "")
                                                        {   
                                                                            if($value->dial_code == $driver_details->dail_code)
                                                                                {   
                                                                                    $html=$html."<option selected value='".$value->dial_code."'>".$value->dial_code."</option>";
                                                                                }
                                                                                else
                                                                                {
                                                                                    $html=$html."<option  value='".$value->dial_code."'>".$value->dial_code."</option>";
                                                                                }   

                                                        }
                                                    }



                                                    echo $html;
                                         ?>

                                       </select> 











                                    <input style="width: 84% !important;height: 37px;" name="mobile_number" placeholder="Mobile Number.." id="mobile_number" type="text"  class="required large tipTop phoneNumber" title="<?php if ($this->lang->line('driver_enter_mobile_number') != '') echo stripslashes($this->lang->line('driver_enter_mobile_number')); else echo 'Please enter the mobile number'; ?>" maxlength="20" value="<?php if (isset($driver_details->mobile_number)) echo $driver_details->mobile_number; ?>"/>
                                </div>
                            </div>
                        </li>

                        <li>
                            <div class="form_grid_12">
                                <h3><?php if ($this->lang->line('admin_drivers_identity') != '') echo stripslashes($this->lang->line('admin_drivers_identity')); else echo 'Identity'; ?></h3>
                            </div>
                        </li>

                        <li>
                            <div class="form_grid_12">
                                <label class="field_title"><?php if ($this->lang->line('admin_drivers_driver_image') != '') echo stripslashes($this->lang->line('admin_drivers_driver_image')); else echo 'Driver Image'; ?></label>
                                <div class="form_input">
                                    <input name="thumbnail" id="thumbnail" type="file"  class="large tipTop" title="<?php if ($this->lang->line('driver_select_driver_image') != '') echo stripslashes($this->lang->line('driver_select_driver_image')); else echo 'Please select driver image'; ?>" value="<?php if (isset($driver_details->driver_name)) echo $driver_details->driver_name; ?>"/>

                                    <br/>

                                    <?php if (isset($driver_details->image) != '') { ?>
                                        <img width="15%" src="<?php echo base_url() . USER_PROFILE_THUMB . $driver_details->image; ?>" />
                                    <?php } else { ?>
                                        <img width="15%" src="<?php echo base_url() . USER_PROFILE_THUMB_DEFAULT; ?>" />
                                    <?php } ?>


                                </div>
                            </div>
                        </li>                   
                    </ul>

                    <ul class="rite-contsec operator_drive_sec">  
                      




                        <?php if ($docx_list->num_rows() > 0  && $d_docx > 0) { ?>

                            <li>
                                <div class="form_grid_12">
                                    <h3><?php if ($this->lang->line('admin_drivers_driver_documents') != '') echo stripslashes($this->lang->line('admin_drivers_driver_documents')); else echo 'Driver Documents'; ?></h3>
                                </div>
                            </li>

                            <?php
                            $doc = 0;
                            foreach ($docx_list->result() as $docx) {
                                if ($docx->category == 'Driver') {
                                    $docx_uniq = 'docx-' . $docx->_id;

                                    $docxValues = '';
                                    $expiryValue = '';
                                    $fileName = '';
                                    if (isset($driver_details->documents) && isset($driver_details->documents['driver'][(string) $docx->_id])) {

                                        if (!isset($driver_details->documents['driver'][(string) $docx->_id]['typeName'])) {
                                            continue;
                                        }
                                        
                                        if(isset($driver_details->documents['driver'][(string) $docx->_id]['fileName'])){
                                            $fileName = $driver_details->documents['driver'][(string) $docx->_id]['fileName'];
                                        }
                                        
                                        if(isset($driver_details->documents['driver'][(string) $docx->_id]['expiryDate'])){
                                            $expiryValue = $driver_details->documents['driver'][(string) $docx->_id]['expiryDate'];
                                        }
                                        
                                        if(isset($driver_details->documents['driver'][(string) $docx->_id]['typeName'])){
                                            $typeName = $driver_details->documents['driver'][(string) $docx->_id]['typeName'];
                                            $docxValues = $typeName . '|:|' . $fileName . '|:|' . (string) $docx->_id . '|:|Old-docx';
                                            
                                        } else {
                                            $docxValues = $docx->name . '|:||:|' . (string) $docx->_id . '|:|Old-docx';
                                        }
                                        
                                    }
                                    ?>

                                    <li>
                                        <div class="form_grid_12">
                                            <label class="field_title"><?php
                                                echo $docx->name;
                                                if ($docx->hasreq == 'Yes') {
                                                  //  echo '<span class="req">*</span>';
                                                }
                                                ?> </label>
                                            <div class="form_input">
                                                <input name="<?php echo $docx_uniq; ?>" id="<?php echo $docx_uniq; ?>" data-docx="<?php echo $docx->name; ?>" data-docx_id="<?php echo $docx->_id; ?>" type="file"  value="" class="large tipTop <?php
                                                if ($docx->hasreq == 'Yes' && $fileName == '') {
                                                    echo '';
                                                }
                                                ?> docx" title="<?php if ($this->lang->line('admin_please_select') != '') echo stripslashes($this->lang->line('admin_please_select')); else echo 'Please select'; ?> <?php echo strtolower($docx->name); ?>"/>
                                                <input type="hidden" name="driver_docx[]" value="<?php echo $docxValues; ?>" id="<?php echo $docx_uniq; ?>-Hid" />
                                                <input type="hidden" name="driver_docx_expiry[]" value="<?php echo $docx->hasexp; ?>" />
                                                <span id="<?php echo $docx_uniq; ?>-Err" style="color:red;"></span>
                                                <span id="<?php echo $docx_uniq; ?>-Succ" style="color:green;"></span>
                                                <?php
                                                if ($fileName != '') {
                                                    ?>
                                                    <a href="drivers_documents/<?php echo $fileName; ?>" target="_blank" id="<?php echo $docx_uniq; ?>-View"> <?php if ($this->lang->line('admin_drivers_view_documents') != '') echo stripslashes($this->lang->line('admin_drivers_view_documents')); else echo 'Dashboard'; ?>  </a>
                                                <?php } else { ?>
                                                    <a href="drivers_documents/<?php echo $fileName; ?>" target="_blank" id="<?php echo $docx_uniq; ?>-View"></a>
                                                <?php } ?>
                                            </div>

                                            <?php if ($docx->hasexp == 'Yes') { ?>
                                                <label class="field_title"></label>
                                                <div class="form_input">
                                                    <div class="expiry_box">
                                                        <b><?php if ($this->lang->line('admin_drivers_expiry_date') != '') echo stripslashes($this->lang->line('admin_drivers_expiry_date')); else echo 'Expiry Date'; ?> : </b>
                                                        <input type="text"  id="expiry-<?php echo $docx_uniq; ?>" class="" name="driver-<?php echo url_title($docx->name); ?>"  value="<?php echo $expiryValue; ?>"/> 
                                                    </div>
                                                </div>

                                                <script>
                                                    $(function () {
                                                        var mdate = new Date('<?php echo date("F d,Y H:i:s"); ?>');
                                                        $("#expiry-<?php echo $docx_uniq; ?>").datepicker();
                                                        $("#expiry-<?php echo $docx_uniq; ?>").datepicker("option", "changeMonth", "true");
                                                        $("#expiry-<?php echo $docx_uniq; ?>").datepicker("option", "changeYear", "true");
                                                        $("#expiry-<?php echo $docx_uniq; ?>").datepicker("option", "minDate", mdate);
                                                        $("#expiry-<?php echo $docx_uniq; ?>").datepicker("option", "showAnim", "clip");
                                                        // drop,fold,slide,bounce,slideDown,blind
                                                    });
                                                </script>

                                            <?php } ?>

                                        </div>
                                    </li>
                                    <?php
                                    $doc++;
                                }
                            }
                        }
                        ?>

                        <?php
                        if ($docx_list->num_rows() > 0 && $v_docx > 0) {
                            ?>
                            <li>
                                <div class="form_grid_12">
                                    <h3><?php if ($this->lang->line('admin_drivers_vehicle_documents') != '') echo stripslashes($this->lang->line('admin_drivers_vehicle_documents')); else echo 'Vehicle Documents'; ?></h3>
                                </div>
                            </li>
                            <?php
                            $doc = 0;
                            foreach ($docx_list->result() as $docx) {
                                if ($docx->category == 'Vehicle') {
                                    $docx_uniq = 'docx-' . $docx->_id;

                                    $docxValues = '';
                                    $expiryValue = '';
                                    $fileName = '';
                                    if (isset($driver_details->documents)) {
                                        $did = (string) $docx->_id;
                                        if (!isset($driver_details->documents['vehicle'][$did]['typeName'])) {
                                            #continue;
                                        }
                                        
                                        if(isset($driver_details->documents['vehicle'][$did]['fileName'])){
                                            $fileName = $driver_details->documents['vehicle'][$did]['fileName'];
                                        }
                                        
                                        if(isset($driver_details->documents['vehicle'][$did]['expiryDate'])){
                                            $expiryValue = $driver_details->documents['vehicle'][$did]['expiryDate'];
                                        }
                                        
                                        if(isset($driver_details->documents['vehicle'][$did]['typeName'])) {
                                            $typeName = $driver_details->documents['vehicle'][$did]['typeName'];
                                            $docxValues = $typeName . '|:|' . $fileName . '|:|' . $did . '|:|Old-docx';
                                            
                                        } else {
                                            $docxValues = $docx->name . '|:||:|' . (string) $docx->_id . '|:|Old-docx';
                                        }
                                    }
                                    ?>

                                    <li>
                                        <div class="form_grid_12">
                                            <label class="field_title"><?php
                                                echo $docx->name;
                                                if ($docx->hasreq == 'Yes') {
                                                  //  echo '<span class="req">*</span>';
                                                }
                                                ?> </label>
                                            <div class="form_input">
                                                <input name="<?php echo $docx_uniq; ?>" id="<?php echo $docx_uniq; ?>" data-docx="<?php echo $docx->name; ?>" data-docx_id="<?php echo $docx->_id; ?>" type="file"  value="" class="large tipTop <?php
                                                if ($docx->hasreq == 'Yes' && $fileName == '') {
                                                    //echo 'required';
                                                }
                                                ?> docx" title="<?php if ($this->lang->line('admin_please_select') != '') echo stripslashes($this->lang->line('admin_please_select')); else echo 'Please select'; ?><?php echo strtolower($docx->name); ?>"/>
                                                <input type="hidden" name="vehicle_docx[]" value="<?php echo $docxValues; ?>" id="<?php echo $docx_uniq; ?>-Hid" />
                                                <input type="hidden" name="vehicle_docx_expiry[]" value="<?php echo $docx->hasexp; ?>" />
                                                <span id="<?php echo $docx_uniq; ?>-Err" style="color:red;"></span>
                                                <span id="<?php echo $docx_uniq; ?>-Succ" style="color:green;"></span>
                                                <?php
                                                if ($fileName != '') {
                                                    ?>
                                                    <a href="drivers_documents/<?php echo $fileName; ?>" target="_blank" id="<?php echo $docx_uniq; ?>-View"> <?php if ($this->lang->line('admin_drivers_view_documents') != '') echo stripslashes($this->lang->line('admin_drivers_view_documents')); else echo 'View Document'; ?> </a>
                                                <?php } else { ?>
                                                    <a href="drivers_documents/<?php echo $fileName; ?>" target="_blank" id="<?php echo $docx_uniq; ?>-View"></a>
                                                <?php } ?>
                                            </div>

                                            <?php if ($docx->hasexp == 'Yes') { ?>
                                                <label class="field_title"></label>
                                                <div class="form_input">
                                                    <div class="expiry_box">
                                                        <b><?php if ($this->lang->line('admin_drivers_expiry_date') != '') echo stripslashes($this->lang->line('admin_drivers_expiry_date')); else echo 'Expiry Date'; ?> : </b>
                                                        <input type="text"  id="expiry-<?php echo $docx_uniq; ?>" class="" name="vehicle-<?php echo url_title($docx->name); ?>"  value="<?php echo $expiryValue; ?>"/> 
                                                    </div>
                                                </div>

                                                <script>
                                                    $(function () {
                                                        var mdate = new Date('<?php echo date("F d,Y H:i:s"); ?>');
                                                        $("#expiry-<?php echo $docx_uniq; ?>").datepicker();
                                                        $("#expiry-<?php echo $docx_uniq; ?>").datepicker("option", "changeMonth", "true");
                                                        $("#expiry-<?php echo $docx_uniq; ?>").datepicker("option", "changeYear", "true");
                                                        $("#expiry-<?php echo $docx_uniq; ?>").datepicker("option", "minDate", mdate);
                                                        $("#expiry-<?php echo $docx_uniq; ?>").datepicker("option", "showAnim", "clip");
                                                        // drop,fold,slide,bounce,slideDown,blind
                                                    });
                                                </script>

                                            <?php } ?>

                                        </div>
                                    </li>
                                    <?php
                                    $doc++;
                                }
                            }
                        }
                        ?>  
                        
                        
                    </ul>
                    
                    <ul class="last-sec-btn">
                        <li class="change-pass">
                            <div class="form_grid_12">
                                <div class="form_input">
                                    <button type="submit" class="btn_small btn_blue" ><span><?php if ($this->lang->line('admin_subadmin_submit') != '') echo stripslashes($this->lang->line('admin_subadmin_submit')); else echo 'Submit'; ?></span></button>
                                </div>
                            </div>
                        </li>
                    </ul>
                    
                    </form>
                </div>
            </div>
        </div>
    </div>
    <span class="clear"></span>
</div>
</div>

<style>

    .expiry_box {
        background: none repeat scroll 0 0 gainsboro;
        border: 1px solid grey;
        border-radius: 5px;
        margin-top: 2%;
        padding: 1%;
        width: 23%;
    }


    .expiry_box input {
        width:50% !important;
        border-radius: 5px;
        border: 1px solid grey !important;
    }

</style>


<script>

var view_uploaded_document =  "<?php if ($this->lang->line('dash_view_uploaded_document') != '') echo stripslashes($this->lang->line('dash_view_uploaded_document')); else echo 'View Uploaded Document'; ?>";

var dash_please_choose_year_of_model =  "<?php if ($this->lang->line('dash_please_choose_year_of_model') != '') echo stripslashes($this->lang->line('dash_please_choose_year_of_model')); else echo 'Please choose year of model'; ?>";


    $(document).ready(function () {

        var catoptions = $("#category").html();
        $("#driver_location").change(function (e) {
            var category_list = $("#driver_location :selected").attr('data-category');
            $("#category").html(catoptions);
            if (category_list == "") {
                return;
            } else {
                var vArr = category_list.split(",");
                $("#category option").each(function (e) {
                    var optval = $(this).val();
                    if (optval != '') {
                        if ($.inArray(optval, vArr) == -1) {
                            $('#category option[value="' + optval + '"]').remove();
                        }
                    }
                });
            }
        });

        var options = $("#vehicle_type").html();
        $("#category").change(function (e) {
            var vehicle_types = $("#category :selected").attr('data-vehicle');
            $("#vehicle_type").html(options);
            if (vehicle_types == "") {
                $("#vehicle_type").html(' <option value=""><?php if ($this->lang->line('driver_choose_vehicle_type') != '') echo stripslashes($this->lang->line('driver_choose_vehicle_type')); else echo 'Please choose vehicle type'; ?></option>');
                return;
            } else {
                var vArr = vehicle_types.split(",");
                $("#vehicle_type option").each(function (e) {
                    var optval = $(this).val();
                    if (optval != '') {
                        if ($.inArray(optval, vArr) == -1) {
                            $('#vehicle_type option[value="' + optval + '"]').remove();
                        }
                    }
                });
            }
        });

        // vehicle model
        var vehicleoptions = $("#vehicle_model").html();
        $("#vehicle_maker").change(function (e) {
            var maker = $("#vehicle_maker :selected").val();
            $("#vehicle_model").html(vehicleoptions);
            if (maker == "") {
                return;
            } else {
                var type = $("#vehicle_type :selected").val();
                $("#vehicle_model").html(vehicleoptions);
                if (type == "") {
                    return;
                } else {
                    var models = maker + '_' + type;
                    updatemodelList(models);
                }
            }
        });
        $("#vehicle_type").change(function (e) {
            var type = $("#vehicle_type :selected").val();
            $("#vehicle_model").html(vehicleoptions);
            if (type == "") {
                return;
            } else {
                var maker = $("#vehicle_maker :selected").val();
                $("#vehicle_model").html(vehicleoptions);
                if (maker == "") {
                    return;
                } else {
                    var models = maker + '_' + type;
                    updatemodelList(models);
                }
            }
        });
        
        $("#vehicle_model").change(function (e) {
        var modelYrs = $("#vehicle_model :selected").attr('data-years'); 
        var option = '<option value="" data-years="">'+dash_please_choose_year_of_model+'</option>';
        if(modelYrs != ''){
            var modelYrsArr = modelYrs.split(',');
            for(var yr=0; yr < modelYrsArr.length; yr++){
                option = option+'<option>'+modelYrsArr[yr]+'</option>';
            }
        }
        $("#vehicle_model_year").html(option);
        });
    });
    function updatemodelList(model) {
        $("#vehicle_model option").each(function (e) {
            var vmodel = $(this).attr("data-vmodel");
            if (vmodel != '') {
                if (model != vmodel) {
                    $('#vehicle_model option[data-vmodel="' + vmodel + '"]').remove();
                }
            }
        });
    }
    $(document).ready(function () {
        $("#county").change(function (e) {
            var dail_code = $(this).find(':selected').attr('data-dialCode'); //.data('dialCode'); 
            $('#country_code').val(dail_code);
        });

        $(".docx").change(function (e) {
            e.preventDefault();
            var docxId = $(this).attr('id');
            var docxType = $(this).attr('data-docx');
            var docxTypeId = $(this).attr('data-docx_id');
            $("#" + docxId + "-Err").html('<img src="images/indicator.gif" />');
            var formData = new FormData($(this).parents('form')[0]);
            $.ajax({
                url: '<?php echo TRANSPORT_NAME; ?>/drivers/ajax_document_upload?docx_name=' + docxId,
                type: 'POST',
                xhr: function () {
                    var myXhr = $.ajaxSettings.xhr();
                    return myXhr;
                },
                success: function (data) {
                    if (data.err_msg == 'Success') {
                        $("#" + docxId + "-Hid").val(docxType + '|:|' + data.docx_name + '|:|' + docxTypeId);
                        $("#" + docxId + "-Err").html('');
                        $("#" + docxId + "-View").attr('href', 'drivers_documents_temp/' + data.docx_name);
                        $("#" + docxId + "-View").html(view_uploaded_document);
                        //$("#"+docxId+"-Succ").html('Success');
                    } else {
                        $("#" + docxId).val('');
                        $("#" + docxId + "-Hid").val('');
                        $("#" + docxId + "-Succ").html('');
                        $("#" + docxId + "-Err").html(data.err_msg);
                    }
                },
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                dataType: "json"
            });
            return false;
        });
    });
</script>

<?php
$this->load->view(TRANSPORT_NAME.'/templates/footer.php');
?>