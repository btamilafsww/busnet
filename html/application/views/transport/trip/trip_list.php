<?php
$this->load->view(TRANSPORT_NAME.'/templates/header.php');
?>
<?php
$type = '';
if(isset($_GET['type']) && $_GET['type']!='') {
        $type=$_GET['type'];
}
$dialcode=array();

foreach ($countryList as $country) {    
     if ($country->dial_code != '') {           
            $dialcode[]=str_replace(' ', '', $country->dial_code);
     }
}
 
asort($dialcode);
$dialcode=array_unique($dialcode);                                    
?>

<script>
$(document).ready(function(){
        $vehicle_category='';
        $country='';
        <?php  if($type == 'vehicle_type' && isset($_GET['vehicle_category'])) { ?>
        $vehicle_category = "<?php echo $_GET['vehicle_category']; ?>";
        <?php }?>
        <?php  if($type == 'mobile_number' &&  isset($_GET['country'])) {?>
        $country = "<?php echo $_GET['country']; ?>";
        <?php }?>
        if($vehicle_category != ''){
                $('.vehicle_category').css("display","inline");
                $('#filtervalue').css("display","none");
                $('#filtervalue').css("display","block");
                $("#country").attr("disabled", true);
        }
        if($country != ''){
                $('#country').css("display","inline");
                $('.vehicle_category').attr("disabled", true);      
        }
        $("#filtertype").change(function(){
                $filter_val = $(this).val();
                $('#filtervalue').val('');
                $('.vehicle_category').css("display","none");
                $('#filtervalue').css("display","inline");
                $('#country').css("display","none");
                $("#country").attr("disabled", true);
                $(".vehicle_category").attr("disabled", true);
            
                if($filter_val == 'vehicle_type'){
                        $('.vehicle_category').css("display","inline");
                        $('#filtervalue').css("display","none");
                        $('#country').css("display","none");
                        $('.vehicle_category').prop("disabled", false);
                        $("#country").attr("disabled", true);
                }
                if($filter_val == 'mobile_number'){
                        $('#country').css("display","inline");
                        $('#country').prop("disabled", false);
                        $(".vehicle_category").attr("disabled", true);
                        $('.vehicle_category').css("display","none");
                }           
        }); 
});
</script>
<div id="content">
    <div class="grid_container">
    
    
        <?php
        $attributes = array('id' => 'display_form');
        echo form_open(TRANSPORT_NAME.'/trip/trip_list', $attributes)
        ?>
        <div class="grid_12">
            <div class="widget_wrap">
                <div class="widget_top">
                    <span class="h_icon blocks_images"></span>
                    <h6><?php echo $heading ?></h6>

                </div>
                <div class="widget_content">
                    <?php
                    if ($paginationLink != '') {
                        echo $paginationLink;
                        $tble = 'alltrip_detail_list_only';
                    } else {
                        $tble = 'trip_detail_list_only';
                    }
                    ?>

                    <table class="display" id="<?php echo $tble; ?>" width='100%'>
                        <thead>
                            <tr>
                                <th style="width:65px" class="center">
                                     <?php if ($this->lang->line('operator_s_no') != '') echo stripslashes($this->lang->line('operator_s_no')); else echo 'S.No'; ?>
                            
                                </th>
                                <th style="width:170px" class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                    <?php if ($this->lang->line('Trip_Name') != '') echo stripslashes($this->lang->line('Trip_Name')); else echo 'Trip Name'; ?>
                            
                                </th>
                                 <th class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                    <?php if ($this->lang->line('Trip_Type') != '') echo stripslashes($this->lang->line('Trip_Type')); else echo ' Trip Type'; ?>
                            
                                </th>
                                <th class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                    <?php if ($this->lang->line('Route_Name') != '') echo stripslashes($this->lang->line('Route_Name')); else echo 'Route Name'; ?>
                             
                                </th>
                                <th class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                    <?php if ($this->lang->line('Number_Of_Stops') != '') echo stripslashes($this->lang->line('Number_Of_Stops')); else echo 'Number Of Stops'; ?>
                            
                                </th>
                                <th class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                    <?php if ($this->lang->line('Driver_Assigned') != '') echo stripslashes($this->lang->line('Driver_Assigned')); else echo 'Driver_Assigned'; ?>
                           
                                </th>
                                 <th class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                    <?php if ($this->lang->line('Bus_Assigned') != '') echo stripslashes($this->lang->line('Bus_Assigned')); else echo 'Bus Assigned'; ?>
                             
                                </th>
                                <th class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                     <?php if ($this->lang->line('Start_Time') != '') echo stripslashes($this->lang->line('Start_Time')); else echo 'Start Time'; ?>
                            
                                </th>
                                <th class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                     <?php if ($this->lang->line('End_Time') != '') echo stripslashes($this->lang->line('End_Time')); else echo 'End_Time'; ?>
                            
                                </th>

                                <th class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                    Availability date
                            
                                </th>
                                
                                
                                <th>
                                    <?php if ($this->lang->line('admin_subadmin_action') != '') echo stripslashes($this->lang->line('admin_subadmin_action')); else echo 'Action'; ?>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 0;

                           
                            if ($bus_trip_list->num_rows() > 0) {
                            
                                foreach ($bus_trip_list->result() as $row) { $i++;
                                    ?>
                                    <tr style="border-bottom: 1px solid #dddddd !important;">
                                        <td class="center tr_select ">
                                            <?php echo $i; ?>
                                        </td>
                                        <td class="center">
                                            <?php echo $row->trip_name; ?>
                                        
                                        </td>
                                        
                                                            
                                        <td class="center" style="width:70px;">
                                            <?php echo $row->trip_type; ?>
                                        </td>       
                                        <td class="center" style="width:200px;">
                                             <?php echo $row->route_details['route_name']; ?>
                                        </td>                               
                                        <td class="center" style="width:50px;">
                                            <?php echo sizeof($row->route_details['waypoints_details']); ?>
                                        </td>

                                       <td class="center" style="width:100px;">
                                          <?php 

                                            $driver_Data = $this->driver_model->get_all_details(DRIVERS, array('_id' => new MongoDB\BSON\ObjectId($row->driver_id)))->result();


                                            echo  $driver_Data[0]->driver_name;

                                          ?>
                                       </td>




                                       <td class="center" style="width:20px;">
                                          <?php 

                                            $bus_Data = $this->bus_details_model->get_all_details(BUS_DETAILS, array('_id' => new MongoDB\BSON\ObjectId($row->bus_id)))->result();


                                            if(isset($bus_Data[0]->vehicle_number)){ echo $bus_Data[0]->vehicle_number;}else{echo "-";};

                                          ?>
                                       </td>







                                        <td class="center" style="width:100px;">
                                            <?php
                                           echo  date("g:i a", strtotime( $row->start_time)); 
                                            ?>
                                        </td>


                                        <td class="center" style="width:100px;">
                                            <?php
                                          echo  date("g:i a", strtotime( $row->end_time)); 
                                            ?>
                                        </td>

                                        <td class="center" style="width:100px;">
                                            <?php
                                          echo  date('d/m/Y',$row->trip_start_date->sec) ." to ". date('d/m/Y',$row->trip_end_date->sec) ; 
                                            ?>
                                        </td>    
                                     
                                        <td class="center action-icons-wrap" style="width:140px;">
                                        
                                          <span>
                                                    <a class="action-icons c-delete" href="<?php echo TRANSPORT_NAME; ?>/trip/delete_trip/<?php echo $row->_id;?>" title="<?php if ($this->lang->line('admin_common_delete') != '') echo stripslashes($this->lang->line('admin_common_delete')); else echo 'Delete'; ?>">
                                                            <?php if ($this->lang->line('admin_common_delete') != '') echo stripslashes($this->lang->line('admin_common_delete')); else echo 'Delete'; ?>
                                                    </a>
                                            </span>
                                            <span><a class="action-icons c-edit" href="<?php echo TRANSPORT_NAME; ?>/trip/edit_trip_bus/<?php echo $row->_id; ?>" title="<?php if ($this->lang->line('admin_common_edit') != '') echo stripslashes($this->lang->line('admin_common_edit')); else echo 'Edit'; ?>"><?php if ($this->lang->line('admin_common_edit') != '') echo stripslashes($this->lang->line('admin_common_edit')); else echo 'Edit'; ?></a></span>
    
                                            
                                        
                                            <span><a class="action-icons c-suspend" href="<?php echo TRANSPORT_NAME; ?>/trip/view_trip/<?php echo $row->_id; ?>" title="<?php if ($this->lang->line('admin_common_view') != '') echo stripslashes($this->lang->line('admin_common_view')); else echo 'View'; ?>"></a></span>                                          
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                 <th style="width:65px" class="center">
                                     <?php if ($this->lang->line('operator_s_no') != '') echo stripslashes($this->lang->line('operator_s_no')); else echo 'S.No'; ?>
                            
                                </th>
                                <th style="width:170px" class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                    <?php if ($this->lang->line('Trip_Name') != '') echo stripslashes($this->lang->line('Trip_Name')); else echo 'Trip Name'; ?>
                            
                                </th>
                                 <th class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                    <?php if ($this->lang->line('Trip_Type') != '') echo stripslashes($this->lang->line('Trip_Type')); else echo ' Trip Type'; ?>
                            
                                </th>
                                <th class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                    <?php if ($this->lang->line('Route_Name') != '') echo stripslashes($this->lang->line('Route_Name')); else echo 'Route Name'; ?>
                             
                                </th>
                                <th class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                    <?php if ($this->lang->line('Number_Of_Stops') != '') echo stripslashes($this->lang->line('Number_Of_Stops')); else echo 'Number Of Stops'; ?>
                            
                                </th>
                                <th class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                    <?php if ($this->lang->line('Driver_Assigned') != '') echo stripslashes($this->lang->line('Driver_Assigned')); else echo 'Driver_Assigned'; ?>
                           
                                </th>
                                 <th class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                    <?php if ($this->lang->line('Bus_Assigned') != '') echo stripslashes($this->lang->line('Bus_Assigned')); else echo 'Bus Assigned'; ?>
                             
                                </th>
                                <th class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                     <?php if ($this->lang->line('Start_Time') != '') echo stripslashes($this->lang->line('Start_Time')); else echo 'Start Time'; ?>
                            
                                </th>
                                <th class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                     <?php if ($this->lang->line('End_Time') != '') echo stripslashes($this->lang->line('End_Time')); else echo 'End_Time'; ?>
                            
                                </th>
                                 <th class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                     Availability date
                            
                                </th>    
                                
                                <th>
                                    <?php if ($this->lang->line('admin_subadmin_action') != '') echo stripslashes($this->lang->line('admin_subadmin_action')); else echo 'Action'; ?>
                                </th>
                            </tr>
                        </tfoot>
                    </table>

                    <?php
                    if ($paginationLink != '') {
                        echo $paginationLink;
                    }
                    ?>

                </div>
            </div>
        </div>
        <input type="hidden" name="statusMode" id="statusMode"/>
        <input type="hidden" name="SubAdminEmail" id="SubAdminEmail"/>
        </form> 
    </div>
    <span class="clear"></span>
</div>
</div>
<style>                                     
        .b_warn {
            background: orangered none repeat scroll 0 0;
            border: medium none red;
        }
        
        .filter_widget .btn_30_light {
            margin: -11px;
            width: 83%;
        }
</style>
<?php
$this->load->view(TRANSPORT_NAME.'/templates/footer.php');
?>