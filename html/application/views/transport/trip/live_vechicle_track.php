<?php
$this->load->view(TRANSPORT_NAME.'/templates/header.php');  

$center_data=explode(",",$this->data['center']);

 $trip_data=$trip_data->result();

?> 

<style>
    
#map {
    min-height: 283px;
    width: 100%;
}

</style>





<div id="content" class="admin-settings edit-global-set add_drive_catagory">
    <div class="grid_container">
            <div class="grid_12" >
                <div class="widget_wrap">
                    <div class="widget_top">
                        <span class="h_icon list"></span>
                        <h6><?php echo $heading; ?></h6>
                        <div id="widget_tab"></div>
                    </div>
                    <div class="widget_content">

                    <div class="assign_padder" style="border-top: #e84c3d solid 3px;">


                      <div class="row">

                              <div class="col-md-6 col-lg-6 col-sm-12" >  
            
                             <h3 style="width:100% !important;margin-top: unset;border-bottom: unset;">Trip :   <?php echo $trip_data[0]->trip_name; ?></h3>

                              </div>
                                              
                               <div class="col-md-6 col-lg-6 col-sm-12" > 
                               
                             
                             <p  style="cursor: pointer;float:right;font-weight: bolder; " onclick="live_driver_location();time_reloader();"><span>Driver location refreshing in </span><span style="margin-left: 2px;margin-right:2px;" id="timmer">30</span><span>seconds</span>   <span > <i class="fa fa-refresh fa-3" aria-hidden="true"></i> </span></p>
                                </div>
                       </div>         

                    <!-- map view -->


                    <div id="map"></div>
                    <br>
                    <ul class="nav nav-tabs">
                      <li class="active tab_set" onclick="document.getElementById('student_details_tab').style.display='none';document.getElementById('route_details_tab').style.display='block';"><a data-toggle="tab" href="#route_details_tab">Route Details</a></li>
                      
                      <li class="tab_set" onclick="document.getElementById('route_details_tab').style.display='none';document.getElementById('student_details_tab').style.display='block';"><a data-toggle="tab" href="#student_details_tab">Student Details</a></li>
                     
                    </ul>
                     <form id="book_trip" method="POST" class="form_container left_label" >
                   
                    <div id="route_details_tab" class="tab-pane fade in active">  
                    <div class="left_base_trip" >    
                    <div class="base_booking_trip" style="padding:unset">
                        <br>

                           
                        
                         
                        <ul class="trip_type_sec">

                       

                          
                                
                                <li>
                                    <div class="form_grid_12">
                                        <div class="form_input">
                                        <label class="field_title">Route</label>
                                          

                                              <?php
                                              foreach ($route_list->result() as $key => $value) {
                                              

                                                   ?>     


                                                   <?php if($value->route_name == $trip_data[0]->route_details['route_name'] ) { ?>


                                                   <p> <?php echo  $value->route_name; ?></p>

                                                   

                                                
                                            <?php
                                                }
                                              }
                                              ?>


                                         
                                           
                                        </div>
                                    </div>
                                </li>
                                
                                <li id="pickup_details">
                                    <div class="form_grid_12">
                                        <div class="form_input">
                                        <label class="field_title">Trip Start Time</label>
                                           <p><?php echo $trip_data[0]->start_time; ?></p>
                                        </div>
                                    </div>
                                </li>   
                                    
                                <li >
                                    <div class="form_grid_12">
                                        <div class="form_input">
                                         <label class="field_title">Driver</label>
                                        
                                              <?php
                                              foreach ($driver_list->result() as $key => $value) {
                                                
                                               ?>     

                                                  <?php if($value->_id == $trip_data[0]->driver_id ) { ?>



                                               <p> <?php echo  $value->driver_name; ?></p>




                                               <?php
                                                    }
                                              }
                                              ?>


                                           
                                        </div>
                                    </div>
                                </li>
                               
                                
                                <li id="pickup_details">
                                    <div class="form_grid_12">
                                        <div class="form_input">
                                        <label class="field_title">Trip Start Date</label>
                                           <p><?php echo date('d/m/Y', $trip_data[0]->trip_start_date->sec);  ?></p>
                                        </div>
                                    </div>
                                </li>   

                    
                                
                        </ul>
                        </div>

                     </div>   
                        
                        
                        <div class="rite_base_trip">
                        <div class="base_booking_trip" style="margin-top: 12px;padding:unset" >
                            <ul class="trip_type_sec">
                                <br>
                               
                                                    
                                                    
                                                    <li>
                                                        <div class="form_grid_12">
                                                            <div class="form_input">
                                                            <label class="field_title">Trip Type</label>
                                                               <?php if( $trip_data[0]->trip_type == "pickup"  ){ echo "<p>Pick up</p>"; }  ?> 

                                                               <?php if( $trip_data[0]->trip_type == "drop"  ){ echo "<p>Drop</p>"; }  ?> 
                                                               
                                                            </div>
                                                        </div>
                                                    </li>
                                                    
                                                    <li id="pickup_details">
                                                        <div class="form_grid_12">
                                                            <div class="form_input">
                                                            <label class="field_title">Trip End Time</label>
                                                             <p><?php echo $trip_data[0]->end_time; ?></p>
                                                            </div>
                                                        </div>
                                                    </li>   
                                                        
                                                    <li >
                                                        <div class="form_grid_12">
                                                            <div class="form_input">
                                                                 <label class="field_title">Bus  List</label>
                                                                         
                                                                              <?php
                                                                              foreach ($Bus_list->result() as $key => $value) {
                                                                                
                                                                               ?>     




                                                                                <?php if($value->_id == $trip_data[0]->bus_id ) { ?>


                                                                            <p> <?php echo  $value->vehicle_number; ?></p>



                                                                              

                                                                               <?php
                                                                                    }
                                                                              }
                                                                              ?>


                                                                            </select>
                                                            </div>
                                                        </div>
                                                    </li>
                                                 
                                                        
                                                  




                                
                                                      <li id="pickup_details">
                                                          <div class="form_grid_12">
                                                              <div class="form_input">
                                                              <label class="field_title">Trip End Date</label>
                                                                 <p><?php echo date('d/m/Y', $trip_data[0]->trip_end_date->sec);  ?></p>
                                                              </div>
                                                          </div>
                                                      </li>   










                                                    
                                                    
                                        
                                
                            </ul>
                            
                            
                            
                        </div>
                        <input type="hidden"  id="route_details" name="route_details"></input>

                      
                   
                    </div>
                       







                      <ul class="admin-pass">
                            <li class="change-pass">
                                    <div class="form_grid_12" style="float:unset">
                                        <div class="form_input">
                                           
                                            
                                        </div>
                                    </div>
                                </li>


                         </ul>
                   

                         <ul class="setting_stop_time" id="stop_append">

                             <h2 class="route_header_triper"> Arrival Time For Stops</h2>



                         </ul>


                     </form>
    
                      </div>


                      
                     <!-- end of route details tab -->





                      <div id="student_details_tab" class="tab-pane fade"> 

                       <br>


                       <table id="class_list_table" style="text-align: center;">
                         <tr>   
                          <th>Sl.No</th>
                          <th>Student Name</th>
                          
                          <th>Status</th>
                          <th>Class</th>
                          <th>Section</th>
                        </tr>

                        <?php 
                        $counter =1;
                        foreach ($student_details as $key => $value) {
                          ?>
                          <tr> 
                            <td><?php echo $counter; ?></td>
                            <td><?php echo $value->student_first_name; ?></td>
                            <td><?php  ///////student status
                                    $status="present";


                                    $current_date=date("Y-m-d");

                                    $status_end_date=date('Y-m-d', $value->student_att_status['to_date']->sec); 

                                    $status_start_date=date('Y-m-d', $value->student_att_status['from_date']->sec); 

                                            if( strtotime($current_date) >= strtotime($status_start_date)    &&  strtotime($current_date) <= strtotime($status_end_date))
                                            {   


                                            $status=$value->student_att_status['status'];   


                                            
                                            } 

                                            echo $status;
                                            ?></td>
                            <td>
                               <?php  
                               $condition=array('_id'=> $value->student_class);

                               $class_details = $this->student_details_model->get_all_details(CLASS_DETAILS, $condition)->result();

                               if(!empty($class_details))
                               {
                                  echo  $class_details[0]->class_name;
                               }
                               else
                               {
                                echo "-";
                               }

                               ?>
                            </td>
                            <td>
                                <?php  
                               $condition=array('_id'=> $value->student_section);

                               $section_details = $this->student_details_model->get_all_details(SECTION_DETAILS, $condition)->result();

                               if(!empty($section_details))
                               {
                                  echo  $section_details[0]->section_name;
                               }
                               else
                               {
                                echo "-";
                               }

                               ?>

                            </td>
                          </tr>
                        <?php $counter++; } ?>



                       </table>

                       </div> 




                    <ul class="admin-pass">
                            <li class="change-pass">
                                    <div class="form_grid_12" style="float:unset">
                                        <div class="form_input">
                                           
                                            <a href="<?php  echo OPERATOR_NAME; ?>/trip/<?php if(null !== $this->uri->segment(5)){echo $this->uri->segment(5); }else{ echo "vehicle_plying_list";} ?>"> <span class="btn_small btn_blue" ><span><?php if ($this->lang->line('Back_To_Trip_List') != '') echo stripslashes($this->lang->line('Back_To_Trip_List')); else echo 'Back To Trip_List'; ?></span></span></a>
                                        </div>
                                    </div>
                                </li>


                    </ul>





                     

                     <br>

                    





                     </div>

                     </div>
                    </div>
                </div>
            </div>
        </div>
        <span class="clear"></span>
</div>



                     





<script>
 
        var all_stops_list=[];




        <?php 

        $route_list_data=$route_list->result();

        
        $counter=0;
      foreach ($route_list_data as $key => $value) {

       
           ?>

                    var way_points=<?php echo json_encode($value->waypoints); ?>;

                    var final_data = {"route_id" : "<?php echo $value->_id; ?>" ,"start_point" : "<?php echo $value->startpoints[0]['start_point']; ?>", "endpoint" : "<?php echo $value->endpoints[0]['end_point']; ?>","route_name" : "<?php echo $value->route_name; ?>",way_points};

              

                    all_stops_list.push(final_data);
            

        <?php

            $counter++;
         } ?>  

  


        // console.log(all_stops_list);

      
      
      
        var route_data=[];


        var initial_data=<?php echo json_encode($trip_data[0]->route_details) ;?>
      


      //////////time of stop
      
      
        function  stop_timming()
        {

            $( "#stop_append" ).hide();
            var route_id="<?php echo $trip_data[0]->route_details["route_id"]; ?>";


             route_data=[]; 


            for(var i=0;i< all_stops_list.length;i++)
            {

                if(route_id == all_stops_list[i]["route_id"] )
                {
                    
                    route_data=all_stops_list[i];


                }


            }   


         

            var html="<h2 class='route_header_triper'> Arrival Time For Stops</h2> <br>";



           console.log(route_data);
            if( typeof route_data.length == 'undefined' || route_data.length > 0)
            {    


            var waypoints=route_data.way_points;
            
                    for(var i=0;i < waypoints.length;i++)
                    {

                     

                     



                        if(initial_data.route_id == route_data.route_id)
                         {
                            

                              html=html+"<li style='margin-bottom: 16px'><label style='margin-right: 18px;'  >"+waypoints[i].way_stop.busstop_name+"  :  </label>       <label>"+initial_data.waypoints_details[i].stop_time+"</label></li>";
                         } 
                       
                    }

            }

            if(html == "<h2 class='route_header_triper'> Arrival Time For Stops</h2> <br>")
            {

                html="<h2 class='route_header_triper'> Arrival Time For Stops</h2> <br><br><p>Please Choose a route</p>"

            }
             validation();

            $( "#stop_append" ).html(html);

             $( "#stop_append" ).show("slow");

             initMap();
        }
      
      




        /////////////stops time binding





        function bind_time()
        {

           var waypoints=route_data.way_points;
            
           var bindingstops=[];
              for(var i=0;i < waypoints.length;i++)
                    {

                      
                      var way_stops_time={"id": waypoints[i]._id.$id ,"busstop_name" : waypoints[i].way_stop.busstop_name ,"location_name" : waypoints[i].way_stop.location_name,"waypoint" : waypoints[i].way_stop.waypoint ,"stop_time" : document.getElementById(waypoints[i]._id.$id).value }
                      bindingstops.push(way_stops_time);

                      
                    } 

            var final_data_stops ={"route_id" : route_data.route_id,"route_name" : route_data.route_name,"start_point" : route_data.start_point,"endpoint": route_data.endpoint,"waypoints_details": bindingstops}  ;    


       
            document.getElementById('route_details').value=JSON.stringify(final_data_stops);

         
        }























      
      
      
$(document).ready(function() {

 setTimeout( function() { 

    validation();
     stop_timming();
  
   
     live_driver_location();
    time_reloader();
  }, 1000 );  

});


  function validation()
  {
     $.validator.setDefaults({ ignore: ":hidden:not(select)" });
    $("#book_trip").validate();

    //get_model(document.getElementById("brand_list").value);
  }
      
      
  







  //////////////////*map*/////////////////////






      var start_point_details=[];
      var end_point_details=[];
      var waypoint_details=[];



      var bus_waypoint_array=[];



      var markers = [];

      var start_point;

      var bus_stop_points=[];

      var end_point;

      var markers_stopper={};
      var map;
      var marker;
        
        var markers_end=[];



      function initMap() {





        start_point_details=[];
        end_point_details=[];
        waypoint_details=[];



        bus_waypoint_array=[];



        markers = [];
        start_point;

        bus_stop_points=[];

        end_point;

        markers_stopper={};
        map;
        marker;

        markers_end=[];


        var directionsService = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer({   suppressMarkers: true });
        var geocoder = new google.maps.Geocoder();
       map = new google.maps.Map(document.getElementById('map'), {
          zoom: 16,
          center: {lat: <?php echo $center_data[0]; ?>, lng: <?php echo $center_data[1]; ?>}
        });
        directionsDisplay.setMap(map);

       
         
        
    
        ///starting
 



      var splitted= route_data.start_point.split(",");




     var latlng = new google.maps.LatLng(splitted[0],splitted[1]);

      console.log(latlng);
     start_point=route_data.start_point;
        var icon = {

          
          url: "/component/images/Start_point.svg", // url
          scaledSize: new google.maps.Size(45,45) // scaled size
          
          };
    
         marker = new google.maps.Marker({
          position: latlng,
           icon: icon,
          
      
          map: map
        });
        markers.push(marker);


    ///ending   
     var splitted= route_data.endpoint.split(",");
     var latlng2 = new google.maps.LatLng(splitted[0],splitted[1]);
      

    
    end_point=route_data.endpoint;
    var icon2 = {
          url: "/component/images/School.svg", // url
          scaledSize: new google.maps.Size(45,45) // scaled size
          
          };
    
    
           marker = new google.maps.Marker({
              position: latlng2,
              icon: icon2,
            
              map: map
            });
            markers_end.push(marker);
            
        
    ///bus stops

  
    var count_way_pnt=0;

    var waypoints=route_data.way_points;
    for (var i=1; i < waypoints.length-1 ; i++) 
    {  
  
      var splitted= waypoints[i].way_stop.waypoint.split(",");


      var latlng= new google.maps.LatLng(splitted[0],splitted[1]);
    
        var icon = {
          url: "/component/images/Stop_point.svg", // url
          scaledSize: new google.maps.Size(45,45) // scaled size
          
          };
      
        var text=i+1;
    
      
           marker = new google.maps.Marker({
            position: latlng,
              
             label:  {text: text.toString(), color: "white"},
            id: location,
            map: map,
            title: waypoints[i].way_stop.busstop_name
          });

            
      bus_stop_points.push(waypoints[i].way_stop.waypoint)

    //  bus_waypoint_array[count_way_pnt]=[];
     // bus_waypoint_array[count_way_pnt].push(waypoints[i].way_stop.busstop_name,waypoints[i].way_stop.waypoint);

      count_way_pnt++;
    } 
  
     

     calculateAndDisplayRoute(directionsService, directionsDisplay);
    
      }
    

  
    
    
      function calculateAndDisplayRoute(directionsService, directionsDisplay) {
        var waypts = [];
       // var checkboxArray = document.getElementById('waypoints');
        for (var i = 0; i < bus_stop_points.length; i++) {
         
            waypts.push({
              location: bus_stop_points[i],
              stopover: true
            });
         
        }

        directionsService.route({
          origin: start_point,
          destination: end_point,
          waypoints: waypts,
      
          optimizeWaypoints: true,
          travelMode: 'DRIVING'
        }, function(response, status) {
    
    //////////////appending value to hidden










  
    













  //  console.log(bus_waypoint_array);
    
    
          if (status === 'OK') {
            directionsDisplay.setDirections(response);
            var route = response.routes[0];
           

          } else {
            window.alert('Directions request failed due to ' + status);
          }
        });
      }
    
    
    
    
    var live_checker=0;
    var live_checker2=0;
   
    function live_driver_location()
    {

       $.ajax({
        url: "<?php echo TRANSPORT_NAME; ?>/trip/live_driver_location", 
        method : "POST",

        data : { driver_id : "<?php echo $trip_data[0]->driver_id; ?>" },

        success: function(result)
        {
              console.log(result);
               var data_returned= JSON.parse(result);


               if(data_returned == "" )
               {
                 $.growl.warning({ location: "tc", message: 'Driver location unavailable'});
               }
              



               if(live_checker !=0)
               {  
               delMarker("live_location");
               }
               else
               {
                live_checker=1;
               }  
                  var latlng= new google.maps.LatLng(data_returned['lat'],data_returned['lon']);
                  var icon = {
                  url: "/component/images/map_vehicle.png", // url
                  scaledSize: new google.maps.Size(45,45) // scaled size

                  };



                  marker = new google.maps.Marker({
                  position: latlng,
                  icon: icon,
                  id:"live_location",
                  map: map,
                  title: "Driver"
                  });

                   if(live_checker2 ==0)
                    {
                        setTimeout( function() {    
                       var center = new google.maps.LatLng(data_returned['lat'],data_returned['lon']);
                          
                         map.panTo(center);
                       live_checker2=1 ;  

                      }, 4000 );

                  }     


                  markers["live_location"] = marker;




        }

      });



    }
    


    
  
    var delMarker = function (id) {
    markerss =markers[id]; 
    markerss.setMap(null);
   }
    


  
   var time=30;
    var intrevaler ="";

  function time_reloader()
  {
    time=30;
      
    clearInterval(intrevaler);
     

    intrevaler = setInterval( function() {

        time--;

        $('#timmer').html(time);

        if (time === 0) {

          live_driver_location();

          time_reloader();
        }    


    }, 1000 );


    
  }

    
    
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?<?php echo substr($this->data['google_maps_api_key'], 1);?>&callback=initMap">
    </script>






<?php
$this->load->view(TRANSPORT_NAME.'/templates/footer.php');
?>