<?php
$this->load->view(TRANSPORT_NAME.'/templates/header.php');  

$center_data=explode(",",$this->data['center']);

  

?> 



<style>
  
  .datepicker-orient-top
  {
  top: 201px;;
  }


</style>









<div id="content" class="admin-settings edit-global-set add_drive_catagory">
    <div class="grid_container">
            <div class="grid_12" >
                <div class="widget_wrap">
                    <div class="widget_top">
                        <span class="h_icon list"></span>
                        <h6>Add Trip</h6>
                        <div id="widget_tab"></div>
                    </div>




 <script>

                   <?php if($route_list->num_rows() == 0 )
                   { ?> 

                                    $( document ).ready(function() {
                                        validate_count();
                                    });


                                    function validate_count()
                                    {
                                        message="Create a route to start with. Would you like to create a route ?";

                                        url="<?php echo TRANSPORT_NAME;?>/bus_route/add_bus_route";

                                        return_url="<?php echo TRANSPORT_NAME;?>";

                                        return student_no_trip(message,url,return_url);
                                    }

                   <?php 
                        }
                  elseif($driver_list->num_rows() == 0 )
                   { ?> 

                                    $( document ).ready(function() {
                                        validate_count();
                                    });


                                    function validate_count()
                                    {
                                        message="Add a drive to start with. Would you like to add a driver ?";

                                        url="<?php echo TRANSPORT_NAME;?>/drivers/add_driver_form";

                                        return_url="<?php echo TRANSPORT_NAME;?>";

                                        title="No driver found";

                                        return  all_no_data(title,message,url,return_url)
                                    }

                   <?php 
                        }
                    elseif($Bus_list->num_rows() == 0 )
                     { ?> 

                                    $( document ).ready(function() {
                                        validate_count();
                                    });


                                    function validate_count()
                                    {
                                        message="Add a vehicle to start with. Would you like to add a vechile ?";

                                        url="<?php echo TRANSPORT_NAME;?>/add_bus/add_bus_new";

                                        return_url="<?php echo TRANSPORT_NAME;?>";

                                        title="No vehicle found";

                                        return  all_no_data(title,message,url,return_url)
                                    }

                   <?php 
                        }
                  ?>

               

                </script>






                    <div class="widget_content">

                    <div class="assign_padder" style="border-top: #e84c3d solid 3px;">
                  <form id="book_trip" method="POST" class="form_container left_label" action="<?php echo TRANSPORT_NAME; ?>/trip/insert_bus_trip">
                    <div class="left_base_trip">    
                    <div class="base_booking_trip" style="padding:unset">
                        <br>
                        <ul class="trip_type_sec">
                          <br>      
                                
                                <li>
                                    <div class="form_grid_12">
                                        <div class="form_input">
                                        <label class="field_title">Route<span class="req">*</span></label>
                                            <select  id="route_id" name="route_id" class="large required select_props" onChange="stop_timming()">
                                                <option value="">Select Route</option>

                                              <?php
                                              foreach ($route_list->result() as $key => $value) {
                                                
                                               ?>     

                                               <option value="<?php echo  $value->_id; ?>"> <?php echo  $value->route_name; ?></option>

                                               <?php

                                              }
                                              ?>


                                            </select>
                                           
                                        </div>
                                    </div>
                                </li>
                              
                                <li id="pickup_details">
                                    <div class="form_grid_12">
                                        <div class="form_input">
                                        <label class="field_title">Trip Start Time<span class="req">*</span></label>
                                            <!-- <input style="min-height: 42px;" placeholder="Start Time" name="start_time" id="start_time" type="time"  class="large required"  /> -->
                                        
                                            <div class="row">
                                              <div class='col-sm-6'>
                                                  <div class="form-group">
                                                      <div class='input-group date' id='datetimepicker1'>
                                                          <input onchange="check_driver_availability()" style="margin-bottom: unset;" placeholder="Start Time" name="start_time" id="start_time" type="text"  class="form-control large required"   />
                                                          <span class="input-group-addon">
                                                              <span class="glyphicon glyphicon-time"></span>
                                                          </span>
                                                      </div>
                                                  </div>
                                              </div>
                                              </div>

                                        </div>
                                    </div>
                                </li>   
                                    
                                <li >
                                    <div class="form_grid_12">
                                        <div class="form_input">
                                         <label class="field_title">Driver<span class="req">*</span></label>
                                            <select onchange="check_driver_availability()" id="driver_id" name="driver_id" class="large required select_props" onChange="">
                                                <option value="">Select Driver</option>

                                              <?php
                                              foreach ($driver_list->result() as $key => $value) {
                                                
                                               ?>     

                                               <option value="<?php echo  $value->_id; ?>"> <?php echo  $value->driver_name; ?></option>

                                               <?php

                                              }
                                              ?>


                                            </select>
                                        </div>
                                    </div>
                                </li>
                               
                               

                               <li >
                                   
                                      <div class="form_grid_12">
                                      <div class="form_input">
                                      <label class="field_title">Trip Name<span class="req">*</span></label>
                                      <input placeholder="Trip Name" name="trip_name" id="trip_name" type="text"  class="large required"  />
                                      </div>
                                      </div>
                                                             
                                     
                                </li>
                    
                                
                        </ul>
                        </div>

                     </div>   
                        
                        
                        <div class="rite_base_trip">
                        <div class="base_booking_trip" style="margin-top: 20px;padding:unset" >
                            <ul class="trip_type_sec">
                                <br>
                               
                                                    
                                                    
                                                    <li>
                                                        <div class="form_grid_12">
                                                            <div class="form_input">
                                                            <label class="field_title">Trip Type<span class="req">*</span></label>
                                                                <select  id="trip_type" name="trip_type" class="large required select_props" >
                                                                    <option value="" >Select trip type</option>

                                                                    <option value="pickup">Pickup</option>

                                                                    <option value="drop">Drop</option>
                                                                </select>
                                                               
                                                            </div>
                                                        </div>
                                                    </li>
                                                    
                                                    <li id="pickup_details">
                                                        <div class="form_grid_12">
                                                            <div class="form_input">
                                                            <label class="field_title">Trip End Time<span class="req">*</span></label>
                                                                

                                                                <div class="row">
                                                                <div class='col-sm-6'>
                                                                    <div class="form-group">
                                                                        <div class='input-group date' id='datetimepicker2'>
                                                                            <input onchange="check_driver_availability()"  style="margin-bottom: unset;" placeholder="End Time" name="end_time" id="end_time" type="text"  class="form-control large required"   />
                                                                            <span class="input-group-addon">
                                                                                <span class="glyphicon glyphicon-time"></span>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                </div>








                                                            </div>
                                                        </div>
                                                    </li>   
                                                        
                                                    <li >
                                                        <div class="form_grid_12">
                                                            <div class="form_input">
                                                                 <label class="field_title">Bus  List<span class="req">*</span></label>
                                                                            <select onchange="check_bus_availability()"  id="bus_id" name="bus_id" class="large required select_props" >
                                                                                <option value="">Select Bus</option>

                                                                              <?php
                                                                              foreach ($Bus_list->result() as $key => $value) {
                                                                                
                                                                               ?>     

                                                                               <option value="<?php echo  $value->_id; ?>"> <?php echo  $value->vehicle_number; ?></option>

                                                                               <?php

                                                                              }
                                                                              ?>


                                                                            </select>
                                                            </div>
                                                        </div>
                                                    </li>
                                                 
                                                        
                                                  
                                                   <li > 
                                                    <div class="form_grid_12">
                                                    <div class="form_input">
                                                    <label class="field_title">Trip Availability<span class="req">*</span></label>                                 

                                                            <div class="input-group input-daterange" data-provide="datepicker">
                                                            <input  style="margin-bottom: unset;" name="start_date" id="start_date"  type="text" class="form-control required " value="">
                                                            <span class="input-group-addon">to</span>
                                                            <input style="margin-bottom: unset;" name="end_date" id="end_date" type="text" class="form-control required " value="">
                                                            </div>


                                                    </div>

                                                    </div>

                                                   </li> 





                                                    
                                                    
                                        
                                
                            </ul>
                            
                            
                            
                        </div>
                        <input type="hidden"  id="route_details" name="route_details"></input>
                   
                    </div>
                       

                   

                          <ul class="admin-pass">
                            <li class="change-pass">
                                    <div class="form_grid_12" style="float:unset">
                                        <div class="form_input">
                                           
                                            
                                        </div>
                                    </div>
                                </li>


                    </ul>
                   

                         <ul class="setting_stop_time" id="stop_append">

                             <h2 class="route_header_triper">Set Arrival Time For Stops</h2>



                         </ul>



                   




                    <ul class="admin-pass">
                            <li class="change-pass">
                                    <div class="form_grid_12" style="float:unset">
                                        <div class="form_input">
                                           
                                            <span id="submit_trip" onclick="bind_time()" class="btn_small btn_blue" ><span><?php if ($this->lang->line('admin_common_submit') != '') echo stripslashes($this->lang->line('admin_common_submit')); else echo 'Submit'; ?></span></span>
                                        </div>
                                    </div>
                                </li>


                    </ul>





                     </form>


                     <br>

                        <!-- map view -->


                         <div id="map"></div>












                     </div>

                     </div>
                    </div>
                </div>
            </div>
        </div>
        <span class="clear"></span>
</div>



                     


<script>
  


   $(function () {
                $('#datetimepicker1').datetimepicker({
                     format: 'HH:mm'
                      
                });

                 $('#datetimepicker2').datetimepicker({
                     format: 'HH:mm'
                });




                 
                          
                    $('#start_date').datepicker({
                        format: 'dd/mm/yyyy'
                      });    

                         $('#end_date').datepicker({
                        format: 'dd/mm/yyyy'
                      });           

                      

            });



</script>
















<script>



var driver_checker=0;


var checkboxes =[];


var time_checker="";


    $("#start_time").blur(function(){

        var trip_starttime = document.getElementById("start_time").value;
        var trip_endtime = document.getElementById("end_time").value;


        if(trip_endtime !="")
        {
            if(trip_starttime <= trip_endtime)
            {
            }
            else
            {
              time_checker=1;
              document.getElementById("start_time").value="";
              

              $.growl.warning({ location: "tc", message: 'Trip Start Time is greter than Trip End Time'});
            }
        }
        check_driver_availability();

       

        assign_stop_times();

        check_bus_availability();

    });


    $("#end_time").blur(function(){

        var trip_starttime = document.getElementById("start_time").value;
        var trip_endtime = document.getElementById("end_time").value;

        if(trip_endtime !="")
        {
            if(trip_starttime <= trip_endtime)
            {

            }
            else
            {
              time_checker=2;
              document.getElementById("end_time").value="";
              

               $.growl.warning({ location: "tc", message: 'Trip End Time is less than Trip Start Time'});
            }
        }
        check_driver_availability();
         check_bus_availability();

    });


     $("#start_date").blur(function(){

       
        check_driver_availability(); 
         check_bus_availability();    

       

    });

      $("#end_date").blur(function(){

       
        check_driver_availability();
         check_bus_availability();     

       

    });


   function  assign_stop_times()
   { 
     checkboxes = $('#stop_append li input[type=text]');
     var trip_starttime = document.getElementById("start_time").value;
     console.log(checkboxes);
     

     checkboxes.each(function(e) {

     $(this).attr('value',trip_starttime);


    });

  }




















 
var all_stops_list=[];

<?php 
      $route_list_data=$route_list->result();

      $counter=0;
      foreach ($route_list_data as $key => $value) 
      {
 ?>
        var way_points=<?php echo json_encode($value->waypoints); ?>;

        var final_data = {"route_id" : "<?php echo $value->_id; ?>" ,"start_point" : "<?php echo $value->startpoints[0]['start_point']; ?>", "endpoint" : "<?php echo $value->endpoints[0]['end_point']; ?>","route_name" : "<?php echo $value->route_name; ?>",way_points};    

        all_stops_list.push(final_data);  
<?php
        $counter++;
      } 
?>  

  
      
      
var route_data=[];

var bus_Stops_id_all="";

      //////////time of stop      
var stopper_stops=1;      
function  stop_timming()
{
        if(stopper_stops ==1 )
        {
          stopper_stops=0;
        }
        else
        {  
            $( "#stop_append" ).hide();
            var route_id=document.getElementById("route_id").value;

            route_data=[]; 

            for(var i=0;i< all_stops_list.length;i++)
            {
                if(route_id == all_stops_list[i]["route_id"] )
                {                    
                    route_data=all_stops_list[i];
                }
            }            

            var html="<h2 class='route_header_triper'>Set Arrival Time For Stops</h2> <br>";
           
            console.log(route_data);            

            if( typeof route_data.length == 'undefined' || route_data.length > 0)
            {    
              var waypoints=route_data.way_points;
            
              for(var i=0;i < waypoints.length;i++)
              {                
                  html=html+"<li style='margin-bottom: 16px;width:34%'><label class='margine_sider'  >"+waypoints[i].way_stop.busstop_name+" </label>  <div class='input-group date datetimepicker' > <input id='"+waypoints[i]._id.$id+"' name='no_need["+i+"]' class='required form-control' value='' type='text'></input> <span class='input-group-addon'><span class='glyphicon glyphicon-time'></span></span></div></li><br>";

                  bus_Stops_id_all=bus_Stops_id_all+','+waypoints[i]._id.$id;                  
              }

            }



            if(html == "<h2 class='route_header_triper'>Set Arrival Time For Stops</h2> <br>")
            {
                html="<h2 class='route_header_triper'>Set Arrival Time For Stops</h2> <br><p>Please Choose a route</p>"
            }
             
           

            $( "#stop_append" ).html(html);

            $( "#stop_append" ).show("slow");

            $('.datetimepicker').datetimepicker({
                  format: 'HH:mm'
              });

            assign_stop_times();
             validation();
            initMap();

         }   
}
      
        

        /////////////stops time binding//////////////

function bind_time()
{

  

                  time_checker="";

                  check_driver_availability();

                  var waypoints=route_data.way_points;
                  
                  var bindingstops=[];

   if(typeof waypoints !== 'undefined' )
    { 



                  for(var i=0;i < waypoints.length;i++)
                  {    
                    var way_stops_time={"id": waypoints[i]._id.$id ,"busstop_name" : waypoints[i].way_stop.busstop_name ,"location_name" : waypoints[i].way_stop.location_name,"waypoint" : waypoints[i].way_stop.waypoint ,"stop_time" : document.getElementById(waypoints[i]._id.$id).value }
                    bindingstops.push(way_stops_time);    
                  } 

                  var final_data_stops ={"route_id" : route_data.route_id,"route_name" : route_data.route_name,"start_point" : route_data.start_point,"endpoint": route_data.endpoint,"waypoints_details": bindingstops}  ;

                  console.log(final_data_stops);


                  document.getElementById('route_details').value=JSON.stringify(final_data_stops);

                  

                  var busroutes_id = bus_Stops_id_all.split(',');  
                  var busroutes_id_len = busroutes_id.length;

                  var get_first_time_id = busroutes_id[1];      //    Stop 1 time field id
                  var get_last_time_id = busroutes_id[busroutes_id_len-1];    //    Last Stop time field id

                  var trip_start_tm = document.getElementById('start_time').value;
                  var trip_end_tm = document.getElementById('end_time').value;



                  if(trip_start_tm != "" && trip_end_tm !="")
                  {
                      for(var i=1;i<=busroutes_id.length;i++)
                      {


                           

                        if (busroutes_id[i] != undefined && busroutes_id[i] != null)
                              {    
                                     
                                     var busstop_id = document.getElementById(busroutes_id[i]).value;

                                      if(busstop_id >= trip_start_tm && busstop_id <= trip_end_tm)
                                      {



                                                    for(var y=1;y<=busroutes_id.length;y++)
                                                    {
                                                         
                                                       if (busroutes_id[y] != undefined && busroutes_id[y] != null)
                                                        {    
                                                             var busstop_id_otherval = document.getElementById(busroutes_id[y]).value;  

                                                             if(y != i)
                                                             {
                                                                if(busstop_id == busstop_id_otherval)
                                                                {
                                                                  time_checker=2;
                                                                   $.growl.warning({ location: "tc", message: 'Stop times cannot be the same'});

                                                                   break;

                                                                }


                                                             }
                                                        } 

                                                    }



                                      }
                                      else
                                      {
                                        time_checker=3;
                                   
                                        $.growl.warning({ location: "tc", message: "Arrival Stops time within Trip Start and End time" });
                                        document.getElementById(busroutes_id[i]).value="";
                                      }


                              }        
                      }

                  }
                  else
                  {
                     

                      if(trip_start_tm == "")
                      {
                         time_checker=4;
                        $.growl.warning({ location: "tc", message: "Please enter the Trip Start Time" });
                        
                      }

                      if(trip_end_tm == "")
                      { 
                         time_checker=4;
                         $.growl.warning({ location: "tc", message: "Please enter the Trip End Time" });
                        
                      }
                  }

                  
                   if(driver_checker == 1)
                  {

                   
                     $.growl.warning({ location: "tc", message: "The selected driver is already assigned with a trip at the same time. Please change the time or select another driver to assign" });

                     time_checker="1";

                     
                  }

                  if(time_checker == "")
                  {
                    check_driver_availability_submit();
                   
                  } 


  }
  else
  {
        $.growl.warning({ location: "tc", message: "Choose a route" });

  }  


}

      
      
      
$(document).ready(function() {
    validation();
     $.validator.setDefaults({ ignore: ":hidden:not(select)" });
    $("#book_trip").validate();
});


  function validation()
  {
    $.validator.setDefaults({ ignore: ":hidden:not(select)" });
    $("#book_trip").validate();
  }
      
    



  //////////////////*map*/////////////////////






      var start_point_details=[];
      var end_point_details=[];
      var waypoint_details=[];



      var bus_waypoint_array=[];



      var markers = [];

      var start_point;

      var bus_stop_points=[];

      var end_point;

      var markers_stopper={};
      var map;
      var marker;
        
        var markers_end=[];




      function initMap() {





        start_point_details=[];
        end_point_details=[];
        waypoint_details=[];



        bus_waypoint_array=[];



        markers = [];
        start_point;

        bus_stop_points=[];

        end_point;

        markers_stopper={};
        map;
        marker;

        markers_end=[];


        var directionsService = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer({   suppressMarkers: true });
        var geocoder = new google.maps.Geocoder();
       map = new google.maps.Map(document.getElementById('map'), {
          zoom: 16,
          center: {lat: <?php echo $center_data[0]; ?>, lng: <?php echo $center_data[1]; ?>}
        });
        directionsDisplay.setMap(map);

       
         
        
    
        ///starting
 

       
     if (route_data != "") {
      var splitted= route_data.start_point.split(",");
      }
      else
      {
         var splitted="";
      }  



     var latlng = new google.maps.LatLng(splitted[0],splitted[1]);

      console.log(latlng);
     start_point=route_data.start_point;
        var icon = {

          
          url: "/component/images/Start_point.svg", // url
          scaledSize: new google.maps.Size(45,45) // scaled size
          
          };
    
         marker = new google.maps.Marker({
          position: latlng,
           icon: icon,
          
      
          map: map
        });
        markers.push(marker);


    ///ending   
     if (route_data != "") {
      var splitted= route_data.endpoint.split(",");
      }
      else
      {
         var splitted="";
      }  


    
     var latlng2 = new google.maps.LatLng(splitted[0],splitted[1]);
      

    
    end_point=route_data.endpoint;
    var icon2 = {
          url: "/component/images/School.svg", // url
          scaledSize: new google.maps.Size(45,45) // scaled size
          
          };
    
    
           marker = new google.maps.Marker({
              position: latlng2,
              icon: icon2,
            
              map: map
            });
            markers_end.push(marker);
            
        
    ///bus stops

  
    var count_way_pnt=0;

    var waypoints=route_data.way_points;

     if (route_data != "") {


                                      for (var i=1; i < waypoints.length-1 ; i++) 
                                      {  
                                    
                                        var splitted= waypoints[i].way_stop.waypoint.split(",");


                                        var latlng= new google.maps.LatLng(splitted[0],splitted[1]);
                                      
                                          var icon = {
                                            url: "/component/images/Stop_point.svg", // url
                                            scaledSize: new google.maps.Size(45,45) // scaled size
                                            
                                            };
                                      
                                      
                                        
                                             marker = new google.maps.Marker({
                                              position: latlng,
                                                icon: icon,
                                              id: location,
                                              map: map,
                                              title: waypoints[i].way_stop.busstop_name
                                            });

                                              
                                        bus_stop_points.push(waypoints[i].way_stop.waypoint)

                                      //  bus_waypoint_array[count_way_pnt]=[];
                                       // bus_waypoint_array[count_way_pnt].push(waypoints[i].way_stop.busstop_name,waypoints[i].way_stop.waypoint);

                                        count_way_pnt++;
                                      } 
                                    
                                       

                                       calculateAndDisplayRoute(directionsService, directionsDisplay);

    } 
    
      }
    

  
    
    
      function calculateAndDisplayRoute(directionsService, directionsDisplay) {
        var waypts = [];
       // var checkboxArray = document.getElementById('waypoints');
        for (var i = 0; i < bus_stop_points.length; i++) {
         
            waypts.push({
              location: bus_stop_points[i],
              stopover: true
            });
         
        }

        directionsService.route({
          origin: start_point,
          destination: end_point,
          waypoints: waypts,
      
          optimizeWaypoints: true,
          travelMode: 'DRIVING'
        }, function(response, status) {
    
    //////////////appending value to hidden










  
    













  //  console.log(bus_waypoint_array);
    
    
          if (status === 'OK') {
            directionsDisplay.setDirections(response);
            var route = response.routes[0];
           

          } else {
          

            $.growl.warning({ location: "tc", message: 'Directions request failed due to ' + status});

          }
        });
      }
    
    
    
   function check_driver_availability()
   { 



       var driver_id_check=document.getElementById("driver_id").value;

       var start_time_check=document.getElementById("start_time").value;

       var end_time_check=document.getElementById("end_time").value;

       var start_date=document.getElementById("start_date").value;
       
       var end_date=document.getElementById("end_date").value;


       


       if($.trim(driver_id_check) != "" && $.trim(start_time_check) != "" && $.trim(end_time_check) != ""  && $.trim(start_date) != "" && $.trim(end_date) != "" )
          {

                $.ajax({



                            url:'<?php echo TRANSPORT_NAME;?>/trip/driver_assigned_check',

                            method:"POST",

                            data:{driver_id:driver_id_check,start_time:start_time_check ,end_time:end_time_check,start_date:start_date,end_date:end_date },

                            success: function(result)
                            {
                                if(result ==1)
                                {
                                  
                                  driver_checker=1;

                                  $.growl.warning({ location: "tc", message: "The selected driver is already assigned with a trip at the same time. Please change the time or select another driver to assign" });
                                }
                                else
                                 {
                                  driver_checker=0;
                                 } 
                            }

                });

          }    

      }
    
   function check_bus_availability()
   { 



       var bus_id_check=document.getElementById("bus_id").value;

       var start_time_check=document.getElementById("start_time").value;

       var end_time_check=document.getElementById("end_time").value;

       var start_date=document.getElementById("start_date").value;
       
       var end_date=document.getElementById("end_date").value;


       


       if($.trim(bus_id_check) != "" && $.trim(start_time_check) != "" && $.trim(end_time_check) != ""  && $.trim(start_date) != "" && $.trim(end_date) != "" )
          {

                $.ajax({



                            url:'<?php echo TRANSPORT_NAME;?>/trip/bus_assigned_check',

                            method:"POST",

                            data:{bus_id:bus_id_check,start_time:start_time_check ,end_time:end_time_check,start_date:start_date,end_date:end_date },

                            success: function(result)
                            {
                                if(result ==1)
                                {
                                  
                                  driver_checker=1;

                                  $.growl.warning({ location: "tc", message: "The selected Bus is already assigned with a trip at the same time. Please change the time or select another Bus to assign" });
                                }
                                else
                                 {
                                  driver_checker=0;
                                 } 
                            }

                });

          }    

      }
    
    
  
    
       
   function check_driver_availability_submit()
   { 



       var driver_id_check=document.getElementById("driver_id").value;

       var start_time_check=document.getElementById("start_time").value;

       var end_time_check=document.getElementById("end_time").value;

       var start_date=document.getElementById("start_date").value;
       
       var end_date=document.getElementById("end_date").value;

        var trip_type=document.getElementById("trip_type").value;
       
       var trip_name=document.getElementById("trip_name").value;
      var bus_id_check=document.getElementById("bus_id").value;

        if(driver_id_check == "")
        {
          $.growl.warning({ location: "tc", message: "Choose a driver" });
        }

        if(start_time_check == "")
        {
           $.growl.warning({ location: "tc", message: "Choose start time" });
        }

        if(end_time_check == "")
        {
           $.growl.warning({ location: "tc", message: "Choose  end time" });
        }
            if(start_date == "")
        {
           $.growl.warning({ location: "tc", message: "Choose start date" });
        }


        if(end_date == "")
        {
           $.growl.warning({ location: "tc", message: "Choose end date" });
        }
     
        if(trip_type == "")
        {
          $.growl.warning({ location: "tc", message: "Select trip type" });
        }
        if(trip_name == "")
        {
          $.growl.warning({ location: "tc", message: "Enter trip name" });
        }

         if(bus_id_check == "")
        {
          $.growl.warning({ location: "tc", message: "Choose a bus" });
        }


       if( $.trim(trip_name) != "" && $.trim(driver_id_check) != "" && $.trim(start_time_check) != "" && $.trim(end_time_check) != ""  && $.trim(start_date) != "" && $.trim(end_date) != ""  && trip_type != "" && bus_id_check != "")
          {

                $.ajax({



                            url:'<?php echo TRANSPORT_NAME;?>/trip/driver_assigned_check',

                            method:"POST",

                            data:{driver_id:driver_id_check,start_time:start_time_check ,end_time:end_time_check,start_date:start_date,end_date:end_date },

                            success: function(result)
                            {
                                if(result ==1)
                                {
                                  
                                  driver_checker=1;

                                  $.growl.warning({ location: "tc", message: "The selected driver is already assigned with a trip at the same time. Please change the time or select another driver to assign" });
                                }
                                else
                                 {




                                                            var bus_id_check=document.getElementById("bus_id").value;

                                                            var start_time_check=document.getElementById("start_time").value;

                                                            var end_time_check=document.getElementById("end_time").value;

                                                            var start_date=document.getElementById("start_date").value;

                                                            var end_date=document.getElementById("end_date").value;





                                                            if($.trim(bus_id_check) != "" && $.trim(start_time_check) != "" && $.trim(end_time_check) != ""  && $.trim(start_date) != "" && $.trim(end_date) != "" )
                                                            {

                                                                        $.ajax({



                                                                                    url:'<?php echo TRANSPORT_NAME;?>/trip/bus_assigned_check',

                                                                                    method:"POST",

                                                                                    data:{bus_id:bus_id_check,start_time:start_time_check ,end_time:end_time_check,start_date:start_date,end_date:end_date },

                                                                                    success: function(result)
                                                                                    {
                                                                                          if(result ==1)
                                                                                          {

                                                                                          driver_checker=1;

                                                                                          $.growl.warning({ location: "tc", message: "The selected Bus is already assigned with a trip at the same time. Please change the time or select another Bus to assign" });
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                                  driver_checker=0;
                                                                                                           document.getElementById("book_trip").submit();
                                                                                    } 
                                                                                    }

                                                                        });

                                                            }    




















                                
                                 } 
                            }

                });

          }


      }
    
    
    
    
    
    
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?<?php echo substr($this->data['google_maps_api_key'], 1);?>&callback=initMap">
    </script>






<?php
$this->load->view(TRANSPORT_NAME.'/templates/footer.php');
?>