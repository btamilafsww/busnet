<?php




$this->load->view(TRANSPORT_NAME.'/templates/header.php');

?>
<div id="content" class="add-subadmin-sec subadmin_top">
		<div class="grid_container">
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon list"></span>
						<h6><?php echo $heading; ?></h6>
					</div>
					<div class="widget_content">
				<?php 
						$attributes = array('class' => 'form_container left_label', 'id' => 'addsubadmin_form');
						echo form_open(TRANSPORT_NAME,$attributes) 
					?>
	 						<ul class="inner-subadmin">
	 							
								<li class="left_admin_label add_2sub">
								<div class="form_grid_12">
									<label class="field_title"><?php if ($this->lang->line('User_Name') != '') echo stripslashes($this->lang->line('User_Name')); else echo 'User Name'; ?> <span class="req">*</span></label>
									<div class="form_input">
										<p> <?php echo $user_details[0]->admin_name; ?> </p>
									</div>
								</div>
								</li>

								<input type="hidden" name="user_id" value="<?php echo  $user_id; ?>">

								<li class="left_admin_label add_1sub">
								<div class="form_grid_12">
									<label class="field_title"><?php if ($this->lang->line('admin_subadmin_email_address') != '') echo stripslashes($this->lang->line('admin_subadmin_email_address')); else echo 'Email Address'; ?> <span class="req">*</span></label>
									<div class="form_input">
										<p> <?php echo $user_details[0]->email; ?></p>
									</div>
								</div>
								</li>

								<li class="left_admin_label">
						            <div class="form_grid_12">
						                <label class="field_title"><?php if ($this->lang->line('admin_drivers_mobile_number') != '') echo stripslashes($this->lang->line('admin_drivers_mobile_number')); else echo 'Mobile Number'; ?><span class="req">*</span></label>
						                <div class="form_input">																								
						                        
						                     
						                        
						                     <p> <?php echo $user_details[0]->mobile_number; ?></p>
						                </div>
						            </div>
						        </li>	

						       


								 <li>
                            <div class="form_grid_12">
                                <label class="field_title"><?php if ($this->lang->line('admin_subadmin_status') != '') echo stripslashes($this->lang->line('admin_subadmin_status')); else echo 'Status'; ?></label>
                                <div class="form_input">
                                    <div class="active_inactive">
                                       <?php if($user_details[0]->status == "Active" ){ echo'Active'; }else{echo "Inactive" ;} ?>
                                    </div>
                                </div>
                            </div>
                        </li>



								<li class="sel-all-management">
								
								<div style="margin-top: 20px;"></div>
								<div class="form_grid_12 manage-time-table">
									<label class="field_title"><?php if ($this->lang->line('admin_user_mangement_name') != '') echo stripslashes($this->lang->line('admin_user_mangement_name')); else echo 'User Management Name'; ?></label>
									<table border="0" cellspacing="0" cellpadding="0" width="400">
								     	<tr>
								              <td align="center" width="15%"><?php if ($this->lang->line('other_users_pages') != '') echo stripslashes($this->lang->line('other_users_pages')); else echo 'Available Pages'; ?></td>
								              
								        </tr>
								    </table>
								</div>
								<?php 

									$setted_privilages=$user_details[0]->privileges;


								   	
								foreach ($schooladminPrevs as $key => $value) {


								?>
								<div class="form_grid_12">
									<label class="field_title"><?php

									if(isset($re_namer[$key]))
									{
										echo $re_namer[$key];
									}
									else{	

									 echo $key;

									}


									  ?></label>
									<table border="0" cellspacing="0" cellpadding="0" width="400">
								     	<tr>
								     	<td align="center" width="15%">
								        	<?php



								        	 for($j=0;$j< sizeof($value); $j++) { ?>
								        	<div class="check_divs">
								        	<label><?php

								        			if(isset($re_namer[$value[$j]]))
													{
														echo $re_namer[$value[$j]];
													}
													else{	

													  echo $value[$j];

													}




								        	?></label>
								        		<span class="checkboxCon">


								        			<?php
								        			$selected="";

								        			if(isset($setted_privilages[$key]))
								        			{


								        				if(in_array($value[$j],$setted_privilages[$key]))
								        				{
								        					$selected="checked";
								        				}
								        			} 

								        			?>






									        		<input disabled  readonly <?php echo  $selected; ?> class="caseSeeker <?php echo $key;?>" type="checkbox"  value="<?php echo $value[$j];?>" />
								        		</span>
								        	</div>	
								        	
											<?php } ?>

										</td>
								        </tr>
								    </table>
								</div>
								<?php } ?>
								</li>
								
							</ul>
							<input type="hidden" name="set_pages" id="hidden_set_pages" value="">
							<ul class="last-btn-submit">
								<li>
								<div class="form_grid_12">
									<div class="form_input">
										<a href="<?php echo TRANSPORT_NAME;?>/other_users/display_user_list" class="tipLeft" title="<?php if ($this->lang->line('admin_back_to_subadmin') != '') echo stripslashes($this->lang->line('admin_back_to_subadmin')); else echo 'Go to subadmin list'; ?>"><span class="badge_style b_done btn-theme"><?php if ($this->lang->line('admin_common_back') != '') echo stripslashes($this->lang->line('admin_common_back')); else echo 'Back'; ?></span></a>
									</div>
								</div>
								</li>
							</ul>
							
						</form>
					</div>
				</div>
			</div>
		</div>
		<span class="clear"></span>
	</div>
</div>








<script>
	


var pages=<?php echo json_encode($schooladminPrevs) ?>;





console.log(pages);




function set_pages_array()
{


		var final_Array={};

		var keys = [];
		
		for(var k in pages) keys.push(k);


		for(var i=0;i< keys.length ;i++)
		{

			//alert(keys[i]);
			var checked=[];

				$('.'+keys[i]+':checkbox:checked').each(function () {


				 checked.push($(this).val());
				
				});

				var name=keys[i];

				if(checked.length > 0)
				{
					final_Array[name]=checked;
				}




		}

		document.getElementById("hidden_set_pages").value =JSON.stringify(final_Array);

		


}					











</script>








<?php 
$this->load->view(TRANSPORT_NAME.'/templates/footer.php');
?>