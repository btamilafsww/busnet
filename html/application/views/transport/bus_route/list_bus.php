<?php
$this->load->view(TRANSPORT_NAME.'/templates/header.php');
?>
<?php
$type = '';
if(isset($_GET['type']) && $_GET['type']!='') {
        $type=$_GET['type'];
}
$dialcode=array();

foreach ($countryList as $country) {    
     if ($country->dial_code != '') {           
            $dialcode[]=str_replace(' ', '', $country->dial_code);
     }
}
 
asort($dialcode);
$dialcode=array_unique($dialcode);                                    
?>

<script>
$(document).ready(function(){
        $vehicle_category='';
        $country='';
        <?php  if($type == 'vehicle_type' && isset($_GET['vehicle_category'])) { ?>
        $vehicle_category = "<?php echo $_GET['vehicle_category']; ?>";
        <?php }?>
        <?php  if($type == 'mobile_number' &&  isset($_GET['country'])) {?>
        $country = "<?php echo $_GET['country']; ?>";
        <?php }?>
        if($vehicle_category != ''){
                $('.vehicle_category').css("display","inline");
                $('#filtervalue').css("display","none");
                $('#filtervalue').css("display","block");
                $("#country").attr("disabled", true);
        }
        if($country != ''){
                $('#country').css("display","inline");
                $('.vehicle_category').attr("disabled", true);      
        }
        $("#filtertype").change(function(){
                $filter_val = $(this).val();
                $('#filtervalue').val('');
                $('.vehicle_category').css("display","none");
                $('#filtervalue').css("display","inline");
                $('#country').css("display","none");
                $("#country").attr("disabled", true);
                $(".vehicle_category").attr("disabled", true);
            
                if($filter_val == 'vehicle_type'){
                        $('.vehicle_category').css("display","inline");
                        $('#filtervalue').css("display","none");
                        $('#country').css("display","none");
                        $('.vehicle_category').prop("disabled", false);
                        $("#country").attr("disabled", true);
                }
                if($filter_val == 'mobile_number'){
                        $('#country').css("display","inline");
                        $('#country').prop("disabled", false);
                        $(".vehicle_category").attr("disabled", true);
                        $('.vehicle_category').css("display","none");
                }           
        }); 
});
</script>
<div id="content">
    <div class="grid_container">
    
        <div class="grid_12">
                <div class="">
                        <div class="widget_content">
                                <span class="clear"></span>                     
                                <div class="">
                                        <div class=" filter_wrap">
                                            
                                                </div>
                                        </div>
                                </div>
                        </div>
                </div>
        </div>
    
        <?php
        $attributes = array('id' => 'display_form');
        echo form_open(TRANSPORT_NAME.'/drivers/change_driver_status_global', $attributes)
        ?>


        <div class="grid_12">
            <div class="widget_wrap">
                <div class="widget_top">
                    <span class="h_icon blocks_images"></span>
                    <h6><?php echo $heading ?></h6>

                </div>

                
                <div class="widget_content">
                    <?php
                    if ($paginationLink != '') {
                        echo $paginationLink;
                        $tble = 'allbusListTbl';
                    } else {
                        $tble = 'busListTbl';
                    }
                    ?>

                    <table class="display" id="<?php echo $tble; ?>" width='100%'>
                        <thead>
                            <tr>
                                <th style="width:100px" class="center">
                                   S.No
                                </th>
                                <th style="width:400px" class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                  Vehicle Number
                                </th>
                                <th style="width:400px" class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                  Vehicle code
                                </th>

                                <th style="width:400px">
                                Vehicle Type
                                </th>

                                
                                <th style="width:400px">
                                Trips Assigned
                                </th>

                               

                                <th style="width:100px">Actions</th>
                               
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 0;
                            if ($bus_list->num_rows() > 0) {
                            
                               
                                foreach ($bus_list->result() as $row) { $i++;
                                    ?>
                                    <tr style="border-bottom: 1px solid #dddddd !important;">

                                        <td class="center tr_select ">
                                            <?php echo $i; ?>
                                        </td>

                                        <td class="center">
                                            <?php echo $row->vehicle_number; ?>
                                        
                                        </td>   

                                        <td class="center">
                                            <?php if(isset($row->vehicle_code)){ echo $row->vehicle_code; } ?>
                                        
                                        </td>   

                                        <td class="center">

                                            <?php 
                                                    
                                                $vehicle_type_details = $this->brand_model->get_all_details(CATEGORY, array("_id" => new MongoDB\BSON\ObjectId($row->type) ) )->result();

                                                if(!empty( $vehicle_type_details ))
                                                {
                                                    foreach ($vehicle_type_details as $key => $value_type) 
                                                    {
                                                        echo "<div>".$value_type->name."</div>";                                                       
                                                    }
                                                }
                                                else
                                                {
                                                    echo "-";
                                                
                                                }                                             

                                            ?>
                                        
                                        </td>   
                                         
                                        <td class="center">

                                            <?php                                              

                                                $route_detail = $this->bus_trip_model->get_all_details(BUS_TRIP, array("bus_id" => (String)$row->_id ) )->result();

                                                if(!empty( $route_detail ))
                                                {
                                                    foreach ($route_detail as $key => $value) 
                                                    {
                                                        echo "<div>".$value->trip_name."</div>";                                                       
                                                    }
                                                }
                                                else
                                                {
                                                    echo "No Trip Assigned";
                                                } 

                                            ?>
                                        
                                         </td>  

                                        <td class="center action-icons-wrap" style="width:10px;padding: 18px">
                                        
                                             <span>
                                                    <a class="action-icons c-delete" href="<?php echo TRANSPORT_NAME; ?>/add_bus/delete_vehicle/<?php echo $row->_id;?>" title="<?php if ($this->lang->line('admin_common_delete') != '') echo stripslashes($this->lang->line('admin_common_delete')); else echo 'Delete'; ?>">
                                                            <?php if ($this->lang->line('admin_common_delete') != '') echo stripslashes($this->lang->line('admin_common_delete')); else echo 'Delete'; ?>
                                                    </a>
                                            </span>
                                            <span style="float: right;"><a class="action-icons c-edit" href="<?php echo TRANSPORT_NAME; ?>/add_bus/edit_vehicle/<?php echo $row->_id; ?>" title="<?php if ($this->lang->line('admin_common_edit') != '') echo stripslashes($this->lang->line('admin_common_edit')); else echo 'Edit'; ?>"><?php if ($this->lang->line('admin_common_edit') != '') echo stripslashes($this->lang->line('admin_common_edit')); else echo 'Edit'; ?></a></span>
    
                                         
                                         
                                            <span><a href="<?php echo TRANSPORT_NAME; ?>/add_bus/view_bus_details/<?php echo $row->_id; ?>" style="float:unset" class="action-icons c-suspend" title="<?php if ($this->lang->line('admin_common_view') != '') echo stripslashes($this->lang->line('admin_common_view')); else echo 'View'; ?>"></a></span>                                          
                                        </td>               
                                       
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th class="center">
                                    S.No
                                </th>
                                <th>
                                   Vehicle Number
                                </th>   
                                <th>
                                   Vehicle Type
                                </th>

                                <th>
                                   Vehicle Code
                                </th>

                                <th>
                                   Trips Asigned
                                </th>                               
                                <th>Actions</th>
                            </tr>
                        </tfoot>
                    </table>

                    <?php
                    if ($paginationLink != '') {
                        echo $paginationLink;
                    }
                    ?>

                </div>
            </div>
        </div>
        <input type="hidden" name="statusMode" id="statusMode"/>
        <input type="hidden" name="SubAdminEmail" id="SubAdminEmail"/>
        </form> 
    </div>
    <span class="clear"></span>
</div>
</div>
<style>                                     
        .b_warn {
            background: orangered none repeat scroll 0 0;
            border: medium none red;
        }
        
        .filter_widget .btn_30_light {
            margin: -11px;
            width: 83%;
        }
</style>
<?php
$this->load->view(TRANSPORT_NAME.'/templates/footer.php');
?>