<?php

$center_data=explode(",",$this->data['center']);


$this->load->view(TRANSPORT_NAME . '/templates/header.php');

$data_getter=$route_detail->result();



?>
<div id="content" class="admin-settings edit-global-set add_drive_catagory">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">
                <div class="widget_top">
                    <span class="h_icon list"></span>
                    <h6><?php echo "Route View-".$data_getter[0]->route_name; ?></h6>
                </div>
                <div class="widget_content">
                  
                                    <div id="content" class="admin-settings profile_set_panel edit-global-set ">
                                            
                                         <div id="map"></div>
                                         
                                         
                                      
                                        <div class="form_container">
                                        <ul class="select_ul_route" >
                                        <div class="map_points_selector">
                                        
                                       
                                        
                                        
                                        
                                        <div id="directions-panel" style="display:none;"></div>


                                        <div>
                                			<ul id="multiple_field_binding_ul_1" class="left-contsec operator_drive_sec">
                                			</ul>
                                			<ul id="multiple_field_binding_ul_2" class="rite-contsec operator_drive_sec">
                                			</ul>                                        			
                                		</div>


                                        <br />
                                        <br />


                                    
                                        </div>                                        
                                     </ul>  
                                                
                                        </div>
                                        
                                        
                                        
                                        
                                        
                            
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                    </div>
</div>
</div>
</div>
</div>
</div>
</div>


<style>

	input.large {
	    width: 75% !important;
	    height: 2rem;
	}


	.form_container input[type="text"], input[type="email"], .form_container input[type="password"], .form_container textarea {
	    height: 47px;
	}

	.admin-settings.edit-global-set.add_drive_catagory .widget_content textarea {
	   width: 73% !important;
	}

	ul.left-contsec {
		border:none;
	}

	ul.rite-contsec {
		border:none;
	}


	

    .admin-settings.edit-global-set.add_drive_catagory ul.operator_drive_sec {
        min-height: auto;
    }
    .admin-settings li .form_grid_12 {

        min-height:92px;

    }
</style>







 <script>
 

 		var start_point_details=[];
 		var end_point_details=[];
 		var waypoint_details=[];



 		var bus_waypoint_array=[];



 		var markers = [];
		
		var start_point;
		
		var bus_stop_points=[];
		
		var end_point;
		
		var markers_stopper={};
		      var map;
			  var marker;
			  
			  var markers_end=[];
      function initMap() {
        var directionsService = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer({
																	  suppressMarkers: true
																  });
        var geocoder = new google.maps.Geocoder();
       map = new google.maps.Map(document.getElementById('map'), {
          zoom: 16,
          center: {lat: <?php echo $center_data[0]; ?>, lng: <?php echo $center_data[1]; ?>}
        });
        directionsDisplay.setMap(map);

       
         
        
		
        ///starting
		 var latlng = new google.maps.LatLng(<?php echo $data_getter[0]->startpoints[0]['start_point']; ?>);

		 console.log(latlng);


		 start_point="<?php echo $data_getter[0]->startpoints[0]['start_point']; ?>";
        var icon = {

        	
					url: "/component/images/Start_point.svg", // url
					scaledSize: new google.maps.Size(45,45) // scaled size
					
					};
		
				 marker = new google.maps.Marker({
				  position: latlng,
				   icon: icon,
				  
			
				  map: map
				});
				markers.push(marker);


		///ending		

		 var latlng2 = new google.maps.LatLng(<?php echo $data_getter[0]->endpoints[0]['end_point']; ?>);
		

		
		end_point="<?php echo $data_getter[0]->endpoints[0]['end_point']; ?>";
		var icon2 = {
					url: "/component/images/School.svg", // url
					scaledSize: new google.maps.Size(45,45) // scaled size
					
					};
		
		
					 marker = new google.maps.Marker({
						  position: latlng2,
						  icon: icon2,
						
						  map: map
						});
						markers_end.push(marker);
						
				
		///bus stops

	<?php 

		$count_way_pnt=0;

		for ($i=1; $i < sizeof($data_getter[0]->waypoints)-1 ; $i++) 
		{  
	?>
		


			var latlng<?php echo $i; ?> = new google.maps.LatLng(<?php echo $data_getter[0]->waypoints[$i]['way_stop']['waypoint']; ?>);
		
				var icon = {
					url: "/component/images/Stop_point.svg", // url
					scaledSize: new google.maps.Size(45,45) // scaled size
					
					};
		
		
			
					 marker = new google.maps.Marker({
					  position: latlng<?php echo $i; ?>,
					    icon: icon,
						id: location,
					  map: map,
					  title: "<?php echo $data_getter[0]->waypoints[$i]['way_stop']['busstop_name']; ?>"

					});

						
			bus_stop_points.push("<?php echo $data_getter[0]->waypoints[$i]['way_stop']['waypoint']; ?>")

			bus_waypoint_array[<?php echo $count_way_pnt; ?>]=[];
			bus_waypoint_array[<?php echo $count_way_pnt; ?>].push("<?php echo $data_getter[0]->waypoints[$i]['way_stop']['location_name']; ?>","<?php echo $data_getter[0]->waypoints[$i]['way_stop']['waypoint']; ?>","<?php echo $data_getter[0]->waypoints[$i]['way_stop']['busstop_name']; ?>");

<?php 
			$count_way_pnt++;
		} 
?>	
		 
		 

		 calculateAndDisplayRoute(directionsService, directionsDisplay);
		
      }
		

	
		
		
      function calculateAndDisplayRoute(directionsService, directionsDisplay) {
        var waypts = [];
       // var checkboxArray = document.getElementById('waypoints');
        for (var i = 0; i < bus_stop_points.length; i++) {
         
            waypts.push({
              location: bus_stop_points[i],
              stopover: true
            });
         
        }

        directionsService.route({
          origin: start_point,
          destination: end_point,
          waypoints: waypts,
		  
          optimizeWaypoints: true,
          travelMode: 'DRIVING'
        }, function(response, status) {
		
		//////////////appending value to hidden










	
		













	//	console.log(bus_waypoint_array);
		
		
          if (status === 'OK') {
            directionsDisplay.setDirections(response);
            var route = response.routes[0];
            var summaryPanel = document.getElementById('directions-panel');
            summaryPanel.innerHTML = '';


            for(var y=0;y<bus_waypoint_array.length;y++)
            {            	
            	var field_count = y + 1;

            	var bus_name_ids = bus_waypoint_array[y][1];

            	$("#multiple_field_binding_ul_1").append('<li style="min-height: 97px;"><div class="form_grid_12"><label class="field_title">Bus Stop Name ('+field_count+')</label><div class="form_input"><p>'+bus_waypoint_array[y][2]+'<span style="float:right;font-size: 31px;font-weight: bolder;">-</span><p></div></div></li>');

            	$("#multiple_field_binding_ul_2").append('<li style="min-height: 97px;"><div class="form_grid_12"><label class="field_title">Bus Stop Address ('+field_count+')</label><div class="form_input"><p>'+bus_waypoint_array[y][0]+'</p></div></div></li>');
            }

            // For each route, display summary information.
            for (var i = 0; i < route.legs.length; i++) {
				 



              var routeSegment = i + 1;
              summaryPanel.innerHTML += '<b >Route Segment: ' + routeSegment +
                  '</b><br>';
              summaryPanel.innerHTML +=''+ route.legs[i].start_address + ' to ';
              summaryPanel.innerHTML += route.legs[i].end_address + '<br>';
              summaryPanel.innerHTML += route.legs[i].distance.text + '<br><br>';
			   
            }
			save_alert=1;
          } else {
            window.alert('Directions request failed due to ' + status);
          }
        });
      }
	  
	  
	  
	  
	  
	 
	  
	  
	  
	
	  
	  
	  
	  
	  
	  
	  
	  
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?<?php echo substr($this->data['google_maps_api_key'], 1);?>&callback=initMap">
    </script>














<?php
$this->load->view(TRANSPORT_NAME . '/templates/footer.php');
?>