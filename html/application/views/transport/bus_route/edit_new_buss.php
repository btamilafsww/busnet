<?php
$this->load->view(TRANSPORT_NAME.'/templates/header.php');

$vehicle_details=$this->data['vehicle_details'];

?>
<div id="content" class="admin-settings add-edit-opr">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">
                <div class="widget_top">
                    <span class="h_icon list"></span>
                    <h6><?php echo $heading; ?></h6>
                    <div id="widget_tab">
                    </div>
                </div>
                <div class="widget_content chenge-pass-base">
                    <form class="form_container left_label" action="<?php echo TRANSPORT_NAME;?>/add_bus/update_bus_details" id="addeditmodel_form" method="post" enctype="multipart/form-data">
                        <div>
                       <ul class="leftsec-contsec">
                      

						<li> 
						<div class="form_grid_12">
						<label class="field_title"><?php if ($this->lang->line('admin_make_and_model_model_list_model_type') != '') echo stripslashes($this->lang->line('admin_make_and_model_model_list_model_type')); else echo 'Model Type'; ?><span class="req">*</span></label>
							<div class="form_input model_type">
								<select id="model_type" class="chzn-select required" name="type"  style="display: none;" data-placeholder="Select Model Type">
									<?php if ($typeList->num_rows() > 0) { ?>
										<?php foreach ($typeList->result() as $type) { ?>
											<option <?php if($vehicle_details[0]->type == $type->_id ){ echo "selected";  } ?> value="<?php echo $type->_id; ?>" ><?php echo $type->name; ?></option>
												<?php } ?>
											<?php } ?>
								</select>
							</div>
							</div>
							</li>







						<li>
                            <div class="form_grid_12">
                                <label class="field_title">Vehicle Number<span class="req">*</span></label>
                                <div class="form_input">
                                    <input value="<?php echo $vehicle_details[0]->vehicle_number;?>" name="vehicle_number" id="vehicle_number" type="text" class="required large tipTop  Vehicle_Number_Chk" original-title="Please enter vehicle number">
                                    <label class="error_chk" id="vehicle_number_exist"></label>
                                </div>
                            </div>
                        </li>




                        <li>
                            <div class="form_grid_12">
                                <label class="field_title">Vehicle Code</label>
                                <div class="form_input">
                                    <input value="<?php if(isset($vehicle_details[0]->vehicle_code)){ echo $vehicle_details[0]->vehicle_code ;}?>"  name="vehicle_code" id="vehicle_code" type="text" class="large tipTop  Vehicle_Number_Chk" original-title="Please enter vehicle code">
                                    
                                </div>
                            </div>
                        </li>

                        <input type="hidden" name="vehicle_id" value="<?php echo $this->data['vehicle_id'];?>">

								<!-- <li>
									<div class="form_grid_12">
										<label class="field_title">Year of Vehicle</label>
										<div class="form_input">
										<textarea name="year_of_vehicle" class=""></textarea>
										</div>
									</div>
								</li> -->

                                <li>
                                    <div class="form_grid_12">
                                        <label class="field_title"><?php if ($this->lang->line('admin_common_status') != '') echo stripslashes($this->lang->line('admin_common_status')); else echo 'Status'; ?></label>
                                        <div class="form_input">
                                            <div class="active_inactive">
                                                <input type="checkbox"  name="status" id="active_inactive_active" class="active_inactive" <?php
                                               

                                                if ($form_mode) {
                                                    if ($vehicle_details[0]->status == 'on') {
                                                        echo 'checked="checked"';
                                                    }
                                                } else {
                                                    echo 'checked="checked"';
                                                }
                                                ?>/>
                                            </div>
                                        </div>
                                    </div>
                                </li>

                               </ul>
								<ul class="admin-pass">
							<li class="change-pass">
                                    <div class="form_grid_12">
                                        <div class="form_input">
                                            <input type="hidden" name="model_id" id="model_id" value="<?php
                                            if ($form_mode) {
                                                echo $modeldetails->row()->_id;
                                            }
                                            ?>"  />
                                            <button type="submit" class="btn_small btn_blue" ><span><?php if ($this->lang->line('admin_common_submit') != '') echo stripslashes($this->lang->line('admin_common_submit')); else echo 'Submit'; ?></span></button>
                                        </div>
                                    </div>
                                </li>


                            </ul>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <span class="clear"></span>
</div>
</div>
<style>
.model_type .error {
    float: right;
    margin-right: 30%;
}
label.error {
float:right!important;
}
/* .year-of-models .chzn-drop{
	width: 65px !important;
}
#year_of_model_chzn{
	width: 250px !important;
} */
.default {
	width: 650px !important;
}
</style>
<script>

$(document).ready(function() {
	$.validator.setDefaults({ ignore: ":hidden:not(select)" });
	$("#addeditmodel_form").validate();

	get_model(document.getElementById("brand_list").value);
});




function get_model(id)
{  

$.ajax({
	url: "operator/add_bus/vechile_type_list",
	method: "POST",
	data : {brand_id : id}, 
	success: function(result){

    $('#model_lister').html(result);
  		


  		}
	});

}







</script>
<?php
$this->load->view(TRANSPORT_NAME.'/templates/footer.php');
?>