<?php
$this->load->view(TRANSPORT_NAME.'/templates/header.php');


?>
<div id="content" class="admin-settings add-edit-opr">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">
                <div class="widget_top">
                    <span class="h_icon list"></span>
                    <h6><?php echo $heading; ?></h6>
                    <div id="widget_tab">
                    </div>
                </div>
              
                <div class="widget_content chenge-pass-base">
                   <br>
                               <ul class="nav nav-tabs" style="margin-top: 9px;">
                                    <li class="active tab_set" style="width:50%"><a data-toggle="tab" href="#section">Section</a></li>
                                    <li  class="tab_set" style="width:50%"><a data-toggle="tab" href="#class_stud">Class</a></li>                                
                               </ul>


                               <div class="tab-content">

                                <!-- setion tab -->

                                <div id="section" class="tab-pane fade in active">
                                    


                                       <ul class="ul_property_class">
                        <form class="form_container left_label" action="<?php echo TRANSPORT_NAME;?>/class_details/insert_class_section" id="addeditmodel_form" method="post" enctype="multipart/form-data">
                                                                <li>
                                                                    
                                                                       
                                                                        <div class="form_input" style="text-align: center">
                                                                            <input placeholder="Enter Section Name" name="section_name" id="section_name" type="text" class="required large tipTop " original-title="Please enter section name">
                                                                            <label class="error_chk" id="section_number_exist"></label>
                                                                             <button type="submit" class="btn btn-success button_extra_style" ><span>Add</span></button>
                                                                       
                                                                        </div>
                                                                    
                                                                </li>













                             </form>
                                                               

                                                                                        </ul>
                                                                                            
                                                                            <br>
                                                                            <br>
                
                                                                                                <div class="form_input table_padding">
                                                                                                    <table id="class_list_table">
                                                                                                    <tr>
                                                                                                    <th>Section List</th>
                                                                                                     <th style="width:12%">Action</th>
                                                                                                    </tr>
                                                                                                    <?php foreach ($section_list->result() as $key => $value) {                                     
                                                                                                   ?>

                                                                                                   <tr>
                                                                                                   <td><?php echo $value->section_name; ?></td>

                                                                                                   <td>
                                                                                                       
                                                                                                                                                             <span>
                                                    <a class="action-icons c-delete" href="<?php echo TRANSPORT_NAME; ?>/class_details/delete_section/<?php echo $value->_id;?>" title="<?php if ($this->lang->line('admin_common_delete') != '') echo stripslashes($this->lang->line('admin_common_delete')); else echo 'Delete'; ?>">
                                                            <?php if ($this->lang->line('admin_common_delete') != '') echo stripslashes($this->lang->line('admin_common_delete')); else echo 'Delete'; ?>
                                                    </a>
                                            </span>




                                            <span><a class="action-icons c-edit" href="<?php echo TRANSPORT_NAME; ?>/class_details/edit_section/<?php echo $value->_id; ?>" title="<?php if ($this->lang->line('admin_common_edit') != '') echo stripslashes($this->lang->line('admin_common_edit')); else echo 'Edit'; ?>"><?php if ($this->lang->line('admin_common_edit') != '') echo stripslashes($this->lang->line('admin_common_edit')); else echo 'Edit'; ?></a></span>

                                                 

                                                                                                   </td>

                                                                                                   </tr>


                                                                                                    <?php  } ?>

                                                                                                    </table>

                                                                                                    <br>
                                                                                                    <br>

                                                                                                </div>
                                                                                         





                                                                                         


                               
                                </div>

                                <!-- section tab end -->

                                <div id="class_stud" class="tab-pane fade">



                                            <div class="row" style="padding: 40px;">
                                                  <form class="form_container left_label" action="<?php echo TRANSPORT_NAME;?>/class_details/insert_class_along_section" id="addeditmodel_form_class" method="post" enctype="multipart/form-data">

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                                <label for="usr">Class Name <span class="red">*</span></label>
                                                                <input placeholder="Enter class name" type="text" name="class_name" class="form-control required" id="usr">
                                                    </div>
                                                </div>    


                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="section_sel">Sections <span class="red">*</span></label><br>
                                                        <select class="custom-select required" name="section_name_mul[]" multiple id="section_sel">
                                                             <option value="none">None</option>
                                                              <?php foreach ($section_list->result() as $key => $value) {                                     
                                                                                                   ?>

                                                                    <option value="<?php echo (string)$value->_id; ?>"><?php echo $value->section_name; ?></option>
                                                                                                  


                                                         <?php  } ?>

                                                        </select>
                                                        <p><span class="red">*</span>Press ctrl for multiple select.</p>
                                                    </div>
                                                </div>    
                                                    <div style="text-align: center">
                                                        <button type="submit" class="btn btn-success button_extra_style" ><span>Add</span></button>
                                                    </div>
                                                </form>
                                            </div>    





                                             <div class="form_input table_padding">
                                                                                                    <table id="class_list_table" style="text-align:center">
                                                                                                    <tr>
                                                                                                    <th>Class</th>
                                                                                                    <th style="width: 50%;">Sections Assigned</th>
                                                                                                    <th style="width: 12%;">Action</th>
                                                                                                    </tr>
                                                                                                    <?php foreach ($class_list->result() as $key => $value) {                                     
                                                                                                   ?>

                                                                                                   <tr>
                                                                                                   <td><?php echo $value->class_name; ?></td>



                                                                                                   <?php

                                                                                                   $myArray = []; 

                                                                                                   foreach ($value->section_ids as $key => $section_dtat) {

                                                                                                    array_push($myArray,new MongoDB\BSON\ObjectId($section_dtat));
                                                                                                     
                                                                                                   }









                                                                                                     $section_list_class = $this->class_details_model->get_all_details(SECTION_DETAILS, array("_id"=> array('$in' =>$myArray)))->result(); ?>

                                                                                                   <td>
                                                                                                       
                                                                                                    <?php foreach ($section_list_class as $key => $datas) {

                                                                                                        echo  $datas->section_name.'<br>'; 
                                                                                                       
                                                                                                    } ?>

                                                                                                   </td>


                                                                                                   <td>
                                                                                                          <span>
                                                    <a class="action-icons c-delete" href="<?php echo TRANSPORT_NAME; ?>/class_details/delete_class/<?php echo $value->_id;?>" title="<?php if ($this->lang->line('admin_common_delete') != '') echo stripslashes($this->lang->line('admin_common_delete')); else echo 'Delete'; ?>">
                                                            <?php if ($this->lang->line('admin_common_delete') != '') echo stripslashes($this->lang->line('admin_common_delete')); else echo 'Delete'; ?>
                                                    </a>
                                            </span>




                                            <span><a class="action-icons c-edit" href="<?php echo TRANSPORT_NAME; ?>/class_details/edit_class/<?php echo $value->_id; ?>" title="<?php if ($this->lang->line('admin_common_edit') != '') echo stripslashes($this->lang->line('admin_common_edit')); else echo 'Edit'; ?>"><?php if ($this->lang->line('admin_common_edit') != '') echo stripslashes($this->lang->line('admin_common_edit')); else echo 'Edit'; ?></a></span>

                                                                                                   </td>


                                                                                                   </tr>


                                                                                                    <?php  } ?>

                                                                                                    </table>

                                                                                                    <br>
                                                                                                    <br>

                                            </div>







                                  
                                </div>
                               
                              </div>
                </div>
            </div>
        </div>
    </div>
    <span class="clear"></span>
</div>

<style>



 ul.left-contsec {
         width:100% !important;
    }

    .admin-settings li .form_grid_12 {
        width:47% !important;
    }

    .left_label ul li .form_input {
        width:100% !important;        
    }


    .form_container ul li {
        width:100% !important;  
    }


    .form_input_right
    {
        float: right;
        width: 47%;
    }



ul.rite-contsec {
        width:100% !important;
    }


.admin-settings .left_label ul li .form_input input {
    width: 100% !important;
}





.model_type .error {
    float: right;
    margin-right: 30%;
}
label.error {
float:right!important;
}

.default {
    width: 650px !important;
}

th,td
{
    text-align: center;
}


</style>








<script>

$(document).ready(function() {
    $.validator.setDefaults({ ignore: ":hidden:not(select)" });
    $("#addeditmodel_form").validate();
    $("#addeditmodel_form_class").validate();

    //get_model(document.getElementById("brand_list").value);
});



$(document).ready(function() {
    if (location.hash) {
        $("a[href='" + location.hash + "']").tab("show");
    }
    $(document.body).on("click", "a[data-toggle]", function(event) {
        location.hash = this.getAttribute("href");
    });
});
$(window).on("popstate", function() {
    var anchor = location.hash || $("a[data-toggle='tab']").first().attr("href");
    $("a[href='" + anchor + "']").tab("show");
});







</script>
<?php
$this->load->view(TRANSPORT_NAME.'/templates/footer.php');
?>