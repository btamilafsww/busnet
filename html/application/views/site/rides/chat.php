<?php 
if($_SERVER['HTTP_HOST']=="192.168.1.251:8081"){
  $bosh_url= "http://192.168.1.150:5280/http-bind/";
  $domain_name='casp83';
} else{
	if (!is_file('xmpp-master/config.php')) {
	   $bosh_url= "http://67.219.149.186:5280/http-bind/";
	   $domain_name="messaging.dectar.com";
	} else {
	   require_once 'xmpp-master/config.php';
	   $bosh_url= "http://".vhost_name.":5280/http-bind/";
	   $domain_name=vhost_name;
	}
}
?>
<script>
var BOSH_SERVICE = '<?php echo $bosh_url; ?>';
var connection = null;
function onConnect(status)
{
    if (status == Strophe.Status.CONNECTING) {
	console.log('Strophe is connecting.');
    } else if (status == Strophe.Status.CONNFAIL) {
	console.log('Strophe failed to connect.');
	$('#connect').get(0).value = 'connect';
    } else if (status == Strophe.Status.DISCONNECTING) {
	console.log('Strophe is disconnecting.');
    } else if (status == Strophe.Status.DISCONNECTED) {
	console.log('Strophe is disconnected.');
	$('#connect').get(0).value = 'connect';
    } else if (status == Strophe.Status.CONNECTED) {
	console.log('Strophe is connected.');
	console.log('ECHOBOT: Send a message to ' + connection.jid + 
	    ' to talk to me.');

	connection.addHandler(onMessage, null, 'message', null, null,  null); 
	connection.send($pres().tree());
    }
}

$(document).ready(function () {
connection = new Strophe.Connection(BOSH_SERVICE);
connection.connect('<?php echo $website_tracking; ?>@<?php echo $domain_name; ?>','trackpass',onConnect);
<?php if($ride_info->row()->ride_status=='Completed')  { ?>
 connection.disconnect();	
<?php } ?>
});
</script>