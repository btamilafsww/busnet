<?php
$this->load->view('site/templates/header');
?>

<section class="rider_login_sec row log-base-sec">
   <div class="container login-center">
      <div class="col-lg-8 col-md-8 col-sm-10 col-xs-12 base-log">
         <div class="login-base">
            <h1><?php if ($this->lang->line('dash_register') != '') echo stripslashes($this->lang->line('dash_register')); else echo 'Register'; ?></h1>
            <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 left-log">
               <h3><?php if ($this->lang->line('admin_menu_operators') != '') echo stripslashes($this->lang->line('admin_menu_operators')); else echo 'Institution'; ?></h3>
               <p>
                  <?php if ($this->lang->line('find_everything_institution') != '') echo stripslashes($this->lang->line('find_everything_institution')); else echo 'Transport Software for student Safety,vehicle routing,allocation and communicating with parents.'; ?>
               </p>
               <p class="driver-login">
                  <a href="institution/signup">
                   <?php if ($this->lang->line('register_institution_login') != '') echo stripslashes($this->lang->line('register_institution_login')); else echo 'Register as Institution'; ?>
                  </a>
               </p>
               <p class="driver-sigin">
                  <span><?php if ($this->lang->line('signup_already_have_account') != '') echo stripslashes($this->lang->line('signup_already_have_account')); else echo 'Already have account?'; ?></span>
                  <a href="institution"><?php if ($this->lang->line('driver_login_ucfirst') != '') echo stripslashes($this->lang->line('driver_login_ucfirst')); else echo 'Login'; ?></a>
               </p>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 left-log rite-log">
               <h3><?php if ($this->lang->line('trasnsport_manager') != '') echo stripslashes($this->lang->line('trasnsport_manager')); else echo 'Transport Operator'; ?></h3>
               <p  style="min-height: 70px;">
                  <?php if ($this->lang->line('find_everything_transport') != '') echo stripslashes($this->lang->line('find_everything_transport')); else echo 'Manage your Fleets without any hassle with our Transport Manager App.'; ?>
               </p>
               <p class="driver-login">
                  <a href="transport/signup" style="padding: 11px 93px 11px 40px;">
                   <?php if ($this->lang->line('register_transport_login') != '') echo stripslashes($this->lang->line('register_transport_login')); else echo 'Register as Transport Operator'; ?>
                  </a>
               </p>
               <p class="driver-sigin">
                  <span><?php if ($this->lang->line('signup_already_have_account') != '') echo stripslashes($this->lang->line('signup_already_have_account')); else echo 'Already have account?'; ?></span>
                  <a href="transport"><?php if ($this->lang->line('driver_login_ucfirst') != '') echo stripslashes($this->lang->line('driver_login_ucfirst')); else echo 'Login'; ?></a>
               </p>
            </div>
         </div>
      </div>
   </div>
</section>

<?php
$this->load->view('site/templates/footer');
?>