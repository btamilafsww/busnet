<?php
$this->load->view(OPERATOR_NAME.'/templates/header.php');
/*extract($privileges);*/

$user_type = '';
if($this->input->get('user_type') != ''){
	$user_type = $this->input->get('user_type');
}
?>


<?php
$dialcode=array();
foreach ($countryList as $country) {
	if ($country->dial_code != '') {
		$dialcode[]=$country->dial_code;
	}
}
asort($dialcode);
$dialcode=array_unique($dialcode);
?>
<script>

$(document).ready(function(){
    $country='';
	<?php if(isset($_GET['country'])) {?>
	$country = "<?php echo $_GET['country']; ?>";
    <?php }?>
	$("#country").attr("disabled", true);
    if($country != ''){
		$('#country').css("display","inline");
       
		$('#country').prop("disabled", false);
	}
	$("#filtertype").change(function(){
        $('#filtervalue').val('');
		$filter_val = $(this).val();
        $('#country').css("display","none");
		$("#country").attr("disabled", true);
        if($filter_val == 'phone_number'){
			$('#country').css("display","inline");
            $('#country').prop("disabled", false);
		}
	});
	
});

</script>


<div id="content">
	<div class="grid_container">
		<div class="grid_12">
			<div class="">
					<div class="widget_content">
						<span class="clear"></span>						
						<div class="">
							<div class=" filter_wrap">
								
								
							</div>
						</div>
					</div>
			</div>
		</div>
		

		<?php 
			$attributes = array('id' => 'display_form');
			echo form_open(OPERATOR_NAME.'/users/change_user_status_global',$attributes);
		?>


			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon blocks_images"></span>
						<h6><?php echo $heading?></h6>
						
					</div>
					
					<div class="widget_content">
						<?php 
						if($user_type=='deleted') {
							if($paginationLink != '') { echo $paginationLink; $tble = 'userListTblCustomdeleted'; } else { $tble='userListTbldeleted';
							} 
						} else {
							if($paginationLink != '') { echo $paginationLink; $tble = 'userListTblCustom'; } else { $tble='userListTbl';
							} 
						}
						?>
					
						<table class="display" id="<?php echo $tble; ?>">
							<thead>
								<tr>
                               
									<th class="center">
										SI.No
									</th>
                              
									<th class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
										 <?php if ($this->lang->line('admin_users_users_list_user_name') != '') echo stripslashes($this->lang->line('admin_users_users_list_user_name')); else echo 'User Name'; ?>
									</th>
									<th class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
										<?php if ($this->lang->line('admin_users_users_list_email') != '') echo stripslashes($this->lang->line('admin_users_users_list_email')); else echo 'Email'; ?> 
									</th>
									<th class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
										 <?php if ($this->lang->line('admin_driver_list_doj') != '') echo stripslashes($this->lang->line('admin_driver_list_doj')); else echo 'DOJ'; ?>
									</th>
									
									
									<th class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
										<?php if ($this->lang->line('admin_subadmin_status') != '') echo stripslashes($this->lang->line('admin_subadmin_status')); else echo 'Status'; ?>
									</th>
									<th>
										 <?php if ($this->lang->line('admin_subadmin_action') != '') echo stripslashes($this->lang->line('admin_subadmin_action')); else echo 'Action'; ?>
									</th>
								</tr>
							</thead>
							<tbody>
								<?php 
								$countery=1;
								if ($usersList->num_rows() > 0){
									foreach ($usersList->result() as $row){
								?>
								<tr>
                             
									<td class="center tr_select ">
										<?php echo $countery;$countery++; ?>
									</td>
                                
									<td class="center">
										<?php echo $row->admin_name;?>
									</td>
									<td class="center">
										<?php if($isDemo){ ?>
										<?php echo $dEmail; ?>
										<?php }  else{ ?>
										<?php echo $row->email;?>
										<?php } ?>
									</td>
									
									<td class="center" style="width:150px;">
										<?php
										if (isset($row->created)) {
											echo get_time_to_string('Y-m-d', strtotime($row->created));
										}
										?>
									</td>
								
									
									
									<td class="center">
										<?php 
										$disp_status = get_language_value_for_keyword($row->status,$this->data['langCode']);

									

										if ($allPrev == '1')
										{
											if($row->status == 'Active')
											{
												$mode = 0;
											}
											elseif($row->status == 'Inactive')
											{
												$mode = 1;
											}
											else
											{
												$mode = 2;
											}

											if ($mode == '0')
											{
										?>
												<a title="<?php if ($this->lang->line('common_click_inactive') != '') echo stripslashes($this->lang->line('common_click_inactive')); else echo 'Click to inactive'; ?>" class="tip_top" href="javascript:confirm_status('<?php echo OPERATOR_NAME; ?>/users/change_user_status/<?php echo $mode;?>/<?php echo $row->_id;?>');">
													<span class="badge_style b_done"><?php echo $disp_status;?></span>
												</a>
										<?php
											}
											else if ($mode == '1')
											{ 	
										?>
												<a title="<?php if ($this->lang->line('common_click_active') != '') echo stripslashes($this->lang->line('common_click_active')); else echo 'Click to active'; ?>" class="tip_top" href="javascript:confirm_status('<?php echo OPERATOR_NAME; ?>/users/change_user_status/<?php echo $mode;?>/<?php echo $row->_id;?>')">
													<span class="badge_style"><?php echo $disp_status;?></span>
												</a>
										<?php 
											}
											else
											{ 
										?>
												<span class="badge_style"><?php echo $disp_status;?></span>
								<?php 
											}

										}
										else 
										{
								?>
											<span class="badge_style b_done"><?php echo $disp_status;?></span>
									<?php
										 }
									?>

									</td>

									<td class="center action-icons-wrap">


								

										
											
										
									
											
											<span><a class="action-icons c-key" href="<?php echo OPERATOR_NAME; ?>/other_users/change_password_form/<?php echo $row->_id;?>" original-title="<?php if ($this->lang->line('admin_user_change_password') != '') echo stripslashes($this->lang->line('admin_user_change_password')); else echo 'Change Password'; ?>"><?php if ($this->lang->line('admin_user_change_password') != '') echo stripslashes($this->lang->line('admin_user_change_password')); else echo 'Change Password'; ?></a></span>
										
											<span><a class="action-icons c-edit" href="<?php echo OPERATOR_NAME; ?>/other_users/edit_user_form/<?php echo $row->_id;?>" title="<?php if ($this->lang->line('admin_common_edit') != '') echo stripslashes($this->lang->line('admin_common_edit')); else echo 'Edit'; ?>"><?php if ($this->lang->line('admin_subadmin_edit') != '') echo stripslashes($this->lang->line('admin_subadmin_edit')); else echo 'Edit'; ?></a></span>
									
											
											

											<span><a class="action-icons c-suspend" href="<?php echo OPERATOR_NAME; ?>/other_users/view_user/<?php echo $row->_id;?>" title="<?php if ($this->lang->line('admin_common_view') != '') echo stripslashes($this->lang->line('admin_common_view')); else echo 'View'; ?>"><?php if ($this->lang->line('admin_subadmin_view') != '') echo stripslashes($this->lang->line('admin_subadmin_view')); else echo 'View'; ?></a></span>


										
											<span><a class="action-icons c-delete" href="javascript:confirm_delete('<?php echo OPERATOR_NAME; ?>/other_users/delete_user/<?php echo $row->_id;?>','restore')" title="<?php if ($this->lang->line('admin_common_delete') != '') echo stripslashes($this->lang->line('admin_common_delete')); else echo 'Delete'; ?>"><?php if ($this->lang->line('admin_subadmin_delete') != '') echo stripslashes($this->lang->line('admin_subadmin_delete')); else echo 'Delete'; ?></a></span>
									
										
										
										
										
									</td>
								</tr>
								<?php 
									}
								}
								?>
							</tbody>
							<tfoot>
								<tr>
                               
									<th class="center">
										SI.No
									</th>
                               
									<th>
										 <?php if ($this->lang->line('admin_users_users_list_user_name') != '') echo stripslashes($this->lang->line('admin_users_users_list_user_name')); else echo 'User Name'; ?>
									</th>
									<th>
										 <?php if ($this->lang->line('admin_users_users_list_email') != '') echo stripslashes($this->lang->line('admin_users_users_list_email')); else echo 'Email'; ?>
									</th>
									
									<th>
										 <?php if ($this->lang->line('admin_driver_list_doj') != '') echo stripslashes($this->lang->line('admin_driver_list_doj')); else echo 'DOJ'; ?>
									</th>
									
									<th>
										<?php if ($this->lang->line('admin_subadmin_status') != '') echo stripslashes($this->lang->line('admin_subadmin_status')); else echo 'Status'; ?>
									</th>
									<th>
										 <?php if ($this->lang->line('admin_subadmin_action') != '') echo stripslashes($this->lang->line('admin_subadmin_action')); else echo 'Action'; ?>
									</th>
								</tr>
							</tfoot>
						</table>
						
							<?php if($paginationLink != '') { echo $paginationLink; } ?>
						
					</div>
				</div>
			</div>
			<input type="hidden" name="statusMode" id="statusMode"/>
			<input type="hidden" name="SubAdminEmail" id="SubAdminEmail"/>
		</form>	
	</div>
	<span class="clear"></span>
	<style>
	.filter_widget .btn_30_light {
		margin: -11px;
		width: 85%;
	}
	</style>
</div>
</div>

<?php 
$this->load->view(OPERATOR_NAME.'/templates/footer.php');
?>