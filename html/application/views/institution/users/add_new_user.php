<?php




$this->load->view(OPERATOR_NAME.'/templates/header.php');

?>
<div id="content" class="add-subadmin-sec subadmin_top">
		<div class="grid_container">
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon list"></span>
						<h6><?php echo $heading; ?></h6>
					</div>
					<div class="widget_content">
					<?php 
						$attributes = array('class' => 'form_container left_label', 'id' => 'addsubadmin_form');
						echo form_open(OPERATOR_NAME.'/other_users/insert_user',$attributes) 
					?>
	 						<ul class="inner-subadmin">
	 							
								<li class="left_admin_label add_2sub">
								<div class="form_grid_12">
									<label class="field_title"><?php if ($this->lang->line('User_Name') != '') echo stripslashes($this->lang->line('User_Name')); else echo 'User Name'; ?> <span class="req">*</span></label>
									<div class="form_input">
										<input name="admin_name" id="admin_name" type="text"  class="required large tipTop alphanumeric" title="<?php if ($this->lang->line('admin_please_enter_user') != '') echo stripslashes($this->lang->line('admin_please_enter_user')); else echo 'Please enter the  user name'; ?>"/> 
									</div>
								</div>
								</li>

								<li class="left_admin_label add_1sub">
								<div class="form_grid_12">
									<label class="field_title"><?php if ($this->lang->line('admin_subadmin_email_address') != '') echo stripslashes($this->lang->line('admin_subadmin_email_address')); else echo 'Email Address'; ?> <span class="req">*</span></label>
									<div class="form_input">
										<input name="email" id="email" type="text"  class="required large tipTop" title="<?php if ($this->lang->line('admin_please_enter_email') != '') echo stripslashes($this->lang->line('admin_please_enter_email')); else echo 'Please enter the email'; ?>"/>
									</div>
								</div>
								</li>

								<li class="left_admin_label">
						            <div class="form_grid_12">
						                <label class="field_title"><?php if ($this->lang->line('admin_drivers_mobile_number') != '') echo stripslashes($this->lang->line('admin_drivers_mobile_number')); else echo 'Mobile Number'; ?><span class="req">*</span></label>
						                <div class="form_input">																								
						                        
						                     
						                        
						                        
						                        <input name="mobile_number" placeholder="<?php if ($this->lang->line('admin_drivers_mobile_number') != '') echo stripslashes($this->lang->line('admin_drivers_mobile_number')); else echo 'Mobile Number'; ?>.." id="mobile_number" type="text"  class="required large tipTop phoneNumber" title="<?php if ($this->lang->line('driver_enter_mobile_number') != '') echo stripslashes($this->lang->line('driver_enter_mobile_number')); else echo 'Please enter the mobile number'; ?>" maxlength="20"  value=""/>
						                </div>
						            </div>
						        </li>	

						        <li class="left_admin_label add_3sub">
								<div class="form_grid_12">
									<label class="field_title"><?php if ($this->lang->line('admin_subadmin_password') != '') echo stripslashes($this->lang->line('admin_subadmin_password')); else echo 'Password'; ?> <span class="req">*</span></label>
									<div class="form_input">
										<input name="admin_password" id="admin_password" type="password"  class="required large tipTop" title="<?php if ($this->lang->line('admin_please_enter_password') != '') echo stripslashes($this->lang->line('admin_please_enter_password')); else echo 'Please Enter the Password'; ?>"/>
									</div>
								</div>
								</li>


								 <li>
                            <div class="form_grid_12">
                                <label class="field_title"><?php if ($this->lang->line('admin_subadmin_status') != '') echo stripslashes($this->lang->line('admin_subadmin_status')); else echo 'Status'; ?></label>
                                <div class="form_input">
                                    <div class="active_inactive">
                                        <input type="checkbox"  name="status" checked="checked" id="active_inactive_active" class="active_inactive"/>
                                    </div>
                                </div>
                            </div>
                        </li>



								<li class="sel-all-management">
								<div class="form_grid_12">
									<label class="field_title"></label>
									<div id="uniform-undefined" class="form_input checker focus">
										<span class="" style="float:left;"><input type="checkbox" class="checkbox" id="selectallseeker" /></span><label style="float:left;margin:5px;"><?php if ($this->lang->line('admin_subadmin_select_all') != '') echo stripslashes($this->lang->line('admin_subadmin_select_all')); else echo 'Select all'; ?></label>
									</div>
								</div>
								<div style="margin-top: 20px;"></div>
								<div class="form_grid_12 manage-time-table">
									<label class="field_title"><?php if ($this->lang->line('admin_user_mangement_name') != '') echo stripslashes($this->lang->line('admin_user_mangement_name')); else echo 'User Management Name'; ?></label>
									<table border="0" cellspacing="0" cellpadding="0" width="400">
								     	<tr>
								              <td align="center" width="15%"><?php if ($this->lang->line('other_users_pages') != '') echo stripslashes($this->lang->line('other_users_pages')); else echo 'Available Pages'; ?></td>
								              
								        </tr>
								    </table>
								</div>
								<?php 


								foreach ($schooladminPrevs as $key => $value) {


								?>
								<div class="form_grid_12">
									<label class="field_title"><?php

									if(isset($re_namer[$key]))
									{
										echo $re_namer[$key];
									}
									else{	

									 echo $key;

									}


									  ?></label>
									<table border="0" cellspacing="0" cellpadding="0" width="400">
								     	<tr>
								     	<td align="center" width="15%">
								        	<?php for($j=0;$j< sizeof($value); $j++) { ?>
								        	<div class="check_divs">
								        	<label><?php

								        			if(isset($re_namer[$value[$j]]))
													{
														echo $re_namer[$value[$j]];
													}
													else{	

													  echo $value[$j];

													}




								        	?></label>
								        		<span class="checkboxCon">
									        		<input class="caseSeeker <?php echo $key;?>" type="checkbox"  value="<?php echo $value[$j];?>" />
								        		</span>
								        	</div>	
								        	
											<?php } ?>

										</td>
								        </tr>
								    </table>
								</div>
								<?php } ?>
								</li>
								
							</ul>
							<input type="hidden" name="set_pages" id="hidden_set_pages" value="">
							<ul class="last-btn-submit">
								<li>
								<div class="form_grid_12">
									<div class="form_input">
										<button onclick="set_pages_array()" type="submit" class="btn_small btn_blue" ><span><?php if ($this->lang->line('admin_subadmin_submit') != '') echo stripslashes($this->lang->line('admin_subadmin_submit')); else echo 'Submit'; ?></span></button>
									</div>
								</div>
								</li>
							</ul>
							
						</form>
					</div>
				</div>
			</div>
		</div>
		<span class="clear"></span>
	</div>
</div>








<script>
	


var pages=<?php echo json_encode($schooladminPrevs) ?>;





console.log(pages);




function set_pages_array()
{


		var final_Array={};

		var keys = [];
		
		for(var k in pages) keys.push(k);


		for(var i=0;i< keys.length ;i++)
		{

			//alert(keys[i]);
			var checked=[];

				$('.'+keys[i]+':checkbox:checked').each(function () {


				 checked.push($(this).val());
				
				});

				var name=keys[i];

				if(checked.length > 0)
				{
					final_Array[name]=checked;
				}




		}

		document.getElementById("hidden_set_pages").value =JSON.stringify(final_Array);

		


}					











</script>








<?php 
$this->load->view(OPERATOR_NAME.'/templates/footer.php');
?>