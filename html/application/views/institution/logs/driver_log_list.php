<?php
$this->load->view(OPERATOR_NAME.'/templates/header.php');
?>
<?php
$type = '';
if(isset($_GET['type']) && $_GET['type']!='') {
		$type=$_GET['type'];
}
$dialcode=array();

foreach ($countryList as $country) {	
	 if ($country->dial_code != '') {			
			$dialcode[]=str_replace(' ', '', $country->dial_code);
	 }
}
 
asort($dialcode);
$dialcode=array_unique($dialcode);                                    
?>

<style>
 .center
 {
    text-align: center !important;
 }   

</style>

<div id="content">
    <div class="grid_container">
	
		<div class="grid_12">
				<div class="">
						<div class="widget_content">
								<span class="clear"></span>						
								<div class="">
										<div class=" filter_wrap">
												<div class="widget_top filter_widget">
												
														<h6>Log filter</h6>
														<div class="btn_30_light">	
														<form method="get" id="filter_form" action="<?php echo OPERATOR_NAME; ?>/logs/<?php echo $todays_log; ?>" accept-charset="UTF-8">
																<select class="form-control" id="filtertype" name="type" >
																		
																		<option value="driver_name" data-val="driver_name" selected>Driver name</option>
																																			
																</select>
																<input name="value" id="filtervalue" type="text"  class="tipTop" title="<?php if ($this->lang->line('driver_enter_keyword') != '') echo stripslashes($this->lang->line('driver_enter_keyword')); else echo 'Please enter keyword'; ?>" value="<?php if(isset($value)) echo $value; ?>" placeholder="<?php if ($this->lang->line('driver_enter_keyword') != '') echo stripslashes($this->lang->line('driver_enter_keyword')); else echo 'Please enter keyword'; ?>" />
																
																
																<button type="submit" class="tipTop filterbtn"  original-title="<?php if ($this->lang->line('driver_enter_keyword_filter') != '') echo stripslashes($this->lang->line('driver_enter_keyword_filter')); else echo 'Select filter type and enter keyword to filter'; ?>">
																		<span class="icon search"></span><span class="btn_link"><?php if ($this->lang->line('admin_drivers_filter') != '') echo stripslashes($this->lang->line('admin_drivers_filter')); else echo 'Filter'; ?></span>
																</button>
																<?php if(isset($filter) && $filter!=""){ ?>
																<a href="<?php echo OPERATOR_NAME; ?>/logs/<?php echo $todays_log; ?>" class="tipTop filterbtn" original-title="<?php if ($this->lang->line('admin_notification_remove_filter') != '') echo stripslashes($this->lang->line('admin_notification_remove_filter')); else echo 'Remove Filter'; ?>">
																		<span class="icon delete_co"></span>
																</a>
																<?php } ?>
														</form>
														</div>
												</div>
										</div>
								</div>
						</div>
				</div>
		</div>
	
		
        <div class="grid_12">
            <div class="widget_wrap">
                <div class="widget_top">
                    <span class="h_icon blocks_images"></span>
                    <h6><?php echo $heading ?></h6>

                </div>
                <div class="widget_content">
                    <?php
                    if ($paginationLink != '') {
                        echo $paginationLink;
                        $tble = 'all_log_list';
                    } else {
                        $tble = 'log_list';
                    }
                    ?>

                    <table class="display" id="<?php echo $tble; ?>" width='100%'>
                        <thead>
                            <tr>
                                <th style="width:65px;text-align: center;" class="center">
                                    <?php if ($this->lang->line('operator_s_no') != '') echo stripslashes($this->lang->line('operator_s_no')); else echo 'S.No'; ?>
                                </th>
                                <th style="width:170px;text-align: center;" class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                    Driver name
                                </th>  
                                <th style="width:170px;text-align: center;" class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                   Trip name
                                </th>  
                                                           
                                <th class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                     Driver notification
                                </th>

                                <th class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                   Sent from location
                                </th>
                               	<th class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                   Sent at
                                </th>

                                <th class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                   Track
                                </th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 0;
                            if ($driver_logs->num_rows() > 0) {
							
                                foreach ($driver_logs->result() as $row) { $i++;
                                    ?>
                                    <tr style="border-bottom: 1px solid #dddddd !important;">
                                        <td class="center tr_select ">
                                            <?php echo $i; ?>
                                        </td>

                                        <td class="center">
                                            <?php echo $row->driver_name; ?>
											
                                        </td>
										
										
                                        <td class="center" style="width:70px;">
                                            <?php echo $row->trip_name; ?>
                                        </td>


                                 		<td class="center" style="width:116px;">
                                 			
 											<?php echo "The trip - ".$row->trip_name." will be delayed by ". $row->delay_message; ?>

                                 		</td>
									
                                    
                                       

                                         <td class="center" style="width:30px;">
                                             <?php echo $row->send_from_address; ?>
                                        </td>

                                        <td class="center" style="width:30px;">
                                             <?php echo date('d-m-Y h:i:s',$row->notified_time->toDateTime()->getTimestamp()); ?>
                                        </td>

                                        <td class="center" style="width:30px;">
                                            <div>
                                                <a href="<?php echo OPERATOR_NAME; ?>/logs/live_track/<?php echo $row->driver_id; ?>/<?php echo $todays_log; ?>">
                                           <img src="/images/site/map_ion.png" height="35" width="35">

                                                </a>
                                            </div>
                                        </td>
                                        
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th class="center">
                                    <?php if ($this->lang->line('operator_s_no') != '') echo stripslashes($this->lang->line('operator_s_no')); else echo 'S.No'; ?>
                                </th>
                                <th class="center">
                                   Driver name
                                </th>								
                               
                                <th class="center">
                                    Trip name
                                </th>
                              
                                <th class="center">
                                     Driver notification
                                </th>
                                <th class="center">
                                    Sent from location
                                </th>
                                <th class="center">
                                     Sent at
                                </th>
                                <th class="center">
                                     Track
                                </th>
                            </tr>
                        </tfoot>
                    </table>

                    <?php
                    if ($paginationLink != '') {
                        echo $paginationLink;
                    }
                    ?>

                </div>
            </div>
        </div>
        <input type="hidden" name="statusMode" id="statusMode"/>
        <input type="hidden" name="SubAdminEmail" id="SubAdminEmail"/>
        </form>	
    </div>
    <span class="clear"></span>
</div>
</div>
<style>										
		.b_warn {
			background: orangered none repeat scroll 0 0;
			border: medium none red;
		}
		
		.filter_widget .btn_30_light {
			margin: -11px;
			width: 83%;
		}
</style>
<?php
$this->load->view(OPERATOR_NAME.'/templates/footer.php');
?>