<?php
$this->load->view(OPERATOR_NAME.'/templates/header.php');
?>
<?php
$type = '';
if(isset($_GET['type']) && $_GET['type']!='') {
		$type=$_GET['type'];
}
$dialcode=array();

foreach ($countryList as $country) {	
	 if ($country->dial_code != '') {			
			$dialcode[]=str_replace(' ', '', $country->dial_code);
	 }
}
 
asort($dialcode);
$dialcode=array_unique($dialcode);                                    
?>



<div id="content">
    <div class="grid_container">
	
		<div class="grid_12">
				<div class="">
						<div class="widget_content">
								<span class="clear"></span>						
								<div class="">
										<div class=" filter_wrap">
												<div class="widget_top filter_widget">
												
														<h6>Log filter</h6>
														<div class="btn_30_light">	
														<form method="get" id="filter_form" action="<?php echo OPERATOR_NAME; ?>/logs/<?php echo $todays_log; ?>" accept-charset="UTF-8">
																<select class="form-control" id="filtertype" name="type" >
																		
																		<option value="student_name" data-val="student_name" selected>Student name</option>
																																			
																</select>
																<input name="value" id="filtervalue" type="text"  class="tipTop" title="<?php if ($this->lang->line('driver_enter_keyword') != '') echo stripslashes($this->lang->line('driver_enter_keyword')); else echo 'Please enter keyword'; ?>" value="<?php if(isset($value)) echo $value; ?>" placeholder="<?php if ($this->lang->line('driver_enter_keyword') != '') echo stripslashes($this->lang->line('driver_enter_keyword')); else echo 'Please enter keyword'; ?>" />
																
																
																<button type="submit" class="tipTop filterbtn"  original-title="<?php if ($this->lang->line('driver_enter_keyword_filter') != '') echo stripslashes($this->lang->line('driver_enter_keyword_filter')); else echo 'Select filter type and enter keyword to filter'; ?>">
																		<span class="icon search"></span><span class="btn_link"><?php if ($this->lang->line('admin_drivers_filter') != '') echo stripslashes($this->lang->line('admin_drivers_filter')); else echo 'Filter'; ?></span>
																</button>
																<?php if(isset($filter) && $filter!=""){ ?>
																<a href="<?php echo OPERATOR_NAME; ?>/logs/<?php echo $todays_log; ?>" class="tipTop filterbtn" original-title="<?php if ($this->lang->line('admin_notification_remove_filter') != '') echo stripslashes($this->lang->line('admin_notification_remove_filter')); else echo 'Remove Filter'; ?>">
																		<span class="icon delete_co"></span>
																</a>
																<?php } ?>
														</form>
														</div>
												</div>
										</div>
								</div>
						</div>
				</div>
		</div>
	
	
        <div class="grid_12">
            <div class="widget_wrap">
                <div class="widget_top">
                    <span class="h_icon blocks_images"></span>
                    <h6><?php echo $heading ?></h6>

                </div>
                <div class="widget_content">
                    <?php
                    if ($paginationLink != '') {
                        echo $paginationLink;
                        $tble = 'all_log_list';
                    } else {
                        $tble = 'log_list';
                    }
                    ?>

                    <table class="display" id="<?php echo $tble; ?>" width='100%'>
                        <thead>
                            <tr>
                                <th style="width:65px" class="center">
                                    <?php if ($this->lang->line('operator_s_no') != '') echo stripslashes($this->lang->line('operator_s_no')); else echo 'S.No'; ?>
                                </th>
                                <th style="width:170px" class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                    Student name
                                </th>  
                                <th style="width:170px" class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                   Class
                                </th>  
                                <th style="width:170px" class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                    Section
                                </th>                            
                                <th class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                     Parent notification
                                </th>

                                <th class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                   Notification date
                                </th>
                               	<th class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                   Sent by
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 0;
                            if ($parent_logs->num_rows() > 0) {
							
                                foreach ($parent_logs->result() as $row) { $i++;
                                    ?>
                                    <tr style="border-bottom: 1px solid #dddddd !important;">
                                        <td class="center tr_select ">
                                            <?php echo $i; ?>
                                        </td>

                                        <td class="center">
                                            <?php echo $row->student_name; ?>
											
                                        </td>
										
										
                                        <td class="center" style="width:70px;">
                                            <?php echo $row->student_class; ?>
                                        </td>


                                 		<td class="center" style="width:116px;">
                                 			
 											<?php echo $row->student_section; ?>

                                 		</td>
									
                                    
                                        <td class="center" style="width:40px;">

                                        	<?php 

                                        	 $absent_status=$row->student_att_status;
                                                 if(is_object($absent_status))
                                                     $absent_status=(array)$absent_status;
                                        	if($absent_status["status"] == "absent")
                                        	{
                                        		$from_date=date('d/m/Y',$absent_status["from_date"]->toDateTime()->getTimestamp());
                                        		$to_date=date('d/m/Y',$absent_status["to_date"]->toDateTime()->getTimestamp());

                                        		$absent_type=ucfirst($absent_status["absent_type"]);

                                        		$pickup_from_school="No";
                                        		if($absent_status["absent_half_availability"] == "pickup" )
                                        		{
                                        			$pickup_from_school="Yes";
                                        		}

                                        		echo "Child status : Absent <br> Absent type : ".$absent_type." <br>From date : ".$from_date."  <br> To date : ".$to_date."  <br> Pick from school : ".$pickup_from_school;
                                        	}
                                        	else
                                        	{
                                        		echo "Changed status from absent to present";
                                        	}


                                        	?>


                                                                            
                                        </td>

                                         <td class="center" style="width:30px;">
                                             <?php echo $row->created_date_time; ?>
                                        </td>

                                        <td class="center" style="width:30px;">
                                             <?php echo $row->parent_number; ?>
                                        </td>
                                        
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th class="center">
                                    <?php if ($this->lang->line('operator_s_no') != '') echo stripslashes($this->lang->line('operator_s_no')); else echo 'S.No'; ?>
                                </th>
                                <th>
                                    Student name
                                </th>								
                               
                                <th>
                                    Class
                                </th>
                              	<th>
                                     Section
                                </th>

                                <th>
                                    Parent notification
                                </th>
                                <th>
                                     Notification date
                                </th>
                                <th>
                                     Sent by
                                </th>
                            </tr>
                        </tfoot>
                    </table>

                    <?php
                    if ($paginationLink != '') {
                        echo $paginationLink;
                    }
                    ?>

                </div>
            </div>
        </div>
        <input type="hidden" name="statusMode" id="statusMode"/>
        <input type="hidden" name="SubAdminEmail" id="SubAdminEmail"/>
        </form>	
    </div>
    <span class="clear"></span>
</div>
</div>
<style>										
		.b_warn {
			background: orangered none repeat scroll 0 0;
			border: medium none red;
		}
		
		.filter_widget .btn_30_light {
			margin: -11px;
			width: 83%;
		}
</style>
<?php
$this->load->view(OPERATOR_NAME.'/templates/footer.php');
?>