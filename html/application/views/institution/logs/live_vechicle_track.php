<?php
$this->load->view(OPERATOR_NAME . '/templates/header.php');
?> 

<style>

    #map {
        min-height: 283px;
        width: 100%;
    }

</style>





<div id="content" class="admin-settings edit-global-set add_drive_catagory">
    <div class="grid_container">
        <div class="grid_12" >
            <div class="widget_wrap">
                <div class="widget_top">
                    <span class="h_icon list"></span>
                    <h6><?php echo $heading; ?></h6>
                    <div id="widget_tab"></div>
                </div>
                <div class="widget_content">

                    <div class="assign_padder" style="border-top: #e84c3d solid 3px;">


                        <div class="row">

                            <div class="col-md-6 col-lg-6 col-sm-12" >  

                                <h3 style="width:100% !important;margin-top: unset;border-bottom: unset;">Driver :   <?php echo $driver_name; ?></h3>

                            </div>

                            <div class="col-md-6 col-lg-6 col-sm-12" > 


                                <p  style="cursor: pointer;float:right;font-weight: bolder; " onclick="live_driver_location();
                                        time_reloader();"><span>Driver location refreshing in </span><span style="margin-left: 2px;margin-right:2px;" id="timmer">30</span><span>seconds</span>	 <span > <i class="fa fa-refresh fa-3" aria-hidden="true"></i> </span></p>
                            </div>
                        </div>         

                        <!-- map view -->


                        <div id="map"></div>




                        <ul class="admin-pass">
                            <li class="change-pass">
                                <div class="form_grid_12" style="float:unset">
                                    <div class="form_input">

                                        <a href="<?php echo OPERATOR_NAME; ?>/logs/<?php if (null !== $this->uri->segment(5)) {
    echo $this->uri->segment(5);
} else {
    echo "vehicle_plying_list";
} ?>"> <span class="btn_small btn_blue" ><span>Back</span></span></a>
                                    </div>
                                </div>
                            </li>


                        </ul>







                        <br>







                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<span class="clear"></span>
</div>









<script>
    var marker = "";

    var markers = [];
    var map;
    function initMap() {
<?php if (is_object($driver_location)) $driver_location = (array) $driver_location; ?>
        var myLatLng = {lat:<?php echo $driver_location["lat"]; ?>, lng:<?php echo $driver_location["lon"]; ?>};

        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 14,
            center: myLatLng
        });

        var icon = {
            url: "/component/images/map_vehicle.png", // url
            scaledSize: new google.maps.Size(45, 45) // scaled size

        };



        marker = new google.maps.Marker({
            position: myLatLng,
            icon: icon,
            id: "live_location",
            map: map,
            title: '<?php echo $driver_name; ?>'
        });

        markers["live_location"] = marker;

        time_reloader();
    }

    var live_checker = 0;
    var live_checker2 = 0;

    function live_driver_location()
    {

        $.ajax({
            url: "<?php echo OPERATOR_NAME; ?>/trip/live_driver_location",
            method: "POST",
            data: {driver_id: "<?php echo $driver_id; ?>"},
            success: function (result)
            {
                console.log(result);
                var data_returned = JSON.parse(result);


                if (data_returned == "")
                {
                    $.growl.warning({location: "tc", message: 'Driver location unavailable'});
                }





                markers["live_location"].setMap(null);
                markers = [];

                var latlng = new google.maps.LatLng(data_returned['lat'], data_returned['lon']);
                var icon = {
                    url: "/component/images/map_vehicle.png", // url
                    scaledSize: new google.maps.Size(45, 45) // scaled size

                };



                marker = new google.maps.Marker({
                    position: latlng,
                    icon: icon,
                    id: "live_location",
                    map: map,
                    title: '<?php echo $driver_name; ?>'
                });

                if (live_checker2 == 0)
                {
                    setTimeout(function () {
                        var center = new google.maps.LatLng(data_returned['lat'], data_returned['lon']);

                        map.panTo(center);
                        live_checker2 = 1;



                    }, 4000);

                }


                markers["live_location"] = marker;




            }

        });



    }





    var delMarker = function (id) {
        markerss = markers[id];
        markerss.setMap(null);
    }




    var time = 30;
    var intrevaler = "";

    function time_reloader()
    {
        time = 30;

        clearInterval(intrevaler);


        intrevaler = setInterval(function () {

            time--;

            $('#timmer').html(time);

            if (time === 0) {

                live_driver_location();

                time_reloader();
            }


        }, 1000);



    }



</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?<?php echo substr($this->data['google_maps_api_key'], 1); ?>&callback=initMap">
</script>






<?php
$this->load->view(OPERATOR_NAME . '/templates/footer.php');
?>