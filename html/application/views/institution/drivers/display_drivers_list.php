<?php
$this->load->view(OPERATOR_NAME.'/templates/header.php');
?>
<?php
$type = '';
if(isset($_GET['type']) && $_GET['type']!='') {
		$type=$_GET['type'];
}
$dialcode=array();

foreach ($countryList as $country) {	
	 if ($country->dial_code != '') {			
			$dialcode[]=str_replace(' ', '', $country->dial_code);
	 }
}
 
asort($dialcode);
$dialcode=array_unique($dialcode);                                    
?>

<script>
$(document).ready(function(){
		$vehicle_category='';
		$country='';
		<?php  if($type == 'vehicle_type' && isset($_GET['vehicle_category'])) { ?>
		$vehicle_category = "<?php echo $_GET['vehicle_category']; ?>";
		<?php }?>
		<?php  if($type == 'mobile_number' &&  isset($_GET['country'])) {?>
		$country = "<?php echo $_GET['country']; ?>";
		<?php }?>
		if($vehicle_category != ''){
				$('.vehicle_category').css("display","inline");
				$('#filtervalue').css("display","none");
				$('#filtervalue').css("display","block");
				$("#country").attr("disabled", true);
		}
		if($country != ''){
				$('#country').css("display","inline");
				$('.vehicle_category').attr("disabled", true);		
		}
		$("#filtertype").change(function(){
				$filter_val = $(this).val();
				$('#filtervalue').val('');
				$('.vehicle_category').css("display","none");
				$('#filtervalue').css("display","inline");
				$('#country').css("display","none");
				$("#country").attr("disabled", true);
				$(".vehicle_category").attr("disabled", true);
			
				if($filter_val == 'vehicle_type'){
						$('.vehicle_category').css("display","inline");
						$('#filtervalue').css("display","none");
						$('#country').css("display","none");
						$('.vehicle_category').prop("disabled", false);
						$("#country").attr("disabled", true);
				}
				if($filter_val == 'mobile_number'){
						$('#country').css("display","inline");
						$('#country').prop("disabled", false);
						$(".vehicle_category").attr("disabled", true);
						$('.vehicle_category').css("display","none");
				}			
		});	
});
</script>
<style>
th {
    text-align: center !important;
}
</style>
<div id="content">
    <div class="grid_container">
	
		<div class="grid_12">
				<div class="">
						<div class="widget_content">
								<span class="clear"></span>						
								<div class="">
										<div class=" filter_wrap">
												<div class="widget_top filter_widget">
												
														<h6><?php if ($this->lang->line('admin_drivers_driver_filter') != '') echo stripslashes($this->lang->line('admin_drivers_driver_filter')); else echo 'Drivers Filter'; ?></h6>
														<div class="btn_30_light">	
														<form method="get" id="filter_form" action="<?php echo OPERATOR_NAME; ?>/drivers/display_drivers_list" accept-charset="UTF-8">
																<select class="form-control" id="sortby" name="sortby"  style="width:150px;">
																		<option value="" data-val=""><?php if ($this->lang->line('admin_driver_select_sort_type') != '') echo stripslashes($this->lang->line('admin_driver_select_sort_type')); else echo 'Select Sort Type'; ?></option></option>
																		<option value="doj_asc" <?php if(isset($sortby)){if($sortby=='doj_asc'){ echo 'selected="selected"'; } }?>><?php if ($this->lang->line('admin_user_by_join_date') != '') echo stripslashes($this->lang->line('admin_user_by_join_date')); else echo 'By Joining Date'; ?></option>
																		<option value="doj_desc" <?php if(isset($sortby)){if($sortby=='doj_desc'){ echo 'selected="selected"'; } }?>><?php if ($this->lang->line('admin_user_by_recently_joined') != '') echo stripslashes($this->lang->line('admin_user_by_recently_joined')); else echo 'By Recently Joined'; ?></option>
																		<option value="rides_asc" <?php if(isset($sortby)){if($sortby=='rides_asc'){ echo 'selected="selected"'; } }?>><?php if ($this->lang->line('admin_user_by_least_rides') != '') echo stripslashes($this->lang->line('admin_user_by_least_rides')); else echo 'By Least Rides'; ?></option>
																		<option value="rides_desc" <?php if(isset($sortby)){if($sortby=='rides_desc'){ echo 'selected="selected"'; } }?>><?php if ($this->lang->line('admin_user_by_maximum_rides') != '') echo stripslashes($this->lang->line('admin_user_by_maximum_rides')); else echo 'By Maximum Rides'; ?></option>
																</select>
																<select class="form-control" id="filtertype" name="type" >
																		<option value="" data-val=""><?php if ($this->lang->line('dash_operator_select_filter_type') != '') echo stripslashes($this->lang->line('dash_operator_select_filter_type')); else echo 'Select Filter Type'; ?></option>
																		<option value="driver_name" data-val="driver_name" <?php if(isset($type)){ if($type=='driver_name'){ echo 'selected="selected"'; } }?>>
																		<?php if ($this->lang->line('admin_drivers_driver_name') != '') echo stripslashes($this->lang->line('admin_drivers_driver_name')); else echo 'Driver Name'; ?></option>
																		<option value="email" data-val="email" <?php if(isset($type)){if($type=='email'){ echo 'selected="selected"'; } }?>><?php if ($this->lang->line('admin_drivers_change_password_driver_email') != '') echo stripslashes($this->lang->line('admin_drivers_change_password_driver_email')); else echo 'Driver Email'; ?></option>
																		<option value="mobile_number" data-val="mobile_number" <?php if(isset($type)){if($type=='mobile_number'){ echo 'selected="selected"'; } }?>>
																		<?php if ($this->lang->line('admin_drivers_phone_number') != '') echo stripslashes($this->lang->line('admin_drivers_phone_number')); else echo 'Driver PhoneNumber'; ?></option>
																	
																		<option value="vehicle_type" data-val="vehicle_type" <?php if(isset($type)){if($type=='vehicle_type'){ echo 'selected="selected"'; } }?>><?php if ($this->lang->line('admin_drivers_vehicle_type') != '') echo stripslashes($this->lang->line('admin_drivers_vehicle_type')); else echo 'Vehicle Type'; ?></option>																		
																</select>
																<select name="country" id="country"  class=" form-control" title="Please choose your country" style="display:none;">
																<?php 
																$country = '';
																if(isset($_GET['country']) && $_GET['country']!=''){
																	$country = $_GET['country'];
																}								
																foreach ($dialcode as $row) {
																		if($country != '' && $country == $row){
																				echo "<option selected value=".$row.">".$row."</option>";
																		}else{
																				echo "<option value=".$row.">".$row."</option>";
																		}
																} ?>
																</select>
																<input name="value" id="filtervalue" type="text"  class="tipTop" title="<?php if ($this->lang->line('driver_enter_keyword') != '') echo stripslashes($this->lang->line('driver_enter_keyword')); else echo 'Please enter keyword'; ?>" value="<?php if(isset($value)) echo $value; ?>" placeholder="<?php if ($this->lang->line('driver_enter_keyword') != '') echo stripslashes($this->lang->line('driver_enter_keyword')); else echo 'Please enter keyword'; ?>" />
																<select name="vehicle_category" class='vehicle_category' style="display:none">
																<?php 
																	$veh_cat = '';
																	if(isset($_GET['vehicle_category']) && $_GET['vehicle_category']!=''){
																		$veh_cat = $_GET['vehicle_category'];
																	}
																	foreach($cabCats as $cat){
																		if($veh_cat != '' && $veh_cat == $cat->name){
																			echo "<option selected value=".$cat->name.">".$cat->name."</option>";
																		}else{
																			echo "<option value=".$cat->name.">".$cat->name."</option>";
																		}
																		
																	}
																?>
																</select>
																
																<button type="submit" class="tipTop filterbtn"  original-title="<?php if ($this->lang->line('driver_enter_keyword_filter') != '') echo stripslashes($this->lang->line('driver_enter_keyword_filter')); else echo 'Select filter type and enter keyword to filter'; ?>">
																		<span class="icon search"></span><span class="btn_link"><?php if ($this->lang->line('admin_drivers_filter') != '') echo stripslashes($this->lang->line('admin_drivers_filter')); else echo 'Filter'; ?></span>
																</button>
																<?php if(isset($filter) && $filter!=""){ ?>
																<a href="<?php echo OPERATOR_NAME; ?>/drivers/display_drivers_list"class="tipTop filterbtn" original-title="<?php if ($this->lang->line('admin_notification_remove_filter') != '') echo stripslashes($this->lang->line('admin_notification_remove_filter')); else echo 'Remove Filter'; ?>">
																		<span class="icon delete_co"></span>
																</a>
																<?php } ?>
														</form>
														</div>
												</div>
										</div>
								</div>
						</div>
				</div>
		</div>
	
		<?php
		$attributes = array('id' => 'display_form');
		echo form_open(OPERATOR_NAME.'/drivers/change_driver_status_global', $attributes)
		?>
        <div class="grid_12">
            <div class="widget_wrap">
                <div class="widget_top">
                    <span class="h_icon blocks_images"></span>
                    <h6><?php echo $heading ?></h6>

                </div>
                <div class="widget_content">
                    <?php
                    if ($paginationLink != '') {
                        echo $paginationLink;
                        $tble = 'alldriverListTbl';
                    } else {
                        $tble = 'driverListTbl';
                    }
                    ?>

                    <table class="display" id="<?php echo $tble; ?>" width='100%'>
                        <thead>
                            <tr>
                                <th style="width:65px" class="center">
                                    <?php if ($this->lang->line('operator_s_no') != '') echo stripslashes($this->lang->line('operator_s_no')); else echo 'S.No'; ?>
                                </th>
                                <th style="width:170px" class="tip_top center" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                    <?php if ($this->lang->line('admin_car_types_name') != '') echo stripslashes($this->lang->line('admin_car_types_name')); else echo 'Name'; ?>
                                </th>                            
                                <th style="width:100px" class="tip_top center" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                     <?php if ($this->lang->line('admin_driver_list_doj') != '') echo stripslashes($this->lang->line('admin_driver_list_doj')); else echo 'DOJ'; ?>
                                </th>

                                 <th  class="tip_top center" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                     <?php if ($this->lang->line('driver_rating') != '') echo stripslashes($this->lang->line('driver_rating')); else echo 'Driver Rating ( Out of 5 )'; ?>
                                </th>




                                <th class="tip_top center" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                    <?php if ($this->lang->line('admin_drivers_verified') != '') echo stripslashes($this->lang->line('admin_drivers_verified')); else echo 'Verified'; ?>
                                </th>
                                <th class="tip_top center" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                    <?php if ($this->lang->line('admin_subadmin_status') != '') echo stripslashes($this->lang->line('admin_subadmin_status')); else echo 'Status'; ?>
                                </th>
                                <th class="tip_top center" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                    <?php if ($this->lang->line('admin_drivers_mode') != '') echo stripslashes($this->lang->line('admin_drivers_mode')); else echo 'Mode'; ?>
                                </th>
                                <th class="center">
                                    <?php if ($this->lang->line('admin_subadmin_action') != '') echo stripslashes($this->lang->line('admin_subadmin_action')); else echo 'Action'; ?>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 0;
                            if ($driversList->num_rows() > 0) {
                                foreach ($driversList->result() as $row) { $i++;
                                    ?>
                                    <tr style="border-bottom: 1px solid #dddddd !important;">
                                        <td class="center tr_select ">
                                            <?php echo $i; ?>
                                        </td>

                                        <td class="center">
                                            <?php echo $row->driver_name; ?>
											<?php 
												if ($this->lang->line('not_available') != '') 
													$cTy=stripslashes($this->lang->line('not_available'));
												else
												
													$cTy='';
												
												if(isset($row->category)){
													$catsId = (string)$row->category; 
													if(array_key_exists($catsId,$cabCats)){
														$cTy = $cabCats[$catsId]->name;
													}
												}
												echo '<br/><br/><span style="color:gray;">'.$cTy.'</span>'; 
											?>
                                        </td>
										
										
                                        <td class="center" style="width:70px;">
                                            <?php
                                            if (isset($row->created)) {
                                                echo get_time_to_string('Y-m-d', strtotime($row->created));
                                            }
                                            ?>
                                        </td>


                                 		<td class="center" style="width:116px;">
                                 			<?php 

                                 			 $driver_id=(string)$row->_id;

                                            $condition=array('driver_id' => $driver_id  );



                                            $option = array(

                                                             array(
                                                                        '$match' => array(
                                                                            'driver_id' => $driver_id
                                                                        )
                                                                    ),
                                                                    array(
                                                                        '$group' => array(
                                                                            '_id' => NULL,
                                                                            'total' => array('$sum' => '$rating'),
                                                                            'count' => array('$sum' => 1)
                                                                        )
                                                                    )


                                                                );
                                            $dcursor=$this->cimongo->aggregate(DRIVERS_RATING,$option);
                                             $driverinfo =  $dcursor->toArray();

                                              $rating="No rating";

                                             

                                             if(!empty($driverinfo))
                                             {

                                                $total=$driverinfo[0]['count']*5;

                                                $sum=$driverinfo[0]['total'];


                                                $average=($sum/$total)*100;

                                                $rating=round($average/2)/10;

                                             }
                                             
                                           echo $rating;


                                 			?>



                                 		</td>
									
                                        <td class="center" style="width:60px;">
                                            <?php											
											if(isset($row->verify_status)){
												if($row->verify_status==''){
													$verify_status = get_language_value_for_keyword('No',$this->data['langCode']);;
												}else{
													$verify_status = get_language_value_for_keyword($row->verify_status,$this->data['langCode']);;
												}
											}else{
												$verify_status = get_language_value_for_keyword('No',$this->data['langCode']);;
											}
											?>
											<?php if(isset($row->verify_status) && $row->verify_status=='Yes'){ ?>
												<span class="badge_style b_done"><?php echo $verify_status; ?></span>
											<?php }else{ ?>
												<span class="badge_style"><?php echo $verify_status; ?></span>
											<?php } ?>
                                        </td>

                                        <td class="center" style="width:40px;">
                                            <?php $disp_status = get_language_value_for_keyword($row->status,$this->data['langCode']); ?>
											<?php if($row->status=='Active'){ ?>
												<span class="badge_style b_done"><?php echo $disp_status; ?></span>
											<?php }else{ ?>
												<span class="badge_style"><?php echo $disp_status; ?></span>
											<?php } ?>                                          
                                        </td>

                                         <td class="center" style="width:30px;">
                                            <?php
                                          
                                            $current=time()-300;
                                            
                                            if (isset($row->availability)) {
                                                if ($row->availability == 'Yes' && isset($row->last_active_time->sec) && $row->last_active_time->sec > $current) {
                                                    ?>
                                                    <img src="images/status-online.png" />
                                                    <?php
                                                } else {
                                                    ?>
                                                    <img src="images/status-offline.png" />
                                                    <?php
                                                }
                                            } else {
                                                ?>
                                                <img src="images/status-offline.png" />
                                                <?php
                                            }
                                            ?>
                                        </td>

                                        <td class="center action-icons-wrap" style="width:140px;">

                                        	<span>
                                                    <a class="action-icons c-delete" href="<?php echo OPERATOR_NAME; ?>/drivers/delete_drivers/<?php echo $row->_id;?>" title="<?php if ($this->lang->line('admin_common_delete') != '') echo stripslashes($this->lang->line('admin_common_delete')); else echo 'Delete'; ?>">
                                                            <?php if ($this->lang->line('admin_common_delete') != '') echo stripslashes($this->lang->line('admin_common_delete')); else echo 'Delete'; ?>
                                                    </a>
                                            </span>
										
																		
											<span><a class="action-icons c-key" href="<?php echo OPERATOR_NAME; ?>/drivers/change_password_form/<?php echo $row->_id; ?>" title="<?php if ($this->lang->line('driver_change_password') != '') echo stripslashes($this->lang->line('driver_change_password')); else echo 'Change Password'; ?>"><?php if ($this->lang->line('admin_user_change_password') != '') echo stripslashes($this->lang->line('admin_user_change_password')); else echo 'Change Password'; ?></a></span>
											
											<span><a class="action-icons c-edit" href="<?php echo OPERATOR_NAME; ?>/drivers/edit_driver_form/<?php echo $row->_id; ?>" title="<?php if ($this->lang->line('admin_common_edit') != '') echo stripslashes($this->lang->line('admin_common_edit')); else echo 'Edit'; ?>"><?php if ($this->lang->line('admin_common_edit') != '') echo stripslashes($this->lang->line('admin_common_edit')); else echo 'Edit'; ?></a></span>
	
											
											
											
											<span><a class="action-icons c-suspend" href="<?php echo OPERATOR_NAME; ?>/drivers/view_driver/<?php echo $row->_id; ?>" title="<?php if ($this->lang->line('admin_common_view') != '') echo stripslashes($this->lang->line('admin_common_view')); else echo 'View'; ?>"></a></span>                                          
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th class="center">
                                    <?php if ($this->lang->line('operator_s_no') != '') echo stripslashes($this->lang->line('operator_s_no')); else echo 'S.No'; ?>
                                </th>
                                <th class="center">
                                    <?php if ($this->lang->line('admin_car_types_name') != '') echo stripslashes($this->lang->line('admin_car_types_name')); else echo 'Name'; ?>
                                </th>								
                               
                                <th class="center">
                                     <?php if ($this->lang->line('admin_driver_list_doj') != '') echo stripslashes($this->lang->line('admin_driver_list_doj')); else echo 'DOJ'; ?>
                                </th>
                              	<th class="center">
                                      <?php if ($this->lang->line('driver_rating') != '') echo stripslashes($this->lang->line('driver_rating')); else echo 'Driver Rating ( Out of 5 )'; ?>
                                </th>

                                <th class="center">
                                    <?php if ($this->lang->line('admin_drivers_verified') != '') echo stripslashes($this->lang->line('admin_drivers_verified')); else echo 'Verified'; ?>
                                </th>
                                <th class="center">
                                    <?php if ($this->lang->line('admin_subadmin_status') != '') echo stripslashes($this->lang->line('admin_subadmin_status')); else echo 'Status'; ?>
                                </th>
                                <th class="center" >
                                    <?php if ($this->lang->line('admin_drivers_mode') != '') echo stripslashes($this->lang->line('admin_drivers_mode')); else echo 'Mode'; ?>
                                </th>
                                <th class="center">
                                    <?php if ($this->lang->line('admin_subadmin_action') != '') echo stripslashes($this->lang->line('admin_subadmin_action')); else echo 'Action'; ?>
                                </th>
                            </tr>
                        </tfoot>
                    </table>

                    <?php
                    if ($paginationLink != '') {
                        echo $paginationLink;
                    }
                    ?>

                </div>
            </div>
        </div>
        <input type="hidden" name="statusMode" id="statusMode"/>
        <input type="hidden" name="SubAdminEmail" id="SubAdminEmail"/>
        </form>	
    </div>
    <span class="clear"></span>
</div>
</div>
<style>										
		.b_warn {
			background: orangered none repeat scroll 0 0;
			border: medium none red;
		}
		
		.filter_widget .btn_30_light {
			margin: -11px;
			width: 83%;
		}
</style>
<?php
$this->load->view(OPERATOR_NAME.'/templates/footer.php');
?>