<?php
$this->load->view(OPERATOR_NAME . '/templates/header.php');

if ($this->lang->line('dash_not_available') != '') $not_available = stripslashes($this->lang->line('dash_not_available')); else $not_available = 'Not Available';

?>
<div id="content" class="admin-settings edit-global-set add_drive_catagory">
	<div class="grid_container">
		<div class="grid_12">
			<div class="widget_wrap">
				<div class="widget_wrap tabby">
					<div class="widget_top"> 
						<span class="h_icon list"></span>
						<h6><?php echo $heading; ?></h6>
						<div id="widget_tab">
							<ul>
								<li><a href="#tab1" class="active_tab"><?php if ($this->lang->line('admin_drivers_driver_details') != '') echo stripslashes($this->lang->line('admin_drivers_driver_details')); else echo 'Driver Details'; ?></a></li>
										<li><a href="#tab3"><?php if ($this->lang->line('admin_drivers_documents') != '') echo stripslashes($this->lang->line('admin_drivers_documents')); else echo 'Documents'; ?></a></li>
								
							</ul>
						</div>
					</div>
					<div class="widget_content">
						<?php 
							$attributes = array('class' => 'form_container left_label ajaxsubmit', 'id' => 'settings_form', 'enctype' => 'multipart/form-data');
							echo form_open_multipart('admin/adminlogin/admin_global_settings',$attributes);
						
							$driver_details = $driver_details->row();
						
						?>
							<div id="tab1">
								<ul class="left-contsec view_driver_list">
								
									<li>
										<div class="form_grid_12">
											<label class="field_title"><?php if ($this->lang->line('admin_drivers_driver_captain_role') != '') echo stripslashes($this->lang->line('admin_drivers_driver_captain_role')); else echo 'Role'; ?></label>
											<div class="form_input">
                                                                                            <p class="text-capitalize"><?php echo (property_exists($driver_details, "role"))?$driver_details->role:"";?></p>
											</div>
										</div>
									</li>
									
									<li>
										<div class="form_grid_12">
											<label class="field_title"><?php if ($this->lang->line('admin_drivers_driver_name') != '') echo stripslashes($this->lang->line('admin_drivers_driver_name')); else echo 'Driver Name'; ?></label>
											<div class="form_input">
                                                                                            <p><?php echo (property_exists($driver_details, "driver_name"))?$driver_details->driver_name:"";?></p>
											</div>
										</div>
									</li>
									
									<li>
									<div class="form_grid_12">
										<label class="field_title"><?php if ($this->lang->line('admin_drivers_email_address') != '') echo stripslashes($this->lang->line('admin_drivers_email_address')); else echo 'Email Address'; ?> </label>
										<div class="form_input">
											<p>											
											<?php if($isDemo){ ?>
											<?php echo $dEmail; ?>
											<?php }  else{ ?>
											<?php if(property_exists($driver_details, "email")) echo $driver_details->email;?>
											<?php } ?>
											</p>											
										</div>
									</div>
									</li>
	
									<li>
										<div class="form_grid_12">
											<label class="field_title"><?php if ($this->lang->line('admin_drivers_driver_profile_image') != '') echo stripslashes($this->lang->line('admin_drivers_driver_profile_image')); else echo 'Driver Profile Image'; ?></label>
											<div class="form_input">
												<?php if(property_exists($driver_details, "image") && isset($driver_details->image) != ''){?>
												 <img width="15%" src="<?php echo base_url().USER_PROFILE_THUMB.$driver_details->image;?>" />
												<?php }else {?>
												 <img width="15%" src="<?php echo base_url().USER_PROFILE_THUMB_DEFAULT;?>" />
												<?php }?>
											</div>
										</div>
									</li>
								</ul>
								
								
								<ul class="rite-contsec view_driver_list">
								<li>
									<h3><?php if ($this->lang->line('admin_drivers_login_details') != '') echo stripslashes($this->lang->line('admin_drivers_login_details')); else echo 'Location Details'; ?></h3>
								</li>
									
									<li>
									<div class="form_grid_12">
										<label class="field_title"><?php if ($this->lang->line('admin_drivers_address') != '') echo stripslashes($this->lang->line('admin_drivers_address')); else echo 'Address'; ?></label>
										<div class="form_input">
                                                                                    <?php if(property_exists($driver_details, "address") && is_object($driver_details->address)) $driver_details->{"address"}=(array)$driver_details->address; ?>
											<p><?php if(property_exists($driver_details, "address") && isset($driver_details->address['address'])) echo $driver_details->address['address']; ?></p>
										</div>
									</div>
									</li>
									
									<li>
									<div class="form_grid_12">
										<label class="field_title"><?php if ($this->lang->line('admin_drivers_country') != '') echo stripslashes($this->lang->line('admin_drivers_country')); else echo 'Country'; ?></label>
										<div class="form_input">
												<p><?php if(property_exists($driver_details, "address")) echo $driver_details->address['county']; ?></p>
											</select>
										</div>
									</div>
									</li>
									
									<li>
									<div class="form_grid_12">
										<label class="field_title"><?php if ($this->lang->line('admin_drivers_state_province_region') != '') echo stripslashes($this->lang->line('admin_drivers_state_province_region')); else echo 'State / Province / Region'; ?></label>
										<div class="form_input">
											<p><?php if(property_exists($driver_details, "address") && isset($driver_details->address['state'])) echo $driver_details->address['state']; ?></p>
										</div>
									</div>
									</li>
									
									<li>
									<div class="form_grid_12">
										<label class="field_title"><?php if ($this->lang->line('admin_drivers_city') != '') echo stripslashes($this->lang->line('admin_drivers_city')); else echo 'City'; ?></label>
										<div class="form_input">
											<p><?php if(property_exists($driver_details, "address") && isset($driver_details->address['city'])) echo $driver_details->address['city']; ?></p>
										</div>
									</div>
									</li>

									<li>
									<div class="form_grid_12">
										<label class="field_title"><?php if ($this->lang->line('admin_drivers_postal_code') != '') echo stripslashes($this->lang->line('admin_drivers_postal_code')); else echo 'Postal Code'; ?></label>
										<div class="form_input">
											<p><?php if(property_exists($driver_details, "address") && isset($driver_details->address['postal_code'])) echo $driver_details->address['postal_code']; ?></p>
										</div>
									</div>
									</li>

									<li>
									<div class="form_grid_12">
										<label class="field_title"><?php if ($this->lang->line('admin_drivers_mobile_number') != '') echo stripslashes($this->lang->line('admin_drivers_mobile_number')); else echo 'Mobile Number'; ?></label>
										<div class="form_input">
											<p><?php if(property_exists($driver_details, "dail_code") && isset($driver_details->dail_code)) echo $driver_details->dail_code; ?>
											<?php if(property_exists($driver_details, "mobile_number") && isset($driver_details->mobile_number)) echo $driver_details->mobile_number; ?></p>
										</div>
									</div>
									</li>
									
									<li>
									<div class="form_grid_12">
										<label class="field_title"><?php if ($this->lang->line('admin_subadmin_status') != '') echo stripslashes($this->lang->line('admin_subadmin_status')); else echo 'Status'; ?></label>
										<div class="form_input">
											<p><?php if(property_exists($driver_details, "status") && isset($driver_details->status)) echo get_language_value_for_keyword($driver_details->status,$this->data['langCode']); ?></p>
										</div>
									</div>
									</li>
								
								</ul>
								
								<ul class="last-sec-btn">
									<li class="change-pass">
										<div class="form_grid_12">
											<div class="form_input">
												<a  href="<?php echo OPERATOR_NAME; ?>/drivers/display_drivers_list" class="btn_small btn_blue" ><span><?php if ($this->lang->line('admin_drivers_back_to_driver_list') != '') echo stripslashes($this->lang->line('admin_drivers_back_to_driver_list')); else echo 'Back To Drivers List'; ?></span></a>
											</div>
										</div>
									</li>
								</ul>
							</div>
						
						
				
							<div id="tab3">
								<ul class="leftsec-contsec">
									<li>
											<h3><?php if ($this->lang->line('admin_drivers_driver_documents') != '') echo stripslashes($this->lang->line('admin_drivers_driver_documents')); else echo 'Driver Documents'; ?></h3>
									</li>
									
								<?php 
                                                                if(property_exists($driver_details, "documents") && is_object($driver_details->documents))
                                                                    $driver_details->{"documents"}=(array)$driver_details->documents;
								if(property_exists($driver_details, "documents") && isset($driver_details->documents['driver'])){
									$driver_docx = (is_object($driver_details->documents['driver']))?(array)$driver_details->documents['driver']:$driver_details->documents['driver'];
								} else {
									$driver_docx = array();
								}
										
								if(count($driver_docx) > 0){
									foreach($driver_docx as $docx_key => $drivers_doc){ 
                                                                            if(is_object($drivers_doc))
                                                                                $drivers_doc=(array)$drivers_doc;
								?>
									
									<li>
								<div class="form_grid_12">
									<label class="field_title"><?php if(isset($drivers_doc['typeName'])) echo $drivers_doc['typeName']; ?> </label>
									<div class="form_input expiry_box">
										<?php 
											if(isset($drivers_doc['fileName'])){
												if($drivers_doc['fileName'] != ''){
										?>
											<a href="drivers_documents/<?php echo $drivers_doc['fileName']; ?>" target="_blank" > <?php if ($this->lang->line('admin_subadmin_view') != '') echo stripslashes($this->lang->line('admin_subadmin_view')); else echo 'View'; ?> <?php echo $drivers_doc['typeName']; ?> </a>
										<?php } else { ?>
											<a><?php echo $drivers_doc['typeName'] ?> <?php if ($this->lang->line('admin_drivers_not_available') != '') echo stripslashes($this->lang->line('admin_drivers_not_available')); else echo $not_available; ?></a>
										<?php } 
											}
										?>
										<?php 
										if(isset($drivers_doc['expiryDate'])){ 
											if($drivers_doc['expiryDate'] != ''){
										?>
											<p><b><?php if ($this->lang->line('admin_drivers_expiry_date') != '') echo stripslashes($this->lang->line('admin_drivers_expiry_date')); else echo 'Expiry Date'; ?> : </b><?php echo get_time_to_string('M,d Y',strtotime($drivers_doc['expiryDate'])); ?></p>
										<?php 
											}
										} ?>	
									</div>
								</div>
								</li>
						<?php 
										}
									} else {
							?>
								<li>
									<div class="form_grid_12">
										<label class="field_title"></label>
										<div class="form_input">
											<p><?php if ($this->lang->line('admin_drivers_no_record_found') != '') echo stripslashes($this->lang->line('admin_drivers_no_record_found')); else echo 'No records found'; ?></p>
										</div>
									</div>
								</li>
							<?php 
								}
							?>
							
							
								</ul>
								<ul class="admin-pass">
									<li class="change-pass">
										<div class="form_grid_12">
											<div class="form_input">
												<a  href="<?php echo OPERATOR_NAME; ?>/drivers/display_drivers_list" class="btn_small btn_blue" ><span><?php if ($this->lang->line('admin_drivers_back_to_driver_list') != '') echo stripslashes($this->lang->line('admin_drivers_back_to_driver_list')); else echo 'Back To Drivers List'; ?></span></a>
											</div>
										</div>
									</li>
								</ul>
							</div>
							
							
							
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<span class="clear"></span> 
</div>
</div>


<style>

.expiry_box {
    background: none repeat scroll 0 0 gainsboro;
    border: 1px solid grey;
    border-radius: 5px;
    margin-top: 2%;
    padding: 1%;
    text-align: center;
    width: 50% !important;
}

.expiry_box_status {
    background: gainsboro none repeat scroll 0 0;
    border: 1px solid grey;
    border-radius: 5px;
    color: #fff;
    float: right;
    font-weight: bold;
     margin: -32px 0 0;
    padding: 5px 13px;
    text-align: center;
    width: 125px !important;
}
.expiry_box a{
	color: green;
}
.expiry_box p{
	margin-bottom: 0;
    margin-top: 12px;
}
</style>

<?php 
$this->load->view(OPERATOR_NAME . '/templates/footer.php');
?>
