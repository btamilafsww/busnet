<?php
$this->load->view(OPERATOR_NAME.'/templates/header.php');
?>
<?php
$type = '';
if(isset($_GET['type']) && $_GET['type']!='') {
		$type=$_GET['type'];
}
$dialcode=array();

foreach ($countryList as $country) {	
	 if ($country->dial_code != '') {			
			$dialcode[]=str_replace(' ', '', $country->dial_code);
	 }
}
 
asort($dialcode);
$dialcode=array_unique($dialcode);                                    
?>

<script>
$(document).ready(function(){
		$vehicle_category='';
		$country='';
		<?php  if($type == 'vehicle_type' && isset($_GET['vehicle_category'])) { ?>
		$vehicle_category = "<?php echo $_GET['vehicle_category']; ?>";
		<?php }?>
		<?php  if($type == 'mobile_number' &&  isset($_GET['country'])) {?>
		$country = "<?php echo $_GET['country']; ?>";
		<?php }?>
		if($vehicle_category != ''){
				$('.vehicle_category').css("display","inline");
				$('#filtervalue').css("display","none");
				$('#filtervalue').css("display","block");
				$("#country").attr("disabled", true);
		}
		if($country != ''){
				$('#country').css("display","inline");
				$('.vehicle_category').attr("disabled", true);		
		}
		$("#filtertype").change(function(){
				$filter_val = $(this).val();
				$('#filtervalue').val('');
				$('.vehicle_category').css("display","none");
				$('#filtervalue').css("display","inline");
				$('#country').css("display","none");
				$("#country").attr("disabled", true);
				$(".vehicle_category").attr("disabled", true);
			
				if($filter_val == 'vehicle_type'){
						$('.vehicle_category').css("display","inline");
						$('#filtervalue').css("display","none");
						$('#country').css("display","none");
						$('.vehicle_category').prop("disabled", false);
						$("#country").attr("disabled", true);
				}
				if($filter_val == 'mobile_number'){
						$('#country').css("display","inline");
						$('#country').prop("disabled", false);
						$(".vehicle_category").attr("disabled", true);
						$('.vehicle_category').css("display","none");
				}			
		});	
});
</script>
<div id="content">
    <div class="grid_container">
	
		<div class="grid_12">
				<div class="">
						<div class="widget_content">
								<span class="clear"></span>						
								<div class="">
										<div class=" filter_wrap">
											
												</div>
										</div>
								</div>
						</div>
				</div>
		</div>
	
		<?php
		$attributes = array('id' => 'display_form');
		echo form_open(OPERATOR_NAME.'/drivers/change_driver_status_global', $attributes)
		?>
        <div class="grid_12">
            <div class="widget_wrap">
                <div class="widget_top">
                    <span class="h_icon blocks_images"></span>
                    <h6><?php echo $heading ?></h6>

                </div>
                <div class="widget_content">
                    <?php
                    if ($paginationLink != '') {
                        echo $paginationLink;
                        $tble = 'alldriverListTbl';
                    } else {
                        $tble = 'driverListTbl';
                    }
                    ?>

                    <table class="display" id="<?php echo $tble; ?>" width='100%'>
                        <thead>
                            <tr>
                                <th style="width:65px" class="center">
                                   S.No
                                </th>
                                <th style="width:400px" class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                  Route Name
                                </th>
                                
                                <th style="width:400px">
                                Number of Stops
                                </th>
                                <th style="width:400px">Actions</th>
                               
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 0;
                            if ($route_list->num_rows() > 0) {
							
                                foreach ($route_list->result() as $row) { $i++;
                                    ?>
                                    <tr style="border-bottom: 1px solid #dddddd !important;">
                                        <td class="center tr_select ">
                                            <?php echo $i; ?>
                                        </td>
                                        <td class="center">
                                        	<?php echo $row->route_name; ?>
                                        
                                         </td> 		
                                         
                                          <td class="center">
                                        	<?php  echo  sizeof($row->waypoints); ?>
                                        
                                         </td> 	
                                           <td class="center action-icons-wrap" style="width:140px;">
                                        
                                     
                                         
    
                                       

                                            <span>
                                                    <a class="action-icons c-delete" href="<?php echo OPERATOR_NAME; ?>/bus_route/delete_bus_routes/<?php echo $row->_id;?>" title="<?php if ($this->lang->line('admin_common_delete') != '') echo stripslashes($this->lang->line('admin_common_delete')); else echo 'Delete'; ?>">
                                                            <?php if ($this->lang->line('admin_common_delete') != '') echo stripslashes($this->lang->line('admin_common_delete')); else echo 'Delete'; ?>
                                                    </a>
                                            </span>
                                         
                                            <span>
                                                <a style="float:unset" class="action-icons c-suspend" href="<?php echo OPERATOR_NAME; ?>/bus_route/view_bus_route/<?php echo $row->_id; ?>" title="<?php if ($this->lang->line('admin_common_view') != '') echo stripslashes($this->lang->line('admin_common_view')); else echo 'View'; ?>"></a>
                                            </span>  

                                               <span><a class="action-icons c-edit" href="<?php echo OPERATOR_NAME; ?>/bus_route/edit_bus_route/<?php echo $row->_id; ?>" title="<?php if ($this->lang->line('admin_common_edit') != '') echo stripslashes($this->lang->line('admin_common_edit')); else echo 'Edit'; ?>"><?php if ($this->lang->line('admin_common_edit') != '') echo stripslashes($this->lang->line('admin_common_edit')); else echo 'Edit'; ?></a></span>    

                                        </td>				
                                       
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th class="center">
                                    S.No
                                </th>
                                <th>
                                   Route Name
                                </th>	
                                <th>
                                   Number of Stops
                                </th>								
                                <th>Actions</th>
                            </tr>
                        </tfoot>
                    </table>

                    <?php
                    if ($paginationLink != '') {
                        echo $paginationLink;
                    }
                    ?>

                </div>
            </div>
        </div>
        <input type="hidden" name="statusMode" id="statusMode"/>
        <input type="hidden" name="SubAdminEmail" id="SubAdminEmail"/>
        </form>	
    </div>
    <span class="clear"></span>
</div>
</div>
<style>										
		.b_warn {
			background: orangered none repeat scroll 0 0;
			border: medium none red;
		}
		
		.filter_widget .btn_30_light {
			margin: -11px;
			width: 83%;
		}

        .action-icons {
            float:none;
        }




</style>
<?php
$this->load->view(OPERATOR_NAME.'/templates/footer.php');
?>