<?php
$this->load->view(OPERATOR_NAME.'/templates/header.php');


?>
<div id="content" class="admin-settings add-edit-opr">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">
                <div class="widget_top">
                    <span class="h_icon list"></span>
                    <h6><?php echo $heading; ?></h6>
                    <div id="widget_tab">
                    </div>
                </div>
                <div class="widget_content chenge-pass-base">
                    <form class="form_container left_label" action="<?php echo OPERATOR_NAME;?>/add_bus/insert_bus_new" id="addeditmodel_form" method="post" enctype="multipart/form-data">
                        <div>
                       <ul class="leftsec-contsec">
                      

						<li> 
						<div class="form_grid_12">
						<label class="field_title"><?php if ($this->lang->line('admin_make_and_model_model_list_model_type') != '') echo stripslashes($this->lang->line('admin_make_and_model_model_list_model_type')); else echo 'Model Type'; ?></label>
							<div class="form_input model_type">
							
								    <label><?php echo  $typeList[0]->name; ?></label>
							</div>
							</div>
							</li>







						<li>
                            <div class="form_grid_12">
                                <label class="field_title">Vehicle Number<span class="req">*</span></label>
                                <div class="form_input">
                                  
                                    <label><?php echo  $bus_data[0]->vehicle_number; ?></label>
                                </div>
                            </div>
                        </li>



                        <li>
                            <div class="form_grid_12">
                                <label class="field_title">Vehicle Code<span class="req">*</span></label>
                                <div class="form_input">
                                  
                                    <label><?php  if(isset($bus_data[0]->vehicle_code)){echo $bus_data[0]->vehicle_code;}; ?></label>
                                </div>
                            </div>
                        </li>




							

                                <li>
                                    <div class="form_grid_12">
                                        <label class="field_title"><?php if ($this->lang->line('admin_common_status') != '') echo stripslashes($this->lang->line('admin_common_status')); else echo 'Status'; ?></label>
                                        <div class="form_input">
                                            <div class="active_inactive">

                                            <label>
                                                 <?php


                                                    if($bus_data[0]->status == "on")
                                                    {    
                                                    $form_mode=true;
                                                    }
                                                    else
                                                    {
                                                    $form_mode=false;
                                                    }    

                                                    if ($form_mode) {

                                                    echo 'Active';
                                                    }
                                                    else
                                                    {

                                                    echo 'Inactive';
                                                    }   
                                              
                                                ?>

                                            </label>
                                            </div>
                                        </div>
                                    </div>
                                </li>

                               </ul>
								
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <span class="clear"></span>
</div>
</div>
<style>
.model_type .error {
    float: right;
    margin-right: 30%;
}
label.error {
float:right!important;
}
/* .year-of-models .chzn-drop{
	width: 65px !important;
}
#year_of_model_chzn{
	width: 250px !important;
} */
.default {
	width: 650px !important;
}
</style>
<script>

$(document).ready(function() {
	$.validator.setDefaults({ ignore: ":hidden:not(select)" });
	$("#addeditmodel_form").validate();

	get_model(document.getElementById("brand_list").value);
});




function get_model(id)
{  

$.ajax({
	url: "operator/add_bus/vechile_type_list",
	method: "POST",
	data : {brand_id : id}, 
	success: function(result){

    $('#model_lister').html(result);
  		


  		}
	});

}







</script>
<?php
$this->load->view(OPERATOR_NAME.'/templates/footer.php');
?>