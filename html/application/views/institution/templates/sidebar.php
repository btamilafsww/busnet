<?php
$currentUrl = $this->uri->segment(2, 0);
$currentPage = $this->uri->segment(3, 0);
if ($currentUrl == '') {
    $currentUrl = 'dashboard';
}
if ($currentPage == '') {
    $currentPage = 'dashboard';
}

$current_url = $_SERVER['REQUEST_URI'];
?>
<div class="" style="position:relative">
    <div id="left_bar" >

        <div class="logo">
            <img src="images/logo/<?php echo $op_logo; ?>" alt="<?php echo $siteTitle; ?>" width="160px" title="<?php echo $siteTitle; ?>">
        </div>


        <div id="sidebar">
            <div id="secondary_nav">
                <ul id="sidenav" class="accordion_mnu collapsible">

                    <li>
                        <a href="<?php echo base_url() . OPERATOR_NAME; ?>/dashboard/display_dashboard" <?php
                        if ($currentUrl == 'dashboard') {
                            echo 'class="active"';
                        }
                        ?>>
                            <span class="nav_icon computer_imac"></span> 
                            <?php
                            if ($this->lang->line('admin_menu_dashboard') != '')
                                echo stripslashes($this->lang->line('admin_menu_dashboard'));
                            else
                                echo 'Dashboard';
                            ?>
                        </a>
                    </li>




                    <?php
                    if ($operator_type == "main") {
                        ?>		

                        <li>
                            <a href="<?php echo $current_url; ?>" <?php
                            if ($currentUrl == 'settings') {
                                echo 'class="active"';
                            }
                            ?>>
                                <span class="nav_icon settings_2"></span><?php
                                if ($this->lang->line('admin_menu_settings') != '')
                                    echo stripslashes($this->lang->line('admin_menu_settings'));
                                else
                                    echo 'Settings';
                                ?><span class="up_down_arrow">&nbsp;</span>
                            </a>

                            <ul class="acitem" <?php
                            if ($currentUrl == 'settings') {
                                echo 'style="display: block;"';
                            } else {
                                echo 'style="display: none;"';
                            }
                            ?>>

                                <li>
                                    <a href="<?php echo OPERATOR_NAME; ?>/settings/edit_profile_form" <?php
                                    if ($currentPage == 'edit_profile_form') {
                                        echo 'class="active"';
                                    }
                                    ?>>
                                        <span class="list-icon">&nbsp;</span><?php
                                        if ($this->lang->line('dash_operator_profile_settings') != '')
                                            echo stripslashes($this->lang->line('dash_operator_profile_settings'));
                                        else
                                            echo 'Profile Settings';
                                        ?>
                                    </a>
                                </li>

                                <li>
                                    <a href="<?php echo OPERATOR_NAME; ?>/settings/change_password" <?php
                                    if ($currentPage == 'change_password') {
                                        echo 'class="active"';
                                    }
                                    ?>>
                                        <span class="list-icon">&nbsp;</span><?php
                                        if ($this->lang->line('admin_menu_change_password') != '')
                                            echo stripslashes($this->lang->line('admin_menu_change_password'));
                                        else
                                            echo 'Change Password';
                                        ?>
                                    </a>
                                </li>

                                <li>
                                    <a href="<?php echo OPERATOR_NAME; ?>/settings/yearly_time_setting" <?php
                                    if ($currentPage == 'yearly_time_setting') {
                                        echo 'class="active"';
                                    }
                                    ?>>
                                        <span class="list-icon">&nbsp;</span><?php
                                        if ($this->lang->line('yearly_time_setting') != '')
                                            echo stripslashes($this->lang->line('yearly_time_setting'));
                                        else
                                            echo 'Academic Calendar';
                                        ?>
                                    </a>
                                </li>

                            </ul>

                        </li>	

                        <?php
                    }
                    ?>




                    <?php
                    $schooladminPrevs = (is_object($schooladminPrevs)) ? (array) $schooladminPrevs : array();
                    if (isset($schooladminPrevs["student_management_details"])) {
                        ?>	                    
                        <li>
                            <a href="<?php echo $current_url; ?>" <?php
                            if ($currentUrl == 'add_new_class' || $currentUrl == 'add_students' || $currentUrl == 'class_details') {
                                echo 'class="active"';
                            }
                            ?>>
                                <span class="nav_icon" style="background: unset;"><img style="width:100%;    vertical-align: unset;" src="images/sprite-icons/Chalkboard.png"></span> <?php
                                if ($this->lang->line('school_management_menu') != '')
                                    echo stripslashes($this->lang->line('school_management_menu'));
                                else
                                    echo 'School Management';
                                ?><span class="up_down_arrow">&nbsp;</span>
                            </a>

                            <ul class="acitem" <?php
                            if ($currentUrl == 'school_management_details' || $currentUrl == 'add_new_class' || $currentUrl == 'add_students' || $currentUrl == 'class_details') {
                                echo 'style="display: block;"';
                            } else {
                                echo 'style="display: none;"';
                            }
                            ?>>

                                <?php
                                if (in_array("add_new_class", $schooladminPrevs["student_management_details"])) {
                                    ?>	
                                    <li>
                                        <a href="<?php echo OPERATOR_NAME; ?>/class_details/add_new_class" <?php
                                        if ($currentPage == 'add_new_class') {
                                            echo 'class="active"';
                                        }
                                        ?>>
                                            <span class="list-icon">&nbsp;</span><?php
                                            if ($this->lang->line('add_class') != '')
                                                echo stripslashes($this->lang->line('add_class'));
                                            else
                                                echo 'Add Class';
                                            ?>
                                        </a>
                                    </li>
                                    <?php
                                }
                                ?>


                                <?php
                                if (in_array("add_studentdetails", $schooladminPrevs["student_management_details"])) {
                                    ?>
                                    <li>
                                        <a href="<?php echo OPERATOR_NAME; ?>/add_students/add_studentdetails" <?php
                                        if ($currentPage == 'add_studentdetails') {
                                            echo 'class="active"';
                                        }
                                        ?>>
                                            <span class="list-icon">&nbsp;</span><?php
                                            if ($this->lang->line('student_add') != '')
                                                echo stripslashes($this->lang->line('student_add'));
                                            else
                                                echo 'Add Student';
                                            ?>
                                        </a>
                                    </li>
                                    <?php
                                }
                                ?>	

                                <?php
                                if (in_array("view_student_details", $schooladminPrevs["student_management_details"]) || in_array("edit_student_detail", $schooladminPrevs["add_students"])) {
                                    ?>
                                    <li>
                                        <a href="<?php echo OPERATOR_NAME; ?>/add_students/student_lists" <?php
                                        if ($currentPage == 'student_lists' || $currentPage == 'view_student_details') {
                                            echo 'class="active"';
                                        }
                                        ?>>
                                            <span class="list-icon">&nbsp;</span><?php
                                            if ($this->lang->line('student_list') != '')
                                                echo stripslashes($this->lang->line('student_list'));
                                            else
                                                echo 'Student List';
                                            ?>
                                        </a>
                                    </li>
                                    <?php
                                }
                                ?>	   



                            </ul>
                        </li>                    
                        <?php
                    }
                    ?>





                    <?php
                    if ($operator_type == "main") {
                        ?>
                        <li>
                            <a href="<?php echo $current_url; ?>" <?php
                            if ($currentUrl == 'other_users') {
                                echo 'class="active"';
                            }
                            ?>>
                                <span class="nav_icon admin_user"></span><?php
                                if ($this->lang->line('user_management') != '')
                                    echo stripslashes($this->lang->line('user_management'));
                                else
                                    echo 'User Management';
                                ?><span class="up_down_arrow">&nbsp;</span>
                            </a>
                            <ul class="acitem" <?php
                            if ($currentUrl == 'other_users') {
                                echo 'style="display: block;"';
                            } else {
                                echo 'style="display: none;"';
                            }
                            ?>>

                                <li>
                                    <a href="<?php echo OPERATOR_NAME; ?>/other_users/add_new_user" <?php
                                    if ($currentPage == 'add_new_user') {
                                        echo 'class="active"';
                                    }
                                    ?>>
                                        <span class="list-icon">&nbsp;</span><?php
                                        if ($this->lang->line('add_user_management') != '')
                                            echo stripslashes($this->lang->line('add_user_management'));
                                        else
                                            echo 'Add User';
                                        ?>
                                    </a>
                                </li>

                                <li>
                                    <a href="<?php echo OPERATOR_NAME; ?>/other_users/display_user_list" <?php
                                    if ($currentPage == 'display_user_list') {
                                        echo 'class="active"';
                                    }
                                    ?>>
                                        <span class="list-icon">&nbsp;</span>User List
                                    </a>
                                </li>



                            </ul>
                        </li>

                        <?php
                    }
                    ?>




                    <?php
                    if (isset($schooladminPrevs["driver"])) {
                        ?>
                        <li>

                            <a href="<?php echo $current_url; ?>" <?php
                            if (($currentUrl == 'drivers' || $currentPage == 'view_driver_reviews') && ($currentPage != 'add_edit_category_types' && $currentPage != 'add_edit_category' && $currentPage != 'display_drivers_category' && $currentPage != 'edit_language_category')) {
                                echo 'class="active"';
                            }
                            ?>>
                                <span class="nav_icon user_2"></span> <?php
                                if ($this->lang->line('admin_menu_driver_management') != '')
                                    echo stripslashes($this->lang->line('admin_menu_driver_management'));
                                else
                                    echo 'Driver Management';
                                ?><span class="up_down_arrow">&nbsp;</span>
                            </a>

                            <ul class="acitem" <?php
                            if (($currentUrl == 'drivers' || $currentPage == 'view_driver_reviews') && ($currentPage != 'add_edit_category_types' && $currentPage != 'add_edit_category' && $currentPage != 'display_drivers_category' && $currentPage != 'edit_language_category')) {
                                echo 'style="display: block;"';
                            } else {
                                echo 'style="display: none;"';
                            }
                            ?>>

                                <?php
                                if (in_array("dashboard", $schooladminPrevs["driver"])) {
                                    ?>
                                    <li>
                                        <a href="<?php echo OPERATOR_NAME; ?>/drivers/display_driver_dashboard" <?php
                                        if ($currentPage == 'display_driver_dashboard') {
                                            echo 'class="active"';
                                        }
                                        ?>>
                                            <span class="list-icon">&nbsp;</span><?php
                                            if ($this->lang->line('admin_menu_drivers_dashboard') != '')
                                                echo stripslashes($this->lang->line('admin_menu_drivers_dashboard'));
                                            else
                                                echo 'Drivers Dashboard';
                                            ?>
                                        </a>
                                    </li>
                                    <?php
                                }
                                ?>

                                <?php
                                if (in_array("edit_driver_form", $schooladminPrevs["driver"]) || in_array("view_driver", $schooladminPrevs["driver"])) {
                                    ?>
                                    <li>
                                        <a href="<?php echo OPERATOR_NAME; ?>/drivers/display_drivers_list" <?php
                                        if ($currentPage == 'display_drivers_list' || $currentPage == 'edit_driver_form' || $currentPage == 'change_password_form' || $currentPage == 'view_driver' || $currentPage == 'banking' || $currentPage == 'view_driver_reviews' || $currentPage == 'display_rides') {
                                            echo 'class="active"';
                                        }
                                        ?>>
                                            <span class="list-icon">&nbsp;</span><?php
                                            if ($this->lang->line('admin_menu_drivers_list') != '')
                                                echo stripslashes($this->lang->line('admin_menu_drivers_list'));
                                            else
                                                echo 'Drivers List';
                                            ?>
                                        </a>
                                    </li>
                                    <?php
                                }
                                ?>

                                <?php
                                if (in_array("add_driver_form", $schooladminPrevs["driver"])) {
                                    ?>
                                    <li>
                                        <a href="<?php echo OPERATOR_NAME; ?>/drivers/add_driver_form" <?php
                                        if ($currentPage == 'add_driver_form') {
                                            echo 'class="active"';
                                        }
                                        ?>>
                                            <span class="list-icon">&nbsp;</span><?php
                                            if ($this->lang->line('admin_menu_add_driver') != '')
                                                echo stripslashes($this->lang->line('admin_menu_add_driver'));
                                            else
                                                echo 'Add Driver';
                                            ?>
                                        </a>
                                    </li>
                                    <?php
                                }
                                ?>
                            </ul>

                        </li>

                        <?php
                    }
                    ?>




                    <?php
                    if (isset($schooladminPrevs["trip"])) {
                        ?>	
                        <li>
                            <a href="<?php echo $currentUrl; ?>" <?php
                            if ($currentUrl == 'trip' || $currentPage == 'assign_route_view') {
                                echo 'class="active"';
                            }
                            ?>>
                                <span class="nav_icon car"></span> <?php
                                if ($this->lang->line('heading_trip_management') != '')
                                    echo stripslashes($this->lang->line('heading_trip_management'));
                                else
                                    echo 'Trip Management';
                                ?><span class="up_down_arrow">&nbsp;</span>
                            </a>

                            <ul class="acitem" <?php
                            if ($currentUrl == 'trip') {
                                echo 'style="display: block;"';
                            } else {
                                echo 'style="display: none;"';
                            }
                            ?>>

                                <?php
                                if (in_array("assign_route_view", $schooladminPrevs["trip"])) {
                                    ?>	
                                    <li>
                                        <a href="<?php echo OPERATOR_NAME; ?>/trip/assign_route_view" <?php
                                        if ($currentPage == 'assign_route_view') {
                                            echo 'class="active"';
                                        }
                                        ?>>
                                            <span class="list-icon">&nbsp;</span><?php
                                            if ($this->lang->line('add_route_trip') != '')
                                                echo stripslashes($this->lang->line('add_route_trip'));
                                            else
                                                echo 'Add Trip';
                                            ?>
                                        </a>
                                    </li>
                                    <?php
                                }
                                ?>	

                                <?php
                                if (in_array("view_trip", $schooladminPrevs["trip"]) || in_array("edit_trip_bus", $schooladminPrevs["trip"])) {
                                    ?>	
                                    <li>
                                        <a href="<?php echo OPERATOR_NAME; ?>/trip/trip_list_view" <?php
                                        if ($currentPage == 'trip_list_view') {
                                            echo 'class="active"';
                                        }
                                        ?>>
                                            <span class="list-icon">&nbsp;</span><?php
                                            if ($this->lang->line('trip_list_view') != '')
                                                echo stripslashes($this->lang->line('trip_list_view'));
                                            else
                                                echo 'Trip List';
                                            ?>
                                        </a>
                                    </li>

                                    <?php
                                }
                                ?>





                                <?php
                                if (in_array("trip_details_list", $schooladminPrevs["trip"])) {
                                    ?>	
                                    <li>
                                        <a href="<?php echo OPERATOR_NAME; ?>/trip/trip_details_list" <?php
                                        if ($currentPage == 'trip_details_list') {
                                            echo 'class="active"';
                                        }
                                        ?>>
                                            <span class="list-icon">&nbsp;</span><?php
                                            if ($this->lang->line('trip_details_list') != '')
                                                echo stripslashes($this->lang->line('trip_details_list'));
                                            else
                                                echo 'Trip History';
                                            ?>
                                        </a>
                                    </li>

                                    <?php
                                }
                                ?>









                            </ul>
                        </li>	
                        <?php
                    }
                    ?>



                    <?php if (isset($schooladminPrevs["bus_route"])) {
                        ?>	


                        <li>
                            <a href="<?php echo $current_url; ?>" <?php
                            if ($currentUrl == 'bus_route') {
                                echo 'class="active"';
                            }
                            ?>>
                                <span class="nav_icon globe"></span> <?php
                                if ($this->lang->line('bus_route_management_side_nav') != '')
                                    echo stripslashes($this->lang->line('bus_route_management_side_nav'));
                                else
                                    echo 'Route Management';
                                ?><span class="up_down_arrow">&nbsp;</span>
                            </a>
                            <ul class="acitem" <?php
                            if ($currentUrl == 'bus_route') {
                                echo 'style="display: block;"';
                            } else {
                                echo 'style="display: none;"';
                            }
                            ?>>



                                <?php
                                if (in_array("add_bus_route", $schooladminPrevs["bus_route"])) {
                                    ?>	


                                    <li>
                                        <a href="<?php echo OPERATOR_NAME; ?>/bus_route/add_bus_route" <?php
                                        if ($currentPage == 'add_bus_route') {
                                            echo 'class="active"';
                                        }
                                        ?>>
                                            <span class="list-icon">&nbsp;</span><?php
                                            if ($this->lang->line('add_bus_route_side_nav') != '')
                                                echo stripslashes($this->lang->line('add_bus_route_side_nav'));
                                            else
                                                echo 'Add Route';
                                            ?>
                                        </a>
                                    </li>

                                    <?php
                                }
                                ?>	

                                <?php
                                if (in_array("view_bus_route", $schooladminPrevs["bus_route"])) {
                                    ?>	

                                    <li>
                                        <a href="<?php echo OPERATOR_NAME; ?>/bus_route/list_bus_route" <?php
                                        if ($currentPage == 'list_bus_route') {
                                            echo 'class="active"';
                                        }
                                        ?>>
                                            <span class="list-icon">&nbsp;</span><?php
                                            if ($this->lang->line('route_list_bus_route_side_nav') != '')
                                                echo stripslashes($this->lang->line('route_list_bus_route_side_nav'));
                                            else
                                                echo 'Route List';
                                            ?>
                                        </a>
                                    </li>


                                    <?php
                                }
                                ?>	




                            </ul>
                        </li>

                    <?php } ?>






                    <?php if (isset($schooladminPrevs["add_bus"])) {
                        ?>	



                        <li>
                            <a href="<?php echo $current_url; ?>" <?php
                            if ($currentUrl == 'add_bus') {
                                echo 'class="active"';
                            }
                            ?>>
                                <span class="nav_icon" style="background: unset;"><img style="width:100%;    vertical-align: unset;" src="images/Bus.png"></span> <?php
                                if ($this->lang->line('vehicle_management_side_nav') != '')
                                    echo stripslashes($this->lang->line('vehicle_management_side_nav'));
                                else
                                    echo 'Vehicle Management';
                                ?><span class="up_down_arrow">&nbsp;</span>
                            </a>
                            <ul class="acitem" <?php
                            if ($currentUrl == 'add_bus') {
                                echo 'style="display: block;"';
                            } else {
                                echo 'style="display: none;"';
                            }
                            ?>>



                                <?php
                                if (in_array("add_bus_new", $schooladminPrevs["add_bus"])) {
                                    ?>	

                                    <li>
                                        <a href="<?php echo OPERATOR_NAME; ?>/add_bus/add_bus_new" <?php
                                        if ($currentPage == 'add_bus_new') {
                                            echo 'class="active"';
                                        }
                                        ?>>
                                            <span class="list-icon">&nbsp;</span><?php
                                            if ($this->lang->line('add_new_vehicle_nav') != '')
                                                echo stripslashes($this->lang->line('add_new_vehicle_nav'));
                                            else
                                                echo 'Add Vehicle';
                                            ?>
                                        </a>
                                    </li>
                                <?php } ?>






                                <?php
                                if (in_array("view_bus_details", $schooladminPrevs["add_bus"])) {
                                    ?>	




                                    <li>
                                        <a href="<?php echo OPERATOR_NAME; ?>/add_bus/bus_list" <?php
                                        if ($currentPage == 'bus_list') {
                                            echo 'class="active"';
                                        }
                                        ?>>
                                            <span class="list-icon">&nbsp;</span><?php
                                            if ($this->lang->line('vehicle_list_nav') != '')
                                                echo stripslashes($this->lang->line('vehicle_list_nav'));
                                            else
                                                echo 'Vehicle List';
                                            ?>
                                        </a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </li>


                    <?php } ?>







































                    <li>

                        <a href="<?php echo $current_url; ?>" <?php
                        if ($currentUrl == 'map') {
                            echo 'class="active"';
                        }
                        ?>>
                            <span class="nav_icon marker map-new"></span> <?php
                            if ($this->lang->line('admin_menu_map_view') != '')
                                echo stripslashes($this->lang->line('admin_menu_map_view'));
                            else
                                echo 'Map View';
                            ?><span class="up_down_arrow">&nbsp;</span>
                        </a>

                        <ul class="acitem" <?php
                        if ($currentUrl == 'map') {
                            echo 'style="display: block;"';
                        } else {
                            echo 'style="display: none;"';
                        }
                        ?>>

                            <li>
                                <a href="<?php echo OPERATOR_NAME; ?>/map/map_avail_drivers" <?php
                                if ($currentPage == 'map_avail_drivers') {
                                    echo 'class="active"';
                                }
                                ?>>
                                    <span class="list-icon">&nbsp;</span><?php
                                    if ($this->lang->line('admin_menu_view_available_drivers') != '')
                                        echo stripslashes($this->lang->line('admin_menu_view_available_drivers'));
                                    else
                                        echo 'View available drivers';
                                    ?>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo OPERATOR_NAME; ?>/map/map_avail_users" <?php
                                if ($currentPage == 'map_avail_users') {
                                    echo 'class="active"';
                                }
                                ?>>
                                    <span class="list-icon">&nbsp;</span><?php
                                    if ($this->lang->line('admin_menu_view_available_users') != '')
                                        echo stripslashes($this->lang->line('admin_menu_view_available_users'));
                                    else
                                        echo 'View available users';
                                    ?>
                                </a>
                            </li>

                        </ul>

                    </li>








                    <!-- notification -->


                    <?php if (isset($schooladminPrevs["notification"])) {
                        ?>	




                        <li>
                            <a href="<?php echo $current_url; ?>" <?php
                            if ($currentUrl == 'notification' || $currentPage == 'display_notification_user_list') {
                                echo 'class="active"';
                            }
                            ?>>
                                <span class="nav_icon users"></span> <?php
                                if ($this->lang->line('admin_menu_notification') != '')
                                    echo stripslashes($this->lang->line('admin_menu_notification'));
                                else
                                    echo 'Notification';
                                ?><span class="up_down_arrow">&nbsp;</span>
                            </a>




                            <ul class="acitem" <?php
                            if ($currentUrl == 'notification' || $currentPage == 'display_notification_user_list' || $currentPage == 'display_notification_driver_list') {
                                echo 'style="display: block;"';
                            } else {
                                echo 'style="display: none;"';
                            }
                            ?>>


                                <?php
                                if (in_array("send_users_notification", $schooladminPrevs["notification"])) {
                                    ?>	

                                    <li>
                                        <a href="<?php echo OPERATOR_NAME; ?>/notification/display_notification_driver_list" <?php
                                        if ($currentPage == 'display_notification_driver_list') {
                                            echo 'class="active"';
                                        }
                                        ?>>
                                            <span class="list-icon"></span> <?php
                                            if ($this->lang->line('admin_menu_drivers') != '')
                                                echo stripslashes($this->lang->line('admin_menu_drivers'));
                                            else
                                                echo 'Drivers';
                                            ?>
                                        </a>
                                    </li>

                                <?php } ?>



                                <?php
                                if (in_array("send_drivers_notification", $schooladminPrevs["notification"])) {
                                    ?>	

                                    <li>
                                        <a href="<?php echo OPERATOR_NAME; ?>/notification/display_notification_user_list" <?php
                                        if ($currentPage == 'display_notification_user_list') {
                                            echo 'class="active"';
                                        }
                                        ?>>
                                            <span class="list-icon"></span> <?php
                                            if ($this->lang->line('admin_menu_parents') != '')
                                                echo stripslashes($this->lang->line('admin_menu_parents'));
                                            else
                                                echo 'Parents';
                                            ?>
                                        </a>
                                    </li>

                                <?php } ?>

                                <?php
                                if (in_array("notification_templates", $schooladminPrevs["notification"])) {
                                    ?>	

                                    <li>
                                        <a href="<?php echo OPERATOR_NAME; ?>/notification/display_notification_templates" <?php
                                        if ($currentPage == 'display_notification_templates' || $currentPage == 'edit_notification_template' || $currentPage == 'view_notification_template') {
                                            echo 'class="active"';
                                        }
                                        ?> >
                                            <span class="list-icon">&nbsp;</span><?php
                                            if ($this->lang->line('admin_menu_notification_templates') != '')
                                                echo stripslashes($this->lang->line('admin_menu_notification_templates'));
                                            else
                                                echo 'Notification Templates';
                                            ?>
                                        </a>
                                    </li>

                                <?php } 
                                if (!in_array("add_notification", $schooladminPrevs["notification"])) {
                                    ?>	

                                    <li>
                                        <a href="<?php echo OPERATOR_NAME ?>/notification/add_notification_template" <?php
                                        if ($currentPage == 'add_notification_template') {
                                            echo 'class="active"';
                                        }
                                        ?> >
                                            <span class="list-icon">&nbsp;</span><?php
                                           
                                                echo 'Add New Notification';
                                            ?>
                                        </a>
                                    </li>

                                <?php } ?>
                            </ul>
                        </li>


                    <?php } ?>

                    <!-- logs -->


                    <?php if (isset($schooladminPrevs["logs"])) {
                        ?>	



                        <li>
                            <a href="<?php echo $current_url; ?>" <?php
                            if ($currentUrl == 'logs' || $currentPage == 'display_parent_logs') {
                                echo 'class="active"';
                            }
                            ?>>
                                <span class="nav_icon folder"></span> Logs<span class="up_down_arrow">&nbsp;</span>
                            </a>




                            <ul class="acitem" <?php
                            if ($currentUrl == 'logs' || $currentPage == 'display_parent_logs' || $currentPage == 'display_driver_logs') {
                                echo 'style="display: block;"';
                            } else {
                                echo 'style="display: none;"';
                            }
                            ?>>


                                <?php
                                if (in_array("display_parent_logs", $schooladminPrevs["logs"])) {
                                    ?>	

                                    <li>
                                        <a href="<?php echo OPERATOR_NAME; ?>/logs/display_parent_logs" <?php
                                        if ($currentPage == 'display_parent_logs') {
                                            echo 'class="active"';
                                        }
                                        ?>>
                                            <span class="list-icon"></span> Parent Notification Log
                                        </a>
                                    </li>

                                <?php } ?>


                                <?php
                                if (in_array("display_driver_logs", $schooladminPrevs["logs"])) {
                                    ?>	

                                    <li>
                                        <a href="<?php echo OPERATOR_NAME; ?>/logs/display_driver_logs" <?php
                                        if ($currentPage == 'display_driver_logs') {
                                            echo 'class="active"';
                                        }
                                        ?>>
                                            <span class="list-icon"></span> Driver Notification Log
                                        </a>
                                    </li>

                                <?php } ?>

                            </ul>
                        </li>

                    <?php } ?>
















































                </ul>
            </div>
        </div>
    </div>


