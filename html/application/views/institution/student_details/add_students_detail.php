<?php
$this->load->view(OPERATOR_NAME . '/templates/header.php');
?>
<div id="content" class="admin-settings edit-global-set add_drive_catagory">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">
                <div class="widget_top">
                    <span class="h_icon list"></span>
                    <h6><?php echo $heading; ?></h6>
                </div>

                <script>

<?php if ($bus_route_list->num_rows() == 0 && $bus_trip_list->num_rows() == 0) {
    ?>

                        $(document).ready(function () {
                            validate_count();
                        });


                        function validate_count()
                        {
                            var message = "Create a route and trip to start with. Would you like to create a route ?";

                            var url = "<?php echo OPERATOR_NAME; ?>/bus_route/add_bus_route";

                            var return_url = "<?php echo OPERATOR_NAME; ?>";

                            return student_no_trip(message, url, return_url);
                        }

    <?php
} elseif ($bus_trip_list->num_rows() == 0) {
    ?>




                        $(document).ready(function () {
                            validate_count();
                        });


                        function validate_count()
                        {
                            var message = "Create a  trip to start with. Would you like to create a Trip ?";



                            var url = "<?php echo OPERATOR_NAME; ?>/trip/assign_route_view";
                            var return_url = "<?php echo OPERATOR_NAME; ?>";
                            return student_no_trip(message, url, return_url);

                        }



<?php }
?>



                </script>   

                <div class="widget_content">




<?php
$attributes = array('class' => 'form_container left_label', 'id' => 'add_student_form', 'enctype' => 'multipart/form-data');
echo form_open_multipart(OPERATOR_NAME . '/add_students/insertEdit_student_details', $attributes)
?>

                    <ul class="left-contsec operator_drive_sec">

                        <li>
                            <div class="form_grid_12" style="width:100% !important;">
                                <h3 style="width:100% !important;"><?php if ($this->lang->line('dash_add_student_detail_category') != '') echo stripslashes($this->lang->line('dash_add_student_detail_category'));
else echo 'Student Details'; ?></h3>
                            </div>   
                        </li>


                        <li> 
                            <div class="form_grid_12">
                                <label class="field_title"><?php if ($this->lang->line('operator_add_student_name_category') != '') echo stripslashes($this->lang->line('operator_add_student_name_category'));
else echo 'First Name'; ?> <span class="req">*</span></label>
                                <div class="form_input">                                    
                                    <input name="student_name" id="student_name_id" type="text"  class="required large tipTop alphanumeric" title="<?php if ($this->lang->line('operator_students_upload_enter_student_firstname') != '') echo stripslashes($this->lang->line('operator_students_upload_enter_student_firstname'));
else echo 'Please enter the Student First Name'; ?>"/>
                                </div>
                            </div>

                            <div class="form_input_right">
                                <label class="field_title"><?php if ($this->lang->line('operator_add_student_lastname_category') != '') echo stripslashes($this->lang->line('operator_add_student_lastname_category'));
else echo 'Last Name'; ?> </label>
                                <div class="form_input">                                    
                                    <input name="student_last_name" id="student_last_name_id" type="text"  class=" large tipTop" title="<?php if ($this->lang->line('operator_students_upload_enter_student_lastname') != '') echo stripslashes($this->lang->line('operator_students_upload_enter_student_lastname'));
else echo 'Please enter the Student Last Name'; ?>"/>
                                </div>                               
                            </div>
                        </li>


                        <li>
                            <div class="form_grid_12">
                                <label class="field_title"><?php if ($this->lang->line('operator_add_student_gender_category') != '') echo stripslashes($this->lang->line('operator_add_student_gender_category'));
else echo 'Gender'; ?> <span class="req">*</span></label>

                                <div class="form_input">   
                                    <select name="student_gender_title" id="student_gender_title_id"  class="required select_props" >
                                        <option value=""><?php if ($this->lang->line('operator_student_select_gender') != '') echo stripslashes($this->lang->line('operator_student_select_gender'));
else echo 'Select Gender'; ?></option>                                        
                                        <option value="Male" data-vehicle="">Male</option>  
                                        <option value="Female" data-vehicle="">Female</option>                                         
                                    </select>
                                </div>
                            </div>

                            <div class="form_input_right">
                                <label class="field_title"><?php if ($this->lang->line('operator_add_student_dob_category') != '') echo stripslashes($this->lang->line('operator_add_student_dob_category'));
else echo 'Date of Birth'; ?> </label>                                
                                <div class="form_input">
                                    <input placeholder="<?php if ($this->lang->line('operator_add_student_dob_category') != '') echo stripslashes($this->lang->line('operator_add_student_dob_category'));
else echo 'Date of Birth'; ?>" name="student_date_of_birth" id="student_date_of_birth_id" type="date"  class="large dob_column_size select_props"  />
                                </div>                                                                
                            </div>
                        </li>


                        <li>
                            <div class="form_grid_12">                                
                                <label class="field_title"><?php if ($this->lang->line('operator_add_student_class_title_category') != '') echo stripslashes($this->lang->line('operator_add_student_class_title_category'));
else echo 'Class'; ?> </label>
                                <div class="form_input"> 
                                    <select onchange="class_based_section()" name="student_class_title" id="student_class_title_id"  class="select_props" >
                                        <option value=""><?php if ($this->lang->line('operator_student_select_class') != '') echo stripslashes($this->lang->line('operator_student_select_class'));
                                        else echo 'Select Class'; ?></option>
                                        <?php
                                        $class_with_sect_id = [];
                                        if ($class_list->num_rows() > 0) {


                                            foreach ($class_list->result() as $classlist) {
                                                $class_with_sect_id[(string) $classlist->_id] = $classlist->section_ids;

                                                if (isset($classlist->class_name)) {
                                                    $studentclass_name = $classlist->class_name;
                                                } else {
                                                    $studentclass_name = '';
                                                }
                                                ?>
                                                <option value="<?php echo $classlist->_id; ?>" data-vehicle="<?php echo $studentclass_name; ?>">
                                                    <?php echo $studentclass_name; ?>
                                                </option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form_input_right">
                                <label class="field_title"><?php if ($this->lang->line('operator_add_student_section_title_category') != '') echo stripslashes($this->lang->line('operator_add_student_section_title_category'));
                                        else echo 'Section'; ?> </label>
                                <div class="form_input">   
                                    <select name="student_section_title" id="student_section_title_id"  class="select_props" >

                                    </select>
                                </div>
                            </div>
                        </li>

                        <li>

                            <div class="form_grid_12">
                                <label class="field_title"><?php if ($this->lang->line('admin_subadmin_status') != '') echo stripslashes($this->lang->line('admin_subadmin_status'));
                                        else echo 'Status'; ?></label>
                                <div class="form_input">
                                    <div class="active_inactive">
                                        <input type="checkbox"  name="status" checked="checked" id="active_inactive_active" class="active_inactive"/>
                                    </div>
                                </div>
                            </div>


                            <div class="form_input_right">
                                <label class="field_title"><?php if ($this->lang->line('operator_add_student_school_id_category') != '') echo stripslashes($this->lang->line('operator_add_student_school_id_category'));
                                        else echo 'Student ID'; ?> </label>
                                <div class="form_input">                                    
                                    <input name="student_school_id" id="student_school_id" type="text"  class=" large tipTop" title="<?php if ($this->lang->line('operator_add_student_school_id_category') != '') echo stripslashes($this->lang->line('operator_add_student_school_id_category'));
                                        else echo 'Student ID'; ?>"/>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="form_grid_12">
                                <label class="field_title">Allow parent to update location</label>
                                <div class="form_input">
                                    <div class="active_inactive">
                                        <input value="1" type="checkbox"  name="parent_update_location" id="parent_update_location" class="active_inactive"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form_input_right">
                                <label class="field_title"><?php if ($this->lang->line('operator_add_student_trip_type_category') != '') echo stripslashes($this->lang->line('operator_add_student_trip_type_category'));
                                        else echo 'Trip Type'; ?> <span class="req">*</span></label>                              
                                <div class="form_input">   
                                    <select name="student_trip_type_title" id="student_trip_type_title_id"  class="required select_props"  onchange="get_trip_type(this.options[this.selectedIndex].value);">
                                        <option value=""><?php if ($this->lang->line('operator_student_select_trip_type') != '') echo stripslashes($this->lang->line('operator_student_select_trip_type'));
                                        else echo 'Select Trip Type'; ?></option>                                        
                                        <option value="pickup" data-vehicle="">One way(Home to School)</option>  
                                        <option value="drop" data-vehicle="">One way(School to Home)</option> 
                                        <option selected value="both" data-vehicle="">Two way</option>
                                        <option value="notrip" data-vehicle="">Student not taking this</option>
                                    </select>
                                </div>
                            </div>
                        </li>
                        <div id="multiple_field_trip_pickup" style="display:none;">
                            <li>
                                <div class="form_grid_12" style="width:100% !important;">
                                    <h3 style="width:100% !important;"><?php if ($this->lang->line('dash_add_student_pickup_category') != '') echo stripslashes($this->lang->line('dash_add_student_pickup_category'));
                                        else echo 'Pickup'; ?></h3>
                                </div>
                            </li>
                            <li>
                                <div class="form_grid_12">
                                    <label class="field_title"><?php if ($this->lang->line('operator_add_student_trip_name_category') != '') echo stripslashes($this->lang->line('operator_add_student_trip_name_category'));
                                        else echo 'Trip Name'; ?> <span class="req">*</span></label>
                                    <div class="form_input">   
                                        <select name="student_trip_name_title" id="student_trip_name_title_id"  class="select_props"  onchange="onchange_trip_name();">  
                                            <option value=""><?php if ($this->lang->line('operator_student_select_trip_name') != '') echo stripslashes($this->lang->line('operator_student_select_trip_name'));
                                        else echo 'Select Trip Name'; ?></option>                                           
                                        </select>
                                    </div>
                                </div>
                                <div class="form_input_right">                                
                                    <label class="field_title"><?php if ($this->lang->line('operator_student_bus_stop_title') != '') echo stripslashes($this->lang->line('operator_student_bus_stop_title'));
                                        else echo 'Bus Stop'; ?><span class="req">*</span></label>
                                    <div class="form_input">
                                        <select name="bus_stop_pickup" id="bus_stop_pickup_id"  class="select_props" >
                                            <option value=""><?php if ($this->lang->line('operator_student_select_bus_stop') != '') echo stripslashes($this->lang->line('operator_student_select_bus_stop'));
                                        else echo 'Select Bus Stop'; ?></option>                                      
                                        </select>
                                    </div>
                                </div>
                            </li>
                        </div>

                        <div id="multiple_field_trip_drop" style="display:none;">
                            <li>
                                <div class="form_grid_12" style="width:100% !important;">
                                    <h3 style="width:100% !important;"><?php if ($this->lang->line('dash_add_student_drop_category') != '') echo stripslashes($this->lang->line('dash_add_student_drop_category'));
                                        else echo 'Drop'; ?></h3>
                                </div>
                            </li>
                            <li>
                                <div class="form_grid_12">
                                    <label class="field_title"><?php if ($this->lang->line('operator_add_student_trip_name_category') != '') echo stripslashes($this->lang->line('operator_add_student_trip_name_category'));
                                        else echo 'Trip Name'; ?> <span class="req">*</span></label>
                                    <div class="form_input">   
                                        <select name="student_trip_name_title_drop" id="student_trip_name_title_drop_id"  class="select_props"  onchange="onchange_trip_name_drop();">
                                            <option value=""><?php if ($this->lang->line('operator_student_select_trip_name') != '') echo stripslashes($this->lang->line('operator_student_select_trip_name'));
                                        else echo 'Select Trip Name'; ?></option>                                            
                                        </select>
                                    </div>
                                </div>
                                <div class="form_input_right">                                
                                    <label class="field_title"><?php if ($this->lang->line('operator_student_bus_stop_title') != '') echo stripslashes($this->lang->line('operator_student_bus_stop_title'));
                                        else echo 'Bus Stop'; ?><span class="req">*</span></label>
                                    <div class="form_input">
                                        <select name="bus_stop_drop" id="bus_stop_drop_id"  class="select_props" >
                                            <option value=""><?php if ($this->lang->line('operator_student_select_bus_stop') != '') echo stripslashes($this->lang->line('operator_student_select_bus_stop'));
                                        else echo 'Select Bus Stop'; ?></option>                                      
                                        </select>
                                    </div>
                                </div>
                            </li>
                        </div> 

                    </ul>












                    <ul class="rite-contsec operator_drive_sec">

                        <li>
                            <div class="form_grid_12" style="width:100% !important;">
                                <h3><?php if ($this->lang->line('guardian') != '') echo stripslashes($this->lang->line('guardian'));
                                        else echo 'Guardian'; ?></h3>
                            </div>
                        </li>

                        <li>                        
                            <div class="form_grid_12">


                                <label class="field_title"><?php if ($this->lang->line('parent_type') != '') echo stripslashes($this->lang->line('parent_type'));
                                        else echo 'Guardian Type'; ?> <span class="req">*</span></label>                              
                                <div class="form_input">   
                                    <select name="guardian_type[0]" id="guardian_type"  class="required select_props"  >
                                        <option value="">Select Guardian Type</option>                                        
                                        <option value="father" data-vehicle="">Father</option>  
                                        <option value="mother" data-vehicle="">Mother</option> 
                                        <option value="guardian" data-vehicle="">Guardian</option>
                                        <option value="helper" data-vehicle="">Helper</option>
                                    </select>
                                </div>






                            </div>



                            <!-- Name-->
                            <div class="form_input_right">



                                <label class="field_title"><?php if ($this->lang->line('operator_add_student_name_category') != '') echo stripslashes($this->lang->line('operator_add_student_name_category'));
                                        else echo 'Name'; ?> </label>
                                <div class="form_input">                                    
                                    <input name="parent_name[0]" id="parent_name_id" type="text"  class="large tipTop" title="<?php if ($this->lang->line('operator_students_upload_enter_parent_firstname') != '') echo stripslashes($this->lang->line('operator_students_upload_enter_parent_firstname'));
                                        else echo 'Please enter the Parent First Name'; ?>"/>
                                </div>  













                            </div>
                        </li>                      




                        <li>





                            <!-- email-->

                            <div class="form_grid_12">





                                <label class="field_title"><?php if ($this->lang->line('admin_drivers_email_address') != '') echo stripslashes($this->lang->line('admin_drivers_email_address'));
                                        else echo 'Email Address'; ?> </label>
                                <div class="form_input">
                                    <input name="email[0]" id="email" type="text"  class="large tipTop email" title="<?php if ($this->lang->line('operator_students_enter_parent_email_address') != '') echo stripslashes($this->lang->line('operator_students_enter_parent_email_address'));
                                        else echo 'Please enter the Parent Email address'; ?>"/>
                                </div>









                            </div>


                            <!-- phone -->

                            <div class="form_input_right">





                                <label class="field_title"><?php if ($this->lang->line('admin_rides_mobile_number') != '') echo stripslashes($this->lang->line('admin_rides_mobile_number'));
                                        else echo 'Mobile Number'; ?><span class="req">*</span></label>
                                <div class="form_input">



                                    <!-- phone code -->



                                    <select style="width: 15% !important;" id="country_code" name="country_code[0]" class="required small tipTop chzn-select"  title="<?php if ($this->lang->line('driver_enter_mobile_country_code') != '') echo stripslashes($this->lang->line('driver_enter_mobile_country_code'));
                                        else echo 'Please enter mobile country code'; ?>" >


<?php echo $html; ?>

                                    </select> 










                                    <!-- phone number -->


                                    <input style="width: 80% !important;height: 37px;" name="mobile_number[0]" placeholder="<?php if ($this->lang->line('admin_drivers_mobile_number') != '') echo stripslashes($this->lang->line('admin_drivers_mobile_number'));
else echo 'Mobile Number'; ?>" id="mobile_number" type="text"  class="required medium tipTop phoneNumber" maxlength="20" title="<?php if ($this->lang->line('driver_enter_mobile_number') != '') echo stripslashes($this->lang->line('driver_enter_mobile_number'));
else echo 'Please enter the mobile number'; ?>" />
                                </div>










                            </div>
                        </li>




                        <li id="family_add_button" style="text-align: right;">
                            <span onclick="add_family()" class="btn btn-success">ADD</span>
                        </li>


                        <!-- <input name="bus_route_id_hidden" id="bus_route_id_hidden" type="hidden" value="" />
                        <input name="bus_stop_id_hidden" id="bus_stop_id_hidden" type="hidden" value="" /> -->

                    </ul>












                    <ul class="last-sec-btn">
                        <li class="change-pass" style="margin-left: 25%;">
                            <div class="form_grid_12">
                                <div class="form_input">

                                    <span onclick="cancel_submit();" type="cancel" class="btn_small btn_blue"><?php if ($this->lang->line('admin_common_cancel') != '') echo stripslashes($this->lang->line('admin_common_cancel'));
else echo 'Cancel'; ?></span>

                                    <button type="submit" class="btn_small btn_cancel" ><span><?php if ($this->lang->line('admin_subadmin_submit') != '') echo stripslashes($this->lang->line('admin_subadmin_submit'));
else echo 'Submit'; ?></span></button>


                                </div>
                            </div>
                        </li>
                    </ul>



                    </form>

                </div>
            </div>
        </div>
    </div>
    <span class="clear"></span>
</div>
</div>



<style>





    ul.left-contsec {
        width:100% !important;
    }

    .admin-settings li .form_grid_12 {
        width:47% !important;
    }

    .left_label ul li .form_input {
        width:100% !important;        
    }


    .form_container ul li {
        width:100% !important;  
    }


    .form_input_right
    {
        float: right;
        width: 47%;
    }


    .dob_column_size
    {
        height: 37px;

    }

    ul.rite-contsec {
        width:100% !important;
    }

    .expiry_box {
        background: none repeat scroll 0 0 gainsboro;
        border: 1px solid grey;
        border-radius: 5px;
        margin-top: 2%;
        padding: 1%;
        width: 23%;
    }


    .expiry_box input {
        width:50% !important;
        border-radius: 5px;
        border: 1px solid grey !important;
    }


    .admin-settings.edit-global-set.add_drive_catagory ul.rite-contsec {
        min-height: auto;
    }

    .admin-settings.edit-global-set.add_drive_catagory ul.operator_drive_sec {
        min-height: auto;
    }

</style>



<script>





    var select_html_dialcode = "<?php echo $html; ?>";













    function cancel_submit()
    {
        window.location.href = "institution/add_students/student_lists";
    }


    function class_based_section()
    {
        var section_list =<?php echo json_encode($section_list->result()); ?>;

        var class_id_section =<?php echo json_encode($class_with_sect_id); ?>;



        var class_id = document.getElementById('student_class_title_id').value;

        var html = '<option value=""><?php if ($this->lang->line('operator_student_select_section') != '') echo stripslashes($this->lang->line('operator_student_select_section'));
else echo 'Select Section'; ?></option>';

        for (var i = 0; i < section_list.length; i++)
        {

            if (typeof class_id_section[class_id] != "undefined" && (class_id_section[class_id] instanceof Array)) {

                if (class_id_section[class_id].includes(section_list[i]['_id'].$oid))
                {
                    html = html + '<option value="' + section_list[i]['_id'].$oid + '" data-vehicle="' + section_list[i]['section_name'] + '">' + section_list[i]['section_name'] + '</option>';
                }

            }

        }

        console.log(html);

        $("#student_section_title_id").html(html);

    }
































    var appender_counter = 1;

    function add_family()
    {




        if ($("div.guardian_new").size() < 4) {
            var appender_html = '<div class="guardian_new" id="appeder_guardian_' + appender_counter + '"><hr><li style="text-align: right;cursor: pointer;"><span onclick="destroy_details(\'appeder_guardian_' + appender_counter + '\')" class="glyphicon glyphicon-trash"></span></li><li><div class="form_grid_12"><label class="field_title"><?php if ($this->lang->line('parent_type') != '') echo stripslashes($this->lang->line('parent_type'));
else echo 'Guardian Type'; ?> <span class="req">*</span></label><div class="form_input"><select name="guardian_type[' + appender_counter + ']" id="guardian_type_' + appender_counter + '"  class="required select_props"  ><option value="">Select Guardian Type</option><option value="father" data-vehicle="">Father</option><option value="mother" data-vehicle="">Mother</option><option value="guardian" data-vehicle="">Guardian</option><option value="helper" data-vehicle="">Helper</option></select></div></div><div class="form_input_right"><label class="field_title"><?php if ($this->lang->line('operator_add_student_name_category') != '') echo stripslashes($this->lang->line('operator_add_student_name_category'));
else echo 'Name'; ?> </label><div class="form_input"><input name="parent_name[' + appender_counter + ']" id="parent_name_id_' + appender_counter + '" type="text"  class="large tipTop" title="<?php if ($this->lang->line('operator_students_upload_enter_parent_firstname') != '') echo stripslashes($this->lang->line('operator_students_upload_enter_parent_firstname'));
else echo 'Please enter the Parent First Name'; ?>"/></div></div></li><li><div class="form_grid_12"><label class="field_title"><?php if ($this->lang->line('admin_drivers_email_address') != '') echo stripslashes($this->lang->line('admin_drivers_email_address'));
else echo 'Email Address'; ?> </label><div class="form_input"><input name="email[' + appender_counter + ']" id="email_' + appender_counter + '" type="text"  class="large tipTop email" title="<?php if ($this->lang->line('operator_students_enter_parent_email_address') != '') echo stripslashes($this->lang->line('operator_students_enter_parent_email_address'));
else echo 'Please enter the Parent Email address'; ?>"/></div></div><div class="form_input_right"><label class="field_title"><?php if ($this->lang->line('admin_rides_mobile_number') != '') echo stripslashes($this->lang->line('admin_rides_mobile_number'));
else echo 'Mobile Number'; ?><span class="req">*</span></label><div class="form_input"> <div style="float:left;margin-right: 6px;"> <select style="width: 15% !important;" id="country_code_' + appender_counter + '" name="country_code[' + appender_counter + ']" class="required small tipTop chzn-select"  title="<?php if ($this->lang->line('driver_enter_mobile_country_code') != '') echo stripslashes($this->lang->line('driver_enter_mobile_country_code'));
else echo 'Please enter mobile country code'; ?>" >' + select_html_dialcode + '</select> </div><div><input name="mobile_number[' + appender_counter + ']" placeholder="<?php if ($this->lang->line('admin_drivers_mobile_number') != '') echo stripslashes($this->lang->line('admin_drivers_mobile_number'));
else echo 'Mobile Number'; ?>" id="mobile_number_' + appender_counter + '" type="text"  class="required medium tipTop phoneNumber" style="width: 79.5% !important;height: 37px;" maxlength="20" title="<?php if ($this->lang->line('driver_enter_mobile_number') != '') echo stripslashes($this->lang->line('driver_enter_mobile_number'));
else echo 'Please enter the mobile number'; ?>" /></div></div></div></li></div>';



            $(appender_html).insertBefore("#family_add_button");


            appender_counter++;
            $(".chzn-select").chosen({no_results_text: no_results_text});
            validator_reset();
        }
        if ($("div.guardian_new").size() >= 4) {
            $('li#family_add_button').hide();
        }
    }





    function destroy_details(data)
    {

        $("#" + data).remove();
        if ($("div.guardian_new").size() < 4) {
            $('li#family_add_button').show();
        }
    }






































    $(document).ready(function () {

        $.validator.setDefaults({ignore: ":hidden"});


        $("#add_student_form").validate();

        get_trip_type("both");

    });



    function validator_reset()
    {

        $.validator.setDefaults({ignore: ":hidden"});
        $("#add_student_form").validate();


    }






//      Bus Route list array
    var array_bus_route = new Array();
<?php
$bus_route_list = $bus_route_list->result();
$count = 0;

for ($i = 0; $i < sizeof($bus_route_list); $i++) {
    for ($j = 0; $j < sizeof($bus_route_list[$i]->waypoints); $j++) {
        if (is_object($bus_route_list[$i]->waypoints[$j]))
            $bus_route_list[$i]->waypoints[$j] = (array) $bus_route_list[$i]->waypoints[$j];
        if (is_object($bus_route_list[$i]->waypoints[$j]['way_stop']))
            $bus_route_list[$i]->waypoints[$j]['way_stop'] = (array) $bus_route_list[$i]->waypoints[$j]['way_stop'];
        ?>
            array_bus_route[<?php echo $count; ?>] = [];
            array_bus_route[<?php echo $count; ?>].push("<?php echo $bus_route_list[$i]->_id; ?>", "<?php echo $bus_route_list[$i]->waypoints[$j]['_id']; ?>", "<?php echo $bus_route_list[$i]->waypoints[$j]['way_stop']['location_name']; ?>", "<?php echo $bus_route_list[$i]->waypoints[$j]['way_stop']['busstop_name']; ?>");
        <?php
        $count++;
    }
}
?>

//      console.log(array_bus_route);

    /*
     
     function bus_based_bus_route(bus_id)
     {    
     var bus_route_ids = bus_id;     
     document.getElementById("bus_route_id_hidden").value=bus_route_ids;
     
     var html='<option value=""><?php if ($this->lang->line('operator_student_select_bus_route') != '') echo stripslashes($this->lang->line('operator_student_select_bus_route'));
else echo 'Select Bus Route'; ?></option>';
     
     for(var y=0;y<array_bus_route.length;y++)
     {
     var bus_routeid = array_bus_route[y][0];         
     
     if(bus_route_ids == bus_routeid)
     {
     var bus_location = array_bus_route[y][3];
     bus_location = bus_location.split(',');
     html = html+'<option value="'+array_bus_route[y][1]+'">'+bus_location+'</option>';
     }
     }
     $("#bus_route_id").html(html);
     }
     
     
     function get_bus_stops_id(bus_stopid)
     {   
     document.getElementById("bus_stop_id_hidden").value=bus_stopid;
     }
     
     */


//      Bus trip list array
    var array_bus_trip = new Array();
<?php
$bus_trip_lists = $bus_trip_list->result();
$count_trip = 0;
for ($k = 0; $k < sizeof($bus_trip_lists); $k++) {
    if (is_object($bus_trip_lists[$k]->route_details))
        $bus_trip_lists[$k]->{"route_details"} = (array) $bus_trip_lists[$k]->route_details;
    ?>
        array_bus_trip[<?php echo $count_trip; ?>] = [];
        array_bus_trip[<?php echo $count_trip; ?>].push("<?php echo $bus_trip_lists[$k]->_id; ?>", "<?php echo $bus_trip_lists[$k]->trip_type; ?>", "<?php echo $bus_trip_lists[$k]->trip_name; ?>", "<?php echo $bus_trip_lists[$k]->route_details['route_id']; ?>", "<?php echo $bus_trip_lists[$k]->route_details['route_name']; ?>", "<?php echo $bus_trip_lists[$k]->start_time; ?>");
    <?php
    $count_trip++;
}
?>

//  console.log(array_bus_trip);


    function get_trip_type(tripname)
    {
        if (tripname == "" || tripname == "notrip")
        {
            document.getElementById("multiple_field_trip_drop").style.display = "none";
            document.getElementById("multiple_field_trip_pickup").style.display = "none";

            document.getElementById("student_trip_name_title_id").value = "";
            document.getElementById("student_trip_name_title_drop_id").value = "";

            document.getElementById("bus_stop_pickup_id").value = "";
            document.getElementById("bus_stop_drop_id").value = "";
        }

        if (tripname == "pickup")
        {
            document.getElementById("multiple_field_trip_drop").style.display = "none";
            document.getElementById("student_trip_name_title_drop_id").value = "";
            document.getElementById("bus_stop_drop_id").value = "";

            $("#student_trip_name_title_drop_id").removeClass("required");
            $("#bus_stop_drop_id").removeClass("required");

            $("#student_trip_name_title_id").addClass("required");
            $("#bus_stop_pickup_id").addClass("required");


            document.getElementById("multiple_field_trip_pickup").style.display = "block";

            var html = '<option value=""><?php if ($this->lang->line('operator_student_select_trip_name') != '') echo stripslashes($this->lang->line('operator_student_select_trip_name'));
else echo 'Select Trip Name'; ?></option>';

            var bus_triptype = "pickup";

            for (var y = 0; y < array_bus_trip.length; y++)
            {
                var bus_trip_types = array_bus_trip[y][1];

                if (bus_triptype == bus_trip_types)
                {
                    var bus_tripname = array_bus_trip[y][2];

                    html = html + '<option value="' + array_bus_trip[y][0] + ',' + array_bus_trip[y][3] + '">' + bus_tripname + '</option>';
                }
            }
            $("#student_trip_name_title_id").html(html);
        }

        if (tripname == "drop")
        {
            document.getElementById("multiple_field_trip_pickup").style.display = "none";
            document.getElementById("student_trip_name_title_id").value = "";
            document.getElementById("bus_stop_pickup_id").value = "";


            $("#student_trip_name_title_id").removeClass("required");
            $("#bus_stop_pickup_id").removeClass("required");

            $("#student_trip_name_title_drop_id").addClass("required");
            $("#bus_stop_drop_id").addClass("required");


            document.getElementById("multiple_field_trip_drop").style.display = "block";

            var html = '<option value=""><?php if ($this->lang->line('operator_student_select_trip_name') != '') echo stripslashes($this->lang->line('operator_student_select_trip_name'));
else echo 'Select Trip Name'; ?></option>';

            var bus_triptype = "drop";

            for (var y = 0; y < array_bus_trip.length; y++)
            {
                var bus_trip_types = array_bus_trip[y][1];

                if (bus_triptype == bus_trip_types)
                {
                    var bus_tripname = array_bus_trip[y][2];

                    html = html + '<option value="' + array_bus_trip[y][0] + ',' + array_bus_trip[y][3] + '">' + bus_tripname + '</option>';
                }
            }
            $("#student_trip_name_title_drop_id").html(html);
        }

        if (tripname == "both")
        {
            //      Function for Pickup
            document.getElementById("multiple_field_trip_pickup").style.display = "block";
            document.getElementById("student_trip_name_title_id").value = "";
            document.getElementById("bus_stop_pickup_id").value = "";


            $("#student_trip_name_title_id").addClass("required");
            $("#bus_stop_pickup_id").addClass("required");





            var html = '<option value=""><?php if ($this->lang->line('operator_student_select_trip_name') != '') echo stripslashes($this->lang->line('operator_student_select_trip_name'));
else echo 'Select Trip Name'; ?></option>';

            var bus_triptype = "pickup";

            for (var y = 0; y < array_bus_trip.length; y++)
            {
                var bus_trip_types = array_bus_trip[y][1];

                if (bus_triptype == bus_trip_types)
                {
                    var bus_tripname = array_bus_trip[y][2];

                    html = html + '<option value="' + array_bus_trip[y][0] + ',' + array_bus_trip[y][3] + '">' + bus_tripname + '</option>';
                }
            }
            $("#student_trip_name_title_id").html(html);

            //      Function for Drop
            document.getElementById("multiple_field_trip_drop").style.display = "block";
            document.getElementById("student_trip_name_title_drop_id").value = "";
            document.getElementById("bus_stop_drop_id").value = "";

            $("#student_trip_name_title_drop_id").addClass("required");
            $("#bus_stop_drop_id").addClass("required");

            var html_drop = '<option value=""><?php if ($this->lang->line('operator_student_select_trip_name') != '') echo stripslashes($this->lang->line('operator_student_select_trip_name'));
else echo 'Select Trip Name'; ?></option>';

            var bus_triptype_dp = "drop";

            for (var m = 0; m < array_bus_trip.length; m++)
            {
                var bus_trip_types_dp = array_bus_trip[m][1];

                if (bus_triptype_dp == bus_trip_types_dp)
                {
                    var bus_tripname_dp = array_bus_trip[m][2];

                    html_drop = html_drop + '<option value="' + array_bus_trip[m][0] + ',' + array_bus_trip[m][3] + '">' + bus_tripname_dp + '</option>';
                }
            }
            $("#student_trip_name_title_drop_id").html(html_drop);
        }
    }


//      Bus stop data array
    var array_busroute_trip = new Array();

<?php
$bus_trip_routelists = $bus_trip_list->result();
$count_trip_route = 0;

for ($n = 0; $n < sizeof($bus_trip_routelists); $n++) {
    for ($j = 0; $j < sizeof($bus_trip_routelists[$n]->route_details['waypoints_details']); $j++) {
        if (is_object($bus_trip_routelists[$n]->route_details['waypoints_details'][$j]))
            $bus_trip_routelists[$n]->route_details['waypoints_details'][$j] = (array) $bus_trip_routelists[$n]->route_details['waypoints_details'][$j];
        ?>
            array_busroute_trip[<?php echo $count_trip_route; ?>] = [];
            array_busroute_trip[<?php echo $count_trip_route; ?>].push("<?php echo $bus_trip_routelists[$n]->_id; ?>", "<?php echo $bus_trip_routelists[$n]->route_details['waypoints_details'][$j]['id']; ?>", "<?php echo $bus_trip_routelists[$n]->route_details['waypoints_details'][$j]['busstop_name']; ?>");
        <?php
        $count_trip_route++;
    }
}
?>
    console.log(array_busroute_trip);

//      onchange function for Pickup -> Trip Name
    function onchange_trip_name()
    {
        var trip_id_pickup_all = document.getElementById("student_trip_name_title_id").value;
        bus_trip_id_route_id = trip_id_pickup_all.split(',');
        var trip_id_pickup = bus_trip_id_route_id[0];

        if (trip_id_pickup != "")
        {

            var pickup_html = '<option value=""><?php if ($this->lang->line('operator_student_select_bus_stop') != '') echo stripslashes($this->lang->line('operator_student_select_bus_stop'));
else echo 'Select Bus Stop'; ?></option>';

            for (var m = 0; m < array_busroute_trip.length; m++)
            {
                var bus_trip_stop_dp = array_busroute_trip[m][0];

                if (trip_id_pickup == bus_trip_stop_dp)
                {
                    var bus_stopname_dp = array_busroute_trip[m][2];

                    console.log(bus_stopname_dp);

                    pickup_html = pickup_html + '<option value="' + array_busroute_trip[m][0] + ',' + array_busroute_trip[m][1] + '">' + bus_stopname_dp + '</option>';
                }
            }

            $("#bus_stop_pickup_id").html(pickup_html);

        }
        else
        {
            document.getElementById("bus_stop_pickup_id").value = "";
        }
    }

//      onchange function for Drop -> Trip Name
    function onchange_trip_name_drop()
    {
        var trip_id_drop_all = document.getElementById("student_trip_name_title_drop_id").value;
        bus_trip_id_route_id_dp = trip_id_drop_all.split(',');
        var trip_id_drop = bus_trip_id_route_id_dp[0];

        if (trip_id_drop != "")
        {
            var drop_html = '<option value=""><?php if ($this->lang->line('operator_student_select_bus_stop') != '') echo stripslashes($this->lang->line('operator_student_select_bus_stop'));
else echo 'Select Bus Stop'; ?></option>';

            for (var m = 0; m < array_busroute_trip.length; m++)
            {
                var bus_trip_stop_dp = array_busroute_trip[m][0];

                if (trip_id_drop == bus_trip_stop_dp)
                {
                    var bus_stopname_dp = array_busroute_trip[m][2];

                    drop_html = drop_html + '<option value="' + array_busroute_trip[m][0] + ',' + array_busroute_trip[m][1] + '">' + bus_stopname_dp + '</option>';
                }
            }

            $("#bus_stop_drop_id").html(drop_html);

        }
        else
        {
            document.getElementById("bus_stop_drop_id").value = "";
        }
    }
</script>


<?php
$this->load->view(OPERATOR_NAME . '/templates/footer.php');
?>