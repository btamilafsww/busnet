<?php
$this->load->view(OPERATOR_NAME . '/templates/header.php');
?>
<script src="js/papaparse/js/common.js"></script>
<script src="js/papaparse/js/papaparse.js"></script>
<div id="content" class="admin-settings edit-global-set add_drive_catagory">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">
                <div class="widget_top">
                    <span class="h_icon list"></span>
                    <h6><?php echo $heading; ?></h6>
                </div>
                <div class="widget_content">
                    <ul class="left-contsec operator_drive_sec">
                        <li>
                            <div class="form_grid_12" style="width:100% !important;">
                                <h3 style="width:100% !important;">Upload student list(CSV)</h3>
                            </div>   
                        </li>
                        <li> 
                            <div class="form_grid_12">
                                <label class="field_title">CSV File<span class="req">*</span></label>
                                <div class="form_input">                                    
                                    <input type="file" name="csvfile" id="csvfile" />
                                </div>
                                <div class="form_input_left">
                                    <button id="btnupload" type="button" class="btn_small btn_cancel" style="margin-top:10px"><span>Upload</span></button>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <span class="clear"></span>
</div>
</div>
<script type="text/javascript">
    $(document).on("click","#btnupload",function(e){
        $('input[name=csvfile]').parse({
	before: function(file, inputElem)
	{
		console.log(file);
                console.log(inputElem);
	},
	error: function(err, file, inputElem, reason)
	{
		console.log(err);
                console.log(reason);
	},
	complete: function(results, file)
	{
		console.log(results);
                console.log(file);
	}
});
    });
</script>
<?php
$this->load->view(OPERATOR_NAME . '/templates/footer.php');
?>