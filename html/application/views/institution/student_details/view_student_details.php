<?php
$this->load->view(OPERATOR_NAME . '/templates/header.php');

if ($this->lang->line('dash_not_available') != '')
    $not_available = stripslashes($this->lang->line('dash_not_available'));
else
    $not_available = 'Not Available';
?>
<div id="content" class="admin-settings edit-global-set add_drive_catagory">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">
                <div class="widget_wrap tabby">
                    <div class="widget_top"> 
                        <span class="h_icon list"></span>
                        <h6><?php echo $heading; ?></h6>

                        <div id="widget_tab">							
                        </div>

                    </div>

                    <div class="widget_content">
                        <?php
                        $attributes = array('class' => 'form_container left_label ajaxsubmit', 'id' => 'settings_form', 'enctype' => 'multipart/form-data');
                        echo form_open_multipart('admin/adminlogin/admin_global_settings', $attributes);

                        $student_details = $student_details->row();
                        ?>


                        <div id="tab1">

                            <ul class="left-contsec view_driver_list">

                                <li>
                                    <div class="form_grid_12" style="width:100% !important;">
                                        <h3 style="width:100% !important;"><?php
                                            if ($this->lang->line('dash_add_student_detail_category') != '')
                                                echo stripslashes($this->lang->line('dash_add_student_detail_category'));
                                            else
                                                echo 'Student Details';
                                            ?></h3>
                                    </div>   
                                </li>								

                                <li>
                                    <div class="form_grid_12">
                                        <label class="field_title"><?php
                                            if ($this->lang->line('operator_add_student_name_category') != '')
                                                echo stripslashes($this->lang->line('operator_add_student_name_category'));
                                            else
                                                echo 'First Name';
                                            ?></label>
                                        <div class="form_input">
                                            <p><?php echo $student_details->student_first_name; ?></p>
                                        </div>
                                    </div>

                                    <div class="form_input_right">
                                        <label class="field_title"><?php
                                            if ($this->lang->line('operator_add_student_lastname_category') != '')
                                                echo stripslashes($this->lang->line('operator_add_student_lastname_category'));
                                            else
                                                echo 'Last Name';
                                            ?> </label>
                                        <div class="form_input">
                                            <p><?php echo $student_details->student_last_name; ?></p>											
                                        </div>
                                    </div>


                                </li>


                                <li>
                                    <div class="form_grid_12">
                                        <label class="field_title"><?php
                                            if ($this->lang->line('operator_add_student_gender_category') != '')
                                                echo stripslashes($this->lang->line('operator_add_student_gender_category'));
                                            else
                                                echo 'Gender';
                                            ?> </label>
                                        <div class="form_input">
                                            <p><?php echo $student_details->student_gender; ?></p>											
                                        </div>
                                    </div>


                                    <div class="form_input_right">
                                        <label class="field_title"><?php
                                            if ($this->lang->line('operator_add_student_dob_category') != '')
                                                echo stripslashes($this->lang->line('operator_add_student_dob_category'));
                                            else
                                                echo 'Date of Birth';
                                            ?> </label>
                                        <div class="form_input">
                                            <p>
                                                <?php
                                                $student_date_of_birth = (!empty($student_details->student_dob)) ? date('d/m/Y', strtotime($student_details->student_dob)) : "";
                                                echo $student_date_of_birth;
                                                ?>
                                            </p>											
                                        </div>
                                    </div>
                                </li>



                                <li>
                                    <div class="form_grid_12">
                                        <label class="field_title"><?php
                                            if ($this->lang->line('operator_add_student_class_title_category') != '')
                                                echo stripslashes($this->lang->line('operator_add_student_class_title_category'));
                                            else
                                                echo 'Class';
                                            ?> </label>
                                        <div class="form_input">
                                            <p>
                                                <?php
                                                if ($student_details->student_class != "") {
                                                    $class_id = array('_id' => new MongoDB\BSON\ObjectId($student_details->student_class));

                                                    $class_detail = $this->class_details_model->get_all_details(CLASS_DETAILS, $class_id);



                                                    $class_detail = $class_detail->result();
                                                    if (isset($class_detail[0])) {
                                                        echo $class_detail[0]->class_name;
                                                    } else {
                                                        echo "-";
                                                    }
                                                } else {
                                                    echo "-";
                                                }
                                                ?>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="form_input_right">
                                        <label class="field_title"><?php
                                            if ($this->lang->line('operator_add_student_section_title_category') != '')
                                                echo stripslashes($this->lang->line('operator_add_student_section_title_category'));
                                            else
                                                echo 'Section';
                                            ?> </label>
                                        <div class="form_input">
                                            <p>
                                                <?php
                                                if ($student_details->student_section != "") {
                                                    $section_id = array('_id' => new MongoDB\BSON\ObjectId($student_details->student_section));

                                                    $section_detail = $this->class_details_model->get_all_details(SECTION_DETAILS, $section_id);

                                                    $section_detail = $section_detail->result();
                                                    if (isset($section_detail[0])) {
                                                        echo $section_detail[0]->section_name;
                                                    } else {
                                                        echo "-";
                                                    }
                                                } else {
                                                    echo "-";
                                                }
                                                ?>
                                            </p>
                                        </div>
                                    </div>
                                </li>


                                <li>

                                    <div class="form_grid_12">
                                        <label class="field_title"><?php
                                            if ($this->lang->line('admin_common_status') != '')
                                                echo stripslashes($this->lang->line('admin_common_status'));
                                            else
                                                echo 'Status';
                                            ?> </label>
                                        <div class="form_input">
                                            <p><?php
                                                if (isset($student_details->student_status)) {
                                                    echo $student_details->student_status;
                                                } else {
                                                    echo "-";
                                                }
                                                ?></p>											
                                        </div>
                                    </div>


                                </li>

                                <li>

                                    <div class="form_grid_12">
                                        <label class="field_title">Allow parent to update location</label>
                                        <div class="form_input">
                                            <p><?php
                                                if ($student_details->parent_update_location) {
                                                    echo "Active";
                                                } else {
                                                    echo "-";
                                                }
                                                ?></p>											
                                        </div>
                                    </div>


                                </li>
                                <li>
                                    <div class="form_grid_12">
                                        <label class="field_title"><?php
                                            if ($this->lang->line('operator_add_student_trip_type_category') != '')
                                                echo stripslashes($this->lang->line('operator_add_student_trip_type_category'));
                                            else
                                                echo 'Trip Type';
                                            ?> </label>
                                        <div class="form_input">
                                            <p>
                                                <?php
                                                $student_trip_type = $student_details->student_trip_type;
                                                ?>

                                                <?php
                                                if ($student_trip_type == "pickup") {
                                                    ?>
                                                <div>
                                                    <?php
                                                    echo "One way(Home to School)";
                                                    ?>
                                                </div>
                                                <?php
                                            }

                                            if ($student_trip_type == "drop") {
                                                ?>
                                                <div>
                                                    <?php
                                                    echo "One way(School to Home)";
                                                    ?>
                                                </div>
                                                <?php
                                            }

                                            if ($student_trip_type == "both") {
                                                ?>
                                                <div>
                                                    <?php
                                                    echo "One way(Home to School)";
                                                    ?>
                                                </div>
                                                <br>
                                                <div>
                                                    <?php
                                                    echo "One way(School to Home)";
                                                    ?>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                            </p>
                                        </div>
                                    </div>

                                    <div class="form_input_right">
                                        <label class="field_title"><?php
                                            if ($this->lang->line('operator_add_student_trip_name_category') != '')
                                                echo stripslashes($this->lang->line('operator_add_student_trip_name_category'));
                                            else
                                                echo 'Trip Name';
                                            ?> </label>
                                        <div class="form_input">
                                            <p>
                                                <?php
                                                $student_trip_type = $student_details->student_trip_type;
                                                ?>

                                                <?php
                                                if ($student_trip_type == "pickup") {
                                                    $pickup_trip_id = array('_id' => new MongoDB\BSON\ObjectId($student_details->student_pickup_trip_id));

                                                    $pikup_trip_detail = $this->bus_trip_model->get_all_details(BUS_TRIP, $pickup_trip_id);

                                                    $pikup_trip_detail = $pikup_trip_detail->result();
                                                    ?>
                                                <div>
                                                    <?php
                                                    echo "One way(Home to School) - " . $pikup_trip_detail[0]->trip_name;
                                                    ?>
                                                </div>
                                                <?php
                                            }

                                            if ($student_trip_type == "drop") {
                                                $drop_trip_id = array('_id' => new MongoDB\BSON\ObjectId($student_details->student_drop_trip_id));

                                                $drop_trip_detail = $this->bus_trip_model->get_all_details(BUS_TRIP, $drop_trip_id);

                                                $drop_trip_detail = $drop_trip_detail->result();
                                                ?>
                                                <div>
                                                    <?php
                                                    echo "One way(School to Home) - " . $drop_trip_detail[0]->trip_name;
                                                    ?>
                                                </div>
                                                <?php
                                            }

                                            if ($student_trip_type == "both") {

                                                $pickup_trip_id = array('_id' => new MongoDB\BSON\ObjectId($student_details->student_pickup_trip_id));

                                                $pikup_trip_detail = $this->bus_trip_model->get_all_details(BUS_TRIP, $pickup_trip_id);

                                                $pikup_trip_detail = $pikup_trip_detail->result();
                                                ?>
                                                <div>
                                                    <?php
                                                    echo "One way(Home to School) - " . $pikup_trip_detail[0]->trip_name;
                                                    ?>
                                                </div>
                                                <br>

                                                <?php
                                                $drop_trip_id = array('_id' => new MongoDB\BSON\ObjectId($student_details->student_drop_trip_id));

                                                $drop_trip_detail = $this->bus_trip_model->get_all_details(BUS_TRIP, $drop_trip_id);

                                                $drop_trip_detail = $drop_trip_detail->result();
                                                ?>
                                                <div>
                                                    <?php
                                                    echo "One way(School to Home) - " . $drop_trip_detail[0]->trip_name;
                                                    ?>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                            </p>
                                        </div>
                                    </div>
                                </li>




                                <li>
                                    <div class="form_grid_12">
                                        <label class="field_title"><?php
                                            if ($this->lang->line('operator_student_bus_route_title') != '')
                                                echo stripslashes($this->lang->line('operator_student_bus_route_title'));
                                            else
                                                echo 'Bus Route';
                                            ?> </label>
                                        <div class="form_input">
                                            <p>
                                                <?php
                                                $student_trip_type = $student_details->student_trip_type;
                                                ?>

                                                <?php
                                                if ($student_trip_type == "pickup") {
                                                    $pickup_trip_id = array('_id' => new MongoDB\BSON\ObjectId($student_details->student_pickup_trip_id));

                                                    $pikup_trip_detail = $this->bus_trip_model->get_all_details(BUS_TRIP, $pickup_trip_id);

                                                    $pikup_trip_detail = $pikup_trip_detail->result();
                                                    ?>
                                                <div>
                                                    <?php
                                                    if (is_object($pikup_trip_detail[0]->route_details))
                                                        $pikup_trip_detail[0]->{"route_details"} = (array) $pikup_trip_detail[0]->route_details;
                                                    echo "One way(Home to School) - " . $pikup_trip_detail[0]->route_details['route_name'];
                                                    ?>
                                                </div>
                                                <?php
                                            }

                                            if ($student_trip_type == "drop") {
                                                $drop_trip_id = array('_id' => new MongoDB\BSON\ObjectId($student_details->student_drop_trip_id));

                                                $drop_trip_detail = $this->bus_trip_model->get_all_details(BUS_TRIP, $drop_trip_id);

                                                $drop_trip_detail = $drop_trip_detail->result();
                                                ?>
                                                <div>
                                                    <?php
                                                    echo "One way(School to Home) - " . $drop_trip_detail[0]->route_details['route_name'];
                                                    ?>
                                                </div>
                                                <?php
                                            }

                                            if ($student_trip_type == "both") {
                                                $pickup_trip_id = array('_id' => new MongoDB\BSON\ObjectId($student_details->student_pickup_trip_id));

                                                $pikup_trip_detail = $this->bus_trip_model->get_all_details(BUS_TRIP, $pickup_trip_id);

                                                $pikup_trip_detail = $pikup_trip_detail->result();
                                                if (count($pikup_trip_detail) > 0 && is_object($pikup_trip_detail[0]->route_details))
                                                    $pikup_trip_detail[0]->{"route_details"} = (array) $pikup_trip_detail[0]->route_details;
                                                ?>
                                                <div>
                                                    <?php
                                                    echo "One way(Home to School) - " . $pikup_trip_detail[0]->route_details['route_name'];
                                                    ?>
                                                </div>
                                                <br>

                                                <?php
                                                $drop_trip_id = array('_id' => new MongoDB\BSON\ObjectId($student_details->student_drop_trip_id));

                                                $drop_trip_detail = $this->bus_trip_model->get_all_details(BUS_TRIP, $drop_trip_id);

                                                $drop_trip_detail = $drop_trip_detail->result();
                                                if (count($drop_trip_detail) > 0 && is_object($drop_trip_detail[0]->route_details))
                                                    $drop_trip_detail[0]->{"route_details"} = (array) $drop_trip_detail[0]->route_details;
                                                ?>
                                                <div>
                                                    <?php
                                                    echo "One way(School to Home) - " . $drop_trip_detail[0]->route_details['route_name'];
                                                    ?>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                            </p>
                                        </div>
                                    </div>

                                    <div class="form_input_right">
                                        <label class="field_title"><?php
                                            if ($this->lang->line('operator_student_bus_stop_title') != '')
                                                echo stripslashes($this->lang->line('operator_student_bus_stop_title'));
                                            else
                                                echo 'Bus Stop';
                                            ?> </label>
                                        <div class="form_input">
                                            <p>

                                                <?php
                                                $student_trip_type = $student_details->student_trip_type;
                                                ?>

                                                <?php
                                                if ($student_trip_type == "pickup") {
                                                    $pickup_trip_id = array('_id' => new MongoDB\BSON\ObjectId($student_details->student_pickup_trip_id));

                                                    $pikup_trip_detail = $this->bus_trip_model->get_all_details(BUS_TRIP, $pickup_trip_id);

                                                    $pikup_trip_detail = $pikup_trip_detail->result();

                                                    $pickup_student_bus_id = (string) $student_details->student_pickup_bus_stop_id;
                                                    if (is_object($pikup_trip_detail[0]->route_details))
                                                        $pikup_trip_detail[0]->{"route_details"} = (array) $pikup_trip_detail[0]->route_details;
                                                    $all_bus_stop_list_pickup = $pikup_trip_detail[0]->route_details['waypoints_details'];

                                                    if (is_object($all_bus_stop_list_pickup))
                                                        $all_bus_stop_list_pickup = (array) $all_bus_stop_list_pickup;

                                                    for ($i = 0; $i < sizeof($all_bus_stop_list_pickup); $i++) {
                                                        if (is_object($all_bus_stop_list_pickup[$i]))
                                                            $all_bus_stop_list_pickup[$i] = (array) $all_bus_stop_list_pickup[$i];
                                                        if ($all_bus_stop_list_pickup[$i]['id'] == $pickup_student_bus_id) {
                                                            ?>
                                                        <div>
                                                            <?php
                                                            echo "One way(Home to School) - " . $all_bus_stop_list_pickup[$i]['busstop_name'];
                                                            ?>
                                                        </div>
                                                        <?php
                                                    }
                                                }
                                            }

                                            if ($student_trip_type == "drop") {
                                                $drop_trip_id = array('_id' => new MongoDB\BSON\ObjectId($student_details->student_drop_trip_id));

                                                $drop_trip_detail = $this->bus_trip_model->get_all_details(BUS_TRIP, $drop_trip_id);

                                                $drop_trip_detail = $drop_trip_detail->result();
                                                if (count($drop_trip_detail) > 0 && is_object($drop_trip_detail[0]->route_details))
                                                    $drop_trip_detail[0]->{"route_details"} = (array) $drop_trip_detail[0]->route_details;
                                                $drop_student_bus_id = (string) $student_details->student_drop_bus_stop_id;

                                                $all_bus_stop_list_drop = $drop_trip_detail[0]->route_details['waypoints_details'];

                                                for ($i = 0; $i < sizeof($all_bus_stop_list_drop); $i++) {
                                                    if ($all_bus_stop_list_drop[$i]['id'] == $drop_student_bus_id) {
                                                        ?>
                                                        <div>
                                                            <?php
                                                            echo "One way(School to Home) - " . $all_bus_stop_list_drop[$i]['busstop_name'];
                                                            ?>
                                                        </div>
                                                        <?php
                                                    }
                                                }
                                            }

                                            if ($student_trip_type == "both") {

                                                $pickup_trip_id = array('_id' => new MongoDB\BSON\ObjectId($student_details->student_pickup_trip_id));

                                                $pikup_trip_detail = $this->bus_trip_model->get_all_details(BUS_TRIP, $pickup_trip_id);

                                                $pikup_trip_detail = $pikup_trip_detail->result();
                                                if (count($pikup_trip_detail) > 0 && is_object($pikup_trip_detail[0]->route_details))
                                                    $pikup_trip_detail[0]->{"route_details"} = (array) $pikup_trip_detail[0]->route_details;
                                                $pickup_student_bus_id = (string) $student_details->student_pickup_bus_stop_id;

                                                $all_bus_stop_list_pickup = $pikup_trip_detail[0]->route_details['waypoints_details'];



                                                for ($i = 0; $i < sizeof($all_bus_stop_list_pickup); $i++) {
                                                    if (is_object($all_bus_stop_list_pickup[$i]))
                                                        $all_bus_stop_list_pickup[$i] = (array) $all_bus_stop_list_pickup[$i];
                                                    if ($all_bus_stop_list_pickup[$i]['id'] == $pickup_student_bus_id) {
                                                        ?>
                                                        <div>
                                                            <?php
                                                            echo "One way(Home to School) - " . $all_bus_stop_list_pickup[$i]['busstop_name'];
                                                            ?>
                                                        </div>
                                                        <br>
                                                        <?php
                                                    }
                                                }


                                                $drop_trip_id = array('_id' => new MongoDB\BSON\ObjectId($student_details->student_drop_trip_id));

                                                $drop_trip_detail = $this->bus_trip_model->get_all_details(BUS_TRIP, $drop_trip_id);

                                                $drop_trip_detail = $drop_trip_detail->result();
                                                if (count($drop_trip_detail) > 0 && is_object($drop_trip_detail[0]->route_details))
                                                    $drop_trip_detail[0]->{"route_details"} = (array) $drop_trip_detail[0]->route_details;
                                                $drop_student_bus_id = (string) $student_details->student_drop_bus_stop_id;

                                                $all_bus_stop_list_drop = $drop_trip_detail[0]->route_details['waypoints_details'];

                                                for ($j = 0; $j < sizeof($all_bus_stop_list_drop); $j++) {
                                                    if (is_object($all_bus_stop_list_drop[$j]))
                                                        $all_bus_stop_list_drop[$j] = (array) $all_bus_stop_list_drop[$j];
                                                    if ($all_bus_stop_list_drop[$j]['id'] == $drop_student_bus_id) {
                                                        ?>
                                                        <div>
                                                            <?php
                                                            echo "One way(School to Home) - " . $all_bus_stop_list_drop[$j]['busstop_name'];
                                                            ?>
                                                        </div>
                                                        <?php
                                                    }
                                                }
                                            }
                                            ?>

                                            </p>
                                        </div>
                                    </div>
                                </li>

                            </ul>



                            <ul class="rite-contsec view_driver_list">								


                                <li>
                                    <div class="form_grid_12" style="width:100% !important;">
                                        <h3><?php
                                            if ($this->lang->line('guardian') != '')
                                                echo stripslashes($this->lang->line('guardian'));
                                            else
                                                echo 'Guardian';
                                            ?></h3>
                                    </div>
                                </li>							

                                <?php
                                $guardian_details = $student_details->guardian_details;



                                $z = 0;

                                for ($z = 0; $z < sizeof($guardian_details); $z++) {
                                    if (is_object($guardian_details[$z]))
                                        $guardian_details[$z] = (array) $guardian_details[$z];
                                    ?>

                                    <?php if ($z != 0) { ?> 

                                        <hr>
                                    <?php } ?>



                                    <div id="appeder_guardian_<?php echo $z; ?>">






                                        <li>                        
                                            <div class="form_grid_12">


                                                <label class="field_title"><?php
                                                    if ($this->lang->line('parent_type') != '')
                                                        echo stripslashes($this->lang->line('parent_type'));
                                                    else
                                                        echo 'Guardian Type';
                                                    ?> </label>                              
                                                <div class="form_input">   

                                                    <p>
                                                        <?php echo $guardian_details[$z]['guardian_type']; ?>

                                                    </p>
                                                </div>






                                            </div>



                                            <!-- parent name-->
                                            <div class="form_input_right">






                                                <label class="field_title"><?php
                                                    if ($this->lang->line('operator_add_student_name_category') != '')
                                                        echo stripslashes($this->lang->line('operator_add_student_name_category'));
                                                    else
                                                        echo 'First Name';
                                                    ?> </label>
                                                <div class="form_input">                                    

                                                    <P> <?php
                                                        if ($guardian_details[$z]['parent_name'] != "") {
                                                            echo $guardian_details[$z]['parent_name'];
                                                        } else {
                                                            echo "-";
                                                        }
                                                        ?> </P>
                                                </div>  






                                            </div>
                                        </li>                      

                                        <li>



                                            <!-- Phone -->

                                            <div class="form_grid_12">






                                                <label class="field_title"><?php
                                                    if ($this->lang->line('admin_rides_mobile_number') != '')
                                                        echo stripslashes($this->lang->line('admin_rides_mobile_number'));
                                                    else
                                                        echo 'Mobile Number';
                                                    ?></label>
                                                <div class="form_input">

                                                    <p> <?php
                                                        if (isset($guardian_details[$z]['phone_code'])) {
                                                            echo $guardian_details[$z]['phone_code'];
                                                        };
                                                        ?>   <?php echo $guardian_details[$z]['parent_mobile_number']; ?> </p>

                                                </div>





                                            </div>

                                            <!-- last name-->

                                            <div class="form_input_right">



                                                <label class="field_title"><?php
                                                    if ($this->lang->line('admin_drivers_email_address') != '')
                                                        echo stripslashes($this->lang->line('admin_drivers_email_address'));
                                                    else
                                                        echo 'Email Address';
                                                    ?> </label>
                                                <div class="form_input">
                                                    <P><?php
                                                        if ($guardian_details[$z]['parent_email'] != "") {
                                                            echo $guardian_details[$z]['parent_email'];
                                                        } else {
                                                            echo "-";
                                                        };
                                                        ?> </P>  
                                                </div>









                                            </div>   
                                        </li>








                                    </div>



                                <?php } ?>	

                            </ul>









                            <ul class="last-sec-btn">
                                <li class="change-pass" style="margin-left: 25%;">
                                    <div class="form_grid_12">
                                        <div class="form_input">
                                            <a  href="<?php echo OPERATOR_NAME; ?>/add_students/<?php
                                            if (null !== $this->uri->segment(5)) {
                                                echo $this->uri->segment(5);
                                            } else {
                                                echo "student_lists";
                                            }
                                            ?>" class="btn_small btn_blue" ><span><?php
                                                if ($this->lang->line('admin_drivers_back_to_student_list') != '')
                                                    echo stripslashes($this->lang->line('admin_drivers_back_to_student_list'));
                                                else
                                                    echo 'Back To Student List';
                                                ?></span></a>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>









                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <span class="clear"></span> 
</div>
</div>


<style>


    .admin-settings.edit-global-set.add_drive_catagory ul.view_driver_list {
        min-height: auto;
    }

    ul.left-contsec {
        width: 100%;
    }

    .form_container ul li {
        width: 100%;
    }

    .admin-settings li .form_grid_12 {
        width: 50%;
    }


    .form_input_right
    {
        float: right;
        width: 47%;
    }

    ul.rite-contsec {
        width:100% !important;
    }







    .expiry_box {
        background: none repeat scroll 0 0 gainsboro;
        border: 1px solid grey;
        border-radius: 5px;
        margin-top: 2%;
        padding: 1%;
        text-align: center;
        width: 50% !important;
    }

    .expiry_box_status {
        background: gainsboro none repeat scroll 0 0;
        border: 1px solid grey;
        border-radius: 5px;
        color: #fff;
        float: right;
        font-weight: bold;
        margin: -32px 0 0;
        padding: 5px 13px;
        text-align: center;
        width: 125px !important;
    }
    .expiry_box a{
        color: green;
    }
    .expiry_box p{
        margin-bottom: 0;
        margin-top: 12px;
    }
</style>

<?php
$this->load->view(OPERATOR_NAME . '/templates/footer.php');
?>
