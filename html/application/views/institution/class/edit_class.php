<?php
$this->load->view(OPERATOR_NAME.'/templates/header.php');


?>
<div id="content" class="admin-settings add-edit-opr">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">
                <div class="widget_top">
                    <span class="h_icon list"></span>
                    <h6><?php echo $heading; ?></h6>
                    <div id="widget_tab">
                    </div>
                </div>
              
                <div class="widget_content chenge-pass-base">
                   <br>




                      <div class="row" style="padding: 40px;background-color:white;">
                                                  <form class="form_container left_label" action="<?php echo OPERATOR_NAME;?>/class_details/update_class_along_section" id="addeditmodel_form_class" method="post" enctype="multipart/form-data">

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                                <label for="usr">Class Name <span class="red">*</span></label>
                                                                <input placeholder="Enter class name" type="text" name="class_name" value="<?php echo $class_details[0]->class_name;?>" class="form-control required" id="usr">
                                                    </div>
                                                </div>    


                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="section_sel">Sections <span class="red">*</span></label><br>
                                                        <select class="custom-select required" name="section_name_mul[]" multiple id="section_sel">
                                                             <option value="none">None</option>
                                                              <?php foreach ($section_list->result() as $key => $value) {                                     
                                                                                                   ?>

                                                                    <option  <?php if(in_array((string)$value->_id, $class_details[0]->section_ids)){ echo "selected"; }     ?>                       value="<?php echo (string)$value->_id; ?>"><?php echo $value->section_name; ?></option>
                                                                                                  


                                                         <?php  } ?>

                                                        </select>
                                                        <p><span class="red">*</span>Press ctrl for multiple select.</p>
                                                    </div>
                                                </div>    
                                                    <div style="text-align: center">
                                                        <button type="submit" class="btn btn-success button_extra_style" ><span>Update</span></button>
                                                    </div>

                                                    <input type="hidden" name="class_id" value="<?php echo $class_id; ?>">
                                                </form>
                        </div>    



                          
                              
                </div>
            </div>
        </div>
    </div>
    <span class="clear"></span>
</div>

<style>



 ul.left-contsec {
         width:100% !important;
    }

    .admin-settings li .form_grid_12 {
        width:47% !important;
    }

    .left_label ul li .form_input {
        width:100% !important;        
    }


    .form_container ul li {
        width:100% !important;  
    }


    .form_input_right
    {
        float: right;
        width: 47%;
    }



ul.rite-contsec {
        width:100% !important;
    }


.admin-settings .left_label ul li .form_input input {
    width: 100% !important;
}





.model_type .error {
    float: right;
    margin-right: 30%;
}
label.error {
float:right!important;
}

.default {
	width: 650px !important;
}

th,td
{
    text-align: center;
}


</style>








<script>

$(document).ready(function() {
	$.validator.setDefaults({ ignore: ":hidden:not(select)" });
	$("#addeditmodel_form").validate();
    $("#addeditmodel_form_class").validate();

	//get_model(document.getElementById("brand_list").value);
});











</script>
<?php
$this->load->view(OPERATOR_NAME.'/templates/footer.php');
?>