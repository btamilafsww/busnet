<?php
$this->load->view(OPERATOR_NAME.'/templates/header.php');
if ($form_mode) $operator_details = $operator_details->row();



?>

<style>
     #map {
        height: 100%;
        min-height: 450px;
      }
     div#country_codeM_chzn
      {
        width: 82px !important; 
      }
</style>
<div id="content" class="admin-settings profile_set_panel edit-global-set">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">
                <div class="widget_top">
                    <span class="h_icon list"></span>
                    <h6><?php echo $heading; ?></h6>
                    <div id="widget_tab">
                    </div>
                </div>
                <div class="widget_content chenge-pass-base">
                    <form class="form_container left_label" action="<?php echo OPERATOR_NAME; ?>/settings/update_profile" id="addEditoperators_form" method="post" enctype="multipart/form-data">
                        <ul class="left-contsec pro_editing">
                            
                            <li>
                                <div class="form_grid_12">
                                    <label class="field_title"><?php if ($this->lang->line('admin_operator_name') != '') echo stripslashes($this->lang->line('admin_operator_name')); else echo 'Operator Name'; ?><span class="req">*</span></label>
                                    <div class="form_input">
                                            <input name="operator_name" id="operator_name" type="text"  class="required large tipTop" title="<?php if ($this->lang->line('admin_enter_operator_name') != '') echo stripslashes($this->lang->line('admin_enter_operator_name')); else echo 'Please enter operator name'; ?>" value="<?php if($form_mode) echo $operator_details->operator_name;  ?>"/>
                                    </div>
                                </div>
                            </li>
                        
                            <li>
                                <div class="form_grid_12">
                                    <label class="field_title"><?php if ($this->lang->line('admin_operator_email') != '') echo stripslashes($this->lang->line('admin_operator_email')); else echo 'Operator Email'; ?><span class="req">*</span></label>
                                    <div class="form_input">
                                            <input name="email" id="email" type="text"  class="required large tipTop email" title="<?php if ($this->lang->line('admin_enter_operator_email') != '') echo stripslashes($this->lang->line('admin_enter_operator_email')); else echo 'Please enter operator email'; ?>" value="<?php if($form_mode) if(isset($operator_details->email)) { echo $operator_details->email; } ?>"/>
                                    </div>
                                </div>
                            </li>

                            <li>
                                <div class="form_grid_12">
                                    <label class="field_title"><?php if ($this->lang->line('admin_drivers_mobile_number') != '') echo stripslashes($this->lang->line('admin_drivers_mobile_number')); else echo 'Mobile Number'; ?><span class="req">*</span></label>
                                    <div class="form_input">
                                              <?php 
                                                $dail_code = $d_country_code;
                                                if($form_mode) if(isset($operator_details->dail_code)) $dail_code = $operator_details->dail_code; 
                                                ?>
                                                
                                                <select style="" name="dail_code" id="country_codeM"  class="required small tipTop chzn-select" style="" title="<?php if ($this->lang->line('select_mobile_country_code') != '') echo stripslashes($this->lang->line('select_mobile_country_code')); else echo 'Please select mobile country code'; ?>">
                                                    <?php foreach ($countryList as $country) { ?>
                                                        <option value="<?php echo $country->dial_code; ?>" <?php if($country->dial_code==$dail_code){ echo "selected='selected'"; } ?>><?php echo $country->dial_code; ?></option>
                                                    <?php } ?>
                                                </select>  


                              <input style="width: 81% !important;height: 37px;" name="mobile_number" placeholder="<?php if ($this->lang->line('admin_drivers_mobile_number') != '') echo stripslashes($this->lang->line('admin_drivers_mobile_number')); else echo 'Mobile Number'; ?>.." id="mobile_number" type="text"  class="required large tipTop phoneNumber" title="<?php if ($this->lang->line('driver_enter_mobile_number') != '') echo stripslashes($this->lang->line('driver_enter_mobile_number')); else echo 'Please enter the mobile number'; ?>" maxlength="20"  value="<?php if($form_mode) if (isset($operator_details->mobile_number)) echo $operator_details->mobile_number; ?>"/>
                                    </div>
                                </div>
                            </li>	
    
                            <li>
                                    <div class="form_grid_12">
                                            <label class="field_title"><?php if ($this->lang->line('admin_drivers_address') != '') echo stripslashes($this->lang->line('admin_drivers_address')); else echo 'Address'; ?><span class="req">*</span></label>
                                            <div class="form_input">
                                                <?php $operator_details->{"address"}=(is_object($operator_details->address))?(array)$operator_details->address:array(); ?>
                                                    <textarea onchange="address_binder()"  name="address" id="address"  class="required large tipTop" title="<?php if ($this->lang->line('admin_enter_operator_address') != '') echo stripslashes($this->lang->line('admin_enter_operator_address')); else echo 'Please enter the operator address'; ?>" style="width: 372px;"><?php if($form_mode) if (isset($operator_details->address['address'])) echo $operator_details->address['address']; ?></textarea>
                                            </div>
                                    </div>
                            </li>
</ul>
                        
                        
                        <ul class="rite-contsec pro_editing">
                            <li>
                                <div class="form_grid_12">
                                    <label class="field_title"><?php if ($this->lang->line('admin_drivers_country') != '') echo stripslashes($this->lang->line('admin_drivers_country')); else echo 'Country'; ?><span class="req">*</span></label>
                                    <div class="form_input">
                                        <select name="country" id="country"  onchange="update_mobile_code();address_binder()"  class="required chzn-select" style="height: 31px; width: 51%;">
                                        <?php foreach ($countryList as $country) { ?>
                                                <option value="<?php echo $country->name; ?>" data-dialCode="<?php echo $country->dial_code; ?>" <?php if ($form_mode) if ($operator_details->address['country'] == $country->name) echo 'selected="selected"' ?>><?php echo $country->name; ?></option>
                                        <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </li>
                        
                            <li>
                                <div class="form_grid_12">
                                    <label class="field_title"><?php if ($this->lang->line('admin_drivers_state_province_region') != '') echo stripslashes($this->lang->line('admin_drivers_state_province_region')); else echo 'State / Province / Region'; ?><span class="req">*</span></label>
                                    <div class="form_input">
                                            <input onchange="address_binder()" name="state" id="state" type="text"  class="required large tipTop" title="<?php if ($this->lang->line('admin_operator_driver_state') != '') echo stripslashes($this->lang->line('admin_operator_driver_state')); else echo 'Please enter the state'; ?>" value="<?php if($form_mode) if (isset($operator_details->address['state'])) echo $operator_details->address['state']; ?>"/>
                                    </div>
                                </div>
                            </li>

                            <li>
                                <div class="form_grid_12">
                                    <label class="field_title"><?php if ($this->lang->line('admin_drivers_city') != '') echo stripslashes($this->lang->line('admin_drivers_city')); else echo 'City'; ?><span class="req">*</span></label>
                                    <div class="form_input">
                                            <input onchange="address_binder()"  name="city" id="city" type="text"  class="required large tipTop" title="<?php if ($this->lang->line('location_enter_the_city') != '') echo stripslashes($this->lang->line('location_enter_the_city')); else echo 'Please enter the city'; ?>" value="<?php if($form_mode) if (isset($operator_details->address['city'])) echo $operator_details->address['city']; ?>"/>
                                    </div>
                                </div>
                            </li>

                            <li>
                                <div class="form_grid_12">
                                    <label class="field_title"><?php if ($this->lang->line('admin_drivers_postal_code') != '') echo stripslashes($this->lang->line('admin_drivers_postal_code')); else echo 'Postal Code'; ?><span class="req">*</span></label>
                                    <div class="form_input">
                                            <input onchange="address_binder()"  name="postal_code" id="postal_code" type="text"  maxlength="10" class="required large tipTop" title="<?php if ($this->lang->line('location_enter_the_postalcode') != '') echo stripslashes($this->lang->line('location_enter_the_postalcode')); else echo 'Please enter the postal code'; ?>" value="<?php if($form_mode) if (isset($operator_details->address['postal_code'])) echo $operator_details->address['postal_code']; ?>"/>
                                    </div>
                                </div>
                            </li>


                             <li class="logo-img-sec">
                                    <div class="form_grid_12">
                                        <label class="field_title"><?php if ($this->lang->line('admin_settings_logo') != '') echo stripslashes($this->lang->line('admin_settings_logo')); else echo 'Logo'; ?></label>
                                        <div class="form_input">
                                            <input name="logo_image" id="logo_image" type="file"  class="large tipTop" title="<?php if ($this->lang->line('admin_setting_choose_logo_image') != '') echo stripslashes($this->lang->line('admin_setting_choose_logo_image')); else echo 'Please Choose the logo image'; ?>"/>
                                        </div>
                                        <div class="form_input logo-out">
                                            <img src="<?php echo base_url(); ?>images/logo/<?php if(isset($operator_details->logo_image)){echo $operator_details->logo_image;}else{ echo "8033804eb19cd93ba13fa1ab69a4cca5(1).png"; } ?>" width="100px"/>
                                        </div>
                                    </div>
                             </li>
                            
                        </ul>
                            
                            
                        <ul class="admin-pass">
                            <li class="change-pass">
                                    <div class="form_grid_12">
                                        <div class="form_input">
                                            <input type="hidden" name="operator_id" id="operator_id" value="<?php if($form_mode){ echo (string)$operator_details->_id; } ?>"  />
                                            <button type="submit" class="btn_small btn_blue" ><span><?php if ($this->lang->line('admin_subadmin_update') != '') echo stripslashes($this->lang->line('admin_subadmin_update')); else echo 'Update'; ?></span></button>
                                        </div>
                                    </div>
                                </li>
                        </ul>


                        <ul class="last-operator">
                                        <li>
                                         <div style="width: 25%;">
                                            <div class="form_grid_12">
                                            <label class="field_title">Latitude</label>
                                            <?php if(property_exists($operator_details, "operator_location")){
                                            $operator_details->{"operator_location"}=(is_object($operator_details->operator_location))?(array)$operator_details->operator_location:array();
                                            } ?>
                                            <input type="number" name="input_lat" class="required"  value="<?php if($form_mode) if(isset($operator_details->operator_location)) {  echo $operator_details->operator_location['lat']; } ?>"  id="input_lat"/>
                                            </div>
                                            <br>
                                            <div class="form_grid_12">
                                            <label class="field_title">Longitude</label>
                                            <input type="number" name="input_long" class="required"  value="<?php if($form_mode) if(isset($operator_details->operator_location)) {  echo $operator_details->operator_location['lat']; }?>" id="input_long"/>
                                            </div>
                                        </div>
                                        <div id="map"> </div>
                                       

                                        </li>
                        </ul>

											 
                    </form>
                </div>
            </div>
        </div>
    </div>
    <span class="clear"></span>
</div>
</div>
<script>
        $('#addEditoperators_form').validate();










        var country=<?php echo json_encode($countryList) ?>;


       




        function update_mobile_code()
        {

            var country_name=document.getElementById("country").value;
            var code_selected="";

            for(var i=0;i< country.length;i++  )
            {

                if(country[i]["name"]== country_name)
                   {
                    code_selected=country[i]["dial_code"];
                   } 


            }

          
            

            
             $('#country_codeM option[value="'+code_selected+'"]').attr("selected", "selected");   
         







        }









        /////////////////map

     var checker=0;
     var geocoder;
      var map;
      var address = "Singapore"; 


      <?php if(isset($operator_details->operator_location)) {  

       

        echo "address ='".$operator_details->operator_location['lat'].",".$operator_details->operator_location['lng']."'";




       } ?>








       function address_binder()
       {

      
             if(checker != 0)
             {  

               var country=document.getElementById('country').value;
                 var city=document.getElementById('city').value;
                   var state=document.getElementById('state').value;
                     var postal_code=document.getElementById('postal_code').value;
                       var address_input=document.getElementById('address').value;



                       address=address_input+' '+city+' '+state+' '+postal_code+' '+country;

                       initMap();

              }
              else
              {
                initMap();
                 checker=1;
              }         


       } 



      










        
      function codeAddress(geocoder, map) {

        console.log(address);
       
        geocoder.geocode({'address': address}, function(results, status) {
          if (status === 'OK') {
            map.setCenter(results[0].geometry.location);
             map.setZoom(16);

            document.getElementById("input_lat").value=results[0].geometry.location.lat();
             document.getElementById("input_long").value=results[0].geometry.location.lng();

            var marker = new google.maps.Marker({
              map: map,
              draggable:true,
              position: results[0].geometry.location
            });
          } else {
            if(checker != 0)
             {   
                    alert('Geocode was not successful for the following reason: ' + status);
                    checker=1;
              }  
          }


                google.maps.event.addListener(marker, 'dragend', function() 
                {
                    var lat_long=marker.getPosition();

                    console.log(lat_long.lat(),lat_long.lng());

                     document.getElementById("input_lat").value=lat_long.lat();
                    document.getElementById("input_long").value=lat_long.lng();



                });



        });


      }
     


      function initMap() {
          map = new google.maps.Map(document.getElementById('map'), {
          zoom: 12,
          center: {lat: -34.397, lng: 150.644}
        });
        geocoder = new google.maps.Geocoder();

        $('.active_inactive :checkbox').iphoneStyle({checkedLabel: 'Active', uncheckedLabel: 'Inactive'});


        codeAddress(geocoder, map);     
       
      }















</script>
 <script async defer
    src="https://maps.googleapis.com/maps/api/js?sensor=false&<?php echo substr($this->data['google_maps_api_key'], 1);?>&callback=initMap">
 </script>
<?php 
$this->load->view(OPERATOR_NAME.'/templates/footer.php');
?>