<?php
$this->load->view(OPERATOR_NAME .'/templates/header.php'); 
?> 

<style>


.stat_block{
	padding: 15px 0;
	background:#ffffff;
}
.data_widget{
	margin: 20px 0;
}
.stat_chart .chart_label{
	text-align: right;
}


</style>


<?php $operator_id = $this->session->userdata(APP_NAME.'_session_operator_id'); ?>
<div id="content" style="clear:both;" class="operoter-dashboard">
    <div class="grid_container">		
   	
        <div class="grid_12 center_driver_mode">
                <div class="widget_wrap">
                        <div class="widget_content">
                                
                                        <div class="social_activities"> 

                                                 <div class="activities_s bluebox big">
                                                <div class="block_label">
                                                        <span><?php if (isset($plying_vehilces)) echo $plying_vehilces; ?></span>
                                                        <small>Vehicle plying</small>
                                                        
                                                        <i class="user_icon_font fa fa-car"></i>
                                                       <a href="<?php echo OPERATOR_NAME;?>/trip/vehicle_plying_list" class="small-box-footer">Track<i class="fa fa-arrow-circle-right"></i></a>
                                                </div>
                                                </div>  
                                                
                                                 <div class="activities_s redbox big" style="background:#ffc926 !important;">
                                                <div class="block_label">
                                                        <span><?php if (isset($delayed_bus)) echo $delayed_bus; ?></span>
                                                        <small>Delayed vehicles</small>
                                                        
                                                        <i class="user_icon_font fa fa-car"></i>
                                                        <a  href="<?php echo OPERATOR_NAME;?>/trip/delayed_vehicles"  class="small-box-footer">Track<i class="fa fa-arrow-circle-right"></i></a>
                                                </div>
                                                </div>  
                                            
                                            
                                                <div class="activities_s redbox big" style="">
                                                <div class="block_label">
                                                        <span><?php if (isset($bus_breakdown)) echo $bus_breakdown; ?></span>
                                                        <small>Breakdown</small>
                                                        
                                                        <i class="user_icon_font fa fa-car"></i>
                                                        <a href="<?php echo OPERATOR_NAME;?>/trip/breakdown_vehicles"  class="small-box-footer">Track<i class="fa fa-arrow-circle-right"></i></a>
                                                </div>
                                                </div>  


                                               
                                                                 
                                                          
                                               
                                               
                                                                 
                                                        

                                        </div>

                                          <div class="social_activities"> 
                                                  <div class="activities_s bluebox big" >
                                                <div class="block_label">
                                                        <span><?php if (isset($parent_notifications)) echo $parent_notifications; ?></span>
                                                        <small>Parent Notification</small>
                                                        
                                                        <i class="user_icon_font fa fa-user-o"></i>
                                                        <a  href="<?php echo OPERATOR_NAME;?>/logs/today_display_parent_logs" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                                                </div>
                                                </div>  
                                                <div class="activities_s bluebox big" style="margin-left: 24px;background:#51b351 !important;">
                                                <div class="block_label">
                                                        <span><?php if (isset($parent_students_pres_abs)) echo $parent_students_pres_abs[0]; ?></span>
                                                        <small>Travelling children</small>
                                                        
                                                        <i class="user_icon_font fa fa-users"></i>
                                                        <a href="<?php echo OPERATOR_NAME;?>/add_students/travelling_student_lists"     class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                                                </div>
                                                </div>  

                                              
                                               
                                                <div class="activities_s redbox big" style="margin-left: 24px;">
                                                <div class="block_label">
                                                        <span><?php if (isset($parent_students_pres_abs)) echo $parent_students_pres_abs[1]; ?></span>
                                                        <small>Absentees</small>
                                                        
                                                        <i class="user_icon_font fa fa-users"></i>
                                                        <a  href="<?php echo OPERATOR_NAME;?>/add_students/absent_student_lists"   class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                                                </div>
                                                </div>   

                                               

                                               
                                               

                                                                    
                                                        

                                        </div>
                            
                                         <div class="social_activities"> 

                                                 <div class="activities_s bluebox big" style="" >
                                                <div class="block_label">
                                                           <span><?php if (isset($driver_notify_count)) echo $driver_notify_count; ?></span>
                                                        <small>Driver Notification</small>
                                                        
                                                        <i class="user_icon_font fa fa-user-circle"></i>
                                                        <a href="<?php echo OPERATOR_NAME;?>/logs/today_driver_notification_log"  class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                                                </div>
                                                </div>                          
                                                          
                                               
                                              
                                               
                                                <div class="activities_s bluebox big" style="margin-left: 24px;background:#51b351 !important;">
                                                <div class="block_label">
                                                        <span><?php if (isset($online_drivers_data)) echo $online_drivers_data[0]; ?></span>
                                                        <small><?php if ($this->lang->line('admin_map_online_drivers') != '') echo stripslashes($this->lang->line('admin_map_online_drivers')); else echo 'Online Drivers'; ?></small>
                                                        
                                                        <i class="user_icon_font fa fa-user-circle"></i>
                                                        <a href="<?php echo OPERATOR_NAME;?>/map/map_avail_drivers" class="small-box-footer">Track<i class="fa fa-arrow-circle-right"></i></a>
                                                </div>
                                                </div>                              
                                                <div class="activities_s redbox big">
                                                <div class="block_label">
                                                        <span><?php if (isset($online_drivers_data)) echo $online_drivers_data[1]; ?></span>
                                                        <small><?php if ($this->lang->line('admin_map_offline_drivers') != '') echo stripslashes($this->lang->line('admin_map_offline_drivers')); else echo 'Offline Drivers'; ?></small>   
                                                        
                                                        <i class="user_icon_font fa fa-user-circle"></i>
                                                        <a href="<?php echo OPERATOR_NAME;?>/map/map_avail_drivers"  class="small-box-footer">Track<i class="fa fa-arrow-circle-right"></i></a>
                                                </div>
                                                </div>         

                                                                    
                                                        

                                        </div>
                                
                                
                        </div>
                </div>
        </div>




        <div class="grid_6" style="margin-top: 20px;">
            <div class="widget_wrap">
                <div class="widget_top">
                    <span class="h_icon graph"></span>
                    <h6><?php if ($this->lang->line('admin_dashboard_drivers') != '') echo stripslashes($this->lang->line('admin_dashboard_drivers')); else echo 'Drivers'; ?></h6>
                </div>
                <div class="widget_content">
                    <div class="stat_block">	
                        <div class="stat_chart">
                            <h4><?php if ($this->lang->line('admin_dashboard_drivers_count') != '') echo stripslashes($this->lang->line('admin_dashboard_drivers_count')); else echo 'Drivers Count'; ?> : <?php echo $totalDrivers; ?></h4>
                            <table>
                                <tbody>
                                    <tr>
                                        <td>
                                            <?php if ($this->lang->line('admin_dashboard_today') != '') echo stripslashes($this->lang->line('admin_dashboard_today')); else echo 'Today'; ?>
                                        </td>
                                        <td>
                                            <?php if (isset($todayDrivers)) echo $todayDrivers; ?>
                                        </td>
                                        <?php /* <td class="min_chart">
                                          <span class="bar">20,30,50,200,250,280,350</span>
                                          </td> */ ?>
                                    </tr>
                                    <tr>
                                        <td>
                                            <?php if ($this->lang->line('admin_dashboard_this_month') != '') echo stripslashes($this->lang->line('admin_dashboard_this_month')); else echo 'This Month'; ?>
                                        </td>
                                        <td>
                                            <?php if (isset($monthDrivers)) echo $monthDrivers; ?>
                                        </td>
                                        <?php /* <td class="min_chart">
                                          <span class="line">20,30,50,200,250,280,350</span>
                                          </td> */ ?>
                                    </tr>
                                    <tr>
                                        <td>
                                            <?php if ($this->lang->line('admin_dashboard_this_year') != '') echo stripslashes($this->lang->line('admin_dashboard_this_year')); else echo 'This Year'; ?>
                                        </td>
                                        <td>
                                            <?php if (isset($yearDrivers)) echo $yearDrivers; ?>
                                        </td>
                                        <?php /* <td class="min_chart">
                                          <span class="line">20,30,50,200,250,280,350</span>
                                          </td> */ ?>
                                    </tr>
                                </tbody>
                            </table>
							<div class="pie_chart">
                                <?php
                                $activeDriversPercent = 0.00;
                                if (isset($activeDrivers)) {
                                    if ($totalDrivers > 0) {
                                        $activeDriversPercent = ($activeDrivers * 100) / $totalDrivers;
                                    }
                                }
                                ?>
                                <span class="inner_circle"><?php echo round($activeDriversPercent, 1) . '%'; ?></span>
                                <span class="pie"><?php if (isset($activeDrivers)) echo $activeDrivers; ?>/<?php if (isset($totalDrivers)) echo $totalDrivers; ?></span>
                            </div>
                            <div class="chart_label">
                                <ul>
                                    <li><span class="new_visits"></span><?php if ($this->lang->line('admin_dashboard_active_drivers') != '') echo stripslashes($this->lang->line('admin_dashboard_active_drivers')); else echo 'Active Drivers'; ?>: <?php if (isset($activeDrivers)) echo $activeDrivers; ?></li>
                                    <li><span class="unique_visits"></span><?php if ($this->lang->line('admin_dashboard_total_drivers') != '') echo stripslashes($this->lang->line('admin_dashboard_total_drivers')); else echo 'Total Drivers'; ?>: <?php if (isset($totalDrivers)) echo $totalDrivers; ?></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>








        <div class="grid_6" style="margin-top: 20px;">
            <div class="widget_wrap">
                <div class="widget_top">
                    <span class="h_icon graph"></span>
                    <h6><?php if ($this->lang->line('dashboard_student_head') != '') echo stripslashes($this->lang->line('dashboard_student_head')); else echo 'Students'; ?></h6>
                </div>


                <div class="widget_content">
                    <div class="stat_block">    
                        <div class="stat_chart">
                            <h4><?php if ($this->lang->line('operator_dashboard_students_count') != '') echo stripslashes($this->lang->line('operator_dashboard_students_count')); else echo 'Students Count'; ?> : <?php echo $totalStudents; ?></h4>
                            <table>
                                <tbody>
                                    <tr>
                                        <td>
                                            <?php if ($this->lang->line('admin_dashboard_today') != '') echo stripslashes($this->lang->line('admin_dashboard_today')); else echo 'Today'; ?>
                                        </td>
                                        <td>
                                            <?php if (isset($todayStudents)) echo $todayStudents; ?>
                                        </td>
                                        <?php /* <td class="min_chart">
                                          <span class="bar">20,30,50,200,250,280,350</span>
                                          </td> */ ?>
                                    </tr>
                                    <tr>
                                        <td>
                                            <?php if ($this->lang->line('admin_dashboard_this_month') != '') echo stripslashes($this->lang->line('admin_dashboard_this_month')); else echo 'This Month'; ?>
                                        </td>
                                        <td>
                                            <?php if (isset($monthStudents)) echo $monthStudents; ?>
                                        </td>
                                        <?php /* <td class="min_chart">
                                          <span class="line">20,30,50,200,250,280,350</span>
                                          </td> */ ?>
                                    </tr>
                                    <tr>
                                        <td>
                                            <?php if ($this->lang->line('admin_dashboard_this_year') != '') echo stripslashes($this->lang->line('admin_dashboard_this_year')); else echo 'This Year'; ?>
                                        </td>
                                        <td>
                                            <?php if (isset($yearStudents)) echo $yearStudents; ?>
                                        </td>
                                        <?php /* <td class="min_chart">
                                          <span class="line">20,30,50,200,250,280,350</span>
                                          </td> */ ?>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="pie_chart">
                            <?php
                                $activeStudentPercent = 0.00;
                                if (isset($activeStudents)) 
                                {  
                                    if ($totalStudents > 0) {
                                        $activeStudentPercent = ($activeStudents * 100) / $totalStudents;
                                    }
                                }
                            ?>
                                <span class="inner_circle"><?php echo round($activeStudentPercent, 1) . '%'; ?></span>
                                <span class="pie"><?php if (isset($activeStudents)) echo $activeStudents; ?>/<?php if (isset($totalStudents)) echo $totalStudents; ?></span>
                            </div>
                            <div class="chart_label">
                                <ul>
                                    <li><span class="new_visits"></span><?php if ($this->lang->line('operator_dashboard_active_students') != '') echo stripslashes($this->lang->line('operator_dashboard_active_students')); else echo 'Active Students'; ?>: <?php if (isset($activeStudents)) echo $activeStudents; ?></li>
                                    
                                    <li><span class="unique_visits"></span><?php if ($this->lang->line('operator_dashboard_total_students') != '') echo stripslashes($this->lang->line('operator_dashboard_total_students')); else echo 'Total Students'; ?>: <?php if (isset($totalStudents)) echo $totalStudents; ?></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>














        <span class="clear"></span>
        
    </div>








    <div class="grid_container">   


        <div class="grid_6" style="margin-top: 20px;">
            <div class="widget_wrap">
                <div class="widget_top">
                    <span class="h_icon graph"></span>
                    <h6><?php if ($this->lang->line('register_user_count') != '') echo stripslashes($this->lang->line('register_user_count')); else echo 'Registered Users count'; ?></h6>
                </div>
                <div class="widget_content">
                    <div class="stat_block">    
                        <div class="stat_chart">
                            <h4><?php if ($this->lang->line('register_user') != '') echo stripslashes($this->lang->line('register_user')); else echo 'Registered Users'; ?> : <?php echo $total_Users; ?></h4>
                            <table>
                                <tbody>
                                    <tr>
                                        <td>
                                            <?php if ($this->lang->line('admin_dashboard_today') != '') echo stripslashes($this->lang->line('admin_dashboard_today')); else echo 'Today'; ?>
                                        </td>
                                        <td>
                                            <?php if (isset($todayUsers)) echo $todayUsers; ?>
                                        </td>
                                        <?php /* <td class="min_chart">
                                          <span class="bar">20,30,50,200,250,280,350</span>
                                          </td> */ ?>
                                    </tr>
                                    <tr>
                                        <td>
                                            <?php if ($this->lang->line('admin_dashboard_this_month') != '') echo stripslashes($this->lang->line('admin_dashboard_this_month')); else echo 'This Month'; ?>
                                        </td>
                                        <td>
                                            <?php if (isset($monthUsers)) echo $monthUsers; ?>
                                        </td>
                                        <?php /* <td class="min_chart">
                                          <span class="line">20,30,50,200,250,280,350</span>
                                          </td> */ ?>
                                    </tr>
                                    <tr>
                                        <td>
                                            <?php if ($this->lang->line('admin_dashboard_this_year') != '') echo stripslashes($this->lang->line('admin_dashboard_this_year')); else echo 'This Year'; ?>
                                        </td>
                                        <td>
                                            <?php if (isset($yearUsers)) echo $yearUsers; ?>
                                        </td>
                                        <?php /* <td class="min_chart">
                                          <span class="line">20,30,50,200,250,280,350</span>
                                          </td> */ ?>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="pie_chart">
                                <?php
                                $activeUserPercent = 0.00;
                                if (isset($active_Users)) {
                                    if ($total_Users > 0) {
                                        $activeUserPercent = ($active_Users * 100) / $total_Users;
                                    }
                                }
                                ?>
                                <span class="inner_circle"><?php echo round($activeUserPercent, 1) . '%'; ?></span>
                                <span class="pie"><?php if (isset($active_Users)) echo $active_Users; ?>/<?php if (isset($total_Users)) echo $total_Users; ?></span>
                            </div>
                            <div class="chart_label">
                                <ul>
                                    <li><span class="new_visits"></span><?php if ($this->lang->line('admin_dashboard_active_users') != '') echo stripslashes($this->lang->line('admin_dashboard_active_users')); else echo 'Active Users'; ?>: <?php if (isset($active_Users)) echo $active_Users; ?></li>
                                    
                                    <li><span class="unique_visits"></span><?php if ($this->lang->line('admin_dashboard_total_users') != '') echo stripslashes($this->lang->line('admin_dashboard_total_users')); else echo 'Total Users'; ?>: <?php if (isset($total_Users)) echo $total_Users; ?></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>













        <span class="clear"></span>
        
    </div>








    <span class="clear"></span>
</div>

</div>
<?php
$this->load->view(OPERATOR_NAME . '/templates/footer.php');
?>