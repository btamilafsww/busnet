<?php
$this->load->view(OPERATOR_NAME.'/templates/header.php');

?>






<style>
    
.calendar-header{

    display: none;
}

</style>











<div id="content" class="admin-settings profile_set_panel edit-global-set">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap" style="background: #f3f3f3;">
                <div class="widget_top">
                    <span class="h_icon list"></span>
                    <h6><?php echo $heading; ?></h6>
                    <div id="widget_tab">
                    </div>
                </div>
                    <br>

                <div class="year_container">    
                <div class="row year_rower">


                <div class="col-sm-4"></div>

                 <div class="col-sm-4" style="text-align:center">

                 <label>Year : </label>
                <select onchange="year_change((this.options[this.selectedIndex].value))" id="selected_year">
                <?php

                $currentYear=date("Y");

                $year_starter=2017;



                for($i=$year_starter;$i <= $currentYear+1 ;$i++)
                {
                    if($year == $i)
                     {   
                    echo "<option selected value='".$i."'>".$i."</option>";
                      }
                      else
                      {

                         echo "<option value='".$i."'>".$i."</option>";
                      }  
                }





                ?>

                </select>

                <br>

                </div>

                 <div class="col-sm-4">
                     
                                <div class="row" style="font-size: 12px;">

                                <div class="weekdays_box">
                                <span><b>Days Having Holiday along the year : </b></span>
                                </div>
                                    <div class="col-sm-4">
                                        <div class="form-check form-check-inline">
                                        <input onchange="load_calender()" class="form-check-input" type="checkbox" id="sunday" value="sunday" <?php if (in_array("sunday", $choosed_days_check_box)){ echo "checked"; } ?>>
                                        <label class="form-check-label" for="sunday">Sunday</label>
                                        </div>
                                    </div>   

                                    <div class="col-sm-4">
                                        <div class="form-check form-check-inline">
                                        <input onchange="load_calender()" class="form-check-input" type="checkbox" id="monday" value="monday" <?php if (in_array("monday", $choosed_days_check_box)){ echo "checked"; } ?>>
                                        <label class="form-check-label" for="monday">Monday</label>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-check form-check-inline">
                                        <input onchange="load_calender()" class="form-check-input" type="checkbox" id="tuesday" value="tuesday"  <?php if (in_array("tuesday", $choosed_days_check_box)){ echo "checked"; } ?>>
                                        <label class="form-check-label" for="tuesday">Tuesday</label>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-check form-check-inline">
                                        <input onchange="load_calender()" class="form-check-input" type="checkbox" id="wednesday" value="wednesday"  <?php if (in_array("wednesday", $choosed_days_check_box)){ echo "checked"; } ?> >
                                        <label class="form-check-label" for="wednesday">Wednesday</label>
                                        </div>
                                    </div>


                                    <div class="col-sm-4">
                                            <div class="form-check form-check-inline">
                                            <input onchange="load_calender()" class="form-check-input" type="checkbox" id="thursday" value="thursday"   <?php if (in_array("thursday", $choosed_days_check_box)){ echo "checked"; } ?>>
                                            <label class="form-check-label" for="thursday">Thursday</label>
                                            </div>
                                    </div>

                                    <div class="col-sm-4">
                                            <div class="form-check form-check-inline">
                                            <input onchange="load_calender()" class="form-check-input" type="checkbox" id="friday" value="friday"  <?php if (in_array("friday", $choosed_days_check_box)){ echo "checked"; } ?>>
                                            <label class="form-check-label" for="friday">Friday</label>
                                            </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-check form-check-inline">
                                        <input onchange="load_calender()" class="form-check-input" type="checkbox" id="saturday" value="saturday"  <?php if (in_array("saturday", $choosed_days_check_box)){ echo "checked"; } ?>>
                                        <label class="form-check-label" for="saturday">Saturday</label>
                                        </div>
                                    </div>

                               </div>     

                 </div>


                </div>









              <form id="selected_year_form" action="<?php echo OPERATOR_NAME; ?>/settings/yearly_time_setting" method="POST">

              <input type="hidden" name="choosen_year" id="choosen_year"></input>

              </form>


                <div id="calendar"></div>










                <div class="modal modal-fade" id="event-modal" style="display: none;">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                                    <h4 class="modal-title">
                                        Event
                                    </h4>
                                </div>
                                <div class="modal-body">
                                    <input type="hidden" name="event-index" value="">
                                    <form class="form-horizontal">
                                        <div class="form-group">
                                            <label for="min-date" class="col-sm-4 control-label">Name</label>
                                            <div class="col-sm-7">
                                                <input name="event-name" type="text" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="min-date" class="col-sm-4 control-label">Event Type</label>
                                            <div class="col-sm-7">
                                               <!--  <input name="event-location" type="text" class="form-control"> -->
                                                <select id="event_type">
                                                    
                                                <option id="select_holiday" value="holiday">Holiday</option>
                                                <option id="select_function" value="function">Function</option>

                                                </select>

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="min-date" class="col-sm-4 control-label">Dates</label>
                                            <div class="col-sm-7">
                                                <div class="input-group input-daterange" data-provide="datepicker">
                                                    <input name="event-start-date" type="text" class="form-control" value="">
                                                    <span class="input-group-addon">to</span>
                                                    <input name="event-end-date" type="text" class="form-control" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                    <button type="button" class="btn btn-primary" id="save-event">
                                        Save
                                    </button>
                                </div>
                            </div>
                        </div>
                </div>


                <form id="data_submit_form" action="<?php echo OPERATOR_NAME; ?>/settings/save_update_yearly_time_setting" method="post">

                <input type="hidden" name="date_submitted" id="date_submitted">

                <input type="hidden" name="year" id="year">

                 <input type="hidden" name="dataSource_hidden" id="dataSource_hidden">

                 <input type="hidden" name="choosed_days_check_box" id="choosed_days_check_box">



                 </form>



                 <div style="text-align:center"> 

                 <button onclick="save()" type="button" class="btn btn-success">Save</button>

                </div>



                </div>


            </div>
        </div>
    </div>
    <span class="clear"></span>
</div>
</div>

<script src="bootstrap/calender/bootstrap-year-calendar.min.js"></script>
<script>
       


























var initial_year=<?php echo  $year; ?>;


var dataSource_initialize=<?php echo  json_encode($dataSource); ?>;




var dataSource_initialize_final_array=[];

for (var i = 0; i < dataSource_initialize.length; i++) {


    dataSource_initialize_final_array.push({id : dataSource_initialize[i]['id'] , name : dataSource_initialize[i]['name'], event_type : dataSource_initialize[i]['event_type'],endDate : new Date(JSON.parse("\""+dataSource_initialize[i]['endDate']+"\"")),startDate : new Date(JSON.parse("\""+dataSource_initialize[i]['startDate']+"\"")),color : dataSource_initialize[i]['color'] })



};











var holiday_dates=[];

var temp_Dates=[];



//////////////weekend getter
function getDefaultOffDays2(year) {

   holiday_dates=[]; 
var date = new Date(year, 0, 1);

var monday=new Date(year, 0, 1);


var tuesday=new Date(year, 0, 1);


var wednesday=new Date(year, 0, 1);

var thursday=new Date(year, 0, 1);


var friday=new Date(year, 0, 1);

var saturday=new Date(year, 0, 1);





  while (date.getDay() != 0) {

    date.setDate(date.getDate() + 1);

  }




  var days = [];


  //////sunday

  if(document.getElementById("sunday").checked)
  {

              while (date.getFullYear() == year) {



                var m = date.getMonth() + 1;
                var d = date.getDate();
              


                days.push(


                     new Date(year,   (m < 10 ? '' + m-1 : m-1),  (d < 10 ? '0' + d : d))

                 
                );




                holiday_dates.push(year+"-"+(m < 10 ? '0' + m : m)+"-"+(d < 10 ? '0' + d : d))


                date.setDate(date.getDate() + 7);

              }


  }


///////monday


  if(document.getElementById("monday").checked)
  {


              while (monday.getDay() != 1) {

            monday.setDate(monday.getDate() + 1);

            }



            while (monday.getFullYear() == year) {



            var m = monday.getMonth() + 1;
            var d = monday.getDate();
          


            days.push(


                 new Date(year,   (m < 10 ? '' + m-1 : m-1),  (d < 10 ? '0' + d : d))

             
            );




            holiday_dates.push(year+"-"+(m < 10 ? '0' + m : m)+"-"+(d < 10 ? '0' + d : d))


            monday.setDate(monday.getDate() + 7);

          }


  } 


///////tuesday




  if(document.getElementById("tuesday").checked)
  {



                while (tuesday.getDay() != 2) {

                tuesday.setDate(tuesday.getDate() + 1);

                }



                while (tuesday.getFullYear() == year) {



                var m = tuesday.getMonth() + 1;
                var d = tuesday.getDate();
              


                days.push(


                     new Date(year,   (m < 10 ? '' + m-1 : m-1),  (d < 10 ? '0' + d : d))

                 
                );




                holiday_dates.push(year+"-"+(m < 10 ? '0' + m : m)+"-"+(d < 10 ? '0' + d : d))


                tuesday.setDate(tuesday.getDate() + 7);

              }
 }

////////wednesday


  if(document.getElementById("wednesday").checked)
  {

            while (wednesday.getDay() != 3) {

            wednesday.setDate(wednesday.getDate() + 1);

            }



            while (wednesday.getFullYear() == year) {



            var m = wednesday.getMonth() + 1;
            var d = wednesday.getDate();
          


            days.push(


                 new Date(year,   (m < 10 ? '' + m-1 : m-1),  (d < 10 ? '0' + d : d))

             
            );




            holiday_dates.push(year+"-"+(m < 10 ? '0' + m : m)+"-"+(d < 10 ? '0' + d : d))


            wednesday.setDate(wednesday.getDate() + 7);

          }

  }


////////thursday

if(document.getElementById("thursday").checked)
  {
            while (thursday.getDay() != 4) {

            thursday.setDate(thursday.getDate() + 1);

            }



            while (thursday.getFullYear() == year) {



            var m = thursday.getMonth() + 1;
            var d = thursday.getDate();
          


            days.push(


                 new Date(year,   (m < 10 ? '' + m-1 : m-1),  (d < 10 ? '0' + d : d))

             
            );




            holiday_dates.push(year+"-"+(m < 10 ? '0' + m : m)+"-"+(d < 10 ? '0' + d : d))


            thursday.setDate(thursday.getDate() + 7);

          }

 }
////////friday

if(document.getElementById("friday").checked)
  {
            while (friday.getDay() != 5) {

            friday.setDate(friday.getDate() + 1);

            }



            while (friday.getFullYear() == year) {



            var m = friday.getMonth() + 1;
            var d = friday.getDate();
          


            days.push(


                 new Date(year,   (m < 10 ? '' + m-1 : m-1),  (d < 10 ? '0' + d : d))

             
            );




            holiday_dates.push(year+"-"+(m < 10 ? '0' + m : m)+"-"+(d < 10 ? '0' + d : d))


            friday.setDate(friday.getDate() + 7);

          }

}

////////saturday

if(document.getElementById("saturday").checked)
  {

                while (saturday.getDay() != 6) {

                saturday.setDate(saturday.getDate() + 1);

                }



                while (saturday.getFullYear() == year) {



                var m = saturday.getMonth() + 1;
                var d = saturday.getDate();
              


                days.push(


                     new Date(year,   (m < 10 ? '' + m-1 : m-1),  (d < 10 ? '0' + d : d))

                 
                );




                holiday_dates.push(year+"-"+(m < 10 ? '0' + m : m)+"-"+(d < 10 ? '0' + d : d))


                saturday.setDate(saturday.getDate() + 7);

              }

  }







  return days;
}






////////////date between two dates

Date.prototype.addDays = function(days) {
    var dateer = new Date(this.valueOf());
    dateer.setDate(dateer.getDate() + days);
    return dateer;
}

function getDates(startDate, stopDate) {

    

    var dateArray = new Array();
    var currentDate = startDate;
    while (currentDate <= stopDate) {

        dateArray.push(new Date (currentDate));


        var m = currentDate.getMonth() + 1;
         var d = currentDate.getDate();

        temp_Dates.push(currentDate.getFullYear()+"-"+(m < 10 ? '0' + m : m)+"-"+(d < 10 ? '0' + d : d)); 


        currentDate = currentDate.addDays(1);


        


    }
    


    return dateArray;
}




var select_stopper=0;


function year_change(data)
{
    if(select_stopper !=0)
    { 
    document.getElementById("choosen_year").value=data;
    document.getElementById("selected_year_form").submit();
    }
    else
    {
        select_stopper=1;
    }    


}







function save()
{

    var swap=holiday_dates;

    temp_Dates=swap;

   var dataSource = $('#calendar').data('calendar').getDataSource();

   

   for(var i=0;i< dataSource.length;i++)
   {

      getDates(dataSource[i].startDate, dataSource[i].endDate);


   }

    getDefaultOffDays2(initial_year);


   var choosed_days_check_box=[];

        $('input.form-check-input:checkbox:checked').each(function() {
               
                choosed_days_check_box.push(this.value);
        });



   document.getElementById("date_submitted").value=JSON.stringify(temp_Dates);

    document.getElementById("year").value=initial_year;

    document.getElementById("dataSource_hidden").value=JSON.stringify(dataSource);

     document.getElementById("choosed_days_check_box").value=JSON.stringify(choosed_days_check_box);

    
  
     document.getElementById("data_submit_form").submit();

}































//////////////////calender







function editEvent(event) {



    $('#event-modal input[name="event-index"]').val(event ? event.id : '');
    $('#event-modal input[name="event-name"]').val(event ? event.name : '');
    $('#event-modal input[name="event-start-date"]').datepicker('update', event ? event.startDate : '');
    $('#event-modal input[name="event-end-date"]').datepicker('update', event ? event.endDate : '');
    $('#event-modal').modal();
}

function deleteEvent(event) {
    var dataSource = $('#calendar').data('calendar').getDataSource();

    for(var i in dataSource) {
        if(dataSource[i].id == event.id) {
            dataSource.splice(i, 1);
            break;
        }
    }
    
    $('#calendar').data('calendar').setDataSource(dataSource);





}

function saveEvent() {

    if($('#event_type').val() == "holiday")
    {    
        var event = {
            id: $('#event-modal input[name="event-index"]').val(),
            name: $('#event-modal input[name="event-name"]').val(),
            event_type: $('#event_type').val(),
            startDate: $('#event-modal input[name="event-start-date"]').datepicker('getDate'),
            endDate: $('#event-modal input[name="event-end-date"]').datepicker('getDate'),

            color: 'Red'
        }
    }
    else
    {

        var event = {
            id: $('#event-modal input[name="event-index"]').val(),
            name: $('#event-modal input[name="event-name"]').val(),
            event_type: $('#event_type').val(),
            startDate: $('#event-modal input[name="event-start-date"]').datepicker('getDate'),
            endDate: $('#event-modal input[name="event-end-date"]').datepicker('getDate'),

            color: 'Blue'
        }

    }    

    var dataSource = $('#calendar').data('calendar').getDataSource();

    if(event.id) {
        for(var i in dataSource) {
            if(dataSource[i].id == event.id) {
                dataSource[i].name = event.name;
                dataSource[i].event_type = event.event_type;
                dataSource[i].startDate = event.startDate;
                dataSource[i].endDate = event.endDate;
            }
        }
    }
    else
    {
        var newId = 0;
        for(var i in dataSource) {
            if(dataSource[i].id > newId) {
                newId = dataSource[i].id;
            }
        }
        
        newId++;
        event.id = newId;
    
        dataSource.push(event);
    }
    











    $('#calendar').data('calendar').setDataSource(dataSource);
    $('#event-modal').modal('hide');
}

$(function() {



    load_calender();




});

    
function load_calender()
{

        days=[];

        holiday_dates=[];      

        var currentYear = initial_year;


        var sundays=getDefaultOffDays2(currentYear);



        $('#calendar').calendar({ 
            enableContextMenu: true,
            enableRangeSelection: true,
            contextMenuItems:[
                {
                    text: 'Update',
                    click: editEvent
                },
                {
                    text: 'Delete',
                    click: deleteEvent
                }
            ],

            
            selectRange: function(e) {
                editEvent({ startDate: e.startDate, endDate: e.endDate });
            },
            mouseOnDay: function(e) {
                if(e.events.length > 0) {
                    var content = '';
                    
                    for(var i in e.events) {
                        content += '<div class="event-tooltip-content">'
                                        + '<div class="event-name" style="color:' + e.events[i].color + '">' + e.events[i].name + '</div>'
                                        + '<div class="event-location">' + e.events[i].event_type + '</div>'
                                    + '</div>';
                    }
                
                    $(e.element).popover({ 
                        trigger: 'manual',
                        container: 'body',
                        html:true,
                        content: content
                    });
                    
                    $(e.element).popover('show');
                }
            },
             dataSource: dataSource_initialize_final_array,

             startYear:currentYear,
            disabledDays:sundays,

            mouseOutDay: function(e) {
                if(e.events.length > 0) {
                    $(e.element).popover('hide');
                }
            },
            dayContextMenu: function(e) {
                $(e.element).popover('hide');
            }
        });

        $('#save-event').click(function() {
            saveEvent();
        });


        
}




</script>
<?php 
$this->load->view(OPERATOR_NAME.'/templates/footer.php');
?>