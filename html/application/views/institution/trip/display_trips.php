<?php
$this->load->view(OPERATOR_NAME.'/templates/header.php');
?>
<div id="content">
		<div class="grid_container">
			<?php 
				$attributes = array('id' => 'display_form');
				echo form_open(OPERATOR_NAME.'/trip/display_trips',$attributes) 
			?>
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon blocks_images"></span>
						<h6><?php echo $heading?></h6>
					</div>
					<div class="widget_content">
						<?php if($paginationLink != '') { echo $paginationLink; $tble = 'allrides_tbl_operator'; } else { $tble='rides_tbl_operator';}?>
						
						<table class="display display_tbl" id="<?php echo $tble; ?>">
						<thead>
						<?php 
						if ($this->lang->line('dash_click_sort') != '') $dash_click_sort = stripslashes($this->lang->line('dash_click_sort')); else $dash_click_sort = 'Click to sort'; ?>
						<tr>
							<th class="center tip_top" title="<?php echo $dash_click_sort; ?>">
								<?php if ($this->lang->line('operator_s_no') != '') echo stripslashes($this->lang->line('operator_s_no')); else echo 'S.No'; ?>
							</th>
							<th class="tip_top" title="<?php echo $dash_click_sort; ?>">
								<?php if ($this->lang->line('operator_ride_id') != '') echo stripslashes($this->lang->line('operator_ride_id')); else echo 'Ride Id'; ?>
							</th>
							<th class="tip_top" title="<?php echo $dash_click_sort; ?>">
								 <?php if ($this->lang->line('operator_booked_date') != '') echo stripslashes($this->lang->line('operator_booked_date')); else echo 'Booked Date'; ?>
							</th>
							<th class="tip_top" title="<?php echo $dash_click_sort; ?>">
								<?php if ($this->lang->line('operator_user') != '') echo stripslashes($this->lang->line('operator_user')); else echo 'User'; ?>
							</th>
							<th class="tip_top" title="<?php echo $dash_click_sort; ?>">
								<?php if ($this->lang->line('operator_status') != '') echo stripslashes($this->lang->line('operator_status')); else echo 'Status'; ?>
							</th>
							<th >
								<?php if ($this->lang->line('dash_driver') != '') echo stripslashes($this->lang->line('dash_driver')); else echo 'Driver'; ?> 
							</th>
							<th>
								 <?php if ($this->lang->line('operator_action') != '') echo stripslashes($this->lang->line('operator_action')); else echo 'Action'; ?>
							</th>
						</tr>
						</thead>
						<tbody>
						<?php 
						if ($ridesList->num_rows() > 0){ $i = $offsetVal+1;
							foreach ($ridesList->result() as $row){
						?>
						<tr>
							<td class="center tr_select ">
								<?php echo $i;?>
							</td>
							<td class="center">
								<?php  if(isset($row->ride_id)) echo $row->ride_id;?>
							</td>
							<td class="center">
								<?php  
								$bookDateSec = $row->booking_information['booking_date']->sec;
								if(isset($row->booking_information['booking_date'])) echo get_time_to_string('d,M-Y h:i A',$bookDateSec);
								?>
							</td>
							<td class="center">
								<?php if($isDemo){ ?>
								<?php echo $dEmail; ?>
								<?php }  else{ ?>
								<?php  if(isset($row->user['email'])) echo $row->user['email'];?>
								<?php } ?>
							</td>
							<td class="center" <?php if($row->ride_status == 'Cancelled') echo 'style="color:red;"'; ?>>
								<?php if (isset($row->ride_status)) echo get_language_value_for_keyword($row->ride_status,$this->data['langCode']); ?>
							</td>
							
							
							<td class="center">
								<ul class="action_list">
									<li style="width:100%;">
										<?php if($row->ride_status=='Booked'){ ?>
										<a class="p_car tipTop" href="<?php echo OPERATOR_NAME; ?>/trip/available_drivers_list/<?php echo $row->_id;?>" title="<?php if ($this->lang->line('admin_rides_assign_cab') != '') echo stripslashes($this->lang->line('admin_rides_assign_cab')); else echo 'Assign Cab'; ?>">
											<?php if ($this->lang->line('dash_operator_assign_driver') != '') echo stripslashes($this->lang->line('dash_operator_assign_driver')); else echo 'Assign Driver'; ?> 
										</a>
										<?php  } else if(isset($row->driver['id'])){ 
											echo $row->driver['name'];
										?>
											
										<?php } else { echo '--'; }?>
									</li>
								</ul>
							</td>
							
							<td class="center" width="23%">
								<ul class="action_list">
								
								<?php 
								
								
								$styleLi = '';
								$btnStyle = 'float:right !important;';
								if ($row->ride_status == 'Booked' || $row->ride_status == 'Confirmed' || $row->ride_status == 'Arrived') {
									$styleLi = '';
								?>
									
									<li style="">
										<a   href="<?php echo OPERATOR_NAME; ?>/trip/cancelling_ride_form?ride_id=<?php echo $row->ride_id;?>" class="p_del tipTop" title="<?php if ($this->lang->line('admin_rides_cancel_ride') != '') echo stripslashes($this->lang->line('admin_rides_cancel_ride')); else echo 'Cancel Ride'; ?>">
											<span class="c_delete"></span> <?php if ($this->lang->line('profile_mobile_number_edit_cancel_option') != '') echo stripslashes($this->lang->line('profile_mobile_number_edit_cancel_option')); else echo 'Cancel'; ?> 
										</a>
									</li>
								<?php } else if($row->ride_status == 'Cancelled'){ ?>
									
									
									<li style="padding: 0 10px;">
										<span class="note"><?php if ($this->lang->line('dash_operator_cancelled') != '') echo stripslashes($this->lang->line('dash_operator_cancelled')); else echo 'Cancelled'; ?> <br/> 
										(<?php if (isset($row->cancelled['primary']['by'])) echo get_language_value_for_keyword(strtolower($row->cancelled['primary']['by']),$this->data['langCode']); ?>)
										</span>
									</li>
									
								<?php } ?>
									<li style="width:<?php echo $styleLi; ?>;">
										<a class="p_edit tipTop" href="<?php echo OPERATOR_NAME; ?>/trip/view_trip/<?php echo $row->_id;?>" title="<?php if ($this->lang->line('operator_rides_view_details') != '') echo stripslashes($this->lang->line('operator_rides_view_details')); else echo 'View Trip details'; ?>" style="<?php echo $btnStyle; ?>">
											<?php if ($this->lang->line('operator_rides_view_details') != '') echo stripslashes($this->lang->line('operator_rides_view_details')); else echo 'View Trip details'; ?>
										</a>
									</li>
								</ul>
							</td>
						</tr>
						<?php 
							$i++;
							}
						}
						?>
						</tbody>
						<tfoot>
						<tr>
							<th>
								<?php if ($this->lang->line('operator_s_no') != '') echo stripslashes($this->lang->line('operator_s_no')); else echo 'S.No'; ?>
							</th>
							<th>
								<?php if ($this->lang->line('operator_ride_id') != '') echo stripslashes($this->lang->line('operator_ride_id')); else echo 'Ride Id'; ?>
							</th>
							<th>
								 <?php if ($this->lang->line('operator_booked_date') != '') echo stripslashes($this->lang->line('operator_booked_date')); else echo 'Booked Date'; ?>
							</th>
							<th>
								<?php if ($this->lang->line('operator_user') != '') echo stripslashes($this->lang->line('operator_user')); else echo 'User'; ?>
							</th>
							<th>
								<?php if ($this->lang->line('operator_status') != '') echo stripslashes($this->lang->line('operator_status')); else echo 'Status'; ?>
							</th>
							<th>
								<?php if ($this->lang->line('dash_driver') != '') echo stripslashes($this->lang->line('dash_driver')); else echo 'Driver'; ?> 
							</th>
							<th>
								 <?php if ($this->lang->line('operator_action') != '') echo stripslashes($this->lang->line('operator_action')); else echo 'Action'; ?>
							</th>
						</tr>
						</tfoot>
						</table>
						<?php if($paginationLink != '') { echo $paginationLink; } ?>
					</div>
				</div>
			</div>
			<input type="hidden" name="statusMode" id="statusMode"/>
			<input type="hidden" name="SubAdminEmail" id="SubAdminEmail"/>
		</form>	
			
		</div>
		<span class="clear"></span>
	</div>
</div>


<div id="cancel_ride" style="display:none;">
	<h3><?php if ($this->lang->line('admin_menu_cancel_ride') != '') echo stripslashes($this->lang->line('admin_menu_cancel_ride')); else echo 'Cancel Ride'; ?> #<span id="ride_id_txt"></span></h3>
   <div class="widget_content">
	<form action="<?php echo OPERATOR_NAME; ?>/trip/cancelling_ride" method="post" id="book_trip">
		<ul>
			<li class="fare_list">
				<div class="form_grid_12 fareInner" style="margin-top: 22px !important;">
					<label class="field_title"><?php if ($this->lang->line('dash_operator_cancellation_made_by') != '') echo stripslashes($this->lang->line('dash_operator_cancellation_made_by')); else echo 'Cancellation Made By'; ?></label>
					<div class="form_input">
						<select name="cancelled_by" class="required" id="cancelled_by" onchange="get_cancellationReason();">
							<option value=""><?php if ($this->lang->line('dash_operator_cancellation_made_by') != '') echo stripslashes($this->lang->line('dash_operator_cancellation_made_by')); else echo 'Cancellation made by'; ?>...</option>
							<option value="User"><?php if ($this->lang->line('driver_user') != '') echo stripslashes($this->lang->line('driver_user')); else echo 'User'; ?></option>
							<option value="Driver"><?php if ($this->lang->line('dash_driver') != '') echo stripslashes($this->lang->line('dash_driver')); else echo 'Driver'; ?></option>
						</select>
						<img src="images/indicator.gif" id="reason_loader" style="display:none;">
					</div>
				</div>
			</li>
			<li class="fare_list">
				<div class="form_grid_12 fareInner">
					<label class="field_title"><?php if ($this->lang->line('admin_cancellation_reason_name') != '') echo stripslashes($this->lang->line('admin_cancellation_reason_name')); else echo 'Cancellation Reason'; ?></label>
					<div class="form_input">
						<div class="form_input">
						<select name="reason" class="required" id="cancellation_reason">
							<option value=""><?php if ($this->lang->line('dash_operator_choose_cancellation_reason') != '') echo stripslashes($this->lang->line('dash_operator_choose_cancellation_reason')); else echo 'Choose cancellation reason'; ?>...</option>
						</select>
					</div>
					</div>
				</div>
			</li>
		
			<li class="fare_list errNote">
				<div class="form_grid_12">
					<label class="field_title"></label>
					<div class="form_input">
						<span id="note"></span>
					</div>
				</div>
			</li>	
			<li>
				<div class="form_grid_12 submitForm">
					<div class="form_input">
						<button  style="margin-top: 40px;" class="btn_small btn_blue" type="submit"><span><?php if ($this->lang->line('admin_settings_submit') != '') echo stripslashes($this->lang->line('admin_settings_submit')); else echo 'Submit'; ?></span></button>
					</div>
				</div>
			</li>
		</ul>
		<input type="hidden" name="ride_id" id="ride_id" value="" />
	</form>
</div>
</div>


<style>
	.submitForm {
		text-align: center; 
		width: 100%;
	}
	.fare_list {
		background-color: #B8C5CC !important;
		background:none;
	}
	 
	.fareInner {
		width:70% !important;
		margin-top: 42px !important;
	}
	
	.fareInner .form_input{
		float:right !important;
	}
	
	.errNote .form_grid_12{
		margin-top: 20px !important;
	}
	
	#fare_estimate {
		border-bottom: medium solid;
		color: #06c;
	}
	
	.field_title {
		font-size: 14px;
	}
	

</style>

<script>
$(document).ready(function() {
	$('#book_trip').validate();
});
function cancelRide_Popup(ride_id){
	$('#ride_id_txt').html(ride_id);
	$('#ride_id').val(ride_id);
	$('#cancel_ride').modal();
}

function get_cancellationReason(){
	var cancelled_by = $('#cancelled_by').val(); 
	if(cancelled_by != ''){
		$('#reason_loader').css('display','block');
		$.ajax({
			  type: "POST",
			  url: "<?php echo OPERATOR_NAME; ?>/trip/get_cancellation_reason",
			  data: {'cancelled_by':cancelled_by},
			  success: function (result) {
				$('#cancellation_reason').html(result.reasons);
				$('#reason_loader').css('display','none');
			  },
			  dataType: "json"
			});
	}
}

</script>
<style>

select{
padding:5px;
font-size:15px;
}
</style>
<?php 
$this->load->view(OPERATOR_NAME.'/templates/footer.php');
?>