<?php
$this->load->view(OPERATOR_NAME.'/templates/header.php');
?>
<?php
$type = '';
if(isset($_GET['type']) && $_GET['type']!='') {
		$type=$_GET['type'];
}
$dialcode=array();

foreach ($countryList as $country) {	
	 if ($country->dial_code != '') {			
			$dialcode[]=str_replace(' ', '', $country->dial_code);
	 }
}
 
asort($dialcode);
$dialcode=array_unique($dialcode);                                    
?>

<script>
$(document).ready(function(){
		$vehicle_category='';
		$country='';
		<?php  if($type == 'vehicle_type' && isset($_GET['vehicle_category'])) { ?>
		$vehicle_category = "<?php echo $_GET['vehicle_category']; ?>";
		<?php }?>
		<?php  if($type == 'mobile_number' &&  isset($_GET['country'])) {?>
		$country = "<?php echo $_GET['country']; ?>";
		<?php }?>
		if($vehicle_category != ''){
				$('.vehicle_category').css("display","inline");
				$('#filtervalue').css("display","none");
				$('#filtervalue').css("display","block");
				$("#country").attr("disabled", true);
		}
		if($country != ''){
				$('#country').css("display","inline");
				$('.vehicle_category').attr("disabled", true);		
		}
		$("#filtertype").change(function(){
				$filter_val = $(this).val();
				$('#filtervalue').val('');
				$('.vehicle_category').css("display","none");
				$('#filtervalue').css("display","inline");
				$('#country').css("display","none");
				$("#country").attr("disabled", true);
				$(".vehicle_category").attr("disabled", true);
			
				if($filter_val == 'vehicle_type'){
						$('.vehicle_category').css("display","inline");
						$('#filtervalue').css("display","none");
						$('#country').css("display","none");
						$('.vehicle_category').prop("disabled", false);
						$("#country").attr("disabled", true);
				}
				if($filter_val == 'mobile_number'){
						$('#country').css("display","inline");
						$('#country').prop("disabled", false);
						$(".vehicle_category").attr("disabled", true);
						$('.vehicle_category').css("display","none");
				}			
		});	
});
</script>
<div id="content">
    <div class="grid_container">
	
	
		<?php
		$attributes = array('id' => 'display_form');
		echo form_open(OPERATOR_NAME.'/trip/trip_list', $attributes)
		?>
        <div class="grid_12">
            <div class="widget_wrap">
                <div class="widget_top">
                    <span class="h_icon blocks_images"></span>
                    <h6><?php echo $heading ?></h6>

                </div>
                <div class="widget_content">
                    <?php
                    if ($paginationLink != '') {
                        echo $paginationLink;
                        $tble = 'allvehicle_location';
                    } else {
                        $tble = 'vehicle_location';
                    }
                    ?>

                    <table class="display" id="<?php echo $tble; ?>" width='100%'>
                        <thead>
                            <tr>
                                <th style="width:65px;text-align: center;" class="center">
                           			 <?php if ($this->lang->line('operator_s_no') != '') echo stripslashes($this->lang->line('operator_s_no')); else echo 'S.No'; ?>
                            
                                </th>
                                <th style="width:170px;text-align: center;" class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                    <?php if ($this->lang->line('Trip_Name') != '') echo stripslashes($this->lang->line('Trip_Name')); else echo 'Trip Name'; ?>
                            
                                </th>
                                 <th style="text-align: center;" class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                    <?php if ($this->lang->line('driver_name') != '') echo stripslashes($this->lang->line('driver_name')); else echo 'Driver Name'; ?>
                            
                                </th>
                                <th style="text-align: center;" class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                    <?php if ($this->lang->line('start_lat_long') != '') echo stripslashes($this->lang->line('start_lat_long')); else echo 'Starting Co-ordinates'; ?>
                             
                                </th>
                               
                                 <th style="text-align: center;">
                                    <?php if ($this->lang->line('admin_subadmin_action') != '') echo stripslashes($this->lang->line('admin_subadmin_action')); else echo 'Action'; ?>
                                </th>
                                
                                
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 0;

                           
                            if ($trip_detail_list->num_rows() > 0) {
							
                                foreach ($trip_detail_list->result() as $row) { $i++;
                                    ?>
                                    <tr style="border-bottom: 1px solid #dddddd !important;">
                                        <td class="center tr_select ">
                                            <?php echo $i; ?>
                                        </td>
                                        <td class="center">
                                            <?php echo $row->trip_name; ?>
										
                                        </td>
										
															
                                        <td class="center" style="width:70px;">
                                            <?php echo $row->driver_name; ?>
                                        </td>		
                                        <td class="center" style="width:200px;">
                                             <?php  if(isset($row->start_address)){ echo $row->start_address ; }else{ echo "-";  } ?>
                                        </td>								
                                      




                                        <td class="center action-icons-wrap" style="width:140px;">
                                        
                                        
                                         
    
                                            
                                        
                                            <span><a style="float: unset;" class="action-icons c-suspend" href="<?php echo OPERATOR_NAME; ?>/trip/vechicle_current_location/<?php echo $row->_id; ?>/<?php echo $page_type; ?>" title="<?php if ($this->lang->line('admin_common_view') != '') echo stripslashes($this->lang->line('admin_common_view')); else echo 'View'; ?>"></a></span>                                          
                                        </td>






                                      
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                          		 <th style="width:65px;text-align: center;" class="center">
                           			 <?php if ($this->lang->line('operator_s_no') != '') echo stripslashes($this->lang->line('operator_s_no')); else echo 'S.No'; ?>
                            
                                </th>
                                <th  style="width:170px;text-align: center;" class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                    <?php if ($this->lang->line('Trip_Name') != '') echo stripslashes($this->lang->line('Trip_Name')); else echo 'Trip Name'; ?>
                            
                                </th>
                                 <th style="text-align: center;" class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                    <?php if ($this->lang->line('driver_name') != '') echo stripslashes($this->lang->line('driver_name')); else echo 'Driver Name'; ?>
                            
                                </th>
                                <th style="text-align: center;" class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
                                    <?php if ($this->lang->line('start_lat_long') != '') echo stripslashes($this->lang->line('start_lat_long')); else echo 'Starting Co-ordinates'; ?>
                             
                                </th>
                               
                                 <th style="text-align: center;" style="text-align: center;">
                                    <?php if ($this->lang->line('admin_subadmin_action') != '') echo stripslashes($this->lang->line('admin_subadmin_action')); else echo 'Action'; ?>
                                </th>                                
                              
                            </tr>
                        </tfoot>
                    </table>

                    <?php
                    if ($paginationLink != '') {
                        echo $paginationLink;
                    }
                    ?>

                </div>
            </div>
        </div>
        <input type="hidden" name="statusMode" id="statusMode"/>
        <input type="hidden" name="SubAdminEmail" id="SubAdminEmail"/>
        </form>	
    </div>
    <span class="clear"></span>
</div>
</div>
<style>										
		.b_warn {
			background: orangered none repeat scroll 0 0;
			border: medium none red;
		}
		
		.filter_widget .btn_30_light {
			margin: -11px;
			width: 83%;
		}
</style>
<?php
$this->load->view(OPERATOR_NAME.'/templates/footer.php');
?>