<?php
$this->load->view('admin/templates/header.php');
if(isset($privileges))
extract($privileges);
?>
<div id="content">
		<div class="grid_container">
				<?php 
						$attributes = array('id' => 'display_form');
						echo form_open('admin/transporter/change_operator_status_global',$attributes) 
				?>

				<script>

				</script>	


				<div class="grid_12">
						<div class="widget_wrap">
								<div class="widget_top">
										<span class="h_icon blocks_images"></span>
												<h6><?php echo $heading?></h6>
												
												<div style="float: right;line-height:40px;padding:0px 10px;height:39px;">
											
												<?php if ($allPrev == '1' || in_array('1', $transporter)){?>
														<div class="btn_30_light" style="height: 29px; text-align:left;">
																<a href="admin/transporter/add_edit_transporter_form" class="tipTop" title="<?php if ($this->lang->line('admin_menu_add_transporter') != '') echo stripslashes($this->lang->line('admin_menu_add_transporter')); else echo 'Click here to Add New Transport Manager'; ?>"><span class="icon add_co addnew-btn"></span><!-- <span class="btn_link"><?php if ($this->lang->line('admin_common_add_new') != '') echo stripslashes($this->lang->line('admin_common_add_new')); else echo 'Add New'; ?></span> --></a>
														</div>
												<?php } ?>
											
											<?php 
											if ($allPrev == '1' || in_array('3', $transporter)){
											?>
												<div class="btn_30_light" style="height: 29px;">
													<a href="javascript:void(0)" onclick="return checkBoxValidationAdmin('Delete','<?php echo $subAdminMail; ?>');" class="tipTop" title="<?php if ($this->lang->line('common_select_delete_records') != '') echo stripslashes($this->lang->line('common_select_delete_records')); else echo 'Select any checkbox and click here to delete records'; ?>"><!--<span class="icon cross_co del-btn"></span>--> <span class="btn_link"><?php if ($this->lang->line('admin_partner_delete') != '') echo stripslashes($this->lang->line('admin_partner_delete')); else echo 'Delete'; ?></span></a>
												</div>
											<?php }?>
									</div>
							</div>
							<div class="widget_content">
									<table class="display display_tbl" id="operator_tbl_admin">
											<thead>
													<tr>
															<th class="center">
																	<input name="checkbox_id[]" type="checkbox" value="on" class="checkall">
															</th>
															<th class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
																	<?php if ($this->lang->line('admin_transport_name') != '') echo stripslashes($this->lang->line('admin_transport_name')); else echo 'Transport Operator Name'; ?>
															</th>
															<th class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
																	<?php if ($this->lang->line('admin_transport_email') != '') echo stripslashes($this->lang->line('admin_transport_email')); else echo 'Transport Operator Email'; ?>
															</th>
															<th class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
																	<?php if ($this->lang->line('cms_phone') != '') echo stripslashes($this->lang->line('cms_phone')); else echo 'Phone Number'; ?>
															</th>
															






															<th class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
																	<?php if ($this->lang->line('students_count') != '') echo stripslashes($this->lang->line('students_count')); else echo 'Students Count'; ?> 
															</th>


															<th class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
																	<?php if ($this->lang->line('parents_count') != '') echo stripslashes($this->lang->line('parents_count')); else echo 'Users Count (Parent)'; ?> 
															</th>



															<th class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
																	<?php if ($this->lang->line('admin_driver_list_doj') != '') echo stripslashes($this->lang->line('admin_driver_list_doj')); else echo 'DOJ'; ?> 
															</th>
															<th class="tip_top" title="<?php if ($this->lang->line('dash_click_sort') != '') echo stripslashes($this->lang->line('dash_click_sort')); else echo 'Click to sort'; ?>">
																	<?php if ($this->lang->line('admin_common_status') != '') echo stripslashes($this->lang->line('admin_common_status')); else echo 'Status'; ?> 
															</th>
															<th>
																 <?php if ($this->lang->line('admin_common_action') != '') echo stripslashes($this->lang->line('admin_common_action')); else echo 'Action'; ?>
															</th>
													</tr>
											</thead>
											<tbody>
													<?php 
													if (!empty($operatorsList)){
														foreach ($operatorsList as $row){
													?>
													<tr>
															<td class="center tr_select">
																	<input name="checkbox_id[]" type="checkbox" value="<?php echo $row['_id'];?>">
															</td>
															<td class="center">
																	<?php echo $row['operator_name'];?>
															</td>
															<td class="center">
																	<?php echo $row['email'];?>
															</td>
															<td class="center">
																	<?php echo $row['dail_code'].$row['mobile_number'];?>
															</td>	









															<td class="center">
																	<?php echo $row['student_details_count']; ?>
															</td>
															<td>

															<?php 

															$counting_users=0;

															$email_repeater=array();


														

															foreach ($row['student_details'] as $key => $data_guardian) {

															

																foreach ($data_guardian['guardian_details'] as $key => $data_guard)
																{
																
																

																				$condition= array('email' => $data_guard['parent_email']  );


																				$data_returned= $this->user_model->get_all_details(USERS, $condition);


																				if($data_returned->num_rows() > 0 && !in_array($data_guard['parent_email'], $email_repeater) )
																				{
																					$counting_users=$counting_users+1;

																					array_push($email_repeater,$data_guard['parent_email']);
																				}

																				

																}
																
															}

															echo $counting_users;

															  ?>
																
															</td>













															<td class="center">
																	<?php echo get_time_to_string('Y-m-d',strtotime($row['created'])); ?>
															</td>
														
															<td class="center">
																	<?php 
																	$disp_status = get_language_value_for_keyword($row['status'],$this->data['langCode']);
																	if ($allPrev == '1' || in_array('2', $transporter)){
																			$mode = ($row['status'] == 'Active')?'0':'1';
																			if ($mode == '0'){
																	?>
																					<a title="<?php if ($this->lang->line('common_click_inactive') != '') echo stripslashes($this->lang->line('common_click_inactive')); else echo 'Click to inactive'; ?>" class="tip_top" href="javascript:confirm_status('admin/transporter/change_transporter_status/<?php echo $mode;?>/<?php echo $row['_id'];?>');"><span class="badge_style b_done"><?php echo $disp_status;?></span></a>
																	<?php
																			}else {	
																	?>
																					<a title="<?php if ($this->lang->line('common_click_active') != '') echo stripslashes($this->lang->line('common_click_active')); else echo 'Click to active'; ?>" class="tip_top" href="javascript:confirm_status('admin/transporter/change_transporter_status/<?php echo $mode;?>/<?php echo $row['_id'];?>')"><span class="badge_style"><?php echo $disp_status;?></span></a>
																	<?php 
																			}
																	}else {
																	?>
																			<span class="badge_style b_done"><?php echo $disp_status;?></span>
																	<?php }?>
															</td>
															<td class="center action-icons-wrap" style="width: 7%;">
																	<span>
																			<a class="action-icons c-suspend" href="admin/transporter/view_transporter/<?php echo $row['_id']; ?>" title="<?php if ($this->lang->line('admin_common_view') != '') echo stripslashes($this->lang->line('admin_common_view')); else echo 'View'; ?>">
																					<?php if ($this->lang->line('admin_common_view') != '') echo stripslashes($this->lang->line('admin_common_view')); else echo 'View'; ?>
																			</a>
																	</span>
																	
																	<?php if ($allPrev == '1' || in_array('2', $transporter)){?>
																			<span><a class="action-icons c-key" href="admin/transporter/change_password_form/<?php echo $row['_id']; ?>" title="<?php if ($this->lang->line('admin_user_change_password') != '') echo stripslashes($this->lang->line('admin_user_change_password')); else echo 'Change Password'; ?>"><?php if ($this->lang->line('driver_change_password') != '') echo stripslashes($this->lang->line('driver_change_password')); else echo 'Change Password'; ?></a></span>
																	<?php }?>
																	
																	<?php if ($allPrev == '1' || in_array('2', $transporter)){?>
																	<span>
																			<a class="action-icons c-edit" href="admin/transporter/add_edit_transporter_form/<?php echo $row['_id']; ?>" title="<?php if ($this->lang->line('admin_common_edit') != '') echo stripslashes($this->lang->line('admin_common_edit')); else echo 'Edit'; ?>">
																					<?php if ($this->lang->line('admin_common_edit') != '') echo stripslashes($this->lang->line('admin_common_edit')); else echo 'Edit'; ?>
																			</a>
																	</span>
																	<?php }?>
																	
																	<?php if ($allPrev == '1' || in_array('3', $transporter)){?>	
																	<span>
																			<a class="action-icons c-delete" href="javascript:confirm_delete('admin/transporter/delete_transporter/<?php echo $row['_id'];?>')" title="<?php if ($this->lang->line('admin_common_delete') != '') echo stripslashes($this->lang->line('admin_common_delete')); else echo 'Delete'; ?>">
																					<?php if ($this->lang->line('admin_common_delete') != '') echo stripslashes($this->lang->line('admin_common_delete')); else echo 'Delete'; ?>
																			</a>
																	</span>
																	<?php }?>


																	<?php if($transporter_switch == "yes") { ?>
																	<span>
																			<a class="action-icons  c-switch" target="_blank" href="admin/transporter/switch_to_transporter_session/<?php echo $row['_id'];?>" title="<?php if ($this->lang->line('switch_session') != '') echo stripslashes($this->lang->line('switch_session')); else echo 'Switch Session'; ?>">
																					
																			</a>
																	</span>
																	<?php }?>






															</td>
													</tr>
													<?php 
															}
													}
													?>
											</tbody>
											<tfoot>
													<tr>
															<th class="center">
																<input name="checkbox_id[]" type="checkbox" value="on" class="checkall">
															</th>
															<th>
																<?php if ($this->lang->line('admin_transport_name') != '') echo stripslashes($this->lang->line('admin_transport_name')); else echo 'Transport Manager Name'; ?>
															</th>
															<th>
																<?php if ($this->lang->line('admin_transport_email') != '') echo stripslashes($this->lang->line('admin_transport_email')); else echo 'Transport Manager Email'; ?>
															</th>
															<th>
																<?php if ($this->lang->line('cms_phone') != '') echo stripslashes($this->lang->line('cms_phone')); else echo 'Phone Number'; ?>
															</th>
															





															<th >
																	<?php if ($this->lang->line('students_count') != '') echo stripslashes($this->lang->line('students_count')); else echo 'Students Count'; ?> 
															</th>


															<th >
																	<?php if ($this->lang->line('parents_count') != '') echo stripslashes($this->lang->line('parents_count')); else echo 'Users Count (Parent)'; ?> 
															</th>





															<th>
																<?php if ($this->lang->line('admin_driver_list_doj') != '') echo stripslashes($this->lang->line('admin_driver_list_doj')); else echo 'DOJ'; ?>
															</th>
															<th>
																<?php if ($this->lang->line('admin_common_status') != '') echo stripslashes($this->lang->line('admin_common_status')); else echo 'Status'; ?> 
															</th>
															<th>
																<?php if ($this->lang->line('admin_common_action') != '') echo stripslashes($this->lang->line('admin_common_action')); else echo 'Action'; ?>
															</th>
													</tr>
											</tfoot>
									</table>
							</div>
				</div>
			</div>
			<input type="hidden" name="statusMode" id="statusMode"/>
			<input type="hidden" name="SubAdminEmail" id="SubAdminEmail"/>
		</form>	
			
		</div>
		<span class="clear"></span>
	</div>
</div>
<?php 
$this->load->view('admin/templates/footer.php');
?>