<?php
$this->load->view('admin/templates/header.php');
if (isset($privileges) && is_array($privileges)){
extract($privileges);
}
?> 
<?php if (!empty($monthlyEarningsGraphNew)) { ?>
<link rel="stylesheet" href="plugins/jqwidgets-master/jqwidgets/styles/jqx.base.css" type="text/css" />
<script type="text/javascript" src="plugins/jqwidgets-master/jqwidgets/jqxcore.js"></script>
<script type="text/javascript" src="plugins/jqwidgets-master/jqwidgets/jqxdata.js"></script>
<script type="text/javascript" src="plugins/jqwidgets-master/jqwidgets/jqxdraw.js"></script>
<script type="text/javascript" src="plugins/jqwidgets-master/jqwidgets/jqxchart.core.js"></script>
    <script type="text/javascript" src="plugins/jqwidgets-master/jqwidgets/jqxchart.rangeselector.js"></script>
<script type="text/javascript">
$(document).ready(function () {
	
	// prepare chart data as an array
	var monthVal = jQuery.parseJSON('<?php echo json_encode($monthArr); ?>');
	var totalEarningsaNew = jQuery.parseJSON('<?php echo json_encode($monthlyEarningsGraphNew); ?>');
	var driverEarningsaNew = jQuery.parseJSON('<?php echo json_encode($monthlyDriverEarningsGraphNew); ?>');
	var siteearningsaNew = jQuery.parseJSON('<?php echo json_encode($monthlySiteEarningsGraphNew); ?>');
	

	// prepare jqxChart settings
	var settings = {
		title: "",
		description: "",
		enableAnimations: true,
		animationDuration: 2500,
		showLegend: true,
		padding: { left: 5, top: 20, right: 30, bottom: 5 },
		titlePadding: { left: 90, top: 0, right: 0, bottom: 10 },
		source: monthVal,
		enableCrosshairs: true,
		xAxis: { 
			dataField: 'Month', 
			displayText: '<?php if ($this->lang->line('this_month') != '') echo stripslashes($this->lang->line('this_month')); else echo 'This Month'; ?>', 
			gridLines: { visible: true },
			rangeSelector: {
                            serieType: 'area',
                            padding: { /*left: 0, right: 0,*/ top: 20, bottom: 0 },
                            backgroundColor: 'white',
                            size: 110,
                            gridLines: {visible: false},
                        }
		},
		colorScheme: 'scheme01',
		valueAxis: { visible: true, title: { text: '<?php if ($this->lang->line('amount_in') != '') echo stripslashes($this->lang->line('amount_in')); else echo 'Amount in'; ?> <?php echo $dcurrencySymbol; ?>' } },
		seriesGroups:
			[
				{
					type: 'stackedarea',
					source: totalEarningsaNew,
					series: [
						  { dataField: 'Amount', displayText: '<?php echo get_language_value_for_keyword("Total",$this->data['langCode']); ?>' }
					]
				},
				{
					type: 'stackedline',
					source: driverEarningsaNew,
					series: [
							{ dataField: 'Amount', displayText: '<?php echo get_language_value_for_keyword("Driver",$this->data['langCode']); ?>' }
					]
				},
				{
					type: 'stackedline',
					source: siteearningsaNew,
					series: [
							{ dataField: 'Amount', displayText: '<?php echo get_language_value_for_keyword("Site",$this->data['langCode']); ?>' }
					]
				}
			]
	};

	// setup the chart
	$('#siteEarnings').jqxChart(settings);
});
</script>


<style>


.stat_block{
	padding: 15px 0;
	background:#ffffff;
}
.data_widget{
	margin: 20px 0;
}
.stat_chart .chart_label{
	text-align: right;
}


</style>
	
	
<?php } ?>
<div id="content" style="clear:both;">
    <div class="grid_container">		
            



        <div class="grid_6" style="margin-top: 20px;">
            <div class="widget_wrap">
                <div class="widget_top">
                    <span class="h_icon graph"></span>
                    <h6><?php if ($this->lang->line('admin_dashboard_drivers') != '') echo stripslashes($this->lang->line('admin_dashboard_drivers')); else echo 'Drivers'; ?></h6>
                </div>
                <div class="widget_content">
                    <div class="stat_block">	
                        <div class="stat_chart">
                            <h4><?php if ($this->lang->line('admin_dashboard_drivers_count') != '') echo stripslashes($this->lang->line('admin_dashboard_drivers_count')); else echo 'Drivers Count'; ?> : <?php echo (isset($totalDrivers)) ? $totalDrivers:""; ?></h4>
                            <table>
                                <tbody>
                                    <tr>
                                        <td>
                                            <?php if ($this->lang->line('admin_dashboard_today') != '') echo stripslashes($this->lang->line('admin_dashboard_today')); else echo 'Today'; ?>
                                        </td>
                                        <td>
                                            <?php if (isset($todayDrivers)) echo number_format($todayDrivers,0); ?>
                                        </td>
                                        <?php /* <td class="min_chart">
                                          <span class="bar">20,30,50,200,250,280,350</span>
                                          </td> */ ?>
                                    </tr>
                                    <tr>
                                        <td>
                                            <?php if ($this->lang->line('admin_dashboard_this_month') != '') echo stripslashes($this->lang->line('admin_dashboard_this_month')); else echo 'This Month'; ?>
                                        </td>
                                        <td>
                                            <?php if (isset($monthDrivers)) echo number_format($monthDrivers,0); ?>
                                        </td>
                                        <?php /* <td class="min_chart">
                                          <span class="line">20,30,50,200,250,280,350</span>
                                          </td> */ ?>
                                    </tr>
                                    <tr>
                                        <td>
                                            <?php if ($this->lang->line('admin_dashboard_this_year') != '') echo stripslashes($this->lang->line('admin_dashboard_this_year')); else echo 'This Year'; ?>
                                        </td>
                                        <td>
                                            <?php if (isset($yearDrivers)) echo number_format($yearDrivers,0); ?>
                                        </td>
                                        <?php /* <td class="min_chart">
                                          <span class="line">20,30,50,200,250,280,350</span>
                                          </td> */ ?>
                                    </tr>
                                </tbody>
                            </table>
							<div class="pie_chart">
                                <?php
                                $activeDriversPercent = 0.00;
                                if (isset($activeDrivers)) {
                                    if ($totalDrivers > 0) {
                                        $activeDriversPercent = ($activeDrivers * 100) / $totalDrivers;
                                    }
                                }
                                ?>
                                <span class="inner_circle"><?php echo round($activeDriversPercent, 1) . '%'; ?></span>
                                <span class="pie"><?php if (isset($activeDrivers)) echo $activeDrivers; ?>/<?php if (isset($totalDrivers)) echo $totalDrivers; ?></span>
                            </div>
                            <div class="chart_label">
                                <ul>
                                    <li><span class="new_visits"></span><?php if ($this->lang->line('admin_dashboard_active_drivers') != '') echo stripslashes($this->lang->line('admin_dashboard_active_drivers')); else echo 'Active Drivers'; ?>: <?php if (isset($activeDrivers)) echo number_format($activeDrivers,0); ?></li>
                                    <li><span class="unique_visits"></span><?php if ($this->lang->line('admin_dashboard_total_drivers') != '') echo stripslashes($this->lang->line('admin_dashboard_total_drivers')); else echo 'Total Drivers'; ?>: <?php if (isset($totalDrivers)) echo number_format($totalDrivers,0); ?></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <div class="grid_6" style="margin-top: 20px;">
            <div class="widget_wrap">
                <div class="widget_top">
                    <span class="h_icon graph"></span>
                    <h6><?php if ($this->lang->line('dashboard_student_head') != '') echo stripslashes($this->lang->line('dashboard_student_head')); else echo 'Students'; ?></h6>
                </div>


                <div class="widget_content">
                    <div class="stat_block">    
                        <div class="stat_chart">
                            <h4><?php if ($this->lang->line('operator_dashboard_students_count') != '') echo stripslashes($this->lang->line('operator_dashboard_students_count')); else echo 'Students Count'; ?> : <?php echo $totalStudents; ?></h4>
                            <table>
                                <tbody>
                                    <tr>
                                        <td>
                                            <?php if ($this->lang->line('admin_dashboard_today') != '') echo stripslashes($this->lang->line('admin_dashboard_today')); else echo 'Today'; ?>
                                        </td>
                                        <td>
                                            <?php if (isset($todayStudents)) echo $todayStudents; ?>
                                        </td>
                                        <?php /* <td class="min_chart">
                                          <span class="bar">20,30,50,200,250,280,350</span>
                                          </td> */ ?>
                                    </tr>
                                    <tr>
                                        <td>
                                            <?php if ($this->lang->line('admin_dashboard_this_month') != '') echo stripslashes($this->lang->line('admin_dashboard_this_month')); else echo 'This Month'; ?>
                                        </td>
                                        <td>
                                            <?php if (isset($monthStudents)) echo $monthStudents; ?>
                                        </td>
                                        <?php /* <td class="min_chart">
                                          <span class="line">20,30,50,200,250,280,350</span>
                                          </td> */ ?>
                                    </tr>
                                    <tr>
                                        <td>
                                            <?php if ($this->lang->line('admin_dashboard_this_year') != '') echo stripslashes($this->lang->line('admin_dashboard_this_year')); else echo 'This Year'; ?>
                                        </td>
                                        <td>
                                            <?php if (isset($yearStudents)) echo $yearStudents; ?>
                                        </td>
                                        <?php /* <td class="min_chart">
                                          <span class="line">20,30,50,200,250,280,350</span>
                                          </td> */ ?>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="pie_chart">
                            <?php
                                $activeStudentPercent = 0.00;
                                if (isset($activeStudents)) 
                                {  
                                    if ($totalStudents > 0) {
                                        $activeStudentPercent = ($activeStudents * 100) / $totalStudents;
                                    }
                                }
                            ?>
                                <span class="inner_circle"><?php echo round($activeStudentPercent, 1) . '%'; ?></span>
                                <span class="pie"><?php if (isset($activeStudents)) echo $activeStudents; ?>/<?php if (isset($totalStudents)) echo $totalStudents; ?></span>
                            </div>
                            <div class="chart_label">
                                <ul>
                                    <li><span class="new_visits"></span><?php if ($this->lang->line('operator_dashboard_active_students') != '') echo stripslashes($this->lang->line('operator_dashboard_active_students')); else echo 'Active Students'; ?>: <?php if (isset($activeStudents)) echo $activeStudents; ?></li>
                                    
                                    <li><span class="unique_visits"></span><?php if ($this->lang->line('operator_dashboard_total_students') != '') echo stripslashes($this->lang->line('operator_dashboard_total_students')); else echo 'Total Students'; ?>: <?php if (isset($totalStudents)) echo $totalStudents; ?></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>














        <span class="clear"></span>
        
    </div>








    <div class="grid_container">   


        <div class="grid_6" style="margin-top: 20px;">
            <div class="widget_wrap">
                <div class="widget_top">
                    <span class="h_icon graph"></span>
                    <h6><?php if ($this->lang->line('admin_review_chart_user') != '') echo stripslashes($this->lang->line('admin_review_chart_user')); else echo 'Users'; ?></h6>
                </div>
                <div class="widget_content">
                    <div class="stat_block">    
                        <div class="stat_chart">
                            <h4><?php if ($this->lang->line('operator_dashboard_user_count') != '') echo stripslashes($this->lang->line('operator_dashboard_user_count')); else echo 'Users Count'; ?> : <?php echo $total_Users; ?></h4>
                            <table>
                                <tbody>
                                    <tr>
                                        <td>
                                            <?php if ($this->lang->line('admin_dashboard_today') != '') echo stripslashes($this->lang->line('admin_dashboard_today')); else echo 'Today'; ?>
                                        </td>
                                        <td>
                                            <?php if (isset($todayUsers)) echo $todayUsers; ?>
                                        </td>
                                        <?php /* <td class="min_chart">
                                          <span class="bar">20,30,50,200,250,280,350</span>
                                          </td> */ ?>
                                    </tr>
                                    <tr>
                                        <td>
                                            <?php if ($this->lang->line('admin_dashboard_this_month') != '') echo stripslashes($this->lang->line('admin_dashboard_this_month')); else echo 'This Month'; ?>
                                        </td>
                                        <td>
                                            <?php if (isset($monthUsers)) echo $monthUsers; ?>
                                        </td>
                                        <?php /* <td class="min_chart">
                                          <span class="line">20,30,50,200,250,280,350</span>
                                          </td> */ ?>
                                    </tr>
                                    <tr>
                                        <td>
                                            <?php if ($this->lang->line('admin_dashboard_this_year') != '') echo stripslashes($this->lang->line('admin_dashboard_this_year')); else echo 'This Year'; ?>
                                        </td>
                                        <td>
                                            <?php if (isset($yearUsers)) echo $yearUsers; ?>
                                        </td>
                                        <?php /* <td class="min_chart">
                                          <span class="line">20,30,50,200,250,280,350</span>
                                          </td> */ ?>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="pie_chart">
                                <?php
                                $activeUserPercent = 0.00;
                                if (isset($active_Users)) {
                                    if ($total_Users > 0) {
                                        $activeUserPercent = ($active_Users * 100) / $total_Users;
                                    }
                                }
                                ?>
                                <span class="inner_circle"><?php echo round($activeUserPercent, 1) . '%'; ?></span>
                                <span class="pie"><?php if (isset($active_Users)) echo $active_Users; ?>/<?php if (isset($total_Users)) echo $total_Users; ?></span>
                            </div>
                            <div class="chart_label">
                                <ul>
                                    <li><span class="new_visits"></span><?php if ($this->lang->line('admin_dashboard_active_users') != '') echo stripslashes($this->lang->line('admin_dashboard_active_users')); else echo 'Active Users'; ?>: <?php if (isset($active_Users)) echo $active_Users; ?></li>
                                    
                                    <li><span class="unique_visits"></span><?php if ($this->lang->line('admin_dashboard_total_users') != '') echo stripslashes($this->lang->line('admin_dashboard_total_users')); else echo 'Total Users'; ?>: <?php if (isset($total_Users)) echo $total_Users; ?></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
















        <span class="clear"></span>
        
    </div>
    <span class="clear"></span>
</div>

</div>
<?php
$this->load->view('admin/templates/footer.php');
?>