<?php
$this->load->view('admin/templates/header.php');
?>
<style>

div#tab4 ul li:nth-child(5) label, div#tab4 ul li:nth-child(7) label, div#tab4 ul li:nth-child(9) label, div#tab4 ul li:nth-child(11) label {
    display: block;
}


</style>

<div id="content" class="base-app-top">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">
                <div class="widget_wrap tabby">
                    <div class="widget_top"> 
                        <span class="h_icon list"></span>
                        <h6><?php echo $heading; ?></h6>
                    </div>
                    <div class="widget_content">
                        <?php
                        $attributes = array('class' => 'form_container left_label', 'id' => 'app_settings_form','enctype' => 'multipart/form-data');
                        echo form_open('admin/adminlogin/admin_global_settings', $attributes)
                        ?>
                        <input type="hidden" name="form_mode" value="app"/>
                        <div id="tab4" class="base-appsec">
                            <ul class="left-contsec-app">

                                <li>
                                    <h3 class="head_social"><?php if ($this->lang->line('admin_settings_website_app_mode') != '') echo stripslashes($this->lang->line('admin_settings_website_app_mode')); else echo 'Website & App Mode'; ?></h3>
                                </li>
								
									<li>
										<div class="form_grid_12">
											<label class="field_title"><?php if ($this->lang->line('admin_common_status') != '') echo stripslashes($this->lang->line('admin_common_status')); else echo 'Status'; ?></label>
											<div class="form_input">
												<div class="prod_dev">
													<input type="checkbox"  name="site_mode"  id="prod_dev" class="prod_dev" <?php if (isset($admin_settings->row()->site_mode)){if ($admin_settings->row()->site_mode == 'production'){ echo 'checked="checked"'; }}  ?> />
												</div>
											</div>
										</div>
									</li>





                                <li>
                                    <h3 class="head_social"><?php if ($this->lang->line('admin_settings_app_settings') != '') echo stripslashes($this->lang->line('admin_settings_app_settings')); else echo 'App Settings'; ?></h3>
                                </li>

                                <li>
                                    <div class="form_grid_12">
                                        <label class="field_title"><?php if ($this->lang->line('admin_settings_driver_timeout') != '') echo stripslashes($this->lang->line('admin_settings_driver_timeout')); else echo 'Driver Timeout'; ?> <span class="req">*</span></label>
                                        <div class="form_input">
                                            <input name="respond_timeout" id="respond_timeout" type="text" value="<?php if (isset($admin_settings->row()->respond_timeout)) echo htmlentities($admin_settings->row()->respond_timeout); ?>"  class="large tipTop required number positiveNumber currencyT minfloatingNumber" title="<?php if ($this->lang->line('admin_setting_maximum_driver_respond_request') != '') echo stripslashes($this->lang->line('admin_setting_maximum_driver_respond_request')); else echo 'Maximum time for driver to respond for a request'; ?>"/>
                                            (<?php if ($this->lang->line('admin_settings_seconds') != '') echo stripslashes($this->lang->line('admin_settings_seconds')); else echo 'Seconds'; ?>)
                                        </div>
                                    </div>
                                </li>
								
								<li>
                                    <div class="form_grid_12">
                                        <label class="field_title"><?php if ($this->lang->line('admin_settings_user_timeout') != '') echo stripslashes($this->lang->line('admin_settings_user_timeout')); else echo 'User Timeout'; ?> <span class="req">*</span></label>
                                        <div class="form_input">
                                            <input type="text" class="large tipTop required number positiveNumber currencyT minfloatingNumber"  value="<?php if (isset($admin_settings->row()->user_timeout)) echo htmlentities($admin_settings->row()->user_timeout); ?>" id="user_timeout" name="user_timeout" original-title="<?php if ($this->lang->line('admin_setting_maximum_driver_respond_payment') != '') echo stripslashes($this->lang->line('admin_setting_maximum_driver_respond_payment')); else echo 'Maximum time for user to respond for a payment'; ?>">
                                            (<?php if ($this->lang->line('admin_settings_seconds') != '') echo stripslashes($this->lang->line('admin_settings_seconds')); else echo 'Seconds'; ?>)
                                        </div>
                                    </div>
                                </li>
								
                              

                                <li>
                                    <h3 class="head_social"><?php if ($this->lang->line('admin_settings_twilio_sms_api') != '') echo stripslashes($this->lang->line('admin_settings_twilio_sms_api')); else echo 'Twilio SMS API'; ?></h3>
                                </li>

                                <li class="net-account-type">
                                    <div class="form_grid_12">
                                        <label class="field_title"><?php if ($this->lang->line('admin_settings_account_type') != '') echo stripslashes($this->lang->line('admin_settings_account_type')); else echo 'Account Type'; ?></label>


                                        <div class="form_input">

                                            <?php
                                            if (isset($admin_settings->row()->twilio_account_type)) {
                                                $twilio_account_type = $admin_settings->row()->twilio_account_type;
                                            } else {
                                                $twilio_account_type = '';
                                            }
                                            ?>

                                            <p class="left-input-sel"><input name="twilio_account_type" id="twilio_account_live" type="radio" <?php if ($twilio_account_type == "prod") { echo "checked"; } ?> value="prod"  class="small tipTop" title="<?php if ($this->lang->line('admin_setting_select_twilio_account_type') != '') echo stripslashes($this->lang->line('admin_setting_select_twilio_account_type')); else echo 'Please select the twilio account type'; ?>"/>
                                            <label><?php if ($this->lang->line('admin_settings_live') != '') echo stripslashes($this->lang->line('admin_settings_live')); else echo 'LIVE'; ?></label></p>

                                            <p class="left-input-sel"><input name="twilio_account_type" <?php if ($twilio_account_type == "sandbox") { echo "checked"; } ?>  id="twilio_account_test" type="radio" value="sandbox"  class="small tipTop" title="<?php if ($this->lang->line('admin_setting_select_twilio_account_type') != '') echo stripslashes($this->lang->line('admin_setting_select_twilio_account_type')); else echo 'Please select the twilio account type'; ?>"/>
                                            <label><?php if ($this->lang->line('admin_settings_test') != '') echo stripslashes($this->lang->line('admin_settings_test')); else echo 'TEST'; ?></label></p>

                                        </div>


                                    </div>
                                </li>


                                <li>
                                    <div class="form_grid_12">
                                        <label class="field_title"><?php if ($this->lang->line('admin_settings_account_sid') != '') echo stripslashes($this->lang->line('admin_settings_account_sid')); else echo 'Account SID'; ?></label>
                                        <div class="form_input">
                                            <input name="twilio_account_sid" id="twilio_account_sid" type="text" value="<?php if (isset($admin_settings->row()->twilio_account_sid)) echo $admin_settings->row()->twilio_account_sid; ?>"  class="large tipTop " title="<?php if ($this->lang->line('admin_setting_enter_twilio_account_id') != '') echo stripslashes($this->lang->line('admin_setting_enter_twilio_account_id')); else echo 'Please enter the twilio account sid'; ?>"/>
                                        </div>
                                    </div>
                                </li>

                                <li>
                                    <div class="form_grid_12">
                                        <label class="field_title"><?php if ($this->lang->line('admin_settings_auth_token') != '') echo stripslashes($this->lang->line('admin_settings_auth_token')); else echo 'Auth Token'; ?></label>
                                        <div class="form_input">
                                            <input name="twilio_auth_token" id="twilio_auth_token" type="text" value="<?php if (isset($admin_settings->row()->twilio_auth_token)) echo $admin_settings->row()->twilio_auth_token; ?>"  class="large tipTop " title="<?php if ($this->lang->line('admin_setting_enter_twilio_auth_token') != '') echo stripslashes($this->lang->line('admin_setting_enter_twilio_auth_token')); else echo 'Please enter the twilio auth token'; ?>"/>
                                        </div>
                                    </div>
                                </li>

                                <li>
                                    <div class="form_grid_12">
                                        <label class="field_title"><?php if ($this->lang->line('admin_settings_number') != '') echo stripslashes($this->lang->line('admin_settings_number')); else echo 'Number'; ?></label>
                                        <div class="form_input">
                                            <input name="twilio_number" id="twilio_number" type="text" value="<?php if (isset($admin_settings->row()->twilio_number)) echo $admin_settings->row()->twilio_number; ?>"  class="large tipTop " title="<?php if ($this->lang->line('admin_setting_enter_twilio_number') != '') echo stripslashes($this->lang->line('admin_setting_enter_twilio_number')); else echo 'Please enter the twilio number'; ?>"/>
                                        </div>
                                    </div>
                                </li>
							
							
							
							


                                <li>
                                    <h3 class="head_social"><?php if ($this->lang->line('admin_settings_googlemap_places') != '') echo stripslashes($this->lang->line('admin_settings_googlemap_places')); else echo 'Google Map Places Search API Key'; ?></h3>
                                </li>
                                <li>
                                    <div class="form_grid_12">
                                        <label class="field_title"><?php if ($this->lang->line('admin_settings_website') != '') echo stripslashes($this->lang->line('admin_settings_website')); else echo 'Website'; ?></label>
                                        <div class="form_input">
                                           
                                            <input name="google_maps_api_key" id="google_maps_api_key" type="text" value="<?php if (isset($admin_settings->row()->google_maps_api_key)) echo $admin_settings->row()->google_maps_api_key; ?>"  class="large tipTop" title="<?php if ($this->lang->line('admin_setting_google_map_api_key') != '') echo stripslashes($this->lang->line('admin_setting_google_map_api_key')); else echo 'Please enter the Google Map Api key'; ?>"/>
                                        
                                     
                                        </div>
                                    </div>
                                </li>


                                <li>
                                    <h3 class="head_social"><?php if ($this->lang->line('admin_settings_pushnotification') != '') echo stripslashes($this->lang->line('admin_settings_pushnotification')); else echo 'Push Notification'; ?></h3>
                                </li>
                                
                                
                                
                                <li>
                                    <h3 class="head_social"><?php if ($this->lang->line('admin_settings_android') != '') echo stripslashes($this->lang->line('admin_settings_android')); else echo 'Android Key'; ?></h3>
                                </li>
                                <li>
                                    <div class="form_grid_12">
                                        <label class="field_title"><?php if ($this->lang->line('admin_settings_android_user') != '') echo stripslashes($this->lang->line('admin_settings_android_user')); else echo 'Android User'; ?></label>
                                        <div class="form_input">
                                           
                                           <input name="push_android_user" id="push_android_user" type="text" value="<?php if (isset($admin_settings->row()->push_android_user)) echo $admin_settings->row()->push_android_user; ?>"  class="large tipTop" />
                                        
                                     
                                        </div>
                                    </div>
                              </li>
                              <li>
                                    <div class="form_grid_12">
                                        <label class="field_title"><?php if ($this->lang->line('admin_settings_android_driver') != '') echo stripslashes($this->lang->line('admin_settings_android_driver')); else echo 'Android Driver'; ?></label>
                                        <div class="form_input">
                                           
                                           <input name="push_android_driver" id="push_android_driver" type="text" value="<?php if (isset($admin_settings->row()->push_android_driver)) echo $admin_settings->row()->push_android_driver; ?>"  class="large tipTop" />
                                        
                                     
                                        </div>
                                    </div>
                              </li>




                                
							</ul>
							
							<ul class="rite-contsec-app">
								
                                
                              <li>
                                    <h3 class="head_social"><?php if ($this->lang->line('admin_settings_ios_pem') != '') echo stripslashes($this->lang->line('admin_settings_ios_pem')); else echo 'Ios Pem File'; ?></h3>
                              </li>
                              <li>
                                <h3 class="head_social"><?php if ($this->lang->line('admin_settings_ios_pem_development') != '') echo stripslashes($this->lang->line('admin_settings_ios_pem_development')); else echo 'Development'; ?></h3>
                              </li>
                              <li>
                                    <div class="form_grid_12">
                                        <label class="field_title"><?php if ($this->lang->line('ios_user_pem') != '') echo stripslashes($this->lang->line('ios_user_pem')); else echo 'Ios User'; ?></label>
                                        <div class="form_input">
                                            <input name="ios_user_dev" id="ios_user_dev" type="file"  class="large tipTop" value="<?php echo $admin_settings->row()->ios_user_dev; ?>"/>
                                        <?php if($admin_settings->row()->ios_user_dev!='') {?>
                                         <a href="certificates/<?php echo $admin_settings->row()->ios_user_dev; ?>" target="_blank" ><?php echo $admin_settings->row()->ios_user_dev; ?></a>
                                        <?php }?>
                                        </div>
                                        
                                      
                                       
                                    </div>
                                </li>
                                <li>
                                    <div class="form_grid_12">
                                        <label class="field_title"><?php if ($this->lang->line('ios_driver_pem') != '') echo stripslashes($this->lang->line('ios_driver_pem')); else echo 'Ios Driver'; ?></label>
                                        <div class="form_input">
                                            <input name="ios_driver_dev" id="ios_driver_dev" type="file"  class="large tipTop" value="<?php echo $admin_settings->row()->ios_driver_dev; ?>"/>
                                              <?php if($admin_settings->row()->ios_driver_dev!='') {?>
                                              <a href="certificates/<?php echo $admin_settings->row()->ios_driver_dev; ?>" target="_blank" ><?php echo $admin_settings->row()->ios_driver_dev; ?></a>
                                           <?php }?>
                                        </div>
                                      
                                    </div>
                               </li>
                               <li>
                                <h3 class="head_social"><?php if ($this->lang->line('admin_settings_ios_pem_production') != '') echo stripslashes($this->lang->line('admin_settings_ios_pem_production')); else echo 'Production'; ?></h3>
                              </li>
                              <li>
                                    <div class="form_grid_12">
                                        <label class="field_title"><?php if ($this->lang->line('ios_user_pem') != '') echo stripslashes($this->lang->line('ios_user_pem')); else echo 'Ios User'; ?></label>
                                        <div class="form_input">
                                            <input name="ios_user_prod" id="ios_user_prod" type="file"  class="large tipTop" value="<?php echo $admin_settings->row()->ios_user_prod; ?>"/>
                                              <?php if($admin_settings->row()->ios_user_prod!='') {?>
                                               <a href="certificates/<?php echo $admin_settings->row()->ios_user_prod; ?>" target="_blank" ><?php echo $admin_settings->row()->ios_user_prod; ?></a>
                                             <?php }?>
                                        </div>
                                       
                                    </div>
                                </li>
                                <li>
                                    <div class="form_grid_12">
                                        <label class="field_title"><?php if ($this->lang->line('ios_driver_pem') != '') echo stripslashes($this->lang->line('ios_driver_pem')); else echo 'Ios Driver'; ?></label>
                                        <div class="form_input">
                                            <input name="ios_driver_prod" id="ios_driver_prod" type="file"  class="large tipTop"value="<?php echo $admin_settings->row()->ios_driver_prod; ?>" />
                                              <?php if($admin_settings->row()->ios_driver_prod!='') {?>
                                                <a href="certificates/<?php echo $admin_settings->row()->ios_driver_prod; ?>" target="_blank" ><?php echo $admin_settings->row()->ios_driver_prod; ?></a>
                                             <?php }?>
                                        </div>
                                      
                                    </div>
                               </li>
                                

																<li>
                                    <h3 class="head_social"><?php if ($this->lang->line('admin_appsettings_link') != '') echo stripslashes($this->lang->line('admin_appsettings_link')); else echo 'Link'; ?></h3>
                                </li>
																<li>
																	<h3 class="head_social"><?php if ($this->lang->line('admin_appsettings_playstore_link') != '') echo stripslashes($this->lang->line('admin_appsettings_playstore_link')); else echo 'Playstore link'; ?></h3>
																</li>

                                <li>
                                    <div class="form_grid_12">
                                        <label class="field_title"><?php if ($this->lang->line('admin_cancelled_by_user') != '') echo stripslashes($this->lang->line('admin_cancelled_by_user')); else echo 'User'; ?></label>
                                        <div class="form_input">
                                            <input name="user_playstore_link" id="user_playstore_link" type="text" value="<?php if (isset($admin_settings->row()->user_playstore_link)) echo htmlentities($admin_settings->row()->user_playstore_link); ?>"  class="large tipTop url"/>
                                        </div>
                                    </div>
                                </li>
																
																<li>
                                    <div class="form_grid_12">
                                        <label class="field_title"><?php if ($this->lang->line('admin_cancelled_by_driver') != '') echo stripslashes($this->lang->line('admin_cancelled_by_driver')); else echo 'Driver'; ?></label>
                                        <div class="form_input">
                                            <input name="driver_playstore_link" id="driver_playstore_link" type="text" value="<?php if (isset($admin_settings->row()->driver_playstore_link)) echo htmlentities($admin_settings->row()->driver_playstore_link); ?>"  class="large tipTop url"/>
                                        </div>
                                    </div>
                                </li>
																
																<li>
																	<h3 class="head_social"><?php if ($this->lang->line('admin_appsettings_appstore_link') != '') echo stripslashes($this->lang->line('admin_appsettings_appstore_link')); else echo 'Appstore Link'; ?></h3>
																</li>

                                <li>
                                    <div class="form_grid_12">
                                        <label class="field_title"><?php if ($this->lang->line('admin_cancelled_by_user') != '') echo stripslashes($this->lang->line('admin_cancelled_by_user')); else echo 'User'; ?></label>
                                        <div class="form_input">
                                            <input name="user_appstore_link" id="user_appstore_link" type="text" value="<?php if (isset($admin_settings->row()->user_appstore_link)) echo htmlentities($admin_settings->row()->user_appstore_link); ?>"  class="large tipTop url"/>
                                        </div>
                                    </div>
                                </li>
																
																<li>
                                    <div class="form_grid_12">
                                        <label class="field_title"><?php if ($this->lang->line('admin_cancelled_by_driver') != '') echo stripslashes($this->lang->line('admin_cancelled_by_driver')); else echo 'Driver'; ?></label>
                                        <div class="form_input">
                                            <input name="driver_appstore_link" id="driver_appstore_link" type="text" value="<?php if (isset($admin_settings->row()->driver_appstore_link)) echo htmlentities($admin_settings->row()->driver_appstore_link); ?>"  class="large tipTop url"/>
                                        </div>
                                    </div>
                                </li>
																
                            </ul>
                            <ul class="last-sec-button">
                                <li>
                                    <div class="form_grid_12">
                                        <div class="form_input">
                                            <button type="submit" class="btn_small btn_blue" ><span><?php if ($this->lang->line('admin_settings_submit') != '') echo stripslashes($this->lang->line('admin_settings_submit')); else echo 'Submit'; ?></span></button>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <span class="clear"></span> 
</div>
</div>


<style>

ul.left-contsec-app {
    min-height: 1170px !important;
}

ul.rite-contsec-app {
    min-height: 1170px !important;
}

</style>







<script type="text/javascript">
    $(document).ready(function () {
        if ($("#publish1").attr("checked") == "checked") {
            $("#d_mode1").attr("checked", false);
            $("#d_mode2").attr("checked", false);
            $("#d_mode3").attr("checked", true);
            $("#d_mode4").attr("checked", false);
            $("li#dev_mode").hide();
        }
        $("#publish1").click(function () {
            $("#d_mode1").attr("checked", false);
            $("#d_mode2").attr("checked", false);
            $("#d_mode3").attr("checked", false);
            $("#d_mode4").attr("checked", false);
            $("li#dev_mode").hide();
        });
    });
</script>

<?php
$this->load->view('admin/templates/footer.php');
?>