<?php
$this->load->view('admin/templates/header.php');
if ($form_mode) $operator_details = $operator_details->row();

?>

<style>
     #map {
        height: 100%;
        min-height: 450px;
      }
</style>


<div id="content" class="add-operator-sec">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">
            <div class="widget_top">
                    <span class="h_icon list"></span>
                    <h6><?php echo $heading; ?></h6>
                    <div id="widget_tab">
                    </div>
            </div>
            <div class="widget_content">
                    <?php
                    $attributes = array('class' => 'form_container left_label', 'id' => 'addEditoperators_form','method'=>'POST','autocomplete'=>'off','enctype' => 'multipart/form-data');
                    echo form_open('admin/operators/insertEditOperators', $attributes)
                    ?>
                    <div>
        <ul class="operator-sec-bar">
             



            <li>
                <div class="form_grid_12">
                        <label class="field_title"><?php if ($this->lang->line('admin_drivers_country') != '') echo stripslashes($this->lang->line('admin_drivers_country')); else echo 'Country'; ?><span class="req">*</span></label>
                        <div class="form_input">
                            <select onchange="update_mobile_code();address_binder()" name="country" id="country"  class="required" style="height: 31px; width: 51%;">
                             <option value=""><?php if ($this->lang->line('company_please_choose_country_tooltip') != '') echo stripslashes($this->lang->line('company_please_choose_country_tooltip')); else echo 'Please choose Country'; ?></option>
                            <?php foreach ($countryList as $country) { ?>
                             <option value="<?php echo $country->name; ?>" data-dialCode="<?php echo $country->dial_code; ?>" <?php if ($form_mode){ if(is_object($operator_details->address)) $operator_details->{"address"}=(array)$operator_details->address; if ($operator_details->address['country'] == $country->name) echo 'selected="selected"';} ?>><?php echo $country->name; ?></option>
                            <?php } ?>
                            </select>
                    </div>
                </div>
            </li>




            <li>
                <div class="form_grid_12">
                    <label class="field_title"><?php if ($this->lang->line('admin_drivers_city') != '') echo stripslashes($this->lang->line('admin_drivers_city')); else echo 'City'; ?><span class="req">*</span></label>
                    <div class="form_input">
                        <input onchange="address_binder()" name="city" id="city" type="text"  class="required large tipTop" title="<?php if ($this->lang->line('location_enter_the_city') != '') echo stripslashes($this->lang->line('location_enter_the_city')); else echo 'Please enter the city'; ?>" value="<?php if($form_mode) if (isset($operator_details->address['city'])) echo $operator_details->address['city']; ?>"/>
                    </div>
                </div>
            </li>


           


            <li class="add-mob-operator">
                <div class="form_grid_12">
                    <label class="field_title"><?php if ($this->lang->line('admin_drivers_mobile_number') != '') echo stripslashes($this->lang->line('admin_drivers_mobile_number')); else echo 'Mobile Number'; ?><span class="req">*</span></label>
                    <div class="form_input">																								
                            
                            <?php 
                            $dail_code = $d_country_code;
                            if($form_mode) if(isset($operator_details->dail_code)) $dail_code = $operator_details->dail_code; 
                            ?>
                            
                            <select style="width: 98px !important;" name="dail_code" id="country_codeM"  class="required small tipTop mCC chzn-select" style="" title="<?php if ($this->lang->line('select_mobile_country_code') != '') echo stripslashes($this->lang->line('select_mobile_country_code')); else echo 'Please select mobile country code'; ?>">
                                <?php foreach ($countryList as $country) { ?>
                                    <option value="<?php echo $country->dial_code; ?>" <?php if($country->dial_code==$dail_code){ echo "selected"; } ?>><?php echo $country->cca3." "; ?>    (<?php echo $country->dial_code; ?>)</option>
                                <?php } ?>
                            </select>


                            
                            
                            <input name="mobile_number" placeholder="<?php if ($this->lang->line('admin_drivers_mobile_number') != '') echo stripslashes($this->lang->line('admin_drivers_mobile_number')); else echo 'Mobile Number'; ?>.." id="mobile_number" type="text"  class="required large tipTop phoneNumber" title="<?php if ($this->lang->line('driver_enter_mobile_number') != '') echo stripslashes($this->lang->line('driver_enter_mobile_number')); else echo 'Please enter the mobile number'; ?>" maxlength="20" style="width:78% !important;    height: 37px;"  value="<?php if($form_mode) if (isset($operator_details->mobile_number)) echo $operator_details->mobile_number; ?>"/>
                    </div>
                </div>
            </li>	

            <li>
                <div class="form_grid_12">
                    <label class="field_title"><?php if ($this->lang->line('admin_drivers_address') != '') echo stripslashes($this->lang->line('admin_drivers_address')); else echo 'Address'; ?><span class="req">*</span></label>
                    <div class="form_input">
                            <textarea onchange="address_binder()" name="address" id="address"  class="required large tipTop" title="<?php if ($this->lang->line('admin_enter_operator_address') != '') echo stripslashes($this->lang->line('admin_enter_operator_address')); else echo 'Please enter the operator address'; ?>" style="width: 372px;"><?php if($form_mode) if (isset($operator_details->address['address'])) echo $operator_details->address['address']; ?></textarea>
                    </div>
                </div>
            </li>






             <li>
                <div class="form_grid_12">
                    <label class="field_title"><?php if ($this->lang->line('admin_common_status') != '') echo stripslashes($this->lang->line('admin_common_status')); else echo 'Status'; ?></label>
                    <div class="form_input">
                        <div class="active_inactive">
                            <input type="checkbox"  name="status"  id="active_inactive_active" class="active_inactive" <?php if($form_mode){ if ($operator_details->status == 'Active'){echo 'checked="checked"';} } ?> />
                        </div>
                    </div>
                </div>
            </li>

            
        </ul>
        
        <ul class="operator-log-rite">
            

            <li>
                <div class="form_grid_12">
                    <label class="field_title"><?php if ($this->lang->line('admin_drivers_state_province_region') != '') echo stripslashes($this->lang->line('admin_drivers_state_province_region')); else echo 'State / Province / Region'; ?><span class="req">*</span></label>
                    <div class="form_input">
                        <input onchange="address_binder()" name="state" id="state" type="text"  class="required large tipTop" title="<?php if ($this->lang->line('admin_operator_driver_state') != '') echo stripslashes($this->lang->line('admin_operator_driver_state')); else echo 'Please enter the state'; ?>" value="<?php if($form_mode) if (isset($operator_details->address['state'])) echo $operator_details->address['state']; ?>"/>
                    </div>
                </div>
            </li>


       
            <li>
                <div class="form_grid_12">
                    <label class="field_title"><?php if ($this->lang->line('admin_drivers_postal_code') != '') echo stripslashes($this->lang->line('admin_drivers_postal_code')); else echo 'Postal Code'; ?><span class="req">*</span></label>
                    <div class="form_input">
                        <input onchange="address_binder()" name="postal_code" id="postal_code" type="text"  maxlength="10" class="required large tipTop" title="<?php if ($this->lang->line('location_enter_the_postalcode') != '') echo stripslashes($this->lang->line('location_enter_the_postalcode')); else echo 'Please enter the postal code'; ?>" value="<?php if($form_mode) if (isset($operator_details->address['postal_code'])) echo $operator_details->address['postal_code']; ?>"/>
                    </div>
                </div>
            </li>




             <li>
                    <div class="form_grid_12">
                            <label class="field_title"><?php if ($this->lang->line('admin_operator_name') != '') echo stripslashes($this->lang->line('admin_operator_name')); else echo 'Operator Name'; ?><span class="req">*</span></label>
                            <div class="form_input">
                                    <input name="operator_name" id="operator_name" type="text"  class="required large tipTop"  title="<?php if ($this->lang->line('admin_enter_operator_name') != '') echo stripslashes($this->lang->line('admin_enter_operator_name')); else echo 'Please enter operator name'; ?>" value="<?php if($form_mode) echo $operator_details->operator_name;  ?>"/>
                            </div>
                    </div>
            </li>






            <li>
                    <div class="form_grid_12">
                            <label class="field_title"><?php if ($this->lang->line('admin_operator_email') != '') echo stripslashes($this->lang->line('admin_operator_email')); else echo 'Operator Email'; ?><span class="req">*</span></label>
                            <div class="form_input">
                                    <input name="email" id="email" type="text"  class="required large tipTop email" title="<?php if ($this->lang->line('admin_enter_operator_email') != '') echo stripslashes($this->lang->line('admin_enter_operator_email')); else echo 'Please enter operator email'; ?>" value="<?php if($form_mode) if(isset($operator_details->email)) { echo $operator_details->email; } ?>"/>
                            </div>
                    </div>
            </li>








             <li>
                <div class="form_grid_12">
                    <label class="field_title"><?php if ($this->lang->line('switch_session') != '') echo stripslashes($this->lang->line('switch_session')); else echo 'Switch session'; ?></label>
                    <div class="form_input">
                        <div class="active_inactive">
                            <input type="checkbox"  name="session_switch"  id="active_inactive_active" class="active_inactive" <?php if($form_mode){ if ($operator_details->session_switch == 'on'){echo 'checked="checked"';} } ?> />
                        </div>
                    </div>
                </div>
            </li>



              

                                                                                      
                                         
         




           
        </ul>
                                                            
        <ul class="last-operator">
            <li>
                <div class="form_grid_12">
                    <div class="form_input">

                        <span onclick="cancel_submit();" type="cancel" class="btn_small btn_blue"><?php if ($this->lang->line('admin_common_cancel') != '') echo stripslashes($this->lang->line('admin_common_cancel')); else echo 'Cancel'; ?></span>

                        <input type="hidden" name="operator_id" id="operator_id" value="<?php if($form_mode){ echo (string)$operator_details->_id; } ?>"  />
                        <button type="submit" class="btn_small btn_cancel" ><span><?php if ($this->lang->line('admin_common_submit') != '') echo stripslashes($this->lang->line('admin_common_submit')); else echo 'Submit'; ?></span></button>
                    </div>
                </div>





            </li>
        </ul>
                                        


                


                                                          
        <ul class="last-operator">
                        <li>
                         <div style="width: 25%;">
                            <div class="form_grid_12">
                            <label class="field_title">Latitude</label>
                            <input type="number" name="input_lat" class="required"  value="<?php if($form_mode) if(isset($operator_details->operator_location)) {  echo $operator_details->operator_location->lat; } ?>"  id="input_lat"/>
                            </div>
                            <br>
                            <div class="form_grid_12">
                            <label class="field_title">Longitude</label>
                            <input type="number" name="input_long" class="required"  value="<?php if($form_mode) if(isset($operator_details->operator_location)) {  echo $operator_details->operator_location->lat; }?>" id="input_long"/>
                            </div>
                        </div>
                        <div id="map"> </div>
                       

                        </li>
        </ul>







    </div>
                                             
    </form>



  








 </div>
 </div>
</div>
 </div>
<span class="clear"></span>
</div>
</div>
<script>




function cancel_submit()
{
    window.location.href="admin/operators/display_operators_list";
}









        $('#addEditoperators_form').validate();

        var country=<?php echo json_encode($countryList) ?>;    




        function update_mobile_code()
        {

            var country_name=document.getElementById("country").value;
            var code_selected="";

            for(var i=0;i< country.length;i++  )
            {

                if(country[i]["name"]== country_name)
                   {
                    code_selected=country[i]["dial_code"];
                   } 


            }

          
            

            
             $('#country_codeM option[value="'+code_selected+'"]').attr("selected", "selected");   
         







        }







        /////////////////map

     var checker=0;
     var geocoder;
      var map;
      var address = "Singapore"; 
     <?php if(isset($operator_details->operator_location)) {  

           

            echo "address ='".$operator_details->operator_location->lat.",".$operator_details->operator_location->lng."'";




           } ?>

       function address_binder()
       {

      

               
             if(checker != 0)
             {  

               var country=document.getElementById('country').value;
                 var city=document.getElementById('city').value;
                   var state=document.getElementById('state').value;
                     var postal_code=document.getElementById('postal_code').value;
                       var address_input=document.getElementById('address').value;



                       address=address_input+' '+city+' '+state+' '+postal_code+' '+country;

                       initMap();

              }
              else
              {
                initMap();
                 checker=1;
              }         

       } 



      










        
      function codeAddress(geocoder, map) {

        if(address == "")
        {
            address="Singapore";
        }

        console.log(address);

       
        geocoder.geocode({'address': address}, function(results, status) {
          if (status === 'OK') {
            map.setCenter(results[0].geometry.location);
             map.setZoom(16);

            document.getElementById("input_lat").value=results[0].geometry.location.lat();
             document.getElementById("input_long").value=results[0].geometry.location.lng();

            var marker = new google.maps.Marker({
              map: map,
              draggable:true,
              position: results[0].geometry.location
            });
          } else {
            if(checker != 0)
             {   
                    alert('Geocode was not successful for the following reason: ' + status);
                    checker=1;
              }  
          }


                google.maps.event.addListener(marker, 'dragend', function() 
                {
                    var lat_long=marker.getPosition();

                    console.log(lat_long.lat(),lat_long.lng());

                     document.getElementById("input_lat").value=lat_long.lat();
                    document.getElementById("input_long").value=lat_long.lng();



                });



        });


      }
     


      function initMap() {
          map = new google.maps.Map(document.getElementById('map'), {
          zoom: 12,
          center: {lat: -34.397, lng: 150.644}
        });
        geocoder = new google.maps.Geocoder();

        $('.active_inactive :checkbox').iphoneStyle({checkedLabel: 'Active', uncheckedLabel: 'Inactive'});


        codeAddress(geocoder, map);     
       
      }





</script>

 <script async defer
    src="https://maps.googleapis.com/maps/api/js?sensor=false&<?php echo substr($this->data['google_maps_api_key'], 1);?>&callback=initMap">
 </script>
<?php 
$this->load->view('admin/templates/footer.php');
?>