<?php
$currentUrl = $this->uri->segment(2, 0);
$currentPage = $this->uri->segment(3, 0);
if ($currentUrl == '') {
    $currentUrl = 'dashboard';
}
if ($currentPage == '') {
    $currentPage = 'dashboard';
}

if (isset($privileges)) {
    if(is_object($privileges)){
        $privileges=(array)$privileges;
    }
    extract($privileges);
}
$current_url = $_SERVER['REQUEST_URI'];
?>

<div class="" style="position:relative">

    <div id="left_bar" >

        <div class="logo">
            <img src="images/logo/<?php echo $logo; ?>" alt="<?php echo $siteTitle; ?>" width="160px" title="<?php echo $siteTitle; ?>">
        </div>

        <div id="sidebar">


            <div id="secondary_nav">



                <ul id="sidenav" class="accordion_mnu collapsible">
                    <li>
                        <a href="<?php echo base_url(); ?>admin/dashboard/admin_dashboard" <?php
if ($currentUrl == 'dashboard') {
    echo 'class="active"';
}
?>>
                            <span class="nav_icon computer_imac"></span> 
                            <?php
                            if ($this->lang->line('admin_menu_dashboard') != '')
                                echo stripslashes($this->lang->line('admin_menu_dashboard'));
                            else
                                echo 'Dashboard';
                            ?>
                        </a>
                    </li>
                    <!--<li>
                        <h6 style="margin: 10px 0;padding-left:10px; font-size:13px; font-weight:bold;color:#333; text-transform:uppercase; "><?php
                            if ($this->lang->line('admin_menu_managements') != '')
                                echo stripslashes($this->lang->line('admin_menu_managements'));
                            else
                                echo 'Managements';
                            ?></h6>
                    </li>-->
                    <?php
                    if ($this->session->userdata(APP_NAME . '_session_admin_mode') == 'admin' || $allPrev == '1') {
                        ?>
                        <li>
                            <a href="<?php echo $current_url; ?>" <?php
                    if ($currentUrl == 'adminlogin') {
                        echo 'class="active"';
                    }
                        ?>>
                                <span class="nav_icon admin_user"></span> <?php
                        if ($this->lang->line('admin_menu_admin') != '')
                            echo stripslashes($this->lang->line('admin_menu_admin'));
                        else
                            echo 'Admin';
                        ?><span class="up_down_arrow">&nbsp;</span>
                            </a>
                            <ul class="acitem" <?php
                            if ($currentUrl == 'adminlogin') {
                                echo 'style="display: block;"';
                            } else {
                                echo 'style="display: none;"';
                            }
                        ?>>			
                                <li>
                                    <a href="admin/adminlogin/change_admin_password_form" <?php
                        if ($currentPage == 'change_admin_password_form') {
                            echo 'class="active"';
                        }
                        ?>>
                                        <span class="list-icon">&nbsp;</span><?php
                                if ($this->lang->line('admin_menu_change_password') != '')
                                    echo stripslashes($this->lang->line('admin_menu_change_password'));
                                else
                                    echo 'Change Password';
                        ?>
                                    </a>
                                </li>
                                <li>
                                    <a href="admin/adminlogin/admin_global_settings_form" <?php
                                    if ($currentPage == 'admin_global_settings_form') {
                                        echo 'class="active"';
                                    }
                        ?>>
                                        <span class="list-icon">&nbsp;</span><?php
                                if ($this->lang->line('admin_menu_settings') != '')
                                    echo stripslashes($this->lang->line('admin_menu_settings'));
                                else
                                    echo 'Settings';
                        ?>
                                    </a>
                                </li>
                                <li>
                                    <a href="admin/adminlogin/admin_smtp_settings" <?php
                                    if ($currentPage == 'admin_smtp_settings') {
                                        echo 'class="active"';
                                    }
                        ?>>
                                        <span class="list-icon">&nbsp;</span><?php
                                if ($this->lang->line('admin_menu_smtp_settings') != '')
                                    echo stripslashes($this->lang->line('admin_menu_smtp_settings'));
                                else
                                    echo 'SMTP Settings';
                        ?>
                                    </a>
                                </li>
                                <li>
                                    <a href="admin/adminlogin/admin_site_settings" <?php
                                    if ($currentPage == 'admin_site_settings') {
                                        echo 'class="active"';
                                    }
                        ?>>
                                        <span class="list-icon">&nbsp;</span><?php
                                if ($this->lang->line('admin_menu_menu_settings') != '')
                                    echo stripslashes($this->lang->line('admin_menu_menu_settings'));
                                else
                                    echo 'Menu Settings';
                        ?>
                                    </a>
                                </li>

                                <li>
                                    <a href="admin/adminlogin/admin_app_settings" <?php
                                    if ($currentPage == 'admin_app_settings') {
                                        echo 'class="active"';
                                    }
                        ?>>
                                        <span class="list-icon">&nbsp;</span><?php
                                if ($this->lang->line('admin_menu_app_settings') != '')
                                    echo stripslashes($this->lang->line('admin_menu_app_settings'));
                                else
                                    echo 'App Settings';
                        ?>
                                    </a>
                                </li>

                                <?php if ($this->config->item('currency_name') == '' || $this->config->item('currency_code') == '' || $this->config->item('currency_symbol') == '') { ?>
                                    <li>
                                        <a href="admin/adminlogin/admin_currency_settings" <?php
                                    if ($currentPage == 'admin_currency_settings') {
                                        echo 'class="active"';
                                    }
                                    ?>>
                                            <span class="list-icon">&nbsp;</span><?php
                                if ($this->lang->line('currency_setting') != '')
                                    echo stripslashes($this->lang->line('admin_menu_admin'));
                                else
                                    echo 'Currency Settings';
                                    ?>
                                        </a>
                                    </li>
                                <?php } ?>
                                <?php if ($this->config->item('countryId') == '' || $this->config->item('countryName') == '') { ?>
                                    <li>
                                        <a href="admin/adminlogin/admin_country_settings" <?php
                            if ($currentPage == 'admin_country_settings') {
                                echo 'class="active"';
                            }
                                    ?>>
                                            <span class="list-icon">&nbsp;</span><?php
                                if ($this->lang->line('currency_setting') != '')
                                    echo stripslashes($this->lang->line('currency_setting'));
                                else
                                    echo 'Country Settings';
                                    ?>
                                        </a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </li>

                        <li><a href="<?php echo $current_url; ?>" <?php
                                if ($currentUrl == 'subadmin') {
                                    echo 'class="active"';
                                }
                                ?>><span class="nav_icon user"></span><?php
                               if ($this->lang->line('admin_menu_subadmin') != '')
                                   echo stripslashes($this->lang->line('admin_menu_subadmin'));
                               else
                                   echo 'Subadmin';
                               ?> 
                                <span class="up_down_arrow">&nbsp;</span></a>
                            <ul class="acitem" <?php
                           if ($currentUrl == 'subadmin') {
                               echo 'style="display: block;"';
                           } else {
                               echo 'style="display: none;"';
                           }
                               ?>>
                                <li>
                                    <a href="admin/subadmin/display_sub_admin" <?php
                        if ($currentPage == 'display_sub_admin') {
                            echo 'class="active"';
                        }
                               ?>><span class="list-icon">&nbsp;</span><?php
                                       if ($this->lang->line('admin_menu_subadmin_list') != '')
                                           echo stripslashes($this->lang->line('admin_menu_subadmin_list'));
                                       else
                                           echo 'Subadmin List';
                                       ?>
                                    </a>
                                </li>

                                <li>
                                    <a href="admin/subadmin/add_sub_admin_form" <?php
                                   if ($currentPage == 'add_sub_admin_form') {
                                       echo 'class="active"';
                                   }
                                       ?>>
                                        <span class="list-icon">&nbsp;</span><?php
                                if ($this->lang->line('admin_menu_add_new_subadmin') != '')
                                    echo stripslashes($this->lang->line('admin_menu_add_new_subadmin'));
                                else
                                    echo 'Add New Subadmin';
                                       ?>
                                    </a>
                                </li>
                            </ul>
                        </li>

                    <?php } else { ?>				

                        <li>
                            <a href="<?php echo $current_url; ?>" <?php
                        if ($currentUrl == 'adminlogin') {
                            echo 'class="active"';
                        }
                        ?>>
                                <span class="nav_icon admin_user"></span><?php
                        if ($this->lang->line('admin_menu_admin') != '')
                            echo stripslashes($this->lang->line('admin_menu_admin'));
                        else
                            echo 'Admin';
                        ?><span class="up_down_arrow">&nbsp;</span>
                            </a>
                            <ul class="acitem" <?php
                            if ($currentUrl == 'adminlogin') {
                                echo 'style="display: block;"';
                            } else {
                                echo 'style="display: none;"';
                            }
                        ?>>
                                <li>
                                    <a href="admin/adminlogin/change_admin_password_form" <?php
                        if ($currentPage == 'change_admin_password_form') {
                            echo 'class="active"';
                        }
                        ?>>
                                        <span class="list-icon">&nbsp;</span><?php
                                if ($this->lang->line('admin_menu_change_password') != '')
                                    echo stripslashes($this->lang->line('admin_menu_change_password'));
                                else
                                    echo 'Change Password';
                        ?>
                                    </a>
                                </li>
                                <?php /*
                                  <li>
                                  <a href="admin/adminlogin/admin_global_settings_form" <?php if($currentPage=='admin_global_settings_form'){ echo 'class="active"';} ?>>
                                  <span class="list-icon">&nbsp;</span>Settings
                                  </a>
                                  </li>
                                  <li>
                                  <a href="admin/adminlogin/admin_smtp_settings" <?php if($currentPage=='admin_smtp_settings'){ echo 'class="active"';} ?>>
                                  <span class="list-icon">&nbsp;</span>SMTP Settings
                                  </a>
                                  </li>

                                  <?php if($this->config->item('currency_name') == '' || $this->config->item('currency_code') == '' || $this->config->item('currency_symbol') == ''){ ?>
                                  <li>
                                  <a href="admin/adminlogin/admin_currency_settings" <?php if($currentPage=='admin_smtp_settings'){ echo 'class="active"';} ?>>
                                  <span class="list-icon">&nbsp;</span>Currency Settings
                                  </a>
                                  </li>
                                  <?php } ?>
                                  <?php if($this->config->item('countryId') == '' || $this->config->item('countryName') == ''){ ?>
                                  <li>
                                  <a href="admin/adminlogin/admin_country_settings" <?php if($currentPage=='admin_country_settings'){ echo 'class="active"';} ?>>
                                  <span class="list-icon">&nbsp;</span>Country Settings
                                  </a>
                                  </li>
                                  <?php } ?>
                                 */ ?>
                            </ul>
                        </li>

                        <li style="display:none;">  <a href="<?php echo $current_url; ?>" <?php
                            if ($currentUrl == 'subadmin') {
                                echo 'class="active"';
                            }
                                ?>><span class="nav_icon user"></span><?php
                                                       if ($this->lang->line('admin_menu_subadmin') != '')
                                                           echo stripslashes($this->lang->line('admin_menu_subadmin'));
                                                       else
                                                           echo 'Subadmin';
                                                       ?> 
                                <span class="up_down_arrow">&nbsp;</span></a>
                            <ul class="acitem" <?php
                                                   if ($currentUrl == 'subadmin') {
                                                       echo 'style="display: block;"';
                                                   } else {
                                                       echo 'style="display: none;"';
                                                   }
                                                       ?>>
                                <li><a href="admin/subadmin/display_sub_admin" <?php
                        if ($currentPage == 'display_sub_admin') {
                            echo 'class="active"';
                        }
                                                       ?>><span class="list-icon">&nbsp;</span><?php
                                       if ($this->lang->line('admin_menu_subadmin_list') != '')
                                           echo stripslashes($this->lang->line('admin_menu_subadmin_list'));
                                       else
                                           echo 'Subadmin List';
                                       ?></a></li>
                                <li><a href="admin/subadmin/add_sub_admin_form" <?php
                                   if ($currentPage == 'add_sub_admin_form') {
                                       echo 'class="active"';
                                   }
                                       ?>><span class="list-icon">&nbsp;</span><?php
                                       if ($this->lang->line('admin_menu_add_new_subadmin') != '')
                                           echo stripslashes($this->lang->line('admin_menu_add_new_subadmin'));
                                       else
                                           echo 'Add New Subadmin';
                                       ?></a></li>
                            </ul>
                        </li>

                    <?php } ?>	

                    <?php if ((isset($institution) && is_array($institution)) && in_array('0', $institution) || $allPrev == '1') { ?>
                        <li>
                            <a href="<?php echo $current_url; ?>" <?php
                        if ($currentUrl == 'operators') {
                            echo 'class="active"';
                        }
                        ?>>
                                <span class="nav_icon headphones"></span><?php
                        if ($this->lang->line('admin_menu_operators') != '')
                            echo stripslashes($this->lang->line('admin_menu_operators'));
                        else
                            echo 'Institution';
                        ?> <span class="up_down_arrow">&nbsp;</span>
                            </a>
                            <ul class="acitem" <?php
                            if ($currentUrl == 'operators') {
                                echo 'style="display: block;"';
                            } else {
                                echo 'style="display: none;"';
                            }
                        ?>>
                                <li>
                                    <a href="admin/operators/display_operators_list" <?php
                        if ($currentPage == 'display_operators_list') {
                            echo 'class="active"';
                        }
                        ?>>
                                        <span class="list-icon">&nbsp;</span><?php
                                if ($this->lang->line('admin_menu_operators_list') != '')
                                    echo stripslashes($this->lang->line('admin_menu_operators_list'));
                                else
                                    echo 'Institution list';
                        ?>
                                    </a>
                                </li>
                                <?php if ($allPrev == '1' || in_array('1', $institution)) { ?>
                                    <li>
                                        <a href="admin/operators/add_edit_operator_form" <?php
                                    if ($currentPage == 'add_edit_operator_form') {
                                        echo 'class="active"';
                                    }
                                    ?>>
                                            <span class="list-icon">&nbsp;</span><?php
                                if ($this->lang->line('admin_menu_add_operator') != '')
                                    echo stripslashes($this->lang->line('admin_menu_add_operator'));
                                else
                                    echo 'Add Institution';
                                    ?>
                                        </a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </li>


                    <?php } ?>










                    <?php if ((isset($transporter) && is_array($transporter)) && in_array('0', $transporter) || $allPrev == '1') { ?>
                        <li>
                            <a href="<?php echo $current_url; ?>" <?php
                        if ($currentUrl == 'transporter') {
                            echo 'class="active"';
                        }
                        ?>>
                                <span class="nav_icon male_contour"></span><?php
                        if ($this->lang->line('trasnsport_manager') != '')
                            echo stripslashes($this->lang->line('trasnsport_manager'));
                        else
                            echo 'Transport Operator';
                        ?> <span class="up_down_arrow">&nbsp;</span>
                            </a>
                            <ul class="acitem" <?php
                            if ($currentUrl == 'transporter') {
                                echo 'style="display: block;"';
                            } else {
                                echo 'style="display: none;"';
                            }
                        ?>>
                                <li>
                                    <a href="admin/transporter/display_transporter_list" <?php
                        if ($currentPage == 'display_transporter_list') {
                            echo 'class="active"';
                        }
                        ?>>
                                        <span class="list-icon">&nbsp;</span><?php
                                if ($this->lang->line('admin_menu_transporter_list') != '')
                                    echo stripslashes($this->lang->line('admin_menu_transporter_list'));
                                else
                                    echo 'Transport Operator';
                        ?>
                                    </a>
                                </li>
                                <?php if ($allPrev == '1' || in_array('1', $transporter)) { ?>
                                    <li>
                                        <a href="admin/transporter/add_edit_transporter_form" <?php
                                    if ($currentPage == 'add_edit_transporter_form') {
                                        echo 'class="active"';
                                    }
                                    ?>>
                                            <span class="list-icon">&nbsp;</span><?php
                                if ($this->lang->line('admin_menu_add_transporter') != '')
                                    echo stripslashes($this->lang->line('admin_menu_add_transporter'));
                                else
                                    echo 'Add Transport Operator';
                                    ?>
                                        </a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </li>
                    <?php } ?>













                    <?php if ((isset($category) && is_array($category)) && in_array('0', $category) || $allPrev == '1') { ?>
                        <li>
                            <a href="<?php echo $current_url; ?>" <?php
                        if (($currentUrl == 'category')) {
                            echo 'class="active"';
                        }
                        ?>>
                                <span class="nav_icon record"></span> <?php
                        if ($this->lang->line('admin_menu_vehicle_types') != '')
                            echo stripslashes($this->lang->line('admin_menu_vehicle_types'));
                        else
                            echo 'Vehicle Types';
                        ?><span class="up_down_arrow">&nbsp;</span>
                            </a>
                            <ul class="acitem" <?php
                            if ($currentUrl == 'category') {
                                echo 'style="display: block;"';
                            } else {
                                echo 'style="display: none;"';
                            }
                        ?>>

                                <li>
                                    <a href="admin/category/category_statistics" <?php
                        if ($currentPage == 'category_statistics') {
                            echo 'class="active"';
                        }
                        ?>>
                                        <span class="list-icon"></span> <?php
                                if ($this->lang->line('admin_car_types_statistics') != '')
                                    echo stripslashes($this->lang->line('admin_car_types_statistics'));
                                else
                                    echo 'Statistics';
                        ?>
                                    </a>
                                </li>

                                <li>
                                    <a href="admin/category/display_drivers_category" <?php
                                    if (($currentPage == 'display_drivers_category' || $currentPage == 'add_edit_category_types' || $currentPage == 'add_edit_category' || $currentPage == 'edit_language_category')) {
                                        echo 'class="active"';
                                    }
                        ?>>
                                        <span class="list-icon"></span> <?php
                                if ($this->lang->line('admin_display_vehicle_types_li') != '')
                                    echo stripslashes($this->lang->line('admin_display_vehicle_types_li'));
                                else
                                    echo 'Vehicle Types List';
                        ?>
                                    </a>
                                </li>

                            </ul>
                        </li>
                    <?php } ?> 











                    <?php if ((isset($banner) && is_array($banner)) && in_array('0', $banner) || $allPrev == '1') { ?>
                        <li>
                            <a href="<?php echo $current_url; ?>" <?php
                        if ($currentUrl == 'banner') {
                            echo 'class="active"';
                        }
                        ?>>
                                <span class="nav_icon ipad"></span><?php
                        if ($this->lang->line('admin_menu_banners') != '')
                            echo stripslashes($this->lang->line('admin_menu_banners'));
                        else
                            echo 'Banners';
                        ?><span class="up_down_arrow">&nbsp;</span>
                            </a>
                            <ul class="acitem" <?php
                            if ($currentUrl == 'banner') {
                                echo 'style="display: block;"';
                            } else {
                                echo 'style="display: none;"';
                            }
                        ?>>
                                <li>
                                    <a href="admin/banner/display_banner" <?php
                        if ($currentPage == 'display_banner') {
                            echo 'class="active"';
                        }
                        ?>>
                                        <span class="list-icon">&nbsp;</span><?php
                                if ($this->lang->line('admin_menu_banners_list') != '')
                                    echo stripslashes($this->lang->line('admin_menu_banners_list'));
                                else
                                    echo 'Banners List';
                        ?>
                                    </a>
                                </li>
                                <?php if ($allPrev == '1' || in_array('1', $banner)) { ?>
                                    <li>
                                        <a href="admin/banner/add_banner_form" <?php
                                    if ($currentPage == 'add_banner_form') {
                                        echo 'class="active"';
                                    }
                                    ?>>
                                            <span class="list-icon">&nbsp;</span><?php
                                if ($this->lang->line('admin_menu_add_banner') != '')
                                    echo stripslashes($this->lang->line('admin_menu_add_banner'));
                                else
                                    echo 'Add Banner';
                                    ?>
                                        </a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </li>
                    <?php } ?> 

                    <?php if ((isset($cms) && is_array($cms)) && in_array('0', $cms) || $allPrev == '1') { ?>
                        <li>
                            <a href="<?php echo $current_url; ?>" <?php
                        if ($currentUrl == 'cms') {
                            echo 'class="active"';
                        }
                        ?>>
                                <span class="nav_icon documents"></span> <?php
                        if ($this->lang->line('admin_menu_pages') != '')
                            echo stripslashes($this->lang->line('admin_menu_pages'));
                        else
                            echo 'Pages';
                        ?><span class="up_down_arrow">&nbsp;</span>
                            </a>
                            <ul class="acitem" <?php
                            if ($currentUrl == 'cms') {
                                echo 'style="display: block;"';
                            } else {
                                echo 'style="display: none;"';
                            }
                        ?>>
                                <li>
                                    <a href="admin/cms/display_cms" <?php
                        if ($currentPage == 'display_cms') {
                            echo 'class="active"';
                        }
                        ?>><span class="list-icon">&nbsp;</span><?php
                                       if ($this->lang->line('admin_menu_list_of_pages') != '')
                                           echo stripslashes($this->lang->line('admin_menu_list_of_pages'));
                                       else
                                           echo 'List of pages';
                                       ?></a>
                                </li>
                                <?php if ($allPrev == '1' || in_array('1', $cms)) { ?>
                                    <li>
                                        <a href="admin/cms/add_cms_form" <?php
                                    if ($currentPage == 'add_cms_form') {
                                        echo 'class="active"';
                                    }
                                    ?>><span class="list-icon">&nbsp;</span><?php
                                           if ($this->lang->line('admin_menu_add_main_page') != '')
                                               echo stripslashes($this->lang->line('admin_menu_add_main_page'));
                                           else
                                               echo 'Add Main Page';
                                           ?></a>
                                    </li>
                                    <li>
                                        <a href="admin/cms/add_subpage_form" <?php
                                   if ($currentPage == 'add_subpage_form') {
                                       echo 'class="active"';
                                   }
                                           ?>><span class="list-icon">&nbsp;</span><?php
                                           if ($this->lang->line('admin_menu_add_sub_page') != '')
                                               echo stripslashes($this->lang->line('admin_menu_add_sub_page'));
                                           else
                                               echo 'Add Sub Page';
                                           ?></a>
                                    </li>
                                    <li>
                                        <a href="admin/cms/add_landing_page_form" <?php
                                   if ($currentPage == 'add_landing_page_form') {
                                       echo 'class="active"';
                                   }
                                           ?>><span class="list-icon">&nbsp;</span><?php
                                           if ($this->lang->line('admin_menu_landing_page') != '')
                                               echo stripslashes($this->lang->line('admin_menu_landing_page'));
                                           else
                                               echo 'Landing Page';
                                           ?></a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </li>

                    <?php } ?> 

                    <?php if ((isset($templates) && is_array($templates)) && in_array('0', $templates) || $allPrev == '1') { ?>                
                        <li>
                            <a href="<?php echo $current_url; ?>" <?php
                        if ($currentUrl == 'templates') {
                            echo 'class="active"';
                        }
                        ?>>
                                <span class="nav_icon mail"></span><?php
                        if ($this->lang->line('admin_menu_templates') != '')
                            echo stripslashes($this->lang->line('admin_menu_templates'));
                        else
                            echo 'Templates';
                        ?><span class="up_down_arrow">&nbsp;</span>
                            </a>
                            <ul class="acitem" <?php
                            if ($currentUrl == 'templates') {
                                echo 'style="display: block;"';
                            } else {
                                echo 'style="display: none;"';
                            }
                        ?>>						
                                <li>
                                    <a href="admin/templates/display_email_template" <?php
                        if ($currentPage == 'display_email_template') {
                            echo 'class="active"';
                        }
                        ?>>
                                        <span class="list-icon">&nbsp;</span><?php
                                if ($this->lang->line('admin_menu_email_template_list') != '')
                                    echo stripslashes($this->lang->line('admin_menu_email_template_list'));
                                else
                                    echo 'Email Template List';
                        ?>
                                    </a>
                                </li>
                                <?php if ($allPrev == '1' || in_array('1', $templates)) { ?>
                                    <li>
                                        <a href="admin/templates/add_email_template" <?php
                                    if ($currentPage == 'add_email_template') {
                                        echo 'class="active"';
                                    }
                                    ?>>
                                            <span class="list-icon">&nbsp;</span><?php
                                if ($this->lang->line('admin_menu_add_email_template') != '')
                                    echo stripslashes($this->lang->line('admin_menu_add_email_template'));
                                else
                                    echo 'Add Email Template';
                                    ?>
                                        </a>
                                    </li>
                                <?php } ?>
                                <li>
                                    <a href="admin/templates/sms_template_list" <?php
                                if ($currentPage == 'sms_template_list' || $currentPage == 'add_edit_sms_templates_form' || $currentPage == 'view_sms_template') {
                                    echo 'class="active"';
                                }
                                ?>>
                                        <span class="list-icon">&nbsp;</span><?php
                                if ($this->lang->line('admin_menu_sms_template') != '')
                                    echo stripslashes($this->lang->line('admin_menu_sms_template'));
                                else
                                    echo 'SMS Templates List';
                                ?>
                                    </a>
                                </li>

                                <!-- 	<?php if ($allPrev == '1') { ?>
                                    <li>
                                        <a href="admin/templates/invoice_template" <?php
                                        if ($currentPage == 'invoice_template') {
                                            echo 'class="active"';
                                        }
                                    ?>>
                                            <span class="list-icon">&nbsp;</span><?php
                            if ($this->lang->line('invoice_template_lang') != '')
                                echo $this->lang->line('invoice_template_lang');
                            else {
                                        ?>Invoice Template<?php } ?>
                                        </a>
                                    </li>
                                <?php } ?> -->



                            </ul>
                        </li>
                    <?php } ?>


                    <?php /* if ((isset($currency) && is_array($currency)) && in_array('0', $currency) || $allPrev == '1'){ ?>
                      <li>
                      <a href="<?php echo $current_url; ?>" <?php if($currentUrl=='currency'){ echo 'class="active"';} ?>>
                      <span class="nav_icon money"></span> Currency<span class="up_down_arrow">&nbsp;</span>
                      </a>
                      <ul class="acitem" <?php if($currentUrl=='currency'){ echo 'style="display: block;"';}else{ echo 'style="display: none;"';} ?>>
                      <li>
                      <a href="admin/currency/display_currency_list" <?php if($currentPage=='display_currency_list' || $currentPage=='location_fare'){ echo 'class="active"';} ?>>
                      <span class="list-icon">&nbsp;</span>Currency List
                      </a>
                      </li>
                      <?php if ($allPrev == '1' || in_array('1', $location)){?>
                      <li>
                      <a href="admin/currency/add_edit_currency" <?php if($currentPage=='add_edit_currency'){ echo 'class="active"';} ?>>
                      <span class="list-icon">&nbsp;</span>Add Currency
                      </a>
                      </li>
                      <?php }?>
                      </ul>
                      </li>
                      <?php } */ ?>



                    <?php if ((isset($notification) && is_array($notification)) && in_array('0', $notification) || $allPrev == '1') { ?>
                        <li>
                            <a href="<?php echo $current_url; ?>" <?php
                        if ($currentUrl == 'notification' || $currentPage == 'display_notification_user_list') {
                            echo 'class="active"';
                        }
                        ?>>
                                <span class="nav_icon users"></span> <?php
                        if ($this->lang->line('admin_menu_notification') != '')
                            echo stripslashes($this->lang->line('admin_menu_notification'));
                        else
                            echo 'Notification';
                        ?><span class="up_down_arrow">&nbsp;</span>
                            </a>
                            <ul class="acitem" <?php
                            if ($currentUrl == 'notification' || $currentPage == 'display_notification_user_list' || $currentPage == 'display_notification_driver_list') {
                                echo 'style="display: block;"';
                            } else {
                                echo 'style="display: none;"';
                            }
                        ?>>
                                <li>
                                    <a href="admin/notification/display_notification_user_list" <?php
                        if ($currentPage == 'display_notification_user_list') {
                            echo 'class="active"';
                        }
                        ?>>
                                        <span class="list-icon">&nbsp;</span><?php
                                if ($this->lang->line('admin_menu_users') != '')
                                    echo stripslashes($this->lang->line('admin_menu_users'));
                                else
                                    echo 'Users';
                        ?>
                                    </a>
                                </li>
                                <li>
                                    <a href="admin/notification/display_notification_driver_list" <?php
                                    if ($currentPage == 'display_notification_driver_list') {
                                        echo 'class="active"';
                                    }
                        ?>>
                                        <span class="list-icon">&nbsp;</span><?php
                                if ($this->lang->line('admin_menu_drivers') != '')
                                    echo stripslashes($this->lang->line('admin_menu_drivers'));
                                else
                                    echo 'Drivers';
                        ?>
                                    </a>
                                </li>
                                <li>
                                    <a href="admin/notification/display_notification_templates" <?php
                                    if ($currentPage == 'display_notification_templates' || $currentPage == 'edit_notification_template' || $currentPage == 'view_notification_template') {
                                        echo 'class="active"';
                                    }
                        ?> >
                                        <span class="list-icon">&nbsp;</span><?php
                                if ($this->lang->line('admin_menu_notification_templates') != '')
                                    echo stripslashes($this->lang->line('admin_menu_notification_templates'));
                                else
                                    echo 'Notification Templates';
                        ?>
                                    </a>
                                </li>

                            </ul>
                        </li>
                    <?php } ?>









                    <?php if ((isset($testimonials) && is_array($testimonials)) && in_array('0', $testimonials) || $allPrev == '1') { ?>
                        <li>						
                            <a href="<?php echo $current_url; ?>" <?php
                        if ($currentUrl == 'testimonials' || $currentPage == 'display_testimonials_list') {
                            echo 'class="active"';
                        }
                        ?>>
                                <span class="nav_icon documents"></span>
                                <?php
                                if ($this->lang->line('admin_menu_testimonials') != '')
                                    echo stripslashes($this->lang->line('admin_menu_testimonials'));
                                else
                                    echo 'Testimonials';
                                ?>
                                <span class="up_down_arrow">&nbsp;</span>
                            </a>



                            <ul class="acitem" <?php
                            if ($currentUrl == 'testimonials' || $currentPage == 'display_testimonials_list' || $currentPage == 'edit_testimonials') {
                                echo 'style="display: block;"';
                            } else {
                                echo 'style="display: none;"';
                            }
                                ?>>
                                <li>
                                    <a href="admin/testimonials/display_testimonials_list" <?php
                        if ($currentPage == 'display_testimonials_list' || $currentPage == 'view_testimonials') {
                            echo 'class="active"';
                        }
                                ?>>
                                        <span class="list-icon">&nbsp;</span><?php
                                if ($this->lang->line('admin_menu_display_testimonials') != '')
                                    echo stripslashes($this->lang->line('admin_menu_display_testimonials'));
                                else
                                    echo 'Display Testimonials';
                                ?>
                                    </a>
                                </li>
                                <li>
                                    <a href="admin/testimonials/add_edit_testimonials_form" <?php
                                    if ($currentPage == 'add_edit_testimonials_form') {
                                        echo 'class="active"';
                                    }
                                ?>>
                                        <span class="list-icon">&nbsp;</span><?php
                                if ($this->lang->line('admin_menu_add_testimonials') != '')
                                    echo stripslashes($this->lang->line('admin_menu_add_testimonials'));
                                else
                                    echo 'Add Testimonials';
                                ?>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    <?php } ?>

                    <?php if ((isset($payment_gateway) && is_array($payment_gateway)) && in_array('0', $payment_gateway) || $allPrev == '1') { ?>
                        <!--  <li>
                             <a href="admin/payment_gateway/display_payment_gateway_list" <?php
                        if ($currentPage == 'display_payment_gateway_list' || $currentPage == 'edit_gateway_form') {
                            echo 'class="active"';
                        }
                        ?>>
                                 <span class="nav_icon shopping_cart_2">&nbsp;</span><?php
                    if ($this->lang->line('admin_menu_payment_gateway') != '')
                        echo stripslashes($this->lang->line('admin_menu_payment_gateway'));
                    else
                        echo 'Payment Gateway';
                        ?>
                             </a>
                         </li> -->
                    <?php } ?>   



                    <?php /* if ((isset($logs) && is_array($logs)) && in_array('0', $logs) || $allPrev == '1') {
                      ?>
                      <li>
                      <a href="admin/logs/display_logs" <?php
                      if ($currentUrl == 'logs') {
                      echo 'class="active"';
                      }
                      ?>>
                      <span class="nav_icon mail"></span><?php if ($this->lang->line('admin_logs') != '') echo stripslashes($this->lang->line('admin_logs')); else echo 'Logs'; ?>
                      </a>
                      </li>
                      <?php } */ ?>



                </ul>
            </div>
        </div>
    </div>


