<?php
$route['v6/api/retry-request'] = "v6/api_v6/common/retry_ride_request";
$route['v6/api/ack-request'] = 'v6/api_v6/common/ack_ride_request';
$route['v6/api/deny-request'] = 'v6/api_v6/common/deny_ride_request';

/*****        Reports    ****/
$route['v6/sent-report'] = 'v6/api_v6/reports/send_reports';
$route['v6/report-list'] = 'v6/api_v6/reports/reports_list';

/*****        Masking    ****/
$route['v6/api/phone-call'] ='v6/api_v6/masking/make_masking_call';
$route['v6/api/send-sms'] ='v6/api_v6/masking/send_sms';




?>