<?php

/*
|----------------------------------------------------------------------------
|  Mobile Application Routes Starts Here For Apple Application -- Later updates consider as a version 2
|----------------------------------------------------------------------------
*/
/* User Application payment Gateway integration */
/* IOS  */ 
$route['v6/mobile/proceed-payment'] = 'v6/mobile/user/proceed_payment';
$route['v6/mobile/userPaymentCard'] = "v6/mobile/mobile_payment/userPaymentCard";
$route['v6/mobile/payment-form'] = "v6/mobile/mobile_payment/credit_card_form";
$route['v6/mobile/payment-paypal'] = "v6/mobile/mobile_payment/paypal_payment_process";
$route['v6/mobile/stripe-manual-payment-form'] = "v6/mobile/mobile_payment/stripe_payment_form";
$route['v6/mobile/stripe-manual-payment-process'] = "v6/mobile/mobile_payment/stripe_payment_process";
$route['v6/paypal-payment-ipn'] = "admin/paypal_payment/ipnpayment";
$route['v6/mobile/success/(:any)'] = "v6/mobile/mobile_payment/pay_success";
$route['v6/mobile/failed/(:any)'] = "v6/mobile/mobile_payment/pay_failed";
$route['v6/mobile/payment/(:any)'] = "v6/mobile/mobile_payment/payment_return";

$route['v6/mobile/wallet-recharge/settings'] = "v6/mobile/mobile_wallet_recharge/wallet_money_settings";
$route['v6/mobile/wallet-recharge/payform'] = "v6/mobile/mobile_wallet_recharge/add_pay_wallet_payment_form";

$route['v6/mobile/wallet-recharge/stripe-process'] = "v6/mobile/mobile_wallet_recharge/stripe_payment_process";

$route['v6/wallet-recharge/success/(:any)'] = "v6/mobile/mobile_wallet_recharge/pay_success";
$route['v6/wallet-recharge/failed/(:any)'] = "v6/mobile/mobile_wallet_recharge/pay_failed";
$route['v6/wallet-recharge/pay-cancel'] = "v6/mobile/mobile_wallet_recharge/payment_return";
$route['v6/wallet-recharge/pay-completed'] = "v6/mobile/mobile_wallet_recharge/payment_return";



/* ANDROID  */ 
$route['v6/api/v1/mobile/proceed-payment'] ='v6/mobile/user/proceed_payment';
$route['v6/api/v1/mobile/userPaymentCard'] = "v6/mobile/mobile_payment/userPaymentCard";
$route['v6/api/v1/mobile/payment-form'] = "v6/mobile/mobile_payment/credit_card_form";
$route['v6/api/v1/mobile/payment-paypal'] = "v6/mobile/mobile_payment/paypal_payment_process";
$route['v6/api/v1/mobile/stripe-manual-payment-form'] = "v6/mobile/mobile_payment/stripe_payment_form";
$route['v6/api/v1/mobile/stripe-manual-payment-process'] = "v6/mobile/mobile_payment/stripe_payment_process";


$route['v6/api/v1/mobile/success/(:any)'] = "v6/mobile/mobile_payment/pay_success";
$route['v6/api/v1/mobile/failed/(:any)'] = "v6/mobile/mobile_payment/pay_failed";
$route['v6/api/v1/mobile/payment/(:any)'] = "v6/mobile/mobile_payment/payment_return";

$route['v6/api/v1/mobile/wallet-recharge/settings'] = "v6/mobile/mobile_wallet_recharge/wallet_money_settings";
$route['v6/api/v1/mobile/wallet-recharge/payform'] = "v6/mobile/mobile_wallet_recharge/add_pay_wallet_payment_form";

$route['v6/api/v1/mobile/wallet-recharge/stripe-process'] = "v6/mobile/mobile_wallet_recharge/stripe_payment_process";

$route['v6/api/v1/wallet-recharge/success/(:any)'] = "v6/mobile/mobile_wallet_recharge/pay_success";
$route['v6/api/v1/wallet-recharge/failed/(:any)'] = "v6/mobile/mobile_wallet_recharge/pay_failed";
$route['v6/api/v1/wallet-recharge/pay-cancel'] = "v6/mobile/mobile_wallet_recharge/payment_return";
$route['v6/api/v1/wallet-recharge/pay-completed'] = "v6/mobile/mobile_wallet_recharge/payment_return";