<?php

/*
  |----------------------------------------------------------------------------
  |  Mobile Application Routes Starts Here For Apple Application
  |----------------------------------------------------------------------------
 */
/* For User */
$route['v6/app/check-user'] = 'v6/mobile/user/check_account';
$route['v6/app/social-check'] = 'v6/mobile/user/check_social_login';
$route['v6/app/register'] = 'v6/mobile/user/register_user';
$route['v6/app/login'] = 'v6/mobile/user/login_user';
$route['v6/app/logout'] = 'v6/mobile/user/logout_user';
$route['v6/app/forgot-password'] = 'v6/mobile/user/findAccount';
$route['v6/app/social-login'] = 'v6/mobile/user/social_Login';
$route['v6/app/set-user-geo'] = 'v6/mobile/user/update_user_location';
$route['v6/app/get-map-view'] = 'v6/mobile/user/get_drivers_in_map';
$route['v6/app/get-eta'] = 'v6/mobile/user/get_eta';
$route['v6/app/apply-coupon'] = 'v6/mobile/user/apply_coupon_code';
$route['v6/app/book-ride'] = 'v6/mobile/user/booking_ride';
$route['v6/app/delete-ride'] = 'v6/mobile/user/delete_ride';
$route['v6/app/cancellation-reason'] = 'v6/mobile/user/user_cancelling_reason';
$route['v6/app/cancel-ride'] = 'v6/mobile/user/cancelling_ride';
$route['v6/app/get-location'] = 'v6/mobile/user/get_location_list';
$route['v6/app/get-category'] = 'v6/mobile/user/get_category_list';
$route['v6/app/get-ratecard'] = 'v6/mobile/user/get_rate_card';


$route['v6/app/my-rides'] = 'v6/mobile/user/all_ride_list';
$route['v6/app/view-ride'] = 'v6/mobile/user/view_ride_information';
$route['v6/app/get-invites'] = 'v6/mobile/user/get_invites';
$route['v6/app/get-earnings'] = 'v6/mobile/user/get_earnings_list';
$route['v6/app/get-money-page'] = 'v6/mobile/user/get_money_page';
$route['v6/app/get-trans-list'] = 'v6/mobile/user/get_transaction_list';
$route['v6/app/payment-list'] = 'v6/mobile/user/get_payment_list';

$route['v6/app/payment/by-cash'] = 'v6/mobile/user/payment_by_cash';
$route['v6/app/payment/by-wallet'] = 'v6/mobile/user/payment_by_wallet';
$route['v6/app/payment/by-gateway'] = 'v6/mobile/user/payment_by_gateway';
$route['v6/app/payment/by-auto-detect'] = 'v6/mobile/user/payment_by_auto_charge';

$route['v6/app/favourite/add'] = 'v6/mobile/user_profile/add_favourite_location';
$route['v6/app/favourite/edit'] = 'v6/mobile/user_profile/edit_favourite_location';
$route['v6/app/favourite/remove'] = 'v6/mobile/user_profile/remove_favourite_location';
$route['v6/app/favourite/display'] = 'v6/mobile/user_profile/display_favourite_location';
$route['v6/app/user/change-name'] = 'v6/mobile/user_profile/change_user_name';
$route['v6/app/user/change-mobile'] = 'v6/mobile/user_profile/change_user_mobile_number';
$route['v6/app/user/change-password'] = 'v6/mobile/user_profile/change_user_password';
$route['v6/app/user/reset-password'] = 'v6/mobile/user_profile/user_reset_password';
$route['v6/app/user/update-reset-password'] = 'v6/mobile/user_profile/update_reset_password';

$route['v6/app/user/set-emergency-contact'] = 'v6/mobile/user_profile/emergency_contact_add_edit';
$route['v6/app/user/view-emergency-contact'] = 'v6/mobile/user_profile/emergency_contact_view';
$route['v6/app/user/delete-emergency-contact'] = 'v6/mobile/user_profile/emergency_contact_delete';
$route['v6/app/user/alert-emergency-contact'] = 'v6/mobile/user_profile/emergency_contact_alert';


$route['v6/app/review/options-list'] = 'v6/mobile/reviews/get_review_options';
$route['v6/app/review/submit'] = 'v6/mobile/reviews/submit_reviews';

$route['v6/app/mail-invoice'] = 'v6/mobile/user/mail_invoice';


/* For Driver */
$route['v6/provider/login'] = 'v6/mobile/drivers/login_driver';
$route['v6/provider/logout'] = 'v6/mobile/drivers/logout_driver';
$route['v6/provider/update-availability'] = 'v6/mobile/drivers/update_driver_availablity';
$route['v6/provider/update-driver-geo'] = 'v6/mobile/drivers/update_driver_location';
$route['v6/provider/update-driver-mode'] = 'v6/mobile/drivers/update_driver_mode';
$route['v6/provider/accept-ride'] = 'v6/mobile/drivers/accept_ride';
$route['v6/provider/cancellation-reason'] = 'v6/mobile/drivers/driver_cancelling_reason';
$route['v6/provider/cancel-ride'] = 'v6/mobile/drivers/cancelling_ride';
$route['v6/provider/arrived'] = 'v6/mobile/drivers/location_arrived';
$route['v6/provider/begin-ride'] = 'v6/mobile/drivers/begin_ride';
$route['v6/provider/end-ride'] = 'v6/mobile/drivers/end_ride';

$route['v6/provider/get-rider-info'] = 'v6/mobile/drivers/get_rider_information';

$route['v6/provider/get-banking-info'] = 'v6/mobile/drivers/get_banking_details';
$route['v6/provider/save-banking-info'] = 'v6/mobile/drivers/save_banking_details';

$route['v6/provider/my-trips/list'] = 'v6/mobile/drivers/driver_all_ride_list';
$route['v6/provider/my-trips/view'] = 'v6/mobile/drivers/view_driver_ride_information';


$route['v6/provider/continue-trip'] = 'v6/mobile/drivers/continue_trip';

$route['v6/provider/payment-list'] = 'v6/mobile/drivers/driver_all_payment_list';
$route['v6/provider/payment-summary'] = 'v6/mobile/drivers/view_driver_payment_information';

$route['v6/provider/get-payment-list'] = 'v6/mobile/drivers/get_payment_list';
$route['v6/provider/request-payment'] = 'v6/mobile/drivers/requesting_payment';
$route['v6/provider/receive-payment'] = 'v6/mobile/drivers/receive_payment_confirmation';

$route['v6/provider/payment-received'] = 'v6/mobile/drivers/payment_received';
$route['v6/provider/payment-completed'] = 'v6/mobile/drivers/trip_completed';

/* Driver Registration on Mobile Application */
$route['v6/provider/register/get-location-list'] = 'v6/mobile/drivers_signup/get_location_list';
$route['v6/provider/register/get-category-list'] = 'v6/mobile/drivers_signup/get_category_list';
$route['v6/provider/register/get-country-list'] = 'v6/mobile/drivers_signup/get_country_list';
$route['v6/provider/register/get-location-with-category'] = 'v6/mobile/drivers_signup/get_location_with_category_list';


