<?php

/*
|-------------------------------------------------------------------
|  Mobile Application Routes For Driver Application
|-------------------------------------------------------------------
*/
 
$route['v7/api/driver/login'] = 'v7/api_v7/driver/login';
$route['v7/api/driver/logout'] = 'v7/api_v7/driver/logout';
$route['v7/api/driver/update/availability'] = 'v7/api_v7/driver/update_driver_availablity';
$route['v7/api/driver/update/mode'] = 'v7/api_v7/driver/update_driver_mode';

$route['v7/api/driver/dashboard'] = 'v7/api_v7/driver/driver_dashboard';
$route['v7/api/driver/password/change'] = 'v7/api_v7/driver/change_password';
$route['v7/api/driver/password/forgot'] = 'v7/api_v7/driver/forgot_password';

$route['v7/api/driver/get/rider'] = 'v7/api_v7/driver/get_rider_information';

$route['v7/api/driver/banking/get'] = 'v7/api_v7/driver/get_banking_details';
$route['v7/api/driver/banking/save'] = 'v7/api_v7/driver/save_banking_details';

$route['v7/api/driver/trip/list'] = 'v7/api_v7/driver/driver_all_ride_list';
$route['v7/api/driver/trip/view'] = 'v7/api_v7/driver/view_driver_ride_information';

$route['v7/api/trip/request/ack'] = 'v7/api_v7/driver/ack_ride_request';
$route['v7/api/trip/request/deny'] = 'v7/api_v7/driver/deny_ride_request';

$route['v7/api/update-ride-location'] = "v7/api_v7/driver/driver_update_ride_location";

$route['v7/api/trip/check'] = 'v7/api_v7/driver/check_trip_payment_status';

$route['v7/api/driver/payment/list'] ='v7/api_v7/driver/driver_all_payment_list';
$route['v7/api/driver/payment/summary'] ='v7/api_v7/driver/view_driver_payment_information';
#------------------------------------------

$route['v7/api/trip/payment/request'] ='v7/api_v7/driver/requesting_payment';
$route['v7/api/trip/payment/received'] ='v7/api_v7/driver/cash_payment_received';
$route['v7/api/trip/payment/completed'] ='v7/api_v7/driver/trip_completed';

	
//////////////////////19.03.2019


$route['v7/api/driver/driver_route_detail'] = 'v7/api_v7/driver/driver_student_tree';



////////////////////22.03.2019

$route['v7/api/driver/trip_list_get'] = 'v7/api_v7/driver/trip_list';


$route['v7/api/driver/delay_notify'] = 'v7/api_v7/driver/insert_delay_notifiction';

/////////////////////03.04.19

$route['v7/api/driver/trip_action_start_stop_action'] = 'v7/api_v7/driver/trip_action';