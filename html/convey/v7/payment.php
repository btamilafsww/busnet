<?php

/*
|----------------------------------------------------------------------------
|  Mobile Application Routes F
|----------------------------------------------------------------------------
*/
/* User Application payment Gateway integration */
$route['v7/api/payment/list'] = 'v7/api_v7/user/get_payment_list';
$route['v7/api/payment/cash'] = 'v7/api_v7/user/payment_by_cash';
$route['v7/api/payment/wallet'] = 'v7/api_v7/user/payment_by_wallet';
$route['v7/api/payment/gateway'] = 'v7/api_v7/user/payment_by_gateway';
$route['v7/api/payment/auto'] = 'v7/api_v7/user/payment_by_auto_charge';
$route['v7/api/payment/proceed'] = 'v7/api_v7/user/proceed_payment';

$route['v7/api/payment/proceed'] = 'v7/api_v7/user/proceed_payment';
$route['v7/api/payment/proceed'] = 'v7/api_v7/user/proceed_payment';
$route['v7/webview/trip/success/(:any)'] = "v7/api_v7/payment/success";
$route['v7/webview/trip/failed/(:any)'] = "v7/api_v7/payment/failed";
$route['v7/webview/trip/(:any)'] = "v7/api_v7/payment/returns";

$route['v7/api/invoice/send'] = 'v7/api_v7/user/mail_invoice';

$route['v7/webview/wallet/form'] = "v7/api_v7/wallet_payment/add_pay_wallet_payment_form";
$route['v7/webview/wallet/auto'] = "v7/api_v7/wallet_payment/stripe_payment_process";
$route['v7/webview/wallet/success/(:any)'] = "v7/api_v7/wallet_payment/success";
$route['v7/webview/wallet/failed/(:any)'] = "v7/api_v7/wallet_payment/failed";
$route['v7/webview/wallet/cancel'] = "v7/api_v7/wallet_payment/returns";
$route['v7/webview/wallet/completed'] = "v7/api_v7/wallet_payment/returns";