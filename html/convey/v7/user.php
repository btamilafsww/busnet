<?php

/*
|-------------------------------------------------------------------
|  Mobile Application Routes For User Application
|-------------------------------------------------------------------
*/
 
$route['v7/api/user/validate'] = 'v7/api_v7/user/check_account';
$route['v7/api/user/validate/social'] = 'v7/api_v7/user/check_social_login';
$route['v7/api/user/register'] = 'v7/api_v7/user/register_user';
$route['v7/api/user/otp/resend'] = 'v7/api_v7/user_profile/resend_otp';
$route['v7/api/user/login'] = 'v7/api_v7/user/login_user';
$route['v7/api/user/login/social'] = 'v7/api_v7/user/social_login';
$route['v7/api/user/logout'] = 'v7/api_v7/user/logout_user';


$route['v7/api/user/location/update'] = 'v7/api_v7/user/update_user_location';

$route['v7/api/locations'] = 'v7/api_v7/user/get_location_list';
$route['v7/api/locations/category'] = 'v7/api_v7/user/get_category_list';
$route['v7/api/ratecard'] = 'v7/api_v7/user/get_rate_card';

$route['v7/api/invites/get'] = 'v7/api_v7/user/get_invites';
$route['v7/api/earnings/get'] = 'v7/api_v7/user/get_earnings_list';
$route['v7/api/wallet/get'] = 'v7/api_v7/user/get_money_page';
$route['v7/api/wallet/trans'] = 'v7/api_v7/user/get_transaction_list';

$route['v7/api/trip/share'] = 'v7/api_v7/user/share_trip_status';

$route['v7/api/tips/apply'] = 'v7/api_v7/user/apply_tips_amount';
$route['v7/api/tips/remove'] = 'v7/api_v7/user/remove_tips_amount';
$route['v7/api/fare/breakup'] = 'v7/api_v7/user/get_fare_breakup';

$route['v7/api/user/password/forgot'] = 'v7/api_v7/user_profile/forgot_password_initiate';
$route['v7/api/user/password/reset'] = 'v7/api_v7/user_profile/reset_password';

$route['v7/api/favourite/location/add'] = 'v7/api_v7/user_profile/add_favourite_location';
$route['v7/api/favourite/location/edit'] = 'v7/api_v7/user_profile/edit_favourite_location';
$route['v7/api/favourite/location/remove'] = 'v7/api_v7/user_profile/remove_favourite_location';
$route['v7/api/favourite/location/display'] = 'v7/api_v7/user_profile/display_favourite_location';

$route['v7/api/favourite/driver/add'] ='v7/api_v7/user_profile/add_favourite_driver';
$route['v7/api/favourite/driver/edit'] ='v7/api_v7/user_profile/edit_favourite_driver';
$route['v7/api/favourite/driver/remove'] ='v7/api_v7/user_profile/remove_favourite_driver';
$route['v7/api/favourite/driver/display'] ='v7/api_v7/user_profile/display_favourite_driver';

$route['v7/api/user/profile/get'] = 'v7/api_v7/user_profile/get_user_profile';
$route['v7/api/user/profile/change/name'] = 'v7/api_v7/user_profile/change_user_name';
$route['v7/api/user/profile/change/mobile'] = 'v7/api_v7/user_profile/change_user_mobile_number';
$route['v7/api/user/profile/change/password'] = 'v7/api_v7/user_profile/change_user_password';
$route['v7/api/user/profile/change/image'] = 'v7/api_v7/user_profile/change_profile_image';

$route['v7/api/user/emergency/contact/update'] = 'v7/api_v7/user_profile/emergency_contact_add_edit';
$route['v7/api/user/emergency/contact/view'] = 'v7/api_v7/user_profile/emergency_contact_view';
$route['v7/api/user/emergency/contact/remove'] = 'v7/api_v7/user_profile/emergency_contact_delete';
$route['v7/api/user/emergency/contact/alert'] = 'v7/api_v7/user_profile/emergency_contact_alert';




////////////////bus tracking


$route['v7/api/user/children_list'] = 'v7/api_v7/user/student_list_user_loged_in';



$route['v7/api/user/children_current_status'] = 'v7/api_v7/user/student_status_insertion';



$route['v7/api/user/live_driver_loc'] = 'v7/api_v7/user/live_driver_location';

$route['v7/api/user/notification_setter'] = 'v7/api_v7/user/notification_allocation';


$route['v7/api/user/current_notification_status'] = 'v7/api_v7/user/notification_current_alloca';


$route['v7/api/user/submit_review'] = 'v7/api_v7/user/driver_rating';

$route['v7/api/user/get_review'] = 'v7/api_v7/user/driver_rating_get';

$route['v7/api/user/otp_request'] = 'driver/sms_twilio/send_otp_user_login';

$route['v7/api/user/otp_check_login'] = 'driver/sms_twilio/otp_verification_user_login';

$route['v7/api/user/notification_list'] = 'v7/api_v7/user/notification_list';