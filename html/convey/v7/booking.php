<?php

/*
|------------------------------------------------------------------------
|  Routes for Bookings
|------------------------------------------------------------------------
*/

$route['v7/api/location/update/driver'] = 'v7/api_v7/booking/update_driver_location';

$route['v7/api/map/drivers'] = 					'v7/api_v7/booking/get_drivers';
$route['v7/api/estimate/get'] = 					'v7/api_v7/booking/get_estimate';
$route['v7/api/coupon/apply'] = 				'v7/api_v7/booking/apply_coupon_code';
$route['v7/api/booking/make'] = 				'v7/api_v7/booking/make_booking';
$route['v7/api/booking/retry'] = 				'v7/api_v7/booking/retry_ride_request';
$route['v7/api/booking/delete'] = 			'v7/api_v7/booking/delete_ride';
$route['v7/api/booking/accept'] = 			'v7/api_v7/booking/accept_ride_request';

$route['v7/api/booking/track/user'] = 		'v7/api_v7/booking/get_track_information_user';
$route['v7/api/booking/track/driver'] = 	'v7/api_v7/booking/get_track_information_driver';

$route['v7/api/booking/cancel/reason'] = 'v7/api_v7/booking/get_cancellation_reasons';
$route['v7/api/booking/cancel'] = 			'v7/api_v7/booking/cancelling_ride';

$route['v7/api/trip/arrived'] = 					'v7/api_v7/booking/location_arrived';
$route['v7/api/trip/begin'] = 					'v7/api_v7/booking/begin_ride';
$route['v7/api/trip/end'] = 						'v7/api_v7/booking/end_ride';

$route['v7/api/trip/list/user'] = 					'v7/api_v7/booking/user_trip_list';
$route['v7/api/trip/view/user'] = 				'v7/api_v7/booking/user_trip_view';
