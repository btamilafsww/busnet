<?php

/*
|-------------------------------------------------------------------
|  Mobile Application Routes For Common Application
|-------------------------------------------------------------------
*/

$route['v7/api/get-app-info'] = 'v7/api_v7/common/get_app_info';
$route['v7/api/notification/status'] ='v7/api_v7/common/update_receive_mode';
$route['v7/api/language/update'] = "v7/api_v7/common/update_primary_language";

$route['v7/api/report/send'] = 'v7/api_v7/common/send_reports';
$route['v7/api/report/list'] = 'v7/api_v7/common/reports_list';

$route['v7/api/reviews/options'] ='v7/api_v7/common/get_review_options';
$route['v7/api/reviews/submit'] ='v7/api_v7/common/submit_reviews';
$route['v7/api/reviews/skip'] ='v7/api_v7/common/skip_reviews';

$route['v7/api/masking/call'] ='v7/api_v7/common/make_call';
$route['v7/api/masking/sms'] ='v7/api_v7/common/send_sms';